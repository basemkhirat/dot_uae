var app = require('express')(),
        http = require('http').Server(app),
        io = require('socket.io')(http),
        Redis = require('ioredis'),
        redis = new Redis(),
        client = Redis.createClient();

var port = 8080,
    users = nicknames = {};

// rooms which are currently available in chat
var rooms = ['room1', 'room2', 'room3'];

http.listen(port, function () {
    console.log('Listening on *:' + port);
});


io.on('connection', function (socket) {

    socket.on('join', function (user) {

        console.info('New client connected (id=' + user.id + ' (' + user.name + ') => socket=' + socket.id + ').');

        // save socket to emit later on a specific one
        socket.userId = user.id;
        socket.nickname = user.name;
        socket.room = user.room;
        // send client to room 
        socket.join(user.room);

        users[user.id] = socket;

        // store connected nicknames
        nicknames[user.id] = {
            'nickname': user.name,
            'socketId': socket.id,
        };

        function updateNicknames() {
            // send connected users to all sockets to display in nickname list
            io.sockets.in(socket.room).emit('chat.users', nicknames);
        }

        updateNicknames();


        // subscribe connected user to a specific channel, later he can receive message directly from our ChatController
        redis.subscribe(['chat.message', 'chat.private'], function (err, count) {

        });

        // get messages send by ChatController
        redis.on("message", function (channel, message) {
            console.log('Receive message %s from system in channel %s', message, channel);

            socket.emit(channel, message);
        });

        // get user sent message and broadcast to all connected users
        socket.on('chat.send.message', function (message) {
            console.log('Receive message ' + message.msg + ' from user in channel chat.message');
            client.set(socket.socketId, message.msg, function (err) {
            });
            io.sockets.in(socket.room).emit('chat.message', socket.room, JSON.stringify(message));
        });


        socket.on('disconnect', function () {
            if (!socket.nickname)
                return;

            delete users[user.id];
            delete nicknames[user.id];
            socket.leave(socket.room);

            updateNicknames();

            console.info('Client gone (id=' + user.id + ' => socket=' + socket.id + ').');
        });
    });
});