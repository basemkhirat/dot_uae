<?php

$websocket = new Hoa\Websocket\Server(
        new Hoa\Socket\Server('tcp://127.0.0.1:8889')
);
$websocket->on('open', function (Hoa\Core\Event\Bucket $bucket) {
    echo 'new connection', "\n";

    return;
});
$websocket->on('message', function (Hoa\Core\Event\Bucket $bucket) {
    $data = $bucket->getData();
    echo '> message ', $data['message'], "\n";
    $bucket->getSource()->send($data['message']);
    echo '< echo', "\n";

    return;
});
$websocket->on('close', function (Hoa\Core\Event\Bucket $bucket) {
    echo 'connection closed', "\n";

    return;
});
$websocket->run();
