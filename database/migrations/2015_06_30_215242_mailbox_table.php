<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MailboxTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('mailbox', function($table) {
            $table->increments('id');
            $table->integer('from_id');
            $table->integer('to_id');
            $table->string('title');
            $table->text('message');
            $table->char('open', 2)->default(0);
            $table->char('status', 2)->default(1);
            $table->char('important', 2)->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
       // Schema::drop('mailbox');
    }

}
