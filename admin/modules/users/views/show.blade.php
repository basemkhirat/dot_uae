@extends("admin::layouts.master")

@section("breadcrumb")
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-4">
        <h2><?php echo trans("users::users.users") ?></h2>
        <ol class="breadcrumb">
            <li>
                <a href="<?php echo URL::to(ADMIN . "/users"); ?>"><?php echo trans("users::users.users") ?> (<?php echo $users->total() ?>)</a>
            </li>
        </ol>
    </div>

    <div class="col-lg-6">
        <form action="" method="get" class="search_form">
            <div class="input-group">
                <input name="q" value="<?php echo Input::get("q"); ?>" type="text" class=" form-control" placeholder="<?php echo trans("users::users.search_users") ?>..."> <span class="input-group-btn">
                    <button class="btn btn-primary" type="button"> <?php echo trans("users::users.search") ?></button> </span>
            </div>
        </form>
    </div>

    <div class="col-lg-2">
        <?php if (User::can("users.create")) { ?>
            <a href="<?php echo route("users.create"); ?>" class="btn btn-primary btn-labeled btn-main"> <span class="btn-label icon fa fa-plus"></span> &nbsp; <?php echo trans("users::users.add_new") ?></a>
        <?php } ?>
    </div>
</div>
@stop

@section("content")

<div id="content-wrapper">

    <?php if (Session::has("message")) { ?>
        <div class="alert alert-success">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <?php echo Session::get("message"); ?>
        </div>
    <?php } ?>

    <form action="" method="post" class="action_form">

        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5> <?php echo trans("users::users.users") ?> </h5>
            </div>
            <div class="ibox-content">
                <div class="row">
                    <div class="col-sm-5 m-b-xs">
                        <div class="form-group">
                            <select name="action" class="form-control chosen-select chosen-rtl" style="width:auto; display: inline-block;">
                                <option value="-1" selected="selected"><?php echo trans("users::users.bulk_actions"); ?></option>
                                <option value="delete"><?php echo trans("users::users.delete"); ?></option>
                            </select>
                            <button type="submit" class="btn btn-primary" style="margin-top:5px"><?php echo trans("users::users.apply"); ?></button>
                        </div>

                    </div>
                    <div class="col-sm-4 m-b-xs">

                    </div>
                    <div class="col-sm-3">

                        <select  name="post_status" id="post_status" class="form-control per_page_filter chosen-select chosen-rtl">
                            <option value="" selected="selected">-- <?php echo trans("users::users.per_page") ?> --</option>
                            <?php foreach (array(10, 20, 30, 40) as $num) { ?>
                                <option value="<?php echo $num; ?>" <?php if ($num == $per_page) { ?> selected="selected" <?php } ?>><?php echo $num; ?></option>
                            <?php } ?>
                        </select>

                    </div>
                </div>
                <div class="table-responsive">

                    <table cellpadding="0" cellspacing="0" border="0" class="table table-striped">
                        <thead>
                            <tr>
                                <?php if (User::can("users.delete")) { ?>
                                    <th style="width:35px"><input type="checkbox" class="i-checks check_all" name="ids[]" /></th>
                                <?php } ?>
                                <th style="width:50px"><?php echo trans("users::users.photo") ?></th>
                                <th><?php echo trans("users::users.name"); ?></th>
                                <th><?php echo trans("users::users.email"); ?></th>
                                <th><?php echo trans("users::users.created"); ?></th>
                                <th><?php echo trans("users::users.role"); ?></th>
                                <th><?php echo trans("users::users.actions") ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($users as $user) { ?>
                                <tr>
                                    <?php if (User::can("users.delete")) { ?>
                                        <td>
                                            <input type="checkbox" class="i-checks" name="id[]" value="<?php echo $user->id; ?>" />
                                        </td>
                                    <?php } ?>
                                    <td>
                                        <?php if ($user->media_path != "") { ?>
                                            <img class="img-rounded" style="width:50px" src="<?php echo thumbnail($user->media_path) ?>" />
                                        <?php } else { ?>
                                            <img class="img-rounded" src="<?php echo assets("images/user.png"); ?>" />
                                        <?php } ?>
                                    </td>
                                    <td>
                                        <?php if (User::can("users.edit")) { ?>
                                            <a class="text-navy" href="<?php echo URL::to(ADMIN) ?>/users/<?php echo $user->id; ?>/edit">
                                                <?php if ($user->first_name != "") { ?>
                                                    <?php echo $user->first_name . " " . $user->last_name; ?>
                                                <?php } else { ?>
                                                    <?php echo $user->username; ?>
                                                <?php } ?>
                                            </a>
                                        <?php } else { ?>
                                            <?php if ($user->first_name != "") { ?>
                                                <?php echo $user->first_name . " " . $user->last_name; ?>
                                            <?php } else { ?>
                                                <?php echo $user->username; ?>
                                            <?php } ?>
                                        <?php } ?>
                                    </td>
                                    <td>
                                        <?php if ($user->email == "") { ?>
                                            -
                                        <?php } else { ?>
                                            <?php echo $user->email; ?>
                                        <?php } ?>
                                    </td>
                                    <td><?php echo arabic_date($user->created_at); ?></td>
                                    <td><?php echo $user->role_name; ?></td>
                                    <td class="center">

                                        <a href="<?php echo URL::to(ADMIN) ?>/posts?user_id=<?php echo $user->id; ?>">
                                            <i class="fa fa-file-text text-navy"></i>
                                        </a>

                                        <?php if (User::can("users.edit")) { ?>
                                            <a href="<?php echo URL::to(ADMIN) ?>/users/<?php echo $user->id; ?>/edit">
                                                <i class="fa fa-pencil text-navy"></i>
                                            </a>
                                        <?php } ?>

                                        <?php if (User::can("users.delete")) { ?>
                                            <a class="delete_user ask" message="<?php echo trans("users::users.sure_delete") ?>" href="<?php echo URL::route("users.delete", array("user_id" => $user->id)) ?>">
                                                <i class="fa fa-times text-navy"></i>
                                            </a>
                                        <?php } ?>

                                    </td>
                                </tr>
                            <?php } ?>

                        </tbody>
                    </table>

                </div>

                <div class="row">
                    <div class="col-sm-4">
                        <div class="dataTables_info" id="editable_info" role="status" aria-live="polite">
                            <?php echo trans("users::users.page"); ?> <?php echo $users->currentPage() ?> <?php echo trans("users::users.of") ?> <?php echo $users->lastPage() ?>
                        </div>
                    </div>
                    <div class="col-sm-8">
                        <div class="dataTables_paginate paging_simple_numbers" id="editable_paginate">
                            <?php echo str_replace('/?', '?', $users->appends(Input::all())->render()); ?>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </form>

</div>
</div> <!-- / #content-wrapper -->

<script src="<?php echo assets("wiki.js"); ?>" type="text/javascript"></script>

<script>
    $(document).ready(function () {
        
        //alert(Wiky.toWiki('<img src="http://sheldonbrown.com/images/scb_eagle_contact.jpeg" />'));

        $('.chosen-select').chosen();

        $('.i-checks').iCheck({
            checkboxClass: 'icheckbox_square-green',
            radioClass: 'iradio_square-green',
        });

        $('.check_all').on('ifChecked', function (event) {
            $("input[type=checkbox]").each(function () {
                $(this).iCheck('check');
                $(this).change();
            });
        });

        $('.check_all').on('ifUnchecked', function (event) {
            $("input[type=checkbox]").each(function () {
                $(this).iCheck('uncheck');
                $(this).change();
            });
        });

        $(".per_page_filter").change(function () {
            var base = $(this);
            var per_page = base.val();
            location.href = "<?php echo route("users.show") ?>?per_page=" + per_page;
        });

        $('.delete_user').click(function (event) {
            var self = $(this);
            var user_id = $(this).attr('data-id');
            $("#current_user_id").val(user_id);
            $('#all_users_delete option').prop('disabled', false);
            $('#all_users_delete option[value=' + user_id + ']').prop('disabled', true);
        });

    });
</script>

@stop
