@extends("admin::layouts.master")


@section("breadcrumb")
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2><?php echo trans("users::users.edit") ?></h2>
        <ol class="breadcrumb">
            <li>
                <a href="<?php echo URL::to(ADMIN . "/users"); ?>"><?php echo trans("users::users.users") ?></a>
            </li>
            <li class="active">
                <strong><?php echo trans("users::users.edit") ?></strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
        <?php if (User::can("users.create")) { ?>
            <a href="<?php echo route("users.create"); ?>" class="btn btn-primary btn-labeled btn-main"> <span class="btn-label icon fa fa-plus"></span> &nbsp; <?php echo trans("users::users.add_new") ?></a>
        <?php } ?>
    </div>
</div>
@stop

@section("content")

<script>
    $(document).ready(function () {
        var elems = Array.prototype.slice.call(document.querySelectorAll('.permission-switcher'));
        elems.forEach(function (html) {
            var switchery = new Switchery(html);
        });

        var config = {
            '.select2': {},
        }
        for (var selector in config) {
            $(selector).chosen(config[selector]);
        }
    });
</script>
<?php if (Session::has("message")) { ?>
    <div class="alert alert-success alert-dark">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <?php echo Session::get("message"); ?>
    </div>
<?php } ?>

<?php if ($errors->count() > 0) { ?>
    <div class="alert alert-danger alert-dark">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <?php echo implode(' <br /> ', $errors->all()) ?>
    </div>
<?php } ?>

<form action="" method="post" >
    <div class="row">
        <div class="col-md-6">
            <div class="panel panel-default">

                <div class="panel-body">

                    <div class="form-group input-group">
                        <span class="input-group-addon"><i class="fa fa-user"></i></span>
                        <input name="username" value="<?php echo @Input::old("username", $user->username); ?>" class="form-control input-lg" placeholder="<?php echo trans("users::users.username") ?>" />
                    </div>

                    <div class="form-group input-group">
                        <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                        <input name="password" autocomplete="off" value="" class="form-control input-lg" placeholder="<?php echo trans("users::users.password") ?>" type="password" />
                    </div>

                    <div class="form-group input-group">
                        <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                        <input name="repassword" autocomplete="off" value="" class="form-control input-lg" placeholder="<?php echo trans("users::users.confirm_password") ?>" type="password" />
                    </div>

                    <div class="form-group input-group">
                        <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                        <input name="email" value="<?php echo @Input::old("email", $user->email); ?>" class="form-control input-lg" placeholder="<?php echo trans("users::users.email") ?>" type="email" />
                    </div>

                    <div class="row">
                        <div class="col-md-2 text-center" style="position:relative; margin: 10px 0">
                            <input type="hidden" value="<?php
                            if ($user and $user->photo != "") {
                                echo $user->media_id;
                            }
                            ?>" id="user_photo_id" name="photo" /> 
                            <img id="user_photo" style="border: 1px solid #ccc; width: 100%;" src="<?php if ($user and $user->media_path != "") { ?> <?php echo thumbnail($user->media_path); ?> <?php } else { ?> <?php echo assets("images/user.png"); ?><?php } ?>" />

                            <a href="javascript:void(0)" id="change_photo" class=""><?php echo trans("users::users.change") ?></a>
                        </div>
                        <div class="col-md-10">
                            <div class="form-group">
                                <input style="margin-top: 15px;" name="first_name" value="<?php echo @Input::old("first_name", $user->first_name); ?>" class="form-control input-lg" placeholder="<?php echo trans("users::users.first_name") ?>" />
                            </div>
                            <?php /*
                              <div class="form-group">
                              <input name="last_name" value="<?php echo @Input::old("last_name", $user->last_name); ?>" class="form-control input-lg" placeholder="<?php echo trans("users::users.last_name") ?>" />
                              </div>
                             * 
                             */ ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <textarea name="about" class="markdown form-control" placeholder="<?php echo trans("users::users.about_me") ?>" rows="10"><?php echo @Input::old("about", $user->about); ?></textarea>
                    </div>


                </div> <!-- / .panel-body -->
            </div>
        </div>
        <div class="col-md-6">
            <div class="panel panel-default">

                <div class="panel-body">

                    <script>
                        $(document).ready(function () {
                            $("#change_photo").filemanager({
                                types: "png|jpg|jpeg|gif|bmp|image",
                                done: function (result, base) {
                                    if (result.length) {
                                        var file = result[0];
                                        $("#user_photo_id").val(file.media_id);
                                        $("#user_photo").attr("src", file.media_thumbnail);
                                    }
                                },
                                galleries: function (result) {
                                    console.log(result);
                                },
                                error: function (media_path) {
                                    alert(media_path + " <?php echo trans("users::users.is_not_an_image") ?>");
                                }
                            });
                        });

                    </script>

                    <?php if (User::is("superadmin")) { ?>
                        <div class="row form-group">
                            <label class="col-sm-3 control-label"><?php echo trans("users::users.role") ?></label>
                            <div class="col-sm-9">
                                <select class="form-control select2 chosen-rtl" name="role_id">
                                    <?php foreach ($roles as $role) { ?>
                                        <option <?php if ($user and $user->role_id == $role->role_id) { ?> selected="selected" <?php } ?>  value="<?php echo $role->role_id ?>"><?php echo $role->role_name; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="row form-group">
                            <label class="col-sm-3 control-label"><?php echo trans("users::users.activation") ?></label>
                            <div class="col-sm-9">
                                <select class="form-control select2 chosen-rtl" name="status">
                                    <option value="1" <?php if ($user and $user->status == 1) { ?> selected="selected" <?php } ?>><?php echo trans("users::users.activated") ?></option>
                                    <option value="0" <?php if ($user and $user->status == 0) { ?> selected="selected" <?php } ?>><?php echo trans("users::users.deactivated") ?></option>
                                </select>
                            </div>
                        </div>
                    <?php } ?>

                    <div class="row form-group">
                        <label class="col-sm-3 control-label"><?php echo trans("users::users.language") ?></label>
                        <div class="col-sm-9">
                            <select class="form-control select2 chosen-rtl" name="lang">
                                <?php foreach (Config::get("locales") as $code => $title) { ?>
                                    <option <?php if ($user and $code == $user->lang) { ?> selected="selected" <?php } ?> value="<?php echo $code; ?>"><?php echo $title; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div> <!-- / .panel-body -->
                </div>

            </div>
            
            
            
            <div class="panel panel-default">

                <div class="panel-body">

                    <div class="form-group input-group">
                        <span class="input-group-addon"><i class="fa fa-facebook"></i></span>
                        <input name="facebook" value="<?php echo @Input::old("facebook", $user->facebook); ?>" class="form-control input-lg" placeholder="<?php echo trans("users::users.facebook") ?>" />
                    </div>

                    <div class="form-group input-group">
                        <span class="input-group-addon"><i class="fa fa-twitter "></i></span>
                        <input name="twitter" value="<?php echo @Input::old("twitter", $user->twitter); ?>" class="form-control input-lg" placeholder="<?php echo trans("users::users.twitter") ?>" />
                    </div>

                    <div class="form-group input-group">
                        <span class="input-group-addon"><i class="fa fa-google-plus"></i></span>
                        <input name="google_plus" value="<?php echo @Input::old("google_plus", $user->google_plus); ?>" class="form-control input-lg" placeholder="<?php echo trans("users::users.googleplus") ?>" />
                    </div>

                    <div class="form-group input-group">
                        <span class="input-group-addon"><i class="fa fa-linkedin"></i></span>
                        <input name="linked_in" value="<?php echo @Input::old("linked_in", $user->linked_in); ?>" class="form-control input-lg" placeholder="<?php echo trans("users::users.linkedin") ?>" />
                    </div>

                </div> <!-- / .panel-body -->


            </div>
            
            <?php /* if (User::can("permissions.manage")) { ?>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <span class="panel-title"><?php echo trans("users::users.special_permissions") ?></span>
                    </div>
                    <div class="panel-body">



                        <?php
                        foreach ($modules as $module => $name) {
                            $permissions = Config::get("$module::permissions");
                            ?>

                            <?php if ($permissions != NULL) { ?>
                                <div class="panel">

                                    <a class="accordion-toggle collapsed" data-toggle="collapse" href="#collapse-<?php echo $module; ?>">
                                        <?php echo ucfirst(trans($name)); ?>
                                    </a>

                                    <div id="collapse-<?php echo $module; ?>" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <?php foreach ($permissions as $slug => $title) { ?>
                                                <label class="checkbox">
                                                    <input <?php if ($user and in_array($slug, Config::get("user_permissions"))) { ?> checked="checked" <?php } ?> type="checkbox" name="permission[]" value="<?php echo $slug; ?>" class="switcher permission-switcher switcher-sm">
                                                    <span style="margin: 0 10px 10px;">
                                                        <?php echo ucfirst(trans($title)); ?>
                                                    </span>
                                                </label>
                                            <?php } ?>
                                        </div> <!-- / .panel-body -->
                                    </div> <!-- / .collapse -->
                                </div> <!-- / .panel -->
                            <?php } ?>
                        <?php } ?>

                    </div>
                </div>
            <?php } */ ?>


        </div>
        <div style="clear:both"></div>
        <div>
            <div class="panel-footer" style="border-top: 1px solid #ececec; position: relative;">
                <div class="form-group" style="margin-bottom:0">
                    <input type="submit" class="pull-right btn btn-flat btn-primary" value="<?php echo trans("users::users.save_user") ?>" />
                </div>
            </div>
        </div>
        <!-- /6. $EASY_PIE_CHARTS -->
    </div>
</form>
<!-- /9. $UNIQUE_VISITORS_STAT_PANEL -->

</div>
</div> <!-- / #content-wrapper -->

@stop
