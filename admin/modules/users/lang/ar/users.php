<?php

return [
    "users" => "الأعضاء",
    "add_new" => "أضف جديد",
    "edit" => "تحرير عضو",
    "search_users" => "البحث فى الأعضاء",
    "per_page" => "عرض بكل صفحة",
    "bulk_actions" => "Bulk actions",
    "delete" => "حذف",
    "apply" => "حفظ",
    "photo" => "صورة",
    "name" => "الإسم",
    "email" => "البريد الإلكترونى",
    "created" => "تاريخ التسجيل",
    "actions" => "الحدث",
    "sure_delete" => "هل أنت متأكد من الحذف",
    "page" => "الصفحة",
    "add_new_user" => "أضف عضو جديد",
    "username" => "إسم المستخدم",
    "password" => "كلمة المرور",
    "confirm_password" => "تأكيد كلمة المرور",
    "email" => "البريد الإلكترونى",
    "change" => "تغيير",
    "first_name" => "الإسم بالكامل",
    "last_name" => "الإسم الأخير",
    "about_me" => "عن العضو",
    "is_not_an_image" => "ليس صورة",
    "role" => "الصلاحية",
    "activation" => "التفعيل",
    "language" => "اللغة",
    "special_permissions" => "صلاحيات خاصة",
    "save_user" => "حفظ العضو",
    "of" => "من",
    "activated" => "مفعل",
    "deactivated" => "غير مفعل",
    "user_created" => "تم إضافة العضو بنجاح",
    "user_updated" => "تم تعديل العضو بنجاح",
    "user_deleted" => "تم حذف العضو بنجاح",
    "bulk_actions" => "اختر أمر",
    
    "facebook" => "فيس بوك",
    "twitter" => "تويتر",
    "googleplus" => "جوجل بلس",
    "linkedin" => "لينكد إن",
    "search" => "بحث",
    
    "module" => "Users",
    "permissions" => [
        "create" => "إضافة الأعضاء",
        "edit" => "تحرير الأعضاء",
        "delete" => "حذف الأعضاء"
    ],
    
    "attributes" => [
        "username" => "إسم المستخدم",
        "password" => "كلمة المرور",
        "repassword" => "تأكيد كلمة المرور",
        "email" => "البريد الالكترونى",
        "first_name" => "إسم المستخدم",
    ],
];
