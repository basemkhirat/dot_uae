<?php

class UserPermission extends Model {

    protected $table = 'users_permissions';
    protected $fillable = ['permission', 'user_id'];

    public function permissions() {
        return $this->hasMany("UserPermission", "user_id", "id");
    }

}
