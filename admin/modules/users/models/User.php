<?php

use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract {

    use Authenticatable,
        CanResetPassword;

    protected $module = 'users';
    protected $creatingRules = [
        'username' => 'required|unique:users',
        "password" => "required|same:repassword",
        "repassword" => "required",
        "email" => "required|email|unique:users",
        "first_name" => "required"
    ];
    protected $updatingRules = [
        "username" => "required|unique:users,username,[id],id",
        "email" => "required|email|unique:users,email,[id],id",
        "first_name" => "required"
    ];
    protected $searchable = [
        "username", "email", "first_name"
    ];
    protected $table = 'users';
    protected $fillable = array();
    protected $guarded = array('id', "repassword", "permission");
    protected $hidden = array();
    protected $appends = ["role"];

    function setUpdateValidation($v) {

        $v->sometimes("password", "required|same:repassword", function($input) {
            return $input->password != "";
        });

        $v->sometimes("repassword", "required", function($input) {
            return $input->password != "";
        });

        return $v;
    }

    function setPasswordAttribute($password) {
        if (trim($password) != "") {
            $this->attributes["password"] = Hash::make($password);
        } else {
            unset($this->attributes["password"]);
        }
    }

    public function getRoleAttribute() {
        return Session::get("user_data")["role"];
    }

    public function permissions() {
        return $this->hasMany('UserPermission', "user_id", "id");
    }

    public function groups() {
        return $this->belongsToMany('Group', 'users_groups', 'group_id', 'user_id');
    }

    public function notifications() {
        return $this->hasMany('Notification');
    }

    public function notifyPosts() {
        return $this->belongsToMany('Post', 'post_notify');
    }

    public function getNameAttribute() {
        return $this->first_name . ' ' . $this->last_name;
    }

    public function getFirstNameAttribute($value) {
        return ($value) ? $value : '';
    }

    public function getLastNameAttribute($value) {
        return ($value) ? $value : '';
    }

    public function imageRelation() {
        return $this->hasOne('Media', 'media_id', 'photo');
    }

    public function getImageURL($size = 'full') {//empty for full size
        if ($this->imageRelation) {
            $sizeName = ($size == 'full') ? '' : $size . '-';
            return URL::to('/') . '/uploads/' . $sizeName . $this->imageRelation->media_path;
        } else {
            return assets('images/author.png');
        }
    }

    public static function is($role = "") {
        return in_array($role, array(Session::get("user_data")["role"], Auth::user()->role_id));
    }

    public static function can($params = array()) {

        $params = is_array($params) ? $params : func_get_args();

        $permissions = config::get("user_permissions");

        $permissions_string = join("", $permissions);

        if (count($params)) {
            foreach ($params as $param) {
                if (!in_array($param, $permissions)) {
                    if (!strstr($permissions_string, $param . ".")) {
                        return false;
                    }
                }
            }
            return true;
        }

        return false;
    }

    public function role() {
        return $this->hasOne('Role', 'role_id', 'role_id');
    }

    /* public function __call($method, $parameters = array()) {
      if (starts_with($method, 'is') and $method != 'is') {
      return $this->is(snake_case(substr($method, 2)));
      } elseif (starts_with($method, 'can') and $method != 'can') {
      return $this->can(snake_case(substr($method, 3)));
      }
      return $method($parameters);
      } */
}
