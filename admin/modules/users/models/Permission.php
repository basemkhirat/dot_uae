<?php

class Permission extends Model {

    protected $table = 'permissions';
    protected $primaryKey = 'permission_id';
    public $timestamps = false;
    protected $fillable = ['permission_name'];

}
