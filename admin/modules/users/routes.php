<?php

Route::group(array(
    "prefix" => ADMIN,
    "before" => "auth",
        ), function($route) {
    $route->group(array("prefix" => "users"), function($route) {
        $route->any('/', array("as" => "users.show", "uses" => "UsersController@index"));
        $route->any('/create', array("as" => "users.create", "uses" => "UsersController@create"));
        $route->any('/{id}/edit', array("as" => "users.edit", "uses" => "UsersController@edit"));
        $route->any('/delete', array("as" => "users.delete", "uses" => "UsersController@delete"));
        $route->any('/search', array("as" => "users.search", "uses" => "UsersController@search"));
    });
});
