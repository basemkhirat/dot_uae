<?php

Menu::set("sidebar", function($menu) {

    if (User::can('users')) {
        $menu->item('users', trans("admin::common.users"), "javascript:void(0)")
                ->order(16)
                ->icon("fa-users");
        
        $menu->item('users.all', trans("admin::common.users"), route("users.show"));        
    }
});

include __DIR__ ."/routes.php";
