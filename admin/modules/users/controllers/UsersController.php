<?php

use League\CommonMark\CommonMarkConverter;

class UsersController extends BackendController {

    public $data = array();

    public function index() {



       



/*
        $converter = new CommonMarkConverter();
        echo $converter->convertToHtml('<ref>{{cite web|url=http://www.filgoal.com/Arabic/News.aspx?NewsID=145306 |title=تعرف على قائمة الزمالك النهائية لموسم 2014/2015)|publisher=filgoal.com |accessdate=2015-06-12}}</ref>');




        die();



*/


        /*
          $users = ["ahmed", "ali", "hassan"];
          $posts = ["first post", "second post", "third post"];

          $string = 'dd
          {if    not  loop.first}
          xxx
          {/if}
          <th>


          {if    loop.last}
          xxx
          {/if}
          <th>';

          preg_match_all("/\{if\s+not\s+loop\.(first|last)\}([^\{]*)\{\/if\}/s", $string, $matches);

          dd($matches);

          $i = 0;

          foreach ($matches[0] as $tag) {

          $all = $matches[0][$i];
          $array = $$matches[1][$i];
          $item = $matches[2][$i];
          $content = $matches[3][$i];

          $new = "";
          foreach ($array as $value) {
          $new .= str_replace("#" . $item . "#", $value, $content);
          }

          $string = str_replace($all, $new, $string);

          $i++;
          }

          echo $string;

          return;

         */

//        return Session::put("user_data", array(
//                        "lang" => "ar",
//                        "permissions" => array("asd", "fgdf"),
//                        "photo" => "image"
//                    ));
//        
        //return User::permissions;

        /* if(User::can("authorss")){
          return "yes";
          }else{
          return "no";
          } */


        //return;

        /* CREATING TYPE
          $indexParams['index'] = 'dotuae';
          $indexParams['type'] = 'posts';
          $indexParams['body']['settings']['number_of_shards'] = 2;
          $indexParams['body']['settings']['number_of_replicas'] = 0;
          Es::create($indexParams);
          die();
         */


        // CREATING DOCUMENT
        /* return Es::index(array(
          "index" => "dotuae",
          "type" => "posts",
          "id" => "yyyy",
          "body" => array(
          "title" => "بسم الله الرحمن الرحيم أحمد",
          "content" => "this first post يس",
          "user_id" => 5,
          "tags" => ["egypt", "saudi"]
          )
          ));

          return; */
        /* get document */
        // http://localhost:9200/dotuae/posts/xxx
        /*
          $es = new Elasticsearch\Client();

          $document = $es->get(array(
          "index" => "dotuae",
          "type" => "posts",
          "id" => "xxxx",
          'ignore' => [400, 404]
          ));

          if(isset($document["found"]) and $document["found"] == 1){
          return $document["_source"];
          }else{
          return "empty";
          }

         */

        /* delete document */
        /*
          $es = new Elasticsearch\Client();
          return $es->delete(array(
          "index" => "dotuae",
          "type" => "posts",
          "id" => "xxxx"
          ));
         * 
         */
        /* */


        // http://localhost:9200/dotuae/posts/_search?q=post
        /*
          $es = new Elasticsearch\Client();

          $document = $es->search(array(
          "index" => "dotuae",
          "type" => "posts",
          "body" => array(
          'query' => array(
          "match" => array(
          "content" => 'post'
          )
          ),
          'sort' => array(
          "user_id" => array(
          "order" => "desc"
          )
          )
          )
          ));

          return $document;
         */
        /**/





        /* $searchParams = array();

          $searchParams['index'] = 'dotuae';
          //$searchParams['size'] = 50;
          $searchParams['body']['query']['query_string']['query'] = 'foofield:barstring';

          $result = Es::search($searchParams);

          return $result;
         */




        // return;

        if (Request::isMethod("post")) {
            $action = Input::get("action");
            $ids = Input::get("id");
            if (count($ids)) {
                switch ($action) {
                    case "delete":
                        User::whereIn("id", $ids)->delete();
                        UserPermission::whereIn("user_id", $ids)->delete();
                        return Redirect::back()->with("message", trans("users::users.user_deleted"));
                        break;
                }
            }
        }

        $ob = User::join("roles", "roles.role_id", "=", "users.role_id")
                ->orderBy("created_at", "DESC");

        if (Input::has("q")) {
            $q = urldecode(Input::get("q"));
            $ob->search($q);
        }

        if (Input::has("per_page")) {
            $this->data["per_page"] = $per_page = Input::get("per_page");
        } else {
            $this->data["per_page"] = $per_page = 20;
        }

        $ob->groupBy("users.id");
        $this->data["users"] = $ob->leftJoin("media", "media.media_id", "=", "users.photo")
                ->paginate($per_page);

        return View::make("users::show", $this->data);
    }

    public function create() {

        if (!User::can("users.create")) {
            return denied();
        }

        if (Request::isMethod("post")) {

            $user = new User(Input::all());
            $user->save();

            if (!$user->hasErrors()) {
                foreach ((array) Input::get("permission") as $permission) {
                    UserPermission::insert(array(
                        "user_id" => $user->id,
                        "permission" => $permission
                    ));
                }
                return Redirect::route("users.edit", array("id" => $user->id))
                                ->with("message", trans("users::users.user_created"));
            }
            return Redirect::back()->withErrors($user->getErrors())->withInput(Input::all());
        }

        $this->data["user"] = false;
        $this->data["roles"] = Role::get();
        $this->data["modules"] = Config::get("modules");

        return View::make("users::edit", $this->data);
    }

    public function edit($user_id) {

        $user = User::where("id", "=", $user_id)
                        ->leftJoin("media", "media.media_id", "=", "users.photo")->first();

        if (count($user) == 0) {
            \App::abort(404);
        }

        if (Request::isMethod("post")) {

            $user->fill(Input::all());

            $user->save();

            if ($user->hasErrors()) {
                return Redirect::back()->withErrors($user->getErrors())->withInput(Input::all());
            }

            UserPermission::where("user_id", $user_id)->delete();
            foreach ((array) Input::get("permission") as $permission) {
                UserPermission::insert(array(
                    "user_id" => $user_id,
                    "permission" => $permission
                ));
            }

            // updating user has the current session with new photo             
            if (Auth::user()->id == $user_id) {

                $user_photo = assets("images/user.png");
                if (Input::get("photo")) {
                    $photo = DB::table("media")->where("media_id", Input::get("photo"))->first();
                    if (count($photo)) {
                        $user_photo = thumbnail($photo->media_path);
                    }
                }

                Session::put("user_data", array(
                    "lang" => Input::get("lang"),
                    "photo" => $user_photo,
                    "role" => Role::where("role_id", Input::get("role_id"))->pluck("role_slug")
                ));
            }

            return Redirect::route("users.edit", array("id" => $user_id))->with("message", trans("users::users.user_updated"));
        }

        $this->data["user"] = $user;
        $this->data["roles"] = Role::get();
        $this->data["user_permissions"] = UserPermission::where("user_id", "=", $user_id)->lists("permission")->toArray();
        $this->data["modules"] = Config::get("modules");

        return View::make("users::edit", $this->data);
    }

    function search() {
        $data = User::search(urldecode(Input::get("term")))
                ->take(6)
                ->get();
        return json_encode($data);
    }

    public function delete() {

        if (!User::can("users.delete")) {
            return denied();
        }

        $user_id = Input::get("user_id");
        User::where("id", $user_id)->delete();
        UserPermission::where("user_id", $user_id)->delete();

        return Redirect::back()->with("message", trans("users::users.user_deleted"));
    }

}
