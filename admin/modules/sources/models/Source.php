<?php
class Source extends Model {

    protected $module = 'sources';
    
    protected $table = 'sources';
    protected $primaryKey = 'ID';
    
    public $timestamps = true;
    
    protected $fillable = array('*');
    protected $guarded = array('ID');
    
    protected $visible = array('*');
    protected $hidden = array();
    
    protected $searchable = ['name', 'slug', 'description', 'length'];
    protected $perPage = 16;
    
    protected $sluggable = [
        'slug' => 'name',
    ];
    
    protected $creatingRules = [
        
    ];
    
    protected $updatingRules = [
       
    ];
    
    
    
    public function image() {
        return $this->hasOne("Media", "media_id", "image_id");
    }
    
    public function user(){
        return $this->hasOne("User", "id", "user_id");
    }
    
    public function categories() {
        return $this->belongsToMany("Category", "source_categories", "source_id", "cat_id");
    }
    public function syncCategories($categories) {
        $this->categories()->sync((array) $categories);
    }
    
    public function tags() {
        return $this->belongsToMany("Tag", "source_tags", "source_id", "tag_id");
    }
    public function syncTags($tags) {
        $tag_ids = array();
        if ($tags = @explode(",", $tags)) {
            foreach ($tags as $tag_name) {
                $tag = Tag::select("tag_id")->where("tag_name", $tag_name)->first();
                if (count($tag)) {
                    // tag exists
                    $tag_ids[] = $tag->tag_id;
                } else {
                    // create new tag
                    $tag = new Tag();
                    $tag->tag_name = $tag_name;
                    $tag->tag_slug = Str::slug($tag_name);
                    $tag->save();
                    $tag_ids[] = $tag->tag_id;
                }
            }
        }
        $this->tags()->sync($tag_ids);
    }
}