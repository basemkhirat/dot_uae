@extends("admin::layouts.master")
@section("breadcrumb")
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-4">
        <h2>
            <i class="fa fa-bar-chart"></i>
            <?php echo trans("sources::sources.sources") ?>
        </h2>
        <ol class="breadcrumb">
            <li>
                <a href="<?php echo URL::to(ADMIN . "/sources"); ?>"><?php echo trans("sources::sources.sources") ?> (<?php echo $sources->total() ?>)</a>
            </li>
        </ol>
    </div>
    <div class="col-lg-6"></div>
    <div class="col-lg-2">
        <a href="<?php echo route("sources.create"); ?>" class="btn btn-primary btn-labeled btn-main"> <span class="btn-label icon fa fa-plus"></span> &nbsp; <?php echo trans("sources::sources.add_new") ?></a>
    </div>
</div>
@stop
@section("content")
<div id="content-wrapper">
    @include("admin::partials.messages")
    <form action="" method="get" class="filter-form">
        <input type="hidden" name="per_page" value="<?php echo Input::get('per_page') ?>" />
        <input type="hidden" name="tag_id" value="<?php echo Input::get('tag_id') ?>" />
        <div class="row">
            <div class="col-sm-5 m-b-xs">
                <div class="form-group">
                    <select name="sort" class="form-control chosen-select chosen-rtl" style="width:auto; display: inline-block;">
                        <option value="ID" <?php if(Input::get("sort") == "ID"){ ?> selected='selected' <?php } ?>><?php echo trans("sources::sources.sort_by"); ?></option>
                        
                        <option value="name" <?php if(Input::get("sort") == "name"){ ?> selected='selected' <?php } ?>><?php echo trans("sources::sources.attributes.name"); ?></option>
                        <option value="published_at" <?php if(Input::get("sort") == "published_at"){ ?> selected='selected' <?php } ?>><?php echo trans("sources::sources.attributes.published_at"); ?></option>
                        <option value="created_at" <?php if(Input::get("sort") == "created_at"){ ?> selected='selected' <?php } ?>><?php echo trans("sources::sources.attributes.created_at"); ?></option>
                        <option value="updated_at" <?php if(Input::get("sort") == "updated_at"){ ?> selected='selected' <?php } ?>><?php echo trans("sources::sources.attributes.updated_at"); ?></option>
                    </select>
                    <select name="order" class="form-control chosen-select chosen-rtl" style="width:auto; display: inline-block;">
                        <option value="DESC" <?php if(Input::get("order") == "DESC"){ ?> selected='selected' <?php } ?>><?php echo trans("sources::sources.desc"); ?></option>
                        <option value="ASC" <?php if(Input::get("order") == "ASC"){ ?> selected='selected' <?php } ?>><?php echo trans("sources::sources.asc"); ?></option>
                    </select>
                    <button type="submit" class="btn btn-primary"><?php echo trans("sources::sources.order"); ?></button>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                    <select name="status" class="form-control chosen-select chosen-rtl" style="width:auto; display: inline-block;">
                        <option value=""><?php echo trans("sources::sources.all"); ?></option>
                        <option <?php if(Input::get("status") == "1"){ ?> selected='selected' <?php } ?> value="1"><?php echo trans("sources::sources.activated"); ?></option>
                        <option <?php if(Input::get("status") == "0"){ ?> selected='selected' <?php } ?> value="0"><?php echo trans("sources::sources.deactivated"); ?></option>
                    </select>
                    <select name="category_id" class="form-control chosen-select chosen-rtl" style="width:auto; display: inline-block;">
                        <option value=""><?php echo trans("sources::sources.all_categories"); ?></option>
                        <?php
                        echo Category::tree(array(
                            "row" => function($row, $depth) {
                                $html = '<option value="' . $row->cat_id . '"';
                                if (Input::get("category_id") == $row->cat_id) {
                                    $html .= 'selected="selected"';
                                }
                                $html .= '>' . str_repeat("&nbsp;", $depth * 10) . " - " . $row->cat_name . '</option>';
                                return $html;
                            }
                        ));
                        ?>
                    </select>
                    <button type="submit" class="btn btn-primary"><?php echo trans("sources::sources.filter"); ?></button>
                </div>
            </div>
            
            <div class="col-lg-3">
                <form action="" method="get" class="search_form">
                    <div class="input-group">
                        <input name="q" value="<?php echo Input::get("q"); ?>" type="text" class=" form-control" placeholder="<?php echo trans("sources::sources.search_sources") ?> ..."> 
                        <span class="input-group-btn">
                            <button class="btn btn-primary" type="submit"> <i class="fa fa-search"></i> </button> 
                        </span>
                    </div>
                </form>
            </div>
        </div>
    </form>
    <form action="" method="post" class="action_form">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>
                    <i class="fa fa-bar-chart"></i>
                    <?php echo trans("sources::sources.sources") ?> 
                </h5>
            </div>
            <div class="ibox-content">
                <?php if(count($sources)){ ?>
                <div class="row">
                    <div class="col-sm-3 m-b-xs">
                        <div class="form-group">
                            <select name="action" class="form-control chosen-select chosen-rtl" style="width:auto; display: inline-block;">
                                <option value="-1" selected="selected"><?php echo trans("sources::sources.bulk_actions"); ?></option>
                                <option value="delete"><?php echo trans("sources::sources.delete"); ?></option>
                                <option value="activate"><?php echo trans("sources::sources.activate"); ?></option>
                                <option value="deactivate"><?php echo trans("sources::sources.deactivate"); ?></option>
                            </select>
                            <button type="submit" class="btn btn-primary"><?php echo trans("sources::sources.apply"); ?></button>
                        </div>
                    </div>
                    <div class="col-sm-6 m-b-xs"></div>
                    <div class="col-sm-3">
                        <select  name="post_status" id="post_status" class="form-control per_page_filter chosen-select chosen-rtl">
                            <option value="" selected="selected">-- <?php echo trans("sources::sources.per_page") ?> --</option>
                            <?php foreach (array(10, 20, 30, 40) as $num) { ?>
                            <option value="<?php echo $num; ?>" <?php if ($num == $per_page) { ?> selected="selected" <?php } ?>><?php echo $num; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="table-responsive">
                    <table cellpadding="0" cellspacing="0" border="0" class="table table-striped">
                        <thead>
                            <tr>
                                <th style="width:35px"><input type="checkbox" class="i-checks check_all" name="ids[]" /></th>
                                
                                <th><?php echo trans("sources::sources.attributes.name"); ?></th>
                                <th><?php echo trans("sources::sources.attributes.published_at"); ?></th>
                                <th><?php echo trans("sources::sources.attributes.created_at"); ?></th>
                                <th><?php echo trans("sources::sources.attributes.updated_at"); ?></th>
                                <th><?php echo trans("sources::sources.user"); ?></th>
                                <th><?php echo trans("sources::sources.categories"); ?></th>
                                <th><?php echo trans("sources::sources.tags"); ?></th>
                                <th><?php echo trans("sources::sources.attributes.status"); ?></th>
                                <th><?php echo trans("sources::sources.actions"); ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($sources as $source) { ?>
                            <tr>
                                <td>
                                    <input type="checkbox" class="i-checks" name="ID[]" value="<?php echo $source->ID; ?>" />
                                </td>
                                
                                <td>
                                    <a data-toggle="tooltip" data-placement="bottom" title="<?php echo trans("sources::sources.edit"); ?>" class="text-navy" href="<?php echo route("sources.edit", array("ID" => $source->ID)); ?>">
                                            <?php echo $source->name; ?>
                                        </a>
                                    
                                </td>
                                <td>
                                    
                                    <?php echo $source->published_at; ?>
                                </td>
                                <td>
                                    
                                    <?php echo $source->created_at; ?>
                                </td>
                                <td>
                                    
                                    <?php echo $source->updated_at; ?>
                                </td>
                                <td>
                                    <a href="?user_id=<?php echo @$source->user->id; ?>" class="text-navy">
                                    <?php echo @$source->user->first_name; ?>
                                    </a>
                                </td>
                                <td>
                                    <?php foreach($source->categories as $category){ ?>
                                    <a href="?category_id=<?php echo $category->cat_id; ?>" class="text-navy">
                                        <?php echo $category->cat_name; ?>
                                    </a> 
                                    <?php } ?>
                                </td>
                                <td>
                                    <?php foreach($source->tags as $tag){ ?>
                                    <a href="?tag_id=<?php echo $tag->tag_id; ?>" class="text-navy"> 
                                        <span class="badge badge-primary"><?php echo $tag->tag_name; ?></span>
                                    </a>
                                    <?php } ?>
                                </td>
                                <td>
                                   <?php if($source->status){ ?>
                                    <a data-toggle="tooltip" data-placement="bottom" title="<?php echo trans("sources::sources.activated"); ?>" class="ask" message="<?php echo trans('sources::sources.sure_deactivate') ?>" href="<?php echo URL::route("sources.status", array("ID" => $source->ID, "status" => 0)) ?>">
                                        <i class="fa fa-toggle-off text-success"></i>
                                    </a>
                                   <?php }else{ ?>
                                   <a data-toggle="tooltip" data-placement="bottom" title="<?php echo trans("sources::sources.deactivated"); ?>" class="ask" message="<?php echo trans('sources::sources.sure_activate') ?>" href="<?php echo URL::route("sources.status", array("ID" => $source->ID, "status" => 1)) ?>">
                                        <i class="fa fa-toggle-off text-danger"></i>
                                    </a>
                                   <?php } ?>
                                </td>
                                <td class="center">
                                    <a data-toggle="tooltip" data-placement="bottom" title="<?php echo trans("sources::sources.edit"); ?>" href="<?php echo route("sources.edit", array("ID" => $source->ID)); ?>">
                                        <i class="fa fa-pencil text-navy"></i>
                                    </a>
                                    <a data-toggle="tooltip" data-placement="bottom" title="<?php echo trans("sources::sources.delete"); ?>" class="delete_user ask" message="<?php echo trans("sources::sources.sure_delete") ?>" href="<?php echo URL::route("sources.delete", array("ID" => $source->ID)) ?>">
                                        <i class="fa fa-times text-navy"></i>
                                    </a>
                                </td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="dataTables_info" id="editable_info" role="status" aria-live="polite">
                            <?php echo trans("sources::sources.page"); ?> <?php echo $sources->currentPage() ?> <?php echo trans("sources::sources.of") ?> <?php echo $sources->lastPage() ?>
                        </div>
                    </div>
                    <div class="col-sm-8">
                        <div class="dataTables_paginate paging_simple_numbers" id="editable_paginate">
                            <?php echo str_replace('/?', '?', $sources->appends(Input::all())->render()); ?>
                        </div>
                    </div>
                </div>
                <?php }else{ ?>
                <?php echo trans("sources::sources.no_records"); ?>
                <?php } ?>
            </div>
        </div>
    </form>
</div>
@section("footer")
@parent
<script>
    $(document).ready(function () {
        $('[data-toggle="tooltip"]').tooltip();
        
        $('.chosen-select').chosen();
    
        $('.i-checks').iCheck({
            checkboxClass: 'icheckbox_square-green',
            radioClass: 'iradio_square-green',
        });
        $('.check_all').on('ifChecked', function (event) {
            $("input[type=checkbox]").each(function () {
                $(this).iCheck('check');
                $(this).change();
            });
        });
        $('.check_all').on('ifUnchecked', function (event) {
            $("input[type=checkbox]").each(function () {
                $(this).iCheck('uncheck');
                $(this).change();
            });
        });
        $(".filter-form input[name=per_page]").val($(".per_page_filter").val());
        $(".per_page_filter").change(function () {
            var base = $(this);
            var per_page = base.val();
            $(".filter-form input[name=per_page]").val(per_page);
            $(".filter-form").submit();
        });
    });
</script>
@stop
@stop