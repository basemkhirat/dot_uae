@extends("admin::layouts.master")
@section("breadcrumb")
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>
        <i class="fa fa-bar-chart"></i>
        <?php if($source){
            echo trans("sources::sources.edit");
           }else{
            echo trans("sources::sources.add_new");
           } ?>
        </h2>
        <ol class="breadcrumb">
            <li>
                <a href="<?php echo URL::to(ADMIN . "/sources"); ?>"><?php echo trans("sources::sources.sources"); ?></a>
            </li>
            <li class="active">
                <strong>
                    <?php if($source){
                     echo trans("sources::sources.edit");
                    }else{
                     echo trans("sources::sources.add_new");
                    } ?>
                </strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-3">
        <?php if($source){ ?>
           <a href="<?php echo route("sources.create"); ?>" class="btn btn-primary btn-labeled btn-main"> <span class="btn-label icon fa fa-plus"></span> &nbsp; <?php echo trans("sources::sources.add_new") ?></a>
        <?php } else { ?>
            <a href="<?php echo route("sources.show"); ?>" class="btn btn-primary btn-labeled btn-main"> 
                <?php echo trans("sources::sources.back_to_sources") ?>
                &nbsp;  <i class="fa fa-chevron-left"></i>
            </a>
        <?php } ?>
    </div>
</div>
@stop
@section("content")
@include("admin::partials.messages")
<form action="" method="post">
    <div class="row">
        <div class="col-md-8">
            <div class="panel panel-default">
                <div class="panel-body">
                    
                    
                    <div class="form-group">
                        <label for="input-name"><?php echo trans("sources::sources.attributes.name") ?></label>
                        <input name="name" type="text" value="<?php echo @Input::old("name", $source->name); ?>" class="form-control" id="input-name" placeholder="<?php echo trans("sources::sources.attributes.name") ?>">
                    </div>
                    
                    <div class="form-group">
                        <label for="input-description"><?php echo trans("sources::sources.attributes.description") ?></label>
                        @include("admin::partials.editor", ["name" => "description", "id" => "description", "value" => @$source->description])
                    </div>
                    
                    <div class="form-group">
                        <label for="input-published_at"><?php echo trans("sources::sources.attributes.published_at") ?></label>
                        <div class="input-group date datetimepick">
                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            <input name="published_at" type="text" value="<?php echo @Input::old("published_at", $source->published_at); ?>" class="form-control" id="input-published_at" placeholder="<?php echo trans("sources::sources.attributes.published_at") ?>">
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label for="input-deleted_at"><?php echo trans("sources::sources.attributes.deleted_at") ?></label>
                        <div class="input-group date datepick">
                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            <input name="deleted_at" type="text" value="<?php echo @Input::old("deleted_at", $source->deleted_at); ?>" class="form-control" id="input-deleted_at" placeholder="<?php echo trans("sources::sources.attributes.deleted_at") ?>">
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label for="input-length"><?php echo trans("sources::sources.attributes.length") ?></label>
                            <select class="form-control chosen-select chosen-rtl" name="length">
<option value="one" <?php if ($source and $source->length == "one") { ?> selected="selected" <?php } ?>><?php echo trans("sources::sources.length_one") ?></option>
<option value="two" <?php if ($source and $source->length == "two") { ?> selected="selected" <?php } ?>><?php echo trans("sources::sources.length_two") ?></option>
<option value="three" <?php if ($source and $source->length == "three") { ?> selected="selected" <?php } ?>><?php echo trans("sources::sources.length_three") ?></option>
</select></div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-check-square"></i>
                    <?php echo trans("sources::sources.source_status"); ?>
                </div>
                <div class="panel-body">
                    <div class="form-group switch-row">
                        <label class="col-sm-9 control-label" for="input-status"><?php echo trans("sources::sources.attributes.status") ?></label>
                        <div class="col-sm-3">
                            <input <?php if (@Input::old("status", $source->status)) { ?> checked="checked" <?php } ?> type="checkbox" id="input-status" name="status" value="1" class="status-switcher switcher-sm">
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-picture-o"></i>
                    <?php echo trans("sources::sources.add_image"); ?>
                </div>
                <div class="panel-body form-group">
                    <div class="row post-image-block">
                        <input type="hidden" name="image_id" class="post-image-id" value="<?php
                        if ($source and @ $source->image->media_path != "") {
                            echo @$source->image->media_id;
                        }
                        ?>">
                        <a class="change-post-image label" href="javascript:void(0)">
                            <i class="fa fa-pencil text-navy"></i>
                            <?php echo trans("sources::sources.change_image"); ?>
                        </a>
                        <a class="post-image-preview" href="javascript:void(0)">
                            <img width="100%" height="130px" class="post-image" src="<?php if ($source and @ $source->image->media_id != "") { ?> <?php echo thumbnail(@$source->image->media_path); ?> <?php } else { ?> <?php echo assets("default/post.png"); ?><?php } ?>">
                        </a>
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-folder"></i>
                    <?php echo trans("sources::sources.add_category"); ?>
                </div>
                <div class="panel-body">
                    <ul class='tree-views'>
                        <?php
                        echo Category::tree(array(
                            "row" => function($row, $depth) use ($source_categories) {
                                $html = "<li><div class='tree-row checkbox i-checks'><a class='expand' href='javascript:void(0)'>+</a><label><input type='checkbox' ";
                                if (in_array($row->cat_id, $source_categories)) {
                                    $html .= 'checked="checked"';
                                }
                                $html .= "name='categories[]' value='" . $row->cat_id . "'> " . $row->cat_name . "</label></div>";
                                return $html;
                            }
                        ));
                        ?>
                    </ul>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-tags"></i>
                    <?php echo trans("sources::sources.add_tag"); ?>
                </div>
                <div class="panel-body">
                    <div class="form-group" style="position:relative">
                        <input type="hidden" name="tags" id="tags_names" value="<?php echo join(",", $source_tags); ?>">
                        <ul id="mytags"></ul>
                    </div>
                </div>
            </div>
        </div>
        <div style="clear:both"></div>
        <div>
            <div class="panel-footer" style="border-top: 1px solid #ececec; position: relative;">
                <div class="form-group" style="margin-bottom:0">
                    <input type="submit" class="pull-right btn btn-flat btn-primary" value="<?php echo trans("sources::sources.save_source") ?>" />
                </div>
            </div>
        </div>
    </div>
</form>
@section("header")
@parent
<link href="<?php echo assets("tagit") ?>/jquery.tagit.css" rel="stylesheet" type="text/css">
<link href="<?php echo assets("tagit") ?>/tagit.ui-zendesk.css" rel="stylesheet" type="text/css">
<link href="<?php echo assets('css/plugins/datetimepicker/bootstrap-datetimepicker.min.css') ?>" rel="stylesheet" type="text/css">
@stop
@section("footer")
@parent
<script type="text/javascript" src="<?php echo assets("tagit") ?>/tag-it.js"></script>
<script type="text/javascript" src="<?php echo assets('ckeditor/ckeditor.js') ?>"></script>
<script type="text/javascript" src="<?php echo assets('js/plugins/moment/moment.min.js') ?>"></script>
<script type="text/javascript" src="<?php echo assets('js/plugins/datetimepicker/bootstrap-datetimepicker.min.js') ?>"></script>
<script>
    $(document).ready(function () {
        var elems = Array.prototype.slice.call(document.querySelectorAll('.status-switcher'));
        elems.forEach(function (html) {
            var switchery = new Switchery(html, {size: 'small'});
        });
        
        $('.datetimepick').datetimepicker({
                        format: 'YYYY-MM-DD HH:mm:ss',
                    });
        $('.datepick').datetimepicker({
                        format: 'YYYY-MM-DD',
                    });
        $('.chosen-select').chosen();
        $(".change-post-image").filemanager({
            types: "image",
            done: function (result, base) {
                if (result.length) {
                    var file = result[0];
                    base.parents(".post-image-block").find(".post-image-id").first().val(file.media_id);
                    base.parents(".post-image-block").find(".post-image").first().attr("src", file.media_thumbnail);
                }
            },
            error: function (media_path) {
                alert("<?php echo trans("sources::sources.not_allowed_file") ?>");
            }
        });
        $("#mytags").tagit({
            singleField: true,
            singleFieldNode: $('#tags_names'),
            allowSpaces: true,
            minLength: 2,
            placeholderText: "",
            removeConfirmation: true,
            tagSource: function (request, response) {
                $.ajax({
                    url: "<?php echo route("tags.search"); ?>",
                    data: {term: request.term},
                    dataType: "json",
                    success: function (data) {
                        response($.map(data, function (item) {
                            return {
                                label: item.tag_name,
                                value: item.tag_name
                            }
                        }));
                    }
                });
            }
        });
        $('.i-checks').iCheck({
            checkboxClass: 'icheckbox_square-green',
            radioClass: 'iradio_square-green',
        });
        
        $('.tree-views input[type=checkbox]').on('ifChecked', function () {
            var checkbox = $(this).closest('ul').parent("li").find("input[type=checkbox]").first();
            checkbox.iCheck('check');
            checkbox.change();
        });
        $('.tree-views input[type=checkbox]').on('ifUnchecked', function () {
            var checkbox = $(this).closest('ul').parent("li").find("input[type=checkbox]").first();
            checkbox.iCheck('uncheck');
            checkbox.change();
        });
        $(".expand").each(function (index, element) {
            var base = $(this);
            if (base.parents("li").find("ul").first().length > 0) {
                base.text("+");
            } else {
                base.text("-");
            }
        });
        $("body").on("click", ".expand", function () {
            var base = $(this);
            if (base.text() == "+") {
                if (base.closest("li").find("ul").length > 0) {
                    base.closest("li").find("ul").first().slideDown("fast");
                    base.text("-");
                }
                base.closest("li").find(".expand").last().text("-");
            } else {
                if (base.closest("li").find("ul").length > 0) {
                    base.closest("li").find("ul").first().slideUp("fast");
                    base.text("+");
                }
            }
            return false;
        });
    });
</script>
@stop
@stop