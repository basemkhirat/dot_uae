<?php
Route::group(array(
    "prefix" => ADMIN,
    "before" => "auth",
        ), function($route) {
        $route->group(array("prefix" => "sources"), function($route) {
            $route->any('/', array("as" => "sources.show", "uses" => "SourcesController@index"));
            $route->any('/create', array("as" => "sources.create", "uses" => "SourcesController@create"));
            $route->any('/{ID}/edit', array("as" => "sources.edit", "uses" => "SourcesController@edit"));
            $route->any('/delete', array("as" => "sources.delete", "uses" => "SourcesController@delete"));
            $route->any('/{status}/status', array("as" => "sources.status", "uses" => "SourcesController@status"));
            
        });
});
