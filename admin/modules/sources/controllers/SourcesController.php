<?php
class SourcesController extends BackendController {
    
    protected $data = [];
    function __construct(){
        parent::__construct();
    }
    
    function index(){
    
        if (Request::isMethod("post")) {
            if (Input::has("action")) {
                switch (Input::get("action")) {
                    case "delete":
                        return $this->delete();
                    case "activate":
                        return $this->status(1);
                    case "deactivate":
                        return $this->status(0);
                }
            }
        }
        
        $this->data["sort"] = (Input::has("sort")) ? Input::get("sort") : "ID";
        $this->data["order"] = (Input::has("order")) ? Input::get("order") : "DESC";
        $this->data['per_page'] = (Input::has("per_page")) ? Input::get("per_page") : NULL;
    
        $query = Source::with('image', 'user', 'categories', 'tags')->orderBy($this->data["sort"], $this->data["order"]);
        if (Input::has("tag_id")) {
            $query->whereHas("tags", function($query) {
                $query->where("tags.tag_id", Input::get("tag_id"));
            });
        }
        if (Input::has("category_id")) {
            $query->whereHas("categories", function($query) {
                $query->where("categories.cat_id", Input::get("category_id"));
            });
        }
        if (Input::has("user_id")) {
            $query->whereHas("user", function($query) {
                $query->where("users.id", Input::get("user_id"));
            });
        }
        if (Input::has("status")) {
            $query->where("status", Input::get("status"));
        }
        if (Input::has("q")) {
            $query->search(urldecode(Input::get("q")));
        }
        $this->data["sources"] = $query->paginate($this->data['per_page']);
        return View::make("sources::show", $this->data);
    
    }
    
    public function create() {
        if (Request::isMethod("post")) {
            $source = new Source();
            
            $source->name = Input::get('name');
            $source->description = Input::get('description');
            $source->deleted_at = Input::get('deleted_at');
            $source->published_at = Input::get('published_at');
            $source->length = Input::get('length');
            $source->image_id = Input::get('image_id');
            $source->user_id = Auth::user()->id;
            $source->status = Input::get("status", 0);
            
            if(!$source->validate()){
                return Redirect::back()->withErrors($source->errors())->withInput(Input::all());
            }
            
            $source->save();
            $source->syncCategories(Input::get("categories"));
            $source->syncTags(Input::get("tags"));
            
            return Redirect::route("sources.edit", array("ID" => $source->ID))
                    ->with("message", trans("sources::sources.events.created"));
        }
        $this->data["source_categories"] = array();
        $this->data["source_tags"] = array();
        $this->data["source"] = false;
        return View::make("sources::edit", $this->data);
    }
    
    public function edit($ID) {
        $source = Source::findOrFail($ID);
        if (Request::isMethod("post")) {
        
            
            $source->name = Input::get('name');
            $source->description = Input::get('description');
            $source->deleted_at = Input::get('deleted_at');
            $source->published_at = Input::get('published_at');
            $source->length = Input::get('length');
            $source->image_id = Input::get('image_id');
            $source->status = Input::get("status", 0);
            
            if(!$source->validate()){
                return Redirect::back()->withErrors($source->errors())->withInput(Input::all());
            }
            
            $source->save();
            $source->syncCategories(Input::get("categories"));
            $source->syncTags(Input::get("tags"));
            return Redirect::route("sources.edit", array("ID" => $ID))->with("message", trans("sources::sources.events.updated"));
        }
        
        $this->data["source_categories"] = $source->categories->lists("cat_id")->toArray();
        $this->data["source_tags"] = $source->tags->lists("tag_name")->toArray();
        $this->data["source"] = $source;
        return View::make("sources::edit", $this->data);
    }
    
    public function delete() {
        $ids = Input::get("ID");
        if (!is_array($ids)) {
            $ids = array($ids);
        }
        foreach ($ids as $ID) {
            $source = Source::findOrFail($ID);
            $source->categories()->detach();
            $source->tags()->detach();
            $source->delete();
        }
        return Redirect::back()->with("message", trans("sources::sources.events.deleted"));
    }
    public function status($status) {
        $ids = Input::get("ID");
        if (!is_array($ids)) {
            $ids = array($ids);
        }
        foreach ($ids as $ID) {
            $source = Source::findOrFail($ID);
            $source->status = $status;
            $source->save();
        }
        
        if($status){
            $message = trans("sources::sources.events.activated");
        }else{
            $message = trans("sources::sources.events.deactivated");
        }
        return Redirect::back()->with("message", $message);
    }
    
}