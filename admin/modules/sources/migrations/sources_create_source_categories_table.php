<?php

use Illuminate\Database\Migrations\Migration;
 
class Sources_create_source_categories_table extends Migration {

    public function up(){
    
        Schema::create('source_categories', function($table) {
		$table->unsignedInteger('source_id');
		$table->unsignedInteger('cat_id');
	});
    
    }

    public function down(){
    
        Schema::drop('source_categories');
    
    }

}