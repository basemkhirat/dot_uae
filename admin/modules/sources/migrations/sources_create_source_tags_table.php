<?php

use Illuminate\Database\Migrations\Migration;
 
class Sources_create_source_tags_table extends Migration {

    public function up(){
    
        Schema::create('source_tags', function($table) {
		$table->unsignedInteger('source_id');
		$table->unsignedInteger('tag_id');
	});
    
    }

    public function down(){
    
        Schema::drop('source_tags');
    
    }

}