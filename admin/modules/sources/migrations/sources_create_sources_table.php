<?php

use Illuminate\Database\Migrations\Migration;
 
class Sources_create_sources_table extends Migration {

    public function up(){
    
        Schema::create('sources', function($table) {
		$table->increments('ID')->unsigned();
		$table->string('name', 200);
		$table->string('slug', 200);
		$table->text('description');
		$table->date('deleted_at');
		$table->dateTime('published_at');
		$table->string('length', 10);
		$table->unsignedInteger('image_id');
		$table->unsignedInteger('user_id');
		$table->timestamp('created_at')->default("0000-00-00 00:00:00");
		$table->timestamp('updated_at')->default("0000-00-00 00:00:00");
		$table->tinyInteger('status');
	});
    
    }

    public function down(){
    
        Schema::drop('sources');
    
    }

}