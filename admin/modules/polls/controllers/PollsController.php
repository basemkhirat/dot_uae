<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PollsController
 *
 * @author Khaled PC
 */
class PollsController extends BackendController {

    var $ipp = 10;
    var $data = [];
    public $conn = '';

    public function __construct() {
        $this->conn = Session::get('conn');
        $this->beforeFilter('csrf', [
            'on' => [
                'post',
                'put'
            ]
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index() {
        $queryString = Input::all();

        $query = Poll::with('answers', 'votes')->where('poll_parent', '=', '0')->where('site', '=', $this->conn)->orderBy('created_at', 'desc');
        foreach ($queryString as $key => $value) {
            switch ($key) {
                case 'poll_status':
                    if ($value !== '-1') {
                        $query->where('poll_status', '=', $value);
                    }
                    break;
                case 'is_active':
                    if ($value !== '-1') {
                        ($value) ? $query->nowActive() : $query->nowInactive();
                    }
                    break;
                case 'search':
                    if (!empty($value)) {

                        $query->where('poll_text', 'LIKE', '%' . $value . '%');
                    }
                    break;
            }
        }
        $questions = $query->paginate($this->ipp);

        $this->data['polls'] = $questions;
        return View::make('polls::index', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create() {
        return View::make('polls::create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store() {

        $data = Input::except('conn');
//        dd($data);
        $validator = Validator::make($data, Poll::$rules);
        if ($validator->fails()) {
            return Redirect::route(ADMIN . '.polls.create')->withErrors($validator)->withInput(Input::all());
        } else {//success
            $slug = Poll::generateSlug($data['poll_title']);
//            dd(date('Y-m-d H:i:s',  strtotime($data['from'])));
            $question = new Poll();
            $question->site = $this->conn;

            $question->poll_slug = $slug;
            $question->poll_status = $data['poll_status'];
            $question->is_multiple = $data['is_multiple'];
            $question->poll_image_id = (isset($data['featured_image'])) ? $data['featured_image'] : '';
            $question->poll_title = $data['poll_title'];
            $question->poll_text = $data['poll_text'];
            if (isset($data['is_always_active'])) {
                $question->is_always_active = $data['is_always_active'];
            } else {
                $question->is_always_active = 0;
                if ($data['from']) {
                    $question->active_from = date('Y-m-d H:i:s', strtotime($data['from']));
                }

                if ($data['to']) {
                    $question->active_to = date('Y-m-d H:i:s', strtotime($data['to']));
                }
            }
            $question->save();
//            $pollLangArray = [
//                'poll_text' => $data['poll_text'],
//                'poll_title' => $data['poll_title'],
//                'lang' => App::getLocale()
//            ];
//            $pollLang = new PollLang($pollLangArray);
//            $pollLang->poll_text = $data['poll_text'];
//            $pollLang->poll_title = $data['poll_title'];
//            $pollLang->lang = App::getLocale();
//            $question->lang()->save($pollLang);
            $answers = $data['answers'];
            $answersArray = [];
            $answersLangArray = [];
            $emptyCounter = 0;
            foreach ($answers as $key => $answer) {
                if (empty($answer)) {
                    $emptyCounter++;
                    continue;
                }
                $current = new Poll();
                $current->site = $this->conn;
                $current->poll_slug = '';
                $current->poll_parent = $question->poll_id;
                $current->poll_image_id = (isset(Input::get('image')[$key + 1])) ? Input::get('image')[$key + 1] : 0;
                $current->poll_title = '';
                $current->poll_text = $answer;
                $answersArray[] = $current;
//                $answersLangArray[$key - $emptyCounter] = [
//                    'poll_text' => $answer,
//                    'poll_title' => '',
//                    'lang' => App::getLocale()
//                ];`
            }
            $question->answers()->saveMany($answersArray);
//            foreach ($question->answers as $key => $storedAnswer) {
//                $answer = $answersLangArray[$key]['poll_text'];
//                $answerslang = new PollLang();
////                $answerslang->setConnection($this->conn);
//                $answerslang->poll_text = $answer;
//                $answerslang->poll_title = '';
//                $answerslang->lang = App::getLocale();
//                $storedAnswer->lang()->save($answerslang);
//            }
            return Redirect::to(route(ADMIN . '.polls.edit', ['slug' => $slug]))->with('flash_message', ['type' => 'success', 'text' => Lang::get('polls::polls.created')]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($slug) {
        
    }

    /**
     * Show the form for editing the specified resource.
     * 
     * @param  int  $id
     * @return Response
     */
    public function edit($slug) {

        $question = Poll::with('answers', 'votes', 'image')->where('poll_slug', '=', $slug)->where('site', '=', $this->conn)->first();
//        dd($question/s);
        if ($question) {
            $chartData = $this->getChartData($question);
            $this->data['chartData'] = $chartData;
            $this->data['poll'] = $question;
            $this->data['nextAnswerNum'] = count($question->answers) + 1;
            return View::make('polls::edit', $this->data);
        } else {
            App::abort(404);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id) {

        $data = Input::all();
        $question = Poll::with('answers', 'votes')->where('poll_id', '=', $id)->first();
        $validator = Validator::make($data, Poll::$rules);
        if ($validator->fails()) {
            
        } else {
            $question->poll_status = $data['poll_status'];
            if (isset($data['is_always_active'])) {
                $question->is_always_active = $data['is_always_active'];
            } else {
                $question->is_always_active = 0;
                if ($data['from']) {
                    $question->active_from = date('Y-m-d H:i:s', strtotime($data['from']));
                }

                if ($data['to']) {
                    $question->active_to = date('Y-m-d H:i:s', strtotime($data['to']));
                }
            }
            $question->is_multiple = $data['is_multiple'];
            if (isset($data['featured_image'])) {
                $question->poll_image_id = $data['featured_image'];
            }
            $question->poll_text = $data['poll_text'];
            $question->poll_title = $data['poll_title'];
            $oldAnswersCount = count($question->answers);
            $newAnswersCount = count($data['answers']);
            if ($oldAnswersCount == $newAnswersCount) {//nothing added nor deleted,but might be edited
                foreach ($data['answers'] as $key => $answer) {
                    $question->answers[$key]->poll_text = $answer;
                    $question->answers[$key]->poll_image_id = (Input::get('image')[$key + 1]) ? Input::get('image')[$key + 1] : 0;
                }
            } elseif ($oldAnswersCount > $newAnswersCount) {//something was deleted
                foreach ($data['deleted'] as $deletedId) {
                    Poll::where('poll_id', '=', $deletedId)->first()->delete();
//                    PollLang::where('poll_id', '=', $deletedId)->first()->delete();//this now is handeled in the model listener for deleting
                }
            } elseif ($oldAnswersCount < $newAnswersCount) {//something was added
                $newAnswersArray = [];
                foreach ($data['answers'] as $key => $answer) {
                    if (isset($question->answers[$key])) {
                        $question->answers[$key]->poll_text = $answer;
                        $question->answers[$key]->poll_image_id = (Input::get('image')[$key + 1]) ? Input::get('image')[$key + 1] : 0;
                    } else {
//                        dd($answer);
                        $current = new Poll([]);
                        $current->poll_text = $answer;
                        $current->poll_image_id = (Input::get('image')[$key + 1]) ? Input::get('image')[$key + 1] : 0;
                        $newAnswersArray[] = $current;

//                        $newAnswersArray[] = new Poll([]);
//                        $current = new PollLang([]);
//                        $current->poll_text = $answer;
//                        //$current->poll_image_id = (Input::get('image')[$key + 1]) ? Input::get('image')[$key + 1] : 0;
//                        $current->lang = App::getLocale();
//                        $newAnswersLangArray[] = $current;
                    }
                }
                $question->answers()->saveMany($newAnswersArray);
//                foreach ($newAnswersArray as $key => $storedAnswers) {
//                    $storedAnswers->lang()->save($newAnswersLangArray[$key]);
//                }
            }
        }
        $question->push();
        return Redirect::route(ADMIN . '.polls.edit', [$question->poll_slug])->with('flash_message', ['type' => 'success', 'text' => Lang::get('polls::polls.updated')]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id) {

        $poll = Poll::whereSite($this->conn)->find($id);
        $poll->delete();
        return Redirect::to(route(ADMIN . '.polls.index'))->with('flash_message', 'Deleted');
    }

    public function newAnswer() {
        $this->data['answerNum'] = Input::get('answer_num');
        return View::make('polls::partials.newAnswer', $this->data);
    }

    public function showLogs($slug) {

        $poll = Poll::with('answers', 'votes')->wherePollSlug($slug)->whereSite($this->conn)->first();
        $this->data['poll'] = $poll;
        return View::make('polls::polls.log', $this->data);
    }

    private function prepQuestionObject($questions, $answers) {
//        dd($questions);
        foreach ($questions as $qKey => $question) {
            $questions[$qKey]['answers'] = [];
            foreach ($answers as $aKey => $answer) {
                if ($answer['poll_parent'] == $question['poll_id']) {
                    $questions[$qKey]['answers'][] = $answer;
                    unset($answers[$aKey]);
                } else {
                    continue;
                }
            }
        }
        return $questions;
    }

    private function getChartData($question) {
        $returnData = [];
        foreach ($question->answers as $answer) {
            $returnData[] = [
                'label' => $answer->poll_text,
                'data' => $answer->total_votes
            ];
        }
        return $returnData;
    }

}
