<?php



/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PollControl
 *
 * @author Khaled PC
 */
class PollVotes extends Model {

    protected $table = 'polls_control';
    public $fillable = array('client_ip');

//    protected $connection;
//
//    function __construct() {
//        $this->setConnection(\Session::get('conn'));
//    }

    public function getPrettyCreatedAtAttribute() {
        return $this->created_at->toDateTimeString();
    }

}
