<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Poll
 *
 * @author Khaled PC
 */
class Poll extends Model {

    protected $table = 'polls';
    protected $primaryKey = 'poll_id';
    protected $fillable = [ 'poll_slug', 'poll_status', 'is_always_active', 'is_multiple', 'poll_image_id'];
    public static $rules = [
        'poll_title' => 'required',
        'poll_text' => 'required',
        'answers' => 'array|min:2'
    ];
    public $appends = array('poll_text', 'poll_title');

//    protected $connection;
//
//    function __construct() {
//        $this->setConnection(Session::get('conn'));
//    }

    /*
     * Accessors
     */

    public function getUrlAttribute() {
        return route(ADMIN . '.polls.edit', [$this->poll_slug]);
    }

//    public function getPollTextAttribute() {
//        return ($this->lang) ? $this->lang->poll_text : '';
//    }
//    public function getPollTitleAttribute() {
//        return ($this->lang) ? $this->lang->poll_title : '';
//    }

    public function getPrettyCreatedAtAttribute() {
        return $this->created_at->toDateTimeString();
    }

    public function getFormatedActiveFromAttribute() {
        return ($this->active_from != '0000-00-00 00:00:00') ? date('d/m/Y', strtotime($this->active_from)) : '';
    }

    public function getFormatedActiveToAttribute() {
        return ($this->active_to != '0000-00-00 00:00:00') ? date('d/m/Y', strtotime($this->active_to)) : '';
    }

    public function scopeNowActive($query) {
        return $query->where('active_from', '>=', date('Y-m-d H:i:s'))
                        ->Where('active_to', '<=', date('Y-m-d H:i:s'))
                        ->orWhere('is_always_active', '=', '1');
    }

    public function scopeNowInactive($query) {
        return $query->where('active_from', '<=', date('Y-m-d H:i:s'))
                        ->Where('active_to', '=<', date('Y-m-d H:i:s'))
                        ->orWhere('is_always_active', '=', '0');
    }

    /*
     * Mutator
     */

//    public function setPollTextAttribute($value) {
//        $this->lang()->poll_text = $value;
//    }
//
//    public function setPollTitleAttribute($value) {
//        $this->lang()->poll_title = $value;
//    }
//
//    public function setPollLangAttribute($value) {
//        $this->lang()->lang = $value;
//    }

    /*
     * Relations
     */

    public function answers() {
        return $this->hasMany('Poll', 'poll_parent', 'poll_id');
    }

    public function question() {
        return $this->belongsTo('Poll', 'poll_parent', 'poll_id');
    }

    public function votes() {
        return $this->hasMany('PollVotes', 'poll_id', 'poll_id');
    }

//    public function lang() {
//
//        return $this->hasOne('PollLang');
//    }

    public function image() {
        return $this->hasOne('Media', 'media_id', 'poll_image_id');
    }

    /*
     * Functions
     */

    public static function generateSlug($string) {
        $slug = str_slug($string);
        $isUnique = self::where('poll_slug', '=', $slug)->first();
        while ($isUnique !== NULL) {
            $slug = $slug . '-' . rand(0, 99999);
            $isUnique = self::where('poll_slug', '=', $slug)->first();
        }
        return $slug;
    }

    public function getImageURL($size = 'full') {
        if ($this->image) {
            $sizeName = ($size == 'full') ? '' : $size . '-';
            return URL::to('/') . '/uploads/' . $sizeName . $this->image->media_path;
        } else {
            return '//:0';
        }
    }

    public static function getActivePools() {
        return self::where('poll_parent', '=', '0')
                        ->where('poll_status', '=', '1')
//                        ->where('is_active', '=', '1')
                        ->orderBy('created_at', 'desc')
                        ->get();
    }

    public function getPercentageAttribute() {
        return ($this->question->total_votes) ? floor(($this->total_votes * 100) / $this->question->total_votes) : 0;
    }

    /*
     * Events binding
     */

    public static function boot() {
        parent::boot();
        Poll::deleting(function($poll) {
            $poll->lang->delete();
            foreach ($poll->answers as $answer) {
//                $votesIds = array();
//                foreach ($answer->votes as $vote) {
//                    $votesIds[] = $vote->id;
//                }
                $answer->votes()->delete();
                $answer->lang->delete();
                $answer->delete();
            }
        });
    }

    public static function getLandingPoll() {
        $block_dir = Config::get("dbDriver");
        $path = getcwd() . DIRECTORY_SEPARATOR . "blocks" . DIRECTORY_SEPARATOR . $block_dir . DIRECTORY_SEPARATOR . "landing_poll.json";
//        $path = 'blocks/1/landing_poll.json';
        if (File::exists($path)) {
            $contents = File::get($path);
            $arr = json_decode($contents);
            $id = isset($arr[0]) ? $arr[0] : 0;
            if (!$id) {
                return FALSE;
            }
            return self::find($id);
        } else {
            return FALSE;
        }
    }

}
