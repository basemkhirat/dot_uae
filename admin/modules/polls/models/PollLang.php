<?php



/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PollLang
 *
 * @author Khaled PC
 */
class PollLang extends Model {

    protected $table = 'polls_lang';
    protected $fillable = ['poll_text', 'poll_title', 'lang'];
    public $timestamps = FALSE;

//    protected $connection;
//
//    function __construct(){
//    	$this->setConnection(\Session::get('conn'));
//    }

    public function poll() {
        return $this->belongsTo('Poll');
    }

}
