@extends("admin::layouts.master")

@section("breadcrumb")
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>{!!trans('polls::polls.polls')!!} </h2>
        <ol class="breadcrumb" style="background: none;">
            <li><a  href="{!!URL::to('/'.ADMIN.'/dashboard')!!}">{!!Lang::get('admin::common.dashboard')!!}</a></li>
            <li><a  href="{!!URL::to('/'.ADMIN.'/polls')!!}">{!!Lang::get('polls::polls.polls')!!}</a></li>
            <li>{!!trans('polls::polls.log_for')!!}{!!$poll->poll_title!!} </li>
        </ol>
    </div>
    <div class="col-lg-2">
        <a class="btn btn-primary btn-labeled btn-main pull-right" href="{!!route(ADMIN . '.polls.create')!!}"> <span class="btn-label icon fa fa-plus"></span> {!!Lang::get('polls::polls.add_new')!!}</a>
    </div>
</div>
@stop


@section("content")
<div id="content-wrapper">

    
    <div class="row" style="margin-top: 10px;">
        <div class="col-sm-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5> {!!trans('polls::polls.polls')!!} </h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>

                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <div class="table-responsive">
                        <table cellspacing="0" cellpadding="0" border="0" class="table table-striped">
                            <thead>
                                <tr>
                                    <th>{!!trans('polls::polls.answers')!!}</th>
                                    <th>{!!trans('polls::polls.client_ip')!!}</th>
                                    <th>{!!trans('polls::polls.date')!!}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($poll->answers as $answer)
                                @foreach($answer->votes as $vote)
                                <tr>
                                    <td><a href="">{!!$answer->poll_text!!}</a></td>
                                    <td><a href="">{!!$vote->client_ip!!}</a></td>
                                    <td><a href="">{!!$vote->pretty_created_at!!}</a></td>
                                </tr>
                                @endforeach
                                @endforeach
                            </tbody>

                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @stop