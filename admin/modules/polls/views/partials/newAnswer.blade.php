<div class="form-group">
    <label for="answer{!!$answerNum!!}" class="col-sm-2 control-label">{!!trans('polls::polls.answer')!!} {!!$answerNum!!}</label>
    <div class="col-sm-8">
        <input name="answers[]" type="text" class="form-control" id="answer{!!$answerNum!!}" placeholder="{!!trans('polls::polls.answer')!!} {!!$answerNum!!}">
    </div>
    <div class="col-sm-2">
        <img src="{!!assets('images/default.png')!!}" style="width: 90px;height: 90px;" id="image_preview_{!!$answerNum!!}" />
        <a href="#" class="set-answer-thumbnail" data-id='{!!$answerNum!!}'>{!!Lang::get('polls::polls.set_image')!!}</a>
    </div>
    <input type="hidden" name="image[{!!$answerNum!!}]" id="image_holder_{!!$answerNum!!}" />
</div>