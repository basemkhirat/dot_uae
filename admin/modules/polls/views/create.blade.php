@extends("admin::layouts.master")

@section("breadcrumb")
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>{!!trans('polls::polls.polls')!!} </h2>
        <ol class="breadcrumb" style="background: none;">
            <li><a  href="{!!URL::to('/'.ADMIN.'/dashboard')!!}">{!!Lang::get('admin::common.dashboard')!!}</a></li>
            <li><a  href="{!!URL::to('/'.ADMIN.'/polls')!!}">{!!Lang::get('polls::polls.polls')!!}</a></li>
            <li>{!!trans('polls::polls.add_new')!!} </li>
        </ol>
    </div>

</div>
@stop

<link href="<?php echo assets() ?>/css/plugins/datapicker/datepicker3.css" rel="stylesheet">

@section("content")
<div id="content-wrapper">

    <div class="row">
        {!! Form::open([
        'route' => [ADMIN . '.polls.store'],
        'method' => 'POST',
        'class' => 'form-horizontal',
        'id' => 'create_form'
        ]) !!}
        <div class="col-sm-8">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5> {!!Lang::get('polls::polls.new_poll')!!} </h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>

                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content" id="polls">
                    <div class="form-group {!!$errors->has('poll_title') ? 'has-error has-feedback' : ''!!}">
                        <label for="pTitle" class="col-sm-2 control-label">{!!trans('polls::polls.title')!!}</label>
                        <div class="col-sm-10">
                            <input name="poll_title" type="text" class="form-control" id="pTitle" placeholder="{!!trans('polls::polls.title')!!}" @if(Input::old('poll_title')) value="{!!Input::old('poll_title')!!}" @endif>
                                   @if($errors->has('poll_title'))<span class="fa fa-times-circle form-control-feedback"></span>@endif
                        </div>
                    </div> <!-- / .form-group -->
                    <div class="form-group {!!$errors->has('poll_text') ? 'has-error has-feedback' : ''!!}">
                        <label for="question-text" class="col-sm-2 control-label">{!!trans('polls::polls.question')!!}</label>
                        <div class="col-sm-10">
                            <textarea name="poll_text" class="form-control" id="question-text" placeholder="{!!trans('polls::polls.question')!!}">@if(Input::old('poll_text')) {!!Input::old('poll_text')!!} @endif</textarea>
                            @if($errors->has('poll_text'))<span class="fa fa-times-circle form-control-feedback"></span>@endif
                        </div>
                    </div> <!-- / .form-group -->
                    <div class="form-group {!!$errors->has('answers') ? 'has-error has-feedback' : ''!!}">
                        <label for="answer1" class="col-sm-2 control-label">{!!trans('polls::polls.answer')!!} 1</label>
                        <div class="col-sm-8">
                            <input name="answers[]" type="text" class="form-control" id="answer1" placeholder="{!!trans('polls::polls.answer')!!} 1" @if(Input::old('answers')[0]) value="{!!Input::old('answers')[0]!!}" @endif>
                                   @if($errors->has('answers'))<span class="fa fa-times-circle form-control-feedback"></span>@endif
                        </div>
                        <div class="col-sm-2">
                            <img src="{!!assets("images/default.png")!!}" style="width: 90px;height: 90px;" id="image_preview_1" />
                                 <a href="#" class="set-answer-thumbnail" data-id='1'>{!!Lang::get('polls::polls.set_image')!!}</a>
                        </div>
                        <input type="hidden" name="image[1]" id="image_holder_1" />
                    </div> <!-- / .form-group -->
                    <div class="form-group {!!$errors->has('answers') ? 'has-error has-feedback' : ''!!}">
                        <label for="answer2" class="col-sm-2 control-label">{!!trans('polls::polls.answer')!!} 2</label>
                        <div class="col-sm-8">
                            <input name="answers[]" type="text" class="form-control" id="answer2" placeholder="{!!trans('polls::polls.answer')!!} 2" @if(Input::old('answers')[1]) value="{!!Input::old('answers')[1]!!}" @endif>
                                   @if($errors->has('answers'))<span class="fa fa-times-circle form-control-feedback"></span>@endif
                        </div>
                        <div class="col-sm-2">
                            <img src="{!!assets("images/default.png")!!}" style="width: 90px;height: 90px;" id="image_preview_2" />
                                 <a href="#" class="set-answer-thumbnail" data-id='2'>{!!Lang::get('polls::polls.set_image')!!}</a>
                        </div>
                        <input type="hidden" name="image[2]" id="image_holder_2" />
                    </div> <!-- / .form-group -->

                    <div id="new-answer-container"></div>

                    <div class="form-group panel-footer text-right" style="border-top: 1px solid #ececec; background: none;">

                        <div class="col-sm-offset-2 col-sm-10">
                            <button type="submit" class="btn btn-primary">{!!trans('polls::polls.post')!!}</button>
                            &nbsp;&nbsp;
                            <a href="#" id="add-answer" data-answer_num='3' class="btn btn-primary"><span class="btn-label icon fa fa-plus"></span>&nbsp;{!!trans('polls::polls.add_answer')!!}</a>
                        </div>
                    </div> <!-- / .form-group -->
                </div>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5> {!!Lang::get('polls::polls.status')!!} </h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>

                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <select name="poll_status" class="form-control">
                        <option value="1">{!!trans('polls::polls.published')!!}</option>
                        <option value="2">{!!trans('polls::polls.draft')!!}</option>
                        <option value="3">{!!trans('polls::polls.pending')!!}</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="col-sm-4 pull-right">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5> {!!Lang::get('polls::polls.is_active')!!} </h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>

                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">

                    <div class="input-group" id="data_5" style="display:inline-block">
                        <div class="input-daterange input-group" id="datepicker" >
                            <input type="text" class="input-sm form-control" name="from" placeholder="{!!Lang::get('posts::posts.start')!!}" value="<?php echo Input::get("from"); ?>"/>
                            <span class="input-group-addon">{!!Lang::get('posts::posts.to')!!}</span>
                            <input type="text" class="input-sm form-control" name="to" value="<?php echo Input::get("to"); ?>" placeholder="{!!Lang::get('posts::posts.end')!!}"/>
                        </div>

                    </div>

                    <div class="form-group">
                        <label class="checkbox-inline">
                            <input id="is_always_active" type="checkbox" name="is_always_active" value="1" class="i-checks"> <span class="lbl">مفعل دائما</span>
                        </label>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-4 pull-right">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5> {!!Lang::get('polls::polls.is_multiple')!!} </h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>

                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <select name="is_multiple" class="form-control">
                        <option value="1">{!!Lang::get('polls::polls.yes')!!}</option>
                        <option value="0" selected>{!!Lang::get('polls::polls.no')!!}</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="col-sm-4 pull-right">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5> {!!Lang::get('polls::polls.featured_image')!!} </h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>

                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content" id="image-container">

                    <a href="#" class="set-post-thumbnail">{!!Lang::get('polls::polls.set_image')!!}</a>

                </div>
            </div>
        </div>
        </form>
    </div>
</div> <!-- / #content-wrapper -->

@section('footer')
<script src="{!!assets()!!}/javascripts/polls.js"></script>
<script src="<?php echo assets() ?>/js/plugins/datapicker/bootstrap-datepicker.js"></script>
<script src="<?php echo assets() ?>/js/plugins/validate/jquery.validate.min.js"></script>


<script>
    var baseURL = '{!! URL::to("/".ADMIN) !!}/';
    var AMAZON_URL = "{!!AMAZON_URL!!}";
    $(function () {
        $.validator.prototype.checkForm = function () {
            //overriden in a specific page
            this.prepareForm();
            for (var i = 0, elements = (this.currentElements = this.elements()); elements[i]; i++) {
                if (this.findByName(elements[i].name).length != undefined && this.findByName(elements[i].name).length > 1) {
                    for (var cnt = 0; cnt < this.findByName(elements[i].name).length; cnt++) {
                        this.check(this.findByName(elements[i].name)[cnt]);
                    }
                } else {
                    this.check(elements[i]);
                }
            }
            return this.valid();
        }

        $(".set-post-thumbnail").filemanager({
            types: "png|jpg|jpeg|gif|bmp",
            done: function (files) {
                console.log('ssd');
                if (files.length) {
                    var formContainer = $('#create_form');
                    var imageContainer = $('#image-container');
                    var file = files[0];
                    imageContainer.find($('a.featured-preview')).remove();
                    $('#featured_image_id').remove();
                    formContainer.append($('<input>', {
                        'type': 'hidden',
                        'name': 'featured_image',
                        'id': 'featured_image_id',
                        'value': file.media_id,
                    }));
                    imageContainer.prepend($('<a>', {
                        'href': '#',
                        'class': 'featured-preview set-post-thumbnail',
                        'style': 'display:block',
                        
                    }).append($('<img>', {
                        'src': AMAZON_URL+file.media_small_path,
                        'width': '100%',
                        'height': '200px'
                    })));
                }
            },
            error: function (media_path) {
                alert(media_path + " <?php echo trans("polls::users.is_not_an_image") ?>");
            }
        });

        $("#polls .set-answer-thumbnail").each(function () {
            $(this).filemanager({
                types: "png|jpg|jpeg|gif|bmp",
                done: function (files, intiator) {
                    if (files.length) {
                        var id = intiator.attr('data-id');
                        var file = files[0];
                        $('#image_holder_' + id).val(file.media_id);
                        $('#image_preview_' + id).attr('src', file.media_thumbnail);

                    }
                },
                error: function (media_path) {
                    alert(media_path + " <?php echo trans("polls::users.is_not_an_image") ?>");
                }
            });
        });

        $('#is_always_active').on('ifChanged', function (event) {
            var self = $(this);
            $('.input-sm').prop('disabled', self.is(':checked'));
        });

        $('#data_5 .input-daterange').datepicker({
            keyboardNavigation: false,
            forceParse: false,
            autoclose: true,
            language: 'ar', //as you defined in bootstrap-datepicker.XX.js
            isRTL: true
        });

        $('.i-checks').iCheck({
            checkboxClass: 'icheckbox_square-green',
        });

        $("#create_form").validate({
            ignore: [],
            rules: {
                poll_title: {
                    required: true
                },
                poll_text: {
                    required: true
                },
                "answers[]": {
                    required: true,
                }
            }
        });


        jQuery.extend(jQuery.validator.messages, {
            required: "{!!Lang::get('pages::pages.required')!!}",
        });
    });


</script>
@stop
@stop