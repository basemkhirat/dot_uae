@extends("admin::layouts.master")

@section("breadcrumb")
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2><i class="fa fa-bar-chart faa-tada animated faa-slow"></i> {!!trans('polls::polls.polls')!!} </h2>
        <ol class="breadcrumb" style="background: none;">
            <li><a  href="{!!URL::to('/'.ADMIN.'/dashboard')!!}">{!!Lang::get('admin::common.dashboard')!!}</a></li>
            <li><a  href="{!!URL::to('/'.ADMIN.'/polls')!!}">{!!Lang::get('polls::polls.polls')!!}</a></li>
            <li>{!!trans('polls::polls.edit')!!} </li>
        </ol>
    </div>
    <div class="col-lg-2">
        <a class="btn btn-primary btn-labeled btn-main pull-right" href="{!!route(ADMIN . '.polls.create')!!}"> <span class="btn-label icon fa fa-plus"></span> {!!Lang::get('polls::polls.add_new')!!}</a>
    </div>
</div>
@stop

<link href="<?php echo assets() ?>/css/plugins/datapicker/datepicker3.css" rel="stylesheet">

<?php
$total = 0;
foreach ($chartData as $key => $value) {
    $total += $value['data'];
}
?>

@section("content")
<div id="content-wrapper">
    @if(Session::get('flash_message'))
    <div id="pa-page-alerts-box">
        <div class="alert alert-page pa_page_alerts_default alert-{!!Session::get('flash_message')['type']!!}" data-animate="true" style="">
            <button type="button" class="close"><i class="fa fa-times"></i></button>
            {!!Session::get('flash_message')['text']!!}
        </div>
    </div>
    @endif
    <div class="row">
        {!! Form::open([
        'route' => [ADMIN . '.polls.update',$poll->poll_id],
        'method' => 'PUT',
        'class' => 'form-horizontal',
        'id' => 'edit_form'
        ]) !!}
        <div class="col-sm-8">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5> {!!Lang::get('polls::polls.edit')!!} </h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>

                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content" id="polls">
                    <div class="form-group {!!$errors->has('poll_title') ? 'has-error has-feedback' : ''!!}">
                        <label for="pTitle" class="col-sm-2 control-label">{!!trans('polls::polls.title')!!}</label>
                        <div class="col-sm-10">
                            <input name="poll_title" type="text" class="form-control" id="pTitle" placeholder="{!!trans('polls::polls.title')!!}"  value="{!!$poll->poll_title!!}">
                            @if($errors->has('poll_title'))<span class="fa fa-times-circle form-control-feedback"></span>@endif
                        </div>
                    </div> <!-- / .form-group -->
                    <div class="form-group {!!$errors->has('poll_text') ? 'has-error has-feedback' : ''!!}">
                        <label for="question-text" class="col-sm-2 control-label">{!!trans('polls::polls.question')!!}</label>
                        <div class="col-sm-10">
                            <textarea name="poll_text" class="form-control" id="question-text" placeholder="{!!trans('polls::polls.question')!!}">{!!$poll->poll_text!!}</textarea>
                            @if($errors->has('poll_text'))<span class="fa fa-times-circle form-control-feedback"></span>@endif
                        </div>
                    </div> <!-- / .form-group -->
                    @foreach($poll->answers as $key => $answer)
                    <div class="form-group {!!$errors->has('answers') ? 'has-error has-feedback' : ''!!}" id="answer_{!!$answer->poll_id!!}">
                        <label for="answer{!!$key + 1!!}" class="col-sm-2 control-label">{!!trans('polls::polls.answer')!!} {!!$key + 1!!}</label>
                        <div class="col-sm-6">
                            <input name="answers[]" type="text" class="form-control" id="answer{!!$key + 1!!}" placeholder="{!!trans('polls::polls.answer')!!} {!!$key + 1!!}"  value="{!!$answer->poll_text!!}" >
                            @if($errors->has('answers'))<span class="fa fa-times-circle form-control-feedback"></span>@endif
                        </div>
                        <div class="col-sm-1">
                            <!--<input name="votes[]" type="text" class="form-control" id="votes{!!$key + 1!!}" value="{!!$answer->total_votes!!}" disabled>-->
                            <p name="votes[]" class="form-control" id="votes{!!$key + 1!!}">{!!$answer->total_votes!!}</p>
                        </div>
                        <div class="col-sm-1 text-left" style="padding: 0">
                            <a href="#" class="remove-answer" data-answer-id='{!!$answer->poll_id!!}'><i class="fa fa-times"></i></a>
                        </div>
                        <div class="col-sm-2">
                            <img src="@if(!thumbnail(@$answer->image->media_path)) {!!assets("images/default.png")!!} @else {!!thumbnail(@$answer->image->media_path)!!} @endif" style="width: 90px;height: 90px;" id="image_preview_{!!$key + 1!!}" />
                                 <a href="#" class="set-answer-thumbnail" data-id='{!!$key + 1!!}'>{!!Lang::get('polls::polls.set_image')!!}</a>
                        </div>
                        <input type="hidden" name="image[{!!$key + 1!!}]" value="{!!$answer->poll_image_id!!}" id="image_holder_{!!$key + 1!!}" />
                    </div> <!-- / .form-group -->
                    @endforeach
                    <div id="new-answer-container"></div>
                    <div class="form-group panel-footer text-right" style="border-top: 1px solid #ececec; background: none;">

                        <div class="col-sm-offset-2 col-sm-10">
                            <button type="submit" class="btn btn-primary">{!!trans('polls::polls.save')!!}</button>
                            &nbsp;&nbsp;
                            <a href="#" id="add-answer" data-answer_num='{!!$nextAnswerNum!!}' class="btn btn-primary"><span class="btn-label icon fa fa-plus"></span>&nbsp;{!!trans('polls::polls.add_answer')!!}</a>
                        </div>
                    </div> <!-- / .form-group -->
                </div>
            </div>
        </div>

        <div class="col-sm-4">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5> {!!Lang::get('polls::polls.status')!!} </h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>

                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <select name="poll_status" class="form-control">
                        <option value="1" {!!($poll->poll_status == 1) ? 'selected' : ''!!}>{!!trans('polls::polls.published')!!}</option>
                        <option value="2" {!!($poll->poll_status == 2) ? 'selected' : ''!!}>{!!trans('polls::polls.draft')!!}</option>
                        <option value="3" {!!($poll->poll_status == 3) ? 'selected' : ''!!}>{!!trans('polls::polls.pending')!!}</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="col-sm-4 pull-right">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5> {!!Lang::get('polls::polls.is_active')!!} </h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>

                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">

                    <div class="input-group" id="data_5" style="display:inline-block">
                        <div class="input-daterange input-group" id="datepicker" >
                            <input type="text" class="input-sm form-control" name="from" placeholder="{!!Lang::get('posts::posts.start')!!}" value="{!! $poll->formated_active_from !!}" {!!($poll->is_always_active) ? 'disabled' : ''!!}/>
                            <span class="input-group-addon">{!!Lang::get('posts::posts.to')!!}</span>
                            <input type="text" class="input-sm form-control" name="to" placeholder="{!!Lang::get('posts::posts.end')!!}" value="{!! $poll->formated_active_to !!}" {!!($poll->is_always_active) ? 'disabled' : ''!!}/>
                        </div>

                    </div>

                    <div class="form-group">
                        <label class="checkbox-inline">
                            <input id="is_always_active" type="checkbox" {!!($poll->is_always_active) ? 'checked' : ''!!} name="is_always_active" value="1" class="i-checks"> <span class="lbl">مفعل دائما</span>
                        </label>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-4 pull-right">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5> {!!Lang::get('polls::polls.is_multiple')!!} </h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>

                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <select name="is_multiple" class="form-control">
                        <option value="1" {!!($poll->is_multiple == 1) ? 'selected' : ''!!}>{!!trans('polls::polls.yes')!!}</option>
                        <option value="0" {!!($poll->is_multiple == 0) ? 'selected' : ''!!}>{!!trans('polls::polls.no')!!}</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="col-sm-4 pull-right">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5> {!!Lang::get('polls::polls.featured_image')!!} </h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>

                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content" id="image-container">
                    @if($poll->image)
                    <a href="#" class="featured-preview set-post-thumbnail">
                        <img width="100%" height="200px" src="{!!thumbnail(@$poll->image->media_path, 'small')!!}">
                    </a>
                    <div style='margin-top: 8px;'>
                        <a href="#" class="set-post-thumbnail">{!!Lang::get('polls::polls.change_image')!!}</a>
                    </div>
                    <div>
                        <a href="#" class="remove-post-thumbnail">{!!Lang::get('polls::polls.remove_image')!!}</a>
                    </div>
                    @else
                    <a href="#" class="set-post-thumbnail">{!!Lang::get('polls::polls.set_image')!!}</a>
                    @endif

                </div>
            </div>
        </div>
        </form>

        <div class="col-sm-4 pull-right" style='margin-top: 12px;'>
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5> {!!Lang::get('polls::polls.chart')!!} </h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>

                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    @if($total != 0)
                    <div class="flot-chart">
                        <div class="flot-chart-pie-content" id="flot-pie-chart" style="width: inherit; height: inherit"></div>
                    </div>
                    @else
                    {!!trans('polls::polls.no_votes')!!}
                    @endif
                </div>
            </div>
        </div>
        <div class="col-sm-4" style='margin-top: 12px;'>
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5> {!!Lang::get('polls::polls.operations')!!} </h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>

                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    {!!Form::open([
                    'route' => array(ADMIN . '.polls.destroy',$poll->poll_id),
                    'method' => 'DELETE',
                    'class' => '',
                    'id' => 'delete-form'
                    ])!!}
                    <button id="delete-poll" class="btn btn-primary" message="sd" type="submit">{!!trans('polls::polls.delete')!!}</button>
                    {!!Form::close()!!}

                    <a href="{!!route('polls.log',array('slug'=>$poll->poll_slug))!!}">{!!trans('polls::polls.show_answers_log')!!}</a>
                </div>
            </div>
        </div>

    </div>
</div>

@section('footer')
<script src="{!!assets()!!}/javascripts/polls.js"></script>
<script src="<?php echo assets() ?>/js/plugins/datapicker/bootstrap-datepicker.js"></script>
<script src="<?php echo assets() ?>/js/plugins/validate/jquery.validate.min.js"></script>

<script src="<?php echo assets() ?>/js/plugins/flot/jquery.flot.js"></script>
<script src="<?php echo assets() ?>/js/plugins/flot/jquery.flot.tooltip.min.js"></script>
<script src="<?php echo assets() ?>/js/plugins/flot/jquery.flot.time.js"></script>
<script src="<?php echo assets() ?>/js/plugins/flot/jquery.flot.pie.js"></script>

<script src="<?php echo assets() ?>/js/bootbox.min.js"></script>

<script>
    var baseURL = '{!! URL::to("/".ADMIN) !!}/';
    var AMAZON_URL = "{!!AMAZON_URL!!}";
    @if ($total != 0)
//Flot Pie Chart
            $(function () {

                var data = <?php echo json_encode($chartData) ?>;

                var plotObj = $.plot($("#flot-pie-chart"), data, {
                    series: {
                        pie: {
                            show: true,
                            innerRadius: 0.5,
                        }
                    },
                    grid: {
                        hoverable: true,
                        clickable: true,
                        borderWidth: 0
                    },
                    tooltip: true,
                    tooltipOpts: {
                        content: "%p.0%, %s", // show percentages, rounding to 2 decimal places
                        shifts: {
                            x: 20,
                            y: 0
                        },
                        defaultTheme: false
                    },
                    legend: {
                        show: false
                    }
                });

            });
    @endif

            $(function () {
                $.validator.prototype.checkForm = function () {
                    //overriden in a specific page
                    this.prepareForm();
                    for (var i = 0, elements = (this.currentElements = this.elements()); elements[i]; i++) {
                        if (this.findByName(elements[i].name).length != undefined && this.findByName(elements[i].name).length > 1) {
                            for (var cnt = 0; cnt < this.findByName(elements[i].name).length; cnt++) {
                                this.check(this.findByName(elements[i].name)[cnt]);
                            }
                        } else {
                            this.check(elements[i]);
                        }
                    }
                    return this.valid();
                }

                $(".set-post-thumbnail").filemanager({
                    types: "png|jpg|jpeg|gif|bmp",
                    done: function (files) {
                        console.log('ssd');
                        if (files.length) {
                            var formContainer = $('#edit_form');
                            var imageContainer = $('#image-container');
                            var file = files[0];
                            imageContainer.find($('a.featured-preview')).remove();
                            $('#featured_image_id').remove();
                            formContainer.append($('<input>', {
                                'type': 'hidden',
                                'name': 'featured_image',
                                'id': 'featured_image_id',
                                'value': file.media_id,
                            }));
                            imageContainer.prepend($('<a>', {
                                'href': '#',
                                'class': 'featured-preview set-post-thumbnail',
                                'style': 'display:block'
                                
                            }).append($('<img>', {
                                'src': AMAZON_URL+file.media_small_path,
                                'width': '100%',
                                'height': '200px'
                            })));
                        }
                    },
                    error: function (media_path) {
                        alert(media_path + " <?php echo trans("polls::users.is_not_an_image") ?>");
                    }
                });
                $('.remove-post-thumbnail').click(function (event) {
                    event.preventDefault();
                    var formContainer = $('#edit-form');
                    var imageContainer = $('#image-container');
                    imageContainer.find($('a.featured-preview')).remove();
                    if ($('#featured_image_id').length) {
                        $('#featured_image_id').val('0');
                    } else {
                        formContainer.append($('<input>', {
                            'type': 'hidden',
                            'name': 'featured_image',
                            'id': 'featured_image_id',
                            'value': 0
                        }));
                    }
                    $(this).hide();
                });
                $("#polls .set-answer-thumbnail").each(function () {
                    $(this).filemanager({
                        types: "png|jpg|jpeg|gif|bmp",
                        done: function (files, intiator) {
                            if (files.length) {
                                var id = intiator.attr('data-id');
                                var file = files[0];
                                $('#image_holder_' + id).val(file.media_id);
                                $('#image_preview_' + id).attr('src', file.media_thumbnail);

                            }
                        },
                        error: function (media_path) {
                            alert(media_path + " <?php echo trans("polls::users.is_not_an_image") ?>");
                        }
                    });
                });

                $('#is_always_active').on('ifChanged', function (event) {
                    var self = $(this);
                    $('.input-sm').prop('disabled', self.is(':checked'));
                });

                $('#data_5 .input-daterange').datepicker({
                    keyboardNavigation: false,
                    forceParse: false,
                    autoclose: true,
                    language: 'ar', //as you defined in bootstrap-datepicker.XX.js
                    isRTL: true
                });

                $('.i-checks').iCheck({
                    checkboxClass: 'icheckbox_square-green',
                });

                $("#edit_form").validate({
                    ignore: [],
                    rules: {
                        poll_title: {
                            required: true
                        },
                        poll_text: {
                            required: true
                        },
                        "answers[]": {
                            required: true,
                        }
                    }
                });


                jQuery.extend(jQuery.validator.messages, {
                    required: "{!!Lang::get('pages::pages.required')!!}",
                });

                $('#delete-poll').on('click', function (e) {
                    e.preventDefault();
                    $this = $(this);
                    var form = $('#delete-form');
                    bootbox.dialog({
                        message: "{!!Lang::get('polls::polls.delete_confirm')!!}",
                        title: "{!!Lang::get('polls::polls.delete_poll')!!}",
                        buttons: {
                            success: {
                                label: "{!!Lang::get('polls::polls.ok')!!}",
                                className: "btn-success",
                                callback: function () {
                                    form.submit();
                                }
                            },
                            danger: {
                                label: "{!!Lang::get('polls::polls.cancel')!!}",
                                className: "btn-primary",
                                callback: function () {
                                }
                            },
                        },
                        className: "bootbox-sm"
                    });
                });
            });


</script>
@stop
@stop