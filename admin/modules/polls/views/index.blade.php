@extends("admin::layouts.master")

@section("breadcrumb")
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2><i class="fa fa-bar-chart faa-tada animated faa-slow"></i> {!!trans('polls::polls.polls')!!} ({!!@$polls->total()!!})</h2>
        <ol class="breadcrumb" style="background: none;">
            <li><a  href="{!!URL::to('/'.ADMIN.'/dashboard')!!}">{!!Lang::get('admin::common.dashboard')!!}</a></li>
            <li>{!!trans('polls::polls.polls')!!} </li>
        </ol>
    </div>
    <div class="col-lg-2">
        <a href="{!!route(ADMIN . '.polls.create')!!}" class="btn btn-primary btn-labeled btn-main pull-right"><span class="btn-label icon fa fa-plus"></span> {!!trans('polls::polls.add_new')!!}</a>
    </div>
</div>
@stop
@section("content")

<div id="content-wrapper">



    <div class="row">
        <div class="col-lg-8">
            <form>
                <select name="poll_status" class="form-control" style="width:auto; display: inline-block">
                    <option value="-1">{!!trans('polls::polls.by_status')!!}</option>
                    <option value="1" {!!Input::get('poll_status') == '1' ? 'selected' : ''!!}>{!!trans('polls::polls.published')!!}</option>
                    <option value="2" {!!Input::get('poll_status') == '2' ? 'selected' : ''!!}>{!!trans('polls::polls.draft')!!}</option>
                    <option value="3" {!!Input::get('poll_status') == '3' ? 'selected' : ''!!}>{!!trans('polls::polls.pending')!!}</option>
                </select>
                <select name="is_active" class="form-control" style="width:auto; display: inline-block;">
                    <option value="-1">{!!trans('polls::polls.by_availability')!!}</option>
                    <option value="1" {!!Input::get('is_active') == '1' ? 'selected' : ''!!}>{!!trans('polls::polls.yes')!!}</option>
                    <option value="0" {!!Input::get('is_active') == '0' ? 'selected' : ''!!}>{!!trans('polls::polls.no')!!}</option>
                </select>
                <button class="btn btn-primary" type="submit">{!!trans('polls::polls.filter')!!}</button>
            </form>
        </div>
        <div class="col-lg-4">
            {!! Form::open(array('method' => 'get', 'class' => 'choosen-rtl', 'id' => 'search')) !!}

            <div class="input-group">
                <input type="text" placeholder="{!!Lang::get('pages::pages.search')!!}" class="input-sm form-control" value="{!!(Input::get('search')) ? Input::get('search') : ''!!}" name="search" id="q"> <span class="input-group-btn">
                    <button type="submit" class="btn btn-sm btn-primary"> {!!Lang::get('pages::pages.search')!!}</button> </span>
            </div>

            {!! Form::close() !!}
        </div>
    </div>
    <div class="row" style="margin-top: 10px;">
        <div class="col-sm-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5> {!!trans('polls::polls.polls')!!} </h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>

                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <div class="table-responsive">
                        <table cellspacing="0" cellpadding="0" border="0" class="table table-striped">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>{!!trans('polls::polls.question')!!}</th>
                                    <th>{!!trans('polls::polls.answers')!!} #</th>
                                    <th>{!!trans('polls::polls.total_votes')!!}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($polls as $poll)

                                <tr>
                                    <td><a href="{!!$poll->url!!}">{!!$poll->poll_id!!}</a></td>
                                    <td><a href="{!!$poll->url!!}">{!!$poll->poll_text!!}</a></td>
                                    <td><a href="{!!$poll->url!!}">{!!count($poll->answers)!!}</a></td>
                                    <td><a href="{!!$poll->url!!}">{!!$poll->total_votes!!}</a></td>
                                </tr>
                                @endforeach

                            </tbody>

                        </table>
                        <div class="panel-footer text-right" style="border-top: 1px solid #ececec; background: none;">

                            <div class="col-sm-10 m-b-xs" style="padding:0">
                                <div class="pull-right">
                                    {!!$polls->appends(Input::all())->setPath('')->render()!!}
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        // search submit
        $("#search").submit(function (e) {
            if ($("#q").val() == '') {
                return false
            }
        });
    });
</script>
@stop
