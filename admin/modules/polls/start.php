<?php

Menu::set("sidebar", function($menu) {
        $menu->item('polls', trans("admin::common.polls"), URL::to(ADMIN . '/polls'))->icon("fa-bar-chart")->order(7);
});
            
include __DIR__ ."/routes.php";
