<?php

Route::group(array(
    "prefix" => ADMIN,
    "before" => "auth",
        ), function($route) {
    Route::resource('polls', 'PollsController', ['except' => ['show']]);
    $route->group(array("prefix" => "polls"), function($route) {
        $route->get('{slug}/log', ['as' => 'polls.log', 'uses' => 'PollsController@showLogs']);
        $route->get('newAnswer', 'PollsController@newAnswer');
    });
});
