<?php

Menu::set("sidebar", function($menu) {
    if (User::can('authors')) {
        $menu->item('users.authors', trans("admin::common.authors"), route("authors.show"));        
    }
});

include __DIR__ ."/routes.php";
