<?php

Route::group(array(
    "prefix" => ADMIN,
    "before" => "auth",
        ), function($route) {
    $route->group(array("prefix" => "authors"), function($route) {
        $route->any('/', array("as" => "authors.show", "uses" => "AuthorsController@index"));
        $route->any('/create', array("as" => "authors.create", "uses" => "AuthorsController@create"));
        $route->any('/{id}/edit', array("as" => "authors.edit", "uses" => "AuthorsController@edit"));
        $route->any('/delete', array("as" => "authors.delete", "uses" => "AuthorsController@delete"));
    });
});
