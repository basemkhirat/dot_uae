<?php

return [
    "authors" => "Authors",
    "add_new" => "Add new",
    "edit" => "Edit author",
    "search_authors" => "search authors ..",
    "page" => "Page",
    "search" => "search",
    "per_page" => "Per page",
    "bulk_actions" => "Bulk actions",
    "delete" => "Delete",
    "apply" => "Apply",
    "photo" => "Photo",
    "name" => "Name",
    "created" => "Registeration date",
    "articles" => "Articles",
    "actions" => "Actions",
    "sure_delete" => "You are about to delete author. continue?", 
    "add_new_user" => "Add new author",
    "first_name" => "Author name",
    "about_me" => "About author ..",
    "is_not_an_image" => "is not an image",
    "role" => "role",
    "activation" => "Activation",
    "save_user" => "Save author",
    "of" => "of",
    "activated" => "Activated",
    "deactivated" => "Deactivated",
    "author_created" => "Author created successfully",
    "author_updated" => "Author updated successfully",
    "author_deleted" => "Author deleted successfully",
    
    
    "change_small_photo" => "Change small photo",
    "change_large_photo" => "Change Large photo",
    "facebook" => "Facebook",
    "twitter" => "Twitter",
    "googleplus" => "Google plus",
    "linkedin" => "Linked in",
    "" => "",
    "" => "",
    "" => "",
    "" => "",
    "" => "",
    "" => "",
    "" => "",
    "" => "",
    "" => "",
    "" => "",
    "" => "",
    "" => "",
    "" => "",
    
    
    

    "module" => "Authors",    
    "permissions" => [
        "create" => "Create authors",
        "edit" => "Edit authors",
        "delete" => "Delete authors"
    ]
];
