<?php

return [
    "authors" => "الكتاب",
    "add_new" => "إضافة جديد",
    "edit" => "تحرير مؤلف",
    "search_authors" => "بحث في المؤلفين ...",
    "per_page" => "لكل صفحة",
    "search" => "بحث",
    "bulk_actions" => "أختر الحدث",
    "delete" => "حذف",
    "apply" => "تطبيق",
    "photo" => "الصورة",
    "name" => "إسم",
    "created" => "تاريخ الإضافة",
    "articles" => "المقالات",
    "role" => "الدور",
    "actions" => "الأوامر",
    "sure_delete" => "أنت على وشك حذف مؤلف، استمرار",
    "page" => "الصفحة",
    "add_new_user" => "إضافة مؤلف",
    "confirm_password" => "تأكيد كلمة السر",
    "change" => "تغيير",
    "first_name" => "الإسم",
    "last_name" => "اللقب",
    "about_me" => "عن المؤلف ..",
    "is_not_an_image" => "ليست صورة",
    "role" => "الصلاحية",
    "activation" => "التفعيل",
    "language" => "اللغة",
    "special_permissions" => "صلاحيات خاصة",
    "save_user" => "حفظ المؤلف",
    "of" => "من",
    "activated" => "مفعل",
    "deactivated" => "غير مفعل",
    "author_created" => "تم إضافة المؤلف بنجاح",
    "author_updated" => "تم تعديل المؤلف بنجاح",
    "author_deleted" => "تم حذف المؤلف بنجاح",
    "change_small_photo" => "تغيير الصورة المصغرة",
    "change_large_photo" => "تغيير الصورة المكبرة",
    "facebook" => "فيسبوك",
    "twitter" => "تويتر",
    "googleplus" => "جوجل بلس",
    "linkedin" => "لينكد إن",
    "module" => "المؤلفين",
    "permissions" => [
        "create" => "إضافة المؤلفين",
        "edit" => "تعديل المؤلفين",
        "delete" => "حذف المؤلفين"
    ]
];
