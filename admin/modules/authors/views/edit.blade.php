@extends("admin::layouts.master")

@section("breadcrumb")
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2><?php echo trans("authors::authors.edit") ?></h2>
        <ol class="breadcrumb">
            <li>
                <a href="<?php echo URL::to(ADMIN . "/authors"); ?>"><?php echo trans("authors::authors.authors") ?></a>
            </li>
            <li class="active">
                <strong><?php echo trans("authors::authors.edit") ?></strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
        <?php if (User::can("authors.create")) { ?>
            <a href="<?php echo route("authors.create"); ?>" class="btn btn-primary btn-labeled btn-main"> <span class="btn-label icon fa fa-plus"></span> &nbsp; <?php echo trans("authors::authors.add_new") ?></a>
        <?php } ?>
    </div>
</div>
@stop

@section("content")

<div id="content-wrapper">

    <?php if (Session::has("message")) { ?>
        <div class="alert alert-success alert-dark">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <?php echo Session::get("message"); ?>
        </div>
    <?php } ?>

    <?php if ($errors->count() > 0) { ?>
        <div class="alert alert-danger alert-dark">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <?php echo implode(' <br /> ', $errors->all()) ?>
        </div>
    <?php } ?>

    <form action="" method="post" >
        <div class="row">
            <div class="col-md-6">
                <div class="panel panel-default">

                    <div class="panel-body">

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <input name="first_name" value="<?php echo @Input::old("first_name", $user->first_name); ?>" class="form-control input-lg" placeholder="<?php echo trans("authors::authors.first_name") ?>" />
                                </div>


                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6 text-center" style="position:relative; margin: 10px 0">

                                <input type="hidden" value="<?php
                                if ($user and $user->photo != "") {
                                    echo $user->photo_id;
                                }
                                ?>" id="user_photo_id" name="photo" /> 
                                <img id="user_photo" style="border: 1px solid #ccc; width: 100%;" src="<?php if ($user and $user->photo != "") { ?> <?php echo thumbnail($user->photo); ?> <?php } else { ?> <?php echo assets("images/user.png"); ?><?php } ?>" />

                                <a href="javascript:void(0)" id="change_photo" class=""><?php echo trans("authors::authors.change_small_photo") ?></a>
                            </div>
                            <div class="col-md-6 text-center" style="position:relative; margin: 10px 0">

                                <input type="hidden" value="<?php
                                if ($user and $user->photo != "") {
                                    echo $user->image_id;
                                }
                                ?>" id="user_image_id" name="image" /> 
                                <img id="user_image" style="border: 1px solid #ccc; width: 100%;" src="<?php if ($user and $user->image != "") { ?> <?php echo thumbnail($user->image); ?> <?php } else { ?> <?php echo assets("images/user.png"); ?><?php } ?>" />

                                <a href="javascript:void(0)" id="change_image" class=""><?php echo trans("authors::authors.change_large_photo") ?></a>
                            </div>
                        </div>


                        <div class="form-group">
                            <textarea name="about" class="markdown form-control" placeholder="<?php echo trans("authors::authors.about_me") ?>" rows="10"><?php echo @Input::old("about", $user->about); ?></textarea>
                        </div>


                    </div> <!-- / .panel-body -->
                </div>

            </div>


            <div class="col-md-6">
                <div class="panel panel-default">

                    <div class="panel-body">

                        <div class="row form-group">
                            <label class="col-sm-3 control-label"><?php echo trans("authors::authors.activation") ?></label>
                            <div class="col-sm-9">
                                <select class="form-control chosen-select chosen-rtl" name="status">
                                    <option value="1" <?php if ($user and $user->status == 1) { ?> selected="selected" <?php } ?>><?php echo trans("authors::authors.activated") ?></option>
                                    <option value="0" <?php if ($user and $user->status == 0) { ?> selected="selected" <?php } ?>><?php echo trans("authors::authors.deactivated") ?></option>
                                </select>
                            </div>
                        </div>
                    </div> <!-- / .panel-body -->

                </div>

                <div class="panel panel-default">

                    <div class="panel-body">

                        <div class="form-group input-group">
                            <span class="input-group-addon"><i class="fa fa-facebook"></i></span>
                            <input name="facebook" value="<?php echo @Input::old("facebook", $user->facebook); ?>" class="form-control input-lg" placeholder="<?php echo trans("authors::authors.facebook") ?>" />
                        </div>

                        <div class="form-group input-group">
                            <span class="input-group-addon"><i class="fa fa-twitter "></i></span>
                            <input name="twitter" value="<?php echo @Input::old("twitter", $user->twitter); ?>" class="form-control input-lg" placeholder="<?php echo trans("authors::authors.twitter") ?>" />
                        </div>

                        <div class="form-group input-group">
                            <span class="input-group-addon"><i class="fa fa-google-plus"></i></span>
                            <input name="google_plus" value="<?php echo @Input::old("google_plus", $user->google_plus); ?>" class="form-control input-lg" placeholder="<?php echo trans("authors::authors.googleplus") ?>" />
                        </div>

                        <div class="form-group input-group">
                            <span class="input-group-addon"><i class="fa fa-linkedin"></i></span>
                            <input name="linked_in" value="<?php echo @Input::old("linked_in", $user->linked_in); ?>" class="form-control input-lg" placeholder="<?php echo trans("authors::authors.linkedin") ?>" />
                        </div>

                    </div> <!-- / .panel-body -->

                </div>

            </div>
            <div style="clear:both"></div>
            <div class="row">
                <div class="panel-footer" style="border-top: 1px solid #ececec; position: relative;">
                    <div class="form-group" style="margin-bottom:0">
                        <input type="submit" class="pull-right btn btn-flat btn-primary" value="<?php echo trans("authors::authors.save_user") ?>" />
                    </div>
                </div>
            </div>
            <!-- /6. $EASY_PIE_CHARTS -->
        </div>
    </form>
    <!-- /9. $UNIQUE_VISITORS_STAT_PANEL -->

</div>
</div> <!-- / #content-wrapper -->

<script>
    $(document).ready(function () {
        
        $('.chosen-select').chosen();
        
        $("#change_photo, #user_photo").filemanager({
            types: "image",
            done: function (files) {
                if (files.length) {
                    var file = files[0];
                    $("#user_photo_id").val(file.media_id);
                    $("#user_photo").attr("src", file.media_thumbnail);
                }
            },
            error: function (media_path) {
                alert(media_path + " <?php echo trans("authors::authors.is_not_an_image") ?>");
            }
        });

        $("#change_image, #user_image").filemanager({
            types: "image",
            done: function (files) {
                if (files.length) {
                    var file = files[0];
                    $("#user_image_id").val(file.media_id);
                    $("#user_image").attr("src", file.media_thumbnail);
                }
            },
            error: function (media_path) {
                alert(media_path + " <?php echo trans("authors::authors.is_not_an_image") ?>");
            }
        });

    });

</script>
@stop
