<?php

Route::group(array(
    "prefix" => ADMIN,
    "before" => "auth",
        ), function($route) {
    Route::resource('immediate', 'ImmediateController');
    $route->group(array("prefix" => "immediate"), function($route) {
        $route->post('delete', 'ImmediateController@destroy');
        $route->get('{id}/delete', 'ImmediateController@destroy');
    });
});
