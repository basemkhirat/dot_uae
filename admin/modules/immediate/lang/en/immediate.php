<?php

return [
    'home' => 'Home',
    'immediates' => 'immediate',
    'new' => 'Add New',
    'search' => 'Search...',
    'all' => 'All',
    'edit' => 'Edit',
    'bulk' => 'Bulk Actions',
    'delete' => 'Delete immediate',
    'delete_immediate_q' => 'Are you sure to delete immediate?',
    'apply' => 'Apply',
    'per_page' => 'Per Page',
    'last_updated' => 'Last Updated',
    'no_immediate' => 'No immediate found!',
    'showing' => 'Showing',
    'to' => 'to',
    'of' => 'of',
    'immediate' => 'immediate',
    /* immediate page */
    'well_done' => 'Well done!',
    'success_update' => 'Successfully updated.',
    'oh_snap' => 'Oh snap!',
    'required' => 'This field is required.',
    'invalid' => 'This field is invalid.',
    'title' => 'Title',
    'link' => 'Link',
    'save' => 'Save',
    'updated_on' => 'Updated On',
    'created_on' => 'Created On',
    'duration' => 'Duration',
    'ended_on' => 'Ended On',
    'minutes' => 'minutes',
    'immediate' => 'Immediate',
    
    "module" => "Immediate",
    "permissions" => [
        "create" => "Create Immediate",
        "edit" => "Edit Immediate",
        "delete" => "Delete Immediate"
    ]
];
