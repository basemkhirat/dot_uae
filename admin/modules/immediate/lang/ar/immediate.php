<?php

return [
    'home' => 'الرئيسية',
    'immediate' => 'عاجل',
    'new' => 'أضف جديد',
    'search' => 'بحث...',
    'all' => 'الكل',
    'edit' => 'تعديل',
    'bulk' => 'تنفيذ الأمر',
    'delete' => 'حذف',
    'delete_immediate_q' => 'هل تريد حذف عاجل بشكل دائم؟',
    'apply' => 'تنفيذ',
    'per_page' => 'لكل صفحة',
    'name' => 'الاسم',
    'author' => 'الكاتب',
    'last_updated' => 'تاريخ اّخر تعديل',
    'no_immediate' => 'لا يوجد عاجل!',
    'showing' => 'عرض',
    'to' => 'الى',
    'of' => 'من',
    'immediate' => 'عاجل',
    'well_done' => 'تم!',
    'success_update' => 'تم التحديث بنجاح.',
    'oh_snap' => 'للاّسف!',
    'required' => 'هذا الحقل مطلوب.',
    'invalid' => 'هذا الحقل غير سليم.',
    'save' => 'حفظ',
    'title' => 'العنوان',
    'link' => 'الرابط',
    'updated_on' => 'تم التحديث فى',
    'created_on' => 'تاريخ البدء',
    'duration' => 'المدة',
    'ended_on' => 'تاريخ الإنتهاء',
    'minutes' => 'دقيقة',
    'immediate' => 'عاجل',
    
    "module" => "عاجل",
    "permissions" => [
        "create" => "إضافة عاجل",
        "edit" => "تعديل عاجل",
        "delete" => "حذف عاجل"
    ]

];
