<?php

Menu::set("sidebar", function($menu) {
    $menu->item('immediate', trans("admin::common.immediate"), URL::to(ADMIN . '/immediate'))
            ->order(4.5)
            ->icon("fa-thumb-tack");
});

include __DIR__ . "/routes.php";
