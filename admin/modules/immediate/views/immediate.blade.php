@extends("admin::layouts.master")

@section("breadcrumb")
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2><i class="fa fa-thumb-tack faa-tada animated faa-slow"></i> {!!Lang::get('immediate::immediate.immediate')!!}</h2>
        <ol class="breadcrumb" style="background: none;">
            <li><a  href="{!!URL::to('/'.ADMIN.'/dashboard')!!}">{!!Lang::get('admin::common.dashboard')!!}</a></li>
            <li><a  href="{!!URL::to('/'.ADMIN.'/immediate')!!}">{!!Lang::get('immediate::immediate.immediate')!!}</a></li>
            @if(isset($immediate->immediate_id))
                <li>{!!Lang::get('immediate::immediate.edit')!!} </li>
            @else
                <li>{!!Lang::get('immediate::immediate.new')!!} </li>
            @endif
        </ol>
    </div>
    @if(User::can('immediate.create'))
    @if(isset($immediate->immediate_id))
    <div class="col-lg-2">
        <a class="btn btn-primary btn-labeled btn-main pull-right" href="{!!URL::to('/'.ADMIN.'/immediate/create')!!}"> <span class="btn-label icon fa fa-plus"></span> {!!Lang::get('immediate::immediate.new')!!}</a> 
    </div>
    @endif
    @endif
</div>
@stop
@section("content")

@if ($errors->has())
<div class="alert alert-danger alert-dark">
    <button type="button" class="close" data-dismiss="alert">×</button>
    @foreach ($errors->all() as $error)
    <strong>{!!Lang::get('immediate::immediate.oh_snap')!!}</strong> {!! $error !!}
    @endforeach
</div>
@endif

@if(Session::has('message'))
<div class="alert alert-success alert-dark">
    <button type="button" class="close" data-dismiss="alert">×</button>
    <strong>{!!Lang::get('immediate::immediate.well_done')!!}</strong> {!!Lang::get('immediate::immediate.success_update')!!}
</div>
@endif
    
<div id="content-wrapper">



    <div class="row wrapper animated fadeInRight ">

        <div id="content-wrapper">

            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>{!!Lang::get('immediate::immediate.immediate')!!}</h5>
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>

                                <a class="close-link">
                                    <i class="fa fa-times"></i>
                                </a>
                            </div>
                        </div>
                        <div class="ibox-content">
                            @if(isset($immediate->immediate_id))
                            {!! Form::open(array('url' => ADMIN.'/immediate/'.$immediate->immediate_id, 'class' => 'form-horizontal', 'method' => 'put', 'files' => true, 'id' => 'form', 'novalidate' => 'novalidate')) !!}
                            @else
                            {!! Form::open(array('url' => ADMIN.'/immediate', 'files' => true, 'class' => 'form-horizontal', 'id' => 'form', 'novalidate' => 'novalidate')) !!}
                            @endif
                            <div class="form-group"><label class="col-sm-2 control-label">{!!Lang::get('immediate::immediate.title')!!}</label>

                                <div class="col-sm-10">
                                    {!! Form::text('title', Input::old('title', $immediate->title), array('class' => 'form-control input-lg', 'id' => 'title', 'placeholder' => Lang::get('immediate::immediate.title'))) !!}
                                </div>
                            </div>

                            <div class="hr-line-dashed"></div>
                            <div class="form-group"><label class="col-sm-2 control-label">{!!Lang::get('immediate::immediate.link')!!}</label>

                                <div class="col-sm-10">
                                    {!! Form::text('link', Input::old('link', $immediate->link), array('class' => 'form-control input-lg', 'id' => 'link', 'placeholder' => Lang::get('immediate::immediate.link'))) !!}
                                </div>
                            </div>

                            <div class="hr-line-dashed"></div>
                            <div class="form-group"><label class="col-sm-2 control-label">{!!Lang::get('immediate::immediate.duration')!!}</label>

                                <div class="col-sm-5">
                                    <?php
                                    if ($immediate->title != '') {
                                        $duration = $immediate->duration;
                                    } else {
                                        $duration = 60;
                                    }
                                    ?>
                                    <div>
                                        {!! Form::text('duration', $duration, array('class' => 'form-control input-lg', 'id' => 'duration', 'placeholder' => Lang::get('immediate::immediate.duration'), 'style' => 'display: inline-block;')) !!}
                                    </div>
                                    
                                </div>
                                <div class="col-sm-5" style="margin: 15px 0 0; padding: 0">
                                        <span >{!!Lang::get('immediate::immediate.minutes')!!}</span>
                                    </div>
                            </div>

                            @if($immediate->title != '')
                            <div class="form-group"><div class="col-sm-4 col-sm-offset-2">
                                    <div>    
                                        <h5><i class="fa fa-clock-o"></i>&nbsp;&nbsp;<span class="font-noraml">{!!Lang::get('immediate::immediate.created_on')!!}: </span>@if(DIRECTION == 'rtl') {!!arabic_date($immediate->created_on)!!} @else {!! date('M d, Y', strtotime($immediate->created_on)) !!} @ {!! date('h:i a', strtotime($immediate->created_on)) !!} @endif</h5>
                                    </div>
                                    <div>    
                                        <h5><i class="fa fa-clock-o"></i>&nbsp;&nbsp;<span class="font-noraml">{!!Lang::get('immediate::immediate.ended_on')!!}: </span>@if(DIRECTION == 'rtl') {!!arabic_date($immediate->ended_on)!!} @else {!! date('M d, Y', strtotime($immediate->ended_on)) !!} @ {!! date('h:i a', strtotime($immediate->ended_on)) !!} @endif</h5>
                                    </div>
                                </div>
                            </div>
                            @endif  

                            <div class="hr-line-dashed"></div>
                            <div class="form-group">
                                <div class="col-sm-4 col-sm-offset-2">

                                    {!! Form::button(Lang::get('immediate::immediate.save'), array('type' => 'submit', 'class' => 'btn btn-primary')) !!}
                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div>


</div>
</div> <!-- / #content-wrapper -->

<script src="<?php echo assets() ?>/js/plugins/validate/jquery.validate.min.js"></script>

<script>
$(document).ready(function () {

    $("#form").validate({
        rules: {
            title: {
                required: true
            },
            link: {
                required: true,
                url: true
            },
            duration: {
                required: true,
                number: true,
                min: 1
            }
        }
    });
});

jQuery.extend(jQuery.validator.messages, {
    required: "{!!Lang::get('immediate::immediate.required')!!}",
    url: "{!!Lang::get('immediate::immediate.invalid')!!}",
    number: "{!!Lang::get('immediate::immediate.invalid')!!}",
    min: "{!!Lang::get('immediate::immediate.invalid')!!}"
});


</script>

@stop