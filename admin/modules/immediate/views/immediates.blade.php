@extends("admin::layouts.master")

<?php
$params = '';
foreach (Input::except('per_page', 'page') as $key => $value) {
    $params .= '&' . $key . '=' . $value;
}
?>

@section("breadcrumb")
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-5">
        <h2><i class="fa fa-thumb-tack faa-tada animated faa-slow"></i> {!!Lang::get('immediate::immediate.immediate')!!} ({!!$immediate->total()!!})</h2>
        <ol class="breadcrumb" style="background: none;">
            <li><a  href="{!!URL::to('/'.ADMIN.'/dashboard')!!}">{!!Lang::get('admin::common.dashboard')!!}</a></li>
            <li>{!!Lang::get('immediate::immediate.immediate')!!}</li>
        </ol>
    </div>
    <div class="col-sm-5 m-b-xs text-left" >
        {!! Form::open(array('url' => ADMIN.'/immediate', 'method' => 'get', 'id' => 'search', 'style' => 'margin-top: 30px;', 'class' => 'choosen-rtl')) !!}

        <div class="input-group">
            <input type="text" placeholder="{!!Lang::get('immediate::immediate.search')!!}" class="input-sm form-control" value="{!!(Input::get('q')) ? Input::get('q') : ''!!}" name="q" id="q"> <span class="input-group-btn">
                <button type="submit" class="btn btn-sm btn-primary"> {!!Lang::get('immediate::immediate.search')!!}</button> </span>
        </div>

        {!! Form::close() !!}
    </div>
    @if(User::can('immediate.create'))
    <div class="col-lg-2">
        <a class="btn btn-primary btn-labeled btn-main pull-right" href="{!!URL::to('/'.ADMIN.'/immediate/create')!!}"> <span class="btn-label icon fa fa-plus"></span> {!!Lang::get('immediate::immediate.new')!!}</a>
    </div>
    @endif
</div>
@stop
@section("content")

<div id="content-wrapper">

    {!! Form::open(array('url' => ADMIN.'/immediate/delete', 'method' => 'POST', 'id' => 'form')) !!}
    <div class="row wrapper animated fadeInRight ">

        <div id="content-wrapper">


            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5> {!!Lang::get('immediate::immediate.immediate')!!} </h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>

                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <div class="row">

                        <div class="col-sm-10 m-b-xs">
                            <select name="action" id="action" class="form-control m-b" style="width:auto; display: inline-block;">
                                <option value="" selected="selected">{!!Lang::get('immediate::immediate.bulk')!!}</option>
                                @if(User::can('immediate.delete') )
                                <option value="delete">{!!Lang::get('immediate::immediate.delete')!!}</option>
                                @endif
                            </select>

                            <button class="btn btn-primary">{!!Lang::get('immediate::immediate.apply')!!}</button>

                        </div>
                        <div class="col-sm-2">

                            <select class="form-control chosen-rtl" id="per_page" name="per_page" style="width:100%;">
                                <option selected="selected" value="">-- {!!Lang::get('immediate::immediate.per_page')!!} --</option>
                                <option selected="selected" value="10">10</option>
                                <option value="20">20</option>
                                <option value="30">30</option>
                                <option value="40">40</option>
                            </select>
                        </div>
                    </div>
                    <div class="table-responsive">

                        <table cellspacing="0" cellpadding="0" border="0" class="table table-striped">
                            <thead>
                                <tr>
                                    <th><div class="action-checkbox"><label class="px-single"><input type="checkbox" name="checkall" id="checkall" value="" class="i-checks"><span class="lbl"></span></label></div></th>
                            <th>{!!Lang::get('immediate::immediate.title')!!}</th>
                            <th>{!!Lang::get('immediate::immediate.created_on')!!}</th>
                            <th>{!!Lang::get('immediate::immediate.ended_on')!!}</th>
                            <th>{!!Lang::get('immediate::immediate.edit')!!}</th>
                            </tr>
                            </thead>
                            <tbody >
                                <?php $count = count($immediate); ?>
                                @if($count)
                                @foreach($immediate as $row)
                                <tr class="odd gradeX">
                                    <td><div class="action-checkbox"><label class="px-single"><input type="checkbox" name="check[]" value="{!!$row->immediate_id!!}" class="i-checks"><span class="lbl"></span></label></div></td>
                                    <td>
                                        @if(!User::can('immediate.edit'))
                                        {!!$row->title!!}
                                        @else
                                        <a class="text-info" href="{!!URL::to(ADMIN.'/immediate/'.$row->immediate_id.'/edit')!!}"> {!!$row->title!!}</a>
                                        @endif

                                    </td>

                                    <td class="center">@if(DIRECTION == 'rtl') {!!arabic_date($row->created_on)!!} @else {!! date('M d, Y', strtotime($row->created_on)) !!} @ {!! date('h:i a', strtotime($row->created_on)) !!} @endif</td>
                                    <td class="center">@if(DIRECTION == 'rtl') {!!arabic_date($row->ended_on)!!} @else {!! date('M d, Y', strtotime($row->ended_on)) !!} @ {!! date('h:i a', strtotime($row->ended_on)) !!} @endif</td>
                                    <td class="tooltip-demo m-t-md">
                                        @if(User::can('immediate.edit'))
                                        <a class="btn btn-white btn-sm" data-toggle="tooltip" data-placement="top" href="{!!URL::to(ADMIN.'/immediate/'.$row->immediate_id.'/edit')!!}" title="{!!Lang::get('immediate::immediate.edit')!!}"><i class="fa fa-pencil"></i></a>
                                        @endif

                                        @if(User::can('immediate.delete'))
                                        <a data-toggle="tooltip" data-placement="top" data-type="delete" href="{!!URL::to(ADMIN.'/immediate/'.$row->immediate_id.'/delete')!!}" class="btn btn-white btn-sm delete_row" message="{!!Lang::get('immediate::immediate.delete_immediate_q')!!}" title="{!!Lang::get('immediate::immediate.delete')!!}"><i class="fa fa-trash-o"></i></a>
                                        @endif
                                    </td>
                                </tr>
                                @endforeach
                                @else
                                <tr class="odd gradeX" style="height:200px">
                                    <td colspan="5" style="vertical-align:middle; text-align:center; font-weight:bold; font-size:22px">{!!Lang::get('immediate::immediate.no_immediate')!!}</td>
                                </tr>
                                @endif
                            </tbody>
                        </table>
                        <div class="panel-footer text-right" style="border-top: 1px solid #ececec; background: none;">
                            <div class="col-sm-10 m-b-xs" style="padding:0">
                                @if($count)
                                {!!Lang::get('immediate::immediate.showing')!!} {!!$immediate->firstItem()!!} {!!Lang::get('immediate::immediate.to')!!} {!!$immediate->lastItem()!!} {!!Lang::get('immediate::immediate.of')!!} {!!$immediate->total()!!} {!!Lang::get('immediate::immediate.immediate')!!}
                                @endif
                            </div>

                            <div class="col-sm-2 m-b-xs" style="padding:0">
                                <div class="pull-right">
                                    @if($count)
                                    {!!$immediate->appends(Input::all())->setPath('')->render()!!}
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>

    {!! Form::close() !!}

</div>
</div> <!-- / #content-wrapper -->

<script src="<?php echo assets() ?>/js/bootbox.min.js"></script>

<script type="text/javascript">

    $(document).ready(function () {
        $('#per_page').change(function () {
            location.href = '{!!URL::to(ADMIN."/immediate")!!}' + '?per_page=' + $(this).val() + '{!!$params!!}'

        });

        $('#per_page').val("{!!Input::get('per_page')!!}");

        // check all action
        $('#checkall').on('ifChecked', function (event) {
            $("input[name='check[]']").each(function () {
                $(this).iCheck('check');
            });
        });
        $('#checkall').on('ifUnchecked', function (event) {
            $("input[name='check[]']").each(function () {
                $(this).iCheck('uncheck');
            });
        });
        $('.i-checks').iCheck({
            checkboxClass: 'icheckbox_square-green',
        });

        // form submit
        $("#form").submit(function (e) {
            if ($("input[name='check[]']:checked").length == 0 || $("#action").val() == '') {
                return false;
            }
        });

        $('.delete_row').on('click', function (e) {
            e.preventDefault();
            $this = $(this);
            bootbox.dialog({
                message: $this.attr('message'),
                title: $this.attr('title'),
                buttons: {
                    success: {
                        label: "{!!Lang::get('pages::pages.ok')!!}",
                        className: "btn-success",
                        callback: function () {
                            location.href = $this.attr('href');
                        }
                    },
                    danger: {
                        label: "{!!Lang::get('pages::pages.cancel')!!}",
                        className: "btn-primary",
                        callback: function () {
                        }
                    },
                },
                className: "bootbox-sm"
            });
        });

        // search submit
        $("#search").submit(function (e) {
            if ($("#q").val() == '') {
                return false
            }
        });

        
    });
</script>


@stop
