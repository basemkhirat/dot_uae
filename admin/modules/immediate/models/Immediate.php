<?php

class Immediate extends Model {

    protected $table = 'immediate';
    protected $primaryKey = 'immediate_id';
    protected $fillable = [ 'title', 'link', 'duration', 'ended_on', 'created_on'];

    const CREATED_AT = 'created_on';

    public static function boot() {

        parent::boot();
        Event::listen('immediate.expire', function($immediate) {
            $now = date("Y-m-d H:i:s");
            $immediate = $immediate::where('ended_on', '<', $now);
            $immediate->delete();
        });
        Immediate::creating(function($immediate) {
            $duration = $immediate->attributes['duration'];
            $immediate->attributes['ended_on'] = date("Y-m-d H:i:s", strtotime("+$duration minutes"));
        });

        Immediate::updating(function($immediate) {
            $duration = $immediate->attributes['duration'];
//            $oldDuration = $immediate->original['duration'];
//            if ($oldDuration != $duration) {
            $ended_on = date("Y-m-d H:i:s", strtotime("$immediate->created_on+$duration minutes"));
            $immediate->attributes['ended_on'] = $ended_on;
//            }
        });
    }

    /**
     * Filter
     */
    public function scopefilter($query, $args = []) {

        if (isset($args['q']))
            $query->where('title', 'LIKE', '%' . $args['q'] . '%');

        return $query;
    }

    public function setUpdatedAt($value) {
        // Do nothing.
    }

    public function getUpdatedAtColumn() {
        return null;
    }

}
