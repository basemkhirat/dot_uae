<?php

class ImmediateController extends BackendController {

    public $conn = '';

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function __construct() {
        date_default_timezone_set("Etc/GMT-2");

        $this->conn = Session::get('conn');
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index() {

        // delete expires
        $objs = new Immediate;
        Event::fire('immediate.expire', array($objs));

        // initialize variables
        $q = trim(Input::get('q'));
        $per_page = (Input::has('per_page')) ? Input::get('per_page') : 20;

        $this->data['immediate'] = Immediate::filter(['q' => $q])->orderBy('created_on', 'desc')->paginate($per_page);

        return View::make('immediate::immediates')->with($this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create() {
        $this->data['immediate'] = new Immediate();
        return View::make('immediate::immediate')->with($this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store() {

        // validate the info, create rules for the inputs
        $rules = array(
            'title' => 'required',
            'link' => 'url',
            'duration' => 'numeric',
        );

        // run the validation rules on the inputs from the form
        $validator = Validator::make(Input::all(), $rules);

        $validator->setCustomMessages(Lang::get('admin::validation'));

        // if the validator fails, redirect back to the form
        if ($validator->fails()) {
            return Redirect::back()
                            ->withErrors($validator) // send back all errors to the login form
                            ->withInput(); // send back the input (not the password) so that we can repopulate the form
        } else {

            $data = Input::all();

            // insert immediate
            $immediate = new Immediate;
            $immediate->fill($data);
            $immediate->save();
            $immediate_id = $immediate->immediate_id;

            Session::flash('message', 'success');
            return Redirect::to(ADMIN . '/immediate/' . $immediate_id . '/edit');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id) {
        $immediate = Immediate::find($id);

        if ($immediate) {
            Event::fire('immediate.expire', array($immediate));
        }

        if (!$immediate) {
            return Redirect::to(ADMIN . '/immediate');
        }

        $this->data['immediate'] = $immediate;
        return View::make('immediate::immediate')->with($this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id) {

        // validate the info, create rules for the inputs
        $rules = array(
            'title' => 'required',
            'link' => 'url',
            'duration' => 'numeric',
        );

        // run the validation rules on the inputs from the form
        $validator = Validator::make(Input::all(), $rules);

        $validator->setCustomMessages(Lang::get('admin::validation'));

        // if the validator fails, redirect back to the form
        if ($validator->fails()) {
            return Redirect::back()
                            ->withErrors($validator) // send back all errors to the login form
                            ->withInput(); // send back the input (not the password) so that we can repopulate the form
        } else {
            $data = Input::all();
            // update immediate
            $immediate = Immediate::find($id);
            $immediate->fill($data);
            $immediate->save();

            Session::flash('message', 'success');
            return Redirect::back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id = 0) {
        $checks = Input::get('check');
        if ($id) {
            Immediate::destroy($id);
        } else {
            Immediate::destroy($checks);
        }
        return Redirect::back();
    }

}
