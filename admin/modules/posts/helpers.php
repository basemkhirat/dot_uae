<?php

if (!function_exists("publish_box")) {

    /**
     * Outputs the page analysis score in the Publish Box.
     *
     */
    function publish_box($meta) {

        if (@$meta->robots_index == 1) {
            $score_label = 'noindex';
            $title = Lang::get('posts::posts.post_noindex');
            $score_title = $title;
        } else {

            $score = @$meta->score;

            if (!$score) {
                $score_label = 'na';
                $title = Lang::get('posts::posts.post_nokw');
            } else {
                $score_label = AMseo::translate_score($score);
            }

            $score_title = AMseo::translate_score($score, false);
            if (!isset($title)) {
                $title = $score_title;
            }
        }

        $output = '<div class="form-group misc-pub-section misc-yoast misc-pub-section-last">    
                    <div style="margin:0; cursor:pointer" title="' . $title . '" class="wpseo-score-icon ' . $score_label . '"></div>
                </div>';

        return $output;
    }

}

if (!function_exists("add_block_post")) {

    /*
      |--------------------------------------------------------------------------
      | add block post
      |--------------------------------------------------------------------------
      |
     */

    function add_block_post($block_id, $post_id, $count = 8) {
        //  block folder
        //$block_dir = Config::get("cms::app.dbDriver");
        $block_dir = Session::get('conn');
        if (!$block_dir) {
            $block_dir = Config::get("cms::app.dbDriver");
        }

        $filePath = getcwd() . DIRECTORY_SEPARATOR . "blocks" . DIRECTORY_SEPARATOR . $block_dir . DIRECTORY_SEPARATOR . $block_id . ".json";
        $dir = getcwd() . DIRECTORY_SEPARATOR . "blocks" . DIRECTORY_SEPARATOR . $block_dir;

        $arr = array();

        if (file_exists($filePath)) {
            $objData = file_get_contents($filePath);
            $arr = (array) json_decode($objData);
            if (count($arr) < $count) {
                if (!in_array($post_id, $arr)) {
                    $arr[] = $post_id;
                }
            } else {
                array_insert($arr, $post_id, $count);
            }
        } else {
            if (!file_exists($dir)) {
                mkdir($dir, 0777, true);
            }

            fopen($filePath, "w");
            $arr[] = $post_id;
        }

        file_put_contents($filePath, json_encode($arr));
    }

}


if (!function_exists("append_block_post")) {

    /*
      |--------------------------------------------------------------------------
      | add block post
      |--------------------------------------------------------------------------
      |
     */

    function append_block_post($block_id, $post_id, $conn, $count = 8) {
        //  block folder
        $block_dir = $conn;

        $filePath = getcwd() . DIRECTORY_SEPARATOR . "blocks" . DIRECTORY_SEPARATOR . $block_dir . DIRECTORY_SEPARATOR . $block_id . ".json";
        $arr = array();

        if (file_exists($filePath)) {
            $objData = file_get_contents($filePath);
            $arr = (array) json_decode($objData);
            array_insert($arr, $post_id, 0);
        } else {
            $arr[] = $post_id;
        }

        file_put_contents($filePath, json_encode($arr));
    }

}

if (!function_exists("create_block_file")) {

    /*
      |--------------------------------------------------------------------------
      | create block order file
      |--------------------------------------------------------------------------
      |
     */

    function create_block_file($block_id) {
        //  block folder
        //$block_dir = Config::get("cms::app.dbDriver");
        $block_dir = Session::get('conn');
        if (!$block_dir) {
            $block_dir = Config::get("cms::app.dbDriver");
        }

        $filePath = getcwd() . DIRECTORY_SEPARATOR . "blocks" . DIRECTORY_SEPARATOR . $block_dir . DIRECTORY_SEPARATOR . $block_id . ".json";
        $arr = array();

        file_put_contents($filePath, json_encode($arr));
    }

}


if (!function_exists("save_block")) {

    /*
      |--------------------------------------------------------------------------
      | save block
      |--------------------------------------------------------------------------
      |
     */

    function save_block($block_id, $posts) {
        //  block folder
        //$block_dir = Config::get("cms::app.dbDriver");
        $block_dir = Session::get('conn');
        if (!$block_dir) {
            $block_dir = Config::get("cms::app.dbDriver");
        }

        $temp = array();

        $filePath = getcwd() . DIRECTORY_SEPARATOR . "blocks" . DIRECTORY_SEPARATOR . $block_dir . DIRECTORY_SEPARATOR . $block_id . ".json";
        $count = count($posts);

        $objData = file_get_contents($filePath);
        $arr = (array) json_decode($objData);
        $arr1 = array_slice($arr, $count - 1, count($arr) - 1);

        foreach ($arr1 as $key => $value) {
            if (!in_array($value, $posts)) {
                $temp[] = $value;
            }
        }

        $block_posts = array_merge($posts, $temp);

        file_put_contents($filePath, json_encode($block_posts));
    }

}


if (!function_exists("get_block_posts")) {

    /*
      |--------------------------------------------------------------------------
      | get block posts
      |--------------------------------------------------------------------------
      |
     */

    function get_block_posts($block_id, $post_id, $count = 8) {
        //  block folder
        //$block_dir = Config::get("cms::app.dbDriver");
        $block_dir = Session::get('conn');
        if (!$block_dir) {
            $block_dir = Config::get("cms::app.dbDriver");
        }

        $filePath = getcwd() . DIRECTORY_SEPARATOR . "blocks" . DIRECTORY_SEPARATOR . $block_dir . DIRECTORY_SEPARATOR . $block_id . ".json";

        $arr = array();
        $temp = array();

        if (file_exists($filePath)) {
            $objData = file_get_contents($filePath);
            $arr = (array) json_decode($objData);
            //$arr = array_slice($arr, 0, $count-1);

            foreach ($arr as $key => $value) {
                if ($value != $post_id) {
                    $temp[] = $value;
                }
            }

            $arr = $temp;
            $arr = array_slice($arr, 0, $count - 1);
        } else {
            file_put_contents($filePath, '');
        }


        return $arr;
    }

}

if (!function_exists("delete_post_block")) {

    /*
      |--------------------------------------------------------------------------
      | delete post blocks
      |--------------------------------------------------------------------------
      |
     */

    function delete_post_block($block_id, $post_id) {
        //  block folder
        //$block_dir = Config::get("cms::app.dbDriver");
        $block_dir = Session::get('conn');
        if (!$block_dir) {
            $block_dir = Config::get("cms::app.dbDriver");
        }

        $filePath = getcwd() . DIRECTORY_SEPARATOR . "blocks" . DIRECTORY_SEPARATOR . $block_dir . DIRECTORY_SEPARATOR . $block_id . ".json";
        $arr = array();
        $temp = array();

        if (file_exists($filePath)) {
            $objData = file_get_contents($filePath);
            $arr = (array) json_decode($objData);

            foreach ($arr as $key => $value) {
                if ($value != $post_id) {
                    $temp[] = $value;
                }
            }

            file_put_contents($filePath, json_encode($temp));
        }
    }

}

if (!function_exists("array_insert")) {
    /*
      |--------------------------------------------------------------------------
      | Insert element at specific Index of Array.
      |--------------------------------------------------------------------------
      |
     */

    function array_insert(&$array, $element, $position = null) {
        if (count($array) == 0) {
            $array[] = $element;
        } elseif (is_numeric($position) && $position < 0) {
            if ((count($array) + position) < 0) {
                $array = array_insert($array, $element, 0);
            } else {
                $array[count($array) + $position] = $element;
            }
        } elseif (is_numeric($position) && isset($array[$position])) {
            $part1 = array_slice($array, 0, $position, true);
            $part2 = array_slice($array, $position, null, true);
            $array = array_merge($part1, array($position => $element), $part2);
            foreach ($array as $key => $item) {
                if (is_null($item)) {
                    unset($array[$key]);
                }
            }
        } elseif (is_null($position)) {
            $array[] = $element;
        } elseif (!isset($array[$position])) {
            $array[$position] = $element;
        }
        $array = array_merge($array);
        return $array;
    }

}

//if (!function_exists("get_post_details")) {
//
//    /*
//      |--------------------------------------------------------------------------
//      | get post details
//      |--------------------------------------------------------------------------
//      |
//     */
//
//    function get_post_details($post_slug, $site, $from_slug = '') {
//        $link = "";
//        $sites = Config::get("sites");
//        foreach ($sites as $key => $value) {
//            if ($key == $site) {
//                $link = $value . "details/" . $post_slug;
//            }
//        }
//        return $link;
//    }
//
//}

if (!function_exists("buildtree")) {

    /**
     * Build tree
     */
    function buildtree($src_arr, $parent_id = 0, $tree = array()) {
        foreach ($src_arr as $idx => $row) {
            if ($row['cat_parent'] == $parent_id) {
                foreach ($row as $k => $v)
                    $tree[$row['cat_id']][$k] = $v;
                unset($src_arr[$idx]);


                $child = buildtree($src_arr, $row['cat_id']);
                if ($child) {
                    $tree[$row['cat_id']]['children'] = $child;
                }
            }
        }
        //ksort($tree);
        return $tree;
    }

}

if (!function_exists("cats_tree")) {

    $output = '';

    /**
     * Draw tree
     */
    function cats_tree($tree, $recursionDepth = 0, $maxDepth, $args = [], $excep = []) {
        global $output;

        if ($maxDepth && ($recursionDepth == $maxDepth))
            return;

        if ($recursionDepth == 0) {
            $output .= "<ul class='full-height-scroll'>";
        } else {
            $output .= "<ul>";
        }

        foreach ($tree as $k => $v) {
            if (!in_array($tree[$k]['cat_id'], $excep)) {

                $output .= "<li><div class='checkbox'><label>- <input type='checkbox' class='i-checks' name='" . $args['checkbox_name'] . "[]' value='" . $tree[$k]['cat_id'] . "'> <span class='lbl'>" . $tree[$k]['cat_name'] . "</span></label></div>";

                if (isset($tree[$k]['children']))
                    cats_tree($tree[$k]['children'], $recursionDepth + 1, $maxDepth, $args, $excep);

                $output .= "</li>";
            }
        }

        $output .= "</ul>";


        return $output;
    }

}

if (!function_exists("parent_tree")) {

    $parent = '';
    $space = '';

    /**
     * Draw tree
     */
    function parent_tree($tree, $recursionDepth = 0, $maxDepth, $class = '', $del = '', $excep = []) {
        global $parent, $space;

        if ($maxDepth && ($recursionDepth == $maxDepth))
            return;

        if ($recursionDepth == 0) {
            $parent = '';
            $space = '';
        }

        foreach ($tree as $k => $v) {
            if (!in_array($tree[$k]['cat_id'], $excep)) {
                $parent .= "<option value='" . $tree[$k]['cat_id'] . "' class='" . $class . "'>" . $del . $tree[$k]['cat_name'] . "</option>";

                if (isset($tree[$k]['children'])) {
                    $space = '&nbsp;&nbsp;&nbsp;';
                    parent_tree($tree[$k]['children'], $recursionDepth + 1, $maxDepth, $class, $space);
                }
            }
        }


        return $parent;
    }

}

if (!function_exists("regenerate_sizes")) {

    function regenerate_sizes($media_id = 0) {
        $media = DB::table("media")->where("media_id", $media_id)->first();
        $uploads = getcwd() . DIRECTORY_SEPARATOR . "uploads";
        if (count($media)) {
            $filename = $media_path = $media->media_path;
            $url = AMAZON_URL;
            $dir_parts = explode("/", $filename);
            $image_name = end($dir_parts);

            if (strstr($filename, "/")) {   // aws file
                $path = $dir_parts[0] . "/" . $dir_parts[1] . "/thumbnail-" . $dir_parts[2];
            } else {
                $filename = "uploads/uploads/" . $image_name;
                $path = "uploads/uploads/" . "thumbnail-" . $image_name;
            }

            $url .= $path;
            $handle = @fopen($url, "r");
            if (!$handle) {
                $url = AMAZON_URL . $filename;
                $handle1 = @fopen($url, "r");
                if ($handle1) {
                    file_put_contents($uploads . "/" . $image_name, file_get_contents($url));
                    if (File::exists($uploads . "/" . $image_name)) {
                        $sizes = Config::get("cms::app.sizes");
                        foreach ($sizes as $size => $dimensions) {
                            if ($size == "free") {
                                \Image::make(UPLOADS . "/" . $image_name)
                                        ->resize($dimensions[0], null, function ($constraint) {
                                            $constraint->aspectRatio();
                                            $constraint->upsize();
                                        })
                                        ->save(UPLOADS . "/" . $size . "-" . $image_name);
                            } else {
                                \Image::make(UPLOADS . "/" . $image_name)
                                        ->fit($dimensions[0], $dimensions[1], function ($constraint) {
                                            $constraint->upsize();
                                        })
                                        ->save(UPLOADS . "/" . $size . "-" . $image_name);
                            }
                            if (strstr($media_path, "/")) {   // aws file
                                $s3_path = $dir_parts[0] . "/" . $dir_parts[1] . "/" . $size . "-" . $image_name;
                            } else {
                                $s3_path = "uploads/uploads/" . $size . "-" . $image_name;
                            }

                            s3_save($s3_path);
                            unlink($uploads . "/" . $size . "-" . $image_name);
                        }
                        unlink($uploads . "/" . $image_name);
                    }
                }
            }
        }
    }

}

