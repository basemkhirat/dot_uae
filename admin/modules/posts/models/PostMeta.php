<?php

class PostMeta extends Model {

    protected $table = 'meta';
    protected $fillable = [];
    protected $guarded = ['id'];
    public $timestamps = false;

    public function post() {
        return $this->belongsTo('Post', 'post_id');
    }

    public function page() {
        return $this->belongsTo('Page', 'post_id');
    }

    public function facebook() {
        return $this->hasOne('Media', 'media_id', 'facebook_image');
    }

    public function twitter() {
        return $this->hasOne('Media', 'media_id', 'twitter_image');
    }

}
