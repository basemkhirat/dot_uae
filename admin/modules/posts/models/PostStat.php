<?php

class PostStat extends Model {

    protected $table = 'posts_stats';
    protected $primaryKey = 'post_id';
    public $timestamps = false;

    public function posts() {
        return $this->belongsTo('Post', 'post_id', 'post_id');     
    }

}
