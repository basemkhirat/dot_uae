<?php

class Post extends Model {

    protected $table = 'posts';
    protected $primaryKey = 'post_id';
    protected $fillable = [
        'post_published_date',
        'post_created_date',
        'post_author_id',
        'post_editor_id',
        'post_image_id',
        'post_media_id',
        'post_type',
        'post_format',
        'has_media',
        'post_slug',
        'post_status',
        'youtube_link',
        'post_views',
        'post_desked',
        'post_submitted',
        'post_writers',
        'comment_options',
        'image_hide',
        'opened',
        'opened_date',
        'post_closed',
        'translated',
        'post_title',
        'post_subtitle',
        'post_excerpt',
        'post_content',
//        'meta_title',
//        'meta_keywords',
//        'meta_description',
//        'facebook_title',
//        'facebook_description',
//        'twitter_description',
//        'facebook_image',
        'source_id',
        'source_name',
        'source_type',
        'provider',
        'mongo_id',
        'link',
        'seen',
        'site',
        'pin',
        'post_meta_id'];
    protected $guarded = ['post_id'];
    public $timestamps = true;

    const CREATED_AT = 'post_created_date';
    const UPDATED_AT = 'post_updated_date';

//    protected $attributes = array('field' => 'value');

    public function user() {
        return $this->hasOne('User', 'id', 'post_user_id');
    }

    public function author() {
        return $this->hasOne('User', 'id', 'post_author_id');
    }

    public function editor() {
        return $this->hasOne('User', 'id', 'post_editor_id');
    }

    public function image() {
        return $this->hasOne('Media', 'media_id', 'post_image_id');
    }

    public function media() {
        return $this->hasOne('Media', 'media_id', 'post_media_id');
    }

    public function meta() {
        return $this->hasOne('PostMeta')->where('type', 1);
    }

    public function revisions() {
        return $this->hasMany('Revision', 'post_id');
    }

    public function liveblogging() {
        return $this->hasMany('Liveblogging', 'post_id')->orderBy('date', 'desc');
    }

    public function history() {
        return $this->hasMany('History', 'post_id')->orderBy('update_date', 'desc');
    }

    public function stats() {
        return $this->hasMany('PostStat', 'post_id');
    }

    public function comments() {
        return $this->hasMany('Comment', 'comment_post_id')->where('comment_status', '!=', 3)
                        ->where('comment_status', '!=', 4)
                        ->orderBy('comment_date', 'desc');
    }

    public function notes() {
        return $this->hasMany('Note', 'note_post_id')->orderBy('note_date', 'desc');
    }

    public function categories() {
        return $this->belongsToMany('Category', 'posts_categories', 'post_id', 'cat_id');
    }

    public function tags() {
        return $this->belongsToMany('Tag', 'posts_tags', 'post_id', 'tag_id');
    }

    public function galleries() {
        return $this->belongsToMany('Gallery', 'posts_galleries', 'post_id', 'post_gallery_id');
    }

    public function linkedPosts() {
        return $this->belongsToMany('Post', 'posts_linked', 'post_id', 'post_linked_id');
    }

    public function relatedPosts() {
        return $this->belongsToMany('Post', 'posts_related', 'post_id', 'post_related_id');
    }

    public function photos() {
        return $this->belongsToMany('Media', 'posts_media', 'post_id', 'media_id')->withPivot(array('media_title'));
    }

    public function sets() {
        return $this->belongsToMany('Set', 'posts_related_groups', 'post_id', 'post_group_id');
    }

    public function topics() {
        return $this->belongsToMany('Topic', 'posts_topics', 'post_id', 'topic_id');
    }

    public function getTitleAttribute() {
        return ($this) ? $this->post_title : '';
    }

    public function notifyUsers() {
        return $this->belongsToMany('User', 'post_notify');
    }

    public function shortUrl() {
        return $this->hasOne('Shortener', 'post_id', 'post_id');
    }

    public function getUrlAttribute() {
        return URL::to('details/' . $this->post_slug);
    }

    public function getUserNotifySelect2ValueAttribute() {
        return implode(',', $this->notifyUsers->lists('id'));
    }

    public function getTopicsSelect2ValueAttribute() {
        return implode(',', $this->topics->lists('id'));
    }

    public function setPostSlugAttribute($value) {

        $slug = create_slug($value);
        if (array_key_exists('post_id', $this->attributes)) {
            $isUnique = self::where('post_slug', $slug)->where('post_id', '!=', $this->attributes['post_id'])->first();
        } else
            $isUnique = self::where('post_slug', $slug)->first();

        while ($isUnique !== NULL) {
            $slug = $slug . '-' . rand(0, 99999);
            if (array_key_exists('post_id', $this->attributes)) {
                $isUnique = self::where('post_slug', $slug)->where('post_id', '!=', $this->attributes['post_id'])->first();
            } else
                $isUnique = self::where('post_slug', $slug)->first();
        }
        $this->attributes['post_slug'] = $slug;
    }

    public static function boot() {
        $statusArray = array(
            '1' => 'منشور',
            '4' => 'مسودة',
            '5' => 'محذوف',
            '6' => 'مجدول'
        );

        parent::boot();

        Post::saved(function($post) {
            Event::fire("post.saved", $post);
        });

        Post::creating(function($post) {
            $post->attributes['post_user_id'] = Auth::user()->id;
            if (!isset($post->attributes['post_editor_id'])) {
                $post->attributes['post_editor_id'] = Auth::user()->id;
            }
            //$post->attributes['post_slug'] = $post->post_title;
            $post->attributes['comment_options'] = 3;
            $post->attributes['post_views'] = 0;
            $post->attributes['seen'] = 1;
            $post->attributes['post_status'] = 0;
            $post->attributes['post_desked'] = 0;
            $post->attributes['post_submitted'] = 0;
            $post->attributes['post_closed'] = 0;
            $post->attributes['translated'] = 0;
            $post->attributes['image_hide'] = 0;

//            if (User::can('posts.unpublish') && User::can('posts.published')) {
//                $post->attributes['post_status'] = (isset($post->attributes['post_status'])) ? $post->attributes['post_status'] : 0;
//            }
//
//            if (!User::can('posts.reviewed')) {
//                $post->attributes['post_desked'] = (isset($post->attributes['post_desked'])) ? $post->attributes['post_desked'] : 0;
//            }
//
//            if (User::can('posts.pending')) {
//                $post->attributes['post_submitted'] = (isset($post->attributes['post_submitted'])) ? $post->attributes['post_submitted'] : 0;
//            }
//
//            if (User::can('posts.close_post')) {
//                $post->attributes['post_closed'] = (isset($post->attributes['post_closed'])) ? $post->attributes['post_closed'] : 0;
//            }
//            $post->attributes['translated'] = (isset($post->attributes['translated'])) ? $post->attributes['translated'] : 0;
//            $post->attributes['image_hide'] = (isset($post->attributes['image_hide'])) ? $post->attributes['image_hide'] : 0;
        });

        Post::created(function($post) {
            // insert post revision
            if ($post->post_status) {
                $revision = new Revision(['post_id' => $post->post_id, 'post_user_id' => Auth::user()->id, 'post_status' => $post->post_status]);
                $post->revisions()->save($revision);
            }

            $post_status = ($post->post_status) ? $post->post_status : null;
            // insert post history
            $history = new History(['user_id' => Auth::user()->id, 'post_status' => $post_status]);
            $post->history()->save($history);

            // create post meta
            $meta = new PostMeta(['robots_index' => 2, 'robots_follow' => 1, 'in_sitemap' => 1, 'sitemap_priority' => .9, 'score' => 0, 'type' => 1]);
            $post->meta()->save($meta); // or $post->meta()->create($meta);
        });

        Post::deleting(function($post) {
            $post->sets()->detach();
            $post->tags()->detach();
            $post->categories()->detach();
            $post->galleries()->detach();
            $post->photos()->detach();
            $post->relatedPosts()->detach();
            $post->linkedPosts()->detach();
            $post->topics()->detach();

            $post->revisions()->delete();
            $post->history()->delete();
            $post->comments()->delete();
            $post->notes()->delete();
            $post->liveblogging()->delete();
            $post->meta()->delete();
        });

        Event::listen('post.updating', function($post) {

            Session::put('original_' . $post->post_id, $post);

            // insert post revision
            if (($post->original['post_status'] || $post->original['post_status'] == '0') && $post->original['post_status'] != $post->attributes['post_status']) {
                $revision = new Revision(['post_id' => $post->post_id, 'post_user_id' => Auth::user()->id, 'post_status' => $post->attributes['post_status']]);
                $post->revisions()->save($revision);
            }

            if (!User::can('posts.published') || (!User::can('posts.unpublish') && $post->original['post_status'] == 1)) {
                $post_status = null;
            } else {
                if ($post->attributes['post_status'] == 1 && $post->original['post_status'] != 1) {
                    $post_status = 1;
                } elseif ($post->attributes['post_status'] != 1 && $post->original['post_status'] == 1) {
                    $post_status = 0;
                } elseif ($post->attributes['post_status'] == 6 && $post->original['post_status'] != 6) {
                    $post_status = 6;
                } else {
                    $post_status = null;
                }
            }

//            if (User::can('posts.unpublish') && User::can('posts.published')) {
//                if ($post->attributes['post_status'] == 1 && $post->original['post_status'] != 1) {
//                    $post_status = 1;
//                } elseif ($post->attributes['post_status'] != 1 && $post->original['post_status'] == 1) {
//                    $post_status = 0;
//                } elseif ($post->attributes['post_status'] == 6 && $post->original['post_status'] != 6) {
//                    $post_status = 6;
//                } else {
//                    $post_status = null;
//                }
//            } else {
//                $post_status = null;
//            }
            // insert post history
            $history = new History(['post_id' => $post->post_id, 'user_id' => Auth::user()->id, 'post_status' => $post_status]);
            $post->history()->save($history);
        });

        Event::listen('post.updated', function($post) use ($statusArray) {

            if ($post->post_user_id != Auth::user()->id) {
                $original = Session::get('original_' . $post->post_id);
                Session::forget('original_' . $post->post_id);
                $body = false;
                if ($original->post_status != $post->post_status) {
                    if ($post->post_status == 0) {
                        $body = 'تم سحب الخبر ' . '<strong>' . $post->post_title . '</strong>' . 'من النشر ';
//                        $body = trans('unpublish', array('title' => '<strong>' . $post->post_title . '</strong>'));
                    } elseif ($original->post_status == 0) {
//                        $body = trans('publish', array('title' => '<strong>' . $post->post_title . '</strong>'));
                        $body = 'تم نشر الخبر ' . '<strong>' . $post->post_title . '</strong>';
                    } else {
                        $body = 'تم تغيير حالة الخبر ' . '<strong>' . $post->post_title . '</strong>' . ' من ' . $statusArray[$original->post_status] . ' الى ' . $statusArray[$post->post_status];
//                        $body = trans('state_change', array(
//                            'title' => '<strong>' . $post->post_title . '</strong>',
//                            'state_from' => $statusArray[$original->post_status],
//                            'state_to' => $statusArray[$post->post_status]
//                        ));
                    }
                } elseif ($post->post_desked != $original->post_desked || $post->post_submitted != $original->post_submitted) {
                    $body = 'تم تغيير حالة الدسك للخبر ' . '<strong>' . $post->post_title . '</strong>';
                    // $body = $body = trans('desk_change', array('title' => '<strong>' . $post->post_title . '</strong>'));
                }
                if ($body) {
                    $noti = new Notification();
                    $noti->user_id = $post->post_user_id;
                    $noti->sender_id = Auth::user()->id;
                    $noti->body = $body;
                    $noti->object_id = $post->post_id;
                    $noti->object_type = 'post';
                    $noti->save();
                }
                foreach ($post->notifyUsers as $notifyUser) {
                    if (Auth::user()->id != $notifyUser->id) {
                        $noti2 = new Notification();
                        $noti2->user_id = $notifyUser->id;
                        $noti2->sender_id = Auth::user()->id;
                        $noti2->body = $body;
                        $noti2->object_id = $post->post_id;
                        $noti2->object_type = 'post';
                        $noti2->save();
                    }
                }
            }

            //clearing the cache
            /* $memcached = \Illuminate\Support\Facades\Cache::driver('memcached');
              $cacheId = 'single_' . $post->post_id;
              if ($memcached->get($cacheId)) {
              $memcached->forget($cacheId);
              } */
        });
    }

    public function scopefilter($query, $args = []) {

        if (is_array($args['cat_id'])) {
            $cat = $args['cat_id'];
            $query->whereHas('categories', function($q) use ($cat) {
                $q->whereIn('categories.cat_id', $cat);
            });
        } elseif (isset($args['cat_id']) && $args['cat_id'] != '') {
            $cat = $args['cat_id'];
            $query->whereHas('categories', function($q) use ($cat) {
                $q->where('categories.cat_id', $cat);
            });
        }

        if (isset($args['tag_id']) && $args['cat_id'] != 'tag_id') {
            $tag = $args['tag_id'];
            $query->whereHas('tags', function($q) use ($tag) {
                $q->where('tags.tag_id', $tag);
            });
        }

        if (isset($args['user_id']) && $args['cat_id'] != 'user_id') {
            $query->where('post_editor_id', $args['user_id']);
        }

        if (isset($args['conn']) && $args['cat_id'] != 'conn') {
            $query->where('posts.site', $args['conn']);
        }

        if (isset($args['post_new']) && $args['cat_id'] != 'post_new') {
            $query->where('post_desked', '!=', 1);
            $query->where('post_submitted', '!=', 1);
        }

        if (isset($args['sources']) && $args['cat_id'] != 'sources') {
            $query->where('source_id', $args['sources']);
        }

        if ((isset($args['provider']) || $args['provider'] === 0) && $args['provider'] != '') {
            $query->where('provider', $args['provider']);
        }

        if (isset($args['post_status']) && $args['cat_id'] != 'post_status') {
            $query->where('post_status', $args['post_status']);
        } else {
            $query->where('post_status', '!=', '5');
        }

        if (isset($args['post_desked']) && $args['cat_id'] != 'post_desked') {
            $query->where('post_desked', $args['post_desked']);
        }

        if (isset($args['post_submitted']) && $args['cat_id'] != 'post_submitted') {
            $query->where('post_submitted', $args['post_submitted'])
                    ->where('post_desked', '!=', 1);
        }

        if (isset($args['format']) && $args['format'] != '')
            $query->where('post_format', $args['format']);

        if (isset($args['post_type']) && $args['post_type'] != '')
            $query->where('post_type', $args['post_type']);

        if (isset($args['translated']) && $args['translated'] != '')
            $query->where('translated', $args['translated']);

        if (isset($args['q']))
            $query->where('post_title', 'LIKE', '%' . $args['q'] . '%');

        if (isset($args['kw_filter']) && $args['kw_filter'] != '') {
            $query->whereHas('meta', function($q) use($args) {
                $q->where('focus_keyword', $args['kw_filter']);
            });
        }

        if ($args['order'] == 'new' || $args['order'] == 'old' || $args['order'] == 'published' || $args['order'] == 'updated') {
            if ($args['order'] == 'new') {
                $query->orderBy('post_created_date', 'DESC');
            } elseif ($args['order'] == 'old') {
                $query->orderBy('post_created_date', 'ASC');
            } elseif ($args['order'] == 'published') {
                $query->orderBy('post_published_date', 'DESC');
            } elseif ($args['order'] == 'updated') {
                $query->orderBy('post_updated_date', 'DESC');
            }
        } else if ($args['order'] == 'most') {
            $query->orderBy('post_views', 'DESC');
        } else {
            $query->orderBy('post_updated_date', 'DESC');
        }

        return $query;
    }

    public static function get_posts_count($status = 1, $user_id = false, $date = false) {

        $ob = DB::table('posts');

        if ($status == 1) {
            $ob->where("post_status", "=", $status);
        }

        if ($status == 2) {
            $ob->where("posts.post_submitted", 1);
            $ob->where("posts.post_desked", 0);
        }
        if ($status == 3) {
            $ob->where("posts.post_desked", 1);
        }


        $ob->where("post_user_id", "=", $user_id);

        if ($date) {
            $ob->where(DB::raw("DATE(post_updated_date)"), "=", $date);
        }

        $count = $ob->count();

        return $count;
    }

}
