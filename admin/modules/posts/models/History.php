<?php

class History extends Model {

    protected $table = 'post_updates_history';
    public $fillable = array('post_id', 'user_id', 'post_status');
    protected $guarded = ['id'];

    const CREATED_AT = 'update_date';

    public function setUpdatedAt($value) {
        // Do nothing.
    }

    public function getUpdatedAtColumn() {
        return null;
    }

    public function user() {
        return $this->hasOne('User', 'id', 'user_id');
    }

}
