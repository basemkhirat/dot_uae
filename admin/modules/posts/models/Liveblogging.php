<?php

class Liveblogging extends Model {

    protected $table = 'posts_liveblogging';
    public $fillable = array('post_id', 'title', 'link');
    protected $guarded = ['id'];

    const CREATED_AT = 'date';

    public function setUpdatedAt($value) {
        // Do nothing.
    }

    public function getUpdatedAtColumn() {
        return null;
    }

}
