<?php

namespace Mediasci\Cms\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;
use Illuminate\Support\Facades\DB;

class PostTag extends Eloquent {

    protected $table = 'posts_tags';
    public $fillable = array(
        'post_id',
        'tag_id'
    );
    public $timestamps = false;

}
