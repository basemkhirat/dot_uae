<?php

class Revision extends Model {

    protected $table = 'posts_revisions';
    public $fillable = array('post_id', 'post_user_id', 'post_status');
    protected $guarded = ['id'];

    const CREATED_AT = 'revision_date';

    public function setUpdatedAt($value) {
        // Do nothing.
    }

    public function getUpdatedAtColumn() {
        return null;
    }
    
    public function getRevisionDateAttribute() {
        return date('Y-m-d h:i:s a', strtotime($this->original['revision_date']));
    }

    public function user() {
        return $this->hasOne('User', 'id', 'post_user_id');
    }

}
