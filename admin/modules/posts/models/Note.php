<?php

class Note extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'notes';
    public $primaryKey = 'note_id';
    protected $fillable = ['note_content', 'note_author', 'note_post_id', 'note_user_id'];
    protected $guarded = ['note_id'];
    public $timestamps = true;

    const CREATED_AT = 'note_date';

    public function user() {
        return $this->hasOne('User', 'id', 'note_user_id');
    }

    public function post() {
        return $this->belongsTo('Post', 'note_post_id', 'post_id');
    }

    public function setUpdatedAt($value) {
        // Do nothing.
    }

    public function getUpdatedAtColumn() {
        return null;
    }

    public static function boot() {

        parent::boot();

        Note::created(function($note) {
            //Event::listen('note.created', function($noteId) {
            //$note = Note::find($noteId);
            $postUserId = @$note->post->post_user_id;
            if ($postUserId != Auth::user()->id) {
                $body = 'ترك ملحوظة على خبر ' . '<strong>' . $note->post->post_title . '</strong>';
                $noti = new Notification();
                $noti->user_id = $postUserId;
                $noti->sender_id = Auth::user()->id;
                $noti->body = $body;
                $noti->object_id = $note->note_id;
                $noti->object_type = 'note';
                $noti->save();
            }
            foreach ($note->post->notifyUsers as $notifyUser) {
                if (Auth::user()->id != $notifyUser->id) {
                    $noti2 = new Notification();
                    $noti2->user_id = $notifyUser->id;
                    $noti2->sender_id = Auth::user()->id;
                    $noti2->body = $body;
                    $noti2->object_id = $note->note_id;
                    $noti2->object_type = 'note';
                    $noti2->save();
                }
            }
        });
    }

}
