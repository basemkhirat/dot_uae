<?php

//

class PostController extends BackendController {

    public $data = array();
    public $conn = '';

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function __construct() {
        date_default_timezone_set("Etc/GMT-2");

        $this->conn = Session::get('conn');
    }

    public function index() {

        // initialize variables
        $cat_id = Input::get('cat_id');
        $tag_id = Input::get('tag_id');
        $user_id = Input::get('user_id');
        $post_status = Input::get('post_status');
        $translated = Input::get('translated');
        $post_desked = Input::get('desked');
        $post_submitted = Input::get('submitted');
        $post_new = Input::get('new');
        $order = Input::get('order_by');
        $format = Input::get('format');
        $sources = Input::get('sources');
        $provider = Input::get('provider');
        $q = trim(Input::get('q'));
        $kw_filter = trim(Input::get('kw_filter'));
        $per_page = (Input::has('per_page')) ? Input::get('per_page') : 20;
        $post_type = (Input::get('post_type'));

        $this->data['posts_count'] = Post::filter(['post_type' => $post_type, 'translated' => $translated, 'sources' => $sources, 'provider' => $provider,
                    'kw_filter' => $kw_filter, 'cat_id' => $cat_id, 'tag_id' => $tag_id, 'user_id' => $user_id, 'q' => $q, 'order' => $order, 'format' => $format, 'conn' => $this->conn])->count();
        $this->data['posts_published'] = Post::filter(['post_type' => $post_type, 'post_status' => 1, 'translated' => $translated, 'sources' => $sources, 'provider' => $provider,
                    'kw_filter' => $kw_filter, 'cat_id' => $cat_id, 'tag_id' => $tag_id, 'user_id' => $user_id, 'q' => $q, 'order' => $order, 'format' => $format, 'conn' => $this->conn])->count();
        $this->data['posts_desked'] = Post::filter(['post_type' => $post_type, 'post_desked' => 1, 'translated' => $translated, 'sources' => $sources, 'provider' => $provider,
                    'kw_filter' => $kw_filter, 'cat_id' => $cat_id, 'tag_id' => $tag_id, 'user_id' => $user_id, 'q' => $q, 'order' => $order, 'format' => $format, 'conn' => $this->conn])->count();
        $this->data['posts_submitted'] = Post::filter(['post_type' => $post_type, 'post_submitted' => 1, 'translated' => $translated, 'sources' => $sources, 'provider' => $provider,
                    'kw_filter' => $kw_filter, 'cat_id' => $cat_id, 'tag_id' => $tag_id, 'user_id' => $user_id, 'q' => $q, 'order' => $order, 'format' => $format, 'conn' => $this->conn])->count();
        $this->data['posts_draft'] = Post::filter(['post_type' => $post_type, 'post_status' => 4, 'translated' => $translated, 'sources' => $sources, 'provider' => $provider,
                    'kw_filter' => $kw_filter, 'cat_id' => $cat_id, 'tag_id' => $tag_id, 'user_id' => $user_id, 'q' => $q, 'order' => $order, 'format' => $format, 'conn' => $this->conn])->count();
        $this->data['posts_trash'] = Post::filter(['post_type' => $post_type, 'post_status' => 5, 'translated' => $translated, 'sources' => $sources, 'provider' => $provider,
                    'kw_filter' => $kw_filter, 'cat_id' => $cat_id, 'tag_id' => $tag_id, 'user_id' => $user_id, 'q' => $q, 'order' => $order, 'format' => $format, 'conn' => $this->conn])->count();
        $this->data['posts_scheduled'] = Post::filter(['post_type' => $post_type, 'post_status' => 6, 'translated' => $translated, 'sources' => $sources, 'provider' => $provider,
                    'kw_filter' => $kw_filter, 'cat_id' => $cat_id, 'tag_id' => $tag_id, 'user_id' => $user_id, 'q' => $q, 'order' => $order, 'format' => $format, 'conn' => $this->conn])->count();
        $this->data['posts_new'] = Post::filter(['post_type' => $post_type, 'post_new' => 1, 'translated' => $translated, 'sources' => $sources, 'provider' => $provider,
                    'kw_filter' => $kw_filter, 'cat_id' => $cat_id, 'tag_id' => $tag_id, 'user_id' => $user_id, 'q' => $q, 'order' => $order, 'format' => $format, 'conn' => $this->conn])->count();

        $this->data['posts'] = Post::with('categories', 'tags', 'comments', 'meta')->filter(['post_type' => $post_type, 'translated' => $translated, 'sources' => $sources, 'provider' => $provider, 'post_status' => $post_status, 'post_desked' => $post_desked, 'post_submitted' => $post_submitted,
                    'kw_filter' => $kw_filter, 'post_new' => $post_new, 'cat_id' => $cat_id, 'tag_id' => $tag_id, 'user_id' => $user_id, 'q' => $q, 'order' => $order, 'format' => $format, 'conn' => $this->conn])->paginate($per_page);

        $this->data['cats'] = Category::whereSite($this->conn)->get()->toArray();

        $this->data['sources'] = MongoSources::where('status', (int) 1)->whereRaw(array('lang' => $this->conn))->where('deleted', (int) 0)->get();

        unset($cat_id, $tag_id, $user_id, $post_status, $translated, $post_desked, $post_submitted, $post_new, $order, $format, $sources, $provider, $q, $per_page, $post_type);
        return View::make('posts::posts')->with($this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create() {

        if (Input::get('post_type') == 'article') {
            $this->data['authors'] = User::whereHas('role', function($q) {
                                $q->where('role_slug', '=', 'author');
                            })
                            ->orderBy('id', 'DESC')->get();
        } else {

            $this->data['editors'] = User::whereHas('role.rolePermissions', function($q) {
                                $q->where('permission', '=', 'posts.create')
                                ->orWhere('permission', '=', 'posts.edit');
                            })
                            ->orWhereHas('permissions', function($q) {
                                $q->where('permission', '=', 'posts.create')
                                ->orWhere('permission', '=', 'posts.edit');
                            })
                            ->orderBy('id', 'DESC')->get();
        }

        $this->data['post'] = new Post();
        $this->data['cats'] = Category::whereSite($this->conn)->get()->toArray();
        $this->data['most_cats'] = Category::with('posts')->whereSite($this->conn)->take('5')->get();
        $this->data['most_tags'] = Tag::with('posts')->take('5')->get();
        $this->data['galleries'] = Gallery::all();
        $this->data['sets'] = Set::all();

        // calculate seo results
        $amseo = new Amseo();
        $this->data['seo_results'] = $amseo->linkdex_output(new Post());
        $this->data['publish_seo'] = $amseo->publish_box();

        return View::make('posts::post')->with($this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store() {

        // validate the info, create rules for the inputs
        $rules = array(
            'post_title' => 'required|unique:posts',
            'tags' => 'required',
        );

        // run the validation rules on the inputs from the form
        $validator = Validator::make(Input::all(), $rules);

        $validator->setCustomMessages(Lang::get('admin::validation'));

        // if the validator fails, redirect back to the form
        if ($validator->fails()) {
            return Redirect::back()
                            ->withErrors($validator) // send back all errors to the login form
                            ->withInput(); // send back the input (not the password) so that we can repopulate the form
        } else {
            $curr_cat = Input::get('curr_cat');
            $post_type = Input::get('post_type');

            $data = Input::all();

            $data['post_image_id'] = Input::get('featured_image');
            $data['post_media_id'] = Input::get('media_file');
            $data['post_editor_id'] = Input::get('post_editor');
            $data['post_author_id'] = Input::get('post_author');
            $data['post_slug'] = Input::get('post_title');
            $data['post_type'] = (Input::get('post_type')) ? Input::get('post_type') : 'post';
            $data['site'] = $this->conn;

            if (Input::has('datepicker') && Input::get('post_status') == 6) {
                $data['post_published_date'] = date("Y-m-d H:i:s", strtotime(Input::get('datepicker')));
            } else {
                if (Input::get('post_status') == 6) {
                    $data['post_status'] = 1;
                    $data['post_published_date'] = date("Y-m-d H:i:s");
                } elseif (Input::get('post_status') == 1) {
                    $data['post_published_date'] = date("Y-m-d H:i:s");
                }
            }

            $post = new Post;
            $post->fill($data);
            $post->save();
            $post_id = $post->post_id;

            $tags = explode(',', Input::get('tags'));
            array_pop($tags);

            $related_posts = explode(',', Input::get('related-posts'));
            array_pop($related_posts);

            $linked_posts = explode(',', Input::get('linked-posts'));
            array_pop($linked_posts);

            $photos = explode(',', Input::get('photos'));
            array_pop($photos);

            // cats
            if (count(Input::get('cats'))) {
                $post->categories()->sync(Input::get('cats'));
            }

            // tags
            if (count($tags)) {
                foreach ($tags as $tag) {
                    $tagObj = Tag::firstOrCreate(['tag_name' => $tag, 'tag_slug' => create_slug($tag)]);

                    // insert posts tags
                    $post->tags()->attach($tagObj->tag_id);
                }
            }

            // related posts
            if (count($related_posts)) {
                $post->relatedPosts()->sync($related_posts);
            }

            // linked posts
            if (count($linked_posts)) {
                $post->linkedPosts()->sync($linked_posts);
            }

            // galleries
            if (Input::get('post_gallery')) {
                $post->galleries()->sync(Input::get('post_gallery'));
            }

            // sets
            if (count(Input::get('post_group'))) {
                $post->sets()->sync(Input::get('post_group'));
            }

            // photos
            if (count($photos)) {
                foreach ($photos as $photo) {

                    if (!$post->photos->contains($photo)) {
                        $photo_title = Input::get('image_text_' . $photo);
                        $post->photos()->attach($photo, ['media_title' => $photo_title]);
                    }
                }
            }

            //khalid-topics
            if (Input::get('topics')) {
                $topicsArray = explode(',', Input::get('topics'));
                $post->topics()->sync($topicsArray);
            }
        }

        unset($post, $tags, $photos, $linked_posts, $related_posts, $data);

        Session::flash('message', 'success');
        return Redirect::to(ADMIN . '/posts/' . $post_id . '/edit?cat_id=' . $curr_cat . '&post_type=' . $post_type);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id) {

        $relations = ['galleries', 'tags', 'categories', 'photos', 'relatedPosts',
            'linkedPosts', 'history', 'topics', 'liveblogging', 'comments', 'sets',
            'notes', 'media', 'image', 'topics', 'photos', 'meta'];

        $post = Post::with($relations)->find($id);

        if (!$post) {
            return Redirect::to(ADMIN . '/posts');
        }

        if ($post->seen == '0') {
            $post->seen = 1;
        }
        $post->opened_date = date("Y-m-d H:i:s");
        $post->save();

        $filePath = getcwd() . DIRECTORY_SEPARATOR . "opened" . DIRECTORY_SEPARATOR . $id . ".json";
        file_put_contents($filePath, Auth::user()->first_name . " " . Auth::user()->last_name);

        if (Input::get('post_type') == 'article') {
            $this->data['authors'] = User::whereHas('role', function($q) {
                                $q->where('role_slug', '=', 'author');
                            })
                            ->orderBy('id', 'DESC')->get();
        } else {

            $this->data['editors'] = User::whereHas('role.rolePermissions', function($q) {
                                $q->where('permission', '=', 'posts.create')
                                ->orWhere('permission', '=', 'posts.edit');
                            })
                            ->orWhereHas('permissions', function($q) {
                                $q->where('permission', '=', 'posts.create')
                                ->orWhere('permission', '=', 'posts.edit');
                            })
                            ->orderBy('id', 'DESC')->get();
        }

        $this->data['cats'] = Category::whereSite($this->conn)->get()->toArray();
        $this->data['most_cats'] = Category::with('posts')->whereSite($this->conn)->take('5')->get();
        $this->data['most_tags'] = Tag::with('posts')->take('5')->get();
        $this->data['galleries'] = Gallery::all();
        $this->data['sets'] = Set::all();

        $post_tags = null;
        foreach ($post->tags as $tag) {
            $post_tags .= $tag->tag_name . ",";
        }

        $post_photos = null;
        foreach ($post->photos as $photo) {
            $post_photos .= $photo->media_id . ",";
        }

        $post_related = null;
        foreach ($post->relatedPosts as $related) {
            $post_related .= $related->post_id . ",";
        }

        $post_linked = null;
        foreach ($post->linkedPosts as $linked) {
            $post_linked .= $linked->post_id . ",";
        }

        if ($post->mongo_id) {
            $this->data['mongo_post'] = MongoPosts::where('id', (int) $post->mongo_id)->first();
        }

        $this->data['post'] = $post;
        $this->data['post_tags'] = $post_tags;
        $this->data['post_photos'] = $post_photos;
        $this->data['post_related'] = $post_related;
        $this->data['post_linked'] = $post_linked;

        // calculate seo results
        $amseo = new Amseo();
        $this->data['seo_results'] = $amseo->linkdex_output($post);
        $this->data['publish_seo'] = $amseo->publish_box();

        unset($relations, $post, $post_tags, $post_photos, $post_photos, $post_linked, $amseo);
        return View::make('posts::post')->with($this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id) {

        $rules = array(
            'post_title' => 'required|unique:posts,post_title,' . $id . ',post_id',
            'tags' => 'required',
        );

        // run the validation rules on the inputs from the form
        $validator = Validator::make(Input::all(), $rules);

        $validator->setCustomMessages(Lang::get('admin::validation'));

        // if the validator fails, redirect back to the form
        if ($validator->fails()) {
            return Redirect::back()
                            ->withErrors($validator) // send back all errors to the login form
                            ->withInput(); // send back the input (not the password) so that we can repopulate the form
        } else {
            $post = Post::find($id);

            $saveandclose = Input::get('saveandclose');
            $curr_cat = Input::get('curr_cat');
            $post_type = Input::get('post_type');

            $data = Input::all();
            $data['post_image_id'] = Input::get('featured_image');
            $data['post_media_id'] = Input::get('media_file');
            $data['post_editor_id'] = Input::get('post_editor');
            $data['post_author_id'] = Input::get('post_author');

//            if (User::can('posts.unpublish') && User::can('posts.published')) {
//                $data['post_status'] = (Input::get('post_status')) ? Input::get('post_status') : 0;
//            }
            // post desked
            if (User::can('posts.reviewed')) {
                $data['post_desked'] = (Input::get('post_desked')) ? Input::get('post_desked') : 0;
            }

            // post submitted
            if (User::can('posts.pending')) {
                $data['post_submitted'] = (Input::get('post_submitted')) ? Input::get('post_submitted') : 0;
            }

            if (User::can('posts.close_post')) {
                $data['post_closed'] = (Input::get('post_closed')) ? Input::get('post_closed') : 0;
            }
            $data['translated'] = (Input::get('translated')) ? Input::get('translated') : 0;
            $data['image_hide'] = (Input::get('image_hide')) ? Input::get('image_hide') : 0;

            // post status
            if (User::can('posts.published')) {
                if (Input::has('datepicker') && Input::has('scheduled')) {
                    $data['post_published_date'] = date("Y-m-d H:i:s", strtotime(Input::get('datepicker')));
                    $data['post_status'] = 6;
                } else {
                    if (Input::get('post_status') == 1) {
                        $data['post_status'] = 1;
                        if ($post->post_status != 1)
                            $data['post_published_date'] = date("Y-m-d H:i:s");
                    } else if (User::can('posts.unpublish')) {
                        $data['post_status'] = 0;
                        $data['post_published_date'] = null;
                    } elseif ($post->post_status != 1) {
                        $data['post_status'] = 0;
                        $data['post_published_date'] = null;
                    }
                }
            }

            $post->fill($data);
            Event::fire('post.updating', array($post));
            $post->save();
            Event::fire('post.updated', array($post));

            $tags = explode(',', Input::get('tags'));
            array_pop($tags);

            $related_posts = explode(',', Input::get('related-posts'));
            array_pop($related_posts);

            $linked_posts = explode(',', Input::get('linked-posts'));
            array_pop($linked_posts);

            $photos = explode(',', Input::get('photos'));
            array_pop($photos);

            // cats
            if (count(Input::get('cats'))) {
                $post->categories()->sync(Input::get('cats'));
            } else {
                $post->categories()->detach();
            }

            // tags
            $post->tags()->detach();
            if (count($tags)) {
                foreach ($tags as $tag) {
                    $tagObj = Tag::firstOrCreate(['tag_name' => $tag, 'tag_slug' => create_slug($tag)]);
                    // insert posts tags
                    $post->tags()->attach($tagObj->tag_id);
                }
            }

            // related posts
            if (count($related_posts)) {
                $post->relatedPosts()->sync($related_posts);
            } else {
                $post->relatedPosts()->detach();
            }

            // linked posts
            if (count($linked_posts)) {
                $post->linkedPosts()->sync($linked_posts);
            } else {
                $post->linkedPosts()->detach();
            }

            // galleries
            if (Input::get('post_gallery')) {
                $post->galleries()->sync([Input::get('post_gallery')]);
            } else {
                $post->galleries()->detach();
            }

            // sets
            if (count(Input::get('post_group'))) {
                $post->sets()->sync(Input::get('post_group'));
            } else {
                $post->sets()->detach();
            }

            // photos
            $post->photos()->detach();
            if (count($photos)) {
                foreach ($photos as $photo) {
                    $photo_title = Input::get('image_text_' . $photo);
                    $post->photos()->attach($photo, ['media_title' => $photo_title]);
                }
            }

            // post meta
            $meta = Input::get('meta');
            $meta['robots_advanced'] = (Input::get('robots_advanced')) ? implode(',', Input::get('robots_advanced')) : '';
            $meta['canonical_url'] = (substr($meta['canonical_url'], 0, 4) != 'http' && $meta['canonical_url'] != '') ? "http://" . $meta['canonical_url'] : $meta['canonical_url'];
            $meta['amseo_redirect'] = (substr($meta['amseo_redirect'], 0, 4) != 'http' && $meta['amseo_redirect'] != '') ? "http://" . $meta['amseo_redirect'] : $meta['amseo_redirect'];
            if ($post->meta) {
                // update meta
                $post->meta()->update($meta);
            } else {
                // create meta
                $meta['amseo_redirect'] = 1;
                $meta = new PostMeta($meta);
                $meta->post()->associate($post);
                $meta->save(); // or $post->meta()->create($meta);
            }

            //khalid-topics

            if (Input::get('topics')) {
                $topicsArray = is_array(Input::get('topics')) ? Input::get('topics') : explode(',', Input::get('topics'));
                // dd($topicsArray);
                $post->topics()->sync($topicsArray);
            } else {
                $post->topics()->sync(array());
            }

            if (Input::get('notify')) {
                $notifyStr = Input::get('notify');
                $notifyArray = explode(',', $notifyStr);
                $post->notifyUsers()->sync($notifyArray);
            } else {
                $post->notifyUsers()->sync(array());
            }

            if (!$post->shortUrl) {
                Shortener::createForPost($post);
            }

            Session::flash('message', 'success');

            unset($tags, $photos, $linked_posts, $related_posts, $data);

            if ($saveandclose) {
                // calculate seo results
                $amseo = new Amseo();
                $this->data['seo_results'] = $amseo->linkdex_output($post);
                $this->data['publish_seo'] = $amseo->publish_box();
                unset($post);
                return Redirect::to(ADMIN . '/posts?cat_id=' . $curr_cat);
            } else {
                unset($post);
                return Redirect::to(ADMIN . '/posts/' . $id . '/edit?cat_id=' . $curr_cat . '&post_type=' . $post_type);
            }
        }
    }

    /**
     * Trash the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function changeStatus($id = 0, $status = 0) {
        $checks = Input::get('check');
        $stat = Input::get('action');

        if ($id) {
            if ($status == 'res') {
                $status = 0;
            }
            $post = Post::find($id);
            $post->post_status = $status;
            $post->save();

            unset($post, $checks, $stat, $status, $id);
            return Redirect::to(ADMIN . '/posts');
        } else {
            if ($stat == '0') {
                Post::destroy($checks);
            } else {
                if ($stat == 'res') {
                    $stat = 0;
                }
                $posts = Post::whereIn('post_id', $checks)->get();
                foreach ($posts as $key => $post) {
                    $post->post_status = $stat;
                    $post->save();
                }
            }

            unset($post, $checks, $stat, $status, $id);
            return Redirect::back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id) {
        Post::destroy($id);
        return Redirect::back();
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function new_slug() {
        $id = Input::get('id');

        $slug = create_slug(Input::get('slug'));
        $isUnique = Post::where('post_slug', '=', $slug)->where('post_id', '!=', $id)->first();
        while ($isUnique !== NULL) {
            $slug = $slug . '-' . rand(0, 99999);
            $isUnique = Post::where('post_slug', '=', $slug)->where('post_id', '!=', $id)->first();
        }

        Post::where('post_id', $id)->update(['post_slug' => $slug]);

        echo json_encode($slug);
        exit;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function ajax_autocomplete() {
        $q = Input::get('q');
        $id = Input::get('id');

        $query = Post::where('post_title', 'LIKE', '%' . $q . '%')->whereSite($this->conn)->wherePostStatus(1);
        if ($id) {
            $query->where('post_id', '!=', $id);
        }

        $posts = $query->orderBy('post_published_date', 'desc')->take(50)->get();

        if (count($posts)) {
            echo json_encode($posts);
        } else {
            echo json_encode('notfound');
        }

        exit;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function ajax_add_note() {

        $note_content = Input::get('note_content');
        $post_id = Input::get('post_id');
        $user_id = Auth::user()->id;
        $user_name = Auth::user()->first_name . ' ' . Auth::user()->last_name;

        $note = new Note(['note_content' => $note_content, 'note_author' => $user_name, 'note_post_id' => $post_id, 'note_user_id' => $user_id]);
        $note->save();

        $id = $note->note_id;
        unset($note);

        //Event::fire('note.created', array($id));
        if (DIRECTION == 'rtl') {
            $style = 'margin-right: 0;';
            $date = arabic_date(date('Y-m-d H:i:s'));
        } else {
            $style = 'margin-left: 0;';
            $date = date('M d, Y') . ' @ ' . date('h:i a');
        }

        echo $output = '<div class="feed-element">
                            <div class="media-body ">
                                <small class="pull-right"><a href="' . URL::to(ADMIN . '/users/' . $user_id) . '" title="">' . $user_name . '</a></small>
                                <strong>' . $note_content . '</strong><br>
                                <small class="text-muted">' . $date . '</small>
                            </div>
                        </div>';


        exit;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function ajax_tagcomplete() {
        $q = Input::get('q');

        $tags = Tag::where('tag_name', 'LIKE', '%' . $q . '%')->take(50)->get();

        if (count($tags)) {
            echo json_encode($tags);
        } else {
            echo json_encode('notfound');
        }

        exit;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function leave_post() {
        $id = Input::get('id');
        $filePath = getcwd() . DIRECTORY_SEPARATOR . "opened" . DIRECTORY_SEPARATOR . $id . ".json";
        unlink($filePath);
        exit;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function revision_post() {

        $post_id = Input::get('post_id');

        $post = Post::with('revisions', 'revisions.user')->find($post_id);
        $revisions = $post->revisions;

        unset($post, $post_id);
        echo json_encode($revisions);

        exit;
    }

    /* Display a listing of the resource.
     *
     * @return Response
     */

    public function publish_posts() {

        $now = date("Y-m-d H:i:s");
        $posts = Post::where('post_status', 6)->where('post_published_date', '<=', $now)->get();
        unset($now);

        foreach ($posts as $post) {
            $post->post_status = 1;
            $post->save();
            unset($post);
        }
        unset($posts);
    }

    /* Display a listing of the resource.
     *
     * @return Response
     */

    public function pin() {
        $post_id = Input::get('post_id');
        $pin = (Input::get('pin')) ? 1 : 0;

        $post = Post::find($post_id);
        $post->pin = $pin;
        $post->save();

        echo json_encode('');

        unset($post_id, $post);
        exit;
    }

    public function update_post_title() {
        $id = Input::get('id');
        $title = trim(Input::get('title'));

        // check if title found before
        $count = Post::where('post_title', $title)->where('post_id', '!=', $id)->count();
        if ($count) {
            echo json_encode('found');
            exit;
        }
    }

    public function quick_save() {
        $id = Input::get('id');
        $status = (Input::get('status')) ? 1 : 0;
        $title = trim(Input::get('title'));
        $slug = trim(Input::get('slug'));
        $tags = explode(',', Input::get('tags'));
        $cats = Input::get('cats');
        $catsArr = array();
        $tagsArr = array();
        foreach ($cats as $key => $value) {
            $catsArr[] = $value['id'];
        }

        $post = Post::find($id);
        $post->post_title = $title;
        $post->post_slug = $slug;

        // post desked
        if (User::can('posts.reviewed')) {
            $post->post_desked = (Input::get('desked')) ? Input::get('desked') : 0;
        }

        // post submitted
        if (User::can('posts.pending')) {
            $post->post_submitted = (Input::get('submitted')) ? Input::get('submitted') : 0;
        }

        // post status
        if (User::can('posts.published')) {
            if (Input::has('datepicker') && Input::has('scheduled')) {
                $post->post_published_date = date("Y-m-d H:i:s", strtotime(Input::get('date')));
                $post->post_status = 6;
            } else {
                if ($status == 1) {
                    if ($post->post_status != 1)
                        $post->post_published_date = date("Y-m-d H:i:s");
                    $post->post_status = 1;
                } else if (User::can('posts.unpublish')) {
                    $post->post_status = 0;
                    $post->post_published_date = null;
                } elseif ($post->post_status != 1) {
                    $post->post_status = 0;
                    $post->post_published_date = null;
                }
            }
        }

        // cats
        if (count($catsArr)) {
            $post->categories()->sync($catsArr);
        } else {
            $post->categories()->detach();
        }

        // tags
        $post->tags()->detach();
        if (count($tags)) {
            foreach ($tags as $tag) {
                if (trim($tag)) {
                    $tagObj = Tag::firstOrCreate(['tag_name' => $tag, 'tag_slug' => create_slug($tag)]);
                    $tagsArr[] = ['id' => $tagObj->tag_id, 'name' => $tagObj->tag_name];
                    // insert posts tags
                    $post->tags()->attach($tagObj->tag_id);
                }
            }
        }

        $post->save();

        $stat = '';
        if ($post->post_status == 1) {
            $stat = Lang::get('posts::posts.published');
        } elseif ($post->post_status == 6) {
            $stat = Lang::get('posts::posts.scheduled');
        } elseif ($post->post_status == 4) {
            $stat = Lang::get('posts::posts.draft');
        } elseif ($post->post_status == 5) {
            $stat = Lang::get('posts::posts.trashed');
        } elseif ($post->post_desked == 1) {
            $stat = Lang::get('posts::posts.desked');
        } elseif ($post->post_submitted == 1) {
            $stat = Lang::get('posts::posts.submitted');
        } elseif (!$post->post_submitted && !$post->post_desked && $post->post_status == 0) {
            $stat = Lang::get('posts::posts.new');
        }

        echo json_encode(['status' => $post->post_status, 'slug' => $post->post_slug, 'tags' => $tagsArr, 'stat' => $stat]);
        exit;
    }

    public function add_manshet() {
        $post_id = Input::get('post_id');
        $title = Input::get('title');
        $link = urldecode(Input::get('link'));
        $id = Input::get('id');

        if ($id) {
            $post = Post::find($post_id);
            $manshet = Liveblogging::find($id);
            $manshet->title = $title;
            $manshet->link = $link;
            $post->liveblogging()->save($manshet);

            $date = $manshet->date;
        } else {
            $post = Post::find($post_id);
            $manshet = new Liveblogging(['title' => $title, 'link' => $link]);
            $post->liveblogging()->save($manshet);

            $id = $manshet->id;
            $date = $manshet->date;
        }

        unset($post_id, $title, $link, $post, $manshet);
        echo json_encode([$id, date("h:i", strtotime($date))]);

        exit;
    }

    public function delete_manshet() {
        $id = Input::get('id');

        Liveblogging::destroy($id);

        echo json_encode('');

        exit;
    }

    /* Display a listing of the resource.
     *
     * @return Response
     */

    public function new_post() {
        $data = Input::all();

        $data['post_status'] = 0;
        $data['post_desked'] = 0;
        $data['post_submitted'] = 0;
        $data['post_closed'] = 0;
        $data['translated'] = 0;
        $data['image_hide'] = 0;
        $data['post_format'] = 1;
        $data['post_slug'] = Input::get('post_title');
        $data['site'] = $this->conn;
        $data['post_type'] = (Input::get('post_type')) ? Input::get('post_type') : 'post';

        // check if title found before
        $count = Post::where('post_title', Input::get('post_title'))->count();
        if ($count) {
            echo json_encode('found');
            exit;
        }

        // insert post

        $post = new Post;
        $post->fill($data);
        $post->save();
        $post_id = $post->post_id;

        // cats & blocks
        if (Input::get('cat_id')) {
            // insert posts categories
            $post->categories()->attach(Input::get('cat_id'));
        }

        $slug = $post->post_slug;

        unset($post, $count, $data);
        echo json_encode(["id" => $post_id, "slug" => $slug]);

        exit;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function order_post() {
        $cat_id = Input::get('cat_id');
        $block_slug = Input::get('block_slug');
        $post_id = Input::get('post_id');
        $lang = App::getLocale();
        $arr = array();
        $posts = array();
        $block_name = '';

        if ($block_slug) {
            $block = Block::get_block(['block_slug' => $block_slug, 'conn' => $this->conn]);
        } else {
            $cat_slug = Category::get_cat_slug(['cat_id' => $cat_id, 'conn' => $this->conn]);
            $block_slug = 'featured-' . $cat_slug;
            $block = Block::get_block(['block_slug' => $block_slug, 'conn' => $this->conn]);
        }

        if ($block->count()) {
            $block = $block->first();
            $block_id = $block->block_id;
            $block_limit = $block->max_posts;
            $block_name = $block->block_name;
            $block_id = $block->block_id;
            $arr = get_block_posts($block_id, $post_id, $block_limit);
            if (count($arr)) {
                $posts = Post::get_block_posts($arr, $lang, $this->conn);
            }
            echo json_encode(['block_name' => $block_name, 'block_id' => $block_id, 'posts' => $posts]);
        } else {
            $block_name = str_replace('-', ' ', $block_slug);
            $block_id = Block::add_block(['name' => $block_name, 'slug' => $block_slug, 'conn' => $this->conn]);
            create_block_file($block_id);
            echo json_encode(['block_name' => $block_name, 'block_id' => $block_id, 'posts' => $posts]);
        }

        exit;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function save_order() {


        $block_id = Input::get('block_id');
        $post_id = Input::get('post_id');
        $posts = (array) json_decode(Input::get('posts'));

        // add block posts
        foreach ($posts as $key => $post) {
            $block_post = Block::get_post_block(['post_id' => $post, 'block_id' => $block_id, 'conn' => $this->conn]);
            if (!$block_post->count()) {
                Block::add_post_blocks(['post_id' => $post, 'block_id' => $block_id, 'conn' => $this->conn]);
            }
        }

        save_block($block_id, $posts);

        exit;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function save_move() {


        $cat_id = Input::get('cat_id');
        $from_cat = Input::get('from_cat');
        $post_id = Input::get('post_id');
        $target_conn = Input::get('conn');
        $lang = App::getLocale();

        $from_cat = Category::get_cat_slug(['cat_id' => $from_cat]);

        $post = Post::get_moved_post(['post_id' => $post_id, 'conn' => $this->conn]);

        $created_date = date("Y-m-d H:i:s");
        $post_type = $post->post_type;
        $post_title = $post->post_title;
        $post_user_id = $post->post_user_id;
        $post_author_id = $post->post_author_id;
        $post_subtitle = $post->post_subtitle;
        $post_slug = $post->post_slug;
        $post_status = $post->post_status;
        $post_format = $post->post_format;
        $post_content = $post->post_content;
        $post_excerpt = $post->post_excerpt;
        $seo_title = $post->meta_title;
        $seo_keywords = $post->meta_keywords;
        $seo_description = $post->meta_description;
        $has_media = $post->hss_media;
        $youtube_link = $post->youtube_link;
        $image_id = $post->post_image_id;
        $media_id = $post->post_media_id;

        // insert post
        $id = Post::add_post(['post_created_date' => $created_date, 'post_user_id' => $post_user_id, 'post_author_id' => $post_author_id, 'has_media' => $has_media,
                    'post_image_id' => $image_id, 'post_media_id' => $media_id,
                    'post_type' => $post_type, 'post_format' => $post_format, 'post_slug' => $post_slug, 'post_status' => 0, 'post_desked' => 0, 'post_submitted' => 0,
                    'youtube_link' => $youtube_link, 'post_title' => $post_title,
                    'post_subtitle' => $post_subtitle, 'post_excerpt' => $post_excerpt,
                    'post_content' => $post_content, 'meta_title' => $seo_title,
                    'meta_keywords' => $seo_keywords, 'meta_description' => $seo_description, 'conn' => $target_conn, 'from_conn' => $this->conn, 'from_cat' => $from_cat
                    , 'from_slug' => $post_slug]);

        // insert tags
        $tags = Tag::get_post_tags(['post_id' => $post_id, 'conn' => $this->conn])->get();
        foreach ($tags as $key => $tag) {
            $check = Tag::get_tag_by_slug(['slug' => $tag->tag_slug, 'conn' => $target_conn])->first();
            if ($check) {
                Tag::add_post_tags(['post_id' => $id, 'tag_id' => $check->tag_id, 'conn' => $target_conn]);
            } else {
                $tag_id = Tag::add_tag(['name' => $tag->tag_name, 'slug' => $tag->tag_slug, 'conn' => $target_conn]);
                Tag::add_post_tags(['post_id' => $id, 'tag_id' => $tag_id, 'conn' => $target_conn]);
            }
        }

        Category::add_post_cats(['post_id' => $id, 'cat_id' => $cat_id, 'conn' => $target_conn]);
        $cat_slug = Category::get_cat_slug(['cat_id' => $cat_id, 'conn' => $target_conn]);
        $block = Block::get_block(['block_slug' => 'featured-' . $cat_slug, 'conn' => $target_conn]);
        if ($block->count()) {
            $block = $block->first();
            $block_id = $block->block_id;
            Block::add_post_blocks(['post_id' => $id, 'block_id' => $block_id, 'conn' => $target_conn]);
            append_block_post($block_id, $id, $target_conn);
        } else {
            $block_name = 'featured ' . $cat_slug;
            $block_slug = 'featured-' . $cat_slug;
            $block_id = Block::add_block(['name' => $block_name, 'slug' => $block_slug, 'conn' => $target_conn]);
            append_block_post($block_id, $id, $target_conn);
        }

        $block = Block::get_block(['block_slug' => 'featured', 'conn' => $target_conn]);
        if ($block->count()) {
            $block = $block->first();
            $block_id = $block->block_id;
            Block::add_post_blocks(['post_id' => $id, 'block_id' => $block_id, 'conn' => $target_conn]);
            append_block_post($block_id, $id, $target_conn);
        } else {
            $block_name = 'featured ' . $cat_slug;
            $block_slug = 'featured-' . $cat_slug;
            $block_id = Block::add_block(['name' => $block_name, 'slug' => $block_slug, 'conn' => $target_conn]);
            append_block_post($block_id, $id, $target_conn);
        }

        $block = Block::get_block(['block_slug' => 'breaking-news', 'conn' => $target_conn]);
        if ($block->count()) {
            $block = $block->first();
            $block_id = $block->block_id;
            Block::add_post_blocks(['post_id' => $id, 'block_id' => $block_id, 'conn' => $target_conn]);
            append_block_post($block_id, $id, $target_conn);
        } else {
            $block_name = 'featured ' . $cat_slug;
            $block_slug = 'featured-' . $cat_slug;
            $block_id = Block::add_block(['name' => $block_name, 'slug' => $block_slug, 'conn' => $target_conn]);
            append_block_post($block_id, $id, $target_conn);
        }

        echo json_encode($post_id);

        exit;
    }

    function download($media_path = false) {

        $link = uploads_url($media_path);
        $parts = explode("/", $media_path);
        $file = end($parts);

        if (strstr($media_path, "/")) {
            return copy($link, UPLOADS . "/" . $file);
        }
    }

    public function check_free() {

        $media_path = Input::get('media_path');

        $this->download($media_path);

        $parts = explode("/", $media_path);
        $filename = end($parts);

        Image::make(UPLOADS . "/" . $filename)
                ->resize(570, null, function ($constraint) {
                    $constraint->aspectRatio();
                    $constraint->upsize();
                })
                ->save(UPLOADS . "/free-" . $filename);

        if (strstr($media_path, "/")) {
            $parts = explode("/", $media_path);
            s3_save($parts[0] . "/" . $parts[1] . "/" . "free-" . $parts[2]);
        } else {
            s3_save("uploads/uploads/free-" . $filename);
        }

        @unlink("uploads/" . $filename);
        @unlink("uploads/free-" . $filename);
    }

    public function quickEdit($id) {
        $post = Post::find($id);
        $selTopics = Input::get('topics') ? explode(',', Input::get('topics')) : array();

        $post->topics()->sync($selTopics);

        if (Input::get('newTitle')) {
            DB::table('posts')->where('post_id', '=', $id)->update(array('post_title' => Input::get('newTitle')));
        }
        if (Input::get('newContent')) {
            $content = html_entity_decode(Input::get('newContent'));
            DB::table('posts')->where('post_id', '=', $id)->update(array('post_content' => $content));
        }
        DB::table('post_updates_history')->insert(
                array(
                    'post_id' => $id,
                    'user_id' => Auth::user()->id,
                )
        );

        $memcached = IlluminateSupportFacadesCache::driver('memcached');
        $cacheId = 'single_' . $post->post_id;
        if ($memcached->get($cacheId)) {
            $memcached->forget($cacheId);
        }
    }

}
