<?php

Route::group(array(
    "prefix" => ADMIN,
    "before" => "auth",
        ), function($route) {

    
    $route->group(array("prefix" => "posts"), function($route) {
        $route->post('check_free', 'PostController@check_free');
        $route->post('autocomplete', 'PostController@ajax_autocomplete');
        $route->post('tagcomplete', 'PostController@ajax_tagcomplete');
        $route->post('orderpost', 'PostController@order_post');
        $route->post('saveorder', 'PostController@save_order');
        $route->post('leavepost', 'PostController@leave_post');
        $route->post('savemove', 'PostController@save_move');
        $route->post('revisionpost', 'PostController@revision_post');
        $route->post('addpost', 'PostController@new_post');
        $route->post('pin', 'PostController@pin');
        $route->post('addmanshet', 'PostController@add_manshet');
        $route->post('deletemanshet', 'PostController@delete_manshet');
        $route->post('addcomment', 'CommentController@ajax_add_comment');
        $route->post('changecomment', 'CommentController@changeStatus');
        $route->post('addnote', 'PostController@ajax_add_note');
        $route->post('newSlug', 'PostController@new_slug');
        $route->get('quickHtml', "PostController@quick_edit_html");
        $route->post('updatepost', 'PostController@update_post_title');
        $route->post('quicksave', 'PostController@quick_save');
        
        $route->get('status/{id}/{status}', 'PostController@changeStatus');
        $route->post('status', 'PostController@changeStatus');
        $route->get('{id}/delete', 'PostController@destroy');
    });
    Route::resource('posts', 'PostController');
});

Route::get('posts/publishposts', "PostController@publish_posts");
