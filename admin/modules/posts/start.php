<?php

Menu::set("sidebar", function($menu) {

    $menu->item('news_options', trans("posts::posts.news_options"), "")
            ->order(8)
            ->icon("fa-cogs");
    

    if (User::can('posts')) {


        $menu->item('posts', trans("posts::posts.module"), URL::to(ADMIN . '/posts'))
                ->order(3)
                ->icon("fa-newspaper-o");
        
         $menu->item('posts.all_news', trans("grabber::grabber.sources"), URL::to(ADMIN . '/aggregator'));
        

        $menu->item('posts.all', trans("posts::posts.all_posts"), URL::to(ADMIN . '/posts'));


        foreach (Category::where('cat_parent', 0)->whereSite(Session::get('conn'))->get() as $cat) {
            $menu->item('posts.' . $cat->cat_id, $cat->cat_name, URL::to(ADMIN . '/posts?cat_id=' . $cat->cat_id));
        }
        
         $menu->item('posts.celebrities', trans("posts::posts.celebrities"), URL::to(ADMIN . '/posts?post_type=celebrity'));

        if (User::can('posts.control_article')) {
            $menu->item('posts.articles', trans("posts::posts.articles"), URL::to(ADMIN . '/posts?post_type=article'));
        }

       
    }
});


include __DIR__ . "/routes.php";
include __DIR__ . "/helpers.php";
