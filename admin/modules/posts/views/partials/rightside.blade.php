
<div class="panel colourable" style="border:none; background:inherit">

    <div class="panel-body" style="border:none; background:inherit; padding:0">
        <div class="ibox-content" style="margin-bottom:10px">

            <div class="form-group">
                <div class="input-group">
                    {!! Form::text('post_title', Input::old('post_title', $post->post_title), array('class' => 'form-control input-lg', 'id' => 'post_title', 'style' => '','placeholder' => Lang::get('posts::posts.enter_title'))) !!}
                    <span class="input-group-btn lg" > <span id="title_count" type="button" class="btn-lg btn btn-primary">{!!mb_strlen(Input::old('post_title', $post->post_title))!!}</span> </span>
                </div>
                <label for="post_title" class="error" style="display:none">{!!Lang::get('posts::posts.required')!!}</label>
            </div>
            <div class="inside" @if(!isset($post->post_id)) style="display: none" @endif>
                 <div id="edit-slug-box" class="hide-if-no-js">
                    <strong style="color: #8FAA69;">{!!Lang::get('posts::posts.permalink')!!}:</strong>
                    <span id="sample-permalink" style="font-weight: bold;" class="tooltip-demo">
                        {!! URL::to("/details") !!}/
                        <span id="editable-post-name" data-toggle="tooltip" data-placement="top" title="{!!Lang::get('posts::posts.permalink_info')!!}">
                            <input id="new-post-slug" style="display:none" value="{!!$post->post_slug!!}" type="text">
                            <span id="slug_name" style="background-color: #FFFBCC;" class="edit_slug">{!!$post->post_slug!!}</span>
                        </span>/
                    </span>
                    ‎&nbsp;<span id="edit-slug-buttons">
                        <a href="#" class="btn btn-primary btn-xs" id="permalink_ok" style="display:none">{!!Lang::get('posts::posts.ok')!!}</a> 
                        <a class="btn btn-default btn-xs" href="#" id="permalink_cancel" style="display:none">{!!Lang::get('posts::posts.cancel')!!}</a>
                        <a href="#post_name" class="btn btn-default btn-xs edit_slug" id="permalink_edit">{!!Lang::get('posts::posts.edit')!!}</a>
                    </span>
                    <span id="editable-post-name-full" style="display:none">{!!$post->post_slug!!}</span>
                </div>
            </div>

        </div>

        @if($post->mongo_id && trim($post->post_content) != '')
        <div class="ibox-content" style="margin-bottom:10px">
            <div class="note note-info" id="note_content" style="height:400px;">
                <div class="content full-height-scroll" style="padding: 0 10px; font-weight: bold">
                    <div class="feed-element" style="padding:0" id="mongo-content">{!!$post->post_content!!}</div>
                </div>
            </div>

        </div>
        @endif

        <div class="panel-group">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5> {!!Lang::get('posts::posts.excerpt')!!} </h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>

                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <div class="panel-body">
                        <div class="form-group" style="margin: 0;">
                            {!! Form::textarea('post_excerpt', Input::old('post_excerpt', $post->post_excerpt), array('class' => 'form-control', 'id' => 'post_excerpt', 'rows' => 3, 'style' => 'resize:vertical')) !!}
                            <div for="post_excerpt" class="jquery-validate-error help-block" style="display:none">{!!Lang::get('posts::posts.required')!!}</div>
                            <span style="margin-top: 10px;display: block;">{!!Lang::get('posts::posts.char_num')!!}: <span  id="excerpt_count">{!!mb_strlen(Input::old('post_excerpt', $post->post_excerpt))!!}</span> </span>
                        </div>  

                    </div>
                </div>
            </div>
        </div>

        <!-- / Javascript -->
        <div class="panel-group">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5> {!!Lang::get('posts::posts.related_posts')!!} </h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>

                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <div class="panel-body no-padding-vr">
                        <input type="hidden" id="related-temp">
                        <input type="hidden" id="related-posts" value="{{ (isset($post_related)) ? $post_related : '' }}" name="related-posts">
                        <div class="form-group" style="position:relative">
                            <div class="input-group">
                                <input type="text" class="form-control" id="related-text" @if(count($post->relatedPosts) == 10) disabled="disabled" @endif style="background-color: snow;" placeholder="{!!Lang::get('posts::posts.search_post')!!}" autocomplete="off">
                                       <span class="input-group-btn">
                                    <button class="btn btn-primary disabled" type="button" id="add-related-post">{!!Lang::get('posts::posts.add')!!}</button>
                                </span>
                            </div>
                            <div id="auto-wrap" style="height:101px; display: none">
                                <div class="feed-activity-list full-height-scroll" style="padding:0 10px"></div>
                            </div>
                            <div id="related-search" style="display: none">
                                <span style="font-style: italic;" >{!!Lang::get('posts::posts.more_character')!!}</span>
                            </div>
                        </div>

                        <div style="height:300px; @if(!count($post->relatedPosts)) display: none; @endif" id="related-add">
                            <ul class="todo-list small-list full-height-scroll" style="padding: 0 10px">
                                @if(count($post->relatedPosts))

                                @foreach($post->relatedPosts as $posts_related_one)

                                <li class="feed-element" style="margin-top:5px">
                                    <a class="check-link" href="#"><i data-id="{!!$posts_related_one->post_id!!}" class="fa fa-square-o"></i> </a>
                                    <span class="m-l-xs">{!!$posts_related_one->post_title!!}</span>
                                    <a class="label label-danger pull-right delete-related-post" href="#{!!$posts_related_one->post_id!!}"><i class="fa fa-times"></i></a>
                                </li>

                                @endforeach

                                @endif
                            </ul>
                        </div>
                        <div class="panel-footer clearfix" style="border-top: none; background: none; @if(!count($post->relatedPosts)) display: none; @endif">
                            <div class="pull-right">
                                <a href="#" class="btn btn-xs" id="clear-completed-tasks"><i class="fa fa-eraser text-success"></i> {!!Lang::get('posts::posts.clear_posts')!!}</a>
                            </div>
                        </div>
                    </div> <!-- / .panel-body -->

                </div>
            </div> <!-- / .panel -->
        </div>

        @if(@$mongo_post)
        <div class="panel-group col-md-12" style="margin-bottom: 0; padding:0">
            <div class="panel-group col-md-6" style="padding:0; @if(DIRECTION == 'rtl') padding-left:5px; @else padding-right:5px; @endif">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>{!!Lang::get('posts::posts.post_info')!!}</h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>

                            <a class="close-link">
                                <i class="fa fa-times"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content">
                        <div class="panel-body">

                            <div class="form-group">    
                                <p><i class="fa fa-clock-o"></i>&nbsp;&nbsp;&nbsp;&nbsp;{!!Lang::get('posts::posts.date')!!}:<strong> @if(DIRECTION == 'rtl') {!!arabic_date(@$mongo_post['date']->sec, 'mongo')!!} @else {!! date('M d, Y', @$mongo_post['date']->sec) !!} @ {!! date('h:i a', @$mongo_post['date']->sec) !!} @endif </strong>

                                </p>
                                @if(@$mongo_post['source_date'])
                                <p><i class="fa fa-clock-o"></i>&nbsp;&nbsp;&nbsp;&nbsp;{!!Lang::get('posts::posts.source_date')!!}:<strong> {!!@$mongo_post['source_date']!!} </strong></p>
                                @endif
                                @if(@$mongo_post['video']['source'] || @$mongo_post['audio']['source'] || @$mongo_post['gallery']) <p><i class="fa fa-th-large"></i>&nbsp;&nbsp;&nbsp;&nbsp;{!!Lang::get('posts::posts.post_type')!!}:<strong > @if(@$mongo_post['video']['source']) {!!Lang::get('posts::posts.video')!!} @elseif(@$mongo_post['audio']['source']) {!!Lang::get('posts::posts.audio')!!} @elseif(@$mongo_post['gallery']) {!!Lang::get('posts::posts.gallery')!!} @endif  </strong></p> @endif
                                <p><i class="fa fa-exchange"></i>&nbsp;&nbsp;&nbsp;&nbsp;{!!Lang::get('posts::posts.source')!!}:<strong > {!!@$mongo_post['source']['name']!!}  </strong></p>
                                <p><i class="fa fa-rss-square"></i>&nbsp;&nbsp;&nbsp;&nbsp;{!!Lang::get('posts::posts.provider')!!}:<strong > @if(@$mongo_post['provider'] == 0) {!!Lang::get('posts::posts.news_site')!!} @elseif(@$mongo_post['provider'] == 1) {!!Lang::get('posts::posts.facebook')!!} @elseif(@$mongo_post['provider'] == 2) {!!Lang::get('posts::posts.twitter')!!} @elseif(@$mongo_post['provider'] == 3) {!!Lang::get('posts::posts.youtube')!!} @elseif(@$mongo_post['provider'] == 4) {!!Lang::get('posts::posts.instagram')!!} @endif</strong></p>
                                @if(@$mongo_post['country'])
                                <p><i class="fa fa-flag"></i>&nbsp;&nbsp;&nbsp;&nbsp;{!!Lang::get('posts::posts.country')!!}:<strong > {!!@$mongo_post['country']['name']!!}  </strong></p>
                                @endif
                                @if(@$mongo_post['lang'])
                                <p><i class="fa fa-flag-checkered"></i>&nbsp;&nbsp;&nbsp;&nbsp;{!!Lang::get('posts::posts.language')!!}:<strong > {!!Lang::get('posts::posts.'.@$mongo_post['lang'])!!} </strong></p>
                                @endif
                                @if(@$mongo_post['video']['length'])
                                <p><i class="fa fa-video-camera"></i>&nbsp;&nbsp;&nbsp;&nbsp;{!!Lang::get('posts::posts.duration')!!}:<strong > {!!gmdate("i:s", @$mongo_post['video']['length'])!!} {!!Lang::get('posts::posts.minute_short')!!}</strong></p>
                                @endif
                                @if(@$mongo_post['audio']['length'])
                                <p><i class="fa fa-volume-up"></i>&nbsp;&nbsp;&nbsp;&nbsp;{!!Lang::get('posts::posts.duration')!!}:<strong > {!!gmdate("i:s", @$mongo_post['audio']['length'])!!} {!!Lang::get('posts::posts.minute_short')!!}</strong></p>
                                @endif
                                @if(@$mongo_post['author'])
                                <p><i class="fa fa-user"></i>&nbsp;&nbsp;&nbsp;&nbsp;{!!Lang::get('posts::posts.author')!!}:<strong > {!!@$mongo_post['author']!!}  </strong></p>
                                @endif
                                <p><i class="fa fa-external-link"></i>&nbsp;&nbsp;&nbsp;&nbsp;{!!Lang::get('posts::posts.link')!!}: <a href="{!!urldecode(@$mongo_post['link'])!!}" target="_blank" style="word-wrap: break-word;">{!!urldecode(@$mongo_post['link'])!!}</a>  </p>

                            </div>
                        </div>


                    </div>
                </div>
            </div>  

            <div class="panel-group col-md-6" style="padding:0; @if(DIRECTION == 'rtl') padding-right:5px; @else padding-left:5px; @endif">

                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>{!!Lang::get('posts::posts.social')!!}</h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>

                            <a class="close-link">
                                <i class="fa fa-times"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content" style="@if(@$mongo_post['provider'] == 3) height: 265px; @elseif(@$mongo_post['provider'] == '0') height: 300px; @else height: 190px; @endif overflow: hidden">
                        <div class="form-group">  

                            <script>
                                var data = new Array();
<?php
$engagement = 0;
?>

                                @if (@$mongo_post['provider'] == 0)
<?php
$total = $post['shares'];
$facebook_shares = 0;
$twitter_shares = 0;
$google_shares = 0;
$linkedin_shares = 0;
if ($total != 0) {
    $facebook_shares = round(($post['facebook_shares'] / $total) * 100, 2);
    $twitter_shares = round(($post['twitter_shares'] / $total) * 100, 2);
    $google_shares = round(($post['google_shares'] / $total) * 100, 2);
    $linkedin_shares = round(($post['linked_in_shares'] / $total) * 100, 2);
}
?>

                                data = [
                                { label: "{!!Lang::get('posts::posts.facebook_shares')!!}", value: {!!$facebook_shares!!} },
                                { label: "{!!Lang::get('posts::posts.twitter_shares')!!}", value: {!!$twitter_shares!!} },
                                { label: "{!!Lang::get('posts::posts.google_shares')!!}", value: {!!$google_shares!!} },
                                { label: "{!!Lang::get('posts::posts.linkedin_shares')!!}", value: {!!$linkedin_shares!!} },
                                ];
                                        @elseif(@$mongo_post['provider'] == 1)
<?php
$total = $post['shares'] + $post['comments'] + $post['likes'];
$shares = 0;
$fcomments = 0;
$likes = 0;

if ($total != 0) {
    $shares = round(($post['shares'] / $total) * 100, 2);
    $fcomments = round(($post['comments'] / $total) * 100, 2);
    $likes = round(($post['likes'] / $total) * 100, 2);
}
?>

                                data = [
                                { label: "{!!Lang::get('posts::posts.facebook_shares')!!}", value: {!!$shares!!} },
                                { label: "{!!Lang::get('posts::posts.facebook_comments')!!}", value: {!!$fcomments!!} },
                                { label: "{!!Lang::get('posts::posts.facebook_likes')!!}", value: {!!$likes!!} },
                                ];
                                        @elseif(@$mongo_post['provider'] == 2)
<?php
$total = $post['retweets'] + $post['favorites'];
$retweets = 0;
$favorites = 0;

if ($total != 0) {
    $retweets = round(($post['retweets'] / $total) * 100, 2);
    $favorites = round(($post['favorites'] / $total) * 100, 2);
}
?>

                                data = [
                                { label: "{!!Lang::get('posts::posts.retweets')!!}", value: {!!$retweets!!} },
                                { label: "{!!Lang::get('posts::posts.favorites')!!}", value: {!!$favorites!!} },
                                ];
                                        @elseif(@$mongo_post['provider'] == 3)
<?php
$total = $post['likes'] + $post['dislikes'];
$likes = 0;
$dislikes = 0;

if ($total != 0) {
    $likes = round(($post['likes'] / $total) * 100, 2);
    $dislikes = round(($post['dislikes'] / $total) * 100, 2);
}
?>

                                data = [
                                { label: "{!!Lang::get('posts::posts.likes')!!}", value: {!!$likes!!} },
                                { label: "{!!Lang::get('posts::posts.dislikes')!!}", value: {!!$dislikes!!} },
                                ];
                                        @elseif(@$mongo_post['provider'] == 4)
<?php
$total = $post['likes'] + $post['comments'];
$likes = 0;
$icomments = 0;

if ($total != 0) {
    $likes = round(($post['likes'] / $total) * 100, 2);
    $icomments = round(($post['comments'] / $total) * 100, 2);
}
?>

                                data = [
                                { label: "{!!Lang::get('posts::posts.likes')!!}", value: {!!$likes!!} },
                                { label: "{!!Lang::get('posts::posts.comments')!!}", value: {!!$icomments!!} },
                                ];
                                        @endif

<?php
if (@$mongo_post['provider'] != 0) {
    $engagement = number_format(@$mongo_post['engagement'] * 100, 4);
} else {
    $engagement = @$mongo_post['engagement'];
}
if ($engagement >= 1) {
    $rate = Lang::get('posts::posts.good');
    $class = "success";
} elseif ($engagement >= .5 && $engagement <= .99) {
    $rate = Lang::get('posts::posts.average');
    $class = "warning";
} else {
    $rate = Lang::get('posts::posts.low');
    $class = "danger";
}
?>

                                @if ($total)
                                        $(function() {
                                        Morris.Donut({
                                        element: 'hero-donut',
                                                data: data,
                                                colors: ['#87d6c6', '#54cdb4', '#1ab394'],
                                                resize: false,
                                                labelColor: '#888',
                                                formatter: function (y) { return y + "%" }
                                        });
                                        });
                                        @endif
                            </script>
                            <div class="pull-left" @if($total) style="width:58%" @else style="width:100%" @endif>
                                 <ul class="list-group clear-list m-t">

                                    @if(@$mongo_post['provider'] == 0)
                                    <li class="list-group-item fist-item" ><span ><img src="{!!assets()!!}/images/1429117426_ShareThis_Social-Network-Communicate-Page-Curl-Effect-Circle-Glossy-Shadow-Shine.png">&nbsp;&nbsp;{!!Lang::get('posts::posts.facebook_shares')!!}:</span> <span class="label label-pa-purple pull-right">{!!@$mongo_post['facebook_shares']!!}</span> </li>
                                    <li class="list-group-item" ><span ><img src="{!!assets()!!}/images/1429034415_bubble-16.png">&nbsp;&nbsp;{!!Lang::get('posts::posts.facebook_comments')!!}:</span> <span class="label label-pa-purple pull-right">{!!@$mongo_post['facebook_comments']!!}</span> </li>
                                    <li class="list-group-item" ><span ><img src="{!!assets()!!}/images/1429034310_like-16.png">&nbsp;&nbsp;{!!Lang::get('posts::posts.facebook_likes')!!}:</span> <span class="label label-pa-purple pull-right">{!!@$mongo_post['facebook_likes']!!}</span> </li>
                                    <li class="list-group-item" ><span ><img src="{!!assets()!!}/images/1429035359_twitter_circle_color-16.png">&nbsp;&nbsp;{!!Lang::get('posts::posts.twitter_shares')!!}:</span> <span class="label label-pa-purple pull-right">{!!@$mongo_post['twitter_shares']!!}</span> </li>
                                    <li class="list-group-item" ><span ><img src="{!!assets()!!}/images/1429035372_circle-google-plus-16.png">&nbsp;&nbsp;{!!Lang::get('posts::posts.google_shares')!!}:</span> <span class="label label-pa-purple pull-right">{!!@$mongo_post['google_shares']!!}</span> </li>
                                    <li class="list-group-item" ><span ><img src="{!!assets()!!}/images/1429035388_linkedin_circle_color-16.png">&nbsp;&nbsp;{!!Lang::get('posts::posts.linkedin_shares')!!}:</span> <span class="label label-pa-purple pull-right">{!!@$mongo_post['linked_in_shares']!!}</span> </li>
                                    @elseif(@$mongo_post['provider'] == 1)
                                    <li class="list-group-item fist-item" ><span ><img src="{!!assets()!!}/images/1429117426_ShareThis_Social-Network-Communicate-Page-Curl-Effect-Circle-Glossy-Shadow-Shine.png">&nbsp;&nbsp;{!!Lang::get('posts::posts.facebook_shares')!!}:</span> <span class="label label-pa-purple pull-right">{!!@$mongo_post['shares']!!}</span> </li>
                                    <li class="list-group-item" ><span ><img src="{!!assets()!!}/images/1429034415_bubble-16.png">&nbsp;&nbsp;{!!Lang::get('posts::posts.facebook_comments')!!}:</span> <span class="label label-pa-purple pull-right">{!!@$mongo_post['comments']!!}</span> </li>
                                    <li class="list-group-item" ><span ><img src="{!!assets()!!}/images/1429034310_like-16.png">&nbsp;&nbsp;{!!Lang::get('posts::posts.facebook_likes')!!}:</span> <span class="label label-pa-purple pull-right">{!!@$mongo_post['likes']!!}</span> </li>
                                    @elseif(@$mongo_post['provider'] == 2)
                                    <li class="list-group-item fist-item" ><span ><img src="{!!assets()!!}/images/1429059314_Twitter_bird.png">&nbsp;&nbsp;{!!Lang::get('posts::posts.retweets')!!}:</span> <span class="label label-pa-purple pull-right">{!!@$mongo_post['retweets']!!}</span> </li>
                                    <li class="list-group-item" ><span ><img src="{!!assets()!!}/images/1429059386_applications-games.png">&nbsp;&nbsp;{!!Lang::get('posts::posts.favorites')!!}:</span> <span class="label label-pa-purple pull-right">{!!@$mongo_post['favorites']!!}</span> </li>
                                    @elseif(@$mongo_post['provider'] == 3)
                                    <li class="list-group-item fist-item" ><span ><img src="{!!assets()!!}/images/1429123922_like-01-16.png">&nbsp;&nbsp;{!!Lang::get('posts::posts.likes')!!}:</span> <span class="label label-pa-purple pull-right">{!!@$mongo_post['likes']!!}</span> </li>
                                    <li class="list-group-item" ><span ><img src="{!!assets()!!}/images/1429123943_dislike-01-16.png">&nbsp;&nbsp;{!!Lang::get('posts::posts.dislikes')!!}:</span> <span class="label label-pa-purple pull-right">{!!@$mongo_post['dislikes']!!}</span> </li>
                                    <li class="list-group-item" ><span ><img src="{!!assets()!!}/images/1429034415_bubble-16.png">&nbsp;&nbsp;{!!Lang::get('posts::posts.comments')!!}:</span> <span class="label label-pa-purple pull-right">{!!@$mongo_post['comments']!!}</span> </li>
                                    <li class="list-group-item" ><span ><img src="{!!assets()!!}/images/1429059386_applications-games.png">&nbsp;&nbsp;{!!Lang::get('posts::posts.favorites')!!}:</span> <span class="label label-pa-purple pull-right">{!!@$mongo_post['favorites']!!}</span> </li>
                                    <li class="list-group-item" ><span ><img src="{!!assets()!!}/images/1429123999_view-01-16.png">&nbsp;&nbsp;{!!Lang::get('posts::posts.views')!!}:</span> <span class="label label-pa-purple pull-right">{!!@$mongo_post['views']!!}</span> </li>
                                    @elseif(@$mongo_post['provider'] == 4)
                                    <li class="list-group-item fist-item" ><span ><img src="{!!assets()!!}/images/1429034310_like-16.png">&nbsp;&nbsp;{!!Lang::get('posts::posts.likes')!!}:</span> <span class="label label-pa-purple pull-right">{!!@$mongo_post['likes']!!}</span> </li>
                                    <li class="list-group-item" ><span ><img src="{!!assets()!!}/images/1429034415_bubble-16.png">&nbsp;&nbsp;{!!Lang::get('posts::posts.comments')!!}:</span> <span class="label label-pa-purple pull-right">{!!@$mongo_post['comments']!!}</span> </li>
                                    @endif
                                    <li class="list-group-item" ><span><i class="fa fa-line-chart"></i>&nbsp;&nbsp;{!!Lang::get('posts::posts.engagement_rate')!!}:</span> <span class="label label-pa-purple pull-right">{!!$engagement!!} @if(@$mongo_post['provider'] != 0)%@endif</span> </li>

                                </ul>

                            </div>

                            @if($total)
                            <div class="pull-right" style="width:40%;">
                                <div id="hero-donut" class="graph" style="margin-top:-90px"></div>
                            </div>
                            @endif
                        </div>
                    </div>
                </div>

            </div> 
        </div>
        @endif

        <div class="panel-group @if($post->post_format ==  5 || $post->post_format ==  4 || $post->post_format ==  3) col-md-6 @else col-md-4 @endif " style="padding:0; @if(DIRECTION == 'rtl') padding-left:5px; @else padding-right:5px; @endif" id="featured-wrapper">
            <div class="ibox float-e-margins" >
                <div class="ibox-title">
                    <h5> {!!Lang::get('posts::posts.featured_image')!!} </h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>

                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content" style="padding:0">
                    <div class="panel-body form-group" style="margin-bottom:0">
                        {!! Form::hidden('featured_image', $post->post_image_id, array('id' => 'featured_image')) !!}
                        <div  <?php if (!$post->post_image_id) { ?> style="display: none" <?php } ?> id="image_wrap">    
                            <a href="#" class="set-post-thumbnail">
                                <img width="100%" height="130px" src="{{{ $post->image ? $post->image->getImageURL('small') : assets("images/default.png") }}}" id="image_post">
                            </a>
                        </div>
                        <div style="padding:17px">
                            <a href="#" id="remove-post-thumbnail" <?php if (!$post->post_image_id) { ?> style="display: none" <?php } ?>>{!!Lang::get('posts::posts.remove_image')!!}</a>

                            <a href="#" class="set-post-thumbnail" id="set-post-thumbnail" <?php if ($post->post_image_id) { ?> style="display: none" <?php } ?>>{!!Lang::get('posts::posts.set_image')!!}</a>


                            <div for="featured_image" class="jquery-validate-error help-block" style="display:none">{!!Lang::get('posts::posts.required')!!}</div>
                            <div style=" padding: 7px 0"><label style="margin-bottom:0"><input type="checkbox" class="i-checks" name="image_hide" value="1" @if($post->image_hide) checked @endif> <span class="lbl">{!!Lang::get('posts::posts.image_hide')!!}</span></label></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="panel-group @if($post->post_format ==  5) col-md-6 @else col-md-4 @endif" style="padding:0; @if($post->post_format ==  4 || $post->post_format ==  3) display:none @endif" id="gallery-wrapper">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5> {!!Lang::get('posts::posts.galleries')!!} </h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>

                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <div class="panel-body" style="padding: 7px 7px @if(count($galleries)) 16px @else 21px @endif 7px;">


                        <div class="row form-group" style="margin-bottom:0">
                            <label class="@if(DIRECTION == 'rtl') col-sm-3 @else col-sm-5 @endif control-label" style="@if(DIRECTION == 'rtl') padding:0; @endif margin-top: 5px;">{!!Lang::get('posts::posts.choose_gallery')!!}: </label>
                            @if(count($galleries))
                            <div class="@if(DIRECTION == 'rtl') col-sm-7 @else col-sm-5 @endif" style="padding:0">
                                <select class="form-control chosen-select chosen-rtl" name="post_gallery" id="post_gallery">
                                    <option selected value="">{!!Lang::get('posts::posts.select_gallery')!!}</option>

                                    @foreach($galleries as $gallery)
                                    @if(isset($post->post_id))
                                    <option @if($post->galleries->contains($gallery->gallery_id)) selected="selected" @endif value="{!!$gallery->gallery_id!!}">{!!$gallery->gallery_name!!}</option>
                                    @else
                                    <option value="{!!$gallery->gallery_id!!}">{!!$gallery->gallery_name!!}</option>
                                    @endif
                                    @endforeach  

                                </select>
                                <div for="post_gallery" class="jquery-validate-error help-block" style="display:none">{!!Lang::get('posts::posts.required')!!}</div>

                            </div>

                            <div class="@if(DIRECTION == 'rtl') col-sm-2 @else col-sm-1 @endif">
                                <a href="#" id="gallery_edit"><i class="fa fa-pencil" style="margin-top: 10px;"></i></a>
                            </div>
                            @else
                            <div style="color:red;  margin-top: 5px">{!!Lang::get('posts::posts.no_galleries')!!}</div>
                            @endif

                        </div>


                    </div>

                </div>
            </div>
        </div>

        <div class="panel-group @if($post->post_format ==  4 || $post->post_format ==  3) col-md-6 @else col-md-4 @endif" id="media-wrapper" style="padding:0; @if(DIRECTION == 'rtl') padding-right:5px; @else padding-left:5px; @endif  @if($post->post_format ==  5) display:none @endif">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5> {!!Lang::get('posts::posts.media_file')!!} </h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>

                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content" style="padding:0">
                    <div class="panel-body" >
                        {!! Form::hidden('media_file', $post->post_media_id, array('id' => 'media_file')) !!}
                        <p style="<?php if (!$post->post_media_id) { ?> display: none; <?php } ?> margin-bottom: 0;" id="media_wrap">    
                            <a href="#" class="set-post-media">
                                <img width="100%" height="130px" src="{{{ $post->media ? $post->media->media_provider_image : assets("images/default.png") }}}" id="media_post">
                            </a>
                        </p>

                        <p style="padding: 7px 7px 53px 7px;">
                            <a href="#" id="remove-post-media" <?php if (!$post->post_media_id) { ?> style="display: none" <?php } ?>>{!!Lang::get('posts::posts.remove_media')!!}</a>
                            <a href="#" class="set-post-media" id="set-post-media"  <?php if ($post->post_media_id) { ?> style="display: none" <?php } ?>>{!!Lang::get('posts::posts.set_media')!!}</a>
                        <div for="media_file" class="jquery-validate-error help-block" style="display:none">{!!Lang::get('posts::posts.required')!!}</div>
                        </p>
                    </div>
                </div>
            </div>
        </div>

        <div style="@if($post->post_format == 2 || $post->post_format == 6 || $post->mongo_id) display:none; @endif margin-top: 20px; margin-bottom: 0; padding: 0" class="panel-group col-md-12" id="editor_content">
            <div style="margin-bottom:5px; ">
                <a  id="add_files" class="btn btn-success" href="#"><span class="btn-label icon fa fa-camera"></span>&nbsp;{!!Lang::get('posts::posts.add_media')!!}</a>
                <a  id="add_galleries_in" class="btn btn-success" href="#"><span class="btn-label icon fa fa-camera-retro"></span>&nbsp;{!!Lang::get('posts::posts.add_gallery')!!}</a>
                <a  class="btn btn-success" href="#" data-toggle="modal" data-target="#modal_polls"><span class="btn-label icon fa fa-signal"></span>&nbsp;{!!Lang::get('posts::posts.add_poll')!!}</a>
                <a  id="add_interactive" class="btn btn-success" href="#"><span class="btn-label icon fa fa-html5"></span>&nbsp;إضافة ملف تفاعلى</a>
                @if(count($post)) <a id="tweet_txt" class="btn btn-success"><span class="btn-label icon fa fa-twitter"></span>&nbsp;{!!Lang::get('posts::posts.add_tweet')!!}</a> @endif

            </div>                      


            <div class="form-group">
                <input type="hidden" id="tweet_selected_txt"/>

                <?php
                $content_options = ['class' => 'form-control', 'rows' => 4, 'id' => 'pagecontent'];
                ?>
                {!! Form::textarea('post_content',  Input::old('post_content', $post->post_content) , $content_options) !!}
                <div for="post_content" class="jquery-validate-error help-block" style="display:none">{!!Lang::get('posts::posts.required')!!}</div>

            </div>
        </div>

    </div> <!-- / .panel-body -->
</div>

<div class="panel-group" id="manshet" style="clear:both; @if($post->post_format != 6) display:none @endif">
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h5> {!!Lang::get('posts::posts.manshet')!!} </h5>
            <div class="ibox-tools">
                <a class="collapse-link">
                    <i class="fa fa-chevron-up"></i>
                </a>

                <a class="close-link">
                    <i class="fa fa-times"></i>
                </a>
            </div>
        </div>
        <div class="ibox-content">
            <div class="panel-body">
                <div class="form-group" style="margin:0"><a href="#add_manshet" class="btn-primary btn btn-flat" id="add_manshet"><i class="fa fa-sort-amount-desc"></i> {!!Lang::get('posts::posts.add_manshet')!!}</a></div>
                <div id="manshet-wrapper" style="height: 213px">
                    <div class="manshets full-height-scroll" style="padding: 0 10px">
                        @if($post->post_format == 6)
                        @foreach($post->liveblogging as $key => $value)
                        <div class="manshet row feed-element" style="border-bottom: 1px solid #e7eaec; margin:10px 0" data-id="{!!$value->id!!}">
                            <div class="row">
                                <div class="panel-group col-md-10" style="margin:0">
                                    <div class="form-group input-group">
                                        <span class="input-group-addon"><i class="fa fa-newspaper-o"></i></span>
                                        <textarea style="resize: vertical;" name="manshet_title" class="form-control manshet_title" placeholder="{!!Lang::get('posts::posts.title')!!}" rows="3">{!!$value->title!!}</textarea>
                                    </div>
                                    <label class="error" for="manshet_title" style="display:none">{!!Lang::get('posts::posts.required')!!}</label>
                                </div>
                                <div class="panel-group col-md-2" style="margin:0">
                                    <span class="label label-pa-purple time-div">{!!date("h:i", strtotime($value->date))!!}</span> 
                                    <a class="remove-manshet" style="margin: 10px">
                                        <i class="fa fa-times"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="row">
                                <div class="panel-group col-md-10" style="margin:0">
                                    <div class="form-group input-group">
                                        <span class="input-group-addon"><i class="fa fa-link"></i></span>
                                        <input name="manshet_link" value="{!!urldecode($value->link)!!}" class="form-control manshet_link" placeholder="{!!Lang::get('posts::posts.link')!!}">
                                    </div>
                                    <label class="error" for="manshet_link" style="display:none">{!!Lang::get('posts::posts.invalid')!!}</label>
                                </div>
                                <div class="panel-group col-md-2" style="margin:0">
                                    <div>
                                        <a class="btn btn-primary save_manshet" style="display: none;">{!!Lang::get('posts::posts.save')!!}</a>
                                        <img src="<?php echo assets("images/loader.gif"); ?>" class="manshet_load" style="display:none">
                                    </div>
                                </div>
                            </div>
                        </div>      

                        @endforeach
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="panel-group" id="photo_story" style="clear:both; @if($post->post_format != 2) display:none @endif">
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h5> {!!Lang::get('posts::posts.photo_story')!!} </h5>
            <div class="ibox-tools">
                <a class="collapse-link">
                    <i class="fa fa-chevron-up"></i>
                </a>

                <a class="close-link">
                    <i class="fa fa-times"></i>
                </a>
            </div>
        </div>
        <div class="ibox-content">
            <div class="panel-body">
                <div class="form-group"><a href="#add_note" class="btn-primary btn btn-flat" id="add_image"><i class="fa fa-camera"></i> {!!Lang::get('posts::posts.add_photos')!!}</a></div>
                <div style="@if(! count($post->photos)) display:none @endif" id="photo-wrapper">
                    <div class="col-md-5" style="border-left:1px solid #ccc; margin:0; padding:0; height: 299px;"> 


                        <input type="hidden" name="photos" value="{{ (isset($post_photos)) ? $post_photos : '' }}" id="photos">
                        <!-- Without padding -->
                        <div id="recent-images" class="full-height-scroll" style="border-top:none; padding: 0 10px">
                            <!-- Panel padding, without vertical padding -->
                            <div class="panel-padding no-padding-vr" id="image-div">
                                @if(count($post->photos))
                                @foreach($post->photos as $photo)
                                <div class="comment" style="border-top:none; padding: 5px 0;">
                                    <a href="#" class="image_delete" id="{!!$photo->media_id!!}">
                                        <i class="fa fa-times"></i>
                                    </a>
                                    <span class="image-wrap" id="{!!$photo->media_id!!}">
                                        <img src="{{{ $photo ? $photo->getImageURL('small') : assets("images/default.png") }}}" width="100%" height="150px">
                                    </span>
                                </div> <!-- / .image -->

                                @endforeach
                                @endif
                                <style type="text/css">
                                    .image-wrap:hover{opacity: .5; cursor: pointer;} 
                                    .image-wrap{display: block;}
                                    .comment{position: relative;}
                                    .image_delete{
                                        color: #fff;
                                        left: 0;
                                        top: 6px;
                                        position: absolute;
                                        text-align: center;
                                        text-shadow: 0 0 0 #000000;
                                        width: 19px;
                                        background-color: #000000;
                                        z-index: 99999
                                    }
                                    .photo-text{
                                        display: none;
                                    }
                                    .photo-text:nth-of-type(1){
                                        display: block;
                                    }
                                </style>
                            </div>
                        </div> <!-- / .widget-images -->
                    </div>
                    <div class="col-md-7" style="padding-left:0">  
                        <h6 class="text-light-gray text-semibold text-xs" style="margin-top:0">{!!Lang::get('posts::posts.image_title')!!}:</h6>
                        <div class="form-group" id="image-text-wrap">

                            @if(count($post->photos))
                            @foreach($post->photos as $photo)
                            {!! Form::textarea("image_text_".$photo->media_id, Input::old("image_text_".$photo->media_id, $photo->pivot->media_title), array('class' => 'form-control photo-text', 'id' => "image_text_".$photo->media_id, 'rows' => 10, 'style' => 'resize:none')) !!}
                            @endforeach
                            @endif
                        </div>  
                    </div>    
                </div>
            </div>
        </div>
    </div>
</div>



<div class="panel-group col-md-6" style="clear:both; padding:0; @if(DIRECTION == 'rtl') padding-left:5px; @else padding-right:5px; @endif">
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h5> {!!Lang::get('posts::posts.related_groups')!!} </h5>
            <div class="ibox-tools">
                <a class="collapse-link">
                    <i class="fa fa-chevron-up"></i>
                </a>

                <a class="close-link">
                    <i class="fa fa-times"></i>
                </a>
            </div>
        </div>
        <div class="ibox-content" style="padding-bottom: 16px;">
            <div class="panel-body">


                <div class="row form-group">
                    <label class="col-sm-4 control-label" style="margin-top: 10px;">{!!Lang::get('posts::posts.choose_group')!!}: </label>

                    @if(count($sets))
                    <div class="col-sm-8">
                        <select data-placeholder="{!!Lang::get('posts::posts.choose_group')!!}" class="form-control chosen-select chosen-rtl" name="post_group[]"  multiple="multiple" id="post_group">
                            @foreach($sets as $set)
                            @if(isset($post->post_id))
                            <option @if($post->sets->contains($set->related_group_id)) selected="selected" @endif value="{!!$set->related_group_id!!}">{!!$set->group_name!!}</option>
                            @else
                            <option value="{!!$set->related_group_id!!}">{!!$set->group_name!!}</option>
                            @endif
                            @endforeach                                      
                        </select>
                    </div>
                    @else
                    <div style="color:red;  margin-top: 5px;">{!!Lang::get('posts::posts.no_groups')!!}</div>
                    @endif

                </div>


            </div>

        </div>
    </div>
</div>

<div class="panel-group col-md-6" style="padding:0; @if(DIRECTION == 'rtl') padding-right:5px; @else padding-left:5px; @endif">
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h5> {!!Lang::get('posts::posts.linked_posts')!!} </h5>
            <div class="ibox-tools">
                <a class="collapse-link">
                    <i class="fa fa-chevron-up"></i>
                </a>

                <a class="close-link">
                    <i class="fa fa-times"></i>
                </a>
            </div>
        </div>
        <div class="ibox-content" style="padding-bottom: 11px;">
            <div class="panel-body-padding-vr" >
                <input type="hidden" id="linked-temp">
                <input type="hidden" id="linked-posts" value="{{ (isset($post_linked)) ? $post_linked : '' }}" name="linked-posts">
                <div class="form-group" style="position:relative">
                    <div class="input-group">
                        <input type="text" class="form-control" id="linked-text" @if(count($post->linkedPosts) == 10) disabled="disabled" @endif style="background-color: snow;" placeholder="{!!Lang::get('posts::posts.search_post')!!}" autocomplete="off">
                               <span class="input-group-btn">
                            <button class="btn btn-primary disabled" type="button" id="add-linked-post">{!!Lang::get('posts::posts.add')!!}</button>
                        </span>
                    </div>
                    <div id="auto-linked" style="height:101px; display: none">
                        <div class="feed-activity-list full-height-scroll" style="padding:0 10px"></div>
                    </div>
                    <div id="linked-search" style="display: none">
                        <span style="font-style: italic;" >{!!Lang::get('posts::posts.more_character')!!}</span>
                    </div>
                </div>

                <div style="height:200px; @if(!count($post->linkedPosts)) display: none; @endif" id="linked-add">
                    <ul class="todo-list small-list full-height-scroll" style="padding: 0 10px">
                        @if(count($post->linkedPosts))

                        @foreach($post->linkedPosts as $posts_linked_one)

                        <li class="feed-element" style="margin-top:5px">
                            <a class="check-link" href="#"><i data-id="{!!$posts_linked_one->post_linked_id!!}" class="fa fa-square-o"></i> </a>
                            <span class="m-l-xs">{!!$posts_linked_one->post_title!!}</span>
                            <a class="label label-danger pull-right delete-linked-post" href="#{!!$posts_linked_one->post_linked_id!!}"><i class="fa fa-times"></i></a>
                        </li>

                        @endforeach

                        @endif

                    </ul>
                </div>
                <div class="panel-footer clearfix" style="border-top: none; background: none; @if(!count($post->linkedPosts)) display: none; @endif">
                    <div class="pull-right">
                        <a href="#" class="btn btn-xs" id="clear-linked-tasks"><i class="fa fa-eraser text-success"></i> {!!Lang::get('posts::posts.clear_posts')!!}</a>
                    </div>
                </div>
            </div> <!-- / .panel-body -->

        </div>
    </div> <!-- / .panel -->
</div>
<!-- /13. $RECENT_TASKS -->

<!-- seo options-->
@include("posts::partials.seo", ["id" => $post->post_id])

@if(isset($post->post_id))
<div class="panel-group">
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h5> {!!Lang::get('posts::posts.comments')!!} </h5>
            <div class="ibox-tools">
                <a class="collapse-link">
                    <i class="fa fa-chevron-up"></i>
                </a>

                <a class="close-link">
                    <i class="fa fa-times"></i>
                </a>
            </div>
        </div>
        <div class="ibox-content">
            <div class="panel-body" style="position:relative">


                @if(User::can('comments.add'))

                <div style="position:absolute; @if(DIRECTION == 'rtl') left:20px; @else right:20px; @endif top:-20px; border-bottom:1px solid #bbb;" id="comments_options">
                    <h6 class="text-light-gray text-semibold text-xs" style="">{!!Lang::get('posts::posts.comment_options')!!}</h6>
                    <label class="radio" style="display:inline-block;margin-bottom: 5px;margin-top: 0; padding: 0 5px;">
                        <input type="radio" value="1" name="comment_options" class="i-checks"> 
                        <span class="lbl">{!!Lang::get('posts::posts.closed')!!}</span>
                    </label>
                    <label class="radio" style="display:inline-block;margin-bottom: 5px;margin-top: 0; padding: 0 5px;">
                        <input type="radio" value="2" name="comment_options" class="i-checks">
                        <span class="lbl">{!!Lang::get('posts::posts.without_approve')!!}</span>
                    </label>
                    <label class="radio" style="display:inline-block;margin-bottom: 5px;margin-top: 0; padding: 0 5px;">
                        <input type="radio" value="3" name="comment_options" class="i-checks">
                        <span class="lbl">{!!Lang::get('posts::posts.after_approve')!!}</span>
                    </label>   
                </div>

                <div class="form-group"><a href="#add_comment" class="btn btn-primary" id="add_comment" @if($post->comment_options == 1) disabled="disabled" @endif>{!!Lang::get('posts::posts.add_comment')!!}</a></div>


                <div class="form-group" style="display:none">
                    <p><h5 >{!!Lang::get('posts::posts.new_comment')!!}</h5></p>

                    {!! Form::textarea('post_comment', null, ['class' => 'form-control', 'rows' => 10, 'id' => 'post_comment']) !!}
                    <a href="#cancel_comment" id="cancel_comment" class="btn btn-default" style="margin-top:5px">{!!Lang::get('posts::posts.cancel')!!}</a>
                    {!! Form::button(Lang::get('posts::posts.add_comment'), array('type' => 'button', 'id' => 'new_comment', 'class' => 'pull-right btn btn-primary', 'style' => 'margin-top:5px')) !!}
                    <img class="pull-right" style="margin:12px; display:none" id="loader" src="<?php echo assets() ?>/images/ajax-loader.gif">
                    <br><br>
                </div>

                @endif

                <!-- Without padding -->
                <div id="recent-comments" style="height: 300px; border-top:none; ">
                    <!-- Panel padding, without vertical padding -->
                    <div class="feed-activity-list full-height-scroll" id="comments" style="padding: 0 10px">
                        @foreach($post->comments as $comment)
                        <div class="feed-element">
                            <div class="media-body ">
                                <small class="pull-right">
                                    @if($comment->comment_user_id)
                                    <a href="{!!URL::to(ADMIN.'/users/'.$comment->comment_user_id)!!}" title="">{!!$comment->comment_author!!}</a> - <a href="mailto:{!!$comment->comment_author_email!!}" target="_top">{!!$comment->comment_author_email!!}</a>
                                    <!--commented on <a href="#" title="">Article Name</a>-->
                                    @else
                                    {!!$comment->comment_author!!} - <a href="mailto:{!!$comment->comment_author_email!!}" target="_top">{!!$comment->comment_author_email!!}</a>
                                    @endif
                                </small>
                                <strong>{!! $comment->comment_content !!}</strong><br>
                                <small class="text-muted pull-right">@if(DIRECTION == 'rtl') {!!arabic_date($comment->comment_date)!!} @else {!! date('M d, Y', strtotime($comment->comment_date)) !!} @{!! date('h:i a', strtotime($comment->comment_date)) !!} @endif</small>
                                <div class="pull-left tooltip-demo m-t-md">
                                    @if(User::can('comments.approve'))
                                    <a data-toggle="tooltip" data-placement="top" data-type="accept" href="#{!!$comment->comment_id!!}" style=" @if($comment->comment_status == 1) display:none @endif" title="{!!Lang::get('comments::comments.approve')!!}" class="accept_comment btn btn-white btn-sm"><i class="fa fa-thumbs-up" ></i></a>
                                    @endif

                                    @if(User::can('comments.unapprove'))
                                    <a data-toggle="tooltip" data-placement="top" data-type="refuse" href="#{!!$comment->comment_id!!}" style="@if($comment->comment_status == 2) display:none @endif" title="{!!Lang::get('comments::comments.unapprove')!!}" class="refuse_comment btn btn-white btn-sm"><i class="fa fa-thumbs-down" ></i></a>
                                    @endif

                                    @if(User::can('comments.edit'))
                                    <a data-toggle="tooltip" data-placement="top" href="{!!URL::to(ADMIN.'/comments/'.$comment->comment_id.'/edit')!!}" class="btn btn-white btn-sm" title="{!!Lang::get('comments::comments.edit')!!}"><i class="fa fa-pencil"></i></a>
                                    @endif
                                    @if(User::can('comments.block'))
                                    <a data-toggle="tooltip" data-placement="top" data-type="block" href="#{!!$comment->comment_id!!}" class="block_comment btn btn-white btn-sm" title="{!!Lang::get('comments::comments.block')!!}"><i class="fa fa-minus-circle"></i></a>
                                    @endif
                                    @if(User::can('comments.trash'))
                                    <a data-toggle="tooltip" data-placement="top" data-type="trash" href="#{!!$comment->comment_id!!}" class="trash_comment btn btn-white btn-sm" title="{!!Lang::get('comments::comments.trash')!!}"><i class="fa fa-trash-o"></i></a>
                                    @endif

                                    @if(User::can('comments.spam'))
                                    <a data-toggle="tooltip" data-placement="top" data-type="spam" href="#{!!$comment->comment_id!!}" class="spam_comment btn btn-white btn-sm" title="{!!Lang::get('comments::comments.spam')!!}"><i class="fa fa-eye"></i></a>
                                    @endif
                                </div>
                            </div>
                        </div>

                        @endforeach

                    </div>
                </div> <!-- / .widget-comments -->


            </div>
        </div>
    </div>
</div>
@endif


