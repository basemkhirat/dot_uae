
@if(isset($post->post_id))
<div class="panel-group">
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h5> {!!Lang::get('posts::posts.notes')!!} </h5>
            <div class="ibox-tools">
                <a class="collapse-link">
                    <i class="fa fa-chevron-up"></i>
                </a>

                <a class="close-link">
                    <i class="fa fa-times"></i>
                </a>
            </div>
        </div>
        <div class="ibox-content">

            <div class="form-group"><a href="#add_note" class="btn btn-primary" id="add_note">{!!Lang::get('posts::posts.add_note')!!}</a></div>

            <div class="form-group" style="display:none">
                <p><h5 >{!!Lang::get('posts::posts.new_note')!!}</h5></p>

                {!! Form::textarea('post_note', null, ['class' => 'form-control', 'rows' => 5, 'id' => 'post_note']) !!}
                <a href="#cancel_note" id="cancel_note" class="btn btn-default" style="margin-top:5px">{!!Lang::get('posts::posts.cancel')!!}</a>
                {!! Form::button(Lang::get('posts::posts.add_note'), array('type' => 'button', 'id' => 'new_note', 'class' => 'pull-right btn btn-primary', 'style' => 'margin-top:5px')) !!}
                <img class="pull-right" style="margin:12px; display:none" id="note_loader" src="<?php echo assets() ?>/images/ajax-loader.gif">
                <br><br>
            </div>


            <!-- Without padding -->
            <div id="recent-notes" style="height: 180px; border-top:none; ">
                <!-- Panel padding, without vertical padding -->
                <div class="feed-activity-list full-height-scroll" id="notes" style="padding: 0 10px">
                    @foreach($post->notes as $note)
                    <div class="feed-element">
                        <div class="media-body ">
                            <small class="pull-right"><a href="{!!URL::to(ADMIN.'/users/'.$note->note_user_id)!!}" title="">{!!$note->note_author!!}</a></small>
                            <strong>{!! $note->note_content !!}</strong><br>
                            <small class="text-muted">@if(DIRECTION == 'rtl') {!!arabic_date($note->note_date)!!} @else {!! date('M d, Y', strtotime($note->note_date)) !!} @{!! date('h:i a', strtotime($note->note_date)) !!} @endif</small>
                        </div>
                    </div>

                    @endforeach

                </div>
            </div> <!-- / .widget-notes -->
        </div>
    </div>
</div>
@endif            

<div class="panel-group">
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h5> {!!Lang::get('posts::posts.publish_options')!!} </h5>
            <div class="ibox-tools">
                <a class="collapse-link">
                    <i class="fa fa-chevron-up"></i>
                </a>

                <a class="close-link">
                    <i class="fa fa-times"></i>
                </a>
            </div>
        </div>
        <div class="ibox-content">
            <div class="panel-body" style="position:relative">

                @if(isset($post->post_id))
                <a class="btn btn-primary" style="margin-bottom: 10px;" href="{!! URL::to('/details/') !!}/{{($post->mongo_id) ? $post->mongo_id : $post->post_id}}" target="_blank">{!!Lang::get('posts::posts.preview_changes')!!}</a>
                @endif
                <!-- <a class="btn pull-right @if(!User::can('posts.draft')) disabled @endif" href="#" id="save_draft">{!!Lang::get('posts::posts.save_draft')!!}</a>-->

                <div class="form-group" style="display:table"> 
                    <div style="display:table-row;">  
                        <div style="display:table-cell; @if(DIRECTION == 'rtl') padding-left:5px @else padding-right:5px @endif" ><i class="fa fa-info" style="display:inline"></i>&nbsp;&nbsp;&nbsp;&nbsp;{!!Lang::get('posts::posts.status')!!}: </div>
                        <div style="display:table-cell;">
                            <div>
                                <label style="display: block;padding-bottom: 5px;"><input type="checkbox" class="i-checks" name="new" value="1" checked disabled> <span class="lbl" >&nbsp;&nbsp;{!!Lang::get('posts::posts.new')!!}</span></label>
                                <label style="display: block;padding-bottom: 5px;"><input type="checkbox" class="i-checks" @if(! User::can('posts.pending')) disabled @endif name="post_submitted" value="1" @if($post->post_submitted || $post->post_desked) checked @endif > <span class="lbl" @if($post->post_submitted || $post->post_desked) style="text-decoration: line-through;" @endif>&nbsp;&nbsp;{!!Lang::get('posts::posts.submitted')!!}</span></label>
                                <label style="display: block;padding-bottom: 5px;"><input type="checkbox" class="i-checks" @if(! User::can('posts.reviewed')) disabled @endif name="post_desked" value="1" @if($post->post_desked) checked @endif > <span class="lbl" @if($post->post_desked) style="text-decoration: line-through;" @endif>&nbsp;&nbsp;{!!Lang::get('posts::posts.desked')!!}</span></label>
                                <label style="display: block;padding-bottom: 5px;"><input type="checkbox" class="i-checks" name="translated" value="1" @if($post->translated) checked @endif > <span class="lbl" >&nbsp;&nbsp;{!!Lang::get('posts::posts.translated')!!}</span></label>
                                <label style="display: block;padding-bottom: 5px;"><input type="checkbox" class="i-checks" @if(! User::can('posts.close_post')) disabled @endif name="post_closed" value="1" @if($post->post_closed) checked @endif > <span class="lbl" style="color: red;">&nbsp;&nbsp;{!!Lang::get('posts::posts.close_post')!!}</span></label>
                                <label style="display: block;padding-bottom: 5px;"><input type="checkbox" class="i-checks" name="scheduled" value="6" @if($post->post_status == 6) checked @endif @if(!User::can('posts.published') || (!User::can('posts.unpublish') && $post->post_status == 1)) disabled @endif> <span class="lbl" style="color: green; font-weight: bold;">&nbsp;&nbsp;{!!Lang::get('posts::posts.scheduled')!!}</span></label>
                            </div>
                            <hr style="margin-top:0; margin-bottom:10px">
                            <?php
                            //$post_status_v = ($post->post_status == 6) ? 6 : 1;
                            ?>
                            <!--<input type="hidden" name="post_status" id="draft_status" value="{!!$post->post_status!!}">-->
                            <label><input type="checkbox" id="post_status"  value="1" name="post_status" class="js-switch" @if($post->post_status == 1) checked @endif @if(!User::can('posts.published') || (!User::can('posts.unpublish') && $post->post_status == 1)) disabled @endif> <span class="lbl">{!!Lang::get('posts::posts.published')!!}</span></label>
                        </div>
                    </div>
                </div>



                @if( Input::get('post_type') == 'article')
                <?php $options = ["" => "- " . Lang::get('posts::posts.choose_author') . " -"]; ?>
                @foreach($authors as $author)
                <?php $options[$author->id] = $author->first_name . " " . $author->last_name; ?>
                @endforeach
                @endif

                @if(isset($editors))
                <?php $options1 = ["" => "- " . Lang::get('posts::posts.choose_editor') . " -"]; ?>
                @foreach($editors as $editor)
                <?php $options1[$editor->id] = $editor->first_name . " " . $editor->last_name; ?>
                @endforeach
                @endif

                @if( Input::get('post_type') == 'article')
                <div class="form-group">    
                    <span><i class="fa fa-user"></i>&nbsp;&nbsp;&nbsp;{!!Lang::get('posts::posts.author')!!}:</span>
                    @if(count($authors))
                    {!! Form::select('post_author', $options, Input::old('post_author', $post->post_author_id), ['class' => 'chosen-select chosen-rtl', 'id' => 'post_author', 'style' => 'width:80%; display: inline-block;'] )!!}
                    <div for="post_author" class="jquery-validate-error help-block" style="display:none">{!!Lang::get('posts::posts.required')!!}</div>

                    @else

                    <span style="color:red">{!!Lang::get('posts::posts.no_authors')!!}</span>
                    @endif
                </div>
                @endif

                @if( Input::get('post_type') != 'article')
                <div class="form-group">
                    <?php
                    if ($post->post_id)
                        $post_editor = $post->post_editor_id;
                    else
                        $post_editor = Auth::user()->id;
                    ?>
                    <span><i class="fa fa-user"></i>&nbsp;&nbsp;&nbsp;{!!Lang::get('posts::posts.editor')!!}:&nbsp;&nbsp;</span>
                    @if(count($editors))
                    {!! Form::select('post_editor', $options1, Input::old('post_editor', $post_editor), ['class' => 'chosen-select chosen-rtl', 'id' => 'post_editor', 'style' => 'width: 80%; display: inline-block;'] )!!}
                    @else

                    <span style="color:red">{!!Lang::get('posts::posts.no_editors')!!}</span>
                    @endif
                </div>
                @endif

                <div class="form-group" style="margin-bottom: 0;">    
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <div class="row">
                        <div class="col-md-1" >
                            <input type="checkbox" class="i-checks" name="writers" id="writers" value="1" @if($post->post_writers) checked @endif > 

                                   <span class="lbl"></span>

                        </div>

                        <div class="col-md-10" style="@if(DIRECTION == 'rtl') padding-left:0 @else padding-right: 0 @endif">
                            {!! Form::textarea('post_writers', Input::old('post_writers', $post->post_writers), ['class' => 'form-control', 'rows' => 3, 'cols'=> 27, 'id' => 'post_writers', 'style' => 'resize: none; ', 'placeholder' => Lang::get('posts::posts.writers'), 'disabled' => 'true']) !!}
                        </div>
                    </div>
                </div><br>
                @if($post->post_id)
                <div class="form-group">    
                    <i class="fa fa-eye"></i>&nbsp;&nbsp;{!!Lang::get('posts::posts.post_views')!!}:&nbsp;&nbsp; 
                    <span class="label label-pa-purple">{!!$post->post_views!!}</span>
                </div>
                @endif

                {!!$publish_seo!!}
                
                <div class="form-group">
                    <p><i class="fa fa-bullhorn"></i>&nbsp;&nbsp;{!!trans("posts::posts.notifications")!!}:</p>

                    <input type="text" name="notify" class="form-control select2-multible" value='{{ (isset($post->post_id)) ? @$post->user_notify_select2_value : "" }}' data-placeholder='{!!trans("posts::posts.noti_placeholder")!!}' data-fetch-url='{!!URL::to(ADMIN.'/notifications/search-users')!!}'>
                </div>

                @if(isset($post->post_id))
                <div class="form-group">    
                    <i class="fa fa-clock-o"></i>&nbsp;&nbsp;{!!Lang::get('posts::posts.updated_on')!!}:<strong> @if(DIRECTION == 'rtl') {!!arabic_date($post->post_updated_date)!!} @else {!! date('M d, Y', strtotime($post->post_updated_date)) !!} @{!! date('h:i a', strtotime($post->post_updated_date)) !!} @endif</strong>
                </div>
                @endif

                @if($post && $post->post_status == 1)
                <div class="form-group">
                    @if(is_null($post->post_published_date))
                    <i class="fa fa-clock-o"></i>&nbsp;&nbsp;{!!Lang::get('posts::posts.publish_on')!!}:<strong> @if(DIRECTION == 'rtl') {!!arabic_date($post->post_created_date)!!} @else {!! date('M d, Y', strtotime($post->post_created_date)) !!} @{!! date('h:i a', strtotime($post->post_created_date)) !!} @endif</strong>
                    @else
                    <i class="fa fa-clock-o"></i>&nbsp;&nbsp;{!!Lang::get('posts::posts.publish_on')!!}:<strong> @if(DIRECTION == 'rtl') {!!arabic_date($post->post_published_date)!!} @else {!! date('M d, Y', strtotime($post->post_published_date)) !!} @{!! date('h:i a', strtotime($post->post_published_date)) !!} @endif</strong>
                    @endif
                </div>
                @endif

                <div class="form-group" id="schedule-wrap"  style="@if($post->post_status != 6) display:none; @endif margin-bottom:5px">    
                    <i class="fa fa-clock-o"></i>&nbsp;

                    @if($post->post_published_date)
                    <span id="schedule-name">{!!Lang::get('posts::posts.schedule_for')!!}</span>: 
                    <strong id="schedule-date">
                        @if(DIRECTION == 'rtl') {!!arabic_date($post->post_published_date)!!} @else {!! date('M d, Y', strtotime($post->post_published_date)) !!} @{!! date('h:i a', strtotime($post->post_published_date)) !!} @endif
                    </strong>
                    @else
                    <span id="schedule-name">{!!Lang::get('posts::posts.publish_on')!!}</span>: 
                    <strong id="schedule-date">
                        {!!Lang::get('posts::posts.immediately')!!}
                    </strong>

                    @endif

                    &nbsp;&nbsp;
                    <a href="#post_status" style="display: inline;" id="schedule-edit">{!!Lang::get('posts::posts.edit')!!} </a>


                </div>

                <div id="schedule-select" class="row" style="display:none;padding: 19px 14px 0 14px;">
                    <?php
                    $date = '';
                    $time = '';
                    if ($post->post_published_date) {
                        $date = date('Y-m-d H:i:s', strtotime($post->post_published_date));
                        //$time = date('h:i A', strtotime($post->post_published_date));
                    }

                    if (DIRECTION == 'rtl') {
                        $style1 = 'margin-left:5px';
                    } else {
                        $style1 = 'margin-right:5px';
                    }
                    ?>
                    <div class="pull-left form-group" id="input-daterange">
                        <div class="pull-left add-on" style="{!!$style1!!}">
                            {!! Form::text('datepicker', Input::old('datepicker', $date), array('class' => 'form-control', 'id' => 'bs-datepicker-example', 'placeholder' => Lang::get('posts::posts.last_published'))) !!}
                        </div>

                    </div>
                    <div class="pull-left form-group">
                        <a href="#post_status" class="btn btn-primary" id="change_schedule">{!!Lang::get('posts::posts.ok')!!}</a> &nbsp;&nbsp;
                        <a href="#post_status" id="schedule-cancel" class="btn btn-default">{!!Lang::get('posts::posts.cancel')!!}</a>
                    </div>
                </div>
                <div class="panel-footer" style="border-top: 1px solid #ececec; background: none;">
                    <div class="form-group" style="margin-bottom:0">
                        @if($post && User::can('posts.trash'))
                        <a class="pull-left" style="margin-top: 9px;" href="{!! URL::to('/'.ADMIN.'/posts/status/' . $post->post_id . '/4?cat_id='.Input::get('cat_id')) !!}">{!!Lang::get('posts::posts.move_trash')!!}</a>
                        @endif
                        <input type="hidden" name="saveandclose" id="saveandclose" value="0">
                        {!! Form::button(Lang::get('posts::posts.save'), array('id' => 'save', 'type' => 'submit', 'class' => 'pull-right btn btn-primary')) !!}
                        {!! Form::button(Lang::get('posts::posts.saveandclose'), array('id' => 'closesave', 'type' => 'submit', 'class' => 'pull-right btn btn-primary', 'style' => 'margin: 0 10px;')) !!}
                        @if(!$post) <img class="pull-right" src="<?php echo assets("images/loader.gif"); ?>" id="save_load" style="display:none">@endif
                    </div>
                </div>
            </div>


        </div>
    </div>
</div> 

@if($post && count($post->history))
<div class="panel-group">
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h5> {!!Lang::get('posts::posts.prev_updates')!!} </h5>
            <div class="ibox-tools">
                <a class="collapse-link">
                    <i class="fa fa-chevron-up"></i>
                </a>

                <a class="close-link">
                    <i class="fa fa-times"></i>
                </a>
            </div>
        </div>
        <div class="ibox-content">
            <div class="panel-body" style="padding: 7px;">



                <!-- Without padding -->
                <div id="recent-updates" style="height: 180px; border-top:none;">
                    <!-- Panel padding, without vertical padding -->
                    <div class="feed-activity-list full-height-scroll" id="updates" style="padding: 0 10px">
                        @foreach($post->history as $update)
                        <div class="feed-element">
                            <div class="media-body ">
                                <small class="pull-right"><a href="{!!URL::to(ADMIN.'/users/'.$update->user_id)!!}" title="">{!!$update->user->first_name!!} {!!$update->user->last_name!!}</a></small>
                                <strong>
                                    @if($update->post_status == 1) 
                                        {!!Lang::get('posts::posts.publish_post')!!} 
                                    @elseif($update->post_status == '6')
                                        {!!Lang::get('posts::posts.schedule')!!}
                                    @elseif($update->post_status == '0')
                                        {!!Lang::get('posts::posts.unpublish_post')!!} 
                                    @else
                                        {!!Lang::get('posts::posts.update_post')!!} 
                                    @endif
                                </strong><br>
                                <small class="text-muted">@if(DIRECTION == 'rtl') {!!arabic_date($update->update_date)!!} @else {!! date('M d, Y', strtotime($update->update_date)) !!} @{!! date('h:i a', strtotime($update->update_date)) !!} @endif</small>
                            </div>
                        </div>

                        @endforeach

                    </div>
                </div> <!-- / .widget-notes --> 
            </div>
        </div>
    </div>
</div>
@endif          

<div class="panel-group">
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h5> {!!Lang::get('posts::posts.post_format')!!} </h5>
            <div class="ibox-tools">
                <a class="collapse-link">
                    <i class="fa fa-chevron-up"></i>
                </a>

                <a class="close-link">
                    <i class="fa fa-times"></i>
                </a>
            </div>
        </div>
        <div class="ibox-content">
            <div class="panel-body">


                <div class="form-group" style="margin-bottom:0px">
                    <div class="radio" style="margin-top: 0;">
                        <label>
                            <input type="radio" name="post_format" value="1" class="i-checks" checked>&nbsp;
                            <i class="fa fa-newspaper-o"></i>&nbsp;
                            <span class="lbl">{!!Lang::get('posts::posts.standard')!!}</span>
                        </label>
                    </div>
                    <div class="radio" style="margin-top: 0;">
                        <label>
                            <input type="radio" name="post_format" value="2" class="i-checks">&nbsp;
                            <i class="fa fa-file-image-o"></i>&nbsp;
                            <span class="lbl">{!!Lang::get('posts::posts.photo_story')!!}</span>
                        </label>
                    </div>
                    <div class="radio" style="margin-top: 0;">
                        <label>
                            <input type="radio" name="post_format" value="3" class="i-checks">&nbsp;
                            <i class="fa fa-volume-up"></i>&nbsp;
                            <span class="lbl">{!!Lang::get('posts::posts.audio')!!}</span>
                        </label>
                    </div>
                    <div class="radio" style="margin-top: 0;">
                        <label>
                            <input type="radio" name="post_format" value="4" class="i-checks">&nbsp;
                            <i class="fa fa-video-camera"></i>&nbsp;
                            <span class="lbl">{!!Lang::get('posts::posts.video')!!}</span>
                        </label>
                    </div>
                    <div class="radio" style="margin-top: 0;">
                        <label>
                            <input type="radio" name="post_format" value="5" class="i-checks" @if(!count($galleries)) disabled @endif>&nbsp;
                                   <i class="fa fa-camera"></i>&nbsp;
                            <span class="lbl">{!!Lang::get('posts::posts.gallery')!!}</span>
                        </label>
                    </div>
                    <div class="radio" style="margin-top: 0;">
                        <label>
                            <input type="radio" name="post_format" value="6" class="i-checks" @if(!$post->post_id) disabled @endif>&nbsp;
                                   <i class="fa fa-sort-amount-desc"></i>&nbsp;
                            <span class="lbl">{!!Lang::get('posts::posts.manshet')!!}</span>
                        </label>
                    </div>
                </div>


            </div>

        </div>
    </div>
</div>



<div class="panel-group">
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h5> {!!Lang::get('posts::posts.categories')!!} </h5>
            <div class="ibox-tools">
                <a class="collapse-link">
                    <i class="fa fa-chevron-up"></i>
                </a>

                <a class="close-link">
                    <i class="fa fa-times"></i>
                </a>
            </div>
        </div>
        <div class="ibox-content">
            <div class="panel-body">
                <ul class="nav nav-tabs">
                    <li class="active">
                        <a href="#categories-tab" class="active" data-toggle="tab" style="margin:0">{!!Lang::get('posts::posts.all_categories')!!} <span class="label label-success">{!! count($cats) !!}</span></a>
                    </li>
                    <li>
                        <a href="#mostused-tab" data-toggle="tab" style="margin:0">

                            @if(Input::has('cat_id'))
                            <?php $sub_cats = buildtree($cats, Input::get('cat_id')) ?>
                            @if(count($sub_cats))
                            {!!Lang::get('posts::posts.subcategories')!!} <span class="badge badge-primary">{!! count($sub_cats) !!}</span></a>
                        @else
                        {!!Lang::get('posts::posts.most_used')!!} <span class="badge badge-primary">{!! count($most_cats) !!}</span></a>
                        @endif
                        @else
                        {!!Lang::get('posts::posts.most_used')!!} <span class="badge badge-primary">{!! count($most_cats) !!}</span></a>
                        @endif                                    
                    </li>
                </ul>

                <div class="tab-content tab-content-bordered" >
                    <div class="tab-pane fade active in " id="categories-tab">
                        <div id="treeOne">
                            @if(count($cats))
                            {!!cats_tree(buildtree($cats), '', 4, ['checkbox_name' => 'cats'])!!}
                            @else
                            {!!Lang::get('posts::posts.no_categories')!!}
                            @endif
                        </div>
                    </div> <!-- / .tab-pane -->
                    <div class="tab-pane fade" id="mostused-tab">
                        <div id="treeTwo">
                            @if(Input::has('cat_id')) 
                            @if(count($sub_cats))
                            {!!cats_tree($sub_cats, '', 4, ['checkbox_name' => 'mosts'])!!}
                            @else
                            @if(count($most_cats))
                            <ul>
                                @foreach($most_cats as $cat)
                                <li><div class="checkbox"><label>- {!! Form::checkbox('mosts[]', $cat->cat_id, false, ['class' => 'i-checks']) !!} <span class="lbl">{!! $cat->cat_name !!}</span></label></div></li>  
                                @endforeach
                            </ul>
                            @else
                            {!!Lang::get('posts::posts.no_most_categories')!!}
                            @endif
                            @endif
                            @else
                            @if(count($most_cats))
                            <ul>
                                @foreach($most_cats as $cat)
                                <li><div class="checkbox"><label>- {!! Form::checkbox('mosts[]', $cat->cat_id, false, ['class' => 'i-checks']) !!} <span class="lbl">{!! $cat->cat_name !!}</span></label></div></li>  
                                @endforeach
                            </ul>
                            @else
                            {!!Lang::get('posts::posts.no_most_categories')!!}
                            @endif
                            @endif  

                        </div>
                    </div> <!-- / .tab-pane -->
                </div> <!-- / .tab-content -->

                <div for="cats" class="jquery-validate-error help-block" id="cats-error" style="display:none">{!!Lang::get('posts::posts.one_cats')!!}</div>


                @if(User::can('categories.manage'))
                <!--                            <div id="category-adder" class=""`>
                                                <h4>
                                                    <a id="category-add-toggle" href="#category-add">+ {!!Lang::get('posts::posts.new_category')!!}</a>
                                                </h4>
                                                <div id="category-add" style="display:none">
                                                    <div class="form-group">
                                                        {!! Form::text('newcategory', '', array('class' => 'form-control', 'id' => 'newcategory', 'placeholder' => Lang::get('posts::posts.category_name'))) !!}
                                                    </div>
                
                
                                                    <div class="form-group">
                                                        <select class="form-control" id="cat_parent" name="cat_parent">
                                                            <option value="" selected="selected">-- {!!Lang::get('posts::posts.parent_category')!!} --</option>
                                                            {!!parent_tree(buildtree($cats), '', 4, '')!!}
                                                        </select>
                
                                                    </div>
                
                                                    <div class="form-group">
                                                        <a href="#new_cat" class="btn btn-default" id="add-new-cat">{!!Lang::get('posts::posts.new_category')!!}</a>
                                                    </div>
                                                </div>
                                            </div>-->
                @endif
            </div>
        </div>
    </div>
</div>


<div class="panel-group">
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h5> {!!Lang::get('posts::posts.tags')!!} </h5>
            <div class="ibox-tools">
                <a class="collapse-link">
                    <i class="fa fa-chevron-up"></i>
                </a>

                <a class="close-link">
                    <i class="fa fa-times"></i>
                </a>
            </div>
        </div>
        <div class="ibox-content">
            <div class="panel-body">
                {!! Form::hidden('tags', @$post_tags, array('id' => 'tags')) !!}
                <div class="form-group" style="position:relative">
                    <div class="input-group">
                        {!! Form::text('newtag', '', array('class' => 'form-control', 'id' => 'newtag', 'placeholder' => '', 'autocomplete' => 'off')) !!}
                        <span class="input-group-btn">
                            {!! Form::button(Lang::get('posts::posts.add'), array('type' => 'submit', 'class' => 'btn btn-primary', 'id' => 'add-tag')) !!}
                        </span>
                    </div>
                    <div id="tag-wrap" style="height:101px; display: none">
                        <div class="feed-activity-list full-height-scroll" style="padding: 0 10px">
                        </div>
                    </div>
                    <div id="tag-search" style="display: none">
                        <span style="font-style: italic;" >{!!Lang::get('posts::posts.more_character')!!}</span>
                    </div>
                    <div for="newtag" class="jquery-validate-error help-block" style="display:none">{!!Lang::get('posts::posts.required')!!}</div>
                    <span>{!!Lang::get('posts::posts.separate_tags')!!}</span>
                </div>

                <div class="tags-show row" style="@if(! count($post->tags) ) display:none; @endif padding: 0 10px;">
                    <ul class="tag-list pull-left" style="border:none; padding: 0">  
                        @if(count($post->tags) )
                        @foreach($post->tags as $tag)
                        <li><span>{!!$tag->tag_name!!}<i data-tag="{!!$tag->tag_name!!}" class="close-tag fa fa-times"></i></span></li>
                        @endforeach
                        @endif
                    </ul>
                </div>

                <hr style="margin: 10px 0;">
                <span><a href="#titlediv" id="link-post_tag">{!!Lang::get('posts::posts.most_tags')!!}</a></span>

                <div id="tagcloud-post_tag" class="the-tagcloud" style="display:none">
                    @if(count($most_tags))
                    <?php $i = 5 ?>
                    @foreach($most_tags as $tag)
                    <a href="#{!!$tag->tag_id!!}"  class="tag-add" title="{!!$tag->posts->first()->counts;!!} {!!Lang::get('posts::posts.topics')!!}" style="font-size: {!!$i * 8!!}pt; line-height: {!!$i * 8!!}pt;">{!!$tag->tag_name!!}</a>
                    <?php $i-- ?>
                    @endforeach
                    @else
                    {!!Lang::get('posts::posts.no_tags')!!}
                    @endif              
                </div>
            </div>
        </div>
    </div>
</div>

<div class="panel-group">
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h5> {!!Lang::get('topics::topics.topics')!!} </h5>
            <div class="ibox-tools">
                <a class="collapse-link">
                    <i class="fa fa-chevron-up"></i>
                </a>

                <a class="close-link">
                    <i class="fa fa-times"></i>
                </a>
            </div>
        </div>
        <div class="ibox-content">
            <div class="panel-body">
                <div class="form-group" style="position:relative">

                    <input type="text" name="topics" class="form-control select2-multible" value='{!!(isset($post->post_id)) ? @$post->topics_select2_value : "" !!}' data-placeholder='{!!trans('topics::topics.topics')!!}' data-fetch-url="{!!URL::to(ADMIN.'/topics/select2-search')!!}">

                </div>
                <a href="#" data-toggle="modal" class="btn btn-primary" data-target="#modal_new_topic">{!!Lang::get('topics::topics.add_new')!!}</a>
            </div>
        </div>
    </div>
</div>

