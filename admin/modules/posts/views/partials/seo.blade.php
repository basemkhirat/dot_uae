<div class="panel-group" style="clear:both">
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h5> {!!Lang::get('posts::posts.meta_options')!!} </h5>
            <div class="ibox-tools">
                <a class="collapse-link">
                    <i class="fa fa-chevron-up"></i>
                </a>

                <a class="close-link">
                    <i class="fa fa-times"></i>
                </a>
            </div>
        </div>
        <div class="ibox-content">
            <div class="panel-body">
                <ul class="nav nav-tabs">
                    <li class="active">
                        <a href="#general-tab" data-toggle="tab">{!!Lang::get('posts::posts.general')!!}</a>
                    </li>
                    <li>
                        <a href="#analysis-tab" data-toggle="tab">{!!Lang::get('posts::posts.page_analysis')!!}</a>                       
                    </li>
                    <li>
                        <a href="#advanced-tab" data-toggle="tab">{!!Lang::get('posts::posts.advanced')!!}</a>
                    </li>
                    <li>
                        <a href="#facebook-tab" data-toggle="tab">{!!Lang::get('posts::posts.facebook')!!}</a>                        
                    </li>
                    <li>
                        <a href="#twitter-tab" data-toggle="tab">{!!Lang::get('posts::posts.twitter')!!}</a>                       
                    </li>
                </ul>
                <div class="tab-content tab-content-bordered form-horizontal" style="padding-top:15px">
                    <div class="tab-pane fade active in " id="general-tab">
                        <div class="form-group tooltip-demo">
                            <label class="col-sm-3 control-label" style="padding-top: 7px;">{!!Lang::get('posts::posts.snippet_preview')!!}</label>
                            <div class="col-sm-1 text-left"><label style="cursor: pointer; padding: 15px 0;" class="control-label " data-toggle="tooltip" data-placement="top" title="{!!Lang::get('posts::posts.snippet_info')!!}"><i class="fa fa-question-circle"></i></label></div>
                            <div class="col-sm-8">
                                <div id="wpseosnippet">
                                    <a class="title" id="wpseosnippet_title" href="#">@if(isset($post->post_id)) {!! @$meta_robots->meta_title !!} @else - {!!Config::get("site_name")!!} @endif</a>
                                    <span class="url">@if(isset($post->post_id)) {!! URL::to("/details") !!}/<strong id="wpseosnippet_slug">{!! $post->post_slug !!}</strong> @else {!! URL::to("/") !!}/<strong id="wpseosnippet_slug"></strong> @endif</span>
                                    <p class="desc"><span class="autogen"></span><span class="content">{!! @$meta_robots->meta_description !!}</span></p>
                                </div>
                            </div>
                        </div>
                        <div class="form-group tooltip-demo">
                            <label for="focus_keyword" class="col-sm-3 control-label" style="padding-top: 7px;">{!!Lang::get('posts::posts.focus_keyword')!!}</label>
                            <div class="col-sm-1 text-left"><label for="focus_keyword" style="cursor: pointer; padding: 15px 0;" id="focuskwhelp" class="control-label " data-toggle="tooltip" data-placement="top" title="{!!Lang::get('posts::posts.focus_info')!!}"><i class="fa fa-question-circle"></i></label></div>
                            <div class="col-sm-8">
                                <input type="hidden" name="meta[focus_keyword]" id="focus_keyword" value="{!! Input::old('meta[focus_keyword]', @$meta_robots->focus_keyword) !!}">
                                <ul id="metafocus" style="border: 1px solid #E5E6E7 !important;"></ul>
                                <br>
                                <div>
                                    <div id="focuskwresults">
<!--                                                    <p>
                                            <strong>{!!Lang::get('posts::posts.focus_keyword_usage')!!}</strong><br>{!!Lang::get('posts::posts.focus_keyword_p')!!}:
                                        </p>
                                        <ul>
                                            <li>{!!Lang::get('posts::posts.article_heading')!!}: <span class="good">{!!Lang::get('posts::posts.yes')!!} (1)</span></li>
                                            <li>{!!Lang::get('posts::posts.page_title')!!}: <span class="wrong">{!!Lang::get('posts::posts.no')!!}</span></li>
                                            <li>{!!Lang::get('posts::posts.page_url')!!}: <span class="good">{!!Lang::get('posts::posts.yes')!!} (1)</span></li>
                                            <li>{!!Lang::get('posts::posts.content')!!}: <span class="good">{!!Lang::get('posts::posts.yes')!!} (8)</span></li>
                                            <li>{!!Lang::get('posts::posts.meta_description')!!}: <span class="wrong">{!!Lang::get('posts::posts.no')!!}</span></li>
                                        </ul>-->
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group tooltip-demo">
                            <label for="meta_title" class="col-sm-3 control-label" style="padding-top: 7px;">{!!Lang::get('posts::posts.meta_title')!!}</label>
                            <div class="col-sm-1 text-left"><label for="meta_title" id="title-length-warning" style="cursor: pointer; padding: 15px 0;" class="control-label " data-toggle="tooltip" data-placement="top" title="{!!Lang::get('posts::posts.title_info')!!}"><i class="fa fa-question-circle"></i></label></div>
                            <div class="col-sm-8">
                                {!! Form::text("meta[meta_title]", string_sanitize(Input::old('meta[meta_title]', @$meta_robots->meta_title)), array('class' => 'form-control input-lg', 'id' => 'meta_title', 'placeholder' => Lang::get('posts::posts.meta_title'))) !!}

                            </div>
                        </div>

                        <div class="form-group tooltip-demo">
                            <label for="meta_keywords" class="col-sm-3 control-label" style="padding-top: 7px;">{!!Lang::get('posts::posts.meta_keywords')!!}</label>
                            <div class="col-sm-1 text-left"><label for="meta_title" style="cursor: pointer; padding: 15px 0;" class="control-label " data-toggle="tooltip" data-placement="top" title="{!!Lang::get('posts::posts.keywords_info')!!}"><i class="fa fa-question-circle"></i></label></div>
                            <div class="col-sm-8">
                                <input type="hidden" name="meta[meta_keywords]" id="meta_keywords" value="{!! Input::old('meta[meta_keywords]', @$meta_robots->meta_keywords) !!}">
                                <ul id="metakeywords" style="border: 1px solid #E5E6E7 !important;"></ul>
                            </div>
                        </div>

                        <div class="form-group tooltip-demo">
                            <label for="meta_description" class="col-sm-3 control-label" style="padding-top: 7px;">{!!Lang::get('posts::posts.meta_description')!!}</label>
                            <div class="col-sm-1 text-left"><label for="meta_description" style="cursor: pointer; padding: 15px 0;" class="control-label " data-toggle="tooltip" data-placement="top" title="{!!Lang::get('posts::posts.description_info')!!}"><i class="fa fa-question-circle"></i></label></div>
                            <div class="col-sm-8">
                                {!! Form::textarea("meta[meta_description]", string_sanitize(Input::old('meta[meta_description]', @$meta_robots->meta_description)), ['class' => 'form-control', 'rows' => 4, 'id' => 'meta_description', 'style' => 'resize: vertical;',  'placeholder' => Lang::get('posts::posts.meta_description')]) !!}
                                <div style="font-size: 14px; margin-top: 2px">
                                    {!!trans('posts::posts.meta_descrip_subtitle', array('maxLength' => 156))!!} <span id="metadesc-length"><span class="good">156</span></span> {!!Lang::get('posts::posts.chars_left')!!}.
                                </div>
                            </div>
                        </div>
                    </div> <!-- / .tab-pane -->
                    <div class="tab-pane fade" id="analysis-tab">
                        <div class="form-group" style="padding:0 14px">
                            {!!$seo_results!!}
                        </div>
                    </div> <!-- / .tab-pane -->
                    <div class="tab-pane fade form-group" id="advanced-tab">
                        <div class="form-group">
                            <label for="robots_index" class="col-sm-3 control-label" style="padding-top: 7px;">{!!Lang::get('posts::posts.meta_robots_index')!!}</label>
                            <div class="col-sm-9">
                                <select name="meta[robots_index]" id="robots_index" class="form-control" style="width:auto; display: inline-block;">
                                    <option value="0">{!!Lang::get('posts::posts.robots_index_default')!!}</option>
                                    <option value="2" @if(@$meta_robots->robots_index == 2) selected @endif>{!!Lang::get('posts::posts.index')!!}</option>
                                    <option value="1" @if(@$meta_robots->robots_index == 1) selected @endif>{!!Lang::get('posts::posts.noindex')!!}</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group" style="margin-top:20px">
                            <label for="robots_follow" class="col-sm-3 control-label" style="padding-top: 7px;">{!!Lang::get('posts::posts.meta_robots_follow')!!}</label>
                            <div class="col-sm-9">
                                <input type="radio" name="meta[robots_follow]" value="1" class="i-checks" checked>
                                <span class="lbl">{!!Lang::get('posts::posts.follow')!!}</span>&nbsp;&nbsp;
                                <input type="radio" name="meta[robots_follow]" value="2" class="i-checks" checked>
                                <span class="lbl">{!!Lang::get('posts::posts.nofollow')!!}</span>
                            </div>
                        </div>
                        <div class="form-group" style="margin-top:20px">
                            <label for="robots_advanced" class="col-sm-3 control-label" style="padding-top: 7px;">{!!Lang::get('posts::posts.meta_robots_advanced')!!}</label>
                            <div class="col-sm-9">

                                <select data-placeholder="{!!Lang::get('posts::posts.site_wide_default')!!}" style="" class="form-control chosen-select chosen-rtl" name="robots_advanced[]"  multiple="multiple" id="robots_advanced">
                                    <option value="none" @if(strpos(@$meta_robots->robots_advanced, 'none') !== false) selected="selected" @endif>{!!Lang::get('posts::posts.none')!!}</option>
                                    <option value="noodp" @if(strpos(@$meta_robots->robots_advanced, 'noodp') !== false) selected="selected" @endif>{!!Lang::get('posts::posts.noodp')!!}</option>
                                    <option value="noydir" @if(strpos(@$meta_robots->robots_advanced, 'noydir') !== false) selected="selected" @endif>{!!Lang::get('posts::posts.noydir')!!}</option>
                                    <option value="noimageindex" @if(strpos(@$meta_robots->robots_advanced, 'noimageindex') !== false) selected="selected" @endif>{!!Lang::get('posts::posts.no_image_index')!!}</option>
                                    <option value="noarchive" @if(strpos(@$meta_robots->robots_advanced, 'noarchive') !== false) selected="selected" @endif>{!!Lang::get('posts::posts.no_archive')!!}</option>
                                    <option value="nosnippet" @if(strpos(@$meta_robots->robots_advanced, 'nosnippet') !== false) selected="selected" @endif>{!!Lang::get('posts::posts.no_snippet')!!}</option>
                                </select>
                                <div style="font-size: 14px; margin-top: 2px">
                                    {!!trans('posts::posts.robots_advanced_info')!!}
                                </div>
                            </div>
                        </div>
                        <div class="form-group" style="margin-top:20px">
                            <label for="in_sitemap" class="col-sm-3 control-label" style="padding-top: 7px;">{!!Lang::get('posts::posts.include_in_sitemap')!!}</label>
                            <div class="col-sm-9">
                                <select name="meta[in_sitemap]" id="in_sitemap" class="form-control" style="width:auto; display: inline-block;">
                                    <option value="0">{!!Lang::get('posts::posts.auto_detect')!!}</option>
                                    <option value="1" @if(@$meta_robots->in_sitemap == 1) selected @endif>{!!Lang::get('posts::posts.always_include')!!}</option>
                                    <option value="2" @if(@$meta_robots->in_sitemap == 2) selected @endif>{!!Lang::get('posts::posts.never_include')!!}</option>
                                </select>
                                <div style="font-size: 14px; margin-top: 2px">
                                    {!!trans('posts::posts.sitemap_info')!!}
                                </div>
                            </div>
                        </div>
                        <div class="form-group" style="margin-top:20px">
                            <label for="sitemap_priority" class="col-sm-3 control-label" style="padding-top: 7px;">{!!Lang::get('posts::posts.sitemap_priority')!!}</label>
                            <div class="col-sm-9">
                                <select name="meta[sitemap_priority]" id="sitemap_priority" class="form-control" style="width:auto; display: inline-block;">
                                    <option value="0">{!!Lang::get('posts::posts.auto_priority')!!}</option>
                                    <option value="1">1 - {!!Lang::get('posts::posts.high_priority')!!}</option>
                                    <option value="0.9">0.9</option>
                                    <option value="0.8">0.8 - {!!Lang::get('posts::posts.priority_first')!!}</option>
                                    <option value="0.7">0.7</option>
                                    <option value="0.6">0.6 - {!!Lang::get('posts::posts.priority_second')!!}</option>
                                    <option value="0.5">0.5 - {!!Lang::get('posts::posts.medium_priority')!!}</option>
                                    <option value="0.4">0.4</option>
                                    <option value="0.3">0.3</option>
                                    <option value="0.2">0.2</option>
                                    <option value="0.1">0.1 - {!!Lang::get('posts::posts.low_priority')!!}</option>
                                </select>
                                <div style="font-size: 14px; margin-top: 2px">
                                    {!!trans('posts::posts.sitemap_priority_info')!!}
                                </div>
                            </div>
                        </div>
                        <div class="form-group" style="margin-top:20px">
                            <label for="canonical_url" class="col-sm-3 control-label" style="padding-top: 7px;">{!!Lang::get('posts::posts.canonical_url')!!}</label>
                            <div class="col-sm-9">
                                {!! Form::text("meta[canonical_url]", string_sanitize(Input::old('meta[canonical_url]', @$meta_robots->canonical_url)), array('class' => 'form-control input-lg', 'id' => 'canonical_url', 'placeholder' => Lang::get('posts::posts.canonical_url'))) !!}
                                <div style="font-size: 14px; margin-top: 2px">
                                    {!!trans('posts::posts.canonical_url_info')!!}
                                </div>
                            </div>
                        </div>
                        <div class="form-group" style="margin-top:20px">
                            <label for="amseo_redirect" class="col-sm-3 control-label" style="padding-top: 7px;">{!!Lang::get('posts::posts.amseo_redirect')!!}</label>
                            <div class="col-sm-9">
                                {!! Form::text("meta[amseo_redirect]", string_sanitize(Input::old('meta[amseo_redirect]', @$meta_robots->amseo_redirect)), array('class' => 'form-control input-lg', 'id' => 'amseo_redirect', 'placeholder' => Lang::get('posts::posts.amseo_redirect'))) !!}
                                <div style="font-size: 14px; margin-top: 2px">
                                    {!!trans('posts::posts.amseo_redirect_info')!!}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="facebook-tab">
                        <div class="form-group">
                            <label for="facebook_title" class="col-sm-3 control-label" style="padding-top: 7px;">{!!Lang::get('posts::posts.meta_title')!!}</label>
                            <div class="col-sm-9">
                                {!! Form::text("meta[facebook_title]", string_sanitize(Input::old('meta[facebook_title]', @$meta_robots->facebook_title)), array('class' => 'form-control input-lg', 'id' => 'facebook_title', 'placeholder' => Lang::get('posts::posts.meta_title'))) !!}
                                <div style="font-size: 14px; margin-top: 2px">
                                    {!!trans('posts::posts.facebook_title_info')!!}
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="facebook_description" class="col-sm-3 control-label" style="padding-top: 7px;">{!!Lang::get('posts::posts.meta_description')!!}</label>
                            <div class="col-sm-9">
                                {!! Form::textarea("meta[facebook_description]", string_sanitize(Input::old('meta[facebook_description]', @$meta_robots->facebook_description)), ['class' => 'form-control', 'rows' => 4, 'id' => 'facebook_description', 'style' => 'resize: vertical;', 'placeholder' => Lang::get('posts::posts.meta_description')]) !!}
                                <div style="font-size: 14px; margin-top: 2px">
                                    {!!trans('posts::posts.facebook_descrip_info')!!}
                                </div>
                            </div>

                        </div>

                        <div class="form-group">
                            <label for="facebook_image" class="col-sm-3 control-label" style="padding-top: 7px;">{!!Lang::get('posts::posts.image')!!}</label>
                            <div class="col-sm-9" style=" padding-top:7px; position:relative;">
                                <span>
                                    <input type="hidden" value="{!!@$meta_robots->facebook_image!!}" id="facebook_photo" name="meta[facebook_image]">
                                    <img src="{{{ @$meta_robots->facebook ? @$meta_robots->facebook->getImageURL('small') : assets("images/default.png") }}}" height="108px" id="facebook_image" @if(!@$meta_robots->facebook_image) style="display:none" @endif>       
                                         <button type="button" class="close" id="remove_fb_image" style="position:absolute; @if(DIRECTION == 'rtl') left:14px; @else right:14px @endif top:0; @if(!@$meta_robots->facebook_image) display:none @endif"><i class="fa fa-times"></i></button>                                         
                                    <a href="#" id="remove-facebook-image" @if(!@$meta_robots->facebook_image) style="display:none" @else style="display:block" @endif>{!!Lang::get('posts::posts.remove_fb_image')!!}</a>
                                    <a href="#" id="set-facebook-image" @if(!@$meta_robots->facebook_image) style="display:block" @else style="display:none" @endif>{!!Lang::get('posts::posts.set_fb_image')!!}</a>
                                </span>
                                <div style="font-size: 14px; margin-top: 2px">
                                    {!!trans('posts::posts.facebook_image_info')!!}
                                </div>
                            </div>
                        </div>
                    </div> <!-- / .tab-pane -->
                    <div class="tab-pane fade" id="twitter-tab">
                        <div class="form-group">
                            <label for="twitter_title" class="col-sm-3 control-label" style="padding-top: 7px;">{!!Lang::get('posts::posts.meta_title')!!}</label>
                            <div class="col-sm-9">
                                {!! Form::text("meta[twitter_title]", string_sanitize(Input::old('meta[twitter_title]', @$meta_robots->twitter_title)), array('class' => 'form-control input-lg', 'id' => 'twitter_title', 'placeholder' => Lang::get('posts::posts.meta_title'))) !!}
                                <div style="font-size: 14px; margin-top: 2px">
                                    {!!trans('posts::posts.twitter_title_info')!!}
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="twitter_description" class="col-sm-3 control-label" style="padding-top: 7px;">{!!Lang::get('posts::posts.meta_description')!!}</label>
                            <div class="col-sm-9">
                                {!! Form::textarea("meta[twitter_description]", string_sanitize(Input::old('meta[twitter_description]', @$meta_robots->twitter_description)), ['class' => 'form-control', 'rows' => 4, 'id' => 'twitter_description', 'style' => 'resize: vertical;', 'placeholder' => Lang::get('posts::posts.meta_description')]) !!}
                                <div style="font-size: 14px; margin-top: 2px">
                                    {!!trans('posts::posts.twitter_descrip_info')!!}
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="twitter_image" class="col-sm-3 control-label" style="padding-top: 7px;">{!!Lang::get('posts::posts.image')!!}</label>
                            <div class="col-sm-9" style=" padding-top:7px; position:relative;">
                                <span>
                                    <input type="hidden" value="{!!@$meta_robots->twitter_image!!}" id="twitter_photo" name="meta[twitter_image]">
                                    <img src="{{{ @$meta_robots->twitter ? @$meta_robots->twitter->getImageURL('small') : assets("images/default.png") }}}" height="108px" id="twitter_image" @if(!@$meta_robots->twitter_image) style="display:none" @endif>       
                                         <button type="button" class="close" id="remove_tw_image" style="position:absolute; @if(DIRECTION == 'rtl') left:14px; @else right:14px @endif top:0; @if(!@$meta_robots->twitter_image) display:none @endif"><i class="fa fa-times"></i></button>                                         
                                    <a href="#" id="remove-twitter-image" @if(!@$meta_robots->twitter_image) style="display:none" @else style="display:block" @endif>{!!Lang::get('posts::posts.remove_fb_image')!!}</a>
                                    <a href="#" id="set-twitter-image" @if(!@$meta_robots->twitter_image) style="display:block" @else style="display:none" @endif>{!!Lang::get('posts::posts.set_fb_image')!!}</a>
                                </span>
                                <div style="font-size: 14px; margin-top: 2px">
                                    {!!trans('posts::posts.twitter_image_info')!!}
                                </div>
                            </div>
                        </div>
                    </div> <!-- / .tab-pane -->
                </div> <!-- / .tab-content -->

            </div>
        </div>
    </div>
</div>

@section("footer")
@parent
<script>
    $(document).ready(function () {
        
        $("#metakeywords").tagit({
            singleField: true,
            singleFieldNode: $('#meta_keywords'),
            allowSpaces: true,
            minLength: 2,
            placeholderText: "{!!Lang::get('posts::posts.meta_keywords')!!}",
            removeConfirmation: true,
            tagSource: function (request, response) {
                $.ajax({
                    url: "<?php echo route("google.search"); ?>",
                    data: {term: request.term},
                    dataType: "json",
                    success: function (data) {
                        console.log(data);
                        response($.map(data, function (item) {
                            return {
                                label: item.name,
                                value: item.name
                            }
                        }));
                    }
                });
            }
        });

        $("#metafocus").tagit({
            singleField: true,
            singleFieldNode: $('#focus_keyword'),
            allowSpaces: true,
            minLength: 2,
            tagLimit: 1,
            placeholderText: "{!!Lang::get('posts::posts.focus_keyword')!!}",
            removeConfirmation: true,
            tagSource: function (request, response) {
                $.ajax({
                    url: "<?php echo route("google.search"); ?>",
                    data: {term: request.term},
                    dataType: "json",
                    success: function (data) {
                        console.log(data);
                        response($.map(data, function (item) {
                            return {
                                label: item.name,
                                value: item.name
                            }
                        }));
                    }
                });
            }
        });

        $("#remove-facebook-image").click(function (e) {
            e.preventDefault();
            $("#facebook_photo").val("");
            $(this).hide();
            $("#facebook_image").hide();
            $("#remove_fb_image").hide();
            $("#set-facebook-image").css('display', 'block');
            $("#facebook_image").attr("src", '');
        });
        $("#set-facebook-image").filemanager({
            types: "image",
            done: function (files) {
                if (files.length) {
                    var file = files[0];
                    $("#facebook_image").show();
                    $("#set-facebook-image").hide();
                    $("#remove-facebook-image").css('display', 'block');
                    $("#remove_fb_image").show();
                    $("#facebook_photo").val(file.media_id);
                    $("#facebook_image").attr("src", AMAZON_URL + file.media_small_path);
                }
            },
            error: function (media_path) {
                alert(media_path + is_not_an_image_lang);
            }
        });
        $("#remove_fb_image").click(function (e) {
            e.preventDefault();
            $("#remove-facebook-image").click();
        });
        $("#remove-twitter-image").click(function (e) {
            e.preventDefault();
            $("#twitter_photo").val("");
            $(this).hide();
            $("#twitter_image").hide();
            $("#remove_tw_image").hide();
            $("#set-twitter-image").css('display', 'block');
            $("#twitter_image").attr("src", '');
        });
        $("#set-twitter-image").filemanager({
            types: "image",
            done: function (files) {
                if (files.length) {
                    var file = files[0];
                    $("#twitter_image").show();
                    $("#set-twitter-image").hide();
                    $("#remove-twitter-image").css('display', 'block');
                    $("#remove_tw_image").show();
                    $("#twitter_photo").val(file.media_id);
                    $("#twitter_image").attr("src", AMAZON_URL + file.media_small_path);
                }
            },
            error: function (media_path) {
                alert(media_path + is_not_an_image_lang);
            }
        });
        $("#remove_tw_image").click(function (e) {
            e.preventDefault();
            $("#remove-twitter-image").click();
        });

        @if (isset($id))
            //$('#robots_index').val("{!!@$meta_robots->robots_index!!}");
            $('input[name="meta[robots_follow]"][value="{!!@$meta_robots->robots_follow!!}"]').prop('checked', 'checked');
            //$('#in_sitemap').val("{!!@$meta_robots->in_sitemap!!}");
            $('#sitemap_priority').val("{!!round(@$meta_robots->sitemap_priority, 2)!!}");
        @endif
    });
    
    $(window).load(function () {
        $('#robots_advanced_chosen').css('width', 'inherit');
        $('#robots_advanced_chosen input').css('width', 'inherit');
        $('#metafocus li.tagit-new').css("cssText", "width: inherit !important;");
        $('#metakeywords li.tagit-new').css("cssText", "width: inherit !important;");
    });
</script>
@stop