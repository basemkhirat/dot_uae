@extends("admin::layouts.master")

<?php
$conn = Session::get('conn');

if(Input::get('cat_id')){
    $cat_name = DB::table('categories')->where('cat_id', Input::get('cat_id'))->pluck('cat_name');
}

$params = '';
foreach (Input::except('per_page', 'page') as $key => $value) {
    $params .= '&' . $key . '=' . $value;
}

$params1 = '';
foreach (Input::except('post_status') as $key => $value) {
    $params1 .= '&' . $key . '=' . $value;
}

$url_post_status = '';
$url_post_status1 = '';
$i = 0;
foreach (Input::except(['post_status', 'desked', 'submitted', 'new', 'page']) as $key => $value) {
    $url_post_status .= '&' . $key . '=' . $value;
    if ($i == 0) {
        $url_post_status1 .= '?' . $key . '=' . $value;
    } else {
        $url_post_status1 .= '&' . $key . '=' . $value;
    }
    $i++;
}
?>

<style type="text/css">
    .m-t-md {
        margin-top: 5px !important;
    }

    .selected{
        color:#555;
    }
    .PostWasViewd{background: #fff; /*color: #fff !important;*/}
    //.PostWasViewd a{color: #fff !important;}
    .PostWasViewd td{background: none !important; /*color: #fff !important;*/};


    .theme-default .table-primary .table-header, .theme-default .table-primary thead, .theme-default .table-primary thead th, .theme-default .table-primary thead tr {
        border-color: #6C7275 !important;
    }

    .theme-default .table-primary thead th, .theme-default .table-primary thead tr {
        background: none repeat scroll 0% 0% #6C7275;
    }
    .theme-default .table-primary .table-header, .theme-default .table-primary thead, .theme-default .table-primary thead th, .theme-default .table-primary thead tr {
        border-color: #6C7275 !important;
    }
    .theme-default .table-primary table {
        border-top-color: #6C7275 !important;
    }

    .table-striped > tbody > tr:nth-child(2n+1) > td, .table-striped > tbody > tr:nth-child(2n+1) > th {   
        background-color: inherit !important;
    }
    .theme-default .table-primary .table-header, .theme-default .table-primary thead, .theme-default .table-primary thead th, .theme-default .table-primary thead tr {
        border-color: #928989 !important;
    }

    .tooltip-inner {max-width:500px;}
</style>

@section("breadcrumb")
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>
            <i class="fa fa-newspaper-o faa-tada animated faa-slow"></i>
            @if(Input::get('post_type') == 'article')
            {!!Lang::get('posts::posts.articles')!!} ({!!$posts->total()!!})
            @elseif(Input::get('post_type') == 'celebrity')
            {!!Lang::get('posts::posts.celebrities')!!} ({!!$posts->total()!!})
            @else
                @if(Input::get('cat_id')) {!!$cat_name!!} ({!!$posts->total()!!}) @else {!!Lang::get('posts::posts.posts')!!} ({!!$posts->total()!!}) @endif
            @endif
        </h2>

        <ol class="breadcrumb" style="background: none;">
            <li ><a @if(! Input::has('post_status')) style="color:#4A85BC;" @endif href="{!!URL::to('/'.ADMIN.'/posts'.$url_post_status1)!!}">{!!Lang::get('posts::posts.all')!!} <span class="count">({!!$posts_count!!})</span></a></li>
            <li ><a @if( Input::get('post_status') == 1) style="color:#4A85BC;" @endif href="{!!URL::to('/'.ADMIN.'/posts'.'?post_status=1'.$url_post_status)!!}">{!!Lang::get('posts::posts.published')!!} <span>({!!$posts_published!!})</span></a></li>
            <li ><a @if( Input::get('new') == 1) style="color:#4A85BC;" @endif href="{!!URL::to('/'.ADMIN.'/posts'.'?new=1'.$url_post_status)!!}">{!!Lang::get('posts::posts.new')!!} <span>({!!$posts_new!!})</span></a></li>
            <li ><a @if( Input::get('post_status') == 6) style="color:#4A85BC;" @endif href="{!!URL::to('/'.ADMIN.'/posts'.'?post_status=6&'.$url_post_status)!!}">{!!Lang::get('posts::posts.scheduled')!!} <span>({!!$posts_scheduled!!})</span></a></li>
            <li ><a @if( Input::get('desked') == 1) style="color:#4A85BC;" @endif href="{!!URL::to('/'.ADMIN.'/posts'.'?desked=1'.$url_post_status)!!}">{!!Lang::get('posts::posts.desked')!!} <span>({!!$posts_desked!!})</span></a></li>
            <li ><a @if( Input::get('submitted') == 1) style="color:#4A85BC;" @endif href="{!!URL::to('/'.ADMIN.'/posts'.'?submitted=1'.$url_post_status)!!}">{!!Lang::get('posts::posts.submitted')!!} <span>({!!$posts_submitted!!})</span></a></li>
            <li ><a @if( Input::get('post_status') == 4) style="color:#4A85BC;" @endif href="{!!URL::to('/'.ADMIN.'/posts'.'?post_status=4'.$url_post_status)!!}">{!!Lang::get('posts::posts.draft')!!} <span>({!!$posts_draft!!})</span></a></li>
            <li ><a @if( Input::get('post_status') == 5) style="color:#4A85BC;" @endif href="{!!URL::to('/'.ADMIN.'/posts'.'?post_status=5'.$url_post_status)!!}">{!!Lang::get('posts::posts.trashed')!!} <span>({!!$posts_trash!!})</span></a></li>
        </ol>

    </div>
    @if(User::can('posts.create'))
    <div class="col-lg-2">
        <a class="btn btn-primary btn-labeled btn-main pull-right" href="{!!URL::to('/'.ADMIN.'/posts/create?cat_id='.Input::get('cat_id').'&post_type='.Input::get('post_type'))!!}"> <span class="btn-label icon fa fa-plus"></span> 
            @if( Input::get('post_type') == 'article')
            {!!Lang::get('posts::posts.new_article')!!}
            @else
            {!!Lang::get('posts::posts.new_post')!!}
            @endif
        </a>
    </div>
    @endif
</div>
@stop
@section("content")

@if(Session::has('message') && Session::get('message') == 'success')
<div class="alert alert-success alert-dark">
    <button type="button" class="close" data-dismiss="alert">x</button>
    <strong>{!!Lang::get('posts::posts.well_done')!!}</strong> {!!Lang::get('posts::posts.success_update')!!}
</div>
@elseif(Session::has('message') && Session::get('message') == 'found')
<div class="alert alert-danger alert-dark">
    <button type="button" class="close" data-dismiss="alert">x</button>
    <strong>{!!Lang::get('posts::posts.well_done')!!}</strong> {!!Lang::get('posts::posts.repeated')!!}
</div>
@endif

<div id="content-wrapper" class="chosen-rtl">

    <div class="row">

        <div class="col-lg-8">

            {!! Form::open(array('url' => ADMIN.'/posts', 'method' => 'get', 'style' => 'display: inline-block;', 'id' => 'order-form')) !!}

            <select name="order_by" id="order_by" class="form-control" style="width:auto; display: inline-block;">
                <option value="">{!!Lang::get('posts::posts.order_by')!!}</option>
                <option value="most">{!!Lang::get('posts::posts.most_viewed')!!}</option>
                <option value="published">{!!Lang::get('posts::posts.last_publishe')!!}</option>
                <option value="updated">{!!Lang::get('posts::posts.last_update')!!}</option>
                <option value="new">{!!Lang::get('posts::posts.newest')!!}</option>
                <option value="old">{!!Lang::get('posts::posts.oldest')!!}</option>
            </select>

            @if(Input::has('translated'))
            {!! Form::hidden('translated', Input::get('translated')) !!}
            @endif

            @if(Input::has('post_type'))
            {!! Form::hidden('post_type', Input::get('post_type')) !!}
            @endif

            @if(Input::has('cat_id'))
            {!! Form::hidden('cat_id', Input::get('cat_id')) !!}
            @endif

            @if(Input::has('user_id'))
            {!! Form::hidden('user_id', Input::get('user_id')) !!}
            @endif

            @if(Input::has('new'))
            {!! Form::hidden('new', Input::get('new')) !!}
            @endif

            @if(Input::has('post_status'))
            {!! Form::hidden('post_status', Input::get('post_status')) !!}
            @endif

            @if(Input::has('tag_id'))
            {!! Form::hidden('tag_id', Input::get('tag_id')) !!}
            @endif

            @if(Input::has('sources'))
            {!! Form::hidden('sources', Input::get('sources')) !!}
            @endif

            @if(Input::has('provider'))
            {!! Form::hidden('provider', Input::get('provider')) !!}
            @endif

            @if(Input::has('q'))
            {!! Form::hidden('q', Input::get('q')) !!}
            @endif

            <button class="btn  btn-primary" type="submit" style="margin-top: -2px">{!!Lang::get('posts::posts.order')!!}</button>
            {!! Form::close() !!}
            &nbsp;
            {!! Form::open(array('url' => ADMIN.'/posts', 'method' => 'get', 'class' => '', 'style' => 'display: inline-block;', 'id' => 'filter')) !!}

            <select class="chosen-select chosen-rtl" id="cat_id" name="cat_id" style="width:150px; display: inline-block;">
                <option value="" selected="selected">— {!!Lang::get('posts::posts.conn_cats')!!} —</option>

                {!!parent_tree(buildtree($cats), '', 4, '', '')!!}

            </select>

            <select class="chosen-select chosen-rtl" id="sources" name="sources" style="width:150px; display: inline-block;">
                <option value="" selected="selected">— {!!Lang::get('posts::posts.sources')!!} —</option>

                @foreach($sources as $key => $source)
                <option @if(Input::get('sources') == $source['id']) selected="selected" @endif value="{!!$source['id']!!}">{!!$source['name']!!}</option>
                @endforeach
            </select>

            <select class="form-control" id="provider" name="provider" style="width:auto; display: inline-block;">
                <option value="" selected="selected">— {!!Lang::get('posts::posts.provider')!!} —</option>
                <option value="0">{!!Lang::get('posts::posts.news_site')!!}</option>
                <option value="1">{!!Lang::get('posts::posts.facebook')!!}</option>
                <option value="2">{!!Lang::get('posts::posts.twitter')!!}</option>
                <option value="3">{!!Lang::get('posts::posts.youtube')!!}</option>
                <option value="4">{!!Lang::get('posts::posts.instagram')!!}</option>
            </select>

            @if(Input::has('post_type'))
            {!! Form::hidden('post_type', Input::get('post_type'), array('id' => 'post_type')) !!}
            @endif

            <button class="btn  btn-primary" type="submit" style="margin-top: -2px">{!!Lang::get('posts::posts.filter')!!}</button>
            {!! Form::close() !!}

        </div>

        <div class="col-lg-4">
            {!! Form::open(array('url' => ADMIN.'/posts', 'method' => 'get', 'id' => 'search', 'class' => 'choosen-rtl', 'style' => 'margin:0')) !!}

            <div class="input-group">
                <input type="text" placeholder="{!!Lang::get('posts::posts.search')!!}" class="input-sm form-control" value="{!!(Input::get('q')) ? Input::get('q') : ''!!}" name="q" id="q"> <span class="input-group-btn">
                    <button type="submit" class="btn btn-sm btn-primary"> {!!Lang::get('posts::posts.search')!!}</button> </span>
            </div>

            @if(Input::has('post_type'))
            {!! Form::hidden('post_type', Input::get('post_type')) !!}
            @endif

            @if(Input::has('cat_id'))
            {!! Form::hidden('cat_id', Input::get('cat_id')) !!}
            @endif

            @if(Input::has('order_by'))
            {!! Form::hidden('order_by', Input::get('order_by')) !!}
            @endif

            @if(Input::has('translated'))
            {!! Form::hidden('translated', Input::get('translated')) !!}
            @endif

            @if(Input::has('user_id'))
            {!! Form::hidden('user_id', Input::get('user_id')) !!}
            @endif

            @if(Input::has('new'))
            {!! Form::hidden('new', Input::get('new')) !!}
            @endif

            @if(Input::has('post_status'))
            {!! Form::hidden('post_status', Input::get('post_status')) !!}
            @endif

            @if(Input::has('tag_id'))
            {!! Form::hidden('tag_id', Input::get('tag_id')) !!}
            @endif

            @if(Input::has('sources'))
            {!! Form::hidden('sources', Input::get('sources')) !!}
            @endif

            @if(Input::has('provider'))
            {!! Form::hidden('provider', Input::get('provider')) !!}
            @endif

            {!! Form::close() !!}
        </div>
    </div>

    {!! Form::open(array('url' =>ADMIN.'/posts/status', 'method' => 'POST', 'id' => 'form')) !!}
    <div class="row wrapper animated fadeInRight ">

        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5> {!!Lang::get('posts::posts.posts')!!} </h5>

                <div class="ibox-tools" style="margin-top: -7px;">

                    @if(Input::get('desked') || Input::get('submitted') || Input::get('new'))
                    <label><input type="checkbox" id="post_status"  value="1" name="post_status" class="js-switch" @if(Input::get('post_status'))checked @endif> <span class="lbl">&nbsp; {!!Lang::get('posts::posts.published')!!}</span></label>
                    @endif
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>

                    <a class="close-link">
                        <i class="fa fa-times"></i>
                    </a>
                </div>

            </div>

            <div class="ibox-content">
                <div class="row">
                    <div class="col-sm-10 m-b-xs">

                        <select name="action" id="action" class="form-control" style="width:auto; display: inline-block;">
                            <option value="" selected="selected">{!!Lang::get('posts::posts.bulk')!!}</option>

                            @if(Input::get('post_status') == 5)
                            @if(User::can('posts.delete') )
                            <option value="0">{!!Lang::get('posts::posts.delete')!!}</option>
                            @endif

                            @if(User::can('posts.restore') )
                            <option value="res">{!!Lang::get('posts::posts.restore')!!}</option>
                            @endif
                            @else
                            @if(User::can('posts.trash') )
                            <option value="5">{!!Lang::get('posts::posts.move_trash')!!}</option>
                            @endif
                            @endif

                        </select>

                        <button class="btn  btn-primary">{!!Lang::get('posts::posts.apply')!!}</button>

                    </div>

                    <div class="col-sm-2">

                        <select class="form-control chosen-rtl" id="per_page" name="per_page">
                            <option selected="selected" value="">-- {!!Lang::get('posts::posts.per_page')!!} --</option>
                            <option selected="selected" value="10">10</option>
                            <option value="20">20</option>
                            <option value="30">30</option>
                            <option value="40">40</option>
                        </select>
                    </div>
                </div>

                <div class="table-responsive">

                    <table cellspacing="0" cellpadding="0" border="0" class="table table-striped">
                        <thead>
                            <tr>
                                <?php
                                if (Input::get('order_by') == 'published') {
                                    $date_text = Lang::get('posts::posts.last_published');
                                } elseif (Input::get('order_by') == 'updated') {
                                    $date_text = Lang::get('posts::posts.last_updated');
                                } elseif (Input::get('order_by') == 'new') {
                                    $date_text = Lang::get('posts::posts.last_created');
                                } elseif (Input::get('order_by') == 'old') {
                                    $date_text = Lang::get('posts::posts.last_created');
                                } else {
                                    $date_text = Lang::get('posts::posts.last_updated');
                                }
                                ?>
                                <th><div class="action-checkbox"><label class="px-single"><input type="checkbox" name="checkall" id="checkall" value="" class="i-checks"><span class="lbl"></span></label></div></th>
                                <th>{!!Lang::get('posts::posts.title')!!}</th>
                                <th>{!!Lang::get('posts::posts.status')!!}</th>
                                <th>{!!Lang::get('posts::posts.author')!!}</th>
                                <th >{!!Lang::get('posts::posts.categories')!!}</th>
                                <th >{!!Lang::get('posts::posts.tags')!!}</th>
                                <th><i class="fa fa-eye" style="font-size: 20px;"></i> </th>
                                <th><i class="fa fa-comments" style="font-size: 20px;"></i></th>
                                <th>{!!Lang::get('posts::posts.seo')!!}</th>
                                <th >{!!$date_text!!}</th>
                                <th>{!!Lang::get('posts::posts.publish')!!}</th>
                            </tr>
                        </thead>
                        <tbody >

                            <?php $count = count($posts); ?>
                            @if($count)
                            @foreach($posts as $post)

                            <?php
                            $seo_box = publish_box($post->meta);
                            $post_tags = null;
                            $post_cats = null;
                            $i = 0;
                            foreach ($post->tags as $tag) {
                                if ($i == 0)
                                    $post_tags .= $tag->tag_name;
                                else
                                    $post_tags .= "," . $tag->tag_name;

                                $i++;
                            }
                            $i = 0;
                            foreach ($post->categories as $cat) {
                                if ($i == 0)
                                    $post_cats .= $cat->cat_id;
                                else
                                    $post_cats .= "," . $cat->cat_id;

                                $i++;
                            }
                            ?>

                            <tr class="odd gradeX @if($post->seen == '0') PostWasViewd @endif" data-id="{!!$post->post_id!!}" data-title="{!!$post->post_title!!}" data-slug="{!!$post->post_slug!!}" data-tags="{!!$post_tags!!}" data-cats="{!!$post_cats!!}" data-status="{!!$post->post_status!!}" data-desked="{!!$post->post_desked!!}" data-submitted="{!!$post->post_submitted!!}" data-published="{!!date('Y-m-d H:i:s', strtotime($post->post_published_date))!!}">
                                <td>
                                    <?php
                                    $filePath = getcwd() . DIRECTORY_SEPARATOR . "opened" . DIRECTORY_SEPARATOR . $post->post_id . ".json";
                                    $to_time = strtotime(date('Y-m-d H:i:s'));
                                    $from_time = strtotime($post->opened_date);
                                    $min = round(abs($to_time - $from_time) / 60, 2);
                                    ?>
                                    @if(!file_exists( $filePath ) || $min > 15)	
                                    @if(!User::can('posts.close_post') && $post->post_closed == 1)
                                    <i class="fa fa-unlock-alt"></i>
                                    @else
                                    <div class="action-checkbox"><label class="px-single"><input type="checkbox" name="check[]" value="{!!$post->post_id!!}" class="i-checks"><span class="lbl"></span></label></div>
                                    @endif
                                    @else
                                    <i class="fa fa-unlock-alt"></i>
                                    @endif
                                </td>
                                <td>
                                    @if(Input::get('post_status') == 5 || !User::can('posts.edit'))
                                    <span class="title">{!!$post->post_title!!}</span>
                                    @else
                                    @if(!file_exists( $filePath ) || $min > 15)
                                    @if(!User::can('posts.close_post') && $post->post_closed == 1)
                                    <span class="title">{!!$post->post_title!!}</span> <br><span style="color: green;font-size: 10px;"> ( {!!Lang::get('posts::posts.closed_post')!!} )</span>
                                    @else
                                    <a class="tooltip-info" href="{!!URL::to('/'.ADMIN.'/posts/'.$post->post_id.'/edit?cat_id='.Input::get('cat_id').'&post_type='.Input::get('post_type'))!!}" data-toggle="tooltip" data-placement="top" data-content="{!!$post->post_id!!}" data-title="" data-original-title=""> <span class="title">{!!$post->post_title!!}</span></a>
                                    @endif
                                    @else
                                    @if(!User::can('posts.unlock_post'))
                                    @else
                                    <a class="tooltip-info" href="{!!URL::to('/'.ADMIN.'/posts/'.$post->post_id.'/edit?cat_id='.Input::get('cat_id').'&post_type='.Input::get('post_type'))!!}" data-toggle="tooltip" data-placement="top" data-content="{!!$post->post_id!!}" data-title="" data-original-title=""> <span class="title">{!!$post->post_title!!}</span></a> <br><span style="color: green;font-size: 10px;"> ( {!!Lang::get('posts::posts.by')!!}: {!!file_get_contents($filePath)!!} )</span>
                                    @endif
                                    @endif
                                    @endif
                                    <div class="tooltip-demo m-t-md">
                                        @if(User::can('posts.restore') )

                                        @if(Input::get('post_status') == 5)
                                        <a class="btn btn-white btn-sm" data-toggle="tooltip" data-placement="top" href="{!!URL::to('/'.ADMIN.'/posts/status/'.$post->post_id.'/res')!!}" title="{!!Lang::get('posts::posts.restore')!!}"><i class="fa fa-reply"></i></a>
                                        @endif
                                        @endif

                                        @if(Input::get('post_status') == 5)
                                        @if(User::can('posts.delete') )
                                        <a data-toggle="tooltip" data-placement="top" data-type="delete" href="{!!URL::to('/'.ADMIN.'/posts/'.$post->post_id.'/delete')!!}"  class="btn btn-white btn-sm trash_post" message="{!!Lang::get('posts::posts.delete_post_q')!!}" title="{!!Lang::get('posts::posts.delete')!!}"><i class="fa fa-trash-o"></i></a>
                                        @endif
                                        @else
                                        @if(User::can('posts.trash') )
                                        @if(!file_exists( $filePath ) || $min > 15)
                                        @if((User::can('posts.close_post') && $post->post_closed == 1) || $post->post_closed != 1)
                                        <a data-toggle="tooltip" data-placement="top" data-type="trash" href="{!!URL::to('/'.ADMIN.'/posts/status/'.$post->post_id.'/5')!!}"  class="btn btn-white btn-sm trash_post" message="{!!Lang::get('posts::posts.trash_post_q')!!}" title="{!!Lang::get('posts::posts.trash_post')!!}"><i class="fa fa-trash-o"></i></a>
                                        @endif
                                        @endif
                                        @endif
                                        @endif

                                        @if(Input::get('post_status') != 5)             
                                        <a class="btn btn-white btn-sm" data-toggle="tooltip" data-placement="top" href="{!! URL::to('/details/') !!}/{{($post->mongo_id) ? $post->mongo_id : $post->post_id}}" target="_blank" title="{!!Lang::get('posts::posts.preview_changes')!!}"><i class="fa fa-eye"></i></a>
                                        <a class="btn btn-white btn-sm quick-edit" data-toggle="tooltip" data-placement="top" href="#" title="{!!Lang::get('posts::posts.quick_edit')!!}"><i class="fa fa-pencil"></i></a>
                                        @endif


                                    </div>
                                </td>
                                <td>
                                    <span class="status-wrapp">
                                        @if($post->post_status == 1)
                                        {!!Lang::get('posts::posts.published')!!}
                                        @elseif($post->post_status == 6)
                                        {!!Lang::get('posts::posts.scheduled')!!}
                                        @elseif($post->post_status == 4)
                                        {!!Lang::get('posts::posts.draft')!!}
                                        @elseif($post->post_status == 5)
                                        {!!Lang::get('posts::posts.trashed')!!}
                                        @elseif($post->post_desked == 1)
                                        {!!Lang::get('posts::posts.desked')!!}
                                        @elseif($post->post_submitted == 1)
                                        {!!Lang::get('posts::posts.submitted')!!}
                                        @elseif(!$post->post_submitted && !$post->post_desked && $post->post_status == 0)
                                        {!!Lang::get('posts::posts.new')!!}
                                        @endif
                                    </span>
                                </td>
                                <td>
                                    @if($post->first_name)
                                    <a href="{!!URL::to('/'.ADMIN.'/posts'.'?user_id='.$post->post_editor_id.'&cat_id='.Input::get('cat_id').'&post_type='.Input::get('post_type'))!!}">  {!!$post->first_name!!} {!!$post->last_name!!} </a>
                                    @else 
                                    — 
                                    @endif
                                </td>

                                <td class="center cats-td">
                                    <?php $catsArr = $post->categories; ?>
                                    @if(count($catsArr))
<!--                                    <ul class="tag-list pull-left" style="padding: 0">-->
                                        @foreach($catsArr as $cat)
                                        <a href="{!!URL::to('/'.ADMIN.'/posts?cat_id='.$cat->cat_id.'&post_type='.Input::get('post_type'))!!}"><span class="badge badge-default">{!!$cat->cat_name!!}</span></a>
                                        @endforeach
                                    <!--</ul>-->
                                    @else
                                    —
                                    @endif
                                </td>
                                <td class="center tags-td">
                                    <?php $tags = $post->tags; ?>
                                    @if(count($tags))
<!--                                    <ul class="tag-list pull-left" style="padding: 0">-->
                                        @foreach($tags as $tag)
                                        <a href="{!!URL::to('/'.ADMIN.'/posts?tag_id='.$tag->tag_id.'&cat_id='.Input::get('cat_id').'&post_type='.Input::get('post_type'))!!}"><span class="badge badge-default">{!!$tag->tag_name!!}</span></a>
                                        @endforeach
                                    <!--</ul>-->
                                    @else
                                    —
                                    @endif
                                </td>
                                <td class="center" >
                                    <span class="label label-pa-purple">{!!$post->post_views!!}</span>
                                </td>
                                <td class="center" >
                                    <!--<a href="{!!URL::to('admin/comments?post_id='.$post->post_id)!!}"></a>-->
                                    <span class="label label-pa-purple">{!!$post->comments->count()!!}</span>
                                </td>
                                <td>
                                    {!!$seo_box!!}
                                </td>
                                <?php
                                if (Input::get('order_by') == 'published') {
                                    $date = $post->post_published_date;
                                } elseif (Input::get('order_by') == 'updated') {
                                    $date = $post->post_updated_date;
                                } elseif (Input::get('order_by') == 'new') {
                                    $date = $post->post_created_date;
                                } elseif (Input::get('order_by') == 'old') {
                                    $date = $post->post_created_date;
                                } else {
                                    $date = $post->post_updated_date;
                                }
                                ?>
                                <!--<td class="center" style="min-width: 150px; color: #555;">{!! date('M d, Y', strtotime($date)) !!} @ {!! date('h:i a', strtotime($date)) !!}</td>-->
                                <td class="center">@if(DIRECTION == 'rtl') {!!arabic_date($date)!!} @else {!! date('M d, Y', strtotime($date)) !!} @ {!! date('h:i a', strtotime($date)) !!} @endif</td>

                                <td><label><input type="checkbox" value="1" name="pin" data-id="{!!$post->post_id!!}" class="js-switch pin" @if($post->pin)checked @endif> </label></td>
                            </tr>
                            @endforeach
                            @else
                            <tr class="odd gradeX" style="height:200px">
                                <td colspan="12" style="vertical-align:middle; text-align:center; font-weight:bold; font-size:22px">{!!Lang::get('posts::posts.no_posts')!!}</td>
                            </tr>
                            @endif
                        </tbody>

                    </table>
                    <div class="panel-footer text-right" style="border-top: 1px solid #ececec; background: none;">
                        <div class="col-sm-2 m-b-xs" style="padding:0">
                            @if($count)
                            {!!Lang::get('posts::posts.showing')!!} {!!$posts->firstItem()!!} {!!Lang::get('posts::posts.to')!!} {!!$posts->lastItem()!!} {!!Lang::get('posts::posts.of')!!} {!!$posts->total()!!} {!!Lang::get('posts::posts.post')!!}
                            @endif
                        </div>

                        <div class="col-sm-10 m-b-xs" style="padding:0">
                            <div class="pull-right">
                                @if($count)
                                {!!$posts->appends(Input::all())->setPath('')->render()!!}
                                @endif
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

    </div>
</div>
{!! Form::close() !!}

<div style="display:none" id="quick-wrapper">
    <div style="display:none" id="quick-content">
        <form id="quick-form" method="post" action="">
            <div class="col-sm-5 m-b-xs">
                <div class="form-group" style="margin-bottom: 5px;">
                    <input id="title" name="title" value="" class="form-control input-lg" placeholder="<?php echo trans("posts::posts.title") ?>" />
                    <label for="post_title" class="error" style="display:none">{!!Lang::get('posts::posts.repeated')!!}</label>
                </div>

                <div class="form-group" style="margin-bottom: 5px;">
                    <input id="slug" name="slug" value="" class="form-control input-lg" placeholder="<?php echo trans("posts::posts.slug") ?>" />
                </div>
                <div class="form-group" style="margin-bottom: 5px;"s>
                    <textarea aria-invalid="false" aria-required="true" placeholder="<?php echo trans("posts::posts.tags") ?>" class="form-control valid" id="tags" rows="1" style="resize:vertical" name="tags" cols="50"></textarea>
                    <span style="display:block">{!!Lang::get('posts::posts.separate_tags')!!}</span>
                </div>
            </div>
            <div class="col-sm-4 m-b-xs" id="quick-cats">
                <div id="treeOne">
                    @if(count($cats))
                    {!!cats_tree(buildtree($cats), '', 4, ['checkbox_name' => 'cats'])!!}
                    @else
                    {!!Lang::get('posts::posts.no_categories')!!}
                    @endif
                </div>
            </div>
            <div class="col-sm-3 m-b-xs">

                <div>
                    <label style="display: block;padding-bottom: 5px;"><input type="checkbox" class="i-checks" @if(! User::can('posts.pending')) disabled @endif name="post_submitted" value="1"> <span class="lbl" >&nbsp;&nbsp;{!!Lang::get('posts::posts.submitted')!!}</span></label>
                    <label style="display: block;padding-bottom: 5px; "><input type="checkbox" class="i-checks" @if(! User::can('posts.reviewed')) disabled @endif name="post_desked" value="1"> <span class="lbl" >&nbsp;&nbsp;{!!Lang::get('posts::posts.desked')!!}</span></label>
                    <label style="display: block;padding-bottom: 5px;"><input type="checkbox" class="i-checks" name="scheduled" value="6"> <span class="lbl">&nbsp;&nbsp;{!!Lang::get('posts::posts.scheduled')!!}</span></label>
                    <hr style="margin: 10px 0;">
                    <label><input type="checkbox" id="status" value="1" name="status" class="js-switch"> <span class="lbl">{!!Lang::get('posts::posts.published')!!}</span></label>            
                </div>
                <div style="display: none" id="scheduled-wrap">
                    <div class="pull-left form-group" id="input-daterange" style="width: 100%; margin: 10px 0 0;">
                        <div class="pull-left add-on" style="width:100%">
                            {!! Form::text('datepicker', '', array('class' => 'form-control', 'id' => 'bs-datepicker-example', 'placeholder' => Lang::get('posts::posts.last_published'))) !!}
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 m-b-xs">
                <a class="btn btn-primary save_manshet" id="submit-edit">{!!Lang::get('posts::posts.save')!!}</a>
                <a class="btn btn-default save_manshet" id="cancel-edit">{!!Lang::get('posts::posts.cancel')!!}</a>
            </div>
        </form>
    </div>
</div>

<style>
    label[for="cats[]"]{
        width: 150px;
        display: block;
        max-width: inherit;
        position: absolute;
        right: 120px;
    }
    #treeOne{
        height:150px;
        background-color: #fff;
    }
    @if(DIRECTION == 'rtl')
    #treeOne ul{
        padding-right:20px;
    }
    .daredevel-tree li span.daredevel-tree-anchor{
        right:-16px;
    }
    @else
    #treeOne ul{
        padding-left:20px;
    }
    @endif
</style>

@section('footer')
<script type="text/javascript" src="<?php echo assets() ?>/minified/jquery.tree.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo assets() ?>/minified/jquery.tree.min.css" />

<script src="<?php echo assets() ?>/js/bootbox.min.js"></script>
<script src="<?php echo assets() ?>/js/plugins/validate/jquery.validate.min.js"></script>
<script src="<?php echo assets() ?>/js/plugins/datapicker/bootstrap-datetimepicker.min.js"></script>
<link href="<?php echo assets() ?>/css/plugins/datapicker/bootstrap-datetimepicker.min.css" rel="stylesheet">

<script type="text/javascript">
var canPub = '{!! User::can("posts.published") !!}';
var canUnpub = '{!! User::can("posts.unpublish") !!}';


    $(window).load(function () {
        $('#input-daterange').datetimepicker({
            format: 'yyyy-MM-dd hh:mm:ss',
            language: 'pt-BR'
        });

//        $(".tooltip-info").hover(function (e) {
//            var tooltip = $(this);
//            var post_id = tooltip.attr('data-content');
//            ajaxData = {
//                post_id: post_id,
//            };
//            $.ajax({
//                type: "POST",
//                dataType: 'json',
//                url: '{!! URL::to("/".ADMIN."/posts/revisionpost") !!}',
//                data: ajaxData,
//                beforeSend: function (res) {
//
//                },
//                success: function (res) {
//                    var text = '';
//                    $.each(res, function (i, object) {
//                        post_status = '';
//                        console.log(object);
//                        if (object.post_status == 0) {
//                            post_status = "{!!Lang::get('posts::posts.unpublish_post')!!}";
//                        } else if (object.post_status == 1) {
//                            post_status = "{!!Lang::get('posts::posts.published')!!}";
//                        } else if (object.post_status == 2) {
//                            post_status = "{!!Lang::get('posts::posts.pending')!!}";
//                        } else if (object.post_status == 3) {
//                            post_status = "{!!Lang::get('posts::posts.reviewed')!!}";
//                        } else if (object.post_status == 4) {
//                            post_status = "{!!Lang::get('posts::posts.draft')!!}";
//                        } else if (object.post_status == 5) {
//                            post_status = "{!!Lang::get('posts::posts.trashed')!!}";
//                        } else if (object.post_status == 6) {
//                            post_status = "{!!Lang::get('posts::posts.scheduled')!!}";
//                        }
//                        text += "<p>" + object.user.first_name + " " + (object.user.last_name ? object.user.last_name : '') + " | " + post_status + " | " + object.revision_date + "</p>";
//                    });
//                    tooltip.attr('data-original-title', text);
//                    tooltip.tooltip('show');
//                },
//                complete: function () {
//
//                }
//            });
//        },
//        function (e) {
//            $(".tooltip-info").tooltip('hide');
//        });
    });

    var baseURL = '{!! URL::to("/".ADMIN) !!}/';
    $(document).ready(function () {

        $('#per_page').change(function () {
            location.href = '{!!URL::to(ADMIN."/posts")!!}' + '?per_page=' + $(this).val() + '{!!$params!!}'
        });

        $('#per_page').val("{!!Input::get('per_page')!!}");

        // check all action
        $('#checkall').on('ifChecked', function (event) {
            $("input[name='check[]']").each(function () {
                $(this).iCheck('check');
            });
        });
        $('#checkall').on('ifUnchecked', function (event) {
            $("input[name='check[]']").each(function () {
                $(this).iCheck('uncheck');
            });
        });
        $('.i-checks').iCheck({
            checkboxClass: 'icheckbox_square-green',
            radioClass: 'iradio_square-green',
        });

        var statusSwitch = new Switchery(document.querySelector('#status'), {color: '#1AB394'});
        @if(Input::get('desked') || Input::get('submitted') || Input::get('new'))
        var publishSwitch = new Switchery(document.querySelector('#post_status'), {color: '#1AB394'});
        @endif
        var elems = Array.prototype.slice.call(document.querySelectorAll('.pin'));
        elems.forEach(function (html) {
            var pinSwitch = new Switchery(html, {color: '#1AB394'});
        });

        $("#form").submit(function () {
            if ($("input[name='check[]']:checked").length == 0 || $("#action").val() == '') {
                return false;
            }
        });

        $("#filter").submit(function (e) {
            if ($("#cat_id").val() == '' && $("#sources").val() == '' && $("#provider").val() == '') {
                return false;
            }
        });

        $("#order-form").submit(function (e) {
            if ($("#order_by").val() == '') {
                return false;
            }
        });

        $('.trash_post').on('click', function (e) {
            e.preventDefault();
            $this = $(this);
            bootbox.dialog({
                message: $this.attr('message'),
                title: $this.attr('title'),
                buttons: {
                    success: {
                        label: "{!!Lang::get('posts::posts.ok')!!}",
                        className: "btn-success",
                        callback: function () {
                            location.href = $this.attr('href');
                        }
                    },
                    danger: {
                        label: "{!!Lang::get('posts::posts.cancel')!!}",
                        className: "btn-primary",
                        callback: function () {
                        }
                    },
                },
                className: "bootbox-sm"
            });
        });

        $('#order_by').val("{!!Input::get('order_by')!!}");
        $('#cat_id').val("{!!Input::get('cat_id')!!}");
        $("#sources").val("{!!Input::get('sources')!!}");
        $("#provider").val("{!!Input::get('provider')!!}");


        $('.tooltip-info').tooltip({
            html: true,
        });

        $(".pin").change(function () {
            $this = $(this);
            ajaxData = {
                post_id: $this.attr('data-id'),
                pin: this.checked,
            };
            console.log(ajaxData);
            $.ajax({
                type: "POST",
                dataType: 'json',
                url: '{!! URL::to("/".ADMIN."/posts/pin") !!}',
                data: ajaxData,
                beforeSend: function (res) {

                },
                success: function (res) {
                    if (res == 'found') {
                        $("#post_title").parent().addClass('has-error').find('.jquery-validate-error').html("{!!Lang::get('posts::posts.repeated')!!}");
                        $("#post_title").parent().addClass('has-error').find('.jquery-validate-error').show();
                    } else {
                        $('#post-form').attr('action', '{!! URL::to("/".ADMIN."/posts/") !!}/' + res);
                        $('#post-form').append('<input name="_method" type="hidden" value="PUT">');
                        console.log(res);
                        flag_focus = false;
                    }
                },
                complete: function () {

                }
            });

        });

        $("#post_status").change(function () {
            if (this.checked) {
                location.href = '{!!URL::to(ADMIN."/posts")!!}' + '?post_status=1' + '{!!$params1!!}'
            } else {
                location.href = '{!!URL::to(ADMIN."/posts")!!}' + '?post_status=0' + '{!!$params1!!}'
            }
        });


        var config = {
            '.chosen-select': {},
            '.chosen-select-deselect': {allow_single_deselect: true},
            '.chosen-select-no-single': {disable_search_threshold: 10},
            '.chosen-select-no-results': {no_results_text: "{!!Lang::get('posts::posts.notfound')!!}"},
            '.chosen-select-width': {width: "95%"}
        }
        for (var selector in config) {
            $(selector).chosen(config[selector]);
        }
        
//        var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
//        elems.forEach(function (html) {
//            var switchery = new Switchery(html, {color: '#1AB394'});
//        });
        
        // search submit
        $("#search").submit(function (e) {
            if ($("#q").val() == '') {
                return false
            }
        });
        
        // quick edit
        var parent = '';
        var editBefore = false;
        $(".quick-edit").click(function (e) {
            e.preventDefault();

            if (editBefore) {
                reset_parent();
            }

            parent = $(this).parents('tr');
            parent.hide();
            title = parent.attr("data-title");
            slug = parent.attr("data-slug");
            tags = parent.attr("data-tags");
            cats = parent.attr("data-cats");
            cats = cats.split(",");
            status = parent.attr("data-status");
            desked = parent.attr("data-desked");
            submitted = parent.attr("data-submitted");
            date = parent.attr("data-published");
            parent.after('<tr style="background-color: #F9F9F9 !important;" id="quick-tr" ><td colspan="12">')

            $.each(cats, function () {
                $("input[name='cats[]'][value='" + this + "']").iCheck('check');
            });
            $('#quick-content #title').val(title);
            $('#quick-content #slug').val(slug);
            $('#quick-content #tags').val(tags);
            if(status == 1 && !$("#status").is(":checked"))
                $("#status").next().click();
            
            if(status == 6){
                $("input[name='scheduled']").iCheck('check');
                $("#bs-datepicker-example").val(date);
            }
            
            if(!canPub || (!canUnpub && status == 1)){
                // disable
                $("input[name='scheduled']").iCheck('disable');
                statusSwitch.disable();
            }
            
            if(desked == 1)
                $("input[name='post_desked']").iCheck('check');
            
            if(submitted == 1)
                $("input[name='post_submitted']").iCheck('check');
            
            $('#quick-content').show().appendTo($("#quick-tr td"));
            editBefore = true;
        });

        $("#cancel-edit").click(function (e) {
            e.preventDefault();
            reset_parent();
        });

        $("#submit-edit").click(function (e) {
            e.preventDefault();
            $("#quick-form").submit();
        });

//        $("#quick-form").submit(function (e) {
//            e.preventDefault();
//        });

        function reset_parent() {
            $.each($("input[name='cats[]']"), function () {
                $(this).iCheck('uncheck');
            });
            $('#quick-content').show().appendTo($("#quick-tr td"));
            $('#quick-content #title').val('').next('.error').hide();
            $('#quick-content #slug').val('').next('.error').hide();
            $('#quick-content #tags').val('').removeClass('error').next('.error').hide();
            $("input[name='cats[]']:first").next('.error').hide();
            if($("#status").is(":checked"))
                $("#status").next().click();
                
            $("input[name='scheduled']").iCheck('uncheck');
            $("input[name='scheduled']").iCheck('enable');
            statusSwitch.enable();
            $("#bs-datepicker-example").val('');
            $("input[name='post_desked']").iCheck('uncheck'); 
            $("input[name='post_submitted']").iCheck('uncheck');
            
            $('#quick-content').hide().appendTo($("#quick-wrapper"));
            parent.show().next().remove();
        }

        $("#quick-form").validate({
            ignore: [],
            focusInvalid: false,
            rules: {
                title: {
                    required: true
                },
                slug: {
                    required: true
                },
                'cats[]': {
                    required: true,
                },
                tags: {
                    required: true,
                    tagsRequired: true
                }
            },
            messages: {
                title: {
                    required: "{!!Lang::get('posts::posts.required')!!}",
                },
                slug: {
                    required: "{!!Lang::get('posts::posts.required')!!}",
                },
                'cats[]': {
                    required: "{!!Lang::get('posts::posts.required')!!}",
                },
                tags: {
                    required: "{!!Lang::get('posts::posts.required')!!}",
                    tagsRequired: "{!!Lang::get('posts::posts.min_tags')!!}",
                }
            },
            submitHandler: function () {
                $this = $("#quick-form");
                id = $this.parents('tr').prev().attr("data-id");
                title = $("#title").val();
                slug = $("#slug").val();
                tags = $("#tags").val();
                status =  ($("#status").is(":checked")) ? 1 : 0;
                scheduled = ($("input[name='scheduled']").is(":checked")) ? 6 : 0;
                desked = ($("input[name='post_desked']").is(":checked")) ? 1 : 0;
                submitted = ($("input[name='post_submitted']").is(":checked")) ? 1 : 0;
                date = $("#bs-datepicker-example").val();
//                cats = $("input[name='cats[]']").map(function() {
//                    return this.checked ? this.value : null;
//                }).get();

                cats = $("input[name='cats[]']").map(function () {
                    return this.checked ? {'id': this.value, 'name': $(this).parent().next().html()} : null;
                }).get();
                console.log(cats);
                ajaxData = {
                    title: title,
                    id: id,
                    slug: slug,
                    tags: tags,
                    cats: cats,
                    status: status,
                    scheduled: scheduled,
                    desked: desked,
                    submitted: submitted,
                    date: date
                };
                $.ajax({
                    type: "POST",
                    dataType: 'json',
                    url: baseURL + "posts/quicksave",
                    data: ajaxData,
                    beforeSend: function (res) {

                    },
                    success: function (res) {
                        $this.parents('tr').prev().find(".cats-td").html('');
                        $this.parents('tr').prev().find(".tags-td").html('');
//                        $('<ul class="tag-list pull-left" style="padding: 0"></ul>').appendTo($this.parents('tr').prev().find(".cats-td"));
//                        $('<ul class="tag-list pull-left" style="padding: 0"></ul>').appendTo($this.parents('tr').prev().find(".tags-td"));
                        tagsArr = res.tags;
                        ids = '';
                        ctotal = cats.length;
                        ttotal = tagsArr.length;
                        if (ctotal) {
                            $.each(cats, function (index) {
                                if (index === ctotal - 1) {
                                    ids += this.id;
                                    $('<a href="' + baseURL + 'posts?cat_id=' + this.id + '"><span class="badge badge-default">' + this.name + '</span></a>').appendTo($this.parents('tr').prev().find(".cats-td"));
                                } else {
                                    $('<a href="' + baseURL + 'posts?cat_id=' + this.id + '"><span class="badge badge-default">' + this.name + '</span></a>').appendTo($this.parents('tr').prev().find(".cats-td"));
                                    ids += this.id + ',';
                                }
                            });
                        }
                        if (ttotal) {
                            $.each(tagsArr, function (index) {
                                if (index === ttotal - 1) {
                                    $('<a href="' + baseURL + 'posts?tag_id=' + this.id + '"><span class="badge badge-default">' + this.name + '</span></a>').appendTo($this.parents('tr').prev().find(".tags-td"));
                                } else {
                                    $('<a href="' + baseURL + 'posts?tag_id=' + this.id + '"><span class="badge badge-default">' + this.name + '</span></a>').appendTo($this.parents('tr').prev().find(".tags-td"));
                                }
                            });
                        }
                        
                        $this.parents('tr').prev().attr("data-status", res.status);
                        $this.parents('tr').prev().find(".status-wrapp").html(res.stat);
                        $this.parents('tr').prev().attr("data-title", title);
                        $this.parents('tr').prev().find(".title").html(title);
                        $this.parents('tr').prev().attr("data-slug", res.slug);
                        $this.parents('tr').prev().attr("data-cats", ids);
                        $this.parents('tr').prev().attr("data-tags", tags);
                        $this.parents('tr').prev().attr("data-desked", desked);
                        $this.parents('tr').prev().attr("data-submitted", submitted);
                        $this.parents('tr').prev().attr("data-published", date);
                        reset_parent();
                    },
                    complete: function () {

                    }
                });
            }
        });

        $('#title').focusout(function () {
            $this = $(this);
            upper = $this.parents('tr').prev();

            if ($this.val() != '') {
                ajaxData = {
                    title: $this.val(),
                    id: upper.attr("data-id")
                };
                $.ajax({
                    type: "POST",
                    dataType: 'json',
                    url: baseURL + "posts/updatepost",
                    data: ajaxData,
                    beforeSend: function (res) {
                        $("#submit-edit").addClass('disabled');
                        $this.next().hide();
                    },
                    success: function (res) {
                        if (res == 'found') {
                            $this.next().show();
                        }
                    },
                    complete: function () {
                        $("#submit-edit").removeClass('disabled');
                    }
                });
            }
        });

        // post stats
        $("input[name='scheduled']").on('ifChanged', function (event) {
            
            if ($(this).is(":checked")) {
                $('#scheduled-wrap').show();
                if($("#status").is(":checked"))
                    $("#status").next().click();
            } else {
                $('#scheduled-wrap').hide();
            }

        });
        var clickCheckbox = document.querySelector('#status')
          , clickButton = document.querySelector('#status');

        clickButton.addEventListener('click', function() {
            if(clickCheckbox.checked){
                $("input[name='scheduled']").iCheck('uncheck');
                $('#scheduled-wrap').hide();
            }
        });
        $("input[name='post_desked']").on('ifChanged', function (event) {
            if ($(this).is(":checked")) {
                $(this).parent().next(".lbl").css('text-decoration', 'line-through');
                $("input[name='post_submitted']").parent().next(".lbl").css('text-decoration', 'line-through');
                $("input[name='post_submitted']").parent().iCheck('check');
            } else {
                $(this).parent().next(".lbl").css('text-decoration', 'inherit');
            }

        });
        $("input[name='post_submitted']").on('ifChanged', function (event) {
            if ($(this).is(":checked")) {
                $(this).parent().next(".lbl").css('text-decoration', 'line-through');
            } else {
                $(this).parent().next(".lbl").css('text-decoration', 'inherit');
            }
        });
    
        $('#treeOne').tree({
            collapseUiIcon: 'ui-icon-plus',
            expandUiIcon: 'ui-icon-minus',
            onCheck: {
                ancestors: 'check',
                descendants: null
            },
            onUncheck: {
                ancestors: null,
                descendants: null
            }
        });
//    $("#save_move").click(function (e) {
//        e.preventDefault();
//        if ($("#conn_cats").val() == '') {
//            return false;
//        }
//
//        ajaxData = {
//            conn: $("#databases").val(),
//            cat_id: $("#conn_cats").val(),
//            post_id: $("#post_temp").val(),
//            from_cat: "{!!Input::get('cat_id')!!}"
//        };
//
//        if (ajaxData.conn == 'live') {
//            ajaxData.conn = 'tahrirnews';
//        }
//
//        $.ajax({
//            type: "POST",
//            dataType: 'json',
//            url: '{!! URL::to("/".ADMIN."/posts/savemove") !!}',
//            data: ajaxData,
//            beforeSend: function (res) {
//
//            },
//            success: function (res) {
//            },
//            complete: function () {
//                $.growl.notice({title: "<strong>{!!Lang::get('posts::posts.well_done')!!}</strong> ", message: "{!!Lang::get('posts::posts.moved')!!}", duration: 2000});
//            }
//        });
//    });

//    $("#save_order").click(function (e) {
//        e.preventDefault();
//        var orders = [];
//        $("#sortable_posts div").each(function (i) {
//            orders[i] = $(this).attr("id");
//        });
//        var myJSON = JSON.stringify(orders);
//        ajaxData = {
//            posts: myJSON,
//            block_id: $("#block_id").val(),
//            post_id: $("#post_temp").val(),
//        };
//        $.ajax({
//            type: "POST",
//            dataType: 'json',
//            url: '{!! URL::to("/".ADMIN."/posts/saveorder") !!}',
//            data: ajaxData,
//            beforeSend: function (res) {
//
//            },
//            success: function (res) {
//
//            },
//            complete: function () {
//                $.growl.notice({title: "<strong>{!!Lang::get('posts::posts.well_done')!!}</strong> ", message: "{!!Lang::get('posts::posts.success_order')!!}", duration: 2000});
//            }
//        });
//    });

//    $(".immediate").click(function (e) {
//        e.preventDefault();
//        var id = $(this).attr("href").substr(1);
//        $("#post_temp").val(id);
//        var title = $(this).attr("data-name");
//        ajaxData = {
//            block_slug: 'breaking-news',
//            post_id: id,
//        };
//        $.ajax({
//            type: "POST",
//            dataType: 'json',
//            url: '{!! URL::to("/".ADMIN."/posts/orderpost") !!}',
//            data: ajaxData,
//            beforeSend: function (res) {
//                $("#sortable_posts").html('');
//                $("#orederModel").hide();
//                $("#save_order").show();
//            },
//            success: function (res) {
//                if (res == "error") {
//                    $("#error").click();
//
//                } else {
//                    $("#block_name").html("( " + res.block_name + " )");
//                    $("#block_id").val(res.block_id);
//                    $.each(res.posts, function (i, object) {
//                        $("#sortable_posts").append("<div id='" + object.post_id + "' class='group' style=''><h3 class='ui-accordion-header ui-helper-reset ui-state-default ui-accordion-header-active ui-state-active ui-corner-top ui-accordion-icons' role='tab' id='ui-accordion-sortable_posts-header-0' aria-controls='ui-accordion-sortable_posts-panel-0' aria-selected='true' aria-expanded='true' tabindex='0'><span class='ui-accordion-header-icon ui-icon ui-icon-triangle-1-s'></span>" + object.post_title + "</h3></div>");
//                    });
//                    $("#sortable_posts").prepend("<div id='" + id + "' class='group' style=''><h3 class='ui-accordion-header ui-helper-reset ui-state-default ui-accordion-header-active ui-state-active ui-corner-top ui-accordion-icons' role='tab' id='ui-accordion-sortable_posts-header-0' aria-controls='ui-accordion-sortable_posts-panel-0' aria-selected='true' aria-expanded='true' tabindex='0'><span class='ui-accordion-header-icon ui-icon ui-icon-triangle-1-s'></span>" + title + "</h3></div>");
//                    $("#order").click();
//                }
//            },
//            complete: function () {
//
//            }
//        });
//    });

//    $(".home").click(function (e) {
//        e.preventDefault();
//        var id = $(this).attr("href").substr(1);
//        $("#post_temp").val(id);
//        var title = $(this).attr("data-name");
//
//        ajaxData = {
//            block_slug: 'featured',
//            post_id: id,
//        };
//
//        $.ajax({
//            type: "POST",
//            dataType: 'json',
//            url: '{!! URL::to("/".ADMIN."/posts/orderpost") !!}',
//            data: ajaxData,
//            beforeSend: function (res) {
//                $("#sortable_posts").html('');
//                $("#orederModel").hide();
//                $("#save_order").show();
//            },
//            success: function (res) {
//                if (res == "error") {
//                    $("#error").click();
//                } else {
//                    $("#block_name").html("( " + res.block_name + " )");
//                    $("#block_id").val(res.block_id);
//                    $.each(res.posts, function (i, object) {
//                        $("#sortable_posts").append("<div id='" + object.post_id + "' class='group' style=''><h3 class='ui-accordion-header ui-helper-reset ui-state-default ui-accordion-header-active ui-state-active ui-corner-top ui-accordion-icons' role='tab' id='ui-accordion-sortable_posts-header-0' aria-controls='ui-accordion-sortable_posts-panel-0' aria-selected='true' aria-expanded='true' tabindex='0'><span class='ui-accordion-header-icon ui-icon ui-icon-triangle-1-s'></span>" + object.post_title + "</h3></div>");
//                    });
//                    $("#sortable_posts").prepend("<div id='" + id + "' class='group' style=''><h3 class='ui-accordion-header ui-helper-reset ui-state-default ui-accordion-header-active ui-state-active ui-corner-top ui-accordion-icons' role='tab' id='ui-accordion-sortable_posts-header-0' aria-controls='ui-accordion-sortable_posts-panel-0' aria-selected='true' aria-expanded='true' tabindex='0'><span class='ui-accordion-header-icon ui-icon ui-icon-triangle-1-s'></span>" + title + "</h3></div>");
//                    $("#order").click();
//                }
//            },
//            complete: function () {
//
//            }
//        });
//    });

//    $(".move").click(function (e) {
//        e.preventDefault();
//        var id = $(this).attr("href").substr(1);
//        $("#post_temp").val(id);
//        var title = $(this).attr("data-name");
//        $("#move").click();
//    });
//
//    $(".order").click(function (e) {
//        e.preventDefault();
//        var id = $(this).attr("href").substr(1);
//        $("#post_temp").val(id);
//        var title = $(this).attr("data-name");
//
//        ajaxData = {
//            cat_id: "{!!Input::get('cat_id')!!}",
//            post_id: id,
//        };
//
//        $.ajax({
//            type: "POST",
//            dataType: 'json',
//            url: '{!! URL::to("/".ADMIN."/posts/orderpost") !!}',
//            data: ajaxData,
//            beforeSend: function (res) {
//                $("#sortable_posts").html('');
//                $("#orederModel").hide();
//                $("#save_order").show();
//            },
//            success: function (res) {
//                if (res == "error") {
//                    $("#error").click();
//                } else {
//                    $("#block_name").html("( " + res.block_name + " )");
//                    $("#block_id").val(res.block_id);
//                    $.each(res.posts, function (i, object) {
//                        $("#sortable_posts").append("<div id='" + object.post_id + "' class='group' style=''><h3 class='ui-accordion-header ui-helper-reset ui-state-default ui-accordion-header-active ui-state-active ui-corner-top ui-accordion-icons' role='tab' id='ui-accordion-sortable_posts-header-0' aria-controls='ui-accordion-sortable_posts-panel-0' aria-selected='true' aria-expanded='true' tabindex='0'><span class='ui-accordion-header-icon ui-icon ui-icon-triangle-1-s'></span>" + object.post_title + "</h3></div>");
//                    });
//                    $("#sortable_posts").prepend("<div id='" + id + "' class='group' style=''><h3 class='ui-accordion-header ui-helper-reset ui-state-default ui-accordion-header-active ui-state-active ui-corner-top ui-accordion-icons' role='tab' id='ui-accordion-sortable_posts-header-0' aria-controls='ui-accordion-sortable_posts-panel-0' aria-selected='true' aria-expanded='true' tabindex='0'><span class='ui-accordion-header-icon ui-icon ui-icon-triangle-1-s'></span>" + title + "</h3></div>");
//                    $("#order").click();
//                }
//            },
//            complete: function () {
//
//            }
//        });
//    });

        //$("#conn_cats").chained("#databases"); /* or $("#series").chainedTo("#mark"); */

    });

    $(function () {
        $.validator.addMethod("tagsRequired", function (value, element) {
            tags = $("#tags").val().split(",");

            if (tags.length >= 5) {
                return true;
            } else {
                return false;
            }
        }, 'Password and Confirm Password should be same');
    });

</script>
@stop
@stop
