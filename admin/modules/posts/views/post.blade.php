@extends("admin::layouts.master")

<?php
$conn = Session::get('conn');
@$meta_robots = $post->meta;
?>

@section("breadcrumb")
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2><i class="fa fa-newspaper-o faa-tada animated faa-slow"></i> {!!Lang::get('posts::posts.posts')!!}</h2>
        <ol class="breadcrumb" style="background: none;">
            <li><a  href="{!!URL::to('/'.ADMIN.'/dashboard')!!}">{!!Lang::get('admin::common.dashboard')!!}</a></li>
            <li><a  href="{!!URL::to('/'.ADMIN.'/posts')!!}">{!!Lang::get('posts::posts.posts')!!}</a></li>
            <li>
                @if(isset($post->post_id))
                @if(Input::get('post_type') == 'article')
                {!!Lang::get('posts::posts.edit_article')!!}
                @else
                {!!Lang::get('posts::posts.edit_post')!!}
                @endif
                @else
                @if(Input::get('post_type') == 'article')
                {!!Lang::get('posts::posts.new_article')!!}
                @else
                {!!Lang::get('posts::posts.new_post')!!}
                @endif
                @endif
            </li>

        </ol>
    </div>
    @if(User::can('posts.create'))
    @if(isset($post->post_id))
    <div class="col-lg-2">
        <a class="btn btn-primary btn-labeled btn-main pull-right" href="{!!URL::to('/'.ADMIN.'/posts/create?cat_id='.Input::get('cat_id').'&post_type='.Input::get('post_type'))!!}"> <span class="btn-label icon fa fa-plus"></span> 
            @if( Input::get('post_type') == 'article')
            {!!Lang::get('posts::posts.new_article')!!}
            @else
            {!!Lang::get('posts::posts.new_post')!!}
            @endif
        </a> 
    </div>
    @endif
    @endif

</div>
@stop
@section("content")

@section("header")
<script type="text/javascript" src="<?php echo assets("ckeditor/ckeditor.js"); ?>"></script>
<link href="<?php echo assets() ?>/css/plugins/morris/morris-0.4.3.min.css" rel="stylesheet">
<link href="<?php echo assets() ?>/css/plugins/datapicker/bootstrap-datetimepicker.min.css" rel="stylesheet">
<link href="<?php echo assets() ?>/css/plugins/select2/select2.css" rel="stylesheet">
@stop

<div id="content-wrapper">

    <style type="text/css">

        .select2-results .select2-highlighted, .select2-results .select2-no-results, .select2-results .select2-searching, .select2-results .select2-ajax-error, .select2-results .select2-selection-limit{
            background: none;
        }

        .select2-container-multi .select2-choices .select2-search-field input.select2-active {
            background-position: 0 center !important;
        }
        .select2-container {
            padding: 0 !important;
            border: none !important;
        }
        .select2-container-multi .select2-search-choice-close {
            top: 2px;
            color: #FFF;
        }
        input {
            font: 13px "font towl" !important;
        }

        label[for="cats[]"]{
            width: 150px;
            display: block;
            max-width: inherit;
            position: absolute;
            right: 120px;
        }

        .ibox, #featured-wrapper, #gallery-wrapper, #media-wrapper{
            margin:0;
        }
        .panel-group, .form-group, .panel {
            margin-bottom: 10px;
        }
        .checkbox, .radio {
            margin-top: 0 !important;
        }
        .ui-widget-content {
            border: none !important;
        }
        .feed-element:last-child {
            border-bottom: none;
        }

        .close-tag{
            padding: 0 0  0 5px;
            cursor: pointer;
        }

        #tag-wrap, #tag-search, #linked-search, #auto-linked, #related-search, #auto-wrap{
            z-index: 10000;          
            border: 1px solid #E7EAEC;
            position: absolute;
            top: 34px;
            display: block;
            background-color: #fff;
            width: 100%;
            padding: 10px;
        }

/*        #tag-wrap{
            padding: 15px 10px 5px 10px;
        }

        #tag-wrap, #tag-search{
            padding: 10px;
        }*/

        .tag-list li span {
            font-size: 10px;
            background-color: #F3F3F4;
            padding: 5px 12px;
            color: inherit;
            border-radius: 2px;
            border: 1px solid #E7EAEC;
            margin-right: 5px;
            margin-top: 5px;
            display: block;
        }

        .panel .widget-tasks .task, .panel.widget-tasks .task{
            margin: 0;
        }  

        .panel-body{
            padding: 0;
        }

        .disable{
            cursor: not-allowed;
            pointer-events: none;
            opacity: .65;
            filter: alpha(opacity=65);
            -webkit-box-shadow: none;
            box-shadow: none;
        }

        #treeOne, #treeTwo, #treeThree{
            height:150px; margin-top: 10px;
        }
        @if(DIRECTION == 'rtl')
        #treeOne ul{
            padding-right:20px;
        }
        #treeThree ul{
            padding-right:20px;
        }
        .daredevel-tree li span.daredevel-tree-anchor{
            right:-16px;
        }
        @else
        #treeOne ul{
            padding-left:20px;
        }
        #treeThree ul{
            padding-left:20px;
        }
        @endif

        #treeTwo ul{

            @if(Input::has('cat_id'))
            @if(DIRECTION == 'rtl')
            padding-right:20px;
            @else
            padding-left:20px;
            @endif

            @else
            @if(DIRECTION == 'rtl')
            padding-right:20px;
            @else
            padding-left:20px;
            @endif
            @endif

        } 

        .the-tagcloud {
            margin: 5px 0 10px;
            padding: 8px;
            border: 1px solid #ddd;
            line-height: 1.8em;
            word-spacing: 3px;
            -webkit-border-radius: 6px;
            border-radius: 6px;
            //display: none;
        }
    </style>
    @if ($errors->has())
    <div class="alert alert-danger alert-dark">
        <button type="button" class="close" data-dismiss="alert">x</button>
        @foreach ($errors->all() as $error)
        <div><strong>{!!Lang::get('posts::posts.oh_snap')!!}</strong> {!! $error !!}</div>
        @endforeach
    </div>
    @endif

    @if(Session::has('message') && Session::get('message') == 'success')
    <div class="alert alert-success alert-dark">
        <button type="button" class="close" data-dismiss="alert">x</button>
        <strong>{!!Lang::get('posts::posts.well_done')!!}</strong> {!!Lang::get('posts::posts.success_update')!!}
    </div>
    @elseif(Session::has('message') && Session::get('message') == 'found')
    <div class="alert alert-danger alert-dark">
        <button type="button" class="close" data-dismiss="alert">x</button>
        <strong>{!!Lang::get('posts::posts.well_done')!!}</strong> {!!Lang::get('posts::posts.repeated')!!}
    </div>
    @endif

    @if(isset($post->post_id))
    {!! Form::open(array('url' => ADMIN.'/posts/'.$post->post_id, 'method' => 'put', 'files' => true, 'id' => 'post-form', 'novalidate' => 'novalidate')) !!}
    @else
    {!! Form::open(array('url' => ADMIN.'/posts', 'files' => true, 'id' => 'post-form', 'novalidate' => 'novalidate')) !!}
    @endif

    {!! Form::hidden('post_type', Input::get('post_type', 'post'), array('id' => 'post_type')) !!}
    {!! Form::hidden('curr_cat', Input::get('cat_id'), array('id' => 'curr_cat')) !!}
    {!! Form::hidden('has_media', Input::get('has_media', $post->has_media), array('id' => 'has_media')) !!}
    {!! Form::hidden('post_id', $post->post_id, array('id' => 'post_id')) !!}

    <div class="row">
        <div class="col-md-8">
            @include("posts::partials.rightside")
        </div>
        <!-- /6. $EASY_PIE_CHARTS -->

        <div class="col-md-4">
            @include("posts::partials.leftside")
        </div>
    </div>

</div>

{!! Form::close() !!}

<div id="modal_new_topic" class="modal fade" tabindex="-1" role="dialog" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></button>
                <h4 class="modal-title">{!! trans('topics::topics.add_new') !!}</h4>
            </div>
            <div class="modal-body">
                <!--form-->
                {!!Form::open(array(
                'route'=>array(ADMIN.'.topics.store'),
                'id'=>'quick-new-topic-form',
                'class'=>"panel form-horizontal",
                'method'=>"post"
                ))!!}

                <div class="panel-body">

                    <div class="row form-group">
                        <label class="error" id="found_topic" style="display: none">{!!trans('topics::topics.found')!!}</label>
                    </div>
                    <div class="row form-group">
                        <label class="col-sm-2">{!!trans('topics::topics.name')!!}</label>
                        <div class="col-sm-10">
                            <input type="text" name="name" class="form-control"/>
                        </div>
                    </div>

                    <div class="row form-group">
                        <label class="col-sm-2">{!!trans('topics::topics.description')!!}</label>
                        <div class="col-sm-10">
                            <textarea class="form-control" name="description"></textarea>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-md-2" >
                            <input type="hidden" id="cat_photo_id" name="photo" />
                            <a href="#" id="change_topic_photo" class="change_photo" rel="0">{!!trans('topics::topics.set_image')!!}</a>
                        </div>
                        <div class="col-sm-10">
                            <img id="topic_photo_preview" style="border: 1px solid #ccc;" width="150" height="150pa" src="{!!assets("images/default.png")!!}" />  
                        </div>
                    </div>

                    <div class="panel-footer text-right" style="border-top: 1px solid #ececec; background: none;">
                        <input id="quick-new-topic-btn" type="submit" value="{!! trans('topics::topics.save') !!}" class="btn btn-primary"/>
                    </div>
                    {!!Form::close()!!}
                    <!--form-->
                </div>
            </div> <!-- / .modal-content -->
        </div> <!-- / .modal-dialog -->
    </div> 
</div> <!-- / .modal -->

<div id="modal_polls" class="modal fade" tabindex="-1" role="dialog" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></button>
                <h4 class="modal-title">{!! trans('topics::topics.add_new') !!}</h4>
            </div>
            <div class="modal-body">
                @foreach(Poll::getActivePools() as $poll)
                <div class="dd-handle">
                    <!-- Default panel contents -->
                    <a href="#" class="embed_poll" data-id='{!!$poll->poll_id!!}'>
                        <div class="panel-heading">
                            <span class="panel-title">{!!$poll->poll_title!!}</span>
                        </div>
                    </a>
                </div>
                @endforeach
            </div> <!-- / .modal-content -->
        </div> <!-- / .modal-dialog -->
    </div> 
</div> <!-- / .modal -->

@section("footer")
<!-- include plugin -->
<script type="text/javascript" src="<?php echo assets() ?>/minified/jquery.tree.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo assets() ?>/minified/jquery.tree.min.css" />

<script src="<?php echo assets() ?>/js/plugins/morris/raphael-2.1.0.min.js"></script>
<script src="<?php echo assets() ?>/js/plugins/morris/morris.js"></script>

<!-- Date Format -->
<script type="text/javascript" src="<?php echo assets() ?>/javascripts/date.format.js"></script>
<script src="<?php echo assets() ?>/js/plugins/datapicker/bootstrap-datetimepicker.min.js"></script>

<script src="<?php echo assets() ?>/js/plugins/validate/jquery.validate.min.js"></script>
<script src="<?php echo assets() ?>/js/plugins/select2/select2.js"></script>
<script src="<?php echo assets() ?>/js/plugins/select2/select2_locale_ar.js"></script>
<link href="<?php echo assets("tagit") ?>/jquery.tagit.css" rel="stylesheet" type="text/css">
<link href="<?php echo assets("tagit") ?>/tagit.ui-zendesk.css" rel="stylesheet" type="text/css">
<script src="<?php echo assets("tagit") ?>/tag-it.js"></script>


<script>

var baseURL = '{!! URL::to("/".ADMIN) !!}/';
var detailPage = '{!! URL::to('/details/') !!}/{{($post->mongo_id) ? $post->mongo_id : $post->post_id}}';
var postURL = '{!! URL::to('/details/') !!}/{!!$post->post_slug!!}';
var baseURL2 = '{!! URL::to("") !!}/';
var assetsURL = '{!! assets("") !!}/';
var post_id = "{!!$post->post_id!!}";
var mongo_id = "{!! ($post->mongo_id && $post->post_content) ? true : false !!}";
var AMAZON_URL = "{!!AMAZON_URL!!}";
var check_free = baseURL + "posts/check_free";
var meta_description = "{!! ($post->meta_description) ? true : false !!}";
var facebook_description = "{!! ($post->facebook_description) ? true : false !!}";
var twitter_description = "{!! ($post->twitter_description) ? true : false !!}";
var meta_title = "{!! ($post->meta_title) ? true : false !!}";
var facebook_title = "{!! ($post->facebook_title) ? true : false !!}";
var meta_keywords = "{!! ($post->meta_keywords) ? true : false !!}";
var comment_options = "{!!$post->comment_options!!}";
var search_lang = "{!!Lang::get('posts::posts.search')!!}";
var immediately_lang = "{!!Lang::get('posts::posts.immediately')!!}";
var publish_on_lang = "{!!Lang::get('posts::posts.publish_on')!!}";
var schedule_for_lang = "{!!Lang::get('posts::posts.schedule_for')!!}";
var required_lang = "{!!Lang::get('posts::posts.required')!!}";
var min_tags_lang = "{!!Lang::get('posts::posts.min_tags')!!}";
var one_cats_lang = "{!!Lang::get('posts::posts.one_cats')!!}";
var more_character_lang = "{!!Lang::get('posts::posts.more_character')!!}";
var no_results_lang = "{!!Lang::get('posts::posts.no_results')!!}";
var notfound_lang = "{!!Lang::get('posts::posts.notfound')!!}";
var repeated_lang = "{!!Lang::get('posts::posts.repeated')!!}";
var invalid_lang = "{!!Lang::get('posts::posts.invalid')!!}";
var is_not_an_image_lang = " <?php echo trans("posts::users.is_not_an_image ") ?>";
var title_lang = " <?php echo Lang::get('posts::posts.title') ?>";
var link_lang = " <?php echo Lang::get('posts::posts.link') ?>";
var save_lang = " <?php echo Lang::get('posts::posts.save') ?>";

// seo
var page_title = " <?php echo Lang::get('posts::posts.article_heading') ?>:";
var page_url = " <?php echo Lang::get('posts::posts.page_url') ?>:";
var post_content = " <?php echo Lang::get('posts::posts.content') ?>:";
var seo_title = " <?php echo Lang::get('posts::posts.page_title') ?>:";
var seo_description = " <?php echo Lang::get('posts::posts.meta_description') ?>:";
var focus_keyword = " <?php echo Lang::get('posts::posts.focus_keyword_p') ?>:";
var focus_keyword_usage = " <?php echo Lang::get('posts::posts.focus_keyword_usage') ?>";
var yes = " <?php echo Lang::get('posts::posts.yes') ?>";
var no = " <?php echo Lang::get('posts::posts.no') ?>";
var sitename = " - <?php echo Config::get("site_name") ?>";

$(document).ready(function () {
    CKEDITOR.replace("pagecontent", {
        language: '<?php echo LANG; ?>'
    });
        
    @if(isset($post->post_id))
    @if (User::can('comments.add'))
        CKEDITOR.replace("post_comment", {
            language: '<?php echo LANG; ?>'
        });
    @endif
    @endif

    @if ($post->post_id)
        $('#post_editor').val("{!!$post->post_editor_id!!}");
    @else
        $('#post_editor').val("{!!Auth::user()->id!!}");
    @endif

    @if(isset($post->post_id))
        $("input[name=post_format][value={!!Input::old('post_format', $post->post_format)!!}]").attr('checked', 'checked');
    @endif

    @if (count(Input::old('cats', $post->categories)))
    @foreach(Input::old('cats', $post->categories) as $cat)
    @if (count(Input::old('cats')))
        $("input[name='cats[]'][value='" + {!!$cat!!} + "']").iCheck('check');
        $("input[name='mosts[]'][value='" + {!!$cat!!} + "']").iCheck('check');
    @else
        $("input[name='cats[]'][value='" + {!!$cat->cat_id!!} + "']").iCheck('check');
        $("input[name='mosts[]'][value='" + {!!$cat->cat_id!!} + "']").iCheck('check');
    @endif
    @endforeach
    @endif


    @if (!$post)
    @if (Input::has('cat_id'))
        $("input[name='cats[]'][value='" + {!!Input::get('cat_id')!!} + "']").iCheck('check');
    @endif
    @endif

    @if ($post->post_writers)
        $("input[name='writers']").prop('checked', true).trigger('change');
        $("#post_writers").prop('disabled', false);
    @endif

    @if (!$post->post_id)
        $("#status").html($("#post_status option:selected").text());
    @endif

    $("#post-form").validate({
        ignore:[],
        focusInvalid: false,
        invalidHandler: function(form, validator) {
            if (!validator.numberOfInvalids())
                    return;
            $('html, body').animate({scrollTop: $(validator.errorList[0].element).offset().top}, 200);
        },
        rules: {
            post_title: {
                required: true
            },
            post_excerpt: {
                required: true
            },
            featured_image: {
                required: true
            },
            media_file: {
                required: function(){
                    if ($('input[type=radio][name=post_format]:checked').val() == 3 || $('input[type=radio][name=post_format]:checked').val() == 4) {
                        return true;
                    } else{
                        return false;
                    }
                }
            },
            @if (Input::get('post_type') == 'article' && count($authors))
            post_author: {
                required: true
            },
            @endif
            post_content: {
                required: function(){
                    if ($('input[type=radio][name=post_format]:checked').val() != 2 /*&& $('input[type=radio][name=post_format]:checked').val() != 6*/) {
                        return CKEDITOR.instances.pagecontent.updateElement();
                    } else{
                        return false;
                    }
                }
            },
            'cats[]': {
                required: true
            },
            tags: {
                required: true,
                tagsRequired: true
            },
        },
        messages: {
            post_title: {
                required: required_lang,
            },
            post_content: {
                required: required_lang,
            },
            featured_image: {
                required: required_lang,
            },
            media_file: {
                required: required_lang,
            },
            post_author: {
                required: required_lang,
            },
            post_excerpt: {
                required: required_lang,
            },
            'cats[]': {
                required: required_lang,
            },
            tags: {
                required: required_lang,
                tagsRequired: min_tags_lang,
            }
        }
    });

//    $("#metakeywords").tagit({
//        singleField: true,
//        singleFieldNode: $('#meta_keywords'),
//        allowSpaces: true,
//        minLength: 2,
//        placeholderText: "{!!Lang::get('posts::posts.meta_keywords')!!}",
//        removeConfirmation: true,
//        tagSource: function (request, response) {
//            $.ajax({
//                url: "<?php echo route("google.search"); ?>",
//                data: {term: request.term},
//                dataType: "json",
//                success: function (data) {
//                    console.log(data);
//                    response($.map(data, function (item) {
//                        return {
//                            label: item.name,
//                            value: item.name
//                        }
//                    }));
//                }
//            });
//        }
//    });
//    
//    $("#metafocus").tagit({
//        singleField: true,
//        singleFieldNode: $('#focus_keyword'),
//        allowSpaces: true,
//        minLength: 2,
//        tagLimit: 1,
//        placeholderText: "{!!Lang::get('posts::posts.focus_keyword')!!}",
//        removeConfirmation: true,
//        tagSource: function (request, response) {
//            $.ajax({
//                url: "<?php echo route("google.search"); ?>",
//                data: {term: request.term},
//                dataType: "json",
//                success: function (data) {
//                    console.log(data);
//                    response($.map(data, function (item) {
//                        return {
//                            label: item.name,
//                            value: item.name
//                        }
//                    }));
//                }
//            });
//        }
//    });
//    
//    if(post_id){
//        //$('#robots_index').val("{!!@$meta_robots->robots_index!!}");
//        $('input[name="meta[robots_follow]"][value="{!!@$meta_robots->robots_follow!!}"]').prop('checked', 'checked');
//        //$('#in_sitemap').val("{!!@$meta_robots->in_sitemap!!}");
//        $('#sitemap_priority').val("{!!round(@$meta_robots->sitemap_priority, 2)!!}");
//    }
    
});
</script>

<script type="text/javascript" src="<?php echo assets("javascripts/posts.js"); ?>"></script>
<script type="text/javascript" src="<?php echo assets("javascripts/posts-seo.js"); ?>"></script>
@stop
@stop
