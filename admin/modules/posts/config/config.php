<?php

return [
    "permissions" => [
        "create",
        "edit",
        "delete",
        "trash",
        "order",
        "restore",
        "published",
        "unpublish",
        "pending",
        "reviewed",
        "draft",
        "scheduled",
        "move",
        "order_home",
        "order_category",
        "control_article",
        "unlock_post",
        "close_post",
        "front_edit",
        "newsletter"
    ]
];
