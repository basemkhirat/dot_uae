<?php foreach ($files as $row) { ?>
    <div class="dz-preview dz-image-preview" media-id="<?php echo $row->media_id ?>">
        <input type="hidden" name="media_amazon" value="<?php echo $row->media_amazon; ?>"/>
        <input type="hidden" name="media_url" value="<?php echo $row->media_url; ?>"/>   
        <input type="hidden" name="media_thumbnail" value="<?php echo $row->media_thumbnail; ?>"/>
        <input type="hidden" name="media_size" value="<?php echo $row->media_size ?>"/>
        <input type="hidden" name="media_path" value="<?php echo $row->media_path ?>"/>
        <input type="hidden" name="media_type" value="<?php echo $row->media_type ?>"/>
        <input type="hidden" name="media_provider" value="<?php echo $row->media_provider; ?>"/>
        <input type="hidden" name="media_provider_id" value="<?php echo $row->media_provider_id; ?>"/>
        <input type="hidden" name="media_duration" value="<?php echo $row->media_duration; ?>"/>
        <input type="hidden" name="media_id" value="<?php echo $row->media_id ?>"/>
        <input type="hidden" name="media_title" value="<?php echo $row->media_title ?>"/>
        <input type="hidden" name="media_description" value="<?php echo $row->media_description ?>"/>
        <input type="hidden" name="media_created_date" value="<?php echo $row->media_created_date ?>"/>
        <input type="hidden" name="media_motive" value="<?php echo $row->media_motive ?>"/>
        
        
        <i class="fa fa-check right-mark"></i>
        
        <div class="dz-details">
            <div class="dz-thumbnail-wrapper">

                <div class="dz-thumbnail">

                    <?php if (in_array($row->media_type, array("video", "audio"))) { ?>
                        <i class="vid fa fa-play-circle"></i>
                    <?php } ?>


                    <img src="<?php echo $row->media_thumbnail; ?>">
   
                </div>
            </div>
        </div>
    </div>
<?php } ?>