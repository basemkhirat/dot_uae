<?php foreach ($files as $row) { ?>
    <div class="dz-preview dz-image-preview" media-id="<?php echo $row->media_id ?>">

        <?php if ($row->media_provider == "youtube") { ?>
            <input type="hidden" name="media_provider" value="youtube"/>
            <input type="hidden" name="media_duration" value="<?php echo format_duration($row->media_duration); ?>"/>
            <input type="hidden" name="media_url" value="<?php echo "https://www.youtube.com/watch?v=" . $row->media_path; ?>"/>   
            <input type="hidden" name="media_thumbnail" value="http://img.youtube.com/vi/<?php echo $row->media_path; ?>/0.jpg"/>
            <input type="hidden" name="media_size" value=""/>
            <input type="hidden" name="media_path" value="<?php echo $row->media_title ?>"/>
            <input type="hidden" name="media_provider_id" value="<?php echo $row->media_path; ?>"/>
        <?php } else { ?>
            <input type="hidden" name="media_url" value="<?php echo uploads_url($row->media_path); ?>"/>   
            <input type="hidden" name="media_thumbnail" value="<?php echo thumbnail($row->media_path) ?>"/>
            <input type="hidden" name="media_size" value="<?php echo (File::exists(uploads_path($row->media_path))) ? format_file_size(File::size(uploads_path($row->media_path))) : "0 MB" ?>"/>
            <input type="hidden" name="media_path" value="<?php echo $row->media_path ?>"/>
            <input type="hidden" name="media_duration" value=""/>
            <input type="hidden" name="media_provider" value=""/>
        <?php } ?>

        <input type="hidden" name="media_id" value="<?php echo $row->media_id ?>"/>
        <input type="hidden" name="media_title" value="<?php echo $row->media_title ?>"/>
        <input type="hidden" name="media_description" value="<?php echo $row->media_description ?>"/>
        <input type="hidden" name="media_created_date" value="<?php echo $row->media_created_date ?>"/>

        <i class="fa fa-check right-mark"></i>
        <div class="dz-details">
            <div class="dz-thumbnail-wrapper">
               
                <div class="dz-thumbnail">
                    <?php if($row->media_type == "video"){ ?>
                    <i class="vid fa fa-play-circle"></i>
                    <?php } ?>
                    <?php if ($row->media_provider == "youtube") { ?>
                        <img src="http://img.youtube.com/vi/<?php echo $row->media_path; ?>/0.jpg">
                    <?php } else { ?>
                        <img src="<?php echo thumbnail($row->media_path) ?>">
                    <?php } ?>
                    <span class="dz-nopreview">No preview</span>
                </div>
            </div>
        </div>
    </div>
<?php } ?>