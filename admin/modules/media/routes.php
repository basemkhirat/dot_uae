<?php

Route::group(array(
    "prefix" => ADMIN,
    "before" => "auth",
        ), function($route) {
        $route->group(array("prefix" => "media"), function($route) {
        $route->any('/get/{offset?}/{type?}/{q?}', array("as" => "media.index", "uses" => "MediaController@index"));
        $route->any('/save_gallery', array("as" => "media.save_gallery", "uses" => "MediaController@save_gallery"));
        $route->any('/save', array("as" => "media.save", "uses" => "MediaController@save"));
        $route->any('/delete', array("as" => "media.delete", "uses" => "MediaController@delete"));
        $route->any('/upload', array("as" => "media.upload", "uses" => "MediaController@upload"));
        $route->any('/download', array("as" => "media.download", "uses" => "MediaController@download"));
        $route->any('/link', array("as" => "media.link", "uses" => "MediaController@link"));
        $route->any('/crop', array("as" => "media.crop", "uses" => "MediaController@crop"));
        $route->any('/watermark', array("as" => "media.watermark", "uses" => "MediaController@watermark"));
        $route->any('/galleries/create', array("as" => "media.gallery_create", "uses" => "MediaController@gallery_create"));
        $route->any('/galleries/delete', array("as" => "media.gallery_delete", "uses" => "MediaController@gallery_delete"));
        $route->any('/galleries/edit', array("as" => "media.gallery_edit", "uses" => "MediaController@gallery_edit"));
    });
});
