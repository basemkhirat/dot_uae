<?php

class Media extends Model {

    protected $table = 'media';
    protected $primaryKey = 'media_id';
    protected $fillable = ['media_provider', 'media_provider_id', 'media_provider_image',
        'media_path', 'media_hash', 'media_type', 'media_title', 'media_description', 'media_duration'];

    const CREATED_AT = 'media_created_date';

    public function setUpdatedAt($value) {
        // Do nothing.
    }

    public function getUpdatedAtColumn() {
        return null;
    }

    public function getImageURL($size = 'full', $encoded = false) {
        $nameParts = explode('/', $this->media_path);
        if (isset($nameParts[1])) {//there are month and year partss
            $year = $nameParts[0] . '/';
            $month = $nameParts[1] . '/';
            $imageName = $nameParts[2];
            $isOld = FALSE;
        } else {
            $year = '';
            $month = '';
            $imageName = $nameParts[0];
            $isOld = TRUE;
        }
        $sizeName = ($size == 'full') ? '' : $size . '-';
        if (!$isOld) {
            $image = AMAZON_URL . $year . $month . $sizeName . $imageName;
        } else {
            $image = AMAZON_URL . 'uploads/uploads/' . $sizeName . $imageName;
        }
        return $image;
        
//        $handle = @fopen($image, "r");
//        if ($handle) {
//            return $image;
//        } else {
//            return assets("images/default.png");
//        }
    }

}
