<?php

class MediaController extends BackendController {

    public $data = array();

    function unique($filename = false) {

        if ($filename) {
            if (File::exists(UPLOADS . "/" . $filename)) {
                $parts = explode(".", $filename);
                $extension = end($parts);
                $name = str_slug(basename($filename, "." . $extension));
                $filename = time() . rand() . "." . strtolower($extension);
                return $filename;
            }
            return $filename;
        }
    }

    function youtube($link = "") {
        $id = get_youtube_video_id($link);
        $details = get_youtube_video_details($id);

        $data["media_provider"] = "youtube";
        $data["media_provider_id"] = $id;
        $data["media_provider_image"] = $details->image;
        $data["media_type"] = "video";
        $data["media_path"] = "https://www.youtube.com/watch?v=" . $id;
        $data["media_title"] = $details->title;
        $data["media_description"] = $details->description;
        $data["media_duration"] = $details->length;
        $data["media_created_date"] = date("Y-m-d H:i:S");

        Media::insert($data);

        $media_id = DB::getPdo()->lastInsertId();

        /*
          // insert data to mongodb
          $ob = array();
          $ob["media_id"] = (int) $media_id;
          $ob["media_provider"] = "youtube";
          $ob["media_provider_id"] = $id;
          $ob["media_provider_image"] = $details->image;
          $ob["media_type"] = "video";
          $ob["media_path"] = "https://www.youtube.com/watch?v=" . $id;
          $ob["media_title"] = $details->title;
          $ob["media_description"] = $details->description;
          $ob["media_duration"] = (int) $details->length;
          $this->mongo($ob);
         * 
         */

// Saving the file to database
        $row = new \stdClass();
        $row->error = false;
        $row->id = $media_id;
        $row->name = $data["media_title"];
        $row->size = "";
        $row->duration = $details->length;
        $row->url = "https://www.youtube.com/watch?v=" . $id;
        $row->thumbnail = $details->image;
        $row->html = View::make("media::index", array(
                    "files" => array(0 => (object) array(
                            "media_id" => $media_id,
                            "media_motive" => 0,
                            "media_amazon" => 0,
                            "media_provider" => "youtube",
                            "media_provider_id" => $id,
                            "media_url" => $row->url,
                            "media_thumbnail" => $row->thumbnail,
                            "media_size" => $row->size,
                            "media_path" => $row->name,
                            "media_duration" => format_duration($row->duration),
                            "media_type" => "video",
                            "media_title" => $data["media_title"],
                            "media_description" => $data["media_description"],
                            "media_created_date" => date("Y-m-d H:i:s")
                        ))
                ))->render();
        return Response::json($row, 200);
    }

    function soundcloud($link = "") {

        $details = get_soundcloud_track_details($link);
        $link = $details->link;

        $data["media_provider"] = "soundcloud";
        $data["media_provider_id"] = $details->id;
        $data["media_provider_image"] = $details->image;
        $data["media_type"] = "audio";
        $data["media_path"] = $link;
        $data["media_title"] = $details->title;
        $data["media_description"] = $details->description;
        $data["media_duration"] = $details->length;
        $data["media_created_date"] = date("Y-m-d H:i:S");

        Media::insert($data);

        $media_id = DB::getPdo()->lastInsertId();

        /*
          // insert data to mongodb
          $ob = array();
          $ob["media_id"] = (int) $media_id;
          $ob["media_provider"] = "soundcloud";
          $ob["media_provider_id"] = $details->id;
          $ob["media_provider_image"] = $details->image;
          $ob["media_type"] = "audio";
          $ob["media_path"] = $link;
          $ob["media_title"] = $details->title;
          $ob["media_description"] = $details->description;
          $ob["media_duration"] = (int) $details->length;
          $this->mongo($ob);
         * 
         */


// Saving the file to database
        $row = new \stdClass();
        $row->error = false;
        $row->id = $media_id;
        $row->name = $details->title;
        $row->size = "";
        $row->duration = $details->length;
        $row->url = $link;

        if ($details->image != "") {
            $row->thumbnail = $details->image;
        } else {
            $row->thumbnail = assets("default/soundcloud.png");
        }

        $row->html = View::make("media::index", array(
                    "files" => array(0 => (object) array(
                            "media_id" => $media_id,
                            "media_motive" => 0,
                            "media_amazon" => 0,
                            "media_provider" => "soundcloud",
                            "media_provider_id" => $details->id,
                            "media_url" => $link,
                            "media_thumbnail" => $row->thumbnail,
                            "media_size" => $row->size,
                            "media_path" => $link,
                            "media_duration" => format_duration($row->duration),
                            "media_type" => "audio",
                            "media_title" => $details->title,
                            "media_description" => $details->description,
                            "media_created_date" => date("Y-m-d H:i:s")
                        ))
                ))->render();
        return Response::json($row, 200);
    }

    /*
      function mongo($data = array()) {

      $dt = new \DateTime('+2 hours');
      $ts = $dt->getTimestamp();
      $time = new \MongoDate($ts);

      $data = array_merge(array(
      "media_id" => 0,
      "media_provider" => NULL,
      "media_provider_id" => NULL,
      "media_provider_image" => NULL,
      "media_path" => "",
      "media_type" => "",
      "media_title" => NULL,
      "media_description" => NULL,
      "media_created_date" => $time,
      "media_duration" => (int) 0,
      "media_motive" => (int) 0
      ), $data);

      if ($data["media_id"] == 0) {
      return false;
      }

      // insert data to mongodb
      DB::connection("mongodb")
      ->collection('media')
      ->insert($data);
      }
     * 
     */

    function link() {

        $data = array(
            "media_provider" => 0,
            "media_path" => "",
            "media_type" => "",
            "media_title" => "",
            "media_description" => ""
        );

        if ($link = Input::get("link")) {

// Detecting link 
            if (strstr($link, "youtube.") and get_youtube_video_id($link)) {
                return $this->youtube($link);
            } else if (strstr($link, "soundcloud.")) {
                return $this->soundcloud($link);
            } else {
                $name = md5($link);
                if (copy($link, UPLOADS . "/temp/" . $name)) {
                    $mime = strtolower(mime_content_type(UPLOADS . "/temp/" . $name));

// check allowed types
                    $media_extension = get_extension($mime);
                    if (!$media_extension) {
                        $row->error = "Invalid link file type";
                        return Response::json($row, 200);
                    }

                    $mime_parts = explode("/", $mime);
                    $media_type = $mime_parts[0];

                    $remote_file_parts = explode("/", ($link));
                    $data["media_title"] = $remote_file_name = urldecode(basename(end($remote_file_parts), "." . $media_extension));

                    $slug = str_slug(str_replace("." . $media_extension, "", urldecode($remote_file_name)));
                    $filename = $this->unique($slug) . "." . $media_extension;

                    $filename = strtolower($filename);

                    $filename = time() * rand() . "." . strtolower($media_extension);

                    if (copy(UPLOADS . "/temp/" . $name, UPLOADS . "/" . $filename)) {

                        s3_save(date("Y/m/") . $filename);

                        if (in_array(strtolower($media_extension), array("jpg", "jpeg", "gif", "png", "bmp"))) {
                            $this->set_sizes($filename);
                        }

                        $data["media_type"] = $media_type;
                        $data["media_path"] = $filename;
                        $data["media_amazon"] = 1;

                        Media::insert($data);

                        $media_id = DB::getPdo()->lastInsertId();
                        /*
                          // insert data to mongodb
                          $ob["media_id"] = (int) $media_id;
                          $ob["media_type"] = $media_type;
                          $ob["media_path"] = $filename;
                          $ob["media_title"] = $data["media_title"];
                          $this->mongo($ob);
                         * 
                         */


// Saving the file to database
                        $row = new \stdClass();
                        $row->error = false;
                        $row->id = $media_id;
                        $row->name = $filename;
                        $row->size = format_file_size(filesize(UPLOADS . "/" . $filename));
                        $row->url = uploads_url(date("Y/m/") . $filename);
                        $row->thumbnail = thumbnail($filename);

                        $row->html = View::make("media::index", array(
                                    "files" => array(0 => (object) array(
                                            "media_id" => $media_id,
                                            "media_motive" => 0,
                                            "media_amazon" => 1,
                                            "media_provider" => "",
                                            "media_provider_id" => "",
                                            "media_url" => $row->url,
                                            "media_thumbnail" => $row->thumbnail,
                                            "media_size" => $row->size,
                                            "media_path" => date("Y/m/") . $row->name,
                                            "media_duration" => "",
                                            "media_type" => "",
                                            "media_title" => $data["media_title"],
                                            "media_description" => "",
                                            "media_created_date" => date("Y-m-d H:i:s")
                                        ))
                                ))->render();

                        return Response::json($row, 200);
                    }
                } else {
                    return "Cannot copy this file";
                }
            }
        }
    }

    function download($media_path = false) {

        if (!$media_path) {
            $media_path = Input::get("media_path");
        }

        $link = uploads_url($media_path);

        $file = $media_path;

        if (strstr($media_path, "/")) {

// S3 File
            $parts = explode("/", $media_path);
            $file = end($parts);

            if (!file_exists(UPLOADS . "/" . $file)) {
                if (copy($link, UPLOADS . "/" . $file)) {

// download sizes if it is an image
                    $parts = explode(".", $file);
                    $extension = end($parts);
                    if (in_array(strtolower($extension), array("jpg", "jpeg", "gif", "png", "bmp"))) {
                        $this->set_sizes($file, 0);
                    }
                }
            }
        }

        return $file;
    }

    function watermark() {

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {

            if (!File::exists(UPLOADS . "/" . $_POST['path'])) {
                $this->download($_POST['amazon_path']);
            }

            $sizes = \Config::get("media.sizes");

            foreach ($sizes as $size => $dimensions) {

                $img = Image::make(UPLOADS . "/" . $size . "-" . $_POST['path']);
                $img->insert('watermarks/text_' . $size . '.png', "center", 0, 0);
                $img->save(UPLOADS . "/" . $size . "-" . $_POST['path']);

                $img = Image::make(UPLOADS . "/" . $size . "-" . $_POST['path']);
                $img->insert('watermarks/logo_' . $size . '.png', "bottom-left", 0, 0);
                $img->save(UPLOADS . "/" . $size . "-" . $_POST['path']);

                if (strstr($_POST['amazon_path'], "/")) {
                    $parts = explode("/", $_POST['amazon_path']);
                    @s3_save($parts[0] . "/" . $parts[1] . "/" . $size . "-" . $_POST['path']);
                } else {
                    @s3_save("uploads/uploads/" . $size . "-" . $_POST['path']);
                }
            }

            /*
              $img = Image::make(UPLOADS . "/" . $_POST['path']);

              // insert a watermark
              $img->insert('logo.png', $_POST['position'], 10, 10);

              // save image in desired format
              $img->save(UPLOADS . "/" . $_POST['path']);
             */


            echo json_encode(array(
                "path" => $_POST['path']
            ));

            exit;
        }
    }

    function crop() {

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {

            if (!File::exists(UPLOADS . "/" . $_POST['path'])) {
                $this->download($_POST['amazon_path']);
            }

            $src = UPLOADS . "/" . $_POST["size"] . "-" . $_POST['path'];

            if ($_POST['w'] != "") {
                $img = Image::make(UPLOADS . "/" . $_POST['path']);
                // crop image
                $img->crop((int) $_POST['w'], (int) $_POST['h'], (int) $_POST['x'], (int) $_POST['y'])->save($src);

                $sizes = Config::get("media.sizes");
                $current_size = $sizes[$_POST['size']];

                Image::make($src)
                        ->resize($current_size[0], $current_size[1])
                        ->save($src);

                echo json_encode(array(
                    "path" => $_POST["size"] . "-" . $_POST['path'],
                    "width" => $current_size[0],
                    "height" => $current_size[1]
                ));
            } else {
                echo json_encode(array(
                    "path" => $_POST["size"] . "-" . $_POST['path']
                ));
            }

            if (strstr($_POST['amazon_path'], "/")) {
                $parts = explode("/", $_POST['amazon_path']);
                @s3_save($parts[0] . "/" . $parts[1] . "/" . $_POST['size'] . "-" . $_POST['path']);
            } else {
                @s3_save("uploads/uploads/" . $_POST['size'] . "-" . $_POST['path']);
            }
//$this->delete_hard($_POST['path']);
            exit;
        }
    }

    function interactive($file, $media_id) {

        $zip = new \ZipArchive;
        if ($zip->open(UPLOADS . "/" . $file) === TRUE) {

            $path = public_path('interactive/' . $media_id);
            $zip->extractTo($path);
            $zip->close();

            $files = array_filter(glob_recursive($path . "/*"), 'is_file');

            foreach ($files as $file) {
                $path_parts = explode("interactive", $file);
                $relative_path = end($path_parts);

// save file to S3
                $s3 = \App::make('aws')->get('s3');
                $op = $s3->putObject(array(
                    'Bucket' => 'dotemirates',
                    'Key' => "interactive" . $relative_path,
                    'SourceFile' => 'interactive' . $relative_path,
                    'ACL' => 'public-read'
                ));
            }

// delete local interactive folder
            rrmdir('interactive/' . $media_id);
        }
    }

    public function upload() {

        $file = Input::file('files')[0];

        $allowed_types = Config("media.allowed_file_types");
        if (is_array($allowed_types)) {
            $allowed_types = join(",", Config("media.allowed_file_types"));
        }

        $rules = array(
            'files.0' => "mimes:" . $allowed_types . "|max:" . Config("media.max_file_size"),
        );

        $size = $file->getSize();
        $filename = strtolower($this->unique($file->getClientOriginalName()));


        $parts = explode(".", $file->getClientOriginalName());
        $extension = end($parts);

// rename file with timestamp
        $filename = time() * rand() . "." . strtolower($extension);


        $mime_parts = explode("/", $file->getMimeType());
        $mime_type = $mime_parts[0];

        $validator = Validator::make(Input::all(), $rules);
        $validator->setAttributeNames(array(
            'files.0' => "file"
        ));

        if ($validator->fails()) {
            $errors = $validator->errors()->toArray();
            return \Response::json(array(
                        "error" => join("|", str_replace("files.0", $file->getClientOriginalName(), $errors["files.0"]))
                            ), 200);
        }

// check hash file is exist
        $f = DB::table("media")
                ->where("media_hash", sha1_file($_FILES['files']["tmp_name"][0]))
                ->first();

        if (count($f)) {

            //print_r($f);

            $row = new \stdClass();
            $row->id = $f->media_id;
            $row->name = $f->media_path;
            $row->title = $f->media_title;
//$row->size = format_file_size(filesize(uploads_path($f->media_path)));
            $row->size = "";
            $row->url = uploads_url($f->media_path);
            $row->thumbnail = thumbnail($f->media_path);
            $row->html = View::make("media::index", array(
                        "files" => array(0 => (object) array(
                                "media_id" => $f->media_id,
                                "media_motive" => $f->media_motive,
                                "media_amazon" => 1,
                                "media_provider" => $f->media_provider,
                                "media_provider_id" => $f->media_provider_id,
                                "media_type" => $f->media_type,
                                "media_url" => uploads_url($f->media_path),
                                "media_thumbnail" => thumbnail($f->media_path),
                                "media_size" => "", //format_file_size(filesize(uploads_path($f->media_path))),
                                "media_path" => $f->media_path,
                                "media_duration" => "",
                                "media_title" => $f->media_title,
                                "media_description" => $f->media_description,
                                "media_created_date" => $f->media_created_date
                            ))
                    ))->render();
            return Response::json(array('files' => array($row)), 200);
        }

        try {

            $operation = $file->move(UPLOADS, $filename);

// check image max dimensions
            $this->checkImageDimensions($filename);

// Moving to amazon s3
            s3_save(date("Y/m/") . $filename);
        } catch (Exception $exception) {
// Something went wrong. Log it.
            $error = array(
                'name' => $file->getClientOriginalName(),
                'size' => $size,
                'error' => $exception->getMessage(),
            );
// Return error
            return \Response::json($error, 400);
        }


        if ($operation) {

// creating image size
            if (in_array(strtolower($extension), array("jpg", "jpeg", "gif", "png", "bmp"))) {
                $this->set_sizes($filename);
            }

// set ia media type if interactive file
            if ($file->getClientOriginalName() == "interactive.zip") {
                $mime_type = "ia";
            }

            Media::insert(array(
                "media_provider" => "",
                "media_path" => date("Y") . "/" . date("m") . "/" . $filename,
                "media_type" => $mime_type,
                "media_title" => basename(strtolower($file->getClientOriginalName()), "." . $extension),
                "media_created_date" => date("Y-m-d H:i:S"),
                "media_hash" => sha1_file(UPLOADS . "/" . ($filename)),
                "media_amazon" => 1
            ));

            $media_id = DB::getPdo()->lastInsertId();

// check if interactive file
            if ($file->getClientOriginalName() == "interactive.zip") {
                $this->interactive($filename, $media_id);
            }

// delete file from local server
            if (Config::get("media.s3.status") and Config::get("media.s3.delete_locally")) {
                $this->delete_hard($filename);
            }

            /*
              // insert data to mongodb
              $this->mongo(array(
              "media_id" => (int) $media_id,
              "media_provider" => "",
              "media_path" => ($filename),
              "media_type" => $mime_type,
              "media_title" => basename($file->getClientOriginalName(), "." . $extension),
              "media_hash" => sha1_file(UPLOADS . "/" . ($filename))
              ));
             */
            $filename = ($filename);

// Saving the file to database
            $row = new \stdClass();
            $row->id = $media_id;
            $row->name = date("Y/m/") . $filename;
            $row->title = basename($file->getClientOriginalName(), "." . $extension);
//$row->size = format_file_size($size);
            $row->size = "";
            $row->url = uploads_url(date("Y/m/") . $filename);
            $row->thumbnail = thumbnail(date("Y/m/") . $filename);
            $row->html = View::make("media::index", array(
                        "files" => array(0 => (object) array(
                                "media_id" => $media_id,
                                "media_motive" => 0,
                                "media_amazon" => 1,
                                "media_provider" => "",
                                "media_provider_id" => "",
                                "media_type" => $mime_type,
                                "media_url" => uploads_url(date("Y/m/") . $filename),
                                "media_thumbnail" => thumbnail(date("Y/m/") . $filename),
                                "media_size" => "", //format_file_size($size),
                                "media_path" => date("Y/m/") . $filename,
                                "media_duration" => "",
                                "media_title" => basename($file->getClientOriginalName(), "." . $extension),
                                "media_description" => "",
                                "media_created_date" => date("Y-m-d H:i:s")
                            ))
                    ))->render();



            return Response::json(array('files' => array($row)), 200);
        } else {
            return Response::json('Error', 400);
        }
    }

    function checkImageDimensions($filename) {

        $parts = explode(".", $filename);
        $extension = end($parts);

        if (!in_array(strtolower($extension), array("jpg", "jpeg", "gif", "png", "bmp"))) {
            return false;
        }

        if (file_exists(UPLOADS . "/" . $filename)) {

            $image_width = Image::make(UPLOADS . "/" . $filename)->width();

            if ($image_width > Config::get("media.max_width")) {
                Image::make(UPLOADS . "/" . $filename)
                        ->resize(Config::get("media.max_width"), null, function ($constraint) {
                            $constraint->aspectRatio();
                            $constraint->upsize();
                        })
                        ->save(UPLOADS . "/" . $filename);
            }
        }
    }

    function delete_hard($filename) {
        if (File::exists(UPLOADS . "/" . $filename)) {
            unlink(UPLOADS . "/" . $filename);

// delete all sizes
            $parts = explode(".", $filename);
            $extension = end($parts);
            if (in_array(strtolower($extension), array("jpg", "jpeg", "gif", "png", "bmp"))) {
// resize
                $sizes = Config::get("media.sizes");
                foreach ($sizes as $size => $dimensions) {

                    if (File::exists(uploads_path($size . "-" . $filename))) {
                        @unlink(uploads_path($size . "-" . $filename));
                    }
                }
            }
        }
    }

    function save_gallery() {

        $name = Input::get("name");
        $slug = str_slug($name);

        Gallery::insert(array(
            "gallery_slug" => $slug,
            "gallery_author" => "",
            "gallery_name" => $name
        ));

        $gallery_id = DB::getPdo()->lastInsertId();


// insert gallery media
        if ($ids = Input::get("content")) {

            $i = 1;
            foreach ($ids as $media_id) {

                DB::table("media")->where("media_id", $media_id)->update(array(
                    "media_description" => $name . "-" . $i
                ));

                GalleryMedia::insert(array(
                    "gallery_id" => $gallery_id,
                    "media_id" => $media_id
                ));

                $i++;
            }
        }

        $media_type = DB::table("media")->where("media_id", $ids[0])->pluck("media_type");

        echo json_encode(array("gallery_id" => $gallery_id, "gallery_type" => $media_type));
    }

    function set_sizes($filename, $s3_save = 1) {


        if (!Config::get("media.media_thumbnails")) {
            return false;
        }

        if (file_exists(UPLOADS . "/" . $filename)) {

            $sizes = Config::get("media.sizes");
            $width = Image::make(UPLOADS . "/" . $filename)->width();
            $height = Image::make(UPLOADS . "/" . $filename)->height();

            foreach ($sizes as $size => $dimensions) {

                if ($size == "free") {
                    Image::make(UPLOADS . "/" . $filename)
                            ->resize($dimensions[0], null, function ($constraint) {
                                $constraint->aspectRatio();
                                $constraint->upsize();
                            })
                            ->save(UPLOADS . "/" . $size . "-" . $filename);
                } else {
                    Image::make(UPLOADS . "/" . $filename)
                            ->fit($dimensions[0], $dimensions[1], function ($constraint) {
                                $constraint->upsize();
                            })
                            ->save(UPLOADS . "/" . $size . "-" . $filename);
                }

                if ($s3_save) {
// run after file upload only and not used in download case
                    s3_save(date("Y/m/") . $size . "-" . $filename);
                }
            }
        } else {
            return "Image Not found";
        }
    }

    /*
      function set_sizes($filename) {

      $sizes = Config::get("cms::app.sizes");
      $width = Image::make(UPLOADS . "/" . $filename)->width();
      $height = Image::make(UPLOADS . "/" . $filename)->height();

      foreach ($sizes as $size => $dimensions) {
      if($size == "thumbnail"){
      Image::make(UPLOADS . "/" . $filename)
      ->crop($dimensions[0], $dimensions[1])
      ->save(UPLOADS . "/" . $size . "-" . $filename);;
      }else{
      if ($width > $height) {
      $new_width = null;
      $new_height = $dimensions[0];
      } else {
      $new_width = $dimensions[1];
      $new_height = null;
      }
      Image::make(UPLOADS . "/" . $filename)
      ->resize($new_height, $new_width, function ($constraint) {
      $constraint->aspectRatio();
      })
      ->resizeCanvas($dimensions[0], $dimensions[1], 'center', false, "#000000")
      ->save(UPLOADS . "/" . $size . "-" . $filename);
      }
      }
      }
     */

    public function index($page = 1, $type = "all", $q = "") {

        $limit = 25;
        $offset = ($page - 1) * $limit;

        $query = Media::orderBy("media_created_date", "DESC");

        if ($type != "all") {
            if ($type == "pdf" or $type == "swf") {
                $query->where("media_path", "LIKE", "%" . $type . "%");
            } else {
                $query->where("media_type", "=", $type);
            }
        }

        if ($q != "") {
//$query->remember(720);
            $query->where(function($ob) use($q) {
                $ob->where("media_title", "LIKE", "%" . $q . "%");
//$ob->orWhere("media_description", "LIKE", "%" . $q . "%");
            });
        }

        if (Input::has("motive")) {
            $query->where("media_motive", "=", Input::get("motive"));
        }

        if (Input::has("media_id")) {
            $query->where("media_id", "=", Input::get("media_id"));
        }

        $files = $query->limit($limit)->skip($offset)->get();

        $new_files = array();
        foreach ($files as $file) {

            $row = new \stdClass();
            $row = $file;

            if ($file->media_provider == "") {
                $row->media_thumbnail = thumbnail($file->media_path);
                $row->media_size = (File::exists(uploads_path($row->media_path))) ? format_file_size(File::size(uploads_path($row->media_path))) : "";
                $row->media_duration = "";
                $row->media_url = uploads_url($file->media_path);
// $row->amazon_media_path = $file->media_path;
                $row->media_path = ($file->media_path);
            } else {
                if ($file->media_provider_image != NULL) {
                    $row->media_thumbnail = $file->media_provider_image;
                } else {
                    $row->media_thumbnail = assets("default/soundcloud.png");
                }

                $row->media_url = $file->media_path;
                $row->media_size = "";
                $row->media_duration = format_duration($file->media_duration);
            }

            $new_files[] = $row;
        }


        $this->data["files"] = $new_files;

        $this->data["q"] = $q;
        return View::make("media::index", $this->data);
    }

    public function index2($page = 1, $type = "all", $q = "") {

        $limit = 25;
        $offset = ($page - 1) * $limit;

        if ($q != "") {
            $query = DB::connection("mongodb")->collection("media")->orderBy("media_created_date", "DESC");
        } else {
            $query = Media::orderBy("media_created_date", "DESC");
        }


        if ($type != "all") {
            if ($type == "pdf" or $type == "swf") {
                $query->where("media_path", "LIKE", "%" . $type . "%");
            } else {
                $query->where("media_type", "=", $type);
            }
        }

        if ($q != "") {
//$query->remember(720);
            $query->where(function($ob) use($q) {
                $ob->where("media_title", "LIKE", "%" . $q . "%");
//$ob->orWhere("media_description", "LIKE", "%" . $q . "%");
            });
        }

        if (Input::has("motive")) {
            $query->where("media_motive", "=", (int) Input::get("motive"));
        }

        if (Input::has("media_id")) {
            $query->where("media_id", "=", (int) Input::get("media_id"));
        }

        $files = $query->limit($limit)->skip($offset)->get();


        if ($q != "") {
            $files_docs = array();
            foreach ($files as $row) {
                $f = (object) $row;
                $f->media_created_date = date("Y-m-d H:i:s", $row["media_created_date"]->sec);
                $files_docs[] = $f;
            }
        } else {
            $files_docs = $files;
        }


        $new_files = array();
        foreach ($files_docs as $file) {

            $row = new \stdClass();
            $row = $file;

            if ($file->media_provider == "") {
                $row->media_thumbnail = thumbnail($file->media_path);
                $row->media_size = (File::exists(uploads_path($row->media_path))) ? format_file_size(File::size(uploads_path($row->media_path))) : "";
                $row->media_duration = "";
                $row->media_url = uploads_url($file->media_path);
            } else {

                if ($file->media_provider_image != NULL) {
                    $row->media_thumbnail = $file->media_provider_image;
                } else {
                    $row->media_thumbnail = assets("default/soundcloud.png");
                }

                $row->media_url = $file->media_path;
                $row->media_size = "";
                $row->media_duration = format_duration($file->media_duration);
            }

            $new_files[] = $row;
        }



        $this->data["files"] = $new_files;

        $this->data["q"] = $q;
        return View::make("media::index", $this->data);
    }

    function gallery_create() {

        if (Request::isMethod("post")) {
            $gallery = Gallery::create(array(
                        "gallery_name" => Input::get("gallery_name"),
                        "gallery_slug" => Str_slug(Input::get("gallery_name")),
                        "gallery_author" => Input::get("gallery_author")
            ));
            return $gallery->gallery_id;
        }
    }

    function gallery_edit() {

        if (Request::isMethod("post")) {
            $gallery_id = Input::get("gallery_id");
            $gallery = Gallery::where("gallery_id", $gallery_id)->update(array(
                "gallery_name" => Input::get("gallery_name"),
                "gallery_author" => Input::get("gallery_author")
            ));
            return $gallery_id;
        }
    }

    function gallery_delete() {

        if (Request::isMethod("post")) {
            $gallery_id = Input::get("gallery_id");
            DB::table("galleries")->where("gallery_id", $gallery_id)->delete();
            DB::table("galleries_media")->where("gallery_id", $gallery_id)->delete();
        }
    }

    public function save() {

        if (Request::isMethod("post")) {
            $media_id = Input::get("file_id");

            if (!Input::has("file_motive")) {
                $file_motive = 0;
            } else {
                $file_motive = 1;
            }

            DB::table("media")->where("media_id", "=", $media_id)->update(array(
                "media_title" => Input::get("file_title"),
                "media_description" => Input::get("file_description"),
                "media_motive" => $file_motive
            ));
            /*
              DB::connection("mongodb")
              ->collection('media')
              ->where("media_id", "=", (int) $media_id)
              ->update(array(
              "media_title" => Input::get("file_title"),
              "media_description" => Input::get("file_description"),
              "media_motive" => (int) $file_motive
              ));
             * 
             */
        }
    }

    function download_file($media_path = false) {

        $link = uploads_url($media_path);
        $parts = explode("/", $media_path);
        $file = end($parts);

        if (strstr($media_path, "/")) {
            return copy($link, UPLOADS . "/" . $file);
        }
    }

    public function check_free() {

        $media_path = Input::get('media_path');

        $this->download_file($media_path);

        $parts = explode("/", $media_path);
        $filename = end($parts);

        \Image::make(UPLOADS . "/" . $filename)
                ->resize(570, null, function ($constraint) {
                    $constraint->aspectRatio();
                    $constraint->upsize();
                })
                ->save(UPLOADS . "/free-" . $filename);

        if (strstr($media_path, "/")) {
            $parts = explode("/", $media_path);
            s3_save($parts[0] . "/" . $parts[1] . "/" . "free-" . $parts[2]);
        } else {
            s3_save("uploads/uploads/free-" . $filename);
        }

        @unlink("uploads/" . $filename);
        @unlink("uploads/free-" . $filename);
    }

    public function delete() {

        if (Request::isMethod("post")) {

            $media_id = Input::get("media_id");
            DB::table("media")->where("media_id", "=", $media_id)->delete();

            /*
              DB::connection("mongodb")
              ->collection('media')
              ->where("media_id", "=", (int) $media_id)
              ->delete();
             * 
             */

            $media_path = Input::get("media_path");

            if (strstr($media_path, "/")) {
                s3_delete($media_path);
            }

            if (file_exists(uploads_path($media_path))) {
                @unlink(uploads_path($media_path));
            }

// delete all sizes
            $parts = explode(".", $media_path);
            $extension = end($parts);
            if (in_array(strtolower($extension), array("jpg", "jpeg", "gif", "png", "bmp"))) {
// resize
                $sizes = Config::get("media.sizes");
                foreach ($sizes as $size => $dimensions) {

                    if (strstr($media_path, "/")) {
                        $dir_parts = explode("/", $media_path);
                        $file = $dir_parts[0] . "/" . $dir_parts[1] . "/" . $size . "-" . $dir_parts[2];
                        s3_delete($file);
                    }

                    if (File::exists(uploads_path($size . "-" . $media_path))) {
                        @unlink(uploads_path($size . "-" . $media_path));
                    }
                }
            }
        }
    }

}

/*
  $request = $this->get('request');
  $files = $request->files;

  // configuration values
  $directory = public_path() . '/uploads/';

  // $file will be an instance of Symfony\Component\HttpFoundation\File\UploadedFile
  foreach ($files as $uploadedFile) {
  // name the resulting file
  $name = //...
  $file = $uploadedFile->move($directory, $name);

  // do something with the actual file
  $this->doSomething($file);
  }

  // return data to the frontend
  return new JsonResponse([]);
 */