<?php

class ApiController extends BackendController {

    public $conn = '';

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function __construct() {
        date_default_timezone_set("Etc/GMT-2");

        $this->conn = Session::get('conn');
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index() {
        
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function news() {
        $posts = Post::with(['categories', 'image'])->where('post_type', 'post')
                ->where('post_status', 1)
                ->orderBy('post_published_date', 'desc')
                ->take(10)
                ->get();

        $arr = array();
        $arr ['s'] = 1;
        foreach ($posts as $key => $post) {
            $arr ['d'][] = [
                "Title" => $post->post_title,
                "publishTime" => $post->post_published_date,
                "Abstract" => $post->post_excerpt,
                "NewsURL" => URL::to('/details') . "/" . $post->post_slug,
                "Image" => AMAZON_URL . @$post->image->media_path,
                "Attributes" => [
                    [
                        "name" => 'referenced',
                        "value" => [
                            "referenced_title" => @$post->categories->first()->cat_name,
                            "referenced_link" => (@$post->categories->first()->cat_slug) ? URL::to('/category') . "/" . @$post->cat_slug : ''
                        ]
                    ],
                    [
                        "name" => "thumbnail",
                        "value" => [
                            "thumbnail_url" => thumbnail(@$post->image->media_path, 'small')
                        ]
                    ]
                ]
            ];
        }

        //return \Response::json(array('response' => $arr, 'error' => ''), 200);
        return \Response::json($arr);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function videos() {

        $posts = Post::with(['media', 'editor'])->where('post_type', 'post')
                ->where('post_status', 1)
                ->where('post_format', 4)
                ->orderBy('post_published_date', 'desc')
                ->take(10)
                ->get();

        $arr = array();
        $arr ['s'] = 1;
        foreach ($posts as $key => $post) {
            $related = Post::where('post_type', 'post')
                    ->where('post_status', 1)
                    ->where('post_format', 4)
                    ->where('post_id', '!=', $post->post_id)
                    ->take(2)
                    ->get();
            $related = $related->toArray();

            $arr ['d'][] = [
                "id" => $post->post_id,
                "thumbPic" => $post->media->media_provider_image,
                "detailPic" => $post->media->media_provider_image,
                "title" => $post->post_title,
                "uploadBy" => @$post->editor->username,
                "intro" => $post->media->media_title,
                "fromSite" => $post->media->media_provider,
                "duration" => $post->media->media_duration,
                "webUrl" => $post->media->media_path,
                "playUrl" => $post->media->media_path,
                "fileUrl" => "",
                "publishTime" => $post->post_published_date,
                "updateTime" => $post->post_updated_date,
                "click" => $post->post_views,
                "recommended" => [
                    [
                        "relatedId" => $related[0]['post_id']
                    ],
                    [
                        "relatedId" => $related[1]['post_id']
                    ]
                ]
            ];
        }

        //return \Response::json(array('response' => $arr, 'error' => ''), 200);
        return \Response::json($arr);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create() {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id) {
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id = 0) {
        
    }

}
