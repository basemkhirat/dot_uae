<?php

Route::group(array(
    "prefix" => ADMIN,
    "before" => "auth",
        ), function($route) {
    $route->group(array("prefix" => "notifications"), function($route) {
        $route->controller('/', 'NotificationsController');
    });
});