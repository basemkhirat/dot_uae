<?php

Menu::set("topnav", function($menu) {
    if (Auth::check()) {
        $user = User::find(Auth::user()->id);
        $notifications = $user->notifications()->limit(10)->orderBy('created_at', 'desc')->get();
        $count = $user->notifications()->unseen()->count();
        $data['notifications'] = $notifications;
        $data['unseenCount'] = $count;
        $menu->make("notifications::dropmenu", $data);
    }
});

include __DIR__ . "/routes.php";
