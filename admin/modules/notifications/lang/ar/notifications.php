<?php

return array(
    'publish' => 'تم نشر الخبر :title ',
    'unpublish' => 'تم سحب الخبر :title من النشر  ',
    'status_changes' => 'تم تغيير حالة الخبر :title من :state_fromالى :state_to ',
    'desk_changed' => 'تم تغيير حالة الدسك للخبر :title ',
    'left_comment' => 'ترك تعليق على الخبر :title',
    'notifications' => 'التنبيهات',
    'sender' => 'الراسل',
    'notification' => 'التنبية',
    'since' => 'منذ',
    "no_notification" => "لا توجد تنبيهات",
    "see_all" => "مشاهدة الكل"
);
