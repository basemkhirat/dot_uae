<?php

return array(
    'publish' => ':title was published',
    'unpublish' => ':title was unpublished',
    'status_changes' => ':title state wsa changed from :state_from to :state_to',
    'desk_changed' => ':title desk state was changed',
    'left_comment' => 'Left a comment on ":title"',
    'notifications' => 'Notifications',
    'sender' => 'Sender',
    'notification' => 'Notification',
    'since' => 'Since',
    "no_notification" => "No notifications",
    "see_all" => "See all"
);
