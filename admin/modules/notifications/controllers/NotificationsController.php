<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of NotificationsController
 *
 * @author Khaled PC
 */
class NotificationsController extends BackendController {

    function getIndex() {
        $user = User::find(Auth::user()->id);
        // dd($user->notifications);
        return View::make('notifications::index')
                        ->with('unredCount', $user->notifications()->whereIsRead('0')->count())
                        ->withNotifications($user->notifications()->orderBy('created_at', 'desc')->paginate(20));
    }

    function getRedirect($id) {
        $notification = Notification::find($id);
        $notification->is_read = 1;
        $notification->save();
        switch ($notification->object_type) {
            case 'post':
                return Redirect::to(ADMIN . '/posts/' . $notification->object_id . '/edit');
            case 'note':
                $note = Note::find($notification->object_id);
                return Redirect::to(ADMIN . '/posts/' . $note->note_post_id . '/edit');
            case'comment':
                $comment = Comment::find($notification->object_id);
                if ($comment) {
                    return Redirect::to(ADMIN . '/comments/' . $comment->comment_id . '/edit');
                } else {
                    $notification->delete();
                    return Redirect::to(ADMIN);
                }
        }
    }

    function getSearchUsers() {
        if (Input::get('idStr')) {
            $ids = explode(',', Input::get('idStr'));
//            dd($ids);
            $users = User::whereIn('id', $ids)->get();
        } else {
            $keyword = Input::get('q');
            $users = User::where('first_name', 'LIKE', '%' . $keyword . '%')
                            ->orWhere('last_name', 'LIKE', '%' . $keyword . '%')
                            ->limit(5)->get();
        }
        $return = array();
        foreach ($users as $user) {
            $current = array();
            $current['id'] = $user->id;
            $current['image'] = $user->getImageUrl('thumbnail');
            $current['name'] = $user->name;
            $return['items'][] = $current;
        }

        return $return;
    }

}
