<?php

class Notification extends Model {

    protected $table = 'notifications';
    protected $fillable = array('user_id', 'type', 'subject', 'body', 'object_id', 'object_type', 'sent_at');

    public function getDates() {
        return array('created_at', 'updated_at', 'sent_at');
    }

    public function user() {
        return $this->belongsTo('User');
    }

    public function sender() {
        return $this->belongsTo('User', 'sender_id');
    }

    public function getTimeAgoAttribute() {
        return time_ago($this->created_at->toDateTimeString());
    }

    public function getTimeAgoShortAttribute() {
        return time_ago($this->created_at->toDateTimeString(), 1);
    }

    public function getSenderNameAttribute() {
        if ($this->sender) {
            $name = $this->sender->name;
        } else {
            $name = ($this->object_type == 'comment') ? Comment::find($this->object_id)->comment_author_email : '';
        }
        return $name;
    }

    public function getSenderImageAttribute() {
        if ($this->sender) {
            $image = $this->sender->getImageUrl('thumbnail');
        } else {
            $image = assets('images/author.png');
        }
        return $image;
    }

    public function getUrlAttribute() {
        return URL::to(ADMIN.'/notifications/redirect/' . $this->id);
    }

    public static function scopeUnseen($query) {
        return $query->whereIsRead(0);
    }

}
