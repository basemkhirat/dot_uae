@extends("admin::layouts.master")

@section("breadcrumb")
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>{!!trans('notifications::notifications.notifications')!!} ({!!$unredCount!!})</h2>
        <ol class="breadcrumb" style="background: none;">
            <li><a  href="{!!URL::to('/'.ADMIN.'/dashboard')!!}">{!!Lang::get('admin::common.dashboard')!!}</a></li>
            <li>{!!trans('notifications::notifications.notifications')!!}</li>
        </ol>
    </div>

</div>
@stop
@section("content")

<div id="content-wrapper">

    <div class="row" style="margin-top: 10px;">
        <div class="col-sm-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5> {!!trans('notifications::notifications.notifications')!!} </h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <div class="table-responsive">
                        <table cellspacing="0" cellpadding="0" border="0" class="table table-striped">
                            <thead>
                                <tr>
                                    <th>{!!trans('notifications::notifications.sender')!!}</th>
                                    <th>{!!trans('notifications::notifications.notification')!!}</th>
                                    <th>{!!trans('notifications::notifications.since')!!}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($notifications as $notification)
                                <tr message {!!($notification->is_read)? '' : 'unread'!!}>
                                    <td><a href="{!!URL::to(ADMIN.'/notifications/redirect/' . $notification->id)!!}" title="" class="from">{!!$notification->sender_name!!}</a></td>
                                    <td><a href="{!!URL::to(ADMIN.'/notifications/redirect/' . $notification->id)!!}" title="" class="title">{!!$notification->body!!}</a></td>
                                    <td>{!!$notification->time_ago_short!!}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <div class="panel-footer text-right" style="border-top: 1px solid #ececec; background: none;">

                            <div class="col-sm-10 m-b-xs" style="padding:0">
                                <div class="pull-right">
                                    {!!@$notifications->appends(Input::all())->setPath('')->render()!!}
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
