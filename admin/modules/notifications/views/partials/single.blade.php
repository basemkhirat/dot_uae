<div class="message">
    <a href="{!!$notification->url!!}"><img src="{!!$notification->sender_image!!}" alt="" class="message-avatar"></a>
    <a href="{!!$notification->url!!}" class="message-subject">{!!$notification->body!!}</a>
    <div class="message-description">
        <a href="{!!$notification->url!!}">{!!$notification->sender_name!!}</a>

        {!!$notification->timeAgo!!}
    </div>
</div> <!-- / .message -->