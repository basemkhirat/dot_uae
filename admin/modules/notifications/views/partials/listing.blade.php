<div class="dropdown-menu widget-messages-alt no-padding" style="width: 300px;">
    <div class="messages-list" id="main-navbar-messages">
        @foreach($notifications as $notification)
        @include('notifications::notifications.partials.single')
        @endforeach
        <a href="{!!URL::to('admin/notifications')!!}" class="messages-link">الكل</a>
    </div> <!-- / .notifications-list -->
</div> <!-- / .dropdown-menu -->