<li class="dropdown">
    <a class="dropdown-toggle count-info" data-toggle="dropdown" href="<?php echo assets() ?>/#">
        <i class="fa fa-bell"></i>  <span class="label label-primary">{{ $unseenCount ? $unseenCount : ''}}</span>
    </a>
    <ul class="dropdown-menu dropdown-messages">
        <?php if (count($notifications)) { ?>

            @foreach($notifications as $notification)
            <li>
                <div class="dropdown-messages-box">
                    <a href="{{$notification->url}}" class="pull-left">
                        <img alt="image" class="img-circle" src="{{$notification->sender_image}}">
                    </a>
                    <div class="media-body">
                        <small class="pull-right">{{$notification->timeAgo}}</small>
                        <strong>{{$notification->sender_name}}</strong> <br>
                        <small class="text-muted">{{$notification->timeAgo}}</small>
                    </div>
                </div>
            </li>
            <!--</li>-->
            <li class="divider"></li>

            @endforeach
            <li>
                <div class="text-center link-block">
                    <a href="{{URL::to(ADMIN.'/notifications')}}">
                        <i class="fa fa-envelope"></i> <strong><?php echo trans("notifications::notifications.see_all") ?></strong>
                    </a>
                </div>
            </li>

        <?php } else { ?>
            <?php echo trans("notifications::notifications.no_notification") ?>
        <?php } ?>

    </ul>
</li>