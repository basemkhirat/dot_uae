<?php

return array(
    "title" => "Categories",
    "add" => "Add category",
    "name" => "Name",
    "slug" => "Slug",
    "parent" => "Parent",
    "top_categories" => "Top categories",
    "del" => "Delete",
    "img" => "Image",
    "edit" => "Edit",
    "child_categories" => "Child categories",
    "sure_delete" => "Delete category?",

    "categories" => "Categories",
    "search_categories" => "Search categories",

    "add_new" => "Add new",
    "add_new_category" => "Add new category",
    "edit_category" => "Edit category",
    "change" => "Change",
    "cat_name" => "Category name",
    "cat_slug" => "Category name",
    "cat_parent" => "Parent category",
    "save_category" => "Save category",
    "main_category" => "Main category",
    "bulk_actions" => "bulk actions",
    "apply" => "Apply",
    "image" => "Image",
    "name" => "Name",
    "actions" => "Actions",
    "sure_delete" => "Are you want to delete category?",
    "no_categories" => "No categories",
    "page" => "Page",
    "of" => "of",
    "per_page" => "Per page",
    "no_categories" => "No categories",
    "delete" => "Delete", 
    "category_created" => "Category created successfully",
    "category_updated" => "Category updated successfully",
    "category_deleted" => "Category deleted successfully",
    "cats_sources" => "Aggregator Categories",
    
    "module" => "Categories",    
    "permissions" => [
        "manage" => "manage categories",
        "stats" => "manage categories statistics"
    ]



);
