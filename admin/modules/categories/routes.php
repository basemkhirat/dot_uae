<?php

Route::group(array(
    "prefix" => ADMIN,
    "before" => "auth",
        ), function($route) {
    $route->group(array("prefix" => "categories"), function($route) {
        $route->any('/slug', array("as" => "categories.slug", "uses" => "CategoryController@slug"));
        $route->any('/delete', array("as" => "categories.delete", "uses" => "CategoryController@delete"));
        $route->any('/create', array("as" => "categories.create", "uses" => "CategoryController@create"));
        $route->any('/{id}/edit', array("as" => "categories.edit", "uses" => "CategoryController@edit"));
        $route->any('/{parent?}', array("as" => "categories.show", "uses" => "CategoryController@index"));
    });
});
