<?php

class CategoryController extends BackendController {

    public $data = array();
    public $conn = '';
    
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function __construct() {
        date_default_timezone_set("Etc/GMT-2");
        $this->conn = Session::get('conn');
    }
    
    private function categories($parent) {
        
        
        $query = Category::leftJoin("media", "media.media_id", "=", "categories.cat_image_id");

        if (Input::has("q")) {
            $query->where('cat_name', 'LIKE', '%' . Input::get("q") . '%');
        }else{
            $query->where("cat_parent", $parent);
        }

        $query->where("site", $this->conn);
        
        if (Input::has("per_page")) {
            $this->data["per_page"] = $per_page = Input::get("per_page");
        } else {
            $this->data["per_page"] = $per_page = 20;
        }
        
        $this->data['mongo_cats'] = DB::connection('mongodb')->collection('categories')->where('status', (int) 1)->get();
        
        $this->data["categories"] = $query->paginate($per_page);
    }

    public function index($parent = 0) {

        if (Request::isMethod("post")) {
            $action = Input::get("action");
            $ids = Input::get("id");
            if (count($ids)) {
                switch ($action) {
                    case "delete":
                        Category::whereIn("cat_id", $ids)->delete();
                        foreach ($ids as $id) {
			            	if(\File::exists(public_path()."/blocks/1/".$id.'.json')){
				            	\File::delete(public_path()."/blocks/1/".$id.'.json');
				        	}
			            }
                        return Redirect::route("categories.show")->with("message", trans("cats::cats.category_deleted"));
                        break;
                }
            }
        }

        $this->data["parent"] = $parent;
        $this->categories($parent);

        $this->data["category"] = false;
        return View::make("cats::show", $this->data);
    }

    public function slug() {

        if (Request::isMethod("post")) {
            $name = Input::get("name");

            if(trim($name) == ""){
            	return "";
            }

            $slug = str_slug($name);
            if (Category::where("cat_slug", "=", $slug)->count()) {
                return $slug . "-1";
            } else {
                return $slug;
            }
        }
    }

    public function create() {
        
        if (Request::isMethod("post")) {


            $slug = Input::get("cat_slug");
            $cats = Input::get("cats");
            
            if($slug != ""){
	            if (Category::where("cat_slug", "=", $slug)->count()) {
	                return Redirect::back()->withErrors($slug . " موجود مسبقا")->withInput(Input::all());
	            }
        	}

            $rules = array(
                'cat_name' => 'required',
                'cat_slug' => 'required'
            );

            $validator = Validator::make(Input::all(), $rules);
            if ($validator->fails()) {
                return Redirect::back()->withErrors($validator)->withInput(Input::all());
            }

            Category::insert(array(
                "cat_slug" => $slug,
                "cat_parent" => Input::get("cat_parent"),
                "cat_image_id" => Input::get("cat_image_id"),
                "cat_name" => Input::get("cat_name"),
                "site" => $this->conn,
                "mongo_id" => $cats
            ));

            $cat_id = DB::getPdo()->lastInsertId();

            $path = public_path("blocks/1/".$cat_id.'.json');
    		\File::put($path, '[]');

            return Redirect::route("categories.edit", array("id" => $cat_id))->with("message", trans("cats::cats.category_created"));
        }
    }

    public function edit($cat_id) {

        $category = Category::leftJoin("media", "media.media_id", "=", "categories.cat_image_id")
                        ->where("categories.cat_id", "=", $cat_id)->first();

        if (count($category) == 0) {
            \App::abort(404);
        }

        $this->data["parent"] = $category->cat_parent;
        $this->categories($category->cat_parent);

        if (Request::isMethod("post")) {

            $rules = array(
                'cat_name' => 'required',
                'cat_slug' => 'required'
            );

            $validator = Validator::make(Input::all(), $rules);
            if ($validator->fails()) {
                return Redirect::back()->withErrors($validator)->withInput(Input::all());
            }
            
            $cats = Input::get("cats");
            
            Category::where("cat_id", $cat_id)->update(array(
                "cat_parent" => Input::get("cat_parent"),
                "cat_slug" => Input::get("cat_slug"),
                "cat_image_id" => Input::get("cat_image_id"),
                "cat_name" => Input::get("cat_name"),
                "mongo_id" => $cats
                
            ));

            return Redirect::back()->with("message", trans("cats::cats.category_updated"));
        }

        $this->data['mongo_cats'] = DB::connection('mongodb')->collection('categories')->where('status', (int) 1)->get();
        
        $this->data["category"] = $category;
        return View::make("cats::show", $this->data);
    }


    public function delete() {
        $ids = Input::get("id");
        if (is_array($ids)) {
            Category::whereIn("cat_id", $ids)->delete();
            foreach ($ids as $id) {
            	if(\File::exists(public_path()."/blocks/1/".$id.'.json')){
	            	\File::delete(public_path()."/blocks/1/".$id.'.json');
	        	}
            }
        } else {
            Category::where("cat_id", $ids)->delete();
            if(\File::exists(public_path()."/blocks/1/".$id.'.json')){
            	\File::delete(public_path()."/blocks/1/".$id.'.json');
        	}
        }
        if (!Request::ajax()) {
            return Redirect::route("categories.show")->with("message", trans("cats::cats.category_deleted"));
        }
    }
    
    /*==========================Adnan=========================*/
    /**
    * Display a listing of the resource.
    *
    * @return Response
    */
    public function ajax_add_cat()
      { 
        $name = Input::get('name');
        $parent = Input::get('parent');
        $found = false;
        $id = '';

        $cat = Category::get_cat_id(['name' => $name, 'lang' => App::getLocale(), 'conn' => $this->conn]);

        if( @$cat->cat_id ){
            $found = true;
            $id = $cat->cat_id;
        }else{
            $slug = create_slug( $name );
            //$slug = Str::slug( $name );
            $id = Category::add_cat(['name' => $name, 'slug' => $slug, 'parent' => $parent, 'lang' => App::getLocale(), 'conn' => $this->conn]);

            //add block
            $block_id = Block::add_block(['name' => 'featured '.$slug, 'slug' => 'featured-'.$slug, 'conn' => $this->conn]);
            create_block_file( $block_id );
        }

        $prop = array( 'found' => $found, 'id' => $id );
        echo json_encode( $prop );
        exit;

      }
    /*==========================Adnan=========================*/
}
