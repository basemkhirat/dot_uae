@extends("cms::layouts.master")


@section("breadcrumb")
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-4">
        <h2><?php echo trans("cms::categories.categories") ?></h2>
        <ol class="breadcrumb">
            <li>
                <a href="<?php echo route("categories.show"); ?>"><?php echo trans("cms::categories.categories") ?> (<?php echo $categories->total() ?>)</a>
            </li>
        </ol>
    </div>

    <div class="col-lg-6">
        <form action="" method="get" class="search_form">
            <div class="input-group">
                <input name="q" value="<?php echo Input::get("q"); ?>" type="text" class=" form-control" placeholder="<?php echo trans("cms::categories.search_categories") ?>..."> <span class="input-group-btn">
                    <button class="btn btn-primary" type="button"> البحث</button> </span>
            </div>
        </form>
    </div>

    <div class="col-lg-2">
        <a href="<?php echo route("categories.show"); ?>" class="btn btn-primary btn-labeled btn-main"> <span class="btn-label icon fa fa-plus"></span> &nbsp; <?php echo trans("cms::categories.add_new") ?></a>
    </div>
</div>
@stop



@section("content")

<?php
$conn = Session::get('conn');
?>

<div id="content-wrapper">

    <?php if (Session::has("message")) { ?>
        <div class="alert alert-success alert-dark">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <?php echo Session::get("message"); ?>
        </div>
    <?php } ?>

    <?php if ($errors->count() > 0) { ?>
        <div class="alert alert-danger alert-dark">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <?php echo implode(' <br /> ', $errors->all()) ?>
        </div>
    <?php } ?>



    <div class="row">
        <div class="col-md-6">

            <div class="panel panel-default">
                <div class="panel-heading">
                    <span class="panel-title">
                        <i class="fa fa-folder"></i> &nbsp;&nbsp;&nbsp;
                        <?php if (!$category) { ?>
                            <?php echo trans("cms::categories.add_new_category") ?>

                        <?php } else { ?>
                            <?php echo trans("cms::categories.edit_category") ?>

                        <?php } ?>
                    </span>
                </div>
                <?php if (!$category) { ?>
                    <form method="post" action="<?php echo route("categories.create"); ?>">
                    <?php } else { ?>
                        <form method="post" action="<?php echo route("categories.edit", array("id" => $category->cat_id)); ?>">
                        <?php } ?>
                        <div class="panel-body">

                            <div class="col-md-2 text-center" style="position:relative; margin: 10px 0">
                                <input type="hidden" value="<?php
                                if ($category) {
                                    echo $category->cat_image_id;
                                }
                                ?>" id="category_image_id" name="cat_image_id" /> 
                                <img id="category_image" style="border: 1px solid #ccc; width: 100%;" src="<?php
                                if ($category) {
                                    echo thumbnail($category->media_path, "thumbnail", "default/category.png");
                                } else {
                                    echo assets("default/category.png");
                                }
                                ?>" />
                                <a href="javascript:void(0)" id="change_photo" class=""><?php echo trans("cms::categories.change") ?></a>
                            </div>
                            <div class="col-md-10">

                                <div class="form-group input-group">
                                    <span class="input-group-addon"><i class="fa fa-folder"></i></span>
                                    <input  name="cat_name" value="<?php echo @Input::old("cat_name", $category->cat_name); ?>" class="form-control input-lg" placeholder="<?php echo trans("cms::categories.cat_name") ?>" />
                                </div>

                                <div class="form-group input-group">
                                    <span class="input-group-addon"><i class="fa fa-chain"></i></span>
                                    <input name="cat_slug" value="<?php echo @Input::old("cat_slug", $category->cat_slug); ?>" class="form-control input-lg" placeholder="<?php echo trans("cms::categories.cat_slug") ?>" />
                                </div>

                                <select class="form-control select2 chosen-rtl" name="cat_parent">
                                    <option selected="selected"  value="0"><?php echo trans("cms::categories.cat_parent"); ?></option>
                                    <?php foreach (Mediasci\Cms\Models\Category::tree(0, 0, $conn) as $row) { ?>
                                        <option value="<?php echo $row->cat_id; ?>" <?php if ($category and $category->cat_parent == $row->cat_id) { ?> selected="selected"<?php } elseif ($parent and $parent == $row->cat_parent) { ?> selected="selected" <?php } ?> ><?php echo $row->cat_name; ?></option>
                                    <?php } ?>
                                </select>

                            </div>

                        </div>

                        <div class="panel-footer">
                            <div class="form-group">
                                <input type="submit" class="pull-right btn btn-flat btn-primary" value="<?php echo trans("cms::categories.save_category") ?>" />
                            </div>
                        </div>

                    </form>
            </div>

        </div>

        <div class="col-md-6 pull-right">
            <form action="<?php echo route("categories.show"); ?>" method="post" class="action_form">



                <div class="ibox float-e-margins panel panel-default">
                    <?php if ($parent != 0) { ?>
                        <div class="ibox-title">
                            <h5> 
                                <div class="row">
                                    <div class="col-sm-12">
                                        <ul class="breadcrumb">
                                            <li><a href="<?php echo URL::to(ADMIN . "/categories/0"); ?>"><?php echo trans("cms::categories.categories") ?></a></li>
                                            <?php foreach (Mediasci\Cms\models\Category::map($parent) as $category) { ?>
                                                <li><a href="<?php echo URL::to(ADMIN . "/categories/" . $category->cat_id); ?>"><?php echo $category->cat_name; ?></a></li>
                                            <?php } ?>

                                        </ul>
                                    </div>
                                </div>
                            </h5>
                        </div>
                    <?php } ?>
                    <div class="ibox-content">
                        <div class="row">
                            <div class="col-sm-5 m-b-xs">

                                <div class="form-group">
                                    <select name="action" class="form-control chosen-rtl" style="width:auto; display: inline-block;">
                                        <option value="-1" selected="selected"><?php echo trans("cms::categories.bulk_actions") ?></option>
                                        <option value="delete"><?php echo trans("cms::categories.delete") ?></option>
                                    </select>
                                    <button type="submit" class="btn btn-primary" style="margin-top:5px"><?php echo trans("cms::categories.apply") ?></button>
                                </div>
                            </div>
                            <div class="col-sm-4 m-b-xs">

                            </div>
                            <div class="col-sm-3">

                                <select  name="post_status" id="post_status" class="form-control per_page_filter chosen-rtl">
                                    <option value="" selected="selected">-- <?php echo trans("cms::categories.per_page") ?> --</option>
                                    <?php foreach (array(10, 20, 30, 40) as $num) { ?>
                                        <option value="<?php echo $num; ?>" <?php if ($num == $per_page) { ?> selected="selected" <?php } ?>><?php echo $num; ?></option>
                                    <?php } ?>
                                </select>

                            </div>
                        </div>
                        <div class="table-responsive">

                            <table cellpadding="0" cellspacing="0" border="0" class="table table-striped" id="jq-datatables-example">
                                <thead>
                                    <tr>
                                        <th style="width:35px"><input type="checkbox" class="check_all i-checks" name="ids[]" /></th>
                                        <th style="width:50px"><?php echo trans("cms::categories.image") ?></th>
                                        <th><?php echo trans("cms::categories.name"); ?></th>
                                        <th><?php echo trans("cms::categories.actions") ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php if (count($categories)) { ?>
                                        <?php foreach ($categories as $category) { ?>
                                            <tr>
                                                <td>
                                                    <input type="checkbox" name="id[]" value="<?php echo $category->cat_id; ?>" class="i-checks" />
                                                </td>
                                                <td>
                                                    <img src="<?php echo thumbnail($category->media_path, "thumbnail", "default/category.png"); ?>" style="width:50px" />
                                                </td>
                                                <td>
                                                    <a class="text-navy" href="<?php echo URL::to(ADMIN) ?>/categories/<?php echo $category->cat_id; ?>">
                                                        <?php echo $category->cat_name; ?>
                                                    </a>
                                                </td>
                                                <td class="center">
                                                    <a href="<?php echo URL::to(ADMIN) ?>/categories/<?php echo $category->cat_id; ?>/edit">
                                                        <i class="fa fa-pencil text-navy"></i>
                                                    </a>
                                                    <a class="ask" message="<?php echo trans("cms::categories.sure_delete") ?>" href="<?php echo URL::to(ADMIN) ?>/categories/delete?id[]=<?php echo $category->cat_id; ?>">
                                                        <i class="fa fa-times text-navy"></i>
                                                    </a> 
                                                </td>
                                            </tr>
                                        <?php } ?>
                                    <?php } else { ?>


                                        <tr>
                                            <td colspan="4">
                                                <?php echo trans("cms::categories.no_categories") ?>
                                            </td>
                                        </tr>
                                    <?php } ?>

                                </tbody>
                            </table>

                        </div>

                        <div class="row">
                            <div class="col-sm-4">
                                <div class="dataTables_info" id="editable_info" role="status" aria-live="polite">
                                    <?php echo trans("cms::categories.page") ?> <?php echo $categories->currentPage() ?> <?php echo trans("cms::categories.of") ?> <?php echo $categories->lastPage() ?>

                                </div>
                            </div>
                            <div class="col-sm-8">
                                <div class="dataTables_paginate paging_simple_numbers" id="editable_paginate">
                                    <?php echo str_replace('/?', '?', $categories->appends(Input::all())->render()); ?>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </form>
            <?php /* ?>
              <div class="col-md-6" style="width: 60%">
              <div class="media_rows">

              <ul class="breadcrumb breadcrumb-no-padding">
              <li><a href="#"><?php echo trans("cms::categories.galleries"); ?></a></li>
              <li><a href="#"><?php  echo @$row->gallery_name; ?> (<?php echo @count($gallery_media); ?>)</a></li>
              </ul>

              <?php if (count($gallery_media)) { ?>
              <?php foreach ($gallery_media as $media) { ?>
              <div class="media_row" >
              <input type="hidden" name="media_id[]" value="<?php echo $media->media_id; ?>" />

              <?php if ($media->media_provider == "") { ?>
              <img src="<?php echo thumbnail($media->media_path); ?>">
              <?php } else { ?>
              <?php if ($media->media_provider_image != "") { ?>
              <img src="<?php echo $media->media_provider_image; ?>" />
              <?php } else { ?>
              <img src="<?php echo assets("default/soundcloud.png"); ?>" />
              <?php } ?>
              <?php } ?>
              <label><?php echo $media->media_title; ?></label>
              </div>
              <?php } ?>
              <?php }else{ ?>
              No media found in <?php echo @$row->gallery_name; ?>
              <?php } ?>

              </div>
              </div>
              <?php */ ?>



            <!-- /11. $JQUERY_DATA_TABLES -->

            <!-- /9. $UNIQUE_VISITORS_STAT_PANEL -->

        </div>
    </div> <!-- / #content-wrapper -->
    <style>

        .select2-container  {
            font-size: 16px;
            height: 42px;
            line-height: 22px;
        }

    </style>
    <script>
        $(document).ready(function () {


            var elems = Array.prototype.slice.call(document.querySelectorAll('.permission-switcher'));
            elems.forEach(function (html) {
                var switchery = new Switchery(html);
            });

            var config = {
                '.select2': {},
                '#cats': {},
            }
            for (var selector in config) {
                $(selector).chosen(config[selector]);
            }



            $('.i-checks').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green',
            });

            $('.check_all').on('ifChecked', function (event) {
                $("input[type=checkbox]").each(function () {
                    $(this).iCheck('check');
                    $(this).change();
                });
            });

            $('.check_all').on('ifUnchecked', function (event) {
                $("input[type=checkbox]").each(function () {
                    $(this).iCheck('uncheck');
                    $(this).change();
                });
            });


            $("input[name=cat_name]").blur(function () {
                var base = $(this);
                var name = base.val();
                $.post('{{ URL::to(ADMIN."/categories/slug") }}', {name: name}, function (cat_slug) {
                    $("input[name=cat_slug]").val(cat_slug);
                });
            });


            $(".per_page_filter").change(function () {
                var base = $(this);
                var per_page = base.val();
                location.href = "<?php echo route("categories.show", array("parent" => 0)) ?>?per_page=" + per_page;
            });



        });



    </script>

    <script>
        $(document).ready(function () {
            $("#change_photo").filemanager({
                types: "image",
                done: function (result, base) {
                    if (result.length) {
                        var file = result[0];
                        $("#category_image_id").val(file.media_id);
                        $("#category_image").attr("src", file.media_thumbnail);
                    }
                },
                error: function (media_path) {
                    alert(media_path + " <?php echo trans("cms::categories.is_not_an_image") ?>");
                }
            });
        });

    </script>


    @stop