<?php

class Category extends Model {

    protected $table = 'categories';
    protected $primaryKey = 'cat_id';
    protected $parentKey = 'cat_parent';
    
    public $timestamps = false;

    //protected $visible = array('id', 'description', 'date', 'editor', 'likes_count', 'dislikes_count', 'comments_count', 'image_url', 'thumb_url', 'rank','address','long','lat');
    //protected $appends = array('id', 'description', 'date', 'editor', 'likes_count', 'dislikes_count', 'comments_count', 'image_url', 'thumb_url', 'rank');

    public function lang() {
        return $this->hasOne('CategoryLang', 'cat_id', 'cat_id');
    }

    public function scopeParent($query, $parent) {
        return $query->where("cat_parent", $parent);
    }

   /* public static function tree($parent = 0, $depth = 0, $conn) {

        $cats = Category::where("cat_parent", $parent)->where('site', $conn)->get();

        $new_cats = array();
        if (count($cats)) {
            foreach ($cats as $cat) {
                $row = $cat;
                $row->depth = $depth;
                $row->children = self::tree($cat->cat_id, $depth++, $conn);
                $new_cats[] = $row;
            }
        }

        return $new_cats;
    }*/

    public static function map($parent = 0) {

        $row = Category::where("categories.cat_id", $parent)->first();

        static $new_cats = array();
        if (count($row)) {
            $new_cats[] = $row;
            self::map($row->cat_parent);
        }

        return array_reverse($new_cats);
    }

    public static function getCategories($db) {
        return $categories = DB::table('categories')
                ->where('categories.site', '=', $db)
                ->get();
    }

    public static function getChildCategories($conn, $parent_id = 0, $key = "", $db) {
        $lang = Lang::getLocale();
        $categories = DB::table('categories')->where("cat_parent", "=", $parent_id)
                ->leftJoin("media", "media.media_id", "=", "categories.cat_img")
                ->where('categories.site', '=', $db);
        if ($key != "") {
            $categories->where("cat_name", "LIKE", '%' . $key . '%');
        }
        return $categories->paginate(20);
    }

    public static function getcategory($id) {
        return $category = DB::table('categories')
                ->leftJoin("media", "media.media_id", "=", "categories.cat_img")
                ->where("categories.cat_id", "=", $id)
                ->get();
    }

    public static function getCategoryLangs($id, $code) {
        return $langs = DB::table('categories')
                ->where("cat_id", "=", $id)
                ->where("site", "=", $code)
                ->get();
    }

    public static function saveCategory($row) {
        return $id = DB::table('categories')->insertGetId($row);
    }

    public static function saveCategorylangs($row) {
        DB::table('categories')->insert($row);
    }

    public static function updateCategory($row, $id) {
        DB::table('categories')->where('cat_id', $id)->update($row);
    }

    public static function updateCategorylangs($row, $id, $lang) {
        DB::table('categories')
                ->where('cat_id', $id)
                ->where('site', $lang)
                ->update($row);
    }

    public static function deleteCategory($id) {
        DB::table('categories')->where('cat_id', '=', $id)->delete();
    }

    public function posts() {
        return $this->belongsToMany('Post', 'posts_categories', 'cat_id', 'post_id')->select(DB::raw('count(posts_categories.cat_id) as counts'))
                        ->groupBy('posts_categories.cat_id')->orderBy('counts', 'desc');
    }

    public function postViews() {
        return $this->belongsToMany('Post', 'posts_categories', 'cat_id', 'post_id')
                        ->select(DB::raw('sum(post_views) as total'))->groupBy('cat_id')->where('post_type', 'post');
    }

    public function postStats() {
        return $this->belongsToMany('PostStat', 'posts_categories', 'cat_id', 'post_id')
                        ->select(DB::raw('sum(facebook) as facebook'), DB::raw('sum(twitter) as twitter'), DB::raw('sum(youtube) as youtube'))
                        ->leftJoin('posts', 'posts.post_id', '=', 'posts_stats.post_id')->where('post_type', 'post')->groupBy('cat_id');
    }

}
