<?php

Menu::set("sidebar", function($menu) {
    if (User::can("categories.manage")) {
        $menu->item('news_options.categories', trans("admin::common.categories"), URL::to(ADMIN . '/grabber?page=cats'))->icon("fa-th-large")
                ->order(1);
    }
});

include __DIR__ . "/routes.php";
