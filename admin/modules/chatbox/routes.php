<?php

Route::group(array(
    "prefix" => ADMIN,
    "before" => "auth",
        ), function($route) {
    $route->group(array("prefix" => "chatbox"), function($route) {
        $route->get('systemMessage', 'ChatboxController@systemMessage');
    });
});
