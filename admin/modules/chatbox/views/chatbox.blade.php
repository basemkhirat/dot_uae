<!--<div class="small-chat-box fadeInRight animated">

    <div class="heading" draggable="true">
        <small class="chat-date pull-right">
            <i class="fa fa-comments"></i>
        </small>
        {{Lang::get('chatbox::chatbox.chat_now')}}
    </div>

    <div class="content" id="chat-log">

    </div>
    <div class="form-chat">
        <input type="text" class="form-control" id="chat-message" style="border: 1px solid #8EA6BF;">
    </div>

</div>
<div id="small-chat">

    <span class="badge badge-warning pull-right" id="chat-counter"></span>
    <a class="open-small-chat">
        <i class="fa fa-comments"></i>

    </a>
</div>

<script type="text/javascript" charset="utf-8">
    var username = "{!! Auth::user()->username !!}";
    var userId = "{!! Auth::user()->id !!}";
</script>

<script src="https://cdn.socket.io/socket.io-1.3.4.js"></script>
<script src="<?php echo assets() ?>/javascripts/socketConfig.js"></script>-->

<style>
    div.chat_wrap {
        bottom: 0px;
        display: block;
        //font-size: 0.85em;
        position: fixed;
        padding-bottom: 0px;
        //right: 229px;
        //width: 255px;
        z-index: 9999;
    }
    .chat_wrap .toggle {
        /*            background: #2F4050 none repeat scroll 0% 0%;
        font-weight: bold;
        color: #FFF;*/
        background: transparent linear-gradient(to bottom, #FAFAFA 0%, rgba(222, 215, 215, 0.32) 100%) repeat scroll 0% 0%;
        border: 1px solid rgba(222, 215, 215, 1);
        //color: #333;
        //border-radius: 5px;
        padding: 0.45rem 0px;
        text-align: center;
        width: 255px;
        cursor: pointer;
        /*            position: absolute;
                    bottom: 0;*/
    }
    .chat_wrap .toggle.open {
        background: #FFF; 
        border-top: none
    }
    div.chat_wrap div.toggle > h3 {
        margin: 0;
    }
    .small-chat-box{
        width: 255px;
        position: static;
    }

    .small-chat-box .heading{
        cursor: pointer;
    }
    .small-chat-box .form-chat{
        padding: 0;
    }
    .chat-message{
        border: none;
        border-top: 1px solid #EDE6E6;
        resize: none;
    }
    .chat-log{
        height: 250px;
    }
    #chat-hide{
        display: none;
        position: absolute;
        bottom: 6px;
        right: 3px;
        width: 130px;
        background: #FFF;
        border: 1px solid rgba(222, 215, 215, 0.8); 
        border-bottom: none; 
        border-top-left-radius: 5px; 
        border-top-right-radius: 5px;
        z-index: 9999;
    }
    #chat-hide li{
        padding:7px 5px; 
        margin: 0;
        cursor: pointer;
        display: block !important;
    }
    #chat-hide li:hover{
        background: rgb(83, 95, 161) none repeat scroll 0% 0%;
        color: rgb(255, 255, 255);
    }
    .chat-room{
        display: inline-block;
        vertical-align: bottom;
        margin-right: 3px;
    }
    .chat-room:first-of-type{
        margin: 0;
    }
    #hidden-chat{
        vertical-align: bottom;
        position: relative; 
        display: none;
        margin-right: 3px;
    }
    #hidden-chat .toggle{
        width:30px;
    }
    .room-notify{
        margin: 0 10px;
    }
</style>
<div class="wrapper wrapper-content animated fadeInRight chat_wrap">
    @foreach($chatGroups->groups as $group)
    <div class="chat-room" data-name="{!!$group->slug!!}">
        <div class="toggle">
            <span class="label label-success pull-right room-notify"></span>
            <h3>{!!$group->name!!}</h3>
        </div>
        <div class="small-chat-box" >

            <div class="heading" draggable="true">
                <small class="chat-date pull-right">
                    <i class="fa fa-comments" style="font-size: 17px;"></i>
                </small>
                {!!$group->name!!}
            </div>

            <div class="content chat-log">

            </div>
            <div class="form-chat">
                <textarea rows="2" class="form-control chat-message" name="cmessage" placeholder="{!!Lang::get('chatbox::chatbox.write_message')!!}"></textarea>
            </div>

        </div>
    </div>    
    @endforeach
    <!--    <div class="chat-room" data-name="room2">
            <div class="toggle">
                <span class="label label-success pull-right room-notify"></span>
                <h3>سياسة</h3>
            </div>
            <div class="small-chat-box fadeInRight animated">
    
                <div class="heading" draggable="true">
                    <small class="chat-date pull-right">
                        <i class="fa fa-comments"></i>
                    </small>
                    سياسة
                </div>
    
                <div class="content chat-log">
    
                </div>
                <div class="form-chat">
                    <textarea rows="2" class="form-control chat-message" name="cmessage" placeholder="{!!Lang::get('chatbox::chatbox.write_message')!!}"></textarea>
                </div>
    
            </div>
        </div>
    
        <div class="chat-room" data-name="room3">
            <div class="toggle">
                <span class="label label-success pull-right room-notify"></span>
                <h3>خارجيات</h3>
            </div>
            <div class="small-chat-box fadeInRight animated">
    
                <div class="heading" draggable="true">
                    <small class="chat-date pull-right">
                        <i class="fa fa-comments"></i>
                    </small>
                    خارجيات
                </div>
    
                <div class="content chat-log">
    
                </div>
                <div class="form-chat">
                    <textarea rows="2" class="form-control chat-message" name="cmessage" placeholder="{!!Lang::get('chatbox::chatbox.write_message')!!}"></textarea>
                </div>
    
            </div>
        </div>
    
        <div class="chat-room" data-name="room4">
            <div class="toggle">
                <span class="label label-success pull-right room-notify"></span>
                <h3>اقتصاد</h3>
            </div>
            <div class="small-chat-box fadeInRight animated">
    
                <div class="heading" draggable="true">
                    <small class="chat-date pull-right">
                        <i class="fa fa-comments"></i>
                    </small>
                    اقتصاد
                </div>
    
                <div class="content chat-log">
    
                </div>
                <div class="form-chat">
                    <textarea rows="2" class="form-control chat-message" name="cmessage" placeholder="{!!Lang::get('chatbox::chatbox.write_message')!!}"></textarea>
                </div>
    
            </div>
        </div>-->
    <div id="hidden-chat">
        <div class="toggle">
            <i class="fa fa-comments"></i> <span class="counter"></span>
        </div>

        <ul class="list-group clear-list m-t" id="chat-hide" >
        </ul>
    </div>
</div>

<script type="text/javascript" charset="utf-8">
    var username = "{!! Auth::user()->username !!}";
    var userId = "{!! Auth::user()->id !!}";
</script>

<script src="https://cdn.socket.io/socket.io-1.3.4.js"></script>
<script src="<?php echo assets() ?>/javascripts/socketConfig.js"></script>
