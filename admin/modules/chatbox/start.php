<?php

View::composer('*', function($view) {
    if (Auth::check()) {
        $this->data['chatGroups'] = \Auth::user();
        $view->with($this->data);
    }
});

include __DIR__ . "/routes.php";
include __DIR__ . "/helpers.php";
