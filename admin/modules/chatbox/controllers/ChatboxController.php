<?php

class ChatboxController extends BackendController {

    public function __construct() {
        //$this->middleware('guest');
    }

    public function index() {
        return View::make('mailbox::mailbox');
    }
    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id) {
        
    }
    
    public function systemMessage() {
        $redis = LRedis::connection();        
        $redis->publish('chat.message', json_encode([
            'msg' => 'System message',
            'nickname' => 'System',
            'system' => true,
        ]));
    }

}
