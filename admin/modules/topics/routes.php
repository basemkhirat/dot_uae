<?php

Route::group(array(
    "prefix" => ADMIN,
    "before" => "auth",
        ), function($route) {
    $route->group(array("prefix" => "topics"), function($route) {
        $route->get('search', array('as' => ADMIN . '.topics.search', 'uses' => 'TopicController@search'));
        $route->post('del/', array('as' => ADMIN . '.topics.delete', 'uses' => 'TopicController@delete'));
        $route->get('select2-search', array('uses' => 'TopicController@select2Search'));
        $route->get('posts/del/{topicId}/{postId}', 'TopicController@deletePost');
    });
    Route::resource('topics', 'TopicController');
});
