<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PollsController
 *
 * @author Khaled PC
 */
class TopicController extends BackendController {

    var $ipp = 10;
    var $data = [];

    public function __construct() {

        $this->beforeFilter('csrf', [
            'on' => [
                'post',
                'put'
            ]
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index() {
        $topics = Topic::orderBy('created_at', 'desc');
        if (Input::get('q')) {
            $q = Input::get('q');
            $topics->where('name', 'LIKE', '%' . $q . '%');
        }
        $this->data['topics'] = $topics->paginate(10);
        return View::make('topics::create', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create() {
        return Redirect::route(ADMIN . '.topics.index');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store() {
        $data = Input::all();
        $validator = Validator::make($data, array('name' => 'required'));
        if ($validator->fails()) {
            return Redirect::route(ADMIN . '.topics.index')->withErrors($validator);
        }
        $topicName = $data['name'];
        $exsists = Topic::where('name', '=', $topicName)->count();
//        dd($exsists);
        if ($exsists > 0) {
            if (Request::ajax()) {
                return json_encode('found');
            } else {
                return Redirect::route(ADMIN . '.topics.index')->withFlashMessage(array(
                            'type' => 'info',
                            'text' => 'الموضوع موجود مسبقا'
                ));
            }
        }

        $canonical_url = (substr($data['canonical_url'], 0, 4) != 'http' && $data['canonical_url'] != '') ? "http://" . $data['canonical_url'] : $data['canonical_url'];
        $topic = new Topic();
        $topic->name = $data['name'];
        $topic->description = (isset($data['description'])) ? $data['description'] : '';
        $topic->image_id = (isset($data['image_id'])) ? $data['image_id'] : '';
        $topic->meta_title = (isset($data['meta_title'])) ? $data['meta_title'] : '';
        $topic->meta_description = (isset($data['meta_description'])) ? $data['meta_description'] : '';
        $topic->canonical_url = $canonical_url;
        $topic->robots_index = (isset($data['image_id'])) ? $data['robots_index'] : '';
        $topic->in_sitemap = (isset($data['in_sitemap'])) ? $data['in_sitemap'] : '';

        $topic->save();
        if (Request::ajax()) {
            return json_encode($topic);
        } else {
            return Redirect::route(ADMIN . '.topics.index')->withFlashMessage(array(
                        'text' => trans('topics::topics.create_message'),
                        'type' => 'success'
            ));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id) {
        $this->data['topic'] = Topic::findOrFail($id);
        return View::make('topics::topic', $this->data);
    }

    /**
     * Show the form for editing the specified resource.
     * 
     * @param  int  $id
     * @return Response
     */
    public function edit($slug) {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id) {
        $topic = Topic::find($id);
        $data = Input::all();
//        dd($data);
        $canonical_url = (substr($data['canonical_url'], 0, 4) != 'http' && $data['canonical_url'] != '') ? "http://" . $data['canonical_url'] : $data['canonical_url'];
        $topic->name = $data['name'];
        $topic->description = $data['description'];
        $topic->image_id = $data['image_id'];
        $topic->meta_title = (isset($data['meta_title'])) ? $data['meta_title'] : '';
        $topic->meta_description = (isset($data['meta_description'])) ? $data['meta_description'] : '';
        $topic->canonical_url = $canonical_url;
        $topic->robots_index = (isset($data['image_id'])) ? $data['robots_index'] : '';
        $topic->in_sitemap = (isset($data['in_sitemap'])) ? $data['in_sitemap'] : '';

        $topic->save();
        return Redirect::route(ADMIN . '.topics.index')->with('flash_message', array('text' => trans('topics::topics.updated'), 'type' => 'success'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id) {
        dd($id);
    }

    public function delete() {
        $deletedTopic = Topic::find(Input::get('deleted'));
        $moveToTopic = Input::get('move_to') ? Topic::find(Input::get('move_to')) : null;
        if ($moveToTopic) {
            $moveToTopic->posts()->attach($deletedTopic->posts->lists('post_id'));
            $numMoved = $deletedTopic->posts->count();
        } else {
            $numMoved = 0;
        }

        $deletedTopic->delete();
        return Redirect::route(ADMIN . '.topics.index')
                        ->withFlashMessage(array(
                            'text' => trans('topics::topics.deleted', array('num' => $numMoved)),
                            'type' => 'success'
        ));
    }

    public function search() {
        $keyword = Input::get('q');
        $topics = Topic::where('name', 'LIKE', '%' . $keyword . '%')->get();
        if (!$topics->isEmpty()) {
            return json_encode($topics);
        } else {
            return json_encode('notfound');
        }
    }

    public function select2Search() {
        if (Input::get('idStr')) {
            $ids = explode(',', Input::get('idStr'));
            $topics = Topic::whereIn('id', $ids)->get();
        } else {
            $keyword = Input::get('q');
            $topics = Topic::where('name', 'LIKE', '%' . $keyword . '%')
                            ->limit(5)->get();
        }
        $return = array();
        foreach ($topics as $topic) {
            $current = array();
            $current['id'] = $topic->id;
            $current['image'] = $topic->image_url;
            $current['name'] = $topic->name;
            $return['items'][] = $current;
        }
        return $return;
    }

    public function deletePost($topicId, $postId) {
        $topic = Topic::find($topicId);
        $topic->posts()->detach($postId);
        return Redirect::to(ADMIN . '/topics/' . $topic->id)
                        ->with('flash_message', array('type' => 'success', 'text' => trans('topics::topics.post_deleted')));
    }

}
