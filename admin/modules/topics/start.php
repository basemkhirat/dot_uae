<?php

Menu::set("sidebar", function($menu) {
        $menu->item('news_options.topics', trans("admin::common.topics"), URL::to(ADMIN . '/topics'))
                ->icon("fa-th-list")->order(2);
});

include __DIR__ ."/routes.php";
