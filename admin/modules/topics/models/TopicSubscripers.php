<?php



/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of aaaa
 *
 * @author khalid-pc
 */
class TopicSubscripers extends Model {

    protected $table = 'topics_subscripers';
    public $fillable = array(
        'email',
        'topic_id'
    );

    public function topic() {
        return $this->hasOne('Topic', 'id', 'topic_id');
    }

}
