<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Topic
 *
 * @author Khaled PC
 */
class Topic extends Model {

    protected $tableName = 'topics';

    public function posts() {
        return $this->belongsToMany('Post', 'posts_topics')->withTimestamps();
    }

    public function image() {
        return $this->hasOne('Media', 'media_id', 'image_id');
    }

    public function getImageUrlAttribute() {
        return ($this->image) ? $this->image->getImageUrl() : assets("images/default.png");
    }

}
