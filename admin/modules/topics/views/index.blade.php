@extends("admin::layouts.master")

@section("content")

<div id="content-wrapper">

    <div class="page-header">
        <h1><span class="text-light-gray">{!!trans('topics::topics.all')}} </span>{!!trans('topics::topics.topics')}}</h1>
        <span style="float: left;"><a href="{!!route('admin.topics.create')}}" class="btn btn-primary">{!!trans('topics::topics.add_new')}}</a></span>
    </div> <!-- / .page-header -->

    <div class="row">

    </div>

</div>
@stop