@extends("admin::layouts.master")


@section("breadcrumb")
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2><i class="fa fa-th-list faa-tada animated faa-slow"></i> {!!trans('topics::topics.topics')!!} ({!!@$topics->total()!!})</h2>
        <ol class="breadcrumb" style="background: none;">
            <li><a  href="{!!URL::to('/'.ADMIN.'/dashboard')!!}">{!!Lang::get('admin::common.dashboard')!!}</a></li>
            <li>{!!trans('topics::topics.all')!!} {!!trans('topics::topics.topics')!!}</li>

        </ol>
    </div>

</div>
@stop
@section("content")

<style>

    #all_topics_delete_chosen{
        width:100% !important;
    }
</style>

<div id="content-wrapper">

    @if(Session::get('flash_message'))
    <div class="alert alert-{!!Session::get('flash_message')['type']!!} alert-dark">
        <button type="button" class="close" data-dismiss="alert">×</button>
        {!!Session::get('flash_message')['text']!!}
    </div>
    @endif

    <div class="row">

        <div class="col-sm-6">
            <div class="ibox float-e-margins">
                {!! Form::open([
                'route' => [ADMIN . '.topics.store'],
                'method' => 'POST',
                'id' => 'topic_form',
                'class' => 'panel form-horizontal',
                ]) !!}
                <div class="ibox-title">
                    <h5> {!!Lang::get('topics::topics.new_topic')!!} </h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>

                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <div class="row form-group">
                        <label class="col-sm-2">{!!trans('topics::topics.name')!!}</label>
                        <div class="col-sm-10">
                            <input type="text" name="name" class="form-control"/>
                        </div>
                    </div>

                    <div class="row form-group">
                        <label class="col-sm-2">{!!trans('topics::topics.description')!!}</label>
                        <div class="col-sm-10">
                            <textarea class="form-control" name="description"></textarea>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-md-2" >
                            <input type="hidden" id="cat_photo_id" name="photo" />

                            <a href="#" id="" class="change_photo" rel="0" data-append-to="topic_form" data-preview="topic_photo_preview_create">{!!trans('topics::topics.set_image')!!}</a>
                        </div>
                        <div class="col-sm-10">
                            <img id="topic_photo_preview_create" style="border: 1px solid #ccc; width: 150px; height: 150px" src="{!!assets("images/default.png")!!}" />
                        </div>
                    </div>
                    <h1 class="col-lg-12" style="margin-top:0;border-top: 1px solid #E7EAEC;padding-top: 10px;">{!!trans('tags::tags.seo_options')!!}</h1>
                    <div class="panel-body">

                        <div class="form-horizontal" style="padding-top:15px">
                            <div class="tab-pane fade active in " id="general-tab">

                                <div class="form-group tooltip-demo">
                                    <label for="meta_title" class="col-sm-2" style="padding-top: 7px;">{!!Lang::get('tags::tags.meta_title')!!}</label>
                                    <div class="col-sm-10">
                                        {!! Form::text("meta_title", '', array('class' => 'form-control input-lg', 'placeholder' => Lang::get('tags::tags.meta_title'))) !!}
                                        <div style="font-size: 14px; margin-top: 2px">
                                            {!!trans('tags::tags.meta_subtitle')!!} <span id="metadesc-length"></span>.
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group tooltip-demo">
                                    <label for="meta_description" class="col-sm-2" style="padding-top: 7px;">{!!Lang::get('tags::tags.meta_description')!!}</label>
                                    <div class="col-sm-10">
                                        {!! Form::textarea("meta_description", '', ['class' => 'form-control', 'rows' => 2, 'style' => 'resize: vertical;',  'placeholder' => Lang::get('tags::tags.meta_description')]) !!}
                                        <div style="font-size: 14px; margin-top: 2px">
                                            {!!trans('tags::tags.meta_descrip_subtitle')!!} <span id="metadesc-length"></span>.
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="canonical_url" class="col-sm-2" style="padding-top: 7px;">{!!Lang::get('tags::tags.canonical_url')!!}</label>
                                    <div class="col-sm-10">
                                        {!! Form::text("canonical_url", '', array('class' => 'form-control input-lg', 'placeholder' => Lang::get('tags::tags.canonical_url'))) !!}
                                        <div style="font-size: 14px; margin-top: 2px">
                                            {!!trans('tags::tags.canonical_url_info')!!}
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="robots_index" class="col-sm-2" style="padding-top: 7px;">{!!Lang::get('tags::tags.meta_robots_index')!!}</label>
                                    <div class="col-sm-10">
                                        <select name="robots_index" class="form-control" style="width:auto; display: inline-block;">
                                            <option value="0">{!!Lang::get('tags::tags.robots_index_default')!!}</option>
                                            <option value="2">{!!Lang::get('tags::tags.index')!!}</option>
                                            <option value="1">{!!Lang::get('tags::tags.noindex')!!}</option>
                                        </select>
                                        <div style="font-size: 14px; margin-top: 2px">
                                            {!!trans('tags::tags.robots_info')!!} <span id="metadesc-length"></span>.
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group" style="margin-top:20px">
                                    <label for="in_sitemap" class="col-sm-2" style="padding-top: 7px;">{!!Lang::get('tags::tags.include_in_sitemap')!!}</label>
                                    <div class="col-sm-10">
                                        <select name="in_sitemap" class="form-control" style="width:auto; display: inline-block;">
                                            <option value="0">{!!Lang::get('tags::tags.auto_detect')!!}</option>
                                            <option value="1" >{!!Lang::get('tags::tags.always_include')!!}</option>
                                            <option value="2" >{!!Lang::get('tags::tags.never_include')!!}</option>
                                        </select>
                                        <div style="font-size: 14px; margin-top: 2px">
                                            {!!trans('tags::tags.sitemap_info')!!}
                                        </div>
                                    </div>
                                </div>
                            </div> <!-- / .tab-pane -->

                        </div> <!-- / .tab-content -->

                    </div>
                    <div class="panel-footer text-right" style="border-top: 1px solid #ececec; background: none;">
                        <input id="quick-new-topic-btn" type="submit" value="{!! trans('topics::topics.save') !!}" class="btn btn-primary"/>
                    </div>


                </div>
                {!! Form::close() !!}
            </div>
        </div>
        <div class="col-lg-6">
            {!! Form::open(array('method' => 'get', 'class' => 'choosen-rtl', 'id' => 'search', 'choosen-rtl')) !!}

            <div class="input-group">
                <input name="q" id="q" value="{!!(Input::get('q')) ? Input::get('q') : ''!!}" type="text" placeholder="بحث" class="input-sm form-control" > <span class="input-group-btn">
                    <button type="submit" class="btn btn-sm btn-primary"> {!!Lang::get('pages::pages.search')!!}</button> </span>
            </div>

            {!! Form::close() !!}
        </div>
        <div class="col-sm-6">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5> {!!trans('topics::topics.all')!!} {!!trans('topics::topics.topics')!!} </h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>

                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <div class="table-responsive">


                        <table cellspacing="0" cellpadding="0" border="0" class="table table-striped">
                            <thead>
                                <tr>
                                    <th><?php echo trans('topics::topics.name') ?></th>
                                    <th><?php echo trans('topics::topics.image') ?></th>
                                    <th><?php echo trans('topics::topics.description') ?></th>
                                    <th><?php echo trans('topics::topics.posts_num') ?></th>
                                    <!--<th><?php echo trans('topics::topics.view') ?></th> -->
                                    <th><?php echo trans('topics::topics.edit') ?></th>
                                    <th><?php echo trans('topics::topics.delete') ?></th>
                                    <!--<th>RSS</th> -->
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($topics as $topic)

                                <tr>
                                    <td><a href="{!!URL::route(ADMIN . '.topics.show',$topic->id)!!}">{!!$topic->name!!}</a></td>
                                    <td><img style="border: 1px solid #ccc; width: 71px; height: 70px" src="{!!$topic->image_url!!}"/></td>
                                    <td>{!!$topic->description!!}</td>
                                    <td>{!!$topic->posts->count()!!}</td>
                                    <!--
                                    <td class="tooltip-demo m-t-md">
                                        <a class="btn btn-white btn-sm"  target="_blank" href="{!!URL::to(ADMIN.'/topic/'.$topic->id)!!}">
                                            <i class="fa fa-external-link"></i>
                                        </a>
                                    </td>
                                    -->
                                    <td class="tooltip-demo m-t-md">
                                        <a class="btn btn-white btn-sm"  rel="" data-toggle="modal" data-target="#modal_{!!$topic->id!!}">
                                            <i class="fa fa-pencil"></i>
                                        </a>
                                    </td>
                                    <td class="tooltip-demo m-t-md">

                                        <a href="#" class="delete_topic btn btn-white btn-sm" data-toggle="modal" data-target="#modal_delete" data-id="{!!$topic->id!!}">
                                            <i class="fa fa-times"></i>
                                        </a>
                                    </td>

                                </tr>

                                @endforeach
                            </tbody>
                        </table>
                        <div class="panel-footer text-right" style="border-top: 1px solid #ececec; background: none;">
                            <div class="col-sm-2 m-b-xs" style="padding:0">

                            </div>

                            <div class="col-sm-10 m-b-xs" style="padding:0">
                                <div class="pull-right">
                                    {!!$topics->appends(Input::all())->setPath('')->render()!!}
                                </div>
                            </div>
                        </div>
                        <!--LINKS-->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> <!-- / #content-wrapper -->

@foreach($topics as $topic)
<div id="modal_{!!$topic->id!!}" class="modal fade" tabindex="-1" role="dialog" style="display: none;">

    {!! Form::open(array(
    'route'=> array(ADMIN . '.topics.update',$topic->id),
    'class'=> "panel form-horizontal",
    'method'=> "put",
    'style' => 'background: none;',
    'id'=> 'topic_form_edit_' . $topic->id
    )) !!}
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="margin-top: -10px;"><i class="fa fa-times"></i></button>
                <h4 class="modal-title">{!!trans('topics::topics.edit')!!}</h4>
            </div>
            <div class="modal-body" id="edit_topic_lightbox_{!!$topic->id!!}">
                <!--form-->


                <div class="panel-body">
                    <div class="row form-group">
                        <label class="col-sm-3">{!!trans('topics::topics.name')!!}</label>
                        <div class="col-sm-9">
                            <input type="text" name="name" class="form-control" value="{!!$topic->name!!}"/>
                        </div>
                    </div>

                    <div class="row form-group">
                        <label class="col-sm-3">{!!trans('topics::topics.description')!!}</label>
                        <div class="col-sm-9">
                            <textarea class="form-control" name="description">{!!$topic->description!!}</textarea>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-md-3" >
                            <input type="hidden" class="featured_image" name="image_id" value="{!!$topic->image_id!!}">
                            <a href="#" class="change_photo" rel="0" data-append-to="topic_form_edit_{!!$topic->id!!}" data-preview="topic_photo_preview_edit_{!!$topic->id!!}">{!!trans('topics::topics.change_image')!!}</a>
                        </div>
                        <div class="col-sm-9">
                            <img id="topic_photo_preview_edit_{!!$topic->id!!}" style="border: 1px solid #ccc; width: 150px; height: 150px" src="{!!$topic->image_url!!}">
                        </div>
                    </div>
                    <h1 class="col-lg-12" style="margin-top:0;border-top: 1px solid #E7EAEC;padding-top: 10px;">{!!trans('tags::tags.seo_options')!!}</h1>
                    <div class="panel-body">

                        <div class="form-horizontal" style="padding-top:15px">
                            <div class="tab-pane fade active in " id="general-tab">

                                <div class="form-group tooltip-demo">
                                    <label for="meta_title" class="col-sm-2" style="padding-top: 7px;">{!!Lang::get('tags::tags.meta_title')!!}</label>
                                    <div class="col-sm-10">
                                        {!! Form::text("meta_title", string_sanitize(Input::old('meta_title', @$topic->meta_title)), array('class' => 'form-control input-lg', 'placeholder' => Lang::get('tags::tags.meta_title'))) !!}
                                        <div style="font-size: 14px; margin-top: 2px">
                                            {!!trans('tags::tags.meta_subtitle')!!} <span id="metadesc-length"></span>.
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group tooltip-demo">
                                    <label for="meta_description" class="col-sm-2" style="padding-top: 7px;">{!!Lang::get('tags::tags.meta_description')!!}</label>
                                    <div class="col-sm-10">
                                        {!! Form::textarea("meta_description", string_sanitize(Input::old('meta_description', @$topic->meta_description)), ['class' => 'form-control', 'rows' => 2, 'style' => 'resize: vertical;',  'placeholder' => Lang::get('tags::tags.meta_description')]) !!}
                                        <div style="font-size: 14px; margin-top: 2px">
                                            {!!trans('tags::tags.meta_descrip_subtitle')!!} <span id="metadesc-length"></span>.
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="canonical_url" class="col-sm-2" style="padding-top: 7px;">{!!Lang::get('tags::tags.canonical_url')!!}</label>
                                    <div class="col-sm-10">
                                        {!! Form::text("canonical_url", string_sanitize(Input::old('canonical_url', @$topic->canonical_url)), array('class' => 'form-control input-lg', 'placeholder' => Lang::get('tags::tags.canonical_url'))) !!}
                                        <div style="font-size: 14px; margin-top: 2px">
                                            {!!trans('tags::tags.canonical_url_info')!!}
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="robots_index" class="col-sm-2" style="padding-top: 7px;">{!!Lang::get('tags::tags.meta_robots_index')!!}</label>
                                    <div class="col-sm-10">
                                        <select name="robots_index" class="form-control" style="width:auto; display: inline-block;">
                                            <option value="0">{!!Lang::get('tags::tags.robots_index_default')!!}</option>
                                            <option value="2" @if(@$topic->robots_index == 2) selected @endif>{!!Lang::get('tags::tags.index')!!}</option>
                                            <option value="1" @if(@$topic->robots_index == 1) selected @endif>{!!Lang::get('tags::tags.noindex')!!}</option>
                                        </select>
                                        <div style="font-size: 14px; margin-top: 2px">
                                            {!!trans('tags::tags.robots_info')!!} <span id="metadesc-length"></span>.
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group" style="margin-top:20px">
                                    <label for="in_sitemap" class="col-sm-2" style="padding-top: 7px;">{!!Lang::get('tags::tags.include_in_sitemap')!!}</label>
                                    <div class="col-sm-10">
                                        <select name="in_sitemap" class="form-control" style="width:auto; display: inline-block;">
                                            <option value="0">{!!Lang::get('tags::tags.auto_detect')!!}</option>
                                            <option value="1" @if(@$topic->in_sitemap == 1) selected @endif>{!!Lang::get('tags::tags.always_include')!!}</option>
                                            <option value="2" @if(@$topic->in_sitemap == 2) selected @endif>{!!Lang::get('tags::tags.never_include')!!}</option>
                                        </select>
                                        <div style="font-size: 14px; margin-top: 2px">
                                            {!!trans('tags::tags.sitemap_info')!!}
                                        </div>
                                    </div>
                                </div>
                            </div> <!-- / .tab-pane -->

                        </div> <!-- / .tab-content -->

                    </div>
                    <div class="panel-footer text-right" style="border-top: 1px solid #ececec; background: none;">
                        <input id="quick-new-topic-btn" type="submit" value="{!! trans('topics::topics.save') !!}" class="btn btn-primary"/>
                    </div>
                </div>

                <!--form-->

            </div> <!-- / .modal-content -->
        </div> <!-- / .modal-dialog -->
    </div> <!-- / .modal -->
    {!! Form::close() !!}

</div> <!-- / .modal -->
@endforeach

<div id="modal_delete" class="modal fade" role="dialog" style="display: none;">
    <div class="modal-dialog modal-bg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="margin-top: -10px;"><i class="fa fa-times"></i></button>
            </div>
            {!! Form::open(array(
            'route' => array(ADMIN . '.topics.delete'),
            'method' => 'POST',
            'id'=>'delete_modal_form'
            )) !!}
            <!--<form action="{!!route(ADMIN . '.topics.destroy')!!}">-->
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6">
                        <label>نقل أخبار الموضوع الى</label>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group" style="width: 100%">
                            <select id="all_topics_delete" class="form-control chosen-select chosen-rtl" name="move_to" style="width: 150px">
                                <option value="0">عدم النقل</option>
                                @foreach(Topic::get() as $dbTopic)
                                <option value="{!!$dbTopic->id!!}">{!!$dbTopic->name!!}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
            </div> <!-- / .modal-content -->
            {!! Form::close() !!}
            <div class="modal-footer">
                <button class="btn btn-primary" id="delete_ok">{!! trans('topics::topics.ok') !!}</button>
                <a class="btn btn-primary" data-dismiss="modal">{!! trans('topics::topics.cancel') !!}</a>
            </div>
            <input id="deleted_id" type="hidden" name="deleted" value=""/>
            </form>
        </div> <!-- / .modal-dialog -->
    </div> <!-- / .modal -->
</div> <!-- / .modal -->

<script src="<?php echo assets() ?>/js/plugins/validate/jquery.validate.min.js"></script>
<script>
    $(document).ready(function () {
        $(".change_photo").each(function () {
            var self = $(this);
            self.filemanager({
                types: "png|jpg|jpeg|gif|bmp",
                done: function (files, initiator) {
                    console.log(self.attr('data-preview'));
                    if (files.length) {
                        $('.featured_image').remove();
                        var file = files[0];
                        $("#" + self.attr('data-preview')).attr("src", file.media_thumbnail);
                        $('<input>', {
                            name: 'image_id',
                            type: 'hidden',
                            class: 'featured_image',
                            value: file.media_id
                        }).appendTo($('#' + self.attr('data-append-to')));
                        console.log(self.attr('data-append-to'));
                    }
                },
                error: function (media_path) {
                    alert(media_path + " is not an image");
                }
            });
        });
        //        $(".change_photo").filemanager({
        //            types: "png|jpg|jpeg|gif|bmp",
        //            done: function(files, initiator) {
        //                console.log(initiator.attr('data-preview'));
        //                if (files.length) {
        //                    $('.featured_image').remove();
        //                    var file = files[0];
        //                    $("#" + initiator.attr('data-preview')).attr("src", file.media_thumbnail);
        //                    $('<input>', {
        //                        name: 'image_id',
        //                        type: 'hidden',
        //                        class: 'featured_image',
        //                        value: file.media_id
        //                    }).appendTo($('#' + initiator.attr('data-append-to')));
        //                    console.log(initiator.attr('data-append-to'));
        //                }
        //            },
        //            error: function(media_path) {
        //                alert(media_path + " is not an image");
        //            }
        //        });

        $('.delete_topic').click(function (event) {
            console.log('d');
            var self = $(this);
            var id = self.attr('data-id');
            $('#all_topics_delete option').prop('disabled', false);
            $('#all_topics_delete option[value=' + id + ']').prop('disabled', true);
            $('#deleted_id').val(id);
        });
//        $('#all_topics_delete').select2({
//        });

        // jQuery plugin to prevent double submission of forms
        jQuery.fn.preventDoubleSubmission = function () {
            $(this).on('submit', function (e) {
                var $form = $(this);

                if ($form.data('submitted') === true) {
                    // Previously submitted - don't submit again
                    e.preventDefault();
                } else {
                    // Mark it so that the next submit can be ignored
                    $form.data('submitted', true);
                }
            });

            // Keep chainability
            return this;
        };
        $('#delete_modal_form').preventDoubleSubmission();

        $('#topic_form').validate({
            ignore: [],
            rules: {
                name: {
                    required: true
                },
            }
        });

        @foreach($topics as $topic)
                $('#topic_form_edit_{!!$topic->id!!}').validate({
        ignore: [],
                rules: {
                name: {
                required: true
                },
                }
        });
                @endforeach

                jQuery.extend(jQuery.validator.messages, {
                    required: "{!!Lang::get('posts::posts.required')!!}",
                });

        var config = {
            '.chosen-select': {},
            '.chosen-select-deselect': {
                allow_single_deselect: true
            },
            '.chosen-select-no-single': {
                disable_search_threshold: 10
            },
            '.chosen-select-no-results': {
                no_results_text: "{!!Lang::get('posts::posts.notfound')!!}"
            },
            '.chosen-select-width': {
                width: "95%"
            }
        }
        for (var selector in config) {
            $(selector).chosen(config[selector]);
        }

        // search submit
        $("#search").submit(function (e) {
            if ($("#q").val() == '') {
                return false
            }
        });
    });
</script>
@stop
