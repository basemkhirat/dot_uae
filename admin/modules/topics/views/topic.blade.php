@extends("admin::layouts.master")

@section("breadcrumb")
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2><i class="fa fa-th-list faa-tada animated faa-slow"></i>  {!!trans('topics::topics.posts')!!}</h2>
        <ol class="breadcrumb" style="background: none;">
            <li><a  href="{!!URL::to('/'.ADMIN.'/dashboard')!!}">{!!Lang::get('admin::common.dashboard')!!}</a></li>
            <li><a  href="{!!URL::to('/'.ADMIN.'/topics')!!}">{!!Lang::get('topics::topics.topics')!!}</a></li>
            <li>{!!$topic->name!!}</li>

        </ol>
    </div>

</div>
@stop
@section("content")
<div id="content-wrapper">

    @if(Session::get('flash_message'))
    <div class="alert alert-{!!Session::get('flash_message')['type']!!} alert-dark">
        <button type="button" class="close" data-dismiss="alert">×</button>
        {!!Session::get('flash_message')['text']!!}
    </div>
    @endif

    <form action="{!!URL::to(ADMIN . '/topics/posts/del')!!}" method="get">
        <!--        <div class="form-group" style="position:relative;">
        
                    <select name="action" class="form-control" style="width:auto; display: inline-block;">
                        <option value="">{!!Lang::get('topics::topics.action')!!}</option>
                        <option value="most">{!!Lang::get('topics::topics.delete')!!}</option>
                    </select>
        
                    <button class="btn" style="margin-top:-5px;" type="submit" >{!!Lang::get('topics::topics.go')!!}</button>
        
                </div>-->
        <div class="ibox float-e-margins">
            <!-- Default panel contents -->
            <div class="ibox-title">
                <h5> {!!Lang::get('topics::topics.posts')!!} </h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>

                    <a class="close-link">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
            </div>
            <div class="ibox-content">
                <!-- Table -->
                <div class="table-responsive">


                    <table cellspacing="0" cellpadding="0" border="0" class="table table-striped">

                        <tbody>
                            @forelse($topic->posts as $post)
                            <tr>
                                <td>
                                    <div class="action-checkbox">
                                        <label class="px-single">
                                            <input type="checkbox" name="check[]" value="{!!$post->post_id!!}" class="px"><span class="lbl"></span>
                                        </label>
                                    </div>
                                </td>
                                <td>{!!$post->title!!}</td>
                                <td><a href="{!!URL::to(ADMIN . '/topics/posts/del/' . $topic->id . '/' . $post->post_id)!!}" class="ask" message="{!!trans('topics::topics.confirm_delete')!!}">{!!trans('topics::topics.delete')!!}</a></td>
                            </tr>
                            @empty
                            <tr class="odd gradeX" style="height:200px">
                                <td colspan="8" style="vertical-align:middle; text-align:center; font-weight:bold; font-size:22px">{!!trans('topics::topics.empty')!!}</td>
                            </tr>
                            @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </form>
</div> <!-- / #content-wrapper -->
@stop