<li class="dropdown">
    <a href="#" class="dropdown-toggle user-menu" data-toggle="dropdown">
        <img src="<?php echo assets("images/flags/" . Config::get("conn") . ".png") ?>">
        <span>{{Lang::get('posts::posts.'.Config::get("conn"))}}</span>
    </a>
    <ul class="dropdown-menu">
        @foreach(Config::get('sites') as $key => $site)
        @if(Config::get("conn") != $key)
        <li><a href="{{switch_lang($key)}}"><img src="<?php echo assets("images/flags/" . $key . ".png") ?>">&nbsp;<span>{{Lang::get('admin::common.'.$site)}}</span></a></li>
        <!--<li class="divider"></li>-->
        @endif
        @endforeach

    </ul>
</li>