@extends("grabber::layouts.grabber")

@section("breadcrumb")
<div class="row wrapper border-bottom white-bg page-heading" style="height: 65px;">
    <div class="col-lg-10">

        <?php
        $title = "أخبار إنستاجرام";
        $link = "";
        if (Input::has("page")) {
            $page = Input::get("page");

            switch ($page) {
                case "sources":
                case "sources/create":
                    $title = '<i class="fa fa-newspaper-o faa-tada animated faa-slow"></i> &nbsp;' . "مصادر الأخبار";
                    $link = URL::to(ADMIN . "/grabber?page=sources/create");
                    break;
                case "posts/search/0":
                    $title = '<i class="fa fa-globe faa-tada animated faa-slow"></i> &nbsp;' . "أخبار المواقع";
                    break;
                case "posts/search/1":
                    $title = '<i class="fa fa-facebook-official faa-tada animated faa-slow"></i> &nbsp;' . "أخبار فيس بوك";
                    break;
                case "posts/search/2":
                    $title = '<i class="fa fa-twitter-square faa-tada animated faa-slow"></i> &nbsp;' . "أخبار تويتر";
                    break;
                case "posts/search/3":
                    $title = '<i class="fa fa-youtube faa-tada animated faa-slow"></i> &nbsp;' . "أخبار يوتيوب";
                    break;
                case "posts/search/4":
                    $title = '<i class="fa fa-instagram faa-tada animated faa-slow"></i> &nbsp;' . "أخبار إنستاجرام";
                    break;
                case "cats":
                    $title = '<i class="fa fa-th-large faa-tada animated faa-slow"></i> &nbsp;' . "التصنيفات";

                    break;
                case "places":
                    $title = '<i class="fa fa-globe faa-tada animated faa-slow"></i> &nbsp;' . "البلدان";
                    break;
            }
        }
        ?>


        <h2><?php echo $title; ?></h2>
    </div>
    <?php if ($link != "") { ?>
        <div class="col-lg-2">

            <a href="<?php echo $link; ?>" style="margin-top: 15px;" class="btn btn-primary btn-labeled btn-main"> <span class="btn-label icon fa fa-plus"></span> &nbsp; أضف جديد</a>

        </div>
    <?php } ?>
</div>

@stop

@section("content")

<style>


    #content-wrap {
        height: 85%;
        position: absolute;
        width: 100%;
    } 

    iframe {
        border: 0 none;
        height: 100%;
        overflow: hidden !important;
        width: 100%;
    }


    iframe html body .content{
        margin-right:0px !important;
    }

    iframe html body .menu.scroller{
        display: none !important;
    }

</style>

<div class="row" id="content-wrap">
    <iframe src="<?php echo Config::get("grabber") ?>/admin/<?php echo Input::get("page"); ?>"></iframe>
</div>


@stop