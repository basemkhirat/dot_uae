@include("admin::partials.header_part")
@include("media::manager") 
<div id="wrapper">
    @include("admin::partials.sidebar")
    @include("admin::partials.topnav")
    @yield("breadcrumb")
    @yield("content")
    @include("admin::partials.footer")
</div>
@include("admin::partials.footer_part")