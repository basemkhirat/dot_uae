<?php

Route::get(ADMIN, function() {
    return Redirect::to(ADMIN . "/dashboard");
});

Route::group(array(
    "prefix" => ADMIN,
    "before" => "auth",
        ), function($route) {
    $route->get('grabber', function() {
        return View::make("grabber::grabber");
    });
});

