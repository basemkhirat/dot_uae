<?php

Menu::set('sidebar', function($menu) {
    
    $menu->item('options.sources', trans("grabber::grabber.sources_settings"), URL::to(ADMIN . '/grabber?page=sources'))->icon("fa-newspaper-o");
    
    /*
    $menu->item('grabber', trans("grabber::grabber.module"), URL::route('dashboard.show'))->order(1)->icon('fa-newspaper-o');
    
    $menu->item('grabber.website_news', trans("grabber::grabber.website_news"), URL::to(ADMIN . '/grabber?page=posts/search/0'));
    $menu->item('grabber.facebook_news', trans("grabber::grabber.facebook_news"), URL::to(ADMIN . '/grabber?page=posts/search/1'));
    $menu->item('grabber.twitter_news', trans("grabber::grabber.twitter_news"), URL::to(ADMIN . '/grabber?page=posts/search/2'));
    $menu->item('grabber.youtube_news', trans("grabber::grabber.youtube_news"), URL::to(ADMIN . '/grabber?page=posts/search/3'));
    $menu->item('grabber.instagram_news', trans("grabber::grabber.instagram_news"), URL::to(ADMIN . '/grabber?page=posts/search/4'));
    */
});


// country selector

Menu::set("topnav", function($menu) {
    $menu->make("grabber::dropmenu");
});


require __DIR__ . "/routes.php";
