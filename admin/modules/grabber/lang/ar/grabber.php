<?php

return [
    "module" => "المصادر",
    "sources" => "كل الأخبار",
    "sources_settings" => "إعدادات المصادر",
    "website_news" => "أخبار المواقع",
    "facebook_news" => "أخبار الفيسبوك",
    "twitter_news" => "أخبار تويتر",
    "youtube_news" => "أخبار يوتيوب",
    "instagram_news" => "أخبار إنستاجرام",
];
