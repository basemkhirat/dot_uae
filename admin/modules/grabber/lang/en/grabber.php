<?php

return [
    "module" => "Sources",
    "sources" => "Sources",
     "sources_settings" => "Sources settings",
    "website_news" => "Website news",
    "facebook_news" => "Facebook news",
    "twitter_news" => "Twitter news",
    "youtube_news" => "Youtube news",
    "instagram_news" => "Instagram news",
];
