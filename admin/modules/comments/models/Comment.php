<?php

class Comment extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'comments';
    protected $primaryKey = 'comment_id';
    protected $fillable = ['comment_author', 'comment_content', 'comment_author_email', 'comment_post_id', 'comment_status', 'comment_user_id'];
    protected $guarded = ['comment_id'];
    public $timestamps = true;

    const CREATED_AT = 'comment_date';

    public function user() {
        return $this->hasOne('User', 'id', 'comment_user_id');
    }

    public function post() {
        return $this->belongsTo('Post', 'comment_post_id', 'post_id');
    }

    public function setUpdatedAt($value) {
        // Do nothing.
    }

    public function getUpdatedAtColumn() {
        return null;
    }

    public function scopefilter($query, $args = []) {

        if (@$args['post_id']) {
            $query->where('comment_post_id', $args['post_id']);
        }

        if (@$args['comment_status'])
            $query->where('comment_status', $args['comment_status']);
        else {
            $query->whereNotIn('comment_status', array(3, 4));
        }

        if (@$args['q'])
            $query->where('comment_content', 'LIKE', '%' . $args['q'] . '%');

        return $query;
    }

}