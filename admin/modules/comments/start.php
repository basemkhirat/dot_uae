<?php

Menu::set("sidebar", function($menu) {
    if (User::can('comments')) {
        $menu->item('news_options.comments', trans("admin::common.comments"), URL::to(ADMIN . '/comments'))
                ->order(5)
                ->icon("fa-comments");
    }
});

include __DIR__ . "/routes.php";
