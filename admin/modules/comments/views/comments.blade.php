@extends("admin::layouts.master")


<?php
$params = '';
foreach (Input::except('per_page', 'page') as $key => $value) {
    $params .= '&' . $key . '=' . $value;
}
?>

<style type="text/css">
    .selected{
        color:#555;
    }
</style>

@section("breadcrumb")
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2><i class="fa fa-comments faa-tada animated faa-slow"></i> {!!Lang::get('comments::comments.comments')!!} ({!!$comments->total()!!})</h2>
        <ol class="breadcrumb" style="background: none;">
            <li><a  href="{!!URL::to('/'.ADMIN.'/dashboard')!!}">{!!Lang::get('admin::common.dashboard')!!}</a></li>
            <li>{!!Lang::get('comments::comments.comments')!!}</li>
        </ol>
    </div>

</div>
@stop
@section("content")

<div id="content-wrapper">

    <div class="row">

        <div class="col-lg-8">
            <ol class="breadcrumb" style="background: none;">
                <li ><a @if(! Input::has('comment_status')) style="color:#4A85BC;" @endif href="{!!URL::to(ADMIN.'/comments')!!}">{!!Lang::get('comments::comments.all')!!} <span>({!!$comments_count!!})</span></a></li> 
                <li ><a @if( Input::get('comment_status') == 1) style="color:#4A85BC;" @endif href="{!!URL::to(ADMIN.'/comments'.'?comment_status=1')!!}">{!!Lang::get('comments::comments.approved')!!} <span>({!!$comments_approved!!})</span></a></li> 
                <li ><a @if( Input::get('comment_status') == 2) style="color:#4A85BC;" @endif href="{!!URL::to(ADMIN.'/comments'.'?comment_status=2')!!}">{!!Lang::get('comments::comments.pending')!!} <span>({!!$comments_pending!!})</span></a></li> 
                <li ><a @if( Input::get('comment_status') == 3) style="color:#4A85BC;" @endif href="{!!URL::to(ADMIN.'/comments'.'?comment_status=3')!!}">{!!Lang::get('comments::comments.spam')!!} <span>({!!$comments_spam!!})</span></a></li> 

                <li ><a @if( Input::get('comment_status') == 5) style="color:#4A85BC;" @endif href="{!!URL::to(ADMIN.'/comments'.'?comment_status=5')!!}">{!!Lang::get('comments::comments.blocked')!!} <span>({!!$comments_blocked!!})</span></a></li> 
                <li ><a @if( Input::get('comment_status') == 4) style="color:#4A85BC;" @endif href="{!!URL::to(ADMIN.'/comments'.'?comment_status=4')!!}">{!!Lang::get('comments::comments.trashed')!!} <span>({!!$comments_trash!!})</span></a></li>
            </ol>

        </div>
        <div class="col-lg-4">
            {!! Form::open(array('url' => ADMIN.'/comments', 'id' => 'search', 'method' => 'get', 'class' => 'choosen-rtl')) !!}

            <div class="input-group">
                <input type="text" placeholder="{!!Lang::get('comments::comments.search')!!}" class="input-sm form-control" value="{!!(Input::get('q')) ? Input::get('q') : ''!!}" name="q" id="q"> <span class="input-group-btn">
                    <button type="submit" class="btn btn-sm btn-primary"> {!!Lang::get('comments::comments.search')!!}</button> </span>
            </div>
            @if(Input::has('comment_status'))
            {!! Form::hidden('comment_status', Input::get('comment_status')) !!}
            @endif
            {!! Form::close() !!}
        </div>
    </div>

    {!! Form::open(array('url' => ADMIN.'/comments/status', 'method' => 'POST', 'id' => 'form')) !!}
    <div class="row wrapper animated fadeInRight ">



        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5> {!!Lang::get('comments::comments.comments')!!} </h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>

                    <a class="close-link">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
            </div>
            <div class="ibox-content">
                <div class="row">
                    <div class="col-sm-10 m-b-xs">
                        <select name="action" id="action" class="form-control m-b" style="width:auto; display: inline-block;">
                            <option value="" selected="selected">{!!Lang::get('comments::comments.bulk')!!}</option>
                            @if(Input::get('comment_status') == 4)
                            @if(User::can('comments.delete'))
                            <option value="4">{!!Lang::get('comments::comments.delete')!!}</option>
                            @endif
                            @if(User::can('comments.restore'))
                            <option value="2">{!!Lang::get('comments::comments.restore')!!}</option>
                            @endif
                            @if(User::can('comments.spam'))
                            <option value="3">{!!Lang::get('comments::comments.mark_spam')!!}</option>
                            @endif
                            @elseif(Input::get('comment_status') == 3)
                            @if(User::can('comments.delete'))
                            <option value="0">{!!Lang::get('comments::comments.delete')!!}</option>
                            @endif
                            @if(User::can('comments.notspam'))
                            <option value="2">{!!Lang::get('comments::comments.notspam')!!}</option>
                            @endif
                            @elseif(Input::get('comment_status') == 2)
                            @if(User::can('comments.approve'))
                            <option value="1">{!!Lang::get('comments::comments.approve')!!}</option>
                            @endif
                            @if(User::can('comments.spam'))
                            <option value="3">{!!Lang::get('comments::comments.mark_spam')!!}</option>
                            @endif
                            @if(User::can('comments.trash'))
                            <option value="4">{!!Lang::get('comments::comments.move_trash')!!}</option>
                            @endif
                            @if(User::can('comments.block'))
                            <option value="5">{!!Lang::get('comments::comments.block')!!}</option>
                            @endif
                            @elseif(Input::get('comment_status') == 1)
                            @if(User::can('comments.unapprove'))
                            <option value="2">{!!Lang::get('comments::comments.unapprove')!!}</option>
                            @endif
                            @if(User::can('comments.spam'))
                            <option value="3">{!!Lang::get('comments::comments.mark_spam')!!}</option>
                            @endif
                            @if(User::can('comments.trash'))
                            <option value="4">{!!Lang::get('comments::comments.move_trash')!!}</option>
                            @endif
                            @if(User::can('comments.block'))
                            <option value="5">{!!Lang::get('comments::comments.block')!!}</option>
                            @endif
                            @elseif(Input::get('comment_status') == 5)
                            @if(User::can('comments.approve'))
                            <option value="5">{!!Lang::get('comments::comments.approve')!!}</option>
                            @endif	
                            @if(User::can('comments.unapprove'))
                            <option value="2">{!!Lang::get('comments::comments.unapprove')!!}</option>
                            @endif
                            @if(User::can('comments.spam'))
                            <option value="3">{!!Lang::get('comments::comments.mark_spam')!!}</option>
                            @endif
                            @if(User::can('comments.trash'))
                            <option value="4">{!!Lang::get('comments::comments.move_trash')!!}</option>
                            @endif

                            @else
                            @if(User::can('comments.approve'))
                            <option value="1">{!!Lang::get('comments::comments.approve')!!}</option>
                            @endif
                            @if(User::can('comments.unapprove'))
                            <option value="2">{!!Lang::get('comments::comments.unapprove')!!}</option>
                            @endif
                            @if(User::can('comments.spam'))
                            <option value="3">{!!Lang::get('comments::comments.mark_spam')!!}</option>
                            @endif   
                            @if(User::can('comments.trash'))                        
                            <option value="4">{!!Lang::get('comments::comments.move_trash')!!}</option>
                            @endif
                            @if(User::can('comments.block'))
                            <option value="5">{!!Lang::get('comments::comments.block')!!}</option>
                            @endif
                            @endif
                        </select>

                        <button class="btn btn-primary">{!!Lang::get('comments::comments.apply')!!}</button>

                    </div>

                    <div class="col-sm-2">

                        <select class="form-control chosen-rtl" id="per_page" name="per_page">
                            <option selected="selected" value="">-- {!!Lang::get('comments::comments.per_page')!!} --</option>
                            <option selected="selected" value="10">10</option>
                            <option value="20">20</option>
                            <option value="30">30</option>
                            <option value="40">40</option>
                        </select>
                    </div>
                </div>
                <div class="table-responsive">

                    <table cellspacing="0" cellpadding="0" border="0" class="table table-striped">
                        <thead>
                            <tr>
                                <th><div class="action-checkbox"><label class="px-single"><input type="checkbox" name="checkall" id="checkall" value="" class="i-checks"><span class="lbl"></span></label></div></th>
                        <th style="min-width:150px">{!!Lang::get('comments::comments.author')!!}</th>
                        <th>{!!Lang::get('comments::comments.comment_')!!}</th>
                        <th>{!!Lang::get('comments::comments.response_to')!!}</th>
                        <th style="min-width:175px">{!!Lang::get('comments::comments.edit')!!}</th>
                        </tr>
                        </thead>
                        <tbody >
                            <?php $count = count($comments);?>
                            @if($count)
                            @foreach($comments as $comment)
                            <tr class="odd gradeX">
                                <td><div class="action-checkbox"><label class="px-single"><input type="checkbox" name="check[]" value="{!!$comment->comment_id!!}" class="i-checks"><span class="lbl"></span></label></div></td>
                                <td>{!!$comment->comment_author!!}</td>
                                <td>
                                    @if(Input::get('comment_status') == 3 || Input::get('comment_status') == 4 || !User::can('comments.edit'))
                                    {!!$comment->comment_content!!}
                                    @else
                                    <a href="{!!URL::to(ADMIN.'/comments/'.$comment->comment_id.'/edit')!!}"> {!!$comment->comment_content!!}</a>
                                    @endif

                                    <div style="font-size:11px; margin-top:5px">{!!Lang::get('comments::comments.submitted_on')!!} @if(DIRECTION == 'rtl') {!!arabic_date($comment->comment_date)!!} @else {!! date('M d, Y', strtotime($comment->comment_date)) !!} @ {!! date('h:i a', strtotime($comment->comment_date)) !!} @endif</div>
                                </td>


                                <td class="center">
                                    <div style="margin-bottom:10px; font-weight:bold"><a href="{!!URL::to(ADMIN.'/posts/'.$comment->post->post_id.'/edit')!!}"> {!!$comment->post->post_title!!}</a></div>

                                </td>
                                <td class="tooltip-demo m-t-md">
                                    @if(Input::get('comment_status') == 4)
                                        @if(User::can('comments.restore'))
                                        <a data-toggle="tooltip" data-placement="top" href="{!!URL::to(ADMIN.'/comments/status/'.$comment->comment_id.'/2')!!}" title="{!!Lang::get('comments::comments.restore')!!}" class="restore_comment btn btn-white btn-sm"><i class="fa fa-reply"></i></a>
                                        @endif
                                        @if(User::can('comments.delete'))
                                        <a data-toggle="tooltip" data-placement="top" href="{!!URL::to(ADMIN.'/comments/'.$comment->comment_id.'/delete')!!}" class="delete_comment btn btn-white btn-sm" message="{!!Lang::get('comments::comments.delete_comment_q')!!}" title="{!!Lang::get('comments::comments.delete')!!}"><i class="fa fa-trash-o"></i></a>
                                        @endif
                                    @else
                                        @if(User::can('comments.trash'))
                                        <a data-toggle="tooltip" data-placement="top" href="{!!URL::to(ADMIN.'/comments/status/'.$comment->comment_id.'/4')!!}" class="trash_comment btn btn-white btn-sm" message="{!!Lang::get('comments::comments.trash_comment_q')!!}" title="{!!Lang::get('comments::comments.trash')!!}"><i class="fa fa-trash-o"></i></a>
                                        @endif
                                    @endif

                                    @if(Input::get('comment_status') != 3 && $comment->comment_status != 3)
                                        @if(User::can('comments.spam'))
                                        <a data-toggle="tooltip" data-placement="top" href="{!!URL::to(ADMIN.'/comments/status/'.$comment->comment_id.'/3')!!}" class="btn btn-white btn-sm" title="{!!Lang::get('comments::comments.spam')!!}"><i class="fa fa-eye"></i></a>
                                        @endif
                                    @else
                                        @if(User::can('comments.notspam'))
                                        <a data-toggle="tooltip" data-placement="top" href="{!!URL::to(ADMIN.'/comments/status/'.$comment->comment_id.'/2')!!}" title="{!!Lang::get('comments::comments.notspam')!!}" class="btn btn-white btn-sm"><i class="fa fa-comment-o"></i></a>
                                        @endif
                                    @endif
                                    
                                    @if(Input::get('comment_status') != 1 && $comment->comment_status != 1 && Input::get('comment_status') != 4)
                                        @if(User::can('comments.approve'))
                                        <a data-toggle="tooltip" data-placement="top" href="{!!URL::to(ADMIN.'/comments/status/'.$comment->comment_id.'/1')!!}" title="{!!Lang::get('comments::comments.approve')!!}" class="btn btn-white btn-sm approve_comment"><i class="fa fa-thumbs-o-up"></i></a>
                                        @endif
                                    @endif
                                    
                                    @if(Input::get('comment_status') != 5 && $comment->comment_status != 5)
                                    @if(User::can('comments.block'))
                                    <a data-toggle="tooltip" data-placement="top" href="{!!URL::to(ADMIN.'/comments/status/'.$comment->comment_id.'/5')!!}" class="block_comment btn btn-white btn-sm" title="{!!Lang::get('comments::comments.block')!!}"><i class="fa fa-ban"></i></a>
                                    @endif
                                    @endif
                                    
                                    @if(Input::get('comment_status') != 2 && $comment->comment_status != 2 && $comment->comment_status == 1)
                                        @if(User::can('comments.unapprove'))
                                        <a data-toggle="tooltip" data-placement="top" href="{!!URL::to(ADMIN.'/comments/status/'.$comment->comment_id.'/2')!!}" class="unapprove_comment btn btn-white btn-sm" title="{!!Lang::get('comments::comments.unapprove')!!}"><i class="fa fa-thumbs-o-down"></i></a>
                                        @endif
                                    @endif
                                    
                                    @if(User::can('comments.edit') && Input::get('comment_status') != 4)
                                    <a data-toggle="tooltip" data-placement="top" href="{!!URL::to(ADMIN.'/comments/'.$comment->comment_id.'/edit')!!}" title="{!!Lang::get('comments::comments.edit')!!}" class="btn btn-white btn-sm"><i class="fa fa-pencil"></i></a>
                                    @endif

                                </td>
                            </tr>
                            @endforeach
                            @else
                            <tr class="odd gradeX" style="height:200px">
                                <td colspan="5" style="vertical-align:middle; text-align:center; font-weight:bold; font-size:22px">{!!Lang::get('comments::comments.no_comments')!!}</td>
                            </tr>
                            @endif
                        </tbody>
                    </table>
                    <div class="panel-footer text-right" style="border-top: 1px solid #ececec; background: none;">
                        <div class="col-sm-2 m-b-xs" style="padding:0">
                            @if($count)
                            {!!Lang::get('comments::comments.showing')!!} {!!$comments->firstItem()!!} {!!Lang::get('comments::comments.to')!!} {!!$comments->lastItem()!!} {!!Lang::get('comments::comments.of')!!} {!!$comments->total()!!} {!!Lang::get('comments::comments.comment')!!}
                            @endif
                        </div>

                        <div class="col-sm-10 m-b-xs" style="padding:0">
                            <div class="pull-right">
                                @if($count)
                                {!!$comments->appends(Input::all())->setPath('')->render()!!}
                                @endif
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

    </div>
</div>

{!! Form::close() !!}


<script src="<?php echo assets() ?>/js/bootbox.min.js"></script>

<script type="text/javascript">

    $(document).ready(function () {

        $('#per_page').change(function () {
            location.href = '{!!URL::to(ADMIN."/comments")!!}' + '?per_page=' + $(this).val() + '{!!$params!!}'

        });

        $('#per_page').val("{!!Input::get('per_page')!!}");

        // check all action
        $('#checkall').on('ifChecked', function (event) {
            $("input[name='check[]']").each(function () {
                $(this).iCheck('check');
            });
        });
        $('#checkall').on('ifUnchecked', function (event) {
            $("input[name='check[]']").each(function () {
                $(this).iCheck('uncheck');
            });
        });
        $('.i-checks').iCheck({
            checkboxClass: 'icheckbox_square-green',
        });
        $("#form").submit(function () {

            if ($("input[name='check[]']:checked").length == 0 || $("#action").val() == '') {
                return false;
            }

        });

        $('.trash_comment, .delete_comment').on('click', function (e) {
            e.preventDefault();
            $this = $(this);
            bootbox.dialog({
                message: $this.attr('message'),
                title: $this.attr('title'),
                buttons: {
                    success: {
                        label: "{!!Lang::get('comments::pages.ok')!!}",
                        className: "btn-success",
                        callback: function () {
                            location.href = $this.attr('href');
                        }
                    },
                    danger: {
                        label: "{!!Lang::get('comments::pages.cancel')!!}",
                        className: "btn-primary",
                        callback: function () {
                        }
                    },
                },
                className: "bootbox-sm"
            });
        });

        // search submit
        $("#search").submit(function (e) {
            if ($("#q").val() == '') {
                return false
            }
        });
    });
</script>

@stop
