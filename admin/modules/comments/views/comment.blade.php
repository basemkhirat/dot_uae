@extends("admin::layouts.master")

@section("breadcrumb")
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2><i class="fa fa-comments faa-tada animated faa-slow"></i> {!!Lang::get('comments::comments.comments')!!} {!!Lang::get('comments::comments.comments')!!}</h2>
        <ol class="breadcrumb" style="background: none;">
            <li><a  href="{!!URL::to('/'.ADMIN.'/dashboard')!!}">{!!Lang::get('admin::common.dashboard')!!}</a></li>
            <li><a  href="{!!URL::to('/'.ADMIN.'/comments')!!}">{!!Lang::get('comments::comments.comments')!!}</a></li>
            <li>{!!Lang::get('comments::comments.edit_comment')!!} </li>
        </ol>
    </div>

</div>
@stop
@section("content")

@if ($errors->has())

<div class="alert alert-danger alert-dark">
    <button type="button" class="close" data-dismiss="alert">×</button>
    @foreach ($errors->all() as $error)
    <div><strong>{!!Lang::get('comments::comments.oh_snap')!!}</strong> {!! $error !!}</div>
    @endforeach
</div>
@endif

@if(Session::has('message'))

<div class="alert alert-success alert-dark">
    <button type="button" class="close" data-dismiss="alert">×</button>
    <strong>{!!Lang::get('comments::comments.well_done')!!}</strong> {!!Lang::get('comments::comments.success_update')!!}
</div>

@endif

<div id="content-wrapper">

    <div class="row">

        <div class="row wrapper animated fadeInRight ">

            {!! Form::open(array('url' => ADMIN.'/comments/'.$comment->comment_id, 'class' => 'form-horizontal', 'method' => 'put', 'files' => true, 'id' => 'form', 'novalidate' => 'novalidate')) !!}    

            <div class="col-md-8">
                <div class="ibox float-e-margins">

                    <div class="ibox-content">
                        <div class="form-group"><label class="col-sm-2 control-label">{!!Lang::get('comments::comments.name')!!}</label>

                            <div class="col-sm-10">
                                {!! Form::text('author_name', Input::old('author_name', $comment->comment_author), array('class' => 'form-control input-lg', 'id' => 'author_name', 'placeholder' => Lang::get('comments::comments.name'))) !!}

                            </div>
                        </div>

                        <div class="hr-line-dashed"></div>
                        <div class="form-group"><label class="col-sm-2 control-label">{!!Lang::get('comments::comments.email')!!}</label>

                            <div class="col-sm-10">
                                {!! Form::text('author_email', Input::old('author_email', $comment->comment_author_email), array('class' => 'form-control input-lg', 'id' => 'author_email', 'placeholder' => Lang::get('comments::comments.email'))) !!}

                            </div>
                        </div>

                        <div class="hr-line-dashed"></div>
                        <div class="form-group"><label class="col-sm-2 control-label">{!!Lang::get('comments::comments.content')!!}</label>

                            <div class="col-sm-10">
                                {!! Form::textarea('comment_content',  Input::old('comment_content', $comment->comment_content) , ['class' => 'form-control', 'rows' => 4, 'id' => 'comment_content']) !!}

                            </div>
                        </div>

                        <div class="hr-line-dashed"></div>
                    </div>
                </div>

            </div>

            <div class="col-md-4">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>{!!Lang::get('comments::comments.status')!!}</h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>

                            <a class="close-link">
                                <i class="fa fa-times"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content">
                        <div class="panel-body">

                            <div class="form-group">

                                @if(User::can('comments.approve'))
                                <div class="radio" style="margin-top: 0;">
                                    <label>
                                        <input @if($comment->comment_status == 1) checked="checked" @endif class="i-checks" type="radio" name="comment_status" value="1">
                                                &nbsp;&nbsp;<span class="lbl" style="color:green;">{!!Lang::get('comments::comments.s_approved')!!}</span>
                                    </label>
                                </div>
                                @endif

                                @if(User::can('comments.unapprove'))
                                <div class="radio" style="margin-top: 0;">
                                    <label>
                                        <input @if($comment->comment_status == 2) checked="checked" @endif class="i-checks" type="radio" name="comment_status" value="2">
                                                &nbsp;&nbsp;<span class="lbl" style="color:#f4b04f;">{!!Lang::get('comments::comments.pending')!!}</span>
                                    </label>
                                </div>
                                @endif

                                @if(User::can('comments.spam'))
                                <div class="radio" style="margin-top: 0;">
                                    <label>
                                        <input @if($comment->comment_status == 3) checked="checked" @endif class="i-checks" type="radio" name="comment_status" value="3" >
                                                &nbsp;&nbsp;<span class="lbl" style="color:red">{!!Lang::get('comments::comments.spam')!!}</span>
                                    </label>
                                </div>
                                @endif

                                @if(User::can('comments.block'))
                                <div class="radio" style="margin-top: 0;">
                                    <label>
                                        <input @if($comment->comment_status == 5) checked="checked" @endif class="i-checks" type="radio" name="comment_status" value="5" >
                                                &nbsp;&nbsp;<span class="lbl" style="color:red">{!!Lang::get('comments::comments.blocked')!!}</span>
                                    </label>
                                </div>
                                @endif
                            </div>

                            @if(isset($comment->comment_id))
                            <div class="form-group">    
                                <i class="fa fa-calendar"></i>&nbsp;&nbsp;&nbsp;&nbsp;<small class="text-muted">{!!Lang::get('comments::comments.submitted_on')!!}: &nbsp;&nbsp;</small><b> @if(DIRECTION == 'rtl') {!!arabic_date($comment->comment_date)!!} @else {!! date('M d, Y', strtotime($comment->comment_date)) !!} @ {!! date('h:i a', strtotime($comment->comment_date)) !!} @endif</b>
                            </div>
                            @endif
                        </div>

                        <div class="panel-footer" style="border-top: 1px solid #ececec; background: none;">

                            <div class="form-group" style="margin-bottom:0">

                                @if(User::can('comments.trash'))
                                @if(isset($comment->comment_id))

                                <a class="submitdelete deletion pull-left" href="{!! URL::to('/'.ADMIN.'/comments/' . $comment->comment_id . '/trash') !!}">{!!Lang::get('comments::comments.move_trash')!!}</a>

                                @endif
                                @endif
                                {!! Form::button(Lang::get('comments::comments.publish'), array('type' => 'submit', 'class' => 'pull-right btn btn-primary')) !!}

                            </div>

                        </div>
                    </div>
                </div>
            </div>
            {!! Form::close() !!}

        </div>
    </div>
</div>

<script src="<?php echo assets() ?>/js/plugins/validate/jquery.validate.min.js"></script>
<script type="text/javascript" src="<?php echo assets("ckeditor/ckeditor.js"); ?>"></script>

<script type="text/javascript">

CKEDITOR.replace("comment_content", {
    language: '<?php echo LANG; ?>'}
);
$(document).ready(function () {
    $('.i-checks').iCheck({
        checkboxClass: 'icheckbox_square-green',
        radioClass: 'iradio_square-green',
    });
        
    function strip_tags(input, allowed) {
        allowed = (((allowed || '') + '')
            .toLowerCase()
            .match(/<[a-z][a-z0-9]*>/g) || [])
            .join(''); // making sure the allowed arg is a string containing only tags in lowercase (<a><b><c>)                 var tags = /<\/?([a-z][a-z0-9]*)\b[^>]*>/gi,
            commentsAndPhpTags = /<!--[\s\S]*?-->|<\?(?:php)?[\s\S]*?\?>/gi;
            return input.replace(commentsAndPhpTags, '')
            .replace(tags, function ($0, $1) {
            return allowed.indexOf('<' + $1.toLowerCase() + '>') > - 1 ? $0 : '';
            });
    }

    $("#form").validate({
        ignore: [],
        rules: {
            author_name: {
                required: true
            },
            author_email: {
                required: true,
                email: true
            },
            comment_content: {
                required: function () {
                    CKEDITOR.instances.comment_content.updateElement();
                }

            },
            //            messages;{
            //                title: {
            //                    required: 'Required'
            //                }
            //            }
        }
    });
});

jQuery.extend(jQuery.validator.messages, {
    required: "{!!Lang::get('comments::comments.required')!!}",
    email: "{!!Lang::get('comments::comments.invalid')!!}",
});

</script>

@stop