<?php

Route::group(array(
    "prefix" => ADMIN,
    "before" => "auth",
        ), function($route) {
    Route::resource('comments', 'CommentController');
    $route->group(array("prefix" => "comments"), function($route) {
        $route->get('status/{id}/{status}', 'CommentController@changeStatus');
        $route->post('status', 'CommentController@changeStatus');
        $route->get('{id}/delete', 'CommentController@destroy');
    });
});
