<?php

class CommentController extends BackendController {

    public $data = array();
    public $conn = '';

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function __construct() {
        date_default_timezone_set("Etc/GMT-2");

        $this->conn = Session::get('conn');
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index() {

        $comment_status = Input::get('comment_status');
        $post_id = Input::get('post_id');
        $q = trim(Input::get('q'));
        $per_page = (Input::has('per_page')) ? Input::get('per_page') : 20;

        $this->data['comments_count'] = Comment::filter(['q' => $q, 'post_id' => $post_id])->count();
        $this->data['comments_approved'] = Comment::filter(['comment_status' => 1, 'q' => $q, 'post_id' => $post_id])->count();
        $this->data['comments_pending'] = Comment::filter(['comment_status' => 2, 'q' => $q, 'post_id' => $post_id])->count();
        $this->data['comments_spam'] = Comment::filter(['comment_status' => 3, 'q' => $q, 'post_id' => $post_id])->count();
        $this->data['comments_trash'] = Comment::filter(['comment_status' => 4, 'q' => $q, 'post_id' => $post_id])->count();
        $this->data['comments_blocked'] = Comment::filter(['comment_status' => 5, 'q' => $q, 'post_id' => $post_id])->count();

        $this->data['comments'] = Comment::filter(['comment_status' => $comment_status, 'q' => $q, 'post_id' => $post_id])->paginate($per_page);

        return View::make('comments::comments')->with($this->data);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function ajax_add_comment() {

        $comment_content = Input::get('comment_content');
        $post_id = Input::get('post_id');
        $user_id = Auth::user()->id;
        $user_name = Auth::user()->first_name . ' ' . Auth::user()->last_name;
        $user_email = Auth::user()->email;
        $comment_status = 1;

        $comment = new Comment(['comment_content' => $comment_content, 'comment_post_id' => $post_id,
            'comment_user_id' => $user_id, 'comment_author' => $user_name, 'comment_author_email' => $user_email, 'comment_status' => $comment_status]);
        $comment->save();

        $id = $comment->comment_id;

        if (DIRECTION == 'rtl') {
            $style = 'margin-right: 0;';
            $date = arabic_date(date("Y-m-d H:i:s"));
        } else {
            $style = 'margin-left: 0;';
            $date = date('M d, Y') . ' @ ' . date('h:i a');
        }

        $output = '<div class="feed-element">
                            <div class="media-body ">
                                <small class="pull-right">
                                    <a href="' . URL::to(ADMIN . '/users/' . $user_id) . '" title="">' . $user_name . '</a> - <a href="mailto:' . $user_email . '" target="_top">' . $user_email . '</a>
                                </small>
                                <strong>' . $comment_content . '</strong><br>
                                <small class="text-muted pull-right">' . $date . '</small>
                                <div class="pull-left tooltip-demo m-t-md">';

        if (User::can('comments.approve')) {
            $output .= '<a data-type="accept" data-toggle="tooltip" data-placement="top" href="#' . $id . '" title="' . Lang::get('comments::comments.approve') . '" style="display:none" class="accept_comment btn btn-white btn-sm"><i class="fa fa-thumbs-up" ></i></a> ';
        }

        if (User::can('comments.unapprove')) {
            $output .= '<a data-toggle="tooltip" data-placement="top" href="#' . $id . '"  title="' . Lang::get('comments::comments.unapprove') . '" class="refuse_comment btn btn-white btn-sm"><i class="fa fa-thumbs-down" ></i></a> ';
        }

        if (User::can('comments.edit')) {
            $output .= '<a data-toggle="tooltip" data-placement="top" href="' . URL::to(ADMIN . '/comments/' . $id . '/edit') . '" class="btn btn-white btn-sm" title="' . Lang::get('comments::comments.edit') . '"><i class="fa fa-pencil"></i></a> ';
        }

        if (User::can('comments.block')) {
            $output .= '<a data-type="block" data-toggle="tooltip" data-placement="top" href="#' . $id . '" class="block_comment btn btn-white btn-sm" title="' . Lang::get('comments::comments.block') . '"><i class="fa fa-minus-circle"></i></a> ';
        }

        if (User::can('comments.trash')) {
            $output .= '<a data-type="trash" data-toggle="tooltip" data-placement="top" href="#' . $id . '" class="trash_comment btn btn-white btn-sm" title="' . Lang::get('comments::comments.trash') . '"><i class="fa fa-trash-o"></i></a> ';
        }

        if (User::can('comments.spam')) {
            $output .= '<a data-type="spam" data-toggle="tooltip" data-placement="top" href="#' . $id . '" class="spam_comment btn btn-white btn-sm" title="' . Lang::get('comments::comments.spam') . '"><i class="fa fa-eye"></i></a> ';
        }

        $output .= '</div>
                            </div>
                        </div>';

        echo $output;

        exit;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create() {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store() {
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id) {

        $comment = Comment::find($id);

        if (!$comment) {
            return Redirect::to(ADMIN . '/comments');
        }

        $this->data['comment'] = $comment;

        return View::make('comments::comment')->with($this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id) {

        $rules = array(
            'author_name' => 'required',
            'author_email' => 'required|email',
            'comment_content' => 'required',
        );

        // run the validation rules on the inputs from the form
        $validator = Validator::make(Input::all(), $rules);

        $validator->setCustomMessages(Lang::get('admin::validation'));

        // if the validator fails, redirect back to the form
        if ($validator->fails()) {
            return Redirect::back()
                            ->withErrors($validator) // send back all errors to the login form
                            ->withInput(); // send back the input (not the password) so that we can repopulate the form
        } else {
            $data = Input::all();
            $data['comment_author'] = Input::get('author_name');
            $data['comment_author_email'] = Input::get('author_email');

            // update page
            $comment = Comment::find($id);
            $comment->fill($data);
            $comment->save();
        }
        Session::flash('message', 'success');
        return Redirect::back();
    }

    /**
     * Trash the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function changeStatus($id = 0, $status = 0) {

        if (Request::ajax()) {
            $id = Input::get('comment_id');
            $status = Input::get('status');

            $comment = Comment::find($id);
            $comment->comment_status = $status;
            $comment->save();

            echo json_encode($status);
        } else {
            $checks = Input::get('check');
            $stat = Input::get('action');

            if ($id) {
                $comment = Comment::find($id);
                $comment->comment_status = $status;
                $comment->save();
                return Redirect::to(ADMIN . '/comments');
            } else {
                if ($stat == '0') {
                    Comment::destroy($checks);
                } else {
                    $comments = Comment::whereIn('comment_id', $checks)->get();

                    foreach ($comments as $key => $comment) {
                        $comment->comment_status = $stat;
                        $comment->save();
                    }
                }
                return Redirect::back();
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id) {
        Comment::destroy($id);
        return Redirect::back();
    }

}
