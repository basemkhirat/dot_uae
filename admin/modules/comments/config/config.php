<?php

return [
    "permissions" => [
        "add",
        "delete",
        "edit",
        "approve",
        "unapprove",
        "trash",
        "spam",
        "restore",
        "notspam",
        "block"
    ]
];
