<?php

class NavigationsController extends BackendController {

    var $ipp = 10;
    var $loadedIpp = 20;
    var $data = [];
    public $conn = '';

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function __construct() {
        date_default_timezone_set("Etc/GMT-2");
        $this->conn = Session::get('conn');
        $this->beforeFilter('csrf', [
            'on' => [
                'post',
                'put'
            ]
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index() {
        if (!User::can('navigation.manage')) {
            return 'denied';
        }
        
        $navs = Navigation::whereSite($this->conn)->whereMenu('0');

        if (Input::has('search')) {

            $navs->where('name', 'LIKE', '%' . trim(Input::get('search')) . '%');
        }
        $this->data['navigations'] = $navs->take(20)->paginate();
        return View::make('navigations::index', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create() {
        if (!User::can('navigation.manage')) {
            return 'denied';
        }
        return View::make('navigations::create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store() {
        $data = Input::except('conn');
        
        $validator = \Validator::make($data, Navigation::$rules);
        if ($validator->fails()) {
            return Redirect::route(ADMIN . '.navigations.create')->withInput(\Input::all())->withErrors($validator);
        } else {
            $navigation = new Navigation($data);
            $navigation->site = $this->conn;
            $navigation->save();
            return Redirect::to(route(ADMIN . '.navigations.edit', ['id' => $navigation->id]) . '?conn=' . $this->conn);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id) {
        if (!User::can('navigation.manage')) {
            return 'denied';
        }
        
        $navigation = Navigation::whereSite($this->conn)->with('menuItems')->whereMenu('0')->find($id);
        if (!$navigation) {
            App::abort(404);
        }
        $this->data['navigation'] = $navigation;
        return View::make('navigations::edit', $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id) {
        
        $data = Input::all();
        $navigation = Navigation::find($id);
        if (!$navigation) {
            App::abort(404);
        }
        $navigation->name = $data['menu_name'];
        $navigation->save();
        return Redirect::route(ADMIN . '.navigations.edit', array($navigation->id))->with('flash_message', array('text' => 'Updated', 'type' => 'success'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id) {
        //
    }

    public function loadItems($type, $offset = 0) {
        $lang = Lang::getLocale();
        switch ($type) {
            case 'posts':
                $posts = DB::table('posts')->where('posts.post_status', '=', 1)->take($this->loadedIpp)->skip($offset)
                                ->whereSite($this->conn)->get();
                return View::make('navigations::partials.loadedPosts', array('posts' => $posts));
            case 'categories':
                $categories = DB::table('categories')->take($this->loadedIpp)->skip($offset)
                                ->whereSite($this->conn)->get();
                return View::make('navigations::partials.loadedCategories', ['categories' => $categories]);
            case'pages':
                $pages = DB::table('pages')->where('pages.page_status', '=', 1)->take($this->loadedIpp)->skip($offset)->get();
                return View::make('navigations::partials.loadedPages', ['pages' => $pages]);
        }
    }

    public function search($q) {
        $lang = Lang::getLocale();
        $searchFor = Input::get('searchFor');
        switch ($searchFor) {
            case 'categories':
                $categories = DB::table('categories')
                                ->whereSite($this->conn)
                                ->where('categories.cat_name', 'LIKE', '%' . $q . '%')->get();
                return View::make('navigations::partials.loadedCategories', ['categories' => $categories]);
            case 'posts':
                $posts = DB::table('posts')->where('posts.post_status', '=', 1)
                                ->whereSite($this->conn)
                                ->where('posts.post_title', 'LIKE', '%' . $q . '%')->get();
                return View::make('navigations::partials.loadedPosts', array('posts' => $posts));
            case'pages':
                $pages = DB::table('pages')->where('pages.page_status', '=', 1)
                                ->where('pages.page_title', 'LIKE', '%' . $q . '%')->get();
                return View::make('navigations::partials.loadedPages', ['pages' => $pages]);
        }
    }

    public function reOrder() {
        $ordered = Input::get('order');
        $navigationId = Input::get('navigationId');
        $deletedItems = (Input::get('deletedItems')) ? Input::get('deletedItems') : FALSE;
        if ($deletedItems) {
            $idsArray = array();
            foreach ($deletedItems as $item) {
                $idsArray[] = $item['value'];
            }
            Navigation::whereIn('id', $idsArray)->delete();
        }
        foreach ($ordered as $key => $order) {
            $item = Navigation::find($order['itemId']);
            if ($item == NULL) {
                $item = new Navigation();
                $item->site = $this->conn;
                $item->type = $order['type'];
                $item->name = $order['name'];
                if (isset($order['value'])) {
                    $item->link = $order['value'];
                } else {
                    $item->link = '#';
                }
            }
            $item->menu = $navigationId;
            $item->order = $key;
            $item->parent = $order['parent'];
            if (isset($order['name'])) {
                $item->name = $order['name'];
            }
            $item->save();
        }
    }

    private function indexOrdered($order) {
        $returnData = [];
        foreach ($order as $key => $item) {
            dd($item);
            $item['order'] = $key + 1;
            $returnData[$item['itemId']] = $item;
        }
        return $returnData;
    }

}
