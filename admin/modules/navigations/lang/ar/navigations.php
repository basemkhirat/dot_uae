<?php

return [
    'navigation' => 'قائمة',
    'navigations' => 'قوائم',
    'all' => 'كل',
    'add_new' => 'أضافة جديد',
    'create' => 'انشاء',
    'menu_name' => 'الاسم',
    'edit' => 'تحرير',
    'items' => 'عناصر',
    'menu_data' => 'معلومات القائمة',
    'new_navigation' => 'قائمة جديدة',
    'save' => 'حفظ',
    'posts' => 'مقالات',
    'pages' => 'صفحات',
    'categories' => 'تصنيفات',
    'url' => 'رابط',
    'add' => 'اضافة',
    'text' => 'النص',
    'save_order' => 'حفظ الترتيب',
    'search' => 'بحث ...',
    'filter' => 'تصفية',
    'updated' => 'تم التحديث بنجاح',
    
    "module" => "القوائم",
    "permissions" => [
        'manage' => "التحكم بالقوائم"
    ]
];
