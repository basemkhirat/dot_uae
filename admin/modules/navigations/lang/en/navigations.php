<?php

return [
    'navigation' => 'Navigation',
    'navigations' => 'Navigations',
    'all' => 'All',
    'add_new' => 'Add New',
    'create' => 'Create',
    'menu_name' => 'Menu Name',
    'edit' => 'Edit',
    'items' => 'Items',
    'menu_data' => 'Menu Data',
    'new_navigation' => 'New Navigation',
    'save' => 'Save',
    'posts' => 'Posts',
    'pages' => 'Pages',
    'categories' => 'Categories',
    'url' => 'URL',
    'add' => 'Add',
    'text' => 'Text',
    'save_order' => 'Save Order',
    'search' => 'search ...',
    'filter' => 'Filter',
    'updated' => 'successfully updated',
    
    "module" => "Navigation",
    "permissions" => [
        'manage' => "Manage Navigation"
    ]
];
