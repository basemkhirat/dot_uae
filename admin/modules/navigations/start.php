<?php

Menu::set("sidebar", function($menu) {
    if (User::can('navigation.manage')) {
        $menu->item('options.navigations', trans("admin::common.navigations"), URL::to(ADMIN . '/navigations'))->icon("fa-tasks");
    }
});

include __DIR__ ."/routes.php";
