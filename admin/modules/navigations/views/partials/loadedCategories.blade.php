@foreach($categories as $category)
<li class="dd-list">
    <a href="#" class="new-item dd-handle" data-item-id="{!!$category->cat_id!!}" data-item-type="category" data-item-value="{!!URL::to('/category/'.$category->cat_slug)!!}">{!!$category->cat_name!!}</a>
</li>
@endforeach