<?php

return [
    "galleries" => "Galleries",
    "edit" => "Edit Gallery",
    "search" => "search",
    "no_media_on" => "No media on ",
    "add_new" => "Add new",
    "per_page" => "per page",
    "bulk_actions" => "Bulk actions",
    "apply" => "Apply",
    "delete" => "Delete",
    "name" => "Gallery name",
    "actions" => "Action",
    "sure_delete" => "you are about to delete ... continue?",
    "page" => "Page",
    "add_new_gallery" => "Add new gallery",
    "add_media" => "Add media",
    "gallery_author" => "Gallery author",
    "no_media" => "No media has been added",
    "save_gallery" => "Save gallery",
    "is_not_valid_image" => "is not an image",
    "of" => "of",
    "search_galleries" => "seach galleries ..",
    "files" => "files",
    "bulk_actions" => "Bulk actions"
];
