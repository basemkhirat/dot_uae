<?php

class GalleriesController extends BackendController {

    public $data = array();

    public function index($gallery_id = false) {

        if (Request::isMethod("post")) {
            $action = Input::get("action");
            $ids = Input::get("id");
            if (count($ids)) {
                switch ($action) {
                    case "delete":
                        Gallery::whereIn("gallery_id", $ids)->delete();
                        //GalleryLang::whereIn("gallery_id", $ids)->delete();
                        GalleryMedia::whereIn("gallery_id", $ids)->delete();
                        return Redirect::back()->with("message", "تم حذف الألبوم بنجاح");
                        break;
                }
            }
        }

        $ob = Gallery::orderBy("gallery_id", "DESC");

        if (Input::has("q")) {
            $ob->where('gallery_name', 'LIKE', '%' . Input::get("q") . '%');
        }

        if (Input::has("per_page")) {
            $this->data["per_page"] = $per_page = Input::get("per_page");
        } else {
            $this->data["per_page"] = $per_page = 20;
        }

        $this->data["galleries"] = $galleries = $ob->paginate($per_page);


        if (!$gallery_id and count($galleries)) {
            $gallery_id = $galleries[0]->gallery_id;
        }

        if (DB::table("galleries_media")->where("gallery_id", $gallery_id)->count()) {
            $this->data["row"] = $last_gallery = Gallery::where("galleries.gallery_id", $gallery_id)
                    ->leftJoin("galleries_media", "galleries.gallery_id", "=", "galleries_media.gallery_id")
                    ->first();

            $this->data["gallery_media"] = GalleryMedia::where("galleries_media.gallery_id", $gallery_id)
                    ->join("media", "media.media_id", "=", "galleries_media.media_id")
                    ->get();
        } else {
            $this->data["row"] = $last_gallery = Gallery::orderBy("galleries.gallery_id", "DESC")
                    ->leftJoin("galleries_media", "galleries.gallery_id", "=", "galleries_media.gallery_id")
                    ->first();

            $this->data["gallery_media"] = [];
            if (count($last_gallery)) {
                $this->data["gallery_media"] = GalleryMedia::where("galleries_media.gallery_id", $last_gallery->gallery_id)
                        ->join("media", "media.media_id", "=", "galleries_media.media_id")
                        ->get();
            }
        }




        return View::make("galleries::show", $this->data);
    }

    public function create() {

        if (Request::isMethod("post")) {

            $name = Input::get("gallery_name");
            $slug = str_slug($name);

            // check role if exists
            if (Gallery::where("gallery_slug", "=", $slug)->count()) {
                return Redirect::back()->withErrors($slug . " is already exists")->withInput(Input::all());
            }

            $rules = array(
                'gallery_name' => 'required'
            );

            $validator = Validator::make(Input::all(), $rules);
            if ($validator->fails()) {
                return Redirect::back()->withErrors($validator)->withInput(Input::all());
            }

            Gallery::insert(array(
                "gallery_slug" => $slug,
                "gallery_author" => Input::get("gallery_author"),
                "gallery_name" => $name
            ));


            $gallery_id = DB::getPdo()->lastInsertId();

            // gallery media
            if ($ids = Input::get("media_id")) {
                $i = 1;
                foreach ($ids as $media_id) {

                    DB::table("media")->where("media_id", $media_id)->update(array(
                        "media_description" => $name . "-" . $i
                    ));

                    GalleryMedia::insert(array(
                        "gallery_id" => $gallery_id,
                        "media_id" => $media_id
                    ));
                    $i++;
                }
            }

            return Redirect::route("galleries.edit", array("id" => $gallery_id))->with("message", "تم إضافة الألبوم بنجاح");
        }


        $this->data["gallery"] = false;
        return View::make("galleries::edit", $this->data);
    }

    public function edit($gallery_id) {

        $role = Gallery::where("gallery_id", "=", $gallery_id)->first();

        if (count($role) == 0) {
            App::abort(404);
        }

        if (Request::isMethod("post")) {
            $name = Input::get("gallery_name");

            DB::table("galleries")->where("gallery_id", $gallery_id)->update(array(
                "gallery_author" => Input::get("gallery_author"),
                "gallery_name" => $name
            ));

            // gallery media
            GalleryMedia::where("gallery_id", $gallery_id)->delete();
            if ($ids = Input::get("media_id")) {
                $i = 1;
                foreach ($ids as $media_id) {

                    DB::table("media")->where("media_id", $media_id)->update(array(
                        "media_description" => $name . "-" . $i
                    ));

                    GalleryMedia::insert(array(
                        "gallery_id" => $gallery_id,
                        "media_id" => $media_id
                    ));
                    $i++;
                }
            }

            return Redirect::back()->with("message", "تم تعديل الألبوم بنجاح");
        }

        $this->data["gallery"] = $role;
        $this->data["gallery_media"] = GalleryMedia::where("gallery_id", $gallery_id)
                ->join("media", "media.media_id", "=", "galleries_media.media_id")
                ->get();

        return View::make("galleries::edit", $this->data);
    }

    function content() {
        if (Request::isMethod("post")) {

            $gallery_id = Input::get("id");
            $files = GalleryMedia::where("gallery_id", $gallery_id)
                    ->join("media", "media.media_id", "=", "galleries_media.media_id")
                    ->get();
            ?>

            <table cellpading="0" cellspacing="0">
                <tbody>
                    <tr>
                        <td>
                            <div style="overflow: hidden; background: #3e3e3e; margin: 10px 0; padding: 10px; border-radius: 4px">
                                <?php foreach ($files as $media) { ?>
                                    <div class="media_row" style="border-radius: 2px; margin: 4px 5px; width: 145px; border: 1px solid rgb(139, 139, 139); box-shadow: 0px 1px 1px rgb(142, 142, 142); float: right; height: 127px; position: relative; padding: 3px;margin-bottom: 30px;">
                                        <?php if ($media->media_provider == "") { ?>
                                            <img style="width:100%; height:100%" src="<?php echo thumbnail($media->media_path); ?>">
                                        <?php } else { ?>
                                            <?php if ($media->media_provider_image != "") { ?>
                                                <img style="width:100%; height:100%" src="<?php echo $media->media_provider_image; ?>" />
                                            <?php } else { ?>
                                                <img style="width:100%; height:100%" src="<?php echo assets("default/soundcloud.png"); ?>" />
                                            <?php } ?>
                                        <?php } ?>
                                        <span style="font-family: tahoma; background: none repeat scroll 0 0 #555555;  color: #ccc;  float: left;  height: 37px;  margin-top: -43px;  opacity: 0.81;  overflow: hidden;  width: 100%;  word-wrap: break-word;"><?php echo Str::limit($media->media_title, 30); ?></span>
                                    </div>
                                <?php } ?>
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>
            <?php
        }
    }

    function get($page = 1) {

        $limit = 20;

        $ob = DB::table("galleries")
                ->select(array("galleries.*", "media.*", "gallery_name", DB::raw('(select COUNT(*) from galleries_media where galleries_media.gallery_id = galleries.gallery_id) as media_count')))
                ->leftJoin("galleries_media", "galleries.gallery_id", "=", "galleries_media.gallery_id")
                ->leftJoin("media", "media.media_id", "=", "galleries_media.media_id")
                ->groupBy("galleries.gallery_id")
                ->orderBy("galleries.gallery_id", "DESC");

        if (Input::has("q")) {
            $ob->where('gallery_name', 'LIKE', '%' . Input::get("q") . '%');
        }

        if (Input::has("gallery_id")) {
            $ob->where('galleries.gallery_id', Input::get("gallery_id"));
        }

        $ob->take($limit);
        $ob->skip(($page - 1) * $limit);
        $galleries = $ob->get();

        if (count($galleries)) {
            ?>
                <?php foreach ($galleries as $gallery) { ?>
                <div class="col-md-12 gallery_row" gallery-type="<?php echo $gallery->media_type; ?>" gallery-id="<?php echo $gallery->gallery_id; ?>">
                    <?php /*
                      <div class="gallery_image">

                      <?php if ($gallery->media_path != "") { ?>
                      <?php if ($gallery->media_provider == "") { ?>
                      <img class="img-rounded" src="<?php echo thumbnail($gallery->media_path); ?>">
                      <?php } else { ?>
                      <?php if ($gallery->media_provider_image != "") { ?>
                      <img class="img-rounded" src="<?php echo $gallery->media_provider_image; ?>" />
                      <?php } else { ?>
                      <img class="img-rounded" src="<?php echo assets("default/soundcloud.png"); ?>" />
                      <?php } ?>
                      <?php } ?>
                      <?php }else{ ?>
                      <img class="img-rounded" src="<?php echo assets("default/post.png"); ?>" />
                      <?php } ?>

                      </div>
                     * 
                     */ ?>
                    <div class="gallery_details">
                        <div class="gallery_details_name"><?php echo $gallery->gallery_name; ?></div>
                        <div style="display: none" class="gallery_details_author"><?php echo $gallery->gallery_author; ?></div>
                        <div class="gallery_details_count">(<span><?php echo $gallery->media_count; ?></span>) <?php echo trans("media::media.files"); ?></div>
                    </div>
                    <div class="gallery_ctrls">
                        <a href="javascript:void(0)" class="gallery_edit text-primary" data-gallery="<?php echo $gallery->gallery_id; ?>">
                            <i class="fa fa-edit"></i>
                        </a>
                        <a href="javascript:void(0)" class="gallery_delete text-danger" data-gallery="<?php echo $gallery->gallery_id; ?>">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
            <?php
            }
        }
        ?>

        <?php
    }

    function files() {
        if (Request::isMethod("post")) {

            $gallery_id = Input::get("id");
            $gallery_media = GalleryMedia::where("gallery_id", $gallery_id)
                    ->join("media", "media.media_id", "=", "galleries_media.media_id")
                    ->get();
            ?>
            <ul class="media_rows">

            <?php foreach ($gallery_media as $media) { ?>
                    <li class="media_row">
                        <input type="hidden" name="media_id[]" value="<?php echo $media->media_id; ?>" />

                        <a href="#" class="media_row_delete">
                            <i class="fa fa-times"></i>
                        </a>

                        <div>
                            <i class="fa fa-arrows"></i>
                        </div>

                        <?php if ($media->media_provider == "") { ?>
                            <img src="<?php echo thumbnail($media->media_path, "thumbnail"); ?>">
                        <?php } else { ?>
                            <?php if ($media->media_provider_image != "") { ?>
                                <img src="<?php echo $media->media_provider_image; ?>" />
                            <?php } else { ?>
                                <img src="<?php echo assets("default/soundcloud.png"); ?>" />
                    <?php } ?>
                <?php } ?>

                        <label>
                            <input type="text" name="media_title[<?php echo $media->media_id; ?>]" value="<?php echo $media->media_title; ?>" />
                        </label>
                    </li>
            <?php } ?>
            </ul>

            <div class="text-center empty-content <?php if (count($gallery_media)) { ?> hidden <?php } ?>"><?php echo trans("galleries::galleries.no_media") ?></div>

            <?php
        }
    }

    function save() {

        $gallery_id = Input::get("gallery_id");
        DB::table("galleries_media")->where("gallery_id", $gallery_id)->delete();

        // update media files titles
        if (Input::get("media_title") == null) {
            return "";
        }

        $titles = Input::get("media_title");

        foreach ($titles as $media_id => $media_title) {
            DB::table("media")->where("media_id", $media_id)->update(array(
                "media_title" => $media_title
            ));
        }

        if ($ids = Input::get("media_id")) {
            foreach ($ids as $media_id) {
                GalleryMedia::insert(array(
                    "gallery_id" => $gallery_id,
                    "media_id" => $media_id
                ));
            }
        }
    }

    public function delete() {
        $ids = Input::get("id");
        if (is_array($ids)) {
            Gallery::whereIn("gallery_id", $ids)->delete();
            GalleryMedia::whereIn("gallery_id", $ids)->delete();
        } else {
            Gallery::where("gallery_id", $ids)->delete();
            GalleryMedia::where("gallery_id", $ids)->delete();
        }
        if (!Request::ajax()) {
            return Redirect::route("galleries.show")->with("message", "تم حذف الألبوم بنجاح");
        }
    }

}
