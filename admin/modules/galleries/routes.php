<?php

Route::group(array(
    "prefix" => ADMIN,
    "before" => "auth",
        ), function($route) {
    $route->group(array("prefix" => "galleries"), function($route) {
        $route->any('/delete', array("as" => "galleries.delete", "uses" => "GalleriesController@delete"));
        $route->any('/save', array("as" => "galleries.save", "uses" => "GalleriesController@save"));
         $route->any('/files', array("as" => "galleries.files", "uses" => "GalleriesController@files"));
        $route->any('/create', array("as" => "galleries.create", "uses" => "GalleriesController@create"));
         $route->any('/{id?}', array("as" => "galleries.show", "uses" => "GalleriesController@index"));
        
        
        $route->any('/content', array("as" => "galleries.content", "uses" => "GalleriesController@content"));
        $route->any('/{id}/edit', array("as" => "galleries.edit", "uses" => "GalleriesController@edit"));
        $route->any('/get/{offset?}', array("as" => "galleries.ajax", "uses" => "GalleriesController@get"));
    });
});
