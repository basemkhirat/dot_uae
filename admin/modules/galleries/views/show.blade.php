@extends("admin::layouts.master")


@section("breadcrumb")
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-4">
        <h2><?php echo trans("galleries::galleries.galleries") ?></h2>
        <ol class="breadcrumb">
            <li>
                <a href="<?php echo route("galleries.show"); ?>"><?php echo trans("galleries::galleries.galleries") ?> (<?php echo $galleries->total() ?>)</a>
            </li>
        </ol>
    </div>

    <div class="col-lg-6">
        <form action="" method="get" class="search_form">
            <div class="input-group">
                <input name="q" value="<?php echo Input::get("q"); ?>" type="text" class=" form-control" placeholder="<?php echo trans("galleries::galleries.search_galleries") ?>..."> <span class="input-group-btn">
                    <button class="btn btn-primary" type="button"> <?php echo trans("galleries::galleries.search"); ?></button> </span>
            </div>
        </form>
    </div>

    <div class="col-lg-2">
        <a href="<?php echo route("galleries.create"); ?>" class="btn btn-primary btn-labeled btn-main"> <span class="btn-label icon fa fa-plus"></span> &nbsp; <?php echo trans("galleries::galleries.add_new") ?></a>
    </div>
</div>
@stop

@section("content")



<?php if (Session::has("message")) { ?>
    <div class="alert alert-success">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <?php echo Session::get("message"); ?>
    </div>
<?php } ?>


<div class="row">
    <?php if(count($gallery_media)){ ?>
    
    <div class="col-md-8">

        <ul class="breadcrumb" style="padding: 8px 24px;font-weight: bold;">
            <li><a href="#"><?php echo trans("galleries::galleries.galleries"); ?></a></li>
            <li><a href="#"><?php echo @$row->gallery_name; ?> (<?php echo @count($gallery_media); ?>)</a></li>
        </ul>

        <br />

        <?php if (count($gallery_media)) { ?>
            <?php foreach ($gallery_media as $media) { ?>
                <div class="file-box">
                    <div class="file">
                        <a href="#">
                            <span class="corner"></span>

                            <?php if ($media->media_type == "image") { ?>
                                <div class="image">
                                    <img src="<?php echo thumbnail($media->media_path, "small"); ?>" class="img-responsive" alt="image">
                                </div>
                            <?php } elseif ($media->media_type == "audio") { ?>
                                <div class="icon"><i class="fa fa-music"></i></div>
                            <?php } elseif ($media->media_type == "video") { ?>
                                <div class="icon"><i class="img-responsive fa fa-film"></i></div>
                            <?php } else { ?>
                                <div class="icon"><i class="fa fa-file"></i></div>
                            <?php } ?>

                            <div class="file-name">
                                <?php echo $media->media_title; ?>
                                <br>
                                <small><?php echo arabic_date($media->media_created_date); ?></small>
                            </div>
                        </a>
                    </div>

                </div>
            <?php } ?>
        <?php } else { ?>
            <?php echo trans("galleries::galleries.no_media_on") ?> <?php echo @$row->gallery_name; ?>
        <?php } ?>

    </div>
    <?php } ?>

    <div class="<?php if(count($gallery_media)){ ?>col-md-4<?php }else{ ?>col-md-12<?php } ?>">
        <form action="" method="post" class="action_form">


            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-sm-4 m-b-xs">

                            <div class="form-group">
                                <select name="action" class="form-control chosen-select chosen-rtl" style="width:auto; display: inline-block;">
                                    <option value="-1" selected="selected"><?php echo trans("galleries::galleries.bulk_actions") ?></option>
                                    <option value="delete"><?php echo trans("galleries::galleries.delete") ?></option>
                                </select>
                                <button type="submit" style="margin-top: 5px" class="btn btn-primary"><?php echo trans("galleries::galleries.apply") ?></button>
                            </div>
                        </div>
                        <div class="col-sm-5 m-b-xs">

                        </div>
                        <div class="col-sm-3">

                            <select  name="post_status" id="post_status" class="form-control chosen-select chosen-rtl per_page_filter ">
                                <option value="" selected="selected">-- <?php echo trans("galleries::galleries.per_page") ?> --</option>
                                <?php foreach (array(10, 20, 30, 40) as $num) { ?>
                                    <option value="<?php echo $num; ?>" <?php if ($num == $per_page) { ?> selected="selected" <?php } ?>><?php echo $num; ?></option>
                                <?php } ?>
                            </select>

                        </div>
                    </div>
                    <div class="table-responsive">

                        <table cellpadding="0" cellspacing="0" border="0" class="table table-striped" id="jq-datatables-example">
                            <thead>
                                <tr>
                                    <th style="width:35px"><input type="checkbox" class="check_all i-checks" name="ids[]" /></th>
                                    <th><?php echo trans("galleries::galleries.name"); ?></th>
                                    <th><?php echo trans("galleries::galleries.actions") ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($galleries as $gallery) { ?>
                                    <tr>
                                        <td>
                                            <input class="i-checks" type="checkbox" name="id[]" value="<?php echo $gallery->gallery_id; ?>" />
                                        </td>
                                        <td>
                                            <a class="text-navy" href="<?php echo URL::to(ADMIN) ?>/galleries/<?php echo $gallery->gallery_id; ?>">
                                                <?php echo $gallery->gallery_name; ?>
                                            </a>
                                        </td>
                                        <td class="center">
                                            <a href="<?php echo URL::to(ADMIN) ?>/galleries/<?php echo $gallery->gallery_id; ?>/edit">
                                                <i class="fa fa-pencil text-navy"></i>
                                            </a>
                                            <a class="ask" message="<?php echo trans("galleries::galleries.sure_delete") ?>" href="<?php echo URL::to(ADMIN) ?>/galleries/delete?id[]=<?php echo $gallery->gallery_id; ?>">
                                                <i class="fa fa-times text-navy"></i>
                                            </a>
                                        </td>
                                    </tr>
                                <?php } ?>

                            </tbody>
                        </table>

                        <div class="table-footer clearfix">
                            <div class="DT-label">
                                <div class="dataTables_info" id="jq-datatables-example_info" role="alert" aria-live="polite" aria-relevant="all"><?php echo trans("galleries::galleries.page") ?> <?php echo $galleries->currentPage() ?> <?php echo trans("galleries::galleries.of") ?> <?php echo $galleries->lastPage() ?> </div>
                            </div>
                            <div class="DT-pagination">
                                <div class="dataTables_paginate paging_simple_numbers" id="jq-datatables-example_paginate">
                                    <?php echo str_replace('/?', '?', $galleries->appends(Input::all())->render()); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    </form>
                </div>
            </div>

    </div>

</div>

</div>

<script>
    $(document).ready(function () {
        
        $('.chosen-select').chosen();

        $('.i-checks').iCheck({
            checkboxClass: 'icheckbox_square-green',
            radioClass: 'iradio_square-green',
        });

        $('.check_all').on('ifChecked', function (event) {
            $("input[type=checkbox]").each(function () {
                $(this).iCheck('check');
                $(this).change();
            });
        });

        $('.check_all').on('ifUnchecked', function (event) {
            $("input[type=checkbox]").each(function () {
                $(this).iCheck('uncheck');
                $(this).change();
            });
        });

        $(".per_page_filter").change(function () {
            var base = $(this);
            var per_page = base.val();
            location.href = "<?php echo route("galleries.show") ?>?per_page=" + per_page;
        });
        
        $('.file-box').each(function () {
            animationHover(this, 'pulse');
        });

    });



</script>

@stop