<?php

class Gallery extends Model {

    protected $table = 'galleries';
    protected $primaryKey = 'gallery_id';
    protected $fillable = ["gallery_name", "gallery_slug", "gallery_author"];
    public $timestamps = true;

    public function scopeLang($query, $lang) {
        $query->join("galleries_langs", "galleries.gallery_id", "=", "galleries_langs.gallery_id")
                ->where("galleries_langs.lang", $lang);
        return $query;
    }

    
}
