<?php

Menu::set("sidebar", function($menu) {
        $menu->item('options.mailposts', trans("admin::common.reporter_emails"), URL::to(ADMIN . '/mailposts'))->icon("fa-envelope");
});

include __DIR__ ."/routes.php";
