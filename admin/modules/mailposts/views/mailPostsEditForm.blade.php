<form action="mailposts/update/<?php echo $emailobj[0]->mail_id ?>" method="post" id="update-form">
    <td>
        <input type="text" name="email<?php echo $emailobj[0]->mail_id ?>" id="email<?php echo $emailobj[0]->mail_id ?>" class="form-control" value="<?php echo $emailobj[0]->email ?>"/>
        <label style="display: none" for="email" class="error" id="email-error">{!!Lang::get('posts::posts.required')!!}</label>
    </td>
    <td>
        <input type="password" name="password<?php echo $emailobj[0]->mail_id ?>" id="password<?php echo $emailobj[0]->mail_id ?>" class="form-control" value="<?php echo $emailobj[0]->password ?>"/>
        <label style="display: none" for="password" class="error" id="email-error">{!!Lang::get('posts::posts.required')!!}</label>
    </td>

    <td>
        <select class="form-control form-group-margin" name="cat<?php echo $emailobj[0]->mail_id ?>" id="cat<?php echo $emailobj[0]->mail_id ?>" >
            <?php foreach ($categories as $category) { ?>
                <option value="<?php echo $category->cat_id ?>" <?php
                if ($category->cat_id == $emailobj[0]->cat_id) {
                    echo " selected";
                }
                ?>><?php echo $category->cat_name ?></option> 
                    <?php } ?>
        </select>
        <label style="display: none" for="cat" class="error" id="email-error">{!!Lang::get('posts::posts.required')!!}</label>
    </td>

    <td colspan="2"><input type="submit" value="<?php echo trans('mailposts::mailposts.done') ?>" id="<?php echo $emailobj[0]->mail_id ?>" class="btn btn-primary edit_tag"/></td>
</form>


