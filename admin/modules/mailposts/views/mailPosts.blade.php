@extends("admin::layouts.master")

@section("breadcrumb")
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2><i class="fa fa-envelope faa-tada animated faa-slow"></i>  <?php echo trans('mailposts::mailposts.title') ?> ({!!@$emails->total()!!})</h2>
        <ol class="breadcrumb" style="background: none;">
            <li><a  href="{!!URL::to('/'.ADMIN.'/dashboard')!!}">{!!Lang::get('admin::common.dashboard')!!}</a></li>
            <li><?php echo trans('mailposts::mailposts.title') ?></li>

        </ol>
    </div>

</div>
@stop
@section("content")


<div id="content-wrapper">

    <div class="row">
        <div class="col-sm-6">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5> <?php echo trans('mailposts::mailposts.add') ?> </h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>

                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>

                <form action="<?php echo URL::to(ADMIN . "/mailposts/add"); ?>" class="panel form-horizontal" method="post" id="mails-form">

                    <div class="ibox-content">
                        <div class="row form-group">
                            <label class="col-sm-4 control-label"><?php echo trans('mailposts::mailposts.email') ?></label>
                            <div class="col-sm-8">
                                <input type="text" name="email" value="" class="form-control">
                            </div>
                        </div>
                        <div class="row form-group">
                            <label class="col-sm-4 control-label"><?php echo trans('mailposts::mailposts.password') ?></label>
                            <div class="col-sm-8">
                                <input type="password" name="password" value="" class="form-control">
                            </div>
                        </div>
                        <div class="row form-group">
                            <label class="col-sm-4 control-label"><?php echo trans('mailposts::mailposts.category') ?></label>
                            <div class="col-sm-8">
                                <select class="form-control form-group-margin" name="cat">
                                    <?php foreach ($categories as $category) { ?>
                                        <option value="<?php echo $category->cat_id ?>"><?php echo $category->cat_name ?></option> 
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="panel-footer text-right" style="border-top: 1px solid #ececec; background: none;">
                            <input type="submit" value="<?php echo trans('mailposts::mailposts.add') ?>" class="btn btn-primary"/>
                        </div>
                    </div>

                </form>
            </div>
        </div>
        <div class="col-sm-6">
            {!! Form::open(array('method' => 'get', 'id' => 'search', 'class' => 'choosen-rtl')) !!}

            <div class="input-group">
                <input name="q" id="q" value="{!!(Input::get('q')) ? Input::get('q') : ''!!}" type="text" placeholder="<?php echo trans("mailposts::mailposts.search_emails") ?>..." type="text" class="input-sm form-control" > <span class="input-group-btn">
                    <button type="submit" class="btn btn-sm btn-primary"> {!!Lang::get('mailposts::mailposts.search')!!}</button> </span>
            </div>

            {!! Form::close() !!}
        </div>
        <div class="col-sm-6">


            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5> <?php echo trans('mailposts::mailposts.title') ?> </h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>

                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <div class="table-responsive">


                        <table cellspacing="0" cellpadding="0" border="0" class="table table-striped">
                            <thead>
                                <tr>
                                    <th><?php echo trans('mailposts::mailposts.email') ?></th>
                                    <th><?php echo trans('mailposts::mailposts.password') ?></th>
                                    <th><?php echo trans('mailposts::mailposts.category') ?></th>
                                    <th><?php echo trans('mailposts::mailposts.edit') ?></th>
                                    <th><?php echo trans('mailposts::mailposts.del') ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($emails as $email) { ?>
                                    <tr class="gradeA" id="tag_row<?php echo $email->mail_id ?>">
                                        <td><a href="" class="tag_link"  rel="<?php echo $email->mail_id ?>"><?php echo $email->email ?></a></td>
                                        <td>*****</td>
                                        <td>
                                            <?php
                                            foreach ($categories as $category) {
                                                if ($category->cat_id == $email->cat_id) {
                                                    echo $category->cat_name;
                                                }
                                            }
                                            ?>
                                        </td>
                                        <td class="tooltip-demo m-t-md"><a href="" class="btn btn-white btn-sm tag_link"  rel="<?php echo $email->mail_id ?>"><i class="fa fa-pencil"></i></a></td>
                                        <td class="tooltip-demo m-t-md"><a class="btn btn-white btn-sm" message="<?php echo trans("mailposts::mailposts.sure_delete") ?>" href="<?php echo URL::to(ADMIN . "/mailposts/del/" . $email->mail_id); ?>">
                                                <i class="fa fa-times"></i>
                                            </a></td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                        <div class="panel-footer text-right" style="border-top: 1px solid #ececec; background: none;">
                            <div class="col-sm-2 m-b-xs" style="padding:0">

                            </div>

                            <div class="col-sm-10 m-b-xs" style="padding:0">
                                <div class="pull-right">
                                    {!!$emails->appends(Input::all())->setPath('')->render()!!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


</div> <!-- / #content-wrapper -->
<script src="<?php echo assets() ?>/js/plugins/validate/jquery.validate.min.js"></script>
<script>
//get edit form
$(document).ready(function () {
    $("body").on("click", ".tag_link", function () {

        var base = $(this);
        var tag_id = base.attr("rel");
        var url = "<?php echo URL::to(ADMIN . '/mailposts/edit'); ?>/" + tag_id;
        $.get(url, function (data) {
            $("#tag_row" + tag_id).html(data);
        });
        return false;
    });

    //call save edit function
    $("body").on("click", ".edit_tag", function () {
        var base = $(this);
        var tag_id = base.attr("id");
        var url = "<?php echo URL::to(ADMIN . '/mailposts/update'); ?>/" + tag_id;
        var email = $('#email' + tag_id).val();
        var password = $('#password' + tag_id).val();
        var category = $('#cat' + tag_id).val();
        var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
        if(email == ''){
            $('#email' + tag_id).next().html("{!!Lang::get('posts::posts.required')!!}").show();
            return false;
        }else if(!filter.test(email)){
            $('#email' + tag_id).next().html("{!!Lang::get('posts::posts.invalid')!!}").show();
            return false;
        }else{
            $('#email' + tag_id).next().hide();
        }
        
        if(password == ''){
            $('#password' + tag_id).next().show();
            return false;
        }else{
            $('#password' + tag_id).next().hide();
        }
        
        if(category == ''){
            $('#cat' + tag_id).next().show();
            return false;
        }else{
            $('#cat' + tag_id).next().hide();
        }
        
        $.get(url, {
            email: email,
            password: password,
            category: category
        }, function (data) {
            $("#tag_row" + tag_id).html(data);
        });
        return false;
    });

    $('#mails-form').validate({
        ignore: [],
        rules: {
            email: {
                required: true,
                email: true
            },
            password: {
                required: true,
            },
            cat: {
                required: true,
            },
        }
    });

    
    jQuery.extend(jQuery.validator.messages, {
        required: "{!!Lang::get('posts::posts.required')!!}",
        email: "{!!Lang::get('posts::posts.invalid')!!}",
    });

    var config = {
        '.chosen-select': {},
        '.chosen-select-deselect': {
            allow_single_deselect: true
        },
        '.chosen-select-no-single': {
            disable_search_threshold: 10
        },
        '.chosen-select-no-results': {
            no_results_text: "{!!Lang::get('posts::posts.notfound')!!}"
        },
        '.chosen-select-width': {
            width: "95%"
        }
    }
    for (var selector in config) {
        $(selector).chosen(config[selector]);
    }
    
    // search submit
    $("#search").submit(function (e) {
        if($("#q").val() == ''){
            return false
        }
    });
    
});

</script>


@stop
