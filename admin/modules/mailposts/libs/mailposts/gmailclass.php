<?php

/**
 * @author  : Chern Kuan GOH
 * @link    : http://chernkuan.blogspot.com/
 * @email   : chernkuangoh@gmail.com
 * @name    : Gmail Class
 * @version : 2
 * @since   : 19 Dec 2010 (Version 1)
 *            24 Dec 2010 (Version 2)
 *              - Added final to certain functions
 *              - Changed constructor to default constructor (__construct)
 *              - Minor Changes to variable casting
 * @uses    : Connect to Gmail,
 * @uses    : Retrieve emails,
 * @uses    : Download Attachments into a file directory,
 * 
 * @todo    : Send Emails, insert attachment
 *
 */

class GmailClass {
    //=========================================================
    // FIELDS
    //=========================================================
    public static $EMAIL_NOT_CONNECTED  = FALSE;
    public static $FILE_NOT_EXIST       = FALSE;
    public static $MAIL_NOT_MULTIPART   = FALSE;

    //Host, Login, Password
    private $mHost;
    private $mLogin;
    private $mPassword;

    //Message Array
    public $mMessage;
    //file directory
    public  $mFilePath;

    //IMAP functions
    private $mImap_open;
    private $mImap_header;

    //Email message
    private $htmlmsg;
    private $plainmsg;
    
    //Download Attachments?
    private $mDOWNLOAD_ATTACHMENTS;

    //Total number of email
    private $total_email;

    public $emails;

    //=========================================================
    // CONSTRUCTOR
    //=========================================================
    final public function  __construct() {
        self::setMessagetype();
    }

    //=========================================================
    // GETTER
    //=========================================================
    private function getFilePath()
    {
        return $this->mFilePath;
    }

    private function getHost()
    {
        return $this->mHost;
    }

    private function getLogin()
    {
        return $this->mLogin;
    }

    private function getPassword()
    {
        return $this->mPassword;
    }

    /**
     * @uses: Get the number of emails in the mailbox
     */
    final public function getEmailNum()
    {
        //echo "Total number of emails: ".$this->total_email."<br>";
    }

    /**
     * @uses: For checking Part Type
     * @param <int> $rParttype
     * @return<string> Part Type
     * @example: getParttype($rPartValue->type)
     */
    private function getParttype($rPartTypeNum)
    {
        switch ($rPartTypeNum)
        {
            case 0 : return "text";        break;
            case 1 : return "multipart";   break;
            case 2 : return "message";     break; //embedded
            case 3 : return "application"; break;
            case 4 : return "audio";       break;
            case 5 : return "image";       break;
            case 6 : return "video";       break;
            case 7 : return "other";       break;
            default: return "unknown";     break;
        }
    }

    /**
     *
     * @param <type> $rMsg_num
     * @param <type> $rStructure
     * @param <type> $rPartnum '1', '2', '2.1', '2.1.3', etc if multipart, 0 if not
     */
    private function getPart($rMsg_num,$rPartValue,$rPartnum)
    {
        //echo"============ Get PartType ============ <br>";
        //echo"Part number: $rPartnum <br>";
        $data = $rPartnum ?
                imap_fetchbody($this->mImap_open, $rMsg_num, $rPartnum) : //Multipart
                imap_body($this->mImap_open, $rMsg_num); //Not Multipart

        //Check all possible encoded parts
        //Any part may be encoded, even plain text messages, so check everything.
        //No need to decode 7-bit, 8-bit, or binary
        switch($rPartValue->encoding)
        {
            case 3  : $data = base64_decode($data); break;
            case 4  : $data = quoted_printable_decode($data); break;
            default : break;
        }

        $fpos = 2;
        $this->mMessage["pid"][$rPartnum] = ($rPartnum);
        switch($rPartValue->disposition)
        {
            case "INLINE"    :
                //echo "INLINE <br>";
                //echo"============ END PART ============ <br>";
                break;
            case "ATTACHMENT":
                //echo "Attachments found! <br>";
                if($this->mDOWNLOAD_ATTACHMENTS)
                {
                    //echo"Downloading Attachments <br>";
                    $fpos = self::downloadattachment($rPartnum,$rPartValue,$fpos,$rMsg_num,$this->mImap_header);
                }
                //echo"============ END PART ============ <br>";
                break;
            default          :
                //echo "Part Type: ".self::getParttype($rPartValue->type)."<br>";
                if($data)
                {
                    switch (self::getParttype($rPartValue->type))
                    {
                        case "text"       : //Email Text (without attachment)
                            //echo"case: text <br>";

                            if (strtolower($rPartValue->subtype)=='plain')
                            {
                                self::setMailDetails($rMsg_num, $data, self::getParttype($rPartValue->type), $rSubParttype='plain');      
                            }
                            else
                            {
                                self::setMailDetails($rMsg_num, $data, self::getParttype($rPartValue->type), $rSubParttype='htmlmsg');
                            }
                            //echo"============ END PART ============ <br>";
                            break;

                        case "message"    : //Embedded Message
                            //echo "case: message <br>";
                            self::setMailDetails($rMsg_num, $data, self::getParttype($rPartValue->type));
                            //echo"============ END PART ============ <br>";
                            break;

                        case "multipart"  : //Email which contains both text and attachment
                            //echo "case: multipart <br>";
                            self::setMailDetails($rMsg_num, $data, self::getParttype($rPartValue->type));
                            //echo"============ END PART ============ <br>";
                            break;

                        case "other"      : //echo "other <br>"; break;
                        case "unknown"    : //echo "case unknown<br>"; break;
                        default           : //echo "parttype oob<br>"; break;
                    }
                }
                /*
 
                 * 
                 */
                break;
         }
            //imap_delete tags a message for deletion
            //imap_delete($this->mImap_open,$num_msg);
    }

    /**
     * @uses: Get decoding number and decode message
     * @param <Message> $rMessage
     * @param <int> $rCoding
     * @return <Message> Decoded message
     */
    private function getdecodevalue($rMessage, $rCoding)
    {
        switch ($rCoding)
        {
            case 0: $rMessage = imap_base64($rMessage);     break;
            case 1: $rMessage = imap_8bit($rMessage);     break;
            case 2: $rMessage = imap_binary($rMessage);   break;
            case 3: $rMessage = imap_base64($rMessage);   break;
            case 4: $rMessage = quoted_printable_decode($rMessage);   break;
            case 5: $rMessage = imap_base64($rMessage);   break;
            default: //echo "Encoding: $rCoding <br>";      break;
        }
        return $rMessage;
    }

    /**
     * @uses: Get Email Messages AFTER IMAP_open connection is established
     */
    final public function getMail()
    {        

        $this->emails = imap_search($this->mImap_open,'UNSEEN');
        if($this->emails) {            
            /* put the newest emails on top */
            rsort($this->emails);
            /* for every email... */
            foreach($this->emails as $email_number) 
            //for ($num_msg = 1; $num_msg <= imap_num_msg($this->mImap_open); $num_msg++)
            {
                $this->mMessage["Message"][$email_number] = $email_number;
                /*//echo "<br>";
                //echo"####################################### <br>";
                //echo"############ EMAIL Message ############ <br>";
                //echo "Message No: ".$num_msg."<br>";*/
                //Header
                $this->mImap_header = imap_header($this->mImap_open, $email_number);
                //Body
                $structure = imap_fetchstructure($this->mImap_open, $email_number);       
                
                switch($parts = $structure->parts)
                {
                    case self::$MAIL_NOT_MULTIPART:
                        ////echo"NOT MULTIPART <br>";
                        self::getPart($email_number,$structure,0);
                        break;
                    default:
                        ////echo "MULTIPART <br>";
                        /*
                         * For each part, $partnum (key) is assigned the part number
                         * Value of current element is assigned to $part (value)
                         * This is the same as:
                           for ($partnum = 0; $partnum < count($parts); $i++)
                           {
                                $part = $parts[$partnum];
                           }
                         */
                        foreach($parts as $partnum=>$part)
                        {
                            //$partnum+1: First part is $partnum==1
                            self::getPart($email_number,$part,$partnum+1);
                        }
                        break;
                }
               /* //echo"############# Message END ############# <br>";
                //echo"####################################### <br>";
                //echo"<br><br>";*/
            }
        }
        // imap_expunge deletes all tagged messages
        //imap_expunge($this->mImap_open);
        imap_close($this->mImap_open);
    }

    /**
     * @uses: Get details of Email
     * @param <int> $rMsg_No
     */
    final public function getMaildetails($rMsg_No)
    {
        (int)$msg_num = $rMsg_No;
        if($this->mMessage["Message"][$msg_num] != null)
        {
            /*//echo "Sender      : ".$this->mMessage["sender"][$msg_num]."<br>";
            //echo "Filename    : ".print_r($this->mMessage["filename"][$msg_num])."<br>";
            ////echo "Mail Date   : ".$this->mMessage["maildate"][$msg_num]."<br>";
            ////echo "CC          : ".$this->mMessage["CC"][$msg_num]."<br>";
            //echo "Subject     : ".imap_utf8($this->mMessage["subject"][$msg_num])."<br>";
            ////echo "Message Size: ".$this->mMessage["size"][$msg_num]."<br>";
            //echo "Unseen      : ".$this->mMessage["unseen"][$msg_num]."<br>";
            //HTML Message
            /*
            if($this->mMessage["htmlmsg"][$msg_num]!= null)
            {
                //echo "HTML Message: <br>".$this->mMessage["htmlmsg"][$msg_num]."<br>";
            }
             * 
             */
            //Message Output
            if(($this->mMessage["text"][$msg_num]) != null)
            {
                return imap_utf8($this->mMessage["text"][$msg_num]);
            }
            elseif(($this->mMessage["multipart"][$msg_num]) != null)
            {
                return imap_utf8($this->mMessage["multipart"][$msg_num]);
            }
            elseif(($this->mMessage["message"][$msg_num]) != null)
            {
                return $this->mMessage["message"][$msg_num];
            }
            elseif(($this->mMessage["application"][$msg_num]) != null)
            {
                return $this->mMessage["application"][$msg_num];
            }
            elseif(($this->mMessage["audio"][$msg_num]) != null)
            {
                return $this->mMessage["audio"][$msg_num];
            }
            elseif(($this->mMessage["image"][$msg_num]) != null)
            {
                return $this->mMessage["image"][$msg_num];
                break;
            }
            elseif(($this->mMessage["video"][$msg_num]) != null)
            {
                return $this->mMessage["video"][$msg_num];
            }
            elseif(($this->mMessage["other"][$msg_num]) != null)
            {
                return $this->mMessage["other"][$msg_num];
            }
            else
            {
                return $this->mMessage["unknown"][$msg_num];
            }
        }

        else
            return "There is no such message";
    }

    //=========================================================
    // SETTER
    //=========================================================
    /**
     * @uses: File directory for the file to be downloaded into
     * @param <String> $rFilePath 
     * @example: $rFilePath = "/usr/Downloads/";
     */
    final public function setFilePath($rFilePath)
    {
        $this->mFilePath    = (string)$rFilePath;
    }

    /**
     * @uses: Set Host
     * @param <String> $rHost
     * @example: $rHost = "{imap.gmail.com:993/imap/ssl}INBOX";
     */
    final public function setHost($rHost)
    {
        $this->mHost        = (string)$rHost;
    }

    /**
     * @uses: Set Login
     * @param <String> $rLogin
     * @example: $rLogin = "myemail@gmail.com";
     */
    final public function setLogin($rLogin)
    {
        $this->mLogin       = (string)$rLogin;
    }

    /**
     * @uses: Set Gmail Password
     * @param <String> $rPassword
     * @Example: $rPassword = "mypassword";
     */
    final public function setPassword($rPassword)
    {
        $this->mPassword    = (string)$rPassword;
    }

    /**
     * @uses: Option to download attachments
     * @uses: DEFAULT: Attachments not downloaded
     * @param <Bool> $rDownload_Attachments
     */
    final public function setDownloadAttachments($rDownload_Attachments = FALSE)
    {
        $this->mDOWNLOAD_ATTACHMENTS = (bool)$rDownload_Attachments;
    }

    private function setMessagetype()
    {
        //Declare a msg array
        $message = array();
        $message["attachment"]["type"][0] = "text";
        $message["attachment"]["type"][1] = "multipart";
        $message["attachment"]["type"][2] = "message";
        $message["attachment"]["type"][3] = "application";
        $message["attachment"]["type"][4] = "audio";
        $message["attachment"]["type"][5] = "image";
        $message["attachment"]["type"][6] = "video";
        $message["attachment"]["type"][7] = "other";

        $this->mMessage = $message;
    }
   
    /**
     * @uses: Save: Mail Date, Sender, CC, Subject, Message size, Email Text Body, filename
     * @param <int> $rMsg_num
     * @param <type> $rData
     * @param <type> $rParttype
     * @param <String> $rSubParttype
     */
    private function setMailDetails($rMsg_num, $rData=null, $rParttype=null, $rSubParttype=null,$rFilename=null)
    {
        //Email Date
        $msg_time = strtotime($this->mImap_header->date);
        $this->mMessage["maildate"][$rMsg_num] = date("Y-m-d,H:i", $msg_time);

        //Sender Address
        $this->mMessage["senderAddr"][$rMsg_num]   = $this->mImap_header->from[0]->mailbox."@".$this->mImap_header->from[0]->host;

        //Sender Name
        $this->mMessage["senderName"][$rMsg_num]   = $this->mImap_header->from[0]->personal;

        //CC
        $this->mMessage["CC"][$rMsg_num]       = $this->mImap_header->cc;

        //Email subject
        $this->mMessage["subject"][$rMsg_num]  = imap_utf8($this->mImap_header->subject);

        //Email size
        $this->mMessage["size"][$rMsg_num]     = $this->mImap_header->size;

        //Unseen
        $this->mMessage["unseen"][$rMsg_num]     = $this->mImap_header->Unseen;

        if($rFilename != null)
        {
           $this->mMessage["filename"][$rMsg_num][] = $rFilename;
        }

        //Email Text Body
        if($rSubParttype == "htmlmsg")
        {
            $this->mMessage["htmlmsg"][$rMsg_num].= $rData."<br><br>";
        }
        else //plain, ...
        {
            //echo $this->mMessage[$rParttype][$rMsg_num][]= trim($rData) ."\n\n";
        }
    }

    //=========================================================
    // METHODS
    //=========================================================
    /**
     * @uses: Starts Gmail connection by opening an IMAP Stream to mailbox
     *
     * Note: Please use the set functions to set the host, login and password
     *       (setHost(), etc) before using this function.
     */
    final public function imap_open()
    {
        switch ($this->mImap_open = imap_open($this->mHost, $this->mLogin, $this->mPassword))
        {
            case self::$EMAIL_NOT_CONNECTED :
                ////echo "Unable to connect: ".imap_last_error()."<br>";
                return self::$EMAIL_NOT_CONNECTED;
                break;
            default                         :
                ////echo "Connection Successful <br>";
                $this->total_email = imap_num_msg($this->mImap_open);
                return TRUE;
                break;

        }
    }
    /**
     * @uses  Download attachment
     * @param <int> $rPartNum
     * @param <type> $rPartType
     * @param int $rfpos
     * @param <int> $rNum_msg
     * @param <type> $rHeader
     * @return int
     */
    private function downloadattachment($rPartNum, $rPartType, $rfpos, $rNum_msg, $rHeader)
    {
        (int)$i = $rPartNum;

        $this->mMessage["type"][$i] = $this->mMessage["attachment"]["type"][$rPartType->type]."/".strtolower($rPartType->subtype);

        ////echo"Type: ".$this->mMessage["attachment"]["type"][$rPartType->type]."<br>";

        $this->mMessage["subtype"][$i] = strtolower($rPartType->subtype);
        $ext = $rPartType->subtype;
        $params = $rPartType->dparameters;
        $filename = $rPartType->dparameters[0]->value;

        $mege = "";
        $data = "";

        $mege = imap_fetchbody($this->mImap_open, $rNum_msg, $rPartNum);
        ////echo "Grabbing details... <br>";

        //Filename
        $filename = $this->getFilePath().$filename;

        self::setMailDetails($rNum_msg, null, null, null,$filename);

        switch (file_exists($filename))
        {
            case self::$FILE_NOT_EXIST:
                ////echo"Creating file... <br>";
                self::createfile($filename,$rPartType->type,$mege);

                break;
            default                   :
                ////echo "Same file exists in the directory <br>";
                break;

        }
        $rfpos+=1;

        return $rfpos;
    }
    
    /**
     *
     * @param <String> $rFilename
     * @param <Part> $rPartType
     */
    private function createfile($rFilename,$rType,$rMege)
    {
        $fp = fopen($rFilename, w);
        $decodedmsg = self::getdecodevalue($rMege, $rType);
        fputs($fp, $decodedmsg);
        fclose($fp);
        ////echo "File has been created! <br>";
    }
}

?>
