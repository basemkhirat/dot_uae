<?php
header('Content-Type: text/html; charset=utf-8');

/**
 * @author  : Chern Kuan GOH
 * @link    : http://chernkuan.blogspot.com/
 * @email   : chernkuangoh@gmail.com
 * @name    : Run Gmail Class
 *
 * @uses    : Connect to Gmail,
 * @uses    : Retrieve emails,
 * @uses    : Download Attachments into a file directory,
 *
 * @todo    : Send Emails, insert attachment
 *
 */

error_reporting(E_ALL);

//Include all the functions that are required
require_once(app_path()."/mailposts/gmailclass.php");

//Inherit Exception class
class IMAP_Exception extends Exception {};

//Set the default time zone
//date_default_timezone_set(Singapore);

//=========================================================
// Initialise ALL Fields
//=========================================================
//Message Number to get Attachment Details
$msg_no = 1;

//Instantiate
$g = new GmailClass();

/*
Gmail
{imap.gmail.com:993/imap/ssl}INBOX
Yahoo
{imap.mail.yahoo.com:993/imap/ssl}INBOX
AOL
{imap.aol.com:993/imap/ssl}INBOX
Horde used by Plesk control panel;
{example.com:143/notls}INBOX
*/
//Gmail Host, Login, Password, File Directory
$g->setHost('{imap.gmail.com:993/imap/ssl}INBOX');
$g->setLogin('adnan.mokhtar@media-sci.com');
$g->setPassword('programmer_2012');
$g->setFilePath(app_path()."/mailposts/usr/Downloads/");

//Download Attachments automatically
$g->setDownloadAttachments(TRUE);

//=========================================================
// Log in to Gmail and Get Email
//=========================================================
try
{
    if(!($g->imap_open()))
    {
        throw new IMAP_Exception("IMAP_OPEN_ERROR");
    }
    else
    {
        $g->getMail();
    }
}
catch (IMAP_Exception $e)
{
    //echo $e->getMessage();
}

//=========================================================
// Get total number of emails
//=========================================================
//$g->getEmailNum();

//=========================================================
// Get Attachment Details
//=========================================================
if(count($g->emails)){
foreach ($g->emails as $email_number) {
	//$g->getMaildetails($email_number);
	echo $g->mMessage["senderAddr"][$email_number]."<br>";
	echo $g->mMessage["senderName"][$email_number]."<br>";
	echo $g->mMessage["subject"][$email_number]."<br>";
	echo $g->mMessage["unseen"][$email_number]."<br>";
	print_r($g->mMessage["filename"][$email_number])."<br>";
	echo $g->getMaildetails($email_number)."<br><br>";
}
}


?>
