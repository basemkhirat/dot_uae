<?php

Route::group(array(
    "prefix" => ADMIN,
    "before" => "auth",
        ), function($route) {
    //Route::resource('mailposts', 'MailPostController');
    $route->group(array("prefix" => "mailposts"), function($route) {
        $route->get('/', "MailPostController@index");
        $route->post('add', "MailPostController@addEmail");
        $route->get('edit/{id}', "MailPostController@editEmail");
        $route->get('update/{id}', "MailPostController@updateEmail");
        $route->get('del/{id}', "MailPostController@deleteEmail");
    });
});

Route::get('mailposts/get/all', "MailPostController@getemails"); //repeate every # minutes
