<?php

class MailPosts extends Model {

    protected $table = 'posts_email';

    public static function getEmails() {
        return $emails = DB::table('posts_email')->take(20)->paginate();
    }

    public static function getEmail($id) {
        return $email = DB::table('posts_email')
                ->where("posts_email.mail_id", "=", $id)
                ->get();
    }

    public static function saveEmail($row) {
        return $id = DB::table('posts_email')->insertGetId($row);
    }

    public static function updateEmail($row, $id) {
        DB::table('posts_email')->where('mail_id', $id)->update($row);
    }

    public static function deleteEmail($id) {
        DB::table('posts_email')->where('mail_id', '=', $id)->delete();
    }

}
