<?php

class MailPostController extends BackendController {

    public $conn = '';

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function __construct() {
        date_default_timezone_set("Etc/GMT-2");

        $this->conn = Session::get('conn');
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index() {
        $this->data['categories'] = Category::getCategories($this->conn);
        $this->data['emails'] = MailPosts::getEmails();
        return View::make("mailposts::mailPosts", $this->data);
    }

    public function addEmail() {
//         if(User::can('mailpost.create')){
        $email = Input::get('email');
        $password = Input::get('password');
        $cat = Input::get('cat');
        $row = array();
        $row['email'] = $email;
        $row['password'] = $password;
        $row['cat_id'] = $cat;
        $ct_id = MailPosts::saveEmail($row);
        return Redirect::to(ADMIN . '/mailposts');
//         }else{
//             return View::make("mailposts::errors.denied");
//         }
    }

    public function editEmail($id) {
//        if(User::can('mailpost.edit')){
        $this->data['emailobj'] = MailPosts::getEmail($id);
        $this->data['categories'] = Category::getCategories($this->conn);
        return View::make("mailposts::mailPostsEditForm", $this->data);
//        }else{
//            return View::make("mailposts::errors.denied");
//        }
    }

    public function updateEmail($id) {
        if (Request::ajax()) {
            $this->data['id'] = $id;
            $this->data['categories'] = Category::getCategories($this->conn);
            $this->data['email'] = $email = Input::get('email');
            $this->data['password'] = $password = Input::get('password');
            $this->data['category'] = $category = Input::get('category');
            $row = array();
            $row['email'] = $email;
            $row['password'] = $password;
            $row['cat_id'] = $category;
            MailPosts::updateEmail($row, $id);
            return View::make("mailposts::mailPostsEdited", $this->data);
        }
    }

    public function deleteEmail($id) {
//        if(User::can('tags.edit')){
        MailPosts::deleteEmail($id);
        return Redirect::to(ADMIN . '/mailposts');
//        }else{
//             return View::make("mailposts::errors.denied");
//        }
    }

    public function getemails() {
        //get all emails and loop on them calling getMailPosts
        $emails = MailPosts::getEmails();
        foreach ($emails as $email) {
            $this->getMailPosts($email->email, $email->password, $email->cat_id);
        }
    }

    public function getMailPosts($email, $password, $category) {
        error_reporting(0);
        //Include all the functions that are required
        //require_once(app_path() . "/mailposts/gmailclass.php");
        $msg_no = 1;

        //Instantiate
        $g = new GmailClass();

        $g->setHost('{imap.gmail.com:993/imap/ssl}INBOX');
        $g->setLogin($email);
        $g->setPassword($password);
        $p = base_path() . "/public/uploads/";
        $g->setFilePath(public_path() . "/uploads/");
        //Download Attachments automatically
        $g->setDownloadAttachments(TRUE);
        if (!($g->imap_open())) {
            die("Can't open email $email, please make sure that email and password you provided were correct.");
        } else {
            $g->getMail();
        }
        $stream = imap_open('{imap.gmail.com:993/imap/ssl}INBOX', $email, $password);
        $this->store($g, $stream, $category);
    }

    //attachments
    public function getAttachments($imap, $mailNum, $part, $partNum) {
        $attachments = array();

        if (isset($part->parts)) {
            foreach ($part->parts as $key => $subpart) {
                if ($partNum != "") {
                    $newPartNum = $partNum . "." . ($key + 1);
                } else {
                    $newPartNum = ($key + 1);
                }
                $result = $this->getAttachments($imap, $mailNum, $subpart, $newPartNum);
                if (count($result) != 0) {
                    array_push($attachments, $result);
                }
            }
        } else if (isset($part->disposition)) {
            if ($part->disposition == "ATTACHMENT") {
                $partStruct = imap_bodystruct($imap, $mailNum, $partNum);
                $attachmentDetails = array(
                    "name" => $part->dparameters[0]->value,
                    "partNum" => $partNum,
                    "enc" => $partStruct->encoding
                );
                return $attachmentDetails;
            }
        }

        return $attachments;
    }

    //attachments
//inline
    public function getInlines($imap, $mailNum, $part, $partNum) {
        $attachments = array();

        if (isset($part->parts)) {
            foreach ($part->parts as $key => $subpart) {
                if ($partNum != "") {
                    $newPartNum = $partNum . "." . ($key + 1);
                } else {
                    $newPartNum = ($key + 1);
                }
                $result = $this->getInlines($imap, $mailNum, $subpart, $newPartNum);
                if (count($result) != 0) {
                    array_push($attachments, $result);
                }
            }
        } else if (isset($part->disposition)) {
            if ($part->disposition == "INLINE") {
                $partStruct = imap_bodystruct($imap, $mailNum, $partNum);
                $partbody = imap_fetchbody($imap, $mailNum, $partNum);
                $attachmentDetails = array(
                    "name" => $part->dparameters[0]->value,
                    "partNum" => $partNum,
                    "enc" => $partStruct->encoding,
                    "img" => $partbody
                );
                return $attachmentDetails;
            }
        }

        return $attachments;
    }

    //inline



    public function store($g, $stream, $category) {

        $lang = App::getLocale();
        $post_type = 'post';
        $s3_url = 'https://dotemirates.s3-eu-west-1.amazonaws.com/';
        $uploads = getcwd() . DIRECTORY_SEPARATOR . "uploads" . DIRECTORY_SEPARATOR;
        if (@$g->emails) {
            foreach (@$g->emails as $email_number) {
                $image_id = null;
                $images = array();
                $newemail = $g->mMessage["senderAddr"][$email_number];
                $at_pos = strpos($newemail, "@");
                $mailhost = substr($newemail, $at_pos);
                $mailhost_pos = strpos($mailhost, "dotmsr");
                if ($mailhost_pos !== false) {

                    $structure = imap_fetchstructure($stream, $email_number);
                    $msg = '';
                    //print_r($structure);
                    if (isset($structure) && count($structure->parts)) {
                        //echo '1'; die;
                        //$boundary_value = 'CHARSET';

                        foreach ($structure->parts as $part) {
                            //print_r($part); die;
                            if ($part->parameters[0]->attribute == 'BOUNDARY') {
                                $boundary_value = $part->parameters[0]->value;
                            }
                        }
                        //echo $boundary_value; die;
                        //$first_section = imap_fetchbody($stream,$email_number,1);

                        $first_section = imap_fetchbody($stream, $email_number, '1.1');
                        if ($first_section == "") {
                            $first_section = imap_fetchbody($stream, $email_number, '1');
                        }

                        //echo base64_decode($first_section); die;
                        // echo $first_section = trim(substr(quoted_printable_decode($first_section), 0, 100)); die;

                        /* $body = imap_qprint($first_section);
                          $charset = 'UTF-8';
                          if(!empty($part->parameters)){
                          for ($k = 0, $l = count($part->parameters); $k < $l; $k++) {
                          $attribute = $part->parameters[$k];
                          if($attribute->attribute == 'CHARSET'){
                          $charset = $attribute->value;
                          }
                          }
                          }
                          //echo $charset;
                          $first_section = mb_convert_encoding($body,'UTF-8',$charset); */

                        $first_section = quoted_printable_decode($first_section);
                        $end = strrpos($first_section, "quoted-printable");
                        $first_section = substr_replace($first_section, '', 0, $end);
                        $first_section = str_replace("quoted-printable", "", $first_section);
                        /* $wierd_str = 'div dir="ltr">';
                          $wierd_pos = strrpos($first_section, $wierd_str);
                          if($wierd_pos !== false){
                          $first_section= str_replace($wierd_str,"",$first_section);
                          } */

                        $first_section = str_replace($boundary_value, "", $first_section);
                        $first_section = str_replace('Content-Type: text/html; charset=UTF-8', '', $first_section);
                        $first_section = str_replace('Content-Type: text/plain; charset=UTF-8', '', $first_section);
                        $first_section = str_replace('--', '', $first_section);
                        $first_section = str_replace('---', '', $first_section);
                        //$first_section = base64_decode($first_section);
                        // hena
                        /* $end2 = strpos($first_section, $boundary_value);
                          $end_pos = strrpos($first_section,"UTF-8");
                          $first_section= substr_replace($first_section,'',$end2, $end_pos+5); */

                        /* $subject = $first_section;
                          $pattern = '/^--Content-Type: text/plain(;) charset=UTF-8/';
                          preg_match($pattern, $subject, $matches);
                          print_r($matches);
                          die; */
                        $attachments = $this->getAttachments($stream, $email_number, $structure, "");
                        $inlines = $this->getInlines($stream, $email_number, $structure, "");
                        if (count($attachments) && count($inlines)) {
                            //$end22 = strpos($first_section, "--0");
                            //$first_section= substr_replace($first_section,'',$end22);
                            $first_section = strip_tags($first_section);
                            $pattern = '/001([a-zA-Z0-9]+)/';
                            $replacement = '';
                            $first_section = preg_replace($pattern, $replacement, $first_section);
                        }
                        //print_r(strip_tags($first_section));

                        if (count($attachments)) {
                            foreach ($g->mMessage["filename"][$email_number] as $file) {
                                $images[] = $file;
                            }
                            //print_r($g->mMessage);
                            /* foreach($g->mMessage["filename"][$email_number] as $file){
                              $basefile = end(explode("/", $file));
                              $extension = explode('.', $basefile);
                              $extension = $extension[count($extension) - 1];
                              $images[] = $filename = time() * rand() . "." . strtolower($extension);
                              rename("$uploads$basefile", "$uploads$filename");
                              $s3_path = date('Y') . '/' . date('m') . '/' . $filename;
                              $first_section .="<img src='".$s3_url.$s3_path."' /><br/><br/>";
                              } */
                            if (count($inlines)) {
                                $inline_images = array();
                                //print_r($inlines);
                                foreach ($inlines as $inline) {
                                    foreach ($inline as $inline1) {
                                        //$inline_images[] = "<img src='data:image/jpg;base64,".rtrim($inline1["img"],"=")."'/>";
                                        $images[] = $file_path = $inline1["name"];

                                        //$images[] = basename($file_path);
                                        $decoded_data = base64_decode(rtrim($inline1["img"], "="));
                                        file_put_contents("$uploads$file_path", $decoded_data);
                                        //$first_section = str_replace($inline1["img"],"", $first_section);
                                    }
                                }
                                /* $inline_images=array_reverse($inline_images);
                                  for($im=0; $im<count($inline_images); $im++){
                                  $html = SunraPhpSimpleHtmlDomParser::str_get_html($first_section);
                                  if(is_object($html)){
                                  if ($html->find('img', $im)) {
                                  $html->find('img', $im)->outertext = $inline_images[$im];
                                  }
                                  }
                                  $first_section .= $html->outertext;
                                  } */
                            }
                            // echo $first_section;
                        } else {

                            /*
                              $first_section = imap_fetchbody($stream,$email_number,1);

                              $first_section = quoted_printable_decode($first_section);
                              $end = strrpos($first_section, "quoted-printable");
                              $first_section= substr_replace($first_section,'',0,$end);
                              $first_section= str_replace("quoted-printable","",$first_section);
                              $first_section= str_replace($boundary_value,"",$first_section);
                              //$end2 = strpos($first_section, $boundary_value);
                              //$first_section= substr_replace($first_section,'',$end2); */
                            $first_section = str_replace('Content-Type: text/html; charset=UTF-8', '', $first_section);
                            $first_section = str_replace('Content-Type: text/plain; charset=UTF-8', '', $first_section);
                            $first_section = str_replace('--', '', $first_section);
                            $first_section = str_replace('---', '', $first_section);

                            $pattern = '/([a-zA-Z0-9]*)[image: Inline image ([0-9])*]/';
                            $replacement = '';
                            $first_section = preg_replace($pattern, $replacement, $first_section);
                            $i = 0;
                            foreach ($structure->parts as $section => $part) {
                                $message = imap_fetchbody($stream, $email_number, $section + 1);
                                if ($part->encoding == 1) {
                                    $msg.= imap_8bit($message);
                                    $first_section = base64_decode($first_section);
                                } elseif ($part->encoding == 2) {
                                    $msg.= imap_binary($message);
                                    $first_section = base64_decode($first_section);
                                } elseif ($part->encoding == 3 && $part->type == 5) { //&& $part->disposition == "INLINE"
                                    $message = imap_mime_header_decode($message);

                                    $images[] = $file_path = $part->dparameters[0]->value;

                                    //$images[] = basename($file_path);
                                    $decoded_data = base64_decode($message[0]->text);
                                    file_put_contents("$uploads$file_path", $decoded_data);

                                    //$im='<img src="data:image/jpg;base64,'.$message[0]->text.'"/>';
                                    //echo $im='<img src="'.$s3_url.$s3_path.'"/>';

                                    /* $html = SunraPhpSimpleHtmlDomParser::str_get_html($first_section);
                                      if(is_object($html)){
                                      if ($html->find('img', $i)) {
                                      $html->find('img', $i)->outertext = $im;
                                      }
                                      $first_section = $html->outertext;
                                      } */

                                    $i++;
                                } elseif ($part->encoding == 4) {
                                    $msg.= imap_qprint($message);
                                    $first_section = base64_decode($first_section);
                                } elseif ($part->encoding == 5) {
                                    $msg.= $message;
                                    $first_section = base64_decode($first_section);
                                }
                            }
                        }
                    } else {

                        $first_section = getMaildetails($email_number);
                    }

//                echo $first_section."<br/><br/><br/><br/><br/>";

                    $user_email = $g->mMessage["senderAddr"][$email_number];

                    $old_editor = User::where('role_id', '=', 17)
                            ->where('users.email', '=', $user_email)
                            ->select('id')
                            ->get();

                    if (count($old_editor) > 0) {
                        $user_id = $author_id = $old_editor[0]->id;
                    } else {
                        $handl_pos = strpos($user_email, '@');
                        $user_name = substr($user_email, 0, $handl_pos);
                        $row = array();
                        $row['username'] = $user_name;
                        $row['email'] = $user_email;
                        $row['first_name'] = $g->mMessage["senderName"][$email_number];
                        $row['created_at'] = $g->mMessage["maildate"][$email_number];
                        $row['role_id'] = 17;
                        $row['status'] = 1;
                        $user_id = $author_id = DB::table('users')->insertGetId($row);
                    }
                    /////
                    //print_r($first_section);
                    /* if(!count($images)){
                      if (preg_match('/[^00-255]+/u', base64_decode($first_section))) // '/[^a-zd]/i' should also work.
                      {
                      $first_section = base64_decode($first_section);
                      }

                      } */

                    $cat = $category;
                    $post_title = $g->mMessage["subject"][$email_number];
                    $post_slug = create_slug($post_title);
                    $post_status = 0;
                    $post_submitted = 1;
                    $post_created_date = $created_date = date("Y-m-d H:i:s")/* $g->mMessage["maildate"][$email_number] */;

                    //if(!count($images)){
                    if (preg_match('/[^00-255]+/u', base64_decode($first_section))) { // '/[^a-zd]/i' should also work.
                        $first_section = base64_decode($first_section);
                    }
                    //}
                    //print_r($g->mMessage);
                    $pattern = '/([a-zA-Z0-9]*)[image: Inline image ([0-9])*]/';
                    $replacement = '';
                    $first_section = preg_replace($pattern, $replacement, $first_section);

                    $pattern = '/Inline image ([0-9])*]/';
                    $replacement = '';
                    $first_section = preg_replace($pattern, $replacement, $first_section);

                    $pattern = '/[image/';
                    $replacement = '';
                    $first_section = preg_replace($pattern, $replacement, $first_section);

                    foreach ($images as $file) {
                        //echo $file;
                        $basefile = end(explode("/", $file));
                        $extension = explode('.', $basefile);
                        $extension = $extension[count($extension) - 1];
                        $file = time() * rand() . "." . strtolower($extension);
                        rename("$uploads$basefile", "$uploads$file");
                        $s3_path = date('Y') . '/' . date('m') . '/' . $file;

                        $media_hash = sha1_file("uploads/" . $file);
                        $s3_path = date('Y') . '/' . date('m') . '/' . $file;
                        $found = DB::table('media')->where('media_hash', '=', $media_hash)->first();
                        if (!$found) {
                            //set_sizes($file);
                            $image_id = Media::add_media(['media_provider' => null, 'media_provider_id' => null, 'media_provider_image' => null,
                                        'media_path' => $s3_path, 'media_type' => 'image', 'media_title' => $post_title, 'media_description' => null,
                                        'media_created_date' => $created_date, 'media_duration' => 0, 'media_hash' => $media_hash, 'conn' => 1]);
                            s3_save($s3_path);

                            set_sizes($file);
                        } else {
                            $image_id = $found->media_id;
                            $s3_path = $found->media_path;
                        }

                        unlink("$uploads$file");

                        $first_section .="<img src='" . $s3_url . $s3_path . "' /><br/><br/>";
                    }

                    $post_content = $first_section;


                    /* foreach($g->mMessage["filename"][$email_number] as $file){
                      //echo $file;
                      $media_hash = sha1_file($file);
                      $found = DB::table('media')->where('media_hash', '=', $media_hash)->first();
                      if (!$found) {
                      set_sizes($file);
                      $image_id = Media::add_media(['media_provider' => null, 'media_provider_id' => null, 'media_provider_image' => null,
                      'media_path' => basename($file) , 'media_type' => 'image', 'media_title' => $post_title, 'media_description' => null,
                      'media_created_date' => $created_date, 'media_duration' => 0, 'media_hash' => $media_hash, 'conn' => 1]);
                      }else{
                      $image_id = $found->media_id;
                      }
                      } */
                    //print_r($images);
                    // insert post
                    // $post_id = Post::add_post(['post_created_date' => $post_created_date, 'post_user_id' => $user_id,
                    //             'post_author_id' => $author_id, 'post_type' => $post_type, 'post_slug' => $post_slug, 
                    //             'post_status' => $post_status, 'lang' => $lang, 'post_title' => $post_title, 'post_format'=>'1',
                    //             'post_content' => $post_content,'post_subtitle' => '', 'post_excerpt' => '',
                    //             'meta_title' => '', 'meta_keywords' => '', 'meta_description' => '', 'conn' =>1]);
                    //form adnan

                    $post_id = Post::add_post(['post_created_date' => $post_created_date, 'post_updated_date' => $post_created_date, 'post_user_id' => $user_id, 'has_media' => null,
                                'post_author_id' => $author_id, 'post_image_id' => $image_id, 'post_media_id' => null, 'image_hide' => 0,
                                'post_type' => $post_type, 'post_format' => '1', 'post_slug' => $post_slug, 'post_status' => $post_status,
                                'post_desked' => 0, 'post_submitted' => 0, 'post_closed' => 0, 'translated' => 0, 'youtube_link' => null, 'lang' => $lang, 'post_title' => $post_title,
                                'post_subtitle' => null, 'post_excerpt' => null, 'comment_options' => 1, 'post_editor_id' => $user_id,
                                'post_content' => $post_content, 'meta_title' => null, 'meta_keywords' => null, 'meta_description' => null,
                                'facebook_title' => null, 'facebook_description' => null, 'facebook_image' => null,
                                'twitter_description' => null, 'conn' => 1, 'from_conn' => null, 'from_cat' => null, 'from_slug' => null]);

                    //from adnan



                    Category::add_post_cats(['post_id' => $post_id, 'cat_id' => $cat, 'conn' => 1]);
                }
            }
//            return Redirect::to('admin/posts/' . $post_id . '/edit?cat_id=' . $cat);
        }
    }

}
