<?php

return array(
    "title" => "الإيميلات",
    "add" => "إضافة إيميل",
    "email" => "إيميل",
    "password" => "كلمة السر",
    "category" => "التصنيف",
    "del" => "حذف",
    "edit" => "تعديل",
    "sure_delete" => "حذف الإيميل",
    "search_emails" => "بحث الإيميلات",
    "done" => "تم",
    "search" => "بحث..."
);
