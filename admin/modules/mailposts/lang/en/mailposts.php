<?php

return array(
    "title" => "Emails",
    "add" => "Add email",
    "email" => "email",
    "password" => "password",
    "category" => "category",
    "del" => "delete",
    "edit" => "edit",
    "sure_delete" => "delete email?",
    "search_emails" => "search emails",
    "done" => "Done",
    "search" => "search..."
);
