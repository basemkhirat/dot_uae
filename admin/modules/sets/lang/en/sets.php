<?php

return [
    'home' => 'Home',
    'sets' => 'Sets',
    'new' => 'Add New',
    'search' => 'Search...',
    'all' => 'All',
    'edit' => 'Edit',
    'bulk' => 'Bulk Actions',
    'delete' => 'Delete Group',
    'delete_set_q' => 'Are you sure to delete set?',
    'apply' => 'Apply',
    'per_page' => 'Per Page',
    'name' => 'Name',
    'author' => 'Author',
    'last_updated' => 'Last Updated',
    'no_sets' => 'No sets found!',
    'showing' => 'Showing',
    'to' => 'to',
    'of' => 'of',
    'sets' => 'sets',
    'set' => 'sets',
    'edit_set' => 'Edit Group',
    /* set page */
    'new_set' => 'Add New Group',
    'well_done' => 'Well done!',
    'success_update' => 'You successfully update this set.',
    'oh_snap' => 'Oh snap!',
    'required' => 'This field is required.',
    'enter_name' => 'Enter name here',
    'related_pages' => 'Related Pages',
    'add' => 'Add!',
    'save' => 'Save',
    'updated_on' => 'Updated On',
    "module" => "Sets",
    "permissions" => [
        "create" => "Create Sets",
        "edit" => "Edit Sets",
        "delete" => "Delete Sets"
    ]
];
