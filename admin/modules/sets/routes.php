<?php

Route::group(array(
    "prefix" => ADMIN,
    "before" => "auth",
        ), function($route) {
    Route::resource('sets', 'SetController');
    $route->group(array("prefix" => "sets"), function($route) {
        $route->post('delete', 'SetController@destroy');
        $route->get('{id}/delete', 'SetController@destroy');
    });
});
