<?php

class SetController extends BackendController {

    public $data = array();
    public $conn = '';

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function __construct() {
        date_default_timezone_set("Etc/GMT-2");

        $this->conn = Session::get('conn');
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index() {

        $user_id = Input::get('user_id');
        $q = trim(Input::get('q'));
        $per_page = (Input::has('per_page')) ? Input::get('per_page') : 20;

        $this->data['sets'] = Set::filter(['user_id' => $user_id, 'q' => $q])->orderBy('created_on', 'DESC')->paginate($per_page);

        return View::make('sets::sets')->with($this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create() {
        $this->data['set'] = new Set();
        return View::make('sets::set')->with($this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store() {

        // validate the info, create rules for the inputs
        $rules = array(
            'group_name' => 'required',
        );

        // run the validation rules on the inputs from the form
        $validator = Validator::make(Input::all(), $rules);

        $validator->setCustomMessages(Lang::get('admin::validation'));

        // if the validator fails, redirect back to the form
        if ($validator->fails()) {
            return Redirect::back()
                            ->withErrors($validator) // send back all errors to the login form
                            ->withInput(); // send back the input (not the password) so that we can repopulate the form
        } else {
            $data = Input::all();

            // insert set
            $set = new Set;
            $set->fill($data);
            $set->save();
            $set_id = $set->related_set_id;

            // pivot table
            $related_posts = explode(',', Input::get('related-posts'));
            array_pop($related_posts);
            $set->posts()->sync($related_posts);

            return Redirect::to(ADMIN . '/sets/' . $set_id . '/edit');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id) {

        $set = Set::with('posts')->find($id);
        if (!$set) {
            return Redirect::to(ADMIN . '/setss');
        }

        $this->data['set'] = $set;
        return View::make('sets::set')->with($this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id) {

        $rules = array(
            'group_name' => 'required',
        );

        // run the validation rules on the inputs from the form
        $validator = Validator::make(Input::all(), $rules);

        $validator->setCustomMessages(Lang::get('admin::validation'));

        // if the validator fails, redirect back to the form
        if ($validator->fails()) {
            return Redirect::back()
                            ->withErrors($validator) // send back all errors to the login form
                            ->withInput(); // send back the input (not the password) so that we can repopulate the form
        } else {
            $data = Input::all();

            // insert set
            $set = Set::find($id);
            $set->fill($data);
            $set->save();

            // pivot table
            $related_posts = explode(',', Input::get('related-posts'));
            array_pop($related_posts);
            $set->posts()->sync($related_posts);
        }
        Session::flash('message', 'success');
        return Redirect::back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id = 0) {
        $checks = Input::get('check');
        if ($id) {
            Set::destroy($id);
        } else {
            Set::destroy($checks);
        }
        return Redirect::back();
    }

}
