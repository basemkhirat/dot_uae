<?php

class Set extends Model {

    protected $table = 'related_groups';
    protected $primaryKey = 'related_group_id';
    protected $fillable = [ 'group_name', 'group_slug', 'created_on', 'user_id'];

    const CREATED_AT = 'created_on';

    public function posts() {
        return $this->belongsToMany('Post', 'posts_related_groups', 'post_group_id', 'post_id');
    }

    public function user() {
        return $this->hasOne('User', 'id', 'user_id');
    }

    public function setUpdatedAt($value) {
        // Do nothing.
    }

    public function getUpdatedAtColumn() {
        return null;
    }

    /**
     * Filter
     */
    public function scopefilter($query, $args = []) {

        if (isset($args['user_id'])) {
            $query->where('user_id', '=', $args['user_id']);
        }

        if (isset($args['q']))
            $query->where('group_name', 'LIKE', '%' . $args['q'] . '%');

        return $query;
    }

    public function setGroupSlugAttribute($value) {
        $slug = create_slug($value);
        $isUnique = self::where('group_slug', '=', $slug)->first();
        while ($isUnique !== NULL) {
            $slug = $slug . '-' . rand(0, 99999);
            $isUnique = self::where('group_slug', '=', $slug)->first();
        }
        $this->attributes['group_slug'] = $slug;
    }

    public static function boot() {
        parent::boot();
        Set::creating(function($set) {
            $set->attributes['user_id'] = Auth::user()->id;
            $set->attributes['group_slug'] = $set->group_name;
        });
        Set::deleting(function($set) {
            $set->posts()->detach();
        });
    }

}
