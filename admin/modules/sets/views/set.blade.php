@extends("admin::layouts.master")

@section("breadcrumb")
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2><i class="fa fa-inbox faa-tada animated faa-slow"></i> {!!Lang::get('sets::sets.sets')!!}</h2>
        <ol class="breadcrumb" style="background: none;">
            <li><a  href="{!!URL::to('/'.ADMIN.'/dashboard')!!}">{!!Lang::get('admin::common.dashboard')!!}</a></li>
            <li><a  href="{!!URL::to('/'.ADMIN.'/sets')!!}">{!!Lang::get('sets::sets.sets')!!}</a></li>
            @if(isset($set->related_group_id))
            <li>{!!Lang::get('sets::sets.edit_set')!!} </li>
            @else
            <li>{!!Lang::get('sets::sets.new_set')!!} </li>
            @endif
        </ol>
    </div>
    @if(User::can('sets.create'))
    @if(isset($set->related_group_id))
    <div class="col-lg-2">
        <a class="btn btn-primary btn-labeled btn-main pull-right" href="{!!URL::to('/'.ADMIN.'/sets/create')!!}"> <span class="btn-label icon fa fa-plus"></span> {!!Lang::get('sets::sets.new_set')!!}</a> 
    </div>
    @endif
    @endif
</div>
@stop
@section("content")

<style>
    #related-search, #auto-wrap{
        z-index: 10000;          
        border: 1px solid #E7EAEC;
        position: absolute;
        top: 34px;
        display: block;
        background-color: #fff;
        width: 100%;
        padding: 10px;
    }
</style>

@if ($errors->has())
<div class="alert alert-danger alert-dark">
    <button type="button" class="close" data-dismiss="alert">×</button>
    @foreach ($errors->all() as $error)
    <strong>{!!Lang::get('sets::sets.oh_snap')!!}</strong> {!! $error !!}
    @endforeach
</div>
@endif

@if(Session::has('message'))
<div class="alert alert-success alert-dark">
    <button type="button" class="close" data-dismiss="alert">×</button>
    <strong>{!!Lang::get('sets::sets.well_done')!!}</strong> {!!Lang::get('sets::sets.success_update')!!}
</div>
@endif

<div id="content-wrapper">



    <div class="row wrapper animated fadeInRight ">

        <div id="content-wrapper">

            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>{!!Lang::get('sets::sets.sets')!!}</h5>
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>

                                <a class="close-link">
                                    <i class="fa fa-times"></i>
                                </a>
                            </div>
                        </div>
                        <div class="ibox-content">
                            @if(isset($set->related_group_id))
                            {!! Form::open(array('url' => ADMIN.'/sets/'.$set->related_group_id, 'class' => 'form-horizontal', 'method' => 'put', 'files' => true, 'id' => 'form', 'novalidate' => 'novalidate')) !!}
                            @else
                            {!! Form::open(array('url' => ADMIN.'/sets', 'files' => true, 'class' => 'form-horizontal', 'id' => 'form', 'novalidate' => 'novalidate')) !!}
                            @endif
                            <div class="form-group"><label class="col-sm-2 control-label">{!!Lang::get('sets::sets.name')!!}</label>

                                <div class="col-sm-10">
                                    {!! Form::text('group_name', Input::old('group_name', $set->group_name), array('class' => 'form-control input-lg', 'id' => 'group_name', 'placeholder' => Lang::get('sets::sets.enter_name'))) !!}
                                    @if(isset($group_id))
                                    <div style="margin-top: 5px;">
                                        <i class="fa fa-edit"></i>
                                        <small class="text-muted">{!!Lang::get('sets::sets.updated_on')!!}: </small>
                                        <b>@if(DIRECTION == 'rtl') {!!arabic_date($set->created_on)!!} @else {!! date('M d, Y', strtotime($set->created_on)) !!} @ {!! date('h:i a', strtotime($set->created_on)) !!} @endif</b>
                                    </div>
                                    @endif
                                </div>
                            </div>

                            <div class="hr-line-dashed"></div>
                            <div class="form-group"><label class="col-sm-2 control-label">{!!Lang::get('posts::posts.related_posts')!!}</label>

                                <div class="col-sm-10" style="padding: 0 30px">
                                    <?php
                                    $post_related = null;
                                    $count = count($set->posts);
                                    if ($count) {
                                        foreach ($set->posts as $post) {
                                            $post_related .= $post->post_id . ",";
                                        }
                                    }
                                    ?>
                                    <input type="hidden" id="related-temp">
                                    <input type="hidden" id="related-posts" value="{!!$post_related!!}" name="related-posts">
                                    <div class="form-group" style="position:relative">
                                        <div class="input-group">
                                            <input type="text" class="form-control" id="related-text" @if($count == 10) disabled="disabled" @endif style="background-color: snow;" placeholder="{!!Lang::get('posts::posts.search_post')!!}" autocomplete="off">
                                                   <span class="input-group-btn">
                                                <button class="btn btn-primary disabled" type="button" id="add-related-post">{!!Lang::get('posts::posts.add')!!}</button>
                                            </span>
                                        </div>
                                        <div id="auto-wrap" style="height:101px; display: none">
                                            <div class="feed-activity-list full-height-scroll" style="padding:0 10px"></div>
                                        </div>
                                        <div id="related-search" style="display: none">
                                            <span style="font-style: italic;" >{!!Lang::get('posts::posts.more_character')!!}</span>
                                        </div>
                                    </div>
                                    <!--                                    <div class="task" style="position: relative">
                                                                            <div class="input-group">
                                    
                                                                                <input type="text" class="form-control" id="related-text" @if(count($set->posts) == 10) disabled="disabled" @endif style="background-color: snow;" placeholder="{!!Lang::get('posts::posts.search_post')!!}" autocomplete="off">
                                                                                       <span class="input-group-btn">
                                                                                    <button class="btn btn-primary disabled" type="button" id="add-related-post">{!!Lang::get('posts::posts.add')!!}</button>
                                                                                </span>
                                                                            </div>
                                                                            <input type="hidden" id="related-temp">
                                                                            <input type="hidden" id="related-posts" value="{!!$post_related!!}" name="related-posts">
                                    
                                                                            <div class="input-group form-control" style="position:absolute; z-index: 10000; display:none; top:34px; height: auto; max-height:200px; overflow-y:auto" id="auto-wrap">
                                                                                <div class="item"></div>
                                                                                <span style="font-style: italic;" id="auto-search">{!!Lang::get('posts::posts.more_character')!!}</span>
                                                                            </div>
                                                                        </div>-->
                                    <div style="height:300px; @if(!count($set->posts)) display: none; @endif" id="related-add">
                                        <ul class="todo-list small-list full-height-scroll" style="padding: 0 10px">

                                            @if(count($set->posts))

                                            @foreach($set->posts as $post)

                                            <li class="feed-element" style="margin-top:5px">
                                                <a class="check-link" href="#"><i data-id="{!!$post->post_id!!}" class="fa fa-square-o"></i> </a>
                                                <span class="m-l-xs">{!!$post->post_title!!}</span>
                                                <a class="label label-danger pull-right delete-related-post" href="#{!!$post->post_id!!}"><i class="fa fa-times"></i></a>
                                            </li>

                                            @endforeach

                                            @endif
                                        </ul>
                                    </div>
                                    <div class="panel-footer clearfix" style="border-top: none; background: none; @if(!count($set->posts)) display: none; @endif">
                                        <div class="pull-right">
                                            <a href="#" class="btn btn-xs" id="clear-completed-tasks"><i class="fa fa-eraser text-success"></i> {!!Lang::get('posts::posts.clear_posts')!!}</a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="hr-line-dashed"></div>



                            <div class="form-group">
                                <div class="col-sm-4 col-sm-offset-2">

                                    {!! Form::button(Lang::get('sets::sets.save'), array('type' => 'submit', 'class' => 'btn btn-primary')) !!}
                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div>


</div>
</div> <!-- / #content-wrapper -->

<script src="<?php echo assets() ?>/js/plugins/validate/jquery.validate.min.js"></script>

<script>
    var baseURL = '{!! URL::to("/".ADMIN) !!}/';
    var more_character_lang = "{!!Lang::get('posts::posts.more_character')!!}";
    var no_results_lang = "{!!Lang::get('posts::posts.no_results')!!}";
    var notfound_lang = "{!!Lang::get('posts::posts.notfound')!!}";
    var search_lang = "{!!Lang::get('posts::posts.search')!!}";
    $(document).ready(function () {
        $('#related-text').on('input', function () {
            if ($(this).val() == '' || $(this).val().length <= 2) {
                $('#auto-wrap').hide();
                $("#related-search span").html(more_character_lang);
                $("#related-search").show();
                //$("#auto-wrap .item").html('');
            } else {

                ajaxData = {
                    q: $(this).val(),
                };
                $.ajax({
                    type: "POST",
                    dataType: 'json',
                    url: baseURL + "posts/autocomplete",
                    data: ajaxData,
                    beforeSend: function (res) {
//                    $('#auto-wrap').show();
//                    $("#auto-wrap .item").html('');
//                    $("#related-search").html(search_lang);

                        $("#related-search span").html(search_lang);
                        $("#auto-wrap .feed-activity-list").html('');

                    },
                    success: function (res) {
                        //$("#auto-wrap .item").html('');
                        if (res != 'notfound') {
                            $.map(res, function (n, i) {
                                $("#auto-wrap .feed-activity-list").prepend('<div style="margin-top:5px; padding-bottom:5px" class="feed-element"><a href="#' + n.post_id + '" class="related-one task-title" >' + n.post_title + '</a></div>');
                            });
                            $('#auto-wrap').show();
                            $("#related-search").hide();
                        } else {
                            //$("#auto-wrap .item").html(no_results_lang);
                            $('#auto-wrap').hide();
                            $("#related-search").show();
                            $("#related-search span").html(notfound_lang);
                        }
                    },
                    complete: function () {
                        //$("#related-search").html('');
                        scroll_height('auto-wrap', 300, false, true);
                    }
                });
            }
        });

        $('body').on('click', function () {
            $('#auto-wrap').hide();
            $('#auto-search').hide();
        });

        $('#auto-wrap').on('click', '.related-one', function (e) {
            e.preventDefault();
            $('#related-text').val($(this).html());
            $('#related-temp').val($(this).attr('href').substr(1));
            $('#auto-wrap').hide();
            $('#add-related-post').removeClass('disabled');
        });

        $('.todo-list').on('click', '.delete-related-post', function (e) {
            e.preventDefault();
            delete_post = $(this).attr('href').substr(1);
            str = $("#related-posts").val().replace(delete_post + ",", "");
            $("#related-posts").val(str);
            $(this).parent().remove();

            posts = $("#related-posts").val().split(",");

            if (posts.length - 1 == 10) {
                $('#related-text').prop("disabled", true);
            } else {
                $('#related-text').prop("disabled", false);
            }

            scroll_height('related-add', 300, true);
        });

        $('.todo-list').on('click', '.check-link', function (e) {
            e.preventDefault();
            if ($(this).find('.fa').hasClass('fa-square-o')) {
                $(this).find('.fa').removeClass('fa-square-o').addClass('fa-check-square');
                $(this).next('span').addClass('todo-completed');
            } else {
                $(this).find('.fa').removeClass('fa-check-square').addClass('fa-square-o');
                $(this).next('span').removeClass('todo-completed');
            }

        });

        $('#add-related-post').click(function (e) {
            e.preventDefault();

            posts = $("#related-posts").val().split(",");
            new_post = $('#related-temp').val();
            found = $.inArray(new_post, posts) > -1;

            if (!found) {

                $("#related-posts").val(new_post + ',' + $("#related-posts").val());

                //$(".select2-primary").show();
                $(".todo-list").prepend('<li class="feed-element" style="margin-top:5px"><a class="check-link" href="#"><i data-id="' + new_post + '" class="fa fa-square-o"></i> </a><span class="m-l-xs">' + $('#related-text').val() + '</span><a class="label label-danger pull-right delete-related-post" href="#' + new_post + '"><i class="fa fa-times"></i></a></li>');
            } else {
                $("#related-text").trigger("focus");

            }

            posts = $("#related-posts").val().split(",");

            if (posts.length - 1 == 10) {
                $('#related-text').prop("disabled", true);
            }

            $(this).addClass('disabled');
            $('#related-text').val('');
            $('.todo-list').show();
            $('.panel-footer').show();
            $('#related-add').show();

            scroll_height('related-add', 300);

        });

        $('#clear-completed-tasks').click(function (e) {
            e.preventDefault();

            $('.todo-list li').find('.check-link').find('.fa').each(function () {
                if ($(this).hasClass("fa-check-square")) {
                    delete_post = $(this).attr('data-id');
                    str = $("#related-posts").val().replace(delete_post + ",", "");
                    $("#related-posts").val(str);

                    posts = $("#related-posts").val().split(",");

                    if (posts.length - 1 == 10) {
                        $('#related-text').prop("disabled", true);
                    } else {
                        $('#related-text').prop("disabled", false);
                    }
                    $(this).parents('li').remove();
                    scroll_height('related-add', 300, true);
                }
            });


        });

        scroll_height('related-add', 300);
        function scroll_height(id, max, footer, auto) {
            height = 0;
            //console.log(max);
            $("#" + id + " .full-height-scroll").find('.feed-element').each(function () {
                height += $(this).outerHeight();
                //console.log(height);
            });
            //console.log(height);
            if (height >= max) {
                $("#" + id).css('height', max);
            } else if (height == 0) {
                $("#" + id).hide();
                if (footer) {
                    $("#" + id).next('.panel-footer').hide();
                }
            } else {
                if (auto) {
                    $("#" + id).css('height', 'auto');
                } else {
                    $("#" + id).css('height', height + 10);
                }
            }
        }

        $("#form").validate({
            rules: {
                group_name: {
                    required: true
                }
            }
        });
    });

    jQuery.extend(jQuery.validator.messages, {
        required: "{!!Lang::get('sets::sets.required')!!}",
    });
</script>

@stop