<?php

Menu::set("sidebar", function($menu) {
    if (User::can('sets')) {
        $menu->item('news_options.groups', trans("admin::common.related_groups"), URL::to(ADMIN . '/sets'))
                ->order(4)
                ->icon("fa-inbox");
    }
});

include __DIR__ . "/routes.php";
