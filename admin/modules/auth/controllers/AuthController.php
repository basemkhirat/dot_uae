<?php

class AuthController extends BackendController {

    public $data = array();

    public function login() {

        if (Request::isMethod("post")) {

            $rules = array(
                'username' => 'required',
                'password' => 'required'
            );

            $validator = Validator::make(Input::all(), $rules);

            if ($validator->fails()) {
                return Redirect::back()
                                ->withErrors($validator)
                                ->withInput(Input::except('password'));
            } else {

                $userdata = array(
                    'username' => Input::get('username'),
                    'password' => Input::get('password'),
                    "status" => 1
                );

                if (Auth::attempt($userdata, Input::get("remember"))) {

                    // user photo
                    $user_photo = assets("images/user.png");
                    if (Auth::user()->photo) {
                        $photo = Media::where("media_id", Auth::user()->photo)->first();
                        if (count($photo)) {
                            $user_photo = thumbnail($photo->media_path);
                        }
                    }

                    Session::put("user_data", array(
                        "lang" => Auth::user()->lang,
                        "photo" => $user_photo,
                        "role" => Role::where("role_id", Auth::user()->role_id)->pluck("role_slug")
                    ));

                    if (Input::has("url")) {
                        Session::forget('url');
                        return Redirect::to(Input::get("url"));
                    } else {
                        return Redirect::route("dashboard.show");
                    }
                } else {
                    return Redirect::route('auth.login')
                                    ->withErrors(array("message" => trans("auth::auth.invalid_login")))
                                    ->withInput(Input::except('password'));
                }
            }
        }

        return View::make("auth::login");
    }

    public function logout() {
        Auth::logout();
        Session::forget('permissions');
        return Redirect::route("auth.login");
    }

    public function forget() {

        if (Request::isMethod("post")) {

            $rules = array(
                'email' => 'required|email'
            );

            $validator = Validator::make(Input::all(), $rules);

            if ($validator->fails()) {
                return Redirect::back()
                                ->withErrors($validator)
                                ->withInput(Input::all());
            } else {

                $email = Input::get("email");

                // Send activation link to user
                // check user is already exists
                $user = User::where("email", $email)->first();

                if (count($user)) {

                    $code = Str::random(30);
                    $link = URL::route("auth.reset") . "/" . $code;

                    $headers = 'From: "'.Config::get("site_name").'" <' . Config::get("site_email") . '>';

                    $content = trans("auth::auth.hi") . " " . $user->first_name . ", \r\n" . trans("auth::auth.check_password_link") . "\r\n" . $link;
                    mail($user->email, trans("auth::auth.reset_password"), $content, $headers);

                    /*
                      Mail::send("auth::auth.forget_email", array("user" => $user, "link" => $link), function($message) use ($user) {
                      $message->from('info@dotmsr.com', 'دوت مصر')->to($user->email, $user->first_name)->subject(trans("auth::auth.reset_password"));
                      });
                     */

                    DB::table("users")
                            ->where("email", $email)
                            ->update(array(
                                "code" => $code
                    ));

                    return Redirect::back()
                                    ->withErrors(array("email_sent" => trans("auth::auth.password_reset_link_sent")))
                                    ->withInput(Input::all());
                } else {
                    return Redirect::back()
                                    ->withErrors(array("not_registed" => trans("auth::auth.email_not_found")))
                                    ->withInput(Input::all());
                }
            }
        }

        return View::make("auth::forget");
    }

    public function reset($code = false, $reseted = false) {

        $this->data["reseted"] = $reseted;

        if ($reseted) {
            return View::make("auth::reset", $this->data);
        }

        if (Input::has("code")) {
            $code = Input::get("code");
        }

        $this->data["code"] = $code;

        $user = User::where("code", $code)->first();

        if (count($user) == 0) {
            return "Forbidden";
        }

        if (Request::isMethod("post")) {

            $rules = array(
                'password' => 'required|min:7',
                /* 'password' => 'required|min:12|alpha_num', */
                'repassword' => 'required|same:password',
            );

            $validator = Validator::make(Input::all(), $rules, [
                        'password.has' => 'كلمة المرور يجب أن تحتوى على حروف كبيرة وحروف صغيرة وأرقام وحروف خاصة'
            ]);

            if ($validator->fails()) {
                return Redirect::route("auth.reset", array("code" => $code))
                                ->withErrors($validator)
                                ->withInput(Input::all());
            } else {

                // Reset user password
                DB::table("users")
                        ->where("id", "=", $user->id)
                        ->update(array(
                            "updated_at" => date("Y-m-d H:i:s"),
                            "code" => "",
                            "password" => Hash::make(Input::get("password"))
                ));

                return Redirect::to(ADMIN."/auth/reset/" . $code . "/1");
            }
        }

        return View::make("auth::reset", $this->data);
    }

}
