<?php

Route::group(array("prefix" => ADMIN . "/auth"), function($route) {
    $route->get('/login', array("as" => "auth.login", "before" => "guest", "uses" => "AuthController@login"));
    $route->post('/login', array("before" => "guest|csrf", "uses" => "AuthController@login"));
    $route->get('/forget', array("as" => "auth.forget", "uses" => "AuthController@forget"));
    $route->post('/forget', array("before" => "csrf", "uses" => "AuthController@forget"));
    $route->get('/reset/{code}/{reseted?}', array("as" => "auth.reset", "uses" => "AuthController@reset"));
    $route->post('/reset/{code}/{reseted?}', array("before" => "csrf", "uses" => "AuthController@reset"));
    $route->get('/logout', array("as" => "auth.logout", "uses" => "AuthController@logout"));
});
