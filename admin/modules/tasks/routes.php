<?php
Route::group(array(
    "prefix" => ADMIN,
    "before" => "auth",
        ), function($route) {
        $route->group(array("prefix" => "tasks"), function($route) {
            $route->any('/', array("as" => "tasks.show", "uses" => "TasksController@index"));
            $route->any('/create', array("as" => "tasks.create", "uses" => "TasksController@create"));
            $route->any('/{id}/edit', array("as" => "tasks.edit", "uses" => "TasksController@edit"));
            $route->any('/delete', array("as" => "tasks.delete", "uses" => "TasksController@delete"));
            $route->any('/{status}/status', array("as" => "tasks.status", "uses" => "TasksController@status"));
            $route->any('/{status}/done', array("as" => "tasks.done", "uses" => "TasksController@done"));
            
        });
});
