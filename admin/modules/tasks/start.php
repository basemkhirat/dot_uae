<?php



Menu::set("sidebar", function($menu) {
    if (User::can('tasks')) {
        $menu->item('site_options.tasks', trans("tasks::tasks.tasks"), route("tasks.show"))
                ->order(2)
                ->icon("fa-tasks");
    }
});









/*
use Illuminate\Console\Scheduling\Schedule;

$schedule = new Schedule();

$schedule->call(function () {
    \Task::update(array(
        "status" => 0
    ));
})->cron('* * * * *');
*/
include __DIR__ . "/routes.php";
