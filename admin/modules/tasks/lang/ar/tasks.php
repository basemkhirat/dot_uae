<?php

return [
    "module" => "قائمة المهام",
    'tasks' => 'قائمة المهام',
    'task' => 'مهمة',
    'add_new' => 'إضافة مهمة جديدة',
    'edit' => 'تعديل المهمة',
    'back_to_tasks' => 'العودة لقائمة المهام',
    'no_records' => 'لا توجد مهام',
    'save_task' => 'حفظ المهمة',
    'search' => 'بحث',
    'search_tasks' => 'البحث فى المهام',
    'per_page' => 'لكل صفحة',
    'bulk_actions' => 'إختر أمر',
    'delete' => 'حذف',
    'apply' => 'تطبيق',
    'page' => 'الصفحة',
    'of' => 'من',
    'order' => 'ترتيب',
    'sort_by' => 'ترتيب حسب',
    'asc' => 'تصاعدى',
    'desc' => 'تنازلى',
    'actions' => 'الحدث',
    'filter' => 'عرض',
    'task_status' => 'التفعيل',
    'activate' => 'تفعيل',
    'activated' => 'مفعل',
    'all' => 'الكل',
    'complete' => "حفظ كمهمة مكتملة",
    'uncomplete' => "حفظ كمهمة غير مكتملة",
    'completed' => "مهمة مكتملة",
    'uncompleted' => "مهمة غير مكتملة",
    'sure_complete' => "حفظ كمهمة مكتملة ?",
    'sure_uncomplete' => 'حفظ كمهمة غير مكتملة ?',
    'completed_by' => "تمت بواسطة",
    'at' => "فى ",
    'deactivate' => 'إلغاء التفعيل',
    'deactivated' => 'غير مفعل',
    'sure_activate' => "أنت على وشك تفعيل المهمة. إستمرار ?",
    'sure_deactivate' => "أنت على وشك إلغاء تفعيل المهمة. إستمرار ?",
    'sure_delete' => 'إنت على وشك الحذف. إستمرار ?',
    'to_user' => 'عضو',
    'to_group' => 'مجموعة',
    'user' => 'بواسطة',
    'tags' => 'الوسوم',
    'add_tag' => 'إضافة وسوم',
    'assigned_to' => 'موجهة إلى',
    'period' => 'الفترة الزمنية',
    'select_users' => 'إختر أعضاء',
    'select_groups' => 'إختر مجموعة أعضاء',
    'attributes' => [
        'title' => 'العنوان',
        'description' => 'الوصف',
        'start_date' => 'تاريخ البدء',
        'end_date' => 'تاريخ الإنتهاء',
        'to' => 'إلى',
        'done' => 'الحالة',
        'created_at' => 'تاريخ الإنشاء',
        'updated_at' => 'تاريخ التحديث',
        'status' => 'التفعيل'
    ],
    "events" => [
        'created' => 'تم إنشاء مهمة بنجاح',
        'updated' => 'تم تحديث المهمة بنجاح',
        'deleted' => 'تم حذف المهمة بنجاح',
        'activated' => 'تم تفعيل المهمة بنجاح',
        'deactivated' => 'تم إلغاء تفعيل المهمة بنجاح',
        "completed" => "تم إكمال المهمة بنجاح",
        "uncompleted" => "تم ظبط المهمة كمهمة غير مكتملة"
    ],
    "permissions" => [
        "create" => "إضافة مهمة جديدة",
        "edit" => "تعديل المهام",
        "delete" => "حذف المهام",
    ]
];
