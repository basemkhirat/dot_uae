<?php
return [
    "module" => "tasks",
    'tasks' => 'tasks',
    'task' => 'task',
    'add_new' => 'Add new task',
    'edit' => 'Edit task',
    'back_to_tasks' => 'Back to tasks',
    'no_records' => 'No tasks found',
    'save_task' => 'save task',
    'search' => 'search',
    'search_tasks' => 'Search tasks',
    'per_page' => 'per page',
    'bulk_actions' => 'bulk actions',
    'delete' => 'delete',
    'apply' => 'apply',
    'page' => 'Page',
    'of' => 'of',
    'order' => 'order',
    'sort_by' => 'Sort by',
    'asc' => 'Ascending',
    'desc' => 'Descending',
    'actions' => 'actions',
    'filter' => 'filter',
    
    'task_status' => 'Task status',
    'activate' => 'activate',
    'activated' => 'activated',
    'all' => 'All',
    
    'complete' => "Complete",
    'uncomplete' => "Uncomplete",
    'completed' => "Completed",
    'uncompleted' => "uncompleted",
    'sure_complete' => "mark as completed task ?",
    'sure_uncomplete' => 'mark as uncompleted task ?',
    'completed_by' => "Completed by",
    'at' => "at",
    'deactivate' => 'deactivate',
    'deactivated' => 'deactivated',
    'sure_activate' => "Are you sure to activate task ?",
    'sure_deactivate' => "Are you sure to deactivate task ?",
    'sure_delete' => 'Are you sure to delete ?',
    
    'to_user' => 'to_user',
    'to_group' => 'to_group',
    'user' => 'user',
    'tags' => 'tags',
    'add_tag' => 'Add tags',
    
    'assigned_to' => 'Assigned to',
    'period' => 'Period',
    'select_users' => 'Select users',
    'select_groups' => 'Select groups',
    
    
    
    'attributes' => [
        
        'title' => 'title',
        'description' => 'description',
        'start_date' => 'start_date',
        'end_date' => 'end_date',
        'to' => 'to',
        'done' => 'done',
        'created_at' => 'created date',
        'updated_at' => 'updated date',
        'status' => 'Status'
    ],
    "events" => [
        'created' => 'Task created successfully',
        'updated' => 'Task updated successfully',
        'deleted' => 'Task deleted successfully',
        'activated' => 'Task activated successfully',
        'deactivated' => 'Task deactivated successfully',
        "completed" => "Task marked as completed successfully",
        "uncompleted" => "Task marked as uncompleted successfully"
    ],
    "permissions" => [
        "create" => "Create new task",
        "edit" => "Edit tasks",
        "delete" => "Remove tasks",
    ]
    
];
