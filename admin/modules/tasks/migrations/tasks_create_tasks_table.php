<?php

use Illuminate\Database\Migrations\Migration;
 
class Tasks_create_tasks_table extends Migration {

    public function up(){
    
        Schema::create('tasks', function($table) {
		$table->increments('id')->unsigned();
		$table->string('title', 200);
		$table->string('slug', 200);
		$table->text('description');
		$table->timestamp('start_date')->default("0000-00-00 00:00:00");
		$table->timestamp('end_date')->default("0000-00-00 00:00:00");
		$table->string('to', 10);
		$table->tinyInteger('done');
		$table->unsignedInteger('user_id');
		$table->timestamp('created_at')->default("0000-00-00 00:00:00");
		$table->timestamp('updated_at')->default("0000-00-00 00:00:00");
		$table->tinyInteger('status');
	});
    
    }

    public function down(){
    
        Schema::drop('tasks');
    
    }

}