<?php

use Illuminate\Database\Migrations\Migration;
 
class Tasks_create_tasks_tags_table extends Migration {

    public function up(){
    
        Schema::create('tasks_tags', function($table) {
		$table->unsignedInteger('task_id');
		$table->unsignedInteger('tag_id');
	});
    
    }

    public function down(){
    
        Schema::drop('tasks_tags');
    
    }

}