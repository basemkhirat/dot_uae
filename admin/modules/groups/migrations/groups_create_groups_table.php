<?php

use Illuminate\Database\Migrations\Migration;
 
class Groups_create_groups_table extends Migration {

    public function up(){
    
        Schema::create('groups', function($table) {
		$table->increments('id')->unsigned();
		$table->string('name', 200);
		$table->string('slug', 200);
		$table->text('description');
		$table->unsignedInteger('user_id');
		$table->timestamp('created_at')->default("0000-00-00 00:00:00");
		$table->timestamp('updated_at')->default("0000-00-00 00:00:00");
		$table->tinyInteger('status');
	});
    
    }

    public function down(){
    
        Schema::drop('groups');
    
    }

}