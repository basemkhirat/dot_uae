<?php

Menu::set("sidebar", function($menu) {
    if (User::can('groups')) {        
        $menu->item('users.groups', trans("groups::groups.groups"), route("groups.show"));        
    }
});

include __DIR__ . "/routes.php";
