<?php

class GroupsController extends BackendController {

    protected $data = [];

    function __construct() {
        parent::__construct();
    }

    function index() {

        if (Request::isMethod("post")) {
            if (Input::has("action")) {
                switch (Input::get("action")) {
                    case "delete":
                        return $this->delete();
                    case "activate":
                        return $this->status(1);
                    case "deactivate":
                        return $this->status(0);
                }
            }
        }

        $this->data["sort"] = (Input::has("sort")) ? Input::get("sort") : "id";
        $this->data["order"] = (Input::has("order")) ? Input::get("order") : "DESC";
        $this->data['per_page'] = (Input::has("per_page")) ? Input::get("per_page") : NULL;

        $query = Group::with('users', 'user')->orderBy($this->data["sort"], $this->data["order"]);


        if (Input::has("user_id")) {
            $query->whereHas("user", function($query) {
                $query->where("users.id", Input::get("user_id"));
            });
        }
        if (Input::has("status")) {
            $query->where("status", Input::get("status"));
        }
        if (Input::has("q")) {
            $query->search(urldecode(Input::get("q")));
        }
        $this->data["groups"] = $query->paginate($this->data['per_page']);
        return View::make("groups::show", $this->data);
    }

    public function create() {
        if (Request::isMethod("post")) {
            $group = new Group();

            $group->name = Input::get('name');
            $group->description = Input::get('description');

            $group->user_id = Auth::user()->id;
            $group->status = Input::get("status", 0);

            if (!$group->validate()) {
                return Redirect::back()->withErrors($group->errors())->withInput(Input::all());
            }

            $group->save();
            $group->syncUsers(Input::get("users"));

            return Redirect::route("groups.edit", array("id" => $group->id))
                            ->with("message", trans("groups::groups.events.created"));
        }

        $this->data["group"] = false;
        $this->data["group_users"] = array();

        return View::make("groups::edit", $this->data);
    }

    public function edit($id) {
        $group = Group::findOrFail($id);
        if (Request::isMethod("post")) {

            $group->name = Input::get('name');
            $group->description = Input::get('description');
            $group->status = Input::get("status", 0);

            if (!$group->validate()) {
                return Redirect::back()->withErrors($group->errors())->withInput(Input::all());
            }

            $group->save();
            $group->syncUsers(Input::get("users"));
            return Redirect::route("groups.edit", array("id" => $id))->with("message", trans("groups::groups.events.updated"));
        }

        $this->data["group_users"] = $group->users->lists("username")->toArray();
        $this->data["group"] = $group;
        return View::make("groups::edit", $this->data);
    }

    function search() {
        $data = Group::search(urldecode(Input::get("term")))
                ->where("status", 1)
                ->take(6)
                ->get();

        return json_encode($data);
    }

    public function delete() {
        $ids = Input::get("id");
        if (!is_array($ids)) {
            $ids = array($ids);
        }
        foreach ($ids as $ID) {
            $group = Group::findOrFail($ID);


            $group->delete();
        }
        return Redirect::back()->with("message", trans("groups::groups.events.deleted"));
    }

    public function status($status) {
        $ids = Input::get("id");
        if (!is_array($ids)) {
            $ids = array($ids);
        }
        foreach ($ids as $id) {
            $group = Group::findOrFail($id);
            $group->status = $status;
            $group->save();
        }

        if ($status) {
            $message = trans("groups::groups.events.activated");
        } else {
            $message = trans("groups::groups.events.deactivated");
        }
        return Redirect::back()->with("message", $message);
    }

}
