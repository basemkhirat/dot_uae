<?php
Route::group(array(
    "prefix" => ADMIN,
    "before" => "auth",
        ), function($route) {
        $route->group(array("prefix" => "groups"), function($route) {
            $route->any('/', array("as" => "groups.show", "uses" => "GroupsController@index"));
            $route->any('/create', array("as" => "groups.create", "uses" => "GroupsController@create"));
            $route->any('/{id}/edit', array("as" => "groups.edit", "uses" => "GroupsController@edit"));
            $route->any('/delete', array("as" => "groups.delete", "uses" => "GroupsController@delete"));
            $route->any('/{status}/status', array("as" => "groups.status", "uses" => "GroupsController@status"));
            $route->any('/search', array("as" => "groups.search", "uses" => "GroupsController@search"));
            
        });
});
