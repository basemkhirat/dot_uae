<?php

return [
    "module" => "Groups",
    'groups' => 'Groups',
    'group' => 'group',
    'add_new' => 'Add new group',
    'edit' => 'Edit group',
    'back_to_groups' => 'Back to groups',
    'no_records' => 'No groups found',
    'save_group' => 'save group',
    'search' => 'search',
    'search_groups' => 'Search groups',
    'per_page' => 'per page',
    'bulk_actions' => 'bulk actions',
    'delete' => 'delete',
    'apply' => 'apply',
    'page' => 'Page',
    'of' => 'of',
    'order' => 'order',
    'sort_by' => 'Sort by',
    'asc' => 'Ascending',
    'desc' => 'Descending',
    'actions' => 'actions',
    'filter' => 'filter',
    'group_status' => 'Group status',
    'activate' => 'activate',
    'activated' => 'activated',
    'all' => 'All',
    'deactivate' => 'deactivate',
    'deactivated' => 'deactivated',
    'sure_activate' => "Are you sure to activate group ?",
    'sure_deactivate' => "Are you sure to deactivate group ?",
    'sure_delete' => 'Are you sure to delete ?',
    'users_count' => 'Users',
    'user' => 'user',
    'users' => 'Users',
    'attributes' => [

        'name' => 'name',
        'description' => 'description',
        'created_at' => 'created date',
        'updated_at' => 'updated date',
        'status' => 'Status'
    ],
    "events" => [
        'created' => 'Group created successfully',
        'updated' => 'Group updated successfully',
        'deleted' => 'Group deleted successfully',
        'activated' => 'Group activated successfully',
        'deactivated' => 'Group deactivated successfully'
    ],
    "permissions" => [
        "create" => "Create new group",
        "edit" => "Edit groups",
        "delete" => "Remove groups",
    ]
];
