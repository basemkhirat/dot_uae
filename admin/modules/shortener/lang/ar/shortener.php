<?php

return array(
    'url' => 'الرابط',
    'url_original' => 'الرابط الاأصلي',
    'url_shortened' => 'الرابط المختصر',
    'clicks' => 'النقرات',
    'code' => 'الكود',
    'shortener' => 'الروابط المختصرة',
    'new_shortener' => 'رابط مختصر جديد',
    'delete_confirm' => 'هل أنت متأكد من الحذف',
    'exsists' => 'هذا الرابط موجود',
    'error' => 'الرابط غير صحيح',
    'success' => 'تم انشاء الرابط المختصر',
    'success_delete' => 'تم مسح الرابط',
    'search' => 'بحث',
    'all' => 'كل الروابط',
    'ok' => 'موافق',
    'cancel' => 'إلغاء',
    'required' => 'هذا الحقل مطلوب.',
    'invalid' => 'هذا الحقل غير سليم.',
);
