<?php

return array(
    'url' => 'URL',
    'url_original' => 'Original URL',
    'url_shortened' => 'Shortened URL',
    'clicks' => 'Clicks',
    'code' => 'Code',
    'shortener' => 'Shortener',
    'new_shortener' => 'New Shortener',
    'delete_confirm' => 'هل أنت متأكد من الحذف',
    'exsists' => 'هذا الرابط موجود',
    'error' => 'الرابط غير صحيح',
    'success' => 'تم انشاء الرابط المختصر',
    'success_delete' => 'تم مسح الرابط',
    'search' => 'بحث',
    'all' => 'All Links',
    'ok' => 'Ok',
    'cancel' => 'Cancel',
    'required' => 'This field is required.',
    'invalid' => 'This field is invalid.',
);
