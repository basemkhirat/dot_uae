<?php

Route::group(array(
    "prefix" => ADMIN,
    "before" => "auth",
        ), function($route) {
    $route->group(array("prefix" => "shortener"), function($route) {
        $route->controller('/', 'UrlShortenerController');
    });
});
