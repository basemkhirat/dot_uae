<?php

Menu::set("sidebar", function($menu) {
    $menu->item('shortener', trans("admin::common.shortener"), URL::to(ADMIN . '/shortener'))->order(6.5)->icon("fa-link");
});

include __DIR__ . "/routes.php";
