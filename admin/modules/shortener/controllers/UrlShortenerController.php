<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of UrlShortenerController
 *
 * @author Khaled PC
 */
class UrlShortenerController extends BackendController {

    var $data = array();

    function getIndex() {
        $shortener = Shortener::orderBy('created_at', 'desc');
        if (Input::get('q')) {
            $shortener->where('url', 'like', '%' . Input::get('q') . '%');
        }
        $this->data['links'] = $shortener->paginate(20);
        return View::make('shortener::index', $this->data);
    }

    function postCreate() {
        $validator = Validator::make(Input::all(), array(
                    'url' => 'required|url'
        ));
        if ($validator->fails()) {
            return Redirect::to(ADMIN.'/shortener')->withErrors($validator)
                            ->with('flash_message', array(
                                'type' => 'danger',
                                'text' => trans('shortener::shortener.error')
                            ))->withInput(Input::all());
        }
        $url = Input::get('url');
        $exsists = Shortener::whereUrl($url)->first();
        if ($exsists) {
            return Redirect::to(ADMIN.'/shortener?q=' . $exsists->url)->with('flash_message', array(
                        'type' => 'info',
                        'text' => trans('shortener::shortener.exsists') . ' ' . $exsists->shortened_url
                    ))->withInput(Input::all());
        }
        $shortener = Shortener::createForUrl($url);
        return Redirect::to(ADMIN.'/shortener')->with('flash_message', array(
                    'type' => 'success',
                    'text' => trans('shortener::shortener.success')
                ))->with('shortened_link', $shortener->shortened_url);
    }

    function getDelete($id) {
        Shortener::whereId($id)->delete();
        return Redirect::to(ADMIN.'/shortener')->with('flash_message', array(
                    'type' => 'success',
                    'text' => trans('shortener::shortener.success_delete')
        ));
    }

}
