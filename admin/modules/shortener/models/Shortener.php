<?php




/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Shortener
 *
 * @author Khaled PC
 */
class Shortener extends Model {

    protected $table = 'short_url';
    public $fillable = array('url', 'code', 'post_id');

    public function getShortenedUrlAttribute() {
//        return 'http://dot.ms/' . $this->code;
        return 'http://dotm.sr/' . $this->code;
    }

    public static function createForUrl($url) {
        $code = self::generateCode();
        $shorened = new Shortener(array(
            'url' => $url,
            'code' => $code
        ));
        $shorened->save();
        return $shorened;
    }

    public static function createForPost($post) {
        $code = self::generateCode();
        $shorened = new Shortener(array(
            'url' => $post->url,
            'code' => $code,
            'post_id' => $post->post_id
        ));
        return $shorened->save();
    }

    public static function generateCode() {
        do {
            $code = str_random(5);
        } while (self::whereRaw('BINARY code = ?', array($code))->count() != 0);
        return $code;
    }

}
