@extends("admin::layouts.master")

@section("breadcrumb")
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2><i class="fa fa-link faa-tada animated faa-slow"></i>  {!!trans('shortener::shortener.shortener')!!} ({!!@$links->total()!!})</h2>
        <ol class="breadcrumb" style="background: none;">
            <li><a  href="{!!URL::to('/'.ADMIN.'/dashboard')!!}">{!!Lang::get('admin::common.dashboard')!!}</a></li>
            <li>{!!trans('shortener::shortener.shortener')!!}</li>

        </ol>
    </div>

</div>
@stop
@section("content")
<div id="content-wrapper">

    @if(Session::get('flash_message'))
    <div class="alert alert-{!!Session::get('flash_message')['type']!!} alert-dark">
        <button type="button" class="close" data-dismiss="alert">×</button>
        {!!Session::get('flash_message')['text']!!}
    </div>
    @endif

    <div class="row">

        <div class="col-sm-6">
            <div class="ibox float-e-margins">
                {!! Form::open([
                    'url' => URL::to('admin/shortener/create'),
                    'method' => 'POST',
                    'id' => 'shortener_form',
                    'class' => 'panel form-horizontal',
        ]) !!}

                <div class="ibox-title">
                    <h5> {!!trans('shortener::shortener.new_shortener')!!} </h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>

                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">

                    <div class="form-group {!!$errors->has('url') ? 'has-error has-feedback' : ''!!}">
                        <label for="url-text" class="col-sm-2 control-label">{!!trans('shortener::shortener.url')!!}</label>
                        <div class="col-sm-10">
                            <input name="url" type="url" class="form-control" id="url-text" placeholder="{!!trans('shortener::shortener.url')!!}" @if(Input::old('url')) value="{!!Input::old('url')!!}" @endif>
                                   @if($errors->has('url'))<span class="fa fa-times-circle form-control-feedback"></span>@endif
                        </div>
                    </div>
                    @if(Session::get('shortened_link'))
                    <div class="alert alert-info alert-dark">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        {!!Session::get('shortened_link')!!}
                    </div>

                    @endif
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-9">
                            <button type="submit" class="btn btn-primary">{!!trans('topics::topics.save')!!}</button>
                        </div>
                    </div>
                </div>
                {!! Form::close() !!}
            </div></div>
        <div class="col-lg-6">
            {!! Form::open(array('method' => 'get', 'id' => 'search', 'class' => 'choosen-rtl')) !!}

            <div class="input-group">
                <input name="q" id="q" value="{!!Input::get('q') ? Input::get('q') : ''!!}" type="text" placeholder="{!!trans('shortener::shortener.search')!!}" class="input-sm form-control" > <span class="input-group-btn">
                    <button type="submit" class="btn btn-sm btn-primary"> {!!Lang::get('pages::pages.search')!!}</button> </span>
            </div>

            {!! Form::close() !!}
        </div>
        <div class="col-sm-6" id="cat_table">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5> {!!trans('shortener::shortener.all')!!} </h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>

                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <div class="table-responsive">


                        <table cellspacing="0" cellpadding="0" border="0" class="table table-striped">
                            <thead>
                                <tr>
                                    <th><?php echo trans('shortener::shortener.url_original') ?></th>
                                    <th><?php echo trans('shortener::shortener.url_shortened') ?></th>
                                    <th><?php echo trans('shortener::shortener.clicks') ?></th>
                                    <th><?php echo trans('topics::topics.delete') ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($links as $link)

                                <tr>
                                    <td title="{!!$link->url!!}">{!!str_limit($link->url,30)!!}</td>
                                    <td>{!!$link->shortened_url!!}</td>
                                    <td>{!!$link->clicks!!}</td>
                                    <td class="tooltip-demo m-t-md">
                                        <a class="btn btn-white btn-sm delete-url" message="<?php echo trans("shortener::shortener.delete_confirm") ?>" href="{!!URL::to(ADMIN.'/shortener/delete/' . $link->id)!!}">
                                            <i class="fa fa-times"></i>
                                        </a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <div class="panel-footer text-right" style="border-top: 1px solid #ececec; background: none;">
                            <div class="col-sm-2 m-b-xs" style="padding:0">

                            </div>

                            <div class="col-sm-10 m-b-xs" style="padding:0">
                                <div class="pull-right">
                                    {!!$links->appends(Input::all())->setPath('')->render()!!}
                                </div>
                            </div>
                        </div>
                        <!--LINKS-->
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
</div> <!-- / #content-wrapper -->
<script src="<?php echo assets() ?>/js/plugins/validate/jquery.validate.min.js"></script>
<script src="<?php echo assets() ?>/js/bootbox.min.js"></script>
<script>
$(document).ready(function () {
    $('#shortener_form').validate({
        ignore: [],
        rules: {
            url: {
                required: true,
                url: true
            },
        }
    });

    jQuery.extend(jQuery.validator.messages, {
        required: "{!!Lang::get('shortener::shortener.required')!!}",
        url: "{!!Lang::get('shortener::shortener.invalid')!!}",
    });

    $('.delete-url').on('click', function (e) {
        e.preventDefault();
        $this = $(this);
        bootbox.dialog({
            message: $this.attr('message'),
            title: $this.attr('title'),
            buttons: {
                success: {
                    label: "{!!Lang::get('shortener::shortener.ok')!!}",
                    className: "btn-success",
                    callback: function () {
                        location.href = $this.attr('href');
                    }
                },
                danger: {
                    label: "{!!Lang::get('shortener::shortener.cancel')!!}",
                    className: "btn-default",
                    callback: function () {
                    }
                },
            },
            className: "bootbox-sm"
        });
    });
    
    // search submit
    $("#search").submit(function (e) {
        if($("#q").val() == ''){
            return false
        }
    });


//        $(".change_photo").filemanager({
//            types: "png|jpg|jpeg|gif|bmp",
//            done: function(files, base) {
//
//                if (files.length) {
//                    $('.featured_image').remove();
//                    var file = files[0];
//                    $("#topic_photo_preview").attr("src", file.media_thumbnail);
//                    $('<input>', {
//                        name: 'image_id',
//                        type: 'hidden',
//                        class: 'featured_image',
//                        value: file.media_id
//                    }).appendTo($('#topic_form'));
//                }
//            },
//            error: function(media_path) {
//                alert(media_path + " is not an image");
//            }
//        });
});
</script>
@stop
