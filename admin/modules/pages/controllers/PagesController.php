<?php

class PagesController extends BackendController {

    protected $data = [];

    function __construct() {
        parent::__construct();
    }

    function index() {
        if (Request::isMethod("post")) {
            if (Input::has("action")) {
                switch (Input::get("action")) {
                    case "delete":
                        return $this->delete();
                    case "activate":
                        return $this->status(1);
                    case "deactivate":
                        return $this->status(0);
                }
            }
        }

        $this->data["sort"] = (Input::has("sort")) ? Input::get("sort") : "id";
        $this->data["order"] = (Input::has("order")) ? Input::get("order") : "DESC";
        $this->data['per_page'] = (Input::has("per_page")) ? Input::get("per_page") : NULL;

        $query = Page::with('meta', 'image', 'user', 'tags')->orderBy($this->data["sort"], $this->data["order"]);
        if (Input::has("tag_id")) {
            $query->whereHas("tags", function($query) {
                $query->where("tags.tag_id", Input::get("tag_id"));
            });
        }

        if (Input::has("user_id")) {
            $query->whereHas("user", function($query) {
                $query->where("users.id", Input::get("user_id"));
            });
        }
        if (Input::has("status")) {
            $query->where("status", Input::get("status"));
        }
        if (Input::has("q")) {
            $query->search(urldecode(Input::get("q")));
        }
        $this->data["pages"] = $query->paginate($this->data['per_page']);
        return View::make("pages::show", $this->data);
    }

    public function create() {
        $page = new Page();
        if (Request::isMethod("post")) {

            $page->title = Input::get('title');
            $page->excerpt = Input::get('excerpt');
            $page->content = Input::get('content');
            $page->image_id = Input::get('image_id');
            $page->user_id = Auth::user()->id;
            $page->status = Input::get("status", 0);
            $page->template = Input::get("template");
            $page->save();
            if ($page->hasErrors()) {
                return Redirect::back()->withErrors($page->getErrors())->withInput(Input::all());
            }

            $page->syncTags(Input::get("tags"));

            return Redirect::route("pages.edit", array("id" => $page->id))
                            ->with("message", trans("pages::pages.events.created"));
        }

        // calculate seo results
        $amseo = new Amseo();
        $this->data['seo_results'] = $amseo->linkdex_output(new Page());
        $this->data['publish_seo'] = $amseo->publish_box();

        $this->data["page_tags"] = array();
        $this->data["page"] = $page;

        // templates
        $template = new Template();
        $this->data["templates"] = $template->get_templates();

        return View::make("pages::edit", $this->data);
    }

    public function edit($id) {
        $page = Page::with('meta')->findOrFail($id);
        if (Request::isMethod("post")) {
            $rules = array(
                'title' => 'required|unique:pages,title,' . $id . ',id',
                'excerpt' => 'required',
                'content' => 'required'
            );

            // run the validation rules on the inputs from the form
            $validator = Validator::make(Input::all(), $rules);

            $validator->setCustomMessages(Lang::get('admin::validation'));

            // if the validator fails, redirect back to the form
            if ($validator->fails()) {
                return Redirect::back()->withErrors($page->getErrors())->withInput(Input::all());
            }

            $page->title = Input::get('title');
            $page->excerpt = Input::get('excerpt');
            $page->content = Input::get('content');
            $page->image_id = Input::get('image_id');
            $page->status = Input::get("status", 0);
            $page->template = Input::get("template");
            $page->save();

            // page meta
            $meta = Input::get('meta');
            $meta['robots_advanced'] = (Input::get('robots_advanced')) ? implode(',', Input::get('robots_advanced')) : '';
            $meta['canonical_url'] = (substr($meta['canonical_url'], 0, 4) != 'http' && $meta['canonical_url'] != '') ? "http://" . $meta['canonical_url'] : $meta['canonical_url'];
            $meta['amseo_redirect'] = (substr($meta['amseo_redirect'], 0, 4) != 'http' && $meta['amseo_redirect'] != '') ? "http://" . $meta['amseo_redirect'] : $meta['amseo_redirect'];
            if ($page->meta) {
                // update meta
                $page->meta()->update($meta);
            } else {
                // create meta
                $meta['amseo_redirect'] = 1;
                $meta = new PostMeta($meta);
                $meta->page()->associate($page);
                $meta->save(); // or $post->meta()->create($meta);
            }

            if ($page->hasErrors()) {
                return Redirect::back()->withErrors($page->getErrors())->withInput(Input::all());
            }

            $page->syncTags(Input::get("tags"));
            return Redirect::route("pages.edit", array("id" => $id))->with("message", trans("pages::pages.events.updated"));
        }

        // calculate seo results
        $amseo = new Amseo();
        $this->data['seo_results'] = $amseo->linkdex_output($page, $type = 2);
        $this->data['publish_seo'] = $amseo->publish_box();

        $this->data["page_tags"] = $page->tags->lists("tag_name")->toArray();
        $this->data["page"] = $page;

        // templates
        $template = new Template();
        $this->data["templates"] = $template->get_templates();

        return View::make("pages::edit", $this->data);
    }

    public function delete() {
        $ids = Input::get("id");
        if (!is_array($ids)) {
            $ids = array($ids);
        }
        foreach ($ids as $ID) {
            $page = Page::findOrFail($ID);

            $page->tags()->detach();
            $page->delete();
        }
        return Redirect::back()->with("message", trans("pages::pages.events.deleted"));
    }

    public function status($status) {
        $ids = Input::get("id");
        if (!is_array($ids)) {
            $ids = array($ids);
        }
        foreach ($ids as $id) {
            $page = Page::findOrFail($id);
            $page->status = $status;
            $page->save();
        }

        if ($status) {
            $message = trans("pages::pages.events.activated");
        } else {
            $message = trans("pages::pages.events.deactivated");
        }
        return Redirect::back()->with("message", $message);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function new_slug() {
        $id = Input::get('id');

        $slug = create_slug(Input::get('slug'));
        $isUnique = Page::where('slug', '=', $slug)->where('id', '!=', $id)->first();
        while ($isUnique !== NULL) {
            $slug = $slug . '-' . rand(0, 99999);
            $isUnique = Page::where('slug', '=', $slug)->where('id', '!=', $id)->first();
        }

        Page::where('id', $id)->update(['slug' => $slug]);

        echo json_encode($slug);
        exit;
    }

}
