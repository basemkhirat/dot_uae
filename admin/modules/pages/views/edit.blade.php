<?php /* Template Name: Example Template */ ?>
@extends("admin::layouts.master")

<?php
@$meta_robots = $page->meta;
?>

@section("breadcrumb")
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>
            <i class="fa fa-file-text-o"></i>
            <?php
            if ($page) {
                echo trans("pages::pages.edit");
            } else {
                echo trans("pages::pages.add_new");
            }
            ?>
        </h2>
        <ol class="breadcrumb">
            <li>
                <a href="<?php echo URL::to(ADMIN . "/pages"); ?>"><?php echo trans("pages::pages.pages"); ?></a>
            </li>
            <li class="active">
                <strong>
                    <?php
                    if ($page) {
                        echo trans("pages::pages.edit");
                    } else {
                        echo trans("pages::pages.add_new");
                    }
                    ?>
                </strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-3 text-left">
        <?php if ($page) { ?>
            <a href="<?php echo route("pages.create"); ?>" class="btn btn-primary btn-labeled btn-main"> <span class="btn-label icon fa fa-plus"></span> &nbsp; <?php echo trans("pages::pages.add_new") ?></a>
        <?php } else { ?>
            <a href="<?php echo route("pages.show"); ?>" class="btn btn-primary btn-labeled btn-main"> 
                <?php echo trans("pages::pages.back_to_pages") ?>
                &nbsp;  <i class="fa fa-chevron-left"></i>
            </a>
        <?php } ?>
    </div>
</div>
@stop
@section("content")
@include("admin::partials.messages")
<form action="" method="post">
    <div class="row">
        <div class="col-md-8">
            <div class="panel panel-default">
                <div class="panel-body">


                    <div class="form-group">

                        <input name="title" type="text" value="<?php echo @Input::old("title", $page->title); ?>" class="form-control input-lg" id="post_title" placeholder="<?php echo trans("pages::pages.attributes.title") ?>">
                        <div class="inside" style="@if(!isset($page->id)) display: none; @endif margin-top:5px">
                            <div id="edit-slug-box" class="hide-if-no-js">
                                <strong style="color: #8FAA69;">{!!Lang::get('posts::posts.permalink')!!}:</strong>
                                <span id="sample-permalink" style="font-weight: bold;" class="tooltip-demo">
                                    {!! URL::to("/details") !!}/
                                    <span id="editable-post-name" data-toggle="tooltip" data-placement="top" title="{!!Lang::get('posts::posts.permalink_info')!!}">
                                        <input id="new-post-slug" style="display:none" value="{!!$page->slug!!}" type="text">
                                        <span id="slug_name" style="background-color: #FFFBCC;" class="edit_slug">{!!$page->slug!!}</span>
                                    </span>/
                                </span>
                                ‎&nbsp;<span id="edit-slug-buttons">
                                    <a href="#" class="btn btn-primary btn-xs" id="permalink_ok" style="display:none">{!!Lang::get('posts::posts.ok')!!}</a> 
                                    <a class="btn btn-default btn-xs" href="#" id="permalink_cancel" style="display:none">{!!Lang::get('posts::posts.cancel')!!}</a>
                                    <a href="#post_name" class="btn btn-default btn-xs edit_slug" id="permalink_edit">{!!Lang::get('posts::posts.edit')!!}</a>
                                </span>
                                <span id="editable-post-name-full" style="display:none">{!!$page->slug!!}</span>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">

                        <textarea name="excerpt" class="form-control" id="post_excerpt" placeholder="<?php echo trans("pages::pages.attributes.excerpt") ?>"><?php echo @Input::old("excerpt", $page->excerpt); ?></textarea>
                    </div>

                    <div class="form-group">

                        @include("admin::partials.editor", ["name" => "content", "id" => "pagecontent", "value" => @$page->content])
                    </div>
                </div>
            </div>

            <!-- seo options-->
            @include("posts::partials.seo", ["id" => $page->id])

        </div>
        <div class="col-md-4">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-check-square"></i>
                    <?php echo trans("pages::pages.page_status"); ?>
                </div>
                <div class="panel-body">
                    <div class="form-group switch-row">
                        <label class="col-sm-9 control-label" for="input-status"><?php echo trans("pages::pages.attributes.status") ?></label>
                        <div class="col-sm-3">
                            <input <?php if (@Input::old("status", $page->status)) { ?> checked="checked" <?php } ?> type="checkbox" id="input-status" name="status" value="1" class="status-switcher switcher-sm">
                        </div>

                    </div>
                    {!!$publish_seo!!}
                    <div class="form-group" style="margin-bottom:0">
                        <input type="submit" class="pull-right btn btn-flat btn-primary" value="<?php echo trans("pages::pages.save_page") ?>" />
                    </div>
                </div>
            </div>
            @if(count($templates))
            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-file-text"></i>
                    <?php echo trans("pages::pages.templates"); ?>
                </div>
                <div class="panel-body">
                    <div class="form-group switch-row">
                        <div class="form-group" style="margin-bottom:0px">
                            <div class="radio" style="margin-top: 0;">
                                <label>
                                    <input type="radio" name="template" value="default" class="i-checks" checked>&nbsp;
                                    <span class="lbl"><?php echo trans("pages::pages.attributes.default") ?></span>
                                </label>
                            </div>
                            @foreach($templates as $key => $value)
                            <div class="radio" style="margin-top: 0;">
                                <label>
                                    <input type="radio" name="template" value="{!!$key!!}" class="i-checks" @if(@$page->template == $key) checked @endif>&nbsp;
                                           <span class="lbl">{!!$value!!}</span>
                                </label>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
            @endif
            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-picture-o"></i>
                    <?php echo trans("pages::pages.add_image"); ?>
                </div>
                <div class="panel-body form-group">
                    <div class="row post-image-block">
                        <input type="hidden" name="image_id" class="post-image-id" value="<?php
                        if ($page and @ $page->image->media_path != "") {
                            echo @$page->image->media_id;
                        }
                        ?>">
                        <a class="change-post-image label" href="javascript:void(0)">
                            <i class="fa fa-pencil text-navy"></i>
                            <?php echo trans("pages::pages.change_image"); ?>
                        </a>
                        <a class="post-image-preview" href="javascript:void(0)">
                            <img width="100%" height="130px" class="post-image" src="<?php if ($page and @ $page->image->media_id != "") { ?> <?php echo thumbnail(@$page->image->media_path); ?> <?php } else { ?> <?php echo assets("default/post.png"); ?><?php } ?>">
                        </a>
                    </div>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-tags"></i>
                    <?php echo trans("pages::pages.add_tag"); ?>
                </div>
                <div class="panel-body">
                    <div class="form-group" style="position:relative">
                        <input type="hidden" name="tags" id="tags_names" value="<?php echo join(",", $page_tags); ?>">
                        <ul id="mytags"></ul>
                    </div>
                </div>
            </div>
        </div>
        <div style="clear:both"></div>
        <div>
            <div class="panel-footer" style="border-top: 1px solid #ececec; position: relative;">
                <div class="form-group" style="margin-bottom:0">
                    <input type="submit" class="pull-right btn btn-flat btn-primary" value="<?php echo trans("pages::pages.save_page") ?>" />
                </div>
            </div>
        </div>
    </div>
</form>
@section("header")
@parent
<link href="<?php echo assets("tagit") ?>/jquery.tagit.css" rel="stylesheet" type="text/css">
<link href="<?php echo assets("tagit") ?>/tagit.ui-zendesk.css" rel="stylesheet" type="text/css">
@stop
@section("footer")
@parent
<script type="text/javascript" src="<?php echo assets("tagit") ?>/tag-it.js"></script>
<script type="text/javascript" src="<?php echo assets('ckeditor/ckeditor.js') ?>"></script>
<script>
    var baseURL = '{!! URL::to("/".ADMIN) !!}/';
    var postURL = '{!! URL::to(' / details / ') !!}/{!!$page->slug!!}';
    var baseURL2 = '{!! URL::to("") !!}/';
    var assetsURL = '{!! assets("") !!}/';
    var post_id = "{!!$page->id!!}";
    var mongo_id = false;
    var AMAZON_URL = "{!!AMAZON_URL!!}";

    // seo
    var page_title = " <?php echo Lang::get('posts::posts.article_heading') ?>:";
    var page_url = " <?php echo Lang::get('posts::posts.page_url') ?>:";
    var post_content = " <?php echo Lang::get('posts::posts.content') ?>:";
    var seo_title = " <?php echo Lang::get('posts::posts.page_title') ?>:";
    var seo_description = " <?php echo Lang::get('posts::posts.meta_description') ?>:";
    var focus_keyword = " <?php echo Lang::get('posts::posts.focus_keyword_p') ?>:";
    var focus_keyword_usage = " <?php echo Lang::get('posts::posts.focus_keyword_usage') ?>";
    var yes = " <?php echo Lang::get('posts::posts.yes') ?>";
    var no = " <?php echo Lang::get('posts::posts.no') ?>";
    var sitename = " - <?php echo Config::get("site_name") ?>";
    var notfound_lang = "{!!Lang::get('posts::posts.notfound')!!}";

    $(document).ready(function () {

        var elems = Array.prototype.slice.call(document.querySelectorAll('.status-switcher'));
        elems.forEach(function (html) {
            var switchery = new Switchery(html, {size: 'small'});
        });

        $('.i-checks').iCheck({
            checkboxClass: 'icheckbox_square-green',
            radioClass: 'iradio_square-green',
        });

        var config = {
            '.chosen-select': {},
            '.chosen-select-deselect': {
                allow_single_deselect: true
            },
            '.chosen-select-no-single': {
                disable_search_threshold: 10
            },
            '.chosen-select-no-results': {
                no_results_text: notfound_lang
            },
            '.chosen-select-width': {
                width: "95%"
            }
        }
        for (var selector in config) {
            $(selector).chosen(config[selector]);
        }

        $(".change-post-image").filemanager({
            types: "image",
            done: function (result, base) {
                if (result.length) {
                    var file = result[0];
                    base.parents(".post-image-block").find(".post-image-id").first().val(file.media_id);
                    base.parents(".post-image-block").find(".post-image").first().attr("src", file.media_thumbnail);
                }
            },
            error: function (media_path) {
                alert("<?php echo trans("pages::pages.not_allowed_file") ?>");
            }
        });
        $("#mytags").tagit({
            singleField: true,
            singleFieldNode: $('#tags_names'),
            allowSpaces: true,
            minLength: 2,
            placeholderText: "",
            removeConfirmation: true,
            tagSource: function (request, response) {
                $.ajax({
                    url: "<?php echo route("tags.search"); ?>",
                    data: {term: request.term},
                    dataType: "json",
                    success: function (data) {
                        response($.map(data, function (item) {
                            return {
                                label: item.tag_name,
                                value: item.tag_name
                            }
                        }));
                    }
                });
            },
            beforeTagAdded: function (event, ui) {
                $("#metakeywords").tagit("createTag", ui.tagLabel);
            }
        });

        CKEDITOR.instances['pagecontent'].on('change', function (evt) {
            amtUpdateDesc();
            amtTestFocusKw();
        });
    });


    $(window).load(function () {
        // slug
        $('.edit_slug').click(function (e) {
            e.preventDefault();
            $('.edit_slug').hide();
            $('#permalink_ok').show();
            $('#permalink_cancel').show();
            $('#slug_name').hide();
            $('#new-post-slug').show();
        });

        $('#permalink_cancel').click(function (e) {
            e.preventDefault();
            $(this).hide();
            $('#permalink_ok').hide();
            $('.edit_slug').show();
            $('#new-post-slug').hide();
        });

        $('#permalink_ok').click(function (e) {
            e.preventDefault();
            ajaxData = {
                slug: $("#new-post-slug").val(),
                id: post_id
            };
            $.ajax({
                type: "POST",
                dataType: 'json',
                url: baseURL + "pages/newSlug",
                data: ajaxData,
                beforeSend: function (res) {

                },
                success: function (res) {
                    $("#slug_name").html(res);
                    $("#editable-post-name-full").html(res);
                    //$("#wpseosnippet_slug").html(res);
                    $("#new-post-slug").val(res).hide();
                    $('#permalink_ok').hide();
                    $('#permalink_cancel').hide();
                    $('.edit_slug').show();
                    postURL = baseURL2 + "/details/" + res;
                    amtUpdateURL();
                },
                complete: function () {

                }
            });
        });
    });


</script>

<script type="text/javascript" src="<?php echo assets("javascripts/posts-seo.js"); ?>"></script>

@stop
@stop
