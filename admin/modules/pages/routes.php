<?php

Route::group(array(
    "prefix" => ADMIN,
    "before" => "auth",
        ), function($route) {
    $route->group(array("prefix" => "pages"), function($route) {
        $route->any('/', array("as" => "pages.show", "uses" => "PagesController@index"));
        $route->any('/create', array("as" => "pages.create", "uses" => "PagesController@create"));
        $route->any('/{id}/edit', array("as" => "pages.edit", "uses" => "PagesController@edit"));
        $route->any('/delete', array("as" => "pages.delete", "uses" => "PagesController@delete"));
        $route->any('/{status}/status', array("as" => "pages.status", "uses" => "PagesController@status"));
        $route->post('newSlug', 'PagesController@new_slug');
    });
});
