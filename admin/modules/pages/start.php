<?php

Menu::set("sidebar", function($menu) {
//    if (User::can('pages')) {
        $menu->item('pages', trans("admin::common.pages"), URL::to(ADMIN . '/pages'))
                ->order(5.5)
                ->icon("fa-file-text-o");
//    }
});


include __DIR__ . "/routes.php";
