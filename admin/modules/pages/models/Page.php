<?php

class Page extends Model {

    protected $module = 'pages';
    protected $table = 'pages';
    protected $primaryKey = 'id';
    public $timestamps = true;
    protected $fillable = array('*');
    protected $guarded = array('id');
    protected $visible = array('*');
    protected $hidden = array();
    protected $searchable = ['title', 'excerpt', 'slug', 'content'];
    protected $perPage = 20;
    protected $sluggable = [
        'slug' => 'title',
    ];
    protected $creatingRules = [
        'title' => 'required|unique:pages',
        'excerpt' => 'required',
        'content' => 'required'
    ];
    protected $updatingRules = [
    ];

    public function meta() {
        return $this->hasOne('PostMeta', 'post_id', 'id');
    }

    public function image() {
        return $this->hasOne("Media", "media_id", "image_id");
    }

    public function user() {
        return $this->hasOne("User", "id", "user_id");
    }

    public function tags() {
        return $this->belongsToMany("Tag", "page_tags", "page_id", "tag_id");
    }

    public function syncTags($tags) {
        $tag_ids = array();
        if ($tags = @explode(",", $tags)) {
            foreach ($tags as $tag_name) {
                $tag = Tag::select("tag_id")->where("tag_name", $tag_name)->first();
                if (count($tag)) {
                    // tag exists
                    $tag_ids[] = $tag->tag_id;
                } else {
                    // create new tag
                    $tag = new Tag();
                    $tag->tag_name = $tag_name;
                    $tag->tag_slug = Str::slug($tag_name);
                    $tag->save();
                    $tag_ids[] = $tag->tag_id;
                }
            }
        }
        $this->tags()->sync($tag_ids);
    }

    public static function boot() {
        Page::created(function($page) {
            // create post meta
            $meta = new PostMeta(['robots_index' => 2, 'robots_follow' => 1, 'in_sitemap' => 1, 'sitemap_priority' => .9, 'score' => 0, 'type' => 2]);
            $page->meta()->save($meta);
        });

        Page::deleting(function($page) {
            $page->tags()->detach();
            $page->meta()->delete();
        });
    }

}
