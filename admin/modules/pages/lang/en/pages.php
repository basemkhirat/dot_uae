<?php
return [
    
    'pages' => 'pages',
    'page' => 'page',
    'add_new' => 'Add new page',
    'edit' => 'Edit page',
    'back_to_pages' => 'Back to pages',
    'no_records' => 'No pages found',
    'save_page' => 'save page',
    'search' => 'search',
    'search_pages' => 'Search pages',
    'per_page' => 'per page',
    'bulk_actions' => 'bulk actions',
    'delete' => 'delete',
    'apply' => 'apply',
    'page' => 'Page',
    'of' => 'of',
    'order' => 'order',
    'sort_by' => 'Sort by',
    'asc' => 'Ascending',
    'desc' => 'Descending',
    'actions' => 'actions',
    'filter' => 'filter',
    
    'page_status' => 'Page status',
    'activate' => 'activate',
    'activated' => 'activated',
    'all' => 'All',
    'deactivate' => 'deactivate',
    'deactivated' => 'deactivated',
    'sure_activate' => "Are you sure to activate page ?",
    'sure_deactivate' => "Are you sure to deactivate page ?",
    'sure_delete' => 'Are you sure to delete ?',
    
    'add_image' => 'Add image',
    'change_image' => 'change image',
    'not_allowed_file' => 'File is not allowed',
    'user' => 'user',
    'tags' => 'tags',
    'add_tag' => 'Add tags',
    'templates' => 'Templates',
    'attributes' => [
        
        'title' => 'title',
        'excerpt' => 'excerpt',
        'content' => 'content',
        'created_at' => 'created date',
        'updated_at' => 'updated date',
        'status' => 'Status',
        'template' => 'Template',
        'default' => 'Default'
    ],
    "events" => [
        'created' => 'Page created successfully',
        'updated' => 'Page updated successfully',
        'deleted' => 'Page deleted successfully',
        'activated' => 'Page activated successfully',
        'deactivated' => 'Page deactivated successfully'
    ]
    
];
