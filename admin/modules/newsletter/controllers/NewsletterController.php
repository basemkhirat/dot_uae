<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of NewsLetterController
 *
 * @author khalid-pc
 */
class NewsletterController extends BackendController {

    var $directoryPath;
    var $filePath;
    var $viewData;

    public function __construct() {
        if (!User::can('posts.newsletter')) {
            die('Persission Denied');
        }
        $this->directoryPath = public_path('blocks/newsletter/');
        $this->filePath = $this->directoryPath . 'order.json';
        if (!File::exists($this->filePath)) {
            File::makeDirectory($this->directoryPath, 0775, true);
            File::put($this->filePath, '');
        }
    }

    public function getIndex() {
        $ids = json_decode(File::get($this->filePath));
        if ($ids) {
            $idsStr = implode(',', $ids);
            $posts = Post::whereIn('post_id', $ids)->orderByRaw(DB::raw("FIELD(post_id, $idsStr)"))->paginate();
            $this->viewData['posts'] = $posts;
        } else {
            $this->viewData['posts'] = array();
        }
        $mottoOption = Options::readOption('newsletter.motto')->first();
        $this->viewData['motto'] = ($mottoOption) ? $mottoOption->value : '';
        return View::make('newsletter::index', $this->viewData);
    }

    public function postSavePosts() {
        $content = Input::get('ordering');
        File::put($this->filePath, $content);
        $motto = Input::get('motto') ? Input::get('motto') : '';
        Options::updateOption('newsletter.motto', $motto);
        Options::updateOption('newsletter.last_updated', time());
        return Redirect::to(ADMIN . '/newsletter/index');
    }

}
