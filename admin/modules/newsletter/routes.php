<?php

Route::group(array(
    "prefix" => ADMIN,
    "before" => "auth",
        ), function($route) {
    $route->group(array("prefix" => "newsletter"), function($route) {
        $route->controller('/', 'NewsletterController');
    });
});
