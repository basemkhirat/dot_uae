<?php

Menu::set("sidebar", function($menu) {
    if (User::can('posts.newsletter')) {
        $menu->item('newsletter', trans("admin::common.newsletter"), URL::to(ADMIN.'/newsletter'))->order(6)->icon("fa-envelope");
    }
});
                    
include __DIR__ ."/routes.php";
