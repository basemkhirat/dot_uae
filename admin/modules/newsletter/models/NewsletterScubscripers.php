<?php




/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of NewsletterScubscribers
 *
 * @author khalid-pc
 */
class NewsletterScubscripers extends Model{

    protected $table = 'newsletter_subscripers';
    public $fillable = array(
        'email',
        'times'
    );

    public static function getMailPosts($filePath) {
        $ids = json_decode(File::get($filePath));
        if ($ids) {
            $idsStr = implode(',', $ids);
            return Post::whereIn('post_id', $ids)->orderByRaw(DB::raw("FIELD(post_id, $idsStr)"))->get();
        }
        return NULL;
    }

}
