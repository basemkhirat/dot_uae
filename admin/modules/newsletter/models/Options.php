<?php



/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Options
 *
 * @author khalid-pc
 */
class Options extends Model {

    protected $table = 'options';
    public $fillable = array('name', 'value');
    public $timestamps = false;

    public function scopeReadOption($query, $name) {
        return $query->whereName($name);
    }

    public static function updateOption($name, $value) {
        $motto = self::readOption($name)->first();
        if (!$motto) {
            return self::create(array(
                        'name' => $name,
                        'value' => $value
            ));
        } else {
            return $motto->whereName($name)->update(array('value' => $value));
        }
    }

}
