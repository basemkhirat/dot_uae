@extends("admin::layouts.master")

@section("breadcrumb")
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2><i class="fa fa-align-justify faa-tada animated faa-slow"></i> <?php echo trans('newsletter::newsletter.title') ?> ({!!count(@$posts)!!})</h2>
        <ol class="breadcrumb" style="background: none;">
            <li><a  href="{!!URL::to('/'.ADMIN.'/dashboard')!!}">{!!Lang::get('admin::common.dashboard')!!}</a></li>
            <li><?php echo trans('newsletter::newsletter.title') ?></li>

        </ol>
    </div>

</div>
@stop
@section("content")

<style>
    .dd-handle {
        padding: 5px 35px;
    }
    .check-link{
        position: absolute;
        top: 8px;
        left: 10px;
        right: inherit important;
    }
    .delete-related-post{
        position: absolute;
        right: 10px;
        left: inherit important;
        top: 8px;
    }
    .add-item{
        position: relative !important;
    }
    #related-search, #auto-wrap{
        z-index: 10000;          
        border: 1px solid #E7EAEC;
        position: absolute;
        top: 34px;
        display: block;
        background-color: #fff;
        width: 100%;
        padding: 10px;
    }
</style>

<div id="content-wrapper">


    <div class="row">
        <div class="col-sm-12">

            <!-- Javascript -->
<!--            <script>


                $(document).ready(function () {

                    $("#order_posts").submit(function () {
                        var orders = [];
                        $("#sortable_posts div").each(function (i) {

                            orders[i] = $(this).attr("id")

                        });
                        var myJSON = JSON.stringify(orders);
                        $("#ordering").val(myJSON);
                    });
                    $("body").on("click", ".new_block_post", function () {
                        var post = $(this).attr("rel");
                        var url = "<?php echo URL::to('/admin/blocks/saveblockposts/editorderfile'); ?>/" + post;

                        $.get(url, {
                            org_posts: $('#org_posts').val()
                        },
                        function (data) {

                            $("#sortable_posts").prepend(data.html);
                            $('#auto-wrap').hide();
                            $("#related-text").val('');
                            $('#org_posts').val(data.newId);
                        },
                                "json");
                        return false;
                    });
                    init.push(function () {
                        $("#sortable_posts").accordion({
                            header: "> div > h3"
                        }).sortable({
                            axis: "y",
                            handle: "h3"
                        });
                        //add post
                        $('#related-text').on('input', function () {
                            if ($(this).val() == '' || $(this).val().length <= 2) {
                                $('#auto-wrap').show();
                                $("#auto-search").html("{!!Lang::get('newsletter::posts.more_character')!!}");
                                $("#auto-wrap div").html('');
                            } else {
                                var q = $(this).val();
                                var url = "<?php echo URL::to("/admin/blocks/searchposts/addblockpost"); ?>";
                                $.ajax({
                                    type: "GET",
                                    url: url + "/" + q,
                                    data: {ids: $('#org_posts').val()},
                                    beforeSend: function (res) {
                                        $('#auto-wrap').show();
                                        $("#auto-wrap div").html('');
                                        $("#auto-search").html("{!!Lang::get('newsletter::posts.search')!!}");
                                    },
                                    success: function (res) {
                                        $("#auto-wrap div").html('');
                                        if (res != 'notfound') {
                                            $("#auto-wrap div").prepend(res);
                                        } else {
                                            $("#auto-wrap div").html("{!!Lang::get('newsletter::posts.no_results')!!}");
                                        }
                                    },
                                    complete: function () {
                                        $("#auto-search").html('');
                                    }
                                });

                            }
                        });
                        $('body').on('click', '.delete-item', function () {
                            var $self = $(this);
                            var $postId = $self.data('target');
                            var $hideThis = $('div#' + $postId);
                            $hideThis.hide('slow', function () {
                                $(this).remove();
                            });
                        });
                    });
                });
            </script>-->
            <!-- / Javascript -->
            <!--addpost-->
            <!--            <div id="collapseRelated" class="panel-collapse collapse in" style="height: auto;">
                            <div class="panel-body no-padding-vr">
                                <div class="task">
                                    <div class="input-group">
                                        <input type="text" class="form-control" id="related-text" style="margin: 8px;" placeholder="Search for post" autocomplete="off">
                                        <span class="input-group-btn">
                                            <button class="btn disabled" type="button" id="add-related-post">{!!Lang::get('newsletter::posts.add')!!}</button>
                                        </span>
                                    </div>
                                    <input type="hidden" id="related-temp">
                                    <div class="input-group form-control" style="position:absolute; z-index: 10000; display:none;" id="auto-wrap">
                                        <div style="max-height:200px; overflow-y:auto"></div>
                                        <span style="font-style: italic;" id="auto-search">{!!Lang::get('newsletter::posts.more_character')!!}</span>
                                    </div>
                                </div>
                            </div>  / .panel-body 
            
                        </div>-->
            <!--addpost-->
            <div class="ibox float-e-margins">

                <div class="ibox-title">
                    <h5> {!!trans('newsletter::newsletter.posts')!!} </h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>

                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">

                    <div class="form-group">
                        
                        <div class="form-group" style="position:relative">
                            <input type="text" class="form-control input-lg " id="related-text"  style="background-color: snow;" placeholder="{!!Lang::get('posts::posts.search_post')!!}" autocomplete="off">

                            <div id="auto-wrap" style="height:101px; display: none">
                                <div class="feed-activity-list full-height-scroll" style="padding:0 10px"></div>
                            </div>
                            <div id="related-search" style="display: none">
                                <span style="font-style: italic;" >{!!Lang::get('posts::posts.more_character')!!}</span>
                            </div>
                        </div>
                        <!--                                                <div class="task" style="position: relative">
                                                                                                        <div class="input-group">
                                                
                                                                            <input type="text" class="form-control input-lg " id="related-text"  style="background-color: snow;" placeholder="{!!Lang::get('posts::posts.search_post')!!}" autocomplete="off">
                                                                                    <span class="input-group-btn">
                                                                                <button class="btn disabled" type="button" id="add-related-post">{!!Lang::get('posts::posts.add')!!}</button>
                                                                            </span>
                                                                                                        </div>
                                                                            <input type="hidden" id="related-temp">
                                                                            <input type="hidden" id="org_posts" value=""/>
                                                
                                                                            <div class="input-group form-control" style="position:absolute; z-index: 10000; display:none; top:34px; height: auto; max-height:200px; overflow-y:auto" id="auto-wrap">
                                                                                <div class="item"></div>
                                                                                <span style="font-style: italic;" id="auto-search">{!!Lang::get('posts::posts.more_character')!!}</span>
                                                                            </div>
                                                                        </div>-->
                        <div style="height:300px; @if(!count(@$posts)) display: none; @endif" id="related-add" class="dd">
                            <ol class="dd-list full-height-scroll" style="padding: 0 10px">
                                @if(count(@$posts))

                                @foreach(@$posts as $post)

                                <li class="dd-item" data-id="{!!$post->post_id!!}">

                                    <a class="check-link" href="#"><i data-id="{!!$post->post_id!!}" class="fa fa-square-o"></i> </a>
                                    <div class="dd-handle">
                                        <span class="m-l-xs">{!!$post->post_title!!}</span>
                                    </div>
                                    <a class="label label-danger pull-right delete-related-post" href="#{!!$post->post_id!!}"><i class="fa fa-times"></i></a>
                                </li>

                                @endforeach

                                @endif
                            </ol>
                        </div>
                        <div class="panel-footer clearfix" style="border-top: none; background: none; @if(!count(@$posts)) display: none; @endif">
                            <div class="pull-right">
                                <a href="#" class="btn btn-xs" id="clear-completed-tasks"><i class="fa fa-eraser text-success"></i> {!!Lang::get('posts::posts.clear_posts')!!}</a>
                            </div>
                        </div>

                    </div>
                    <div class="hr-line-dashed"></div>
                    <div class="form-group">
                        <form action="{!!URL::to(ADMIN . '/newsletter/save-posts')!!}" method="post" id="order_posts" style="margin-top:20px;">

                            <div class="row form-group">
                                <label class="col-sm-2">المقولة</label>
                                <div class="col-sm-10">
                                    <textarea name="motto" class="form-control">{!!$motto!!}</textarea>
                                </div>
                            </div>
                            <div class="panel-footer text-right" style="border-top: 1px solid #ececec; background: none;">
                                <input type="hidden" name="ordering" id="ordering"/>
                                <input type="submit" value="{!! trans('newsletter::newsletter.save') !!}" class="btn btn-primary"/>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>


</div> <!-- / #content-wrapper -->

<script src="<?php echo assets() ?>/js/plugins/nestable/jquery.nestable.js"></script>
<script>
    var baseURL = '{!! URL::to("/".ADMIN) !!}/';
    var more_character_lang = "{!!Lang::get('posts::posts.more_character')!!}";
    var no_results_lang = "{!!Lang::get('posts::posts.no_results')!!}";
    var notfound_lang = "{!!Lang::get('posts::posts.notfound')!!}";
    var search_lang = "{!!Lang::get('posts::posts.search')!!}";
    $(document).ready(function () {

        $('#related-add').nestable({
            group: 0
        });

        $("#order_posts").submit(function () {
            var orders = [];
            $(".dd-list li").each(function (i) {
                orders[i] = $(this).find('.delete-related-post').attr("href").substr(1);
            });
            var myJSON = JSON.stringify(orders);
            console.log(myJSON);
            $("#ordering").val(myJSON);
            //return false;
        });

        $('#related-text').on('input', function () {
            if ($(this).val() == '' || $(this).val().length <= 2) {
                $('#auto-wrap').hide();
                $("#related-search span").html(more_character_lang);
                $("#related-search").show();
                //$("#auto-wrap .item").html('');
            } else {

                ajaxData = {
                    q: $(this).val(),
                };
                $.ajax({
                    type: "POST",
                    dataType: 'json',
                    url: baseURL + "posts/autocomplete",
                    data: ajaxData,
                    beforeSend: function (res) {
//                    $('#auto-wrap').show();
//                    $("#auto-wrap .item").html('');
//                    $("#related-search").html(search_lang);

                        $("#related-search span").html(search_lang);
                        $("#auto-wrap .feed-activity-list").html('');

                    },
                    success: function (res) {
                        //$("#auto-wrap .item").html('');
                        if (res != 'notfound') {
                            $.map(res, function (n, i) {
                                $("#auto-wrap .feed-activity-list").prepend('<div style="margin-top:5px; padding-bottom:5px" class="dd-item"><a href="#' + n.post_id + '" class="related-one task-title" >' + n.post_title + '</a></div>');
                            });
                            $('#auto-wrap').show();
                            $("#related-search").hide();
                        } else {
                            //$("#auto-wrap .item").html(no_results_lang);
                            $('#auto-wrap').hide();
                            $("#related-search").show();
                            $("#related-search span").html(notfound_lang);
                        }
                    },
                    complete: function () {
                        //$("#related-search").html('');
                        scroll_height('auto-wrap', 300, false, true);
                    }
                });
            }
        });

        $('body').on('click', function () {
            $('#auto-wrap').hide();
            $('#auto-search').hide();
        });

        $('.dd-list').on('click', '.delete-related-post', function (e) {
            e.preventDefault();
            $(this).parent().remove();
            scroll_height('related-add', 300, true);
        });

        $('.dd-list').on('click', '.check-link', function (e) {
            e.preventDefault();
            if ($(this).find('.fa').hasClass('fa-square-o')) {
                $(this).find('.fa').removeClass('fa-square-o').addClass('fa-check-square');
                $(this).next('span').addClass('todo-completed');
            } else {
                $(this).find('.fa').removeClass('fa-check-square').addClass('fa-square-o');
                $(this).next('span').removeClass('todo-completed');
            }
        });

        $('#auto-wrap').on('click', '.related-one', function (e) {
            e.preventDefault();
            //        $('#related-text').val($(this).html());
            //        $('#related-temp').val($(this).attr('href').substr(1));
            $('#auto-wrap').hide();
            //$('#add-related-post').removeClass('disabled');

            id = $(this).attr('href').substr(1);
            text = $(this).html();
            $(".dd-list").prepend('<li class="dd-item" data-id="' + id + '"><a class="check-link" href="#"><i data-id="' + id + '" class="fa fa-square-o"></i> </a><div class="dd-handle"><span class="m-l-xs">' + text + '</span></div><a class="label label-danger pull-right delete-related-post" href="#' + id + '"><i class="fa fa-times"></i></a></li>');
            $(this).addClass('disabled');
            $('#related-text').val('');
            $('.dd-list').show();
            $('.panel-footer').show();
            $('#related-add').show();
            scroll_height('related-add', 300);

        });

        //    $('#add-related-post').click(function (e) {
        //        e.preventDefault();
        //        new_post = $('#related-temp').val();
        //        $(".dd-list").prepend('<li class="dd-item" data-id="' + new_post + '"><a class="check-link" href="#"><i data-id="' + new_post + '" class="fa fa-square-o"></i> </a><div class="dd-handle"><span class="m-l-xs">' + $('#related-text').val() + '</span></div><a class="label label-danger pull-right delete-related-post" href="#' + new_post + '"><i class="fa fa-times"></i></a></li>');
        //        $(this).addClass('disabled');
        //        $('#related-text').val('');
        //        $('.dd-list').show();
        //        $('.panel-footer').show();
        //        $('#related-add').show();
        //        slimscroll_height('related-add');
        //    });

        $('#clear-completed-tasks').click(function (e) {
            e.preventDefault();
            $('.dd-list li').find('.check-link').find('.fa').each(function () {
                if ($(this).hasClass("fa-check-square")) {
                    $(this).parents('li').remove();
                    scroll_height('related-add', 300, true);
                }
            });
        });

        scroll_height('related-add', 300);
        function scroll_height(id, max, footer, auto) {
            height = 0;
            //console.log(max);
            $("#" + id + " .full-height-scroll").find('.dd-item').each(function () {
                height += $(this).outerHeight();
                //console.log(height);
            });
            //console.log(height);
            if (height >= max) {
                $("#" + id).css('height', max);
            } else if (height == 0) {
                $("#" + id).hide();
                if (footer) {
                    $("#" + id).next('.panel-footer').hide();
                }
            } else {
                if (auto) {
                    $("#" + id).css('height', 'auto');
                } else {
                    $("#" + id).css('height', height + 10);
                }
            }
        }

    });
</script>
@stop