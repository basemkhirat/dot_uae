<?php

Route::group(array(
    "prefix" => ADMIN,
    "before" => "auth",
        ), function($route) {
    $route->group(array("prefix" => "aggregator"), function($route) {
        $route->post('delete', 'AggregatorController@destroy');
        $route->get('{id}/delete', 'AggregatorController@destroy');
        
        $route->post('savemove', 'AggregatorController@save_move');
        $route->get('stats', 'AggregatorController@stats');
    });
    Route::resource('aggregator', 'AggregatorController');
});

Route::get('aggregator/themost', "AggregatorController@themost");
Route::get('aggregator/move', "AggregatorController@move");
