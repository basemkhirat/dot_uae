<?php

if (!function_exists("set_themost_sizes")) {

    function set_themost_sizes($filename) {

        if (File::exists(UPLOADS . "/" . $filename)) {
            $sizes = array(
                'large' => array(750, 500),
                'medium' => array(460, 307),
                'small' => array(234, 156)
            );

            $width = \Image::make(UPLOADS . "/" . $filename)->width();
            $height = \Image::make(UPLOADS . "/" . $filename)->height();

            foreach ($sizes as $size => $dimensions) {
                if ($size == 'small') {
                    $handle = @fopen(AMAZON_URL . $filename, "r");
                    if ($handle) {
                        continue;
                    }
                }

                \Image::make(UPLOADS . "/" . $filename)
                        ->fit($dimensions[0], $dimensions[1], function ($constraint) {
                            $constraint->upsize();
                        })
                        ->save(UPLOADS . "/" . $size . "-" . $filename);
                $s3_path = date('Y') . '/' . date('m') . '/' . $size . "-" . $filename;
                s3_save($s3_path);
                unlink(UPLOADS . "/" . $size . "-" . $filename);
            }
        }
    }

}

if (!function_exists("set_sizes")) {

    function set_sizes($filename) {

        if (File::exists(UPLOADS . "/" . $filename)) {
            $sizes = Config::get("media.sizes");
            $width = \Image::make(UPLOADS . "/" . $filename)->width();
            $height = \Image::make(UPLOADS . "/" . $filename)->height();

            foreach ($sizes as $size => $dimensions) {

                \Image::make(UPLOADS . "/" . $filename)
                        ->fit($dimensions[0], $dimensions[1], function ($constraint) {
                            $constraint->upsize();
                        })
                        ->save(UPLOADS . "/" . $size . "-" . $filename);
                $s3_path = date('Y') . '/' . date('m') . '/' . $size . "-" . $filename;
                s3_save($s3_path);
                unlink(UPLOADS . "/" . $size . "-" . $filename);
            }
        }
    }

}