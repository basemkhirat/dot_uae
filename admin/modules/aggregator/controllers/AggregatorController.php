<?php

class AggregatorController extends BackendController {

    public $data = array();
    public $conn = '';

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function __construct() {
        date_default_timezone_set("Etc/GMT-2");

        $this->conn = Session::get('conn');
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index() {
        $q = trim(Input::get('q'));
        $search_type = (Input::get('search_type')) ? Input::get('search_type') : 'post';
        $source = Input::get('sources');
        $cats = Input::get('cats');
        $tags = Input::get('tags');
        $type = Input::get('type');
        $provider = Input::get('provider');
        $format = Input::get('format');
        $order_by = (Input::get('order_by')) ? Input::get('order_by') : 'date';
        $order_type = (Input::get('order_type')) ? Input::get('order_type') : 'DESC';
        $per_page = (Input::has('per_page')) ? Input::get('per_page') : 20;

        $this->data['posts'] = MongoPosts::filter(['type' => $type, 'cats' => $cats, 'provider' => $provider,
                            'source' => $source, 'tags' => $tags, 'search_type' => $search_type, 'q' => $q, 'order_by' => $order_by,
                            'order_type' => $order_type, 'format' => $format, 'site' => $this->conn])
                        ->orderBy($order_by, $order_type)->paginate($per_page);

        $this->data['sources'] = MongoSources::where('status', (int) 1)->whereRaw(array('lang' => $this->conn))->where('deleted', (int) 0)->get();
        $this->data['categories'] = MongoCats::whereRaw(array('lang' => $this->conn))->where('status', (int) 1)->get();

        return View::make('aggregator::aggregator')->with($this->data);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function stats() {

        $source = Input::get('sources');
        $cats = Input::get('cats');
        $sites = Config::get('sites');

        $posts = MongoPosts::whereRaw(array('lang' => $this->conn));
        $site_posts = MongoPosts::whereRaw(array('lang' => $this->conn));
        $facebook_posts = MongoPosts::whereRaw(array('lang' => $this->conn));
        $twitter_posts = MongoPosts::whereRaw(array('lang' => $this->conn));
        $youtube_posts = MongoPosts::whereRaw(array('lang' => $this->conn));
        $instagram_posts = MongoPosts::whereRaw(array('lang' => $this->conn));
        $graph_posts = MongoPosts::whereRaw(array('lang' => $this->conn));

        foreach ($sites as $key => $value) {
            ${"posts_" . $key} = new MongoPosts;
        }

        if ($source) {
            $posts->whereRaw(array('source.id' => (int) $source));
            $site_posts->whereRaw(array('source.id' => (int) $source));
            $facebook_posts->whereRaw(array('source.id' => (int) $source));
            $twitter_posts->whereRaw(array('source.id' => (int) $source));
            $youtube_posts->whereRaw(array('source.id' => (int) $source));
            $instagram_posts->whereRaw(array('source.id' => (int) $source));
            $graph_posts->whereRaw(array('source.id' => (int) $source));

            foreach ($sites as $key => $value) {
                ${"posts_" . $key}->whereRaw(array('source.id' => (int) $source));
            }
        }


        if ($cats) {
            $posts->whereRaw(array('categories.id' => (int) $cats));
            $site_posts->whereRaw(array('categories.id' => (int) $cats));
            $facebook_posts->whereRaw(array('categories.id' => (int) $cats));
            $twitter_posts->whereRaw(array('categories.id' => (int) $cats));
            $youtube_posts->whereRaw(array('categories.id' => (int) $cats));
            $instagram_posts->whereRaw(array('categories.id' => (int) $cats));
            $graph_posts->whereRaw(array('categories.id' => (int) $cats));
            foreach ($sites as $key => $value) {
                ${"posts_" . $key}->whereRaw(array('categories.id' => (int) $cats));
            }
        }

        foreach ($sites as $key => $value) {
            $this->data["posts_" . $key] = ${"posts_" . $key}->whereRaw(array('lang' => $key))->count();
        }


        //$this->data['posts'] = $posts->count();
        $this->data['site_posts'] = $site_posts->where('provider', (int) 0)->count();
        $this->data['facebook_posts'] = $facebook_posts->where('provider', (int) 1)->count();
        $this->data['twitter_posts'] = $twitter_posts->where('provider', (int) 2)->count();
        $this->data['youtube_posts'] = $youtube_posts->where('provider', (int) 3)->count();
        $this->data['instagram_posts'] = $instagram_posts->where('provider', (int) 4)->count();

        $posts_charts = array();

        $from = new \DateTime('+1 day');
        $to = new \DateTime('+2 day');
        for ($i = 0; $i <= 15; $i++) {
            $day = date("Y-m-d", time() - $i * 24 * 60 * 60);

            $from->setTime(0, 0);
            $from->modify('-1 day');

            $to->setTime(0, 0);
            $to->modify('-1 day');

            $posts_charts[$day] = $graph_posts->where('date', '>', $from)->where('date', '<', $to)->count();
        }

        $this->data["posts_charts"] = $posts_charts;
        //print_r($this->data["posts_charts"]);
        $this->data['sources'] = MongoSources::where('status', (int) 1)->whereRaw(array('lang' => $this->conn))->where('deleted', (int) 0)->get();
        $this->data['categories'] = MongoCats::whereRaw(array('lang' => $this->conn))->where('status', (int) 1)->get();

        $posts_sources = array();
        foreach ($this->data['sources'] as $key => $source) {
            $posts_sources[$source['name']] = MongoPosts::whereRaw(array('source.id' => (int) $source['id']))->count();
        }
        asort($posts_sources);
        $this->data["posts_sources"] = $posts_sources;

        $posts_cats = array();
        foreach ($this->data['categories'] as $key => $category) {
            $posts_cats[$category['name']] = MongoPosts::whereRaw(array('categories.id' => (int) $category['id']))->count();
        }
        asort($posts_cats);
        $this->data["posts_cats"] = $posts_cats;

        $this->data['posts'] = $posts->count();
        //print_r($posts_sources);
        //die;
        //print_r($sources); die;
        return View::make('aggregator::stats')->with($this->data);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function aggregator_posts() {

        /* DB::connection('mongodb')->collection('posts')->insertGetId(array(
          '_id' => $this->getNextSequence('counters', 'postid'),
          'title' => 'first',
          'content' => 'content',
          'date' => new \MongoDate(),
          )); */

        /* Retrieve only the sub document element in an object array */
        //$posts = DB::connection('mongodb')->collection('test')->where('cats.id', 1)->get(); Or DB::connection('mongodb')->collection('test')->whereRaw(array('cats.id'=> 2))->get();
        ////DB::connection('mongodb')->collection('posts')->where('_id', '=', 2)->delete();
        //$post = DB::connection('mongodb')->collection('posts')->find(5); 
        //$post = DB::connection('mongodb')->collection('posts')->first();
        $posts = DB::connection('mongodb')->collection('posts')->get();
        //DB::connection('mongodb')->collection('posts')->where('_id', '=', 3)->update(['title' => 'updated']); 

        foreach ($posts as $key => $value) {
            $post = DB::connection('localhost')->table('aggregator_posts')->where('title', '=', $value['title'])->first();
            if (!$post) {
                DB::connection('localhost')->table('aggregator_posts')->insert([
                    'title' => $value['title'],
                    'content' => $value['content'],
                    'date' => date('Y-m-d H:i:s', $value['date']->sec)
                ]);
            }
        }
    }

    /*     * ****************Function to auto increment seq***************** */

    private function getNextSequence($collection, $field) {

        $retval = DB::connection('mongodb')->getCollection($collection)->findAndModify(
                array('_id' => $field), array('$inc' => array("seq" => (int) 1)), null, array(
            "new" => true,
                )
        );
        return (int) $retval['seq'];

        /*
         * db.counters.insert(
         *   {
         *      _id: "postid",
         *      seq: 0
         *   }
         *  )
         */
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create() {
        //return View::make('posts::immediate');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store() {
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id) {
        $post = MongoPosts::where('id', (int) $id)->first();

        if (!$post) {
            return Redirect::to(ADMIN . '/aggregator');
        }

        $this->data['post'] = $post;
        return View::make('aggregator::aggregator_post')->with($this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id) {
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id = 0) {
        $checks = Input::get('check');
        if ($id) {
            MongoPosts::destroy($id);
        } else {
            MongoPosts::destroy($checks);
        }
        return Redirect::back();
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function move() {
        $lang = (int) (Input::get('lang')) ? Input::get('lang') : 0;
        $sites = array_keys(Config::get('sites'));
        if ($lang >= count($sites)) {
            die;
        }
        $lang_code = $sites[$lang];
        $cSites = count($sites);
        unset($sites);

        $uploads = getcwd() . DIRECTORY_SEPARATOR . "uploads" . DIRECTORY_SEPARATOR;
        $count = 10;
        $page = (int) (Input::get('offset')) ? Input::get('offset') : 0;
        $offset = ($count - 1) * $page;
        $now = date("Y-m-d");
        $date = new DateTime($now);
        $date->setTime(0, 0, 0);

        $posts = array();

        if ($offset <= 90) {
            $posts = MongoPosts::where('lang', $lang_code)->where('move', 'exists', false)->where('date', '>', $date)
                            ->orderBy('date', 'DESC')->take($count)->skip($offset)->get();
        }

        unset($count, $offset, $now, $date, $lang_code);

        foreach ($posts as $key => $post) {

            $data = array();
            $data['mongo_id'] = $post['id'];
            $data['post_created_date'] = $data['post_published_date'] = date("Y-m-d H:i:s", $post['date']->sec);
            $data['source_id'] = $post['source']['id'];
            $data['source_name'] = $post['source']['name'];
            $data['source_type'] = $post['source']['type'];
            $data['provider'] = $post['provider'];
            $data['site'] = @$post['lang'];
            $data['link'] = $post['link'];
            $data['post_type'] = ($post['source']['type'] == 1) ? 'post' : (($post['source']['type'] == 2) ? 'article' : 'celebrity');
            $data['post_title'] = $data['post_slug'] = $post['title'];
            $data['post_excerpt'] = $post['title'];
            $data['post_status'] = 1;
            $data['post_desked'] = 0;
            $data['post_submitted'] = 0;
            $data['post_closed'] = 0;
            $data['translated'] = 0;
            $data['post_format'] = $post_format = 1;
            $data['seen'] = 0;
            $data['post_content'] = $post_content = $post['content'];
            $data['meta_title'] = $data['facebook_title'] = string_sanitize(mb_substr($data['post_title'], 0, 70));
            $data['meta_description'] = $data['facebook_description'] = $data['twitter_description'] = string_sanitize(strip_tags(mb_substr($post['content'], 0, 160)));
            $youtube_link = null;
            $image_id = null;
            $media_id = null;
            $tags = '';

            // article author
            if (@$post['source']['type'] == 2) {
                $author = User::firstOrCreate(array("first_name" => @$post['source']['name'], 'role_id' => 9));
                $data['post_author_id'] = $author->id;
            }
            unset($author);

            // check if title found before
            $count = Post::where('mongo_id', '=', $data['mongo_id'])->first();
            if ($count) {
                continue;
            }
            unset($count);

            // meta keywords
            if (array_key_exists('tags', $post)) {
                foreach ($post['tags'] as $key => $value) {
                    if ($key == 0) {
                        $tags .= $value['name'];
                    } else {
                        $tags .= ',' . $value['name'];
                    }
                }
            }
            $data['meta_keywords'] = $tags;
            unset($tags);

            // video
            $youtube = $youtube_link = '';
            if (array_key_exists('video', $post)) {
                if (@$post['video']['type'] == 2) {

                    $youtube = $youtube_link = $post['video']['source'];

                    $pattern = "/\s*[a-zA-Z\/\/:\.]*youtube.com\/embed\/([a-zA-Z0-9\-_]+)([a-zA-Z0-9\/\*\-\_\?\&\;\%\=\.]*)/i";
                    if (preg_match($pattern, $youtube_link, $matches)) {
                        $youtube = preg_replace($pattern, "http://www.youtube.com/v/$1", $youtube_link);
                    }

                    $media_id = $this->youtube($youtube);

                    $post_format = 4;
                    $post_content .= '<br><br><iframe width="650" height="400" src="' . $youtube_link . '" frameborder="0" allowfullscreen></iframe>';
                } else if (@$post['video']['type'] == 1) {
                    $post_content .= '<br><br><video width="650" height="400" controls>
                                    <source src="' . $youtube_link . '" type="video/mp4">
                                </video> ';
                }
            }
            unset($youtube);

            // audio
            $soundcloud = '';
            if (array_key_exists('audio', $post)) {
                if (@$post['audio']['source']) {
                    $soundcloud = urldecode($post['audio']['source']);
                    $media_id = $this->soundcloud($soundcloud);
                    $post_format = 3;
                    $post_content .= '<br><br><iframe width="650" height="400" src="https://w.soundcloud.com/player/?url=' . $soundcloud . '&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false&amp;visual=true" frameborder="0" allowfullscreen></iframe>';
                }
            }
            unset($soundcloud);

            // gallery
            if (@$post['gallery']) {
                $post_format = 5;
            }

            // featured image
            $image = $post['image']['source'];
            if ($image) {
                $handle = @fopen($image, "r");
                if ($handle) {
                    $imagePath = time() * rand() . "." . $this->image_type($image);
                    $contentFile = file_get_contents($image);
                    $fp = fopen("$uploads$imagePath", "w");
                    fwrite($fp, $contentFile);
                    fclose($fp);
                    $media_hash = sha1_file($uploads . $imagePath);
                    $imgfound = Media::where('media_hash', $media_hash)->first();
                    $media_title = ($post['image']['caption']) ? $post['image']['caption'] : $data['post_title'];
                    if (!$imgfound) {
                        $s3_path = date('Y') . '/' . date('m') . '/' . $imagePath;
                        $media = new Media(['media_path' => $s3_path, 'media_hash' => $media_hash, 'media_type' => 'image', 'media_title' => $media_title]);
                        $media->save();
                        $image_id = $media->media_id;
                        s3_save($s3_path);
                        set_sizes($imagePath);
                        unlink("$uploads$imagePath");
                        unset($s3_path, $media);
                    } else {
                        $image_id = $imgfound->media_id;
                    }
                    unset($imagePath, $contentFile, $fp, $media_hash, $imgfound, $media_title);
                }
            }

            $data['youtube_link'] = $youtube_link;
            $data['post_image_id'] = $image_id;
            $data['post_media_id'] = $media_id;
            $data['post_format'] = $post_format;
            $data['post_content'] = $post_content;
            unset($youtube_link, $image_id, $media_id, $post_format, $post_content);

            // insert post
            $newPost = new Post($data);
            $newPost->save();
            //$id = $newPost->post_id;
            unset($data);

            // insert tags
            foreach (@$post['tags'] as $key => $tag) {
                $tagObj = Tag::firstOrCreate(['tag_name' => $tag['name'], 'tag_slug' => create_slug($tag['name']), 'mongo_id' => $tag['id']]);
                // insert posts tags
                $newPost->tags()->attach($tagObj->tag_id);
                unset($tag, $tagObj);
            }

            // autotags
//            $autotags = Tag::where('auto', 1)->get();
//            foreach (@$autotags as $key => $tag) {
//                if (strpos($data['post_title'], $tag->tag_name) !== false || strpos($data['post_content'], $tag->tag_name) !== false) {
//                    if (!$newPost->tags()->contains($tag->tag_id)) {
//                        $newPost->tags()->attach($tag->tag_id);
//                    }
//                }
//            }
            // insert categories
            foreach (@$post['categories'] as $key => $cat) {
                $check = Category::where('mongo_id', $cat['id'])->first();

                if ($check) {
                    $newPost->categories()->attach($check->cat_id);
                }
                unset($cat, $check);
            }

            //MongoPosts::where('id', $post_id)->update(array('move' => (int) 1));
            // update move
            $post->move = (int) 1;
            $post->save();
            unset($post, $newPost);
        }

        if (count($posts)) {
            $page = $page + 1;
            return Redirect::to('aggregator/move?offset=' . $page);
        } else {
            if ($lang < $cSites) {
                $lang = $lang + 1;
                return Redirect::to('aggregator/move?offset=0' . '&lang=' . $lang);
            } else {
                die;
            }
        }
    }

    function themost() {

        $lang = (int) (Input::get('lang')) ? Input::get('lang') : 0;
        $sites = array_keys(Config::get('sites'));
        if ($lang >= count($sites)) {
            die;
        }
        $lang_code = $sites[$lang];
        $cSites = count($sites);
        unset($sites);

        $count = 10;
        $page = (int) (Input::get('offset')) ? Input::get('offset') : 0;
        $offset = ($count - 1) * $page;

        $posts = $videos = $albums = array();

        if ($offset <= 90) {
            $posts = MongoPosts::where('video', 'exists', false)->where('lang', $lang_code)->where('gallery', '!=', 1)
                            ->orderBy('engagement', 'DESC')->take($count)->skip($offset)->get();
        }

        if ($offset <= 20) {
            $videos = MongoPosts::where('video', 'exists', true)->where('lang', $lang_code)->where('gallery', '!=', 1)
                            ->orderBy('engagement', 'DESC')->take($count)->skip($offset)->get();
        }

        if ($offset <= 25) {
            $albums = MongoPosts::where('video', 'exists', false)->where('lang', $lang_code)->where('gallery', '=', 1)
                            ->orderBy('engagement', 'DESC')->take($count)->skip($offset)->get();
        }
        unset($lang_code, $count, $offset);

        $this->handle_themost($posts, 1);
        $cPosts = count($posts);
        unset($posts);
        $this->handle_themost($videos, 2);
        $cVideos = count($videos);
        unset($videos);
        $this->handle_themost($albums, 3);
        $cAlbums = count($albums);
        unset($albums);

        if ($cPosts || $cVideos || $cAlbums) {
            $page = $page + 1;
            return Redirect::to('aggregator/themost?offset=' . $page);
        } else {
            if ($lang < $cSites) {
                $lang = $lang + 1;
                return Redirect::to('aggregator/themost?offset=0' . '&lang=' . $lang);
            } else {
                die;
            }
        }
    }

    private function handle_themost($posts = array(), $format) {
        $uploads = getcwd() . DIRECTORY_SEPARATOR . "uploads" . DIRECTORY_SEPARATOR;
        foreach ($posts as $key => $value) {
            $post = Most::find($value['id']);
            if (!$post) {
                $image = $value['image']['source'];
                $s3_path = '';
                $shares = 0;
                $comments = 0;
                $cat_name = '';
                $cat_id = 0;

                // get shares & comments
                if ($value['provider'] == '0' || $value['provider'] == 1) {
                    $shares = $value['shares'];
                    $comments = $value['comments'];
                } elseif ($value['provider'] == 2) {
                    $shares = $value['favorites'];
                    $comments = $value['retweets'];
                } elseif ($value['provider'] == 3) {
                    $shares = $value['favorites'];
                    $comments = $value['comments'];
                } elseif ($value['provider'] == 4) {
                    $shares = $value['likes'];
                    $comments = $value['comments'];
                }

                // category
                if (count($value['categories'])) {
                    foreach ($value['categories'] as $k1 => $cat) {
                        $cat_name = $cat['name'];
                        $catOb = Category::where('cat_name', '=', $cat_name)->select('cat_id')->first();
                        if ($catOb) {
                            $cat_id = $catOb->cat_id;
                        }
                        unset($catOb, $cat);
                        break;
                    }
                }

                // generates thumbs
                if ($image) {
                    $handle = @fopen($image, "r");
                    if ($handle) {
                        $imagePath = md5($image) . "." . $this->image_type($image);

                        $contentFile = file_get_contents($image);
                        $fp = fopen("$uploads$imagePath", "w");
                        fwrite($fp, $contentFile);
                        fclose($fp);

                        $s3_path = date('Y') . '/' . date('m') . '/' . $imagePath;
                        s3_save($s3_path);
                        set_themost_sizes($imagePath);
                        unlink("$uploads$imagePath");
                        unset($imagePath, $contentFile, $fp);
                    }
                    unset($handle);
                }
                unset($image);

                // save the most
                $post = new Most([
                    'title' => $value['title'],
                    'brief' => mb_substr($value['content'], 0, 150),
                    'image' => $s3_path,
                    'format' => $format,
                    'engagement' => $value['engagement'],
                    'date' => date('Y-m-d H:i:s', $value['date']->sec),
                    'shares' => $shares,
                    'comments' => $comments,
                    'cat_name' => $cat_name,
                    'cat_id' => $cat_id,
                    'source' => $value['source']['name'],
                    'mongo_id' => $value['id'],
                    'site' => $value['lang']
                ]);

                $post->save();
                unset($s3_path, $shares, $comments, $cat_name, $cat_id);
            } else {
                $post->engagement = $value['engagement'];
                $post->save();
            }
            unset($value, $post);
        }
    }

    function youtube($link = "") {
        $id = get_youtube_video_id($link);
        $details = get_youtube_video_details($id);

        $data["media_provider"] = "youtube";
        $data["media_provider_id"] = $id;
        $data["media_provider_image"] = $details->image;
        $data["media_type"] = "video";
        $data["media_path"] = "https://www.youtube.com/watch?v=" . $id;
        $data["media_title"] = $details->title;
        $data["media_description"] = $details->description;
        $data["media_duration"] = $details->length;
        $data["media_created_date"] = date("Y-m-d H:i:S");

        Media::insert($data);

        $media_id = DB::getPdo()->lastInsertId();

        return $media_id;
    }

    function soundcloud($link = "") {

        $details = get_soundcloud_track_details($link);
        $link = $details->link;

        $data["media_provider"] = "soundcloud";
        $data["media_provider_id"] = $details->id;
        $data["media_provider_image"] = $details->image;
        $data["media_type"] = "audio";
        $data["media_path"] = $link;
        $data["media_title"] = $details->title;
        $data["media_description"] = $details->description;
        $data["media_duration"] = $details->length;
        $data["media_created_date"] = date("Y-m-d H:i:S");

        Media::insert($data);

        $media_id = DB::getPdo()->lastInsertId();

        return $media_id;
    }

    function image_type($image = '') {
        $type = exif_imagetype($image);
        switch ($type) {
            case 1:
                $type = 'gif';
                break;
            case 2:
                $type = 'jpg';
                break;
            case 3:
                $type = 'png';
                break;
            default :
                break;
        }
        return $type;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function save_move() {


        $cat_id = Input::get('cats');
        $post_id = Input::get('post_id');
        $user_id = Auth::user()->id;
        $author_id = null;

        $post = DB::connection('mongodb')->collection('posts')->where('id', (int) $post_id)->first();

        $post_created_date = date("Y-m-d H:i:s", $post['date']->sec);
        $post_updated_date = date("Y-m-d H:i:s");

        $source_id = $post['source']['id'];
        $source_name = $post['source']['name'];
        $source_type = $post['source']['type'];
        $provider = $post['provider'];
        $lang = @$post['lang'];
        $link = $post['link'];
        $post_type = ($post['source']['type'] == 1) ? 'post' : (($post['source']['type'] == 2) ? 'article' : 'celebrity');
        $post_title = $post['title'];
        $post_excerpt = $post_title;
        $post_slug = create_slug($post['title']);
        $role_id = 9;

        if (@$post['source']['type'] == 2) {
            $author = User::firstOrCreate(array("first_name" => @$post['source']['name'], 'role_id' => $role_id));
            $author_id = $author->id;
        }

        // check if title found before
        $count = DB::table('posts')->where('post_title', '=', $post_title)->count();

        if ($count) {
            echo json_encode('found');
            exit;
        }

        $post_user_id = $user_id;
        $post_author_id = null;
        $post_subtitle = null;

        $post_status = 0;
        $post_format = 1;
        $post_desked = 0;
        $post_submitted = 0;
        $post_closed = 0;
        $translated = 0;
        $comment_options = '3';
        $post_content = $post['content'];
        $seo_title = string_sanitize(mb_substr($post_title, 0, 70));
        $tags = '';

        if (array_key_exists('tags', $post)) {
            foreach ($post['tags'] as $key => $value) {
                if ($key == 0) {
                    $tags .= $value['name'];
                } else {
                    $tags .= ',' . $value['name'];
                }
            }
        }

        $seo_keywords = $tags;
        $seo_description = string_sanitize(strip_tags(mb_substr($post_content, 0, 160)));
        $facebook_title = $seo_title;
        $facebook_description = $seo_description;
        $facebook_image = null;
        $twitter_description = $seo_description;

        $youtube_link = null;
        $has_media = null;
        $post_published_date = null;


        $image_id = null;
        $media_id = null;

        if (array_key_exists('video', $post)) {
            if (@$post['video']['type'] == 2) {

                $youtube = $youtube_link = $post['video']['source'];

                $pattern = "/\s*[a-zA-Z\/\/:\.]*youtube.com\/embed\/([a-zA-Z0-9\-_]+)([a-zA-Z0-9\/\*\-\_\?\&\;\%\=\.]*)/i";
                if (preg_match($pattern, $youtube_link, $matches)) {
                    $youtube = preg_replace($pattern, "http://www.youtube.com/v/$1", $youtube_link);
                }

                $media_id = $this->youtube($youtube);

                $post_format = 4;
                $post_content .= '<br><br><iframe width="650" height="400" src="' . $youtube_link . '" frameborder="0" allowfullscreen></iframe>';
            } else if (@$post['video']['type'] == 1) {
                $post_content .= '<br><br><video width="650" height="400" controls>
                                <source src="' . $youtube_link . '" type="video/mp4">
                            </video> ';
            }
        }

        if (array_key_exists('audio', $post)) {
            if (@$post['audio']['source']) {
                $soundcloud = urldecode($post['audio']['source']);

                $media_id = $this->soundcloud($soundcloud);

                $post_format = 3;

                $post_content .= '<br><br><iframe width="650" height="400" src="https://w.soundcloud.com/player/?url=' . $soundcloud . '&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false&amp;visual=true" frameborder="0" allowfullscreen></iframe>';
                ;
            }
        }

        if (@$post['gallery']) {
            $post_format = 5;
        }

        $image = $post['image']['source'];

        //if ($post_format != 3 && $post_format != 4) {
        if ($image) {
            $handle = @fopen($image, "r");
            if ($handle) {
                $uploads = getcwd() . DIRECTORY_SEPARATOR . "uploads" . DIRECTORY_SEPARATOR;
                $imageTemp = explode('?', $image);
                $imageStrp = $imageTemp[0];
                $imagePath = explode('/', $imageStrp);
                $imagePath = $imagePath[count($imagePath) - 1];
                $extension = explode('.', $imagePath);
                $extension = $extension[count($extension) - 1];
                $imagePath = time() * rand() . "." . strtolower($extension);

                $contentFile = file_get_contents($image);
                $fp = fopen("$uploads$imagePath", "w");
                fwrite($fp, $contentFile);
                fclose($fp);
                $media_hash = sha1_file($uploads . $imagePath);
                $imgfound = DB::table('media')->where('media_hash', '=', $media_hash)->first();

                $media_title = ($post['image']['caption']) ? $post['image']['caption'] : $post_title;

                if (!$imgfound) {
                    $s3_path = date('Y') . '/' . date('m') . '/' . $imagePath;
                    $image_id = Media::add_media(['media_provider' => null, 'media_provider_id' => null, 'media_provider_image' => null,
                                'media_path' => $s3_path, 'media_hash' => $media_hash, 'media_type' => 'image', 'media_title' => $post['image']['caption'], 'media_description' => null,
                                'media_created_date' => $post_updated_date, 'media_duration' => 0, 'conn' => 1]);
                    s3_save($s3_path);
                    set_sizes($imagePath);
                    unlink("$uploads$imagePath");
                } else {
                    $image_id = $imgfound->media_id;
                }
            }
        }
        //}
        // insert post
        $id = Post::add_post(['post_created_date' => $post_created_date, 'post_updated_date' => $post_updated_date, 'post_user_id' => $user_id, 'has_media' => $has_media,
                    'post_author_id' => $author_id, 'post_editor_id' => $user_id, 'post_image_id' => $image_id, 'post_media_id' => $media_id, 'image_hide' => 0, 'comment_options' => $comment_options,
                    'post_type' => $post_type, 'post_format' => $post_format, 'post_slug' => $post_slug, 'post_status' => $post_status,
                    'post_desked' => $post_desked, 'post_submitted' => $post_submitted, 'post_closed' => $post_closed, 'youtube_link' => $youtube_link,
                    'post_title' => $post_title, 'post_subtitle' => $post_subtitle, 'post_excerpt' => $post_excerpt, 'translated' => $translated,
                    'post_content' => $post_content, 'meta_title' => $seo_title, 'meta_keywords' => $seo_keywords, 'facebook_title' => $facebook_title,
                    'facebook_description' => $facebook_description, 'facebook_image' => $image_id, 'twitter_description' => $twitter_description,
                    'meta_description' => $seo_description, 'source_id' => $source_id, 'source_name' => $source_name, 'source_type' => $source_type,
                    'provider' => $provider, 'mongo_id' => $post_id, 'link' => $link, 'seen' => 0, 'conn' => $lang]);

        // insert tags
        if (array_key_exists('tags', $post)) {
            $tags = array();
            foreach ($post['tags'] as $key => $tag) {
                if (trim($tag['name']) != '') {
                    $tags[] = mb_strtolower($tag['name']);
                }
            }
            $tags = array_unique($tags);

            foreach ($tags as $key => $tag) {
                $check = Tag::get_tag_id(['name' => $tag])->first();
                if ($check) {
                    Tag::add_post_tags(['post_id' => $id, 'tag_id' => $check->tag_id]);
                } else {
                    $tag_id = Tag::add_tag(['name' => $tag, 'slug' => create_slug($tag)]);
                    Tag::add_post_tags(['post_id' => $id, 'tag_id' => $tag_id]);
                }
            }
        }


        Category::add_post_cats(['post_id' => $id, 'cat_id' => $cat_id]);

        echo json_encode($id);

        exit;
    }

}
