<?php

Menu::set("sidebar", function($menu) {
    //$menu->item('grabber', trans("grabber::grabber.module"), URL::route('dashboard.show'));
   
    $menu->item('dashboard.aggregator', trans("admin::common.aggregator_statistics"), URL::to(ADMIN . '/aggregator/stats'));
});

include __DIR__ . "/routes.php";
include __DIR__ . "/helpers.php";
