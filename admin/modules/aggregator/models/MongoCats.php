<?php

use Jenssegers\Mongodb\Model as Eloquent;

class MongoCats extends Eloquent {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $connection = 'mongodb';
    protected $collection = 'categories';

}
