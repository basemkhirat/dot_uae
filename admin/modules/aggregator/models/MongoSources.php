<?php

use Jenssegers\Mongodb\Model as Eloquent;

class MongoSources extends Eloquent {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $connection = 'mongodb';
    protected $collection = 'sources';

}
