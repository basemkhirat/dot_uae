<?php

use Jenssegers\Mongodb\Model as Eloquent;

class MongoPosts extends Eloquent {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $connection = 'mongodb';
    protected $collection = 'posts';

    public function scopefilter($query, $args = []) {

        if ($args['q'] && $args['search_type'] == 'post') {
            $query->where('title', 'LIKE', '%' . $args['q'] . '%');
        } else if ($args['q'] && $args['search_type'] == 'tag') {
            foreach (explode(' ', $args['q']) as $key => $value) {
                $query->orWhere('tags.name', 'LIKE', '%' . $value . '%');
            }
        }

        if ($args['source']) {
            $query->whereRaw(array('source.id' => (int) $args['source']));
        }

        if ($args['type']) {
            $query->whereRaw(array('source.type' => (int) $args['type']));
        }

        if ($args['cats']) {
            $query->whereRaw(array('categories.id' => (int) $args['cats']));
        }

        if ($args['tags']) {
            $query->whereRaw(array('tags.id' => (int) $args['tags']));
        }

        if ($args['site']) {
            $query->whereRaw(array('lang' => $args['site']));
        }

        if ($args['provider'] || $args['provider'] == '0') {
            $query->where('provider', (int) $args['provider']);
        }

        if ($args['format'] == 1) {
            $query->where('video', 'exists', true);
        } elseif ($args['format'] == 2) {
            $query->where('audio', 'exists', true);
        } elseif ($args['format'] == 3) {
            $query->where('gallery', 1);
        }

        return $query;
    }

}
