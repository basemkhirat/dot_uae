<?php

class Most extends Model {

    protected $table = 'themost';
    protected $fillable = [ 'title',
        'brief',
        'image',
        'format',
        'engagement',
        'date',
        'shares',
        'comments',
        'cat_name',
        'cat_id',
        'source',
        'mongo_id',
        'site'];
    public $timestamps = false;

}
