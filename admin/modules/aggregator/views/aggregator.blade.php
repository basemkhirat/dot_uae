@extends("admin::layouts.master")

<?php
$conn = Session::get('conn');
$params = '';
foreach (Input::except('per_page', 'page') as $key => $value) {
    $params .= '&' . $key . '=' . $value;
}

$langParams = '';
$lang = (Input::has('lang')) ? Input::get('lang') : 'ar';
foreach (Input::except('lang', 'per_page', 'page') as $key => $value) {
    $langParams .= '&' . $key . '=' . $value;
}

$orderParams = '';
foreach (Input::except('per_page', 'order_by', 'order_type', 'page') as $key => $value) {
    $orderParams .= '&' . $key . '=' . $value;
}

$rate_type = 'desc';
if (Input::get('order_by') == 'engagement') {
    $rate_type = (Input::get('order_type') == 'desc') ? 'asc' : 'desc';
}

if ($rate_type == 'asc') {
    $rate_arrow = 'up';
} else {
    $rate_arrow = 'down';
}

$date_type = 'asc';
if (Input::get('order_by') == 'date') {
    $date_type = (Input::get('order_type') == 'desc') ? 'asc' : 'desc';
}

if ($date_type == 'asc') {
    $date_arrow = 'up';
} else {
    $date_arrow = 'down';
}

$prop = '';

$outSources = url_except('sources', $prop);
$outCats = url_except('cats', $prop);
$outType = url_except('type', $prop);
$outProvider = url_except('provider', $prop);
$outTags = url_except('tags', $prop);
$outFormat = url_except('format', $prop);

$cats = Category::whereSite($conn)->get()->toArray();
?>

<style type="text/css">
    .m-t-md {
        margin-top: 5px !important;
    }
    .selected{
        color:#555;
    }

    .theme-default .table-primary .table-header, .theme-default .table-primary thead, .theme-default .table-primary thead th, .theme-default .table-primary thead tr {
        border-color: #6C7275 !important;
    }

    .theme-default .table-primary thead th, .theme-default .table-primary thead tr {
        background: none repeat scroll 0% 0% #6C7275;
    }
    .theme-default .table-primary .table-header, .theme-default .table-primary thead, .theme-default .table-primary thead th, .theme-default .table-primary thead tr {
        border-color: #6C7275 !important;
    }
    .theme-default .table-primary table {
        border-top-color: #6C7275 !important;
    }
    .theme-default .table-primary .table-header, .theme-default .table-primary thead, .theme-default .table-primary thead th, .theme-default .table-primary thead tr {
        border-color: #928989 !important;
    }
</style>

@section("breadcrumb")
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2><i class="fa fa-newspaper-o faa-tada animated faa-slow"></i>  {!!Lang::get('posts::posts.posts')!!} ({!!$posts->total()!!})</h2>
        <ol class="breadcrumb" style="background: none;">
            <li><a  href="{!!URL::to('/'.ADMIN.'/dashboard')!!}">{!!Lang::get('admin::common.dashboard')!!}</a></li>
            <li>{!!Lang::get('posts::posts.aggregator_posts')!!}</li>
        </ol>

    </div>


</div>
@stop

@section("content")

<div id="content-wrapper">

    <div class="row">

        <div class="col-lg-8">
            
            {!! Form::open(array('url' => ADMIN.'/aggregator', 'method' => 'get', 'style' => 'display: inline-block;', 'id' => 'filter')) !!}

            <select class="chosen-select chosen-rtl" id="sources" name="sources" tabindex="2" style="width:150px; display: inline-block;">
                <option value="" selected="selected">— {!!Lang::get('posts::posts.sources')!!} —</option>

                @foreach($sources as $key => $source)
                <option @if(Input::get('sources') == $source['id']) selected="selected" @endif value="{!!$source['id']!!}">{!!$source['name']!!}</option>
                @endforeach
            </select>

            <select class="chosen-select chosen-rtl" id="cats" name="cats"style="width:150px; display: inline-block;">
                <option value="" selected="selected">— {!!Lang::get('posts::posts.conn_cats')!!} —</option>

                @foreach($categories as $key => $value)
                <option @if(Input::get('cats') == $value['id']) selected="selected" @endif value="{!!$value['id']!!}">{!!$value['name']!!}</option>
                @endforeach

            </select>

            <select class="form-control" id="format" name="format" style="width:auto; display: inline-block;">
                <option value="" selected="selected">— {!!Lang::get('posts::posts.post_format')!!} —</option>
                <option value="1">{!!Lang::get('posts::posts.video')!!}</option>
                <option value="2">{!!Lang::get('posts::posts.audio')!!}</option>
                <option value="3">{!!Lang::get('posts::posts.gallery')!!}</option>

            </select>

            <select class="form-control" id="type" name="type" style="width:auto; display: inline-block;;">
                <option value="" selected="selected">— {!!Lang::get('posts::posts.type')!!} —</option>
                <option value="1">{!!Lang::get('posts::posts.news')!!}</option>
                <option value="2">{!!Lang::get('posts::posts.authors')!!}</option>
                <option value="3">{!!Lang::get('posts::posts.celebrities')!!}</option>

            </select>

            <select class="form-control" id="provider" name="provider" style="width:auto; display: inline-block;">
                <option value="" selected="selected">— {!!Lang::get('posts::posts.provider')!!} —</option>
                <option value="0">{!!Lang::get('posts::posts.news_site')!!}</option>
                <option value="1">{!!Lang::get('posts::posts.facebook')!!}</option>
                <option value="2">{!!Lang::get('posts::posts.twitter')!!}</option>
                <option value="3">{!!Lang::get('posts::posts.youtube')!!}</option>
                <option value="4">{!!Lang::get('posts::posts.instagram')!!}</option>
            </select>

            @if(Input::has('lang'))
            <input type="hidden" value="{!!Input::get('lang')!!}" name="lang">
            @endif

            <button class="btn btn-primary" type="submit" style="margin-top: -2px">{!!Lang::get('posts::posts.filter')!!}</button>
            {!! Form::close() !!}

        </div>

        <div class="col-lg-4">
            {!! Form::open(array('url' => ADMIN.'/aggregator', 'id' => 'search', 'method' => 'get', 'class' => 'choosen-rtl', 'style' => 'margin:0')) !!}

            <div class="input-group">
                <input type="text" placeholder="{!!Lang::get('posts::posts.search')!!}" class="input-sm form-control" value="{!!(Input::get('q')) ? Input::get('q') : ''!!}" name="q" id="q"> <span class="input-group-btn">
                    <button type="submit" class="btn btn-sm btn-primary"> {!!Lang::get('posts::posts.search')!!}</button> </span>
            </div>

            @if(Input::has('sources'))
            <input type="hidden" value="{!!Input::get('sources')!!}" name="sources">
            @endif

            @if(Input::has('cats'))
            <input type="hidden" value="{!!Input::get('cats')!!}" name="cats">
            @endif

            @if(Input::has('type'))
            <input type="hidden" value="{!!Input::get('type')!!}" name="type">
            @endif

            @if(Input::has('provider'))
            <input type="hidden" value="{!!Input::get('provider')!!}" name="provider">
            @endif

            @if(Input::has('tags'))
            <input type="hidden" value="{!!Input::get('tags')!!}" name="tags">
            @endif

            @if(Input::has('format'))
            <input type="hidden" value="{!!Input::get('format')!!}" name="format">
            @endif

            <div class="input-group no-margin" >


                <div class="form-group" style="display:inline-block; margin-bottom: 0">
                    <div class="radio i-checks"><label style="padding:0"> <input @if(Input::get('search_type') == 'tag') checked="checked" @endif type="radio" name="search_type" value="tag">&nbsp;&nbsp; {!!Lang::get('posts::posts.tags')!!} </label></div>
                </div>
                &nbsp;&nbsp;&nbsp;&nbsp;
                <div class="form-group" style="display:inline-block; margin-bottom: 0"">
                    <div class="radio i-checks"><label style="padding:0"> <input @if(Input::get('search_type') == 'post') checked="checked" @endif type="radio" name="search_type" value="post">&nbsp;&nbsp; {!!Lang::get('posts::posts.posts')!!} </label></div>
                </div>
            </div>

            {!! Form::close() !!}
        </div>
    </div>

    {!! Form::open(array('url' =>ADMIN.'/aggregator/delete', 'method' => 'POST', 'id' => 'form')) !!}
    <div class="row wrapper animated fadeInRight ">

            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5> {!!Lang::get('posts::posts.posts')!!} </h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>

                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-sm-10 m-b-xs">

                            <select name="action" id="action" class="form-control" style="width:auto; display: inline-block;">
                                <option value="" selected="selected">{!!Lang::get('posts::posts.bulk')!!}</option>

                                <option value="0">{!!Lang::get('posts::posts.delete')!!}</option>

                            </select>
                            <button class="btn btn-primary">{!!Lang::get('posts::posts.apply')!!}</button>
                        </div>

                        <div class="col-sm-2">

                            <select class="form-control chosen-rtl" id="per_page" name="per_page">
                                <option selected="selected" value="">-- {!!Lang::get('posts::posts.per_page')!!} --</option>
                                <option selected="selected" value="10">10</option>
                                <option value="20">20</option>
                                <option value="30">30</option>
                                <option value="40">40</option>
                            </select>
                        </div>
                    </div>

                    <div class="table-responsive">

                        <table cellspacing="0" cellpadding="0" border="0" class="table table-striped">
                            <thead>
                                <tr>

                                    <th><div class="action-checkbox"><label class="px-single"><input type="checkbox" name="checkall" id="checkall" value="" class="i-checks"><span class="lbl"></span></label></div></th>
                                    <th style="min-width: 150px;">{!!Lang::get('posts::posts.title')!!}</th>
                                    <th>{!!Lang::get('posts::posts.source')!!}</th>
                                    <th style="min-width: 100px;">{!!Lang::get('posts::posts.provider')!!}</th>
                                    <th>{!!Lang::get('posts::posts.categories')!!}</th>
                                    <th>{!!Lang::get('posts::posts.tags')!!}</th>
                                    <th style="min-width: 80px;"><a href="{!!URL::to(ADMIN."/aggregator")!!}?order_by=engagement&order_type={!!$rate_type!!}{!!$orderParams!!}">{!!Lang::get('posts::posts.engagement_rate')!!}&nbsp;&nbsp;&nbsp;<i class="fa fa-arrow-{!!$rate_arrow!!}"></i></a></th>
                                    <th style="min-width: 150px;""><a href="{!!URL::to(ADMIN."/aggregator")!!}?order_by=date&order_type={!!$date_type!!}{!!$orderParams!!}">{!!Lang::get('posts::posts.date')!!}&nbsp;&nbsp;&nbsp;<i class="fa fa-arrow-{!!$date_arrow!!}"></i></a></th>
                                </tr>
                            </thead>
                            <tbody >
                                <?php $count = count($posts); ?>
                                @if($count)
                                @foreach($posts as $post)
                                <tr class="odd gradeX" >
                                    <td>

                                        <div class="action-checkbox"><label class="px-single"><input type="checkbox" name="check[]" value="{!!$post['id']!!}" class="i-checks"><span class="lbl"></span></label></div>

                                    </td>
                                    <td>
                                        <a href="{!!URL::to('/'.ADMIN.'/aggregator/'.$post['id'].'/edit')!!}" > {!!$post['title']!!}</a>
                                        <div class="tooltip-demo m-t-md">
                                            <a class="btn btn-white btn-sm" data-toggle="tooltip" data-placement="top" href="{!!$post['link']!!}" target="_blank" title="{!!Lang::get('posts::posts.preview_changes')!!}"><i class="fa fa-eye"></i></a>
                                            <a data-toggle="tooltip" data-placement="top" data-type="delete" href="{!!URL::to('/'.ADMIN.'/aggregator/'.$post['id'].'/delete')!!}" class="btn btn-white btn-sm trash_post" message="{!!Lang::get('posts::posts.delete_post_q')!!}" title="{!!Lang::get('posts::posts.delete')!!}"><i class="fa fa-trash-o"></i></a>
                                        </div>
                                    </td>
                                    <td><a href="{!!URL::to(ADMIN.'/aggregator')!!}?sources={!!$post['source']['id']!!}">{!!$post['source']['name']!!}</a></td>
                                    <td><a href="{!!URL::to(ADMIN.'/aggregator')!!}?provider={!!$post['provider']!!}">@if($post['provider'] == 0) {!!Lang::get('posts::posts.news_site')!!} @elseif($post['provider'] == 1) {!!Lang::get('posts::posts.facebook')!!} @elseif($post['provider'] == 2) {!!Lang::get('posts::posts.twitter')!!} @elseif($post['provider'] == 3) {!!Lang::get('posts::posts.youtube')!!} @elseif($post['provider'] == 4) {!!Lang::get('posts::posts.instagram')!!} @endif</a></td>
                                    <td class="center" style="">
                                        <?php
                                        $i = 0;
                                        $len = count($post['categories']);
                                        ?>

                                        @if($len)
                                        @foreach($post['categories'] as $cat)
                                        <a href="{!!URL::to('/'.ADMIN.'/aggregator?cats='.$cat['id'])!!}">{!!$cat['name']!!}</a>
                                        @if ($i != $len - 1)                                         
                                        ,&nbsp;
                                        @endif
                                        <?php $i++; ?>
                                        @endforeach
                                        @else
                                        —
                                        @endif
                                    </td>
                                    <td class="center" style="">
                                        <?php
                                        $i = 0;
                                        $len = count($post['tags']);
                                        ?>

                                        @if($len)
                                        @foreach($post['tags'] as $tag)
                                        @if(trim($tag['name']) != '')
                                        <a href="{!!URL::to('/'.ADMIN.'/aggregator?tags='.$tag['id'])!!}">{!!$tag['name']!!}</a>
                                        @if ($i != $len - 1)                                         
                                        ,&nbsp;
                                        @endif
                                        <?php $i++; ?>
                                        @endif
                                        @endforeach
                                        @else
                                        —
                                        @endif
                                    </td>
                                    <td class="center" style="color: #555;"><span class="label label-pa-purple">@if ($post['provider'] != 0) {!!number_format($post['engagement'] * 100, 4)!!}% @else {!!$post['engagement']!!} @endif</span></td>

                                    <td class="center" style="color: #555;">@if(DIRECTION == 'rtl') {!!arabic_date($post['date']->sec, 'mongo')!!} @else {!! date('M d, Y', $post['date']->sec) !!} @ {!! date('h:i a', $post['date']->sec) !!} @endif</td>

                                </tr>
                                @endforeach
                                @else
                                <tr class="odd gradeX" style="height:200px">
                                    <td colspan="9" style="vertical-align:middle; text-align:center; font-weight:bold; font-size:22px">{!!Lang::get('posts::posts.no_posts')!!}</td>
                                </tr>
                                @endif
                            </tbody>

                        </table>
                        <div class="panel-footer text-right" style="border-top: 1px solid #ececec; background: none;">
                            <div class="col-sm-2 m-b-xs" style="padding:0">
                                @if($count)
                                {!!Lang::get('posts::posts.showing')!!} {!!$posts->firstItem()!!} {!!Lang::get('posts::posts.to')!!} {!!$posts->lastItem()!!} {!!Lang::get('posts::posts.of')!!} {!!$posts->total()!!} {!!Lang::get('posts::posts.post')!!}
                                @endif
                            </div>

                            <div class="col-sm-10 m-b-xs" style="padding:0">
                                <div class="pull-right">
                                    @if($count)
                                    {!!$posts->appends(Input::all())->setPath('')->render()!!}
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>
    {!! Form::close() !!}


<script src="<?php echo assets() ?>/js/bootbox.min.js"></script>

<script type="text/javascript">


$(document).ready(function () {
    $('#per_page').change(function () {
        location.href = '{!!URL::to(ADMIN."/aggregator")!!}' + '?per_page=' + $(this).val() + '{!!$params!!}'

    });

    $('#per_page').val("{!!Input::get('per_page')!!}");

    // check all action
    $('#checkall').on('ifChecked', function (event) {
        $("input[name='check[]']").each(function () {
            $(this).iCheck('check');
        });
    });
    $('#checkall').on('ifUnchecked', function (event) {
        $("input[name='check[]']").each(function () {
            $(this).iCheck('uncheck');
        });
    });
    $('.i-checks').iCheck({
        checkboxClass: 'icheckbox_square-green',
        radioClass: 'iradio_square-green',
    });
    
    $('.trash_post').on('click', function (e) {
        e.preventDefault();
        $this = $(this);
        bootbox.dialog({
            message: $this.attr('message'),
            title: $this.attr('title'),
            buttons: {
                success: {
                    label: "{!!Lang::get('posts::posts.ok')!!}",
                    className: "btn-success",
                    callback: function () {
                        location.href = $this.attr('href');
                    }
                },
                danger: {
                    label: "{!!Lang::get('posts::posts.cancel')!!}",
                    className: "btn-primary",
                    callback: function () {
                    }
                },
            },
            className: "bootbox-sm"
        });
    });
        
    // form submit
    $("#form").submit(function (e) {
        if ($("input[name='check[]']:checked").length == 0 || $("#action").val() == '') {
            return false;
        }
    });

    $("#filter").submit(function (e) {
        if ($("#cats").val() == '' && $("#sources").val() == '' && $("#type").val() == '' && $("#provider").val() == '' && $("#format").val() == '') {
            return false;
        }
    });

    $("#order-form").submit(function (e) {

        if ($("#order_by").val() == '') {
            return false;
        }
    });

//$("#cats").chained("#sources"); /* or $("#series").chainedTo("#mark"); */

    $("#sources").val("{!!Input::get('sources')!!}");
//$("#sources").trigger('change');
    $("#cats").val("{!!Input::get('cats')!!}");
    $("#type").val("{!!Input::get('type')!!}");
    $("#provider").val("{!!Input::get('provider')!!}");
    $("#format").val("{!!Input::get('format')!!}");

//$("input[name=search_type][value= {!!Input::get('search_type')!!}]").attr('checked', 'checked');

    $("#save_move").click(function (e) {
        e.preventDefault();

        if ($("[name=to_cats]:checked").size() == 0) {
            return false;
        }

        $('#overlay').show();

        ajaxData = {
            cats: $('[name=to_cats]:checked').val(),
            post_id: $("#post_temp").val(),
        };
        console.log(ajaxData);
        $.ajax({
            type: "POST",
            dataType: 'json',
            url: '{!! URL::to("/".ADMIN."/aggregator/savemove") !!}',
            data: ajaxData,
            beforeSend: function (res) {

            },
            success: function (res) {
                $('#overlay').hide();

                if (res == 'found') {
                    $.growl.error({title: "<strong>{!!Lang::get('posts::posts.error')!!}</strong> ", message: "{!!Lang::get('posts::posts.repeated')!!}", duration: 3000});
                } else {
                    $.growl.notice({title: "<strong>{!!Lang::get('posts::posts.well_done')!!}</strong> ", message: "{!!Lang::get('posts::posts.moved')!!}", duration: 3000});
                }
            },
            complete: function () {
                //$('#save_load').remove();
                //$(".move").show();
            },
            error: function () {
                $('#overlay').hide();
                $.growl.error({title: "<strong>{!!Lang::get('posts::posts.error')!!}</strong> ", message: "{!!Lang::get('posts::posts.not_moved')!!}", duration: 3000});
            }
        });
    });

    $("#close_move").click(function (e) {
        $('#save_load').remove();
        $(".move").show();
    });

    $(".move").click(function (e) {
        e.preventDefault();
        var id = $(this).attr("href").substr(1);
        $("#post_temp").val(id);
        $("#move").click();
        //$(this).hide();
        //$(this).parent().append("<img src='{!!URL::to('/')!!}/packages/admin/images/loader.gif' id='save_load'>");
    });

    var config = {
        '.chosen-select': {},
        '.chosen-select-deselect': {allow_single_deselect: true},
        '.chosen-select-no-single': {disable_search_threshold: 10},
        '.chosen-select-no-results': {no_results_text: "{!!Lang::get('posts::posts.notfound')!!}"},
        '.chosen-select-width': {width: "95%"}
    }
    for (var selector in config) {
        $(selector).chosen(config[selector]);
    }
    
    // search submit
    $("#search").submit(function (e) {
        if($("#q").val() == ''){
            return false
        }
    });
});
</script>


@stop
