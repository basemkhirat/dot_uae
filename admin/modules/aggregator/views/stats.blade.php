@extends("admin::layouts.master")

<?php
$sites = Config::get('sites');
$posts_sources = array_slice($posts_sources, count($posts_sources) - 20);
$posts_cats = array_slice($posts_cats, count($posts_cats) - 20);
?>

@section("breadcrumb")
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-6">
        <h2><i class="fa fa-info-circle faa-tada animated faa-slow"></i> {!!Lang::get('posts::posts.aggregator_stats')!!}</h2>
        <ol class="breadcrumb" style="background: none;">
            <li><a  href="{!!URL::to('/'.ADMIN.'/dashboard')!!}">{!!Lang::get('admin::common.dashboard')!!}</a></li>
            <li>{!!Lang::get('posts::posts.aggregator_stats')!!}</li>
        </ol>
    </div>
    <div class="col-lg-6" style="padding: 0">
        <!-- Search field -->
        <form action="#" class="pull-right col-xs-12 col-sm-12" style="margin-top: 30px">
            <div style="display: inline-block" class="col-xs-12 col-sm-6">
                <select class="form-control chosen-select chosen-rtl" name="cats" id="category_filter" style="width:100%">
                    <option value="">{!!Lang::get('posts::posts.all_categories')!!}</option>
                    @foreach($categories as $key => $value)
                    <option @if(Input::get('cats') == $value['id']) selected="selected" @endif value="{!!$value['id']!!}">{!!$value['name']!!}</option>
                    @endforeach

                </select>
            </div>

            <div style="display: inline-block" class="col-xs-12 col-sm-6">
                <select class="form-control chosen-select chosen-rtl" name="sources" id="source_filter" style="width:100%">
                    <option value="">{!!Lang::get('posts::posts.all_sources')!!}</option>
                    @foreach($sources as $key => $value)
                    <option @if(Input::get('sources') == $value['id']) selected="selected" @endif value="{!!$value['id']!!}">{!!$value['name']!!}</option>
                    @endforeach

                </select>
            </div>
        </form>
    </div>
</div>
@stop

@section("content")

<link href="<?php echo assets() ?>/css/plugins/morris/morris-0.4.3.min.css" rel="stylesheet">
<style>
    .panel-body {
        padding: 0;
    }
</style>
<div id="content-wrapper" class="chosen-rtl">
    <div class="row wrapper animated fadeInRight ">
        <div class="panel-body row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>{!!Lang::get('posts::posts.stats')!!}</h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>

                            <a class="close-link">
                                <i class="fa fa-times"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content">
                        <div class="row">
                            <div class="col-lg-9">
                                <?php
                                $ctotal = 0;
                                foreach ($posts_charts as $key => $value) {
                                    $ctotal += $value;
                                }
                                ?>
                                @if($ctotal != 0)
                                <div class="flot-chart">
                                    <div class="flot-chart-content" id="flot-dashboard-chart"></div>
                                </div>
                                @else
                                {!!Lang::get('posts::posts.no_posts')!!}
                                @endif

                            </div>
                            <div class="col-lg-3">
                                <ul class="list-group clear-list m-t">

                                    <li class="list-group-item fist-item" ><span >{!!Lang::get('posts::posts.posts')!!}</span> <span class="label label-pa-purple pull-right">{!!@$posts!!}</span> </li>
                                    <li class="list-group-item" ><span >{!!Lang::get('posts::posts.categories')!!}</span> <span class="label label-pa-purple pull-right">{!!count(@$posts_cats)!!}</span> </li>
                                    <li class="list-group-item" ><span >{!!Lang::get('posts::posts.all_sources')!!}</span> <span class="label label-pa-purple pull-right">{!!count(@$sources)!!}</span> </li>

                                </ul>
                            </div>
                        </div>
                    </div>

                </div>
            </div>


        </div>
        <!-- /9. $UNIQUE_VISITORS_STAT_PANEL -->

        <div class="panel-body row">
            <div class="col-xs-12 col-sm-6" >


                <?php
                $total = @$site_posts + @$facebook_posts + @$twitter_posts + @$youtube_posts + @$instagram_posts;
                $sratio = 0;
                $fratio = 0;
                $tratio = 0;
                $yratio = 0;
                $iratio = 0;
                if ($total != 0) {
                    $sratio = round((@$site_posts/$total) * 100, 2);
                    $fratio = round((@$facebook_posts/$total) * 100, 2);
                    $tratio = round((@$twitter_posts/$total) * 100, 2);
                    $yratio = round((@$youtube_posts/$total) * 100, 2);
                    $iratio = round((@$instagram_posts/$total) * 100, 2);
                }
                ?>
                <!-- 8. $MORRISJS_DONUT ============================================================================
    
                        Morris.js Donut
                -->
                <!-- Javascript -->
            @if($total != 0)
                <script>
                    $(function() {
                    Morris.Donut({
                    element: 'hero-donut',
                            data: [
                            { label: "{!!Lang::get('posts::posts.news_sites')!!}", value: {!!$sratio!!} },
                            { label: "{!!Lang::get('posts::posts.facebook')!!}", value: {!!$fratio!!} },
                            { label: "{!!Lang::get('posts::posts.twitter')!!}", value: {!!$tratio!!} },
                            { label: "{!!Lang::get('posts::posts.youtube')!!}", value: {!!$yratio!!} },
                            { label: "{!!Lang::get('posts::posts.instagram')!!}", value: {!!$iratio!!} }
                            ],
                            colors: ['#87d6c6', '#54cdb4', '#1ab394'],
                            resize: true,
                            labelColor: '#888',
                            formatter: function (y) { return y + "%" }
                    });
                    });</script>
            @endif
                <!--/Javascript -->

                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>{!!Lang::get('posts::posts.provider')!!} - {!!Lang::get('posts::posts.posts')!!}</h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>

                            <a class="close-link">
                                <i class="fa fa-times"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content">
                        @if($total != 0)
                        <div id="hero-donut" class="graph"></div>
                        @else
                        {!!Lang::get('posts::posts.no_posts')!!}
                        @endif
                    </div>
                </div>

            </div>

            <div class="col-xs-12 col-sm-6" >
                <?php
                $total = 0;
                foreach ($sites as $key => $value) {
                    $total += @${"posts_" . $key};
                }

                foreach ($sites as $key => $value) {
                    if ($total != 0) {
                        @${$key . "ratio"} = round((@${"posts_" . $key}/$total) * 100, 2);
                    }
                }
                ?>
                <!-- 8. $MORRISJS_DONUT ============================================================================
    
                        Morris.js Donut
                -->
                <!-- Javascript -->
            @if($total != 0)
                <script>
                            $(function() {
                            Morris.Donut({
                            element: 'hero-donut1',
                                    data: [
                                            @foreach ($sites as $key=>$value)
                                    { label: "{!!Lang::get('posts::posts.'.$key)!!}", value: <?php echo @${$key . "ratio"}; ?> },
                                            @endforeach
                                    ],
                                    colors: ['#87d6c6', '#54cdb4', '#1ab394'],
                                    resize: true,
                                    labelColor: '#888',
                                    formatter: function (y) { return y + "%" }
                            });
                            });
                    </script>
            @endif
                <!--/Javascript -->

                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>{!!Lang::get('posts::posts.languages')!!} - {!!Lang::get('posts::posts.posts')!!}</h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>

                            <a class="close-link">
                                <i class="fa fa-times"></i>
                            </a>
                        </div>
                    </div>

                    <div class="ibox-content">
                        @if($total != 0)
                        <div id="hero-donut1" class="graph"></div>
                        @else
                        {!!Lang::get('posts::posts.no_posts')!!}
                        @endif
                    </div>
                </div>

            </div>
        </div>

        
        <div class="panel-body row">


            <div class="ibox float-e-margins">

                <div class="ibox-title">
                    <h5>{!!Lang::get('posts::posts.all_sources')!!} - {!!Lang::get('posts::posts.posts')!!}</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>

                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <?php
                    $stotal = 0;
                    foreach ($posts_sources as $key => $value) {
                        $stotal += $value;
                    }
                    ?>
                    @if($stotal != 0)
                    <div class="flot-chart">
                        <div id="sources-bar" class="flot-chart-content"></div>
                    </div>
                    @else
                    {!!Lang::get('posts::posts.no_posts')!!}
                    @endif
                </div>
            </div>
            <!-- /7. $MORRISJS_BARS -->


        </div>
       



        <div class="panel-body row">


            <div class="ibox float-e-margins">

                <div class="ibox-title">
                    <h5>{!!Lang::get('posts::posts.categories')!!} - {!!Lang::get('posts::posts.posts')!!}</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>

                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <?php
                    $atotal = 0;
                    foreach ($posts_cats as $key => $value) {
                        $atotal += $value;
                    }
                    ?>
                    @if($atotal != 0)
                    <div class="flot-chart">
                        <div id="cats-bar" class="flot-chart-content"></div>
                    </div>
                    @else
                    {!!Lang::get('posts::posts.no_posts')!!}
                    @endif
                </div>
            </div>
            <!-- /7. $MORRISJS_BARS -->


        </div>



    </div> <!--/#content-wrapper -->
</div>

<script src="<?php echo assets() ?>/js/plugins/flot/jquery.flot.js"></script>
<script src="<?php echo assets() ?>/js/plugins/flot/jquery.flot.tooltip.min.js"></script>
<script src="<?php echo assets() ?>/js/plugins/flot/jquery.flot.time.js"></script>
<script src="<?php echo assets() ?>/js/plugins/flot/jquery.flot.pie.js"></script>
<script src="<?php echo assets() ?>/js/plugins/easypiechart/jquery.easypiechart.js"></script>
<script src="<?php echo assets() ?>/js/plugins/morris/raphael-2.1.0.min.js"></script>
<script src="<?php echo assets() ?>/js/plugins/morris/morris.js"></script>


<script>
$(document).ready(function() {
var config = {
    '.chosen-select': {},
    '.chosen-select-deselect': {
    allow_single_deselect: true
    },
    '.chosen-select-no-single': {
    disable_search_threshold: 10
    },
    '.chosen-select-no-results': {
    no_results_text: "{!!Lang::get('posts::posts.notfound')!!}"
    },
    '.chosen-select-width': {
    width: "95%"
    }
}
for (var selector in config) {
$(selector).chosen(config[selector]);
}

$("#category_filter").change(function() {
var base = $(this);
        location.href = "<?php echo URL::to(ADMIN . "/aggregator/stats?cats=") ?>" + base.val();
});
        $("#source_filter").change(function() {
var base = $(this);
        location.href = "<?php echo URL::to(ADMIN . "/aggregator/stats?sources=") ?>" + base.val();
});
        $("#category_filter").val("{!!Input::get('cats')!!}");
        $("#source_filter").val("{!!Input::get('sources')!!}");
        
@if ($ctotal != 0)
        $(function() {
        var data2 = [
                @foreach($posts_charts as $date=>$count)
                [gd({!!date("Y", strtotime($date))!!}, {!!date("n", strtotime($date))!!}, {!!date("j", strtotime($date))!!}), {!!$count!!}],
                @endforeach

        ];
                var dataset = [{
                label: "{!!Lang::get('posts::posts.posts')!!}",
                        data: data2,
                        yaxis: 2,
                        color: "#464f88",
                        lines: {
                        lineWidth: 1,
                                show: true,
                                fill: true,
                                fillColor: {
                                colors: [{
                                opacity: 0.2
                                }, {
                                opacity: 0.2
                                }]
                                }
                        },
                        splines: {
                        show: false,
                                tension: 0.6,
                                lineWidth: 1,
                                fill: 0.1
                        },
                }];
                var options = {
                xaxis: {
                mode: "time",
                        tickSize: [3, "day"],
                        tickLength: 0,
                        axisLabel: "Date",
                        axisLabelUseCanvas: true,
                        axisLabelFontSizePixels: 12,
                        axisLabelFontFamily: 'Arial',
                        axisLabelPadding: 10,
                        color: "#d5d5d5"
                },
                        yaxes: [{
                        position: "left",
                                max: 1070,
                                color: "#d5d5d5",
                                axisLabelUseCanvas: true,
                                axisLabelFontSizePixels: 12,
                                axisLabelFontFamily: 'Arial',
                                axisLabelPadding: 3
                        }, {
                        position: "right",
                                clolor: "#d5d5d5",
                                axisLabelUseCanvas: true,
                                axisLabelFontSizePixels: 12,
                                axisLabelFontFamily: ' Arial',
                                axisLabelPadding: 67
                        }],
                        legend: {
                        noColumns: 1,
                                labelBoxBorderColor: "#000000",
                                position: "nw"
                        },
                        tooltip: true,
                        tooltipOpts: {
                        content: "x: %x, y: %y"
                        },
                        grid: {
                        hoverable: true,
                                clickable: true,
                                borderWidth: 0
                        }
                };
                $.plot($("#flot-dashboard-chart"), dataset, options);
        });
@endif


        //Flot Bar Chart
        $(function() {
        
        @if ($atotal != 0)
                var barOptions1 = {
                series: {
                bars: {
                show: true,
                        barWidth: 0.6,
                        fill: true,
                        fillColor: {
                        colors: [{
                        opacity: 0.8
                        }, {
                        opacity: 0.8
                        }]
                        }
                }
                },
                        xaxis: {
                        ticks: [
                                <?php $i = 0; ?>
                                @foreach($posts_cats as $category=>$count)
                                @if ($count != 0)
                                [{!!$i!!}, '{!!$category!!}'],
                                <?php $i++; ?>
                                @endif
                                @endforeach
                        ],
                                tickDecimals: 0
                        },
                        colors: ["#1ab394"],
                        grid: {
                        color: "#999999",
                                hoverable: true,
                                clickable: true,
                                tickColor: "#D4D4D4",
                                borderWidth:0
                        },
                        legend: {
                        show: false
                        },
                        tooltip: true,
                        tooltipOpts: {
                        content: "x: %x, y: %y"
                        }
                };
                
                <?php $i = 0; ?>
                var barData1 = {
                label: "bar",
                        data: [
                                @foreach($posts_cats as $category=>$count)
                                @if ($count != 0)
                                [{!!$i!!}, {!!$count!!}],
                                <?php $i++; ?>
                                @endif
                                @endforeach
                        ]
                };
                
                $.plot($("#cats-bar"), [barData1], barOptions1);
        @endif

        @if ($stotal != 0)
                var barOptions2 = {
                series: {
                bars: {
                show: true,
                        barWidth: 0.6,
                        fill: true,
                        fillColor: {
                        colors: [{
                        opacity: 0.8
                        }, {
                        opacity: 0.8
                        }]
                        }
                }
                },
                        xaxis: {
                        ticks: [
                                <?php $i = 0; ?>
                                @foreach($posts_sources as $source=>$count)
                                @if($count != 0)
                                [{!!$i!!}, '{!!$source!!}'],
                                <?php $i++; ?>
                                @endif
                                @endforeach
                        ],
                                tickDecimals: 0
                        },
                        colors: ["#1ab394"],
                        grid: {
                        color: "#999999",
                                hoverable: true,
                                clickable: true,
                                tickColor: "#D4D4D4",
                                borderWidth:0
                        },
                        legend: {
                        show: false
                        },
                        tooltip: true,
                        tooltipOpts: {
                        content: "x: %x, y: %y"
                        }
                };
                <?php $i = 0; ?>
                var barData2 = {
                label: "bar",
                        data: [
                                @foreach($posts_sources as $source=>$count)
                                @if($count != 0)
                                [{!!$i!!}, {!!$count!!}],
                                <?php $i++; ?>
                                @endif
                                @endforeach
                        ]
                };
                $.plot($("#sources-bar"), [barData2], barOptions2);
        @endif


        });
        
        function gd(year, month, day) {
            return new Date(year, month - 1, day).getTime();
        }

});


</script>
@stop