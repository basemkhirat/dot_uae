@extends("admin::layouts.master")

<?php
$conn = Session::get('conn');
?>

@section("breadcrumb")
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2><i class="fa fa-newspaper-o faa-tada animated faa-slow"></i> {!!Lang::get('posts::posts.aggregator_posts')!!}</h2>
        <ol class="breadcrumb" style="background: none;">
            <li><a  href="{!!URL::to('/'.ADMIN.'/dashboard')!!}">{!!Lang::get('admin::common.dashboard')!!}</a></li>
            <li><a  href="{!!URL::to('/'.ADMIN.'/aggregator')!!}">{!!Lang::get('posts::posts.aggregator_posts')!!}</a></li>
            <li>{!!Lang::get('posts::posts.preview')!!}</li>
        </ol>
    </div>

</div>
@stop
@section("content")

<!-- orris -->
<link href="<?php echo assets() ?>/css/plugins/morris/morris-0.4.3.min.css" rel="stylesheet">
<link href="<?php echo assets() ?>/css/plugins/blueimp/css/blueimp-gallery.min.css" rel="stylesheet">

<style>
    .tag-list li span {
        font-size: 10px;
        background-color: #F3F3F4;
        padding: 5px 12px;
        color: inherit;
        border-radius: 2px;
        border: 1px solid #E7EAEC;
        margin-right: 5px;
        margin-top: 5px;
        display: block;
    }
</style>

<div id="content-wrapper">

    <div class="row">

        <div class="row wrapper animated fadeInRight ">

            <div class="col-md-8">
                <div class="ibox float-e-margins">

                    <div class="ibox-content" >
                        <div class="text-center article-title">
                            <span class="text-muted"><i class="fa fa-clock-o"></i> @if(DIRECTION == 'rtl') {!!arabic_date($post['date']->sec, 'mongo')!!} @else {!! date('M d, Y', $post['date']->sec) !!} @ {!! date('h:i a', $post['date']->sec) !!} @endif </span>
                            <h1>
                                {!!Input::old('title', $post['title'])!!}
                            </h1>
                        </div>
                        <div class=" full-height-scroll white-bg">
                            <div style="padding: 20px">
                                <?php
                                $content = $post['content'];
                                if ($post['video']['type'] == 2) {
                                    $video = $post['video']['source'];

                                    $content .= '<br><br><iframe width="650" height="400" src="' . $video . '" frameborder="0" allowfullscreen></iframe>';
                                } elseif ($post['video']['type'] == 1) {
                                    $content .= '<br><br><video width="650" height="400" controls>
                                            <source src="' . $post['video']['source'] . '" type="video/mp4">
                                          </video> ';
                                }

                                if ($post['audio']['source']) {
                                    $content .= '<br><br><iframe width="650" height="400" src="https://w.soundcloud.com/player/?url=' . urldecode($post['audio']['source']) . '&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false&amp;visual=true" frameborder="0" allowfullscreen></iframe>';
                                }
                                ?>
                                @if($content)

                                {!!$content!!}

                                @endif

                                <hr>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="small text-right">
                                            <h5>{!!Lang::get('posts::posts.tags')!!}</h5>

                                            @if(count($post['tags']))
                                            <ul class="tag-list pull-left" style="padding: 0">   
                                                @foreach($post['tags'] as $tag)
                                                @if(trim($tag['name']) != '')
                                                <li><span>{!! $tag['name'] !!}</span></li>
                                                @endif
                                                @endforeach
                                            </ul>
                                            @else
                                            {!!Lang::get('posts::posts.no_tags')!!}
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="small text-right">
                                            <h5>{!!Lang::get('posts::posts.categories')!!}</h5>

                                            @if(count($post['categories']))
                                            <ul class="tag-list pull-left" style="padding: 0">   
                                                @foreach($post['categories'] as $cat)
                                                @if(trim($cat['name']) != '')
                                                <li><span>{!! $cat['name'] !!}</span></li>
                                                @endif
                                                @endforeach
                                            </ul>
                                            @else
                                            {!!Lang::get('posts::posts.no_categories')!!}
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

            </div>

            <div class="col-md-4">
                <div class="row">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>{!!Lang::get('posts::posts.post_info')!!}</h5>
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>

                                <a class="close-link">
                                    <i class="fa fa-times"></i>
                                </a>
                            </div>
                        </div>
                        <div class="ibox-content">
                            <div class="panel-body">

                                <div class="form-group">    
                                    <p><i class="fa fa-clock-o"></i>&nbsp;&nbsp;&nbsp;&nbsp;{!!Lang::get('posts::posts.date')!!}:<strong> @if(DIRECTION == 'rtl') {!!arabic_date($post['date']->sec, 'mongo')!!} @else {!! date('M d, Y', $post['date']->sec) !!} @ {!! date('h:i a', $post['date']->sec) !!} @endif </strong>

                                    </p>
                                    @if($post['source_date'])
                                    <p><i class="fa fa-clock-o"></i>&nbsp;&nbsp;&nbsp;&nbsp;{!!Lang::get('posts::posts.source_date')!!}:<strong> {!!$post['source_date']!!} </strong></p>
                                    @endif
                                    @if($post['video']['source'] || $post['audio']['source'] || $post['gallery']) <p><i class="fa fa-th-large"></i>&nbsp;&nbsp;&nbsp;&nbsp;{!!Lang::get('posts::posts.post_type')!!}:<strong > @if($post['video']['source']) {!!Lang::get('posts::posts.video')!!} @elseif($post['audio']['source']) {!!Lang::get('posts::posts.audio')!!} @elseif($post['gallery']) {!!Lang::get('posts::posts.gallery')!!} @endif  </strong></p> @endif
                                    <p><i class="fa fa-exchange"></i>&nbsp;&nbsp;&nbsp;&nbsp;{!!Lang::get('posts::posts.source')!!}:<strong > {!!$post['source']['name']!!}  </strong></p>
                                    <p><i class="fa fa-rss-square"></i>&nbsp;&nbsp;&nbsp;&nbsp;{!!Lang::get('posts::posts.provider')!!}:<strong > @if($post['provider'] == 0) {!!Lang::get('posts::posts.news_site')!!} @elseif($post['provider'] == 1) {!!Lang::get('posts::posts.facebook')!!} @elseif($post['provider'] == 2) {!!Lang::get('posts::posts.twitter')!!} @elseif($post['provider'] == 3) {!!Lang::get('posts::posts.youtube')!!} @elseif($post['provider'] == 4) {!!Lang::get('posts::posts.instagram')!!} @endif</strong></p>
                                    @if($post['country'])
                                    <p><i class="fa fa-flag"></i>&nbsp;&nbsp;&nbsp;&nbsp;{!!Lang::get('posts::posts.country')!!}:<strong > {!!$post['country']['name']!!}  </strong></p>
                                    @endif
                                    @if($post['lang'])
                                    <p><i class="fa fa-flag-checkered"></i>&nbsp;&nbsp;&nbsp;&nbsp;{!!Lang::get('posts::posts.language')!!}:<strong > {!!Lang::get('posts::posts.'.$post['lang'])!!} </strong></p>
                                    @endif
                                    @if($post['video']['length'])
                                    <p><i class="fa fa-video-camera"></i>&nbsp;&nbsp;&nbsp;&nbsp;{!!Lang::get('posts::posts.duration')!!}:<strong > {!!gmdate("i:s", $post['video']['length'])!!} {!!Lang::get('posts::posts.minute_short')!!}</strong></p>
                                    @endif
                                    @if($post['audio']['length'])
                                    <p><i class="fa fa-volume-up"></i>&nbsp;&nbsp;&nbsp;&nbsp;{!!Lang::get('posts::posts.duration')!!}:<strong > {!!gmdate("i:s", $post['audio']['length'])!!} {!!Lang::get('posts::posts.minute_short')!!}</strong></p>
                                    @endif
                                    @if($post['author'])
                                    <p><i class="fa fa-user"></i>&nbsp;&nbsp;&nbsp;&nbsp;{!!Lang::get('posts::posts.author')!!}:<strong > {!!$post['author']!!}  </strong></p>
                                    @endif
                                    <p><i class="fa fa-external-link"></i>&nbsp;&nbsp;&nbsp;&nbsp;{!!Lang::get('posts::posts.link')!!}: <a href="{!!urldecode($post['link'])!!}" target="_blank" style="word-wrap: break-word;">{!!urldecode($post['link'])!!}</a>  </p>

                                </div>
                            </div>


                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>{!!Lang::get('posts::posts.social')!!}</h5>
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>

                                <a class="close-link">
                                    <i class="fa fa-times"></i>
                                </a>
                            </div>
                        </div>
                        <div class="ibox-content" style="@if($post['provider'] == 3) height: 265px; @elseif($post['provider'] == '0') height: 300px; @else height: 190px; @endif overflow: hidden">
                            <div class="form-group">  

                                <script>
                                    var data = new Array();
<?php
$engagement = 0;
?>

                                    @if ($post['provider'] == 0)
<?php
$total = $post['shares'];
$facebook_shares = 0;
$twitter_shares = 0;
$google_shares = 0;
$linkedin_shares = 0;
if ($total != 0) {
    $facebook_shares = round(($post['facebook_shares'] / $total) * 100, 2);
    $twitter_shares = round(($post['twitter_shares'] / $total) * 100, 2);
    $google_shares = round(($post['google_shares'] / $total) * 100, 2);
    $linkedin_shares = round(($post['linked_in_shares'] / $total) * 100, 2);
}
?>

                                    data = [
                                    { label: "{!!Lang::get('posts::posts.facebook_shares')!!}", value: {!!$facebook_shares!!} },
                                    { label: "{!!Lang::get('posts::posts.twitter_shares')!!}", value: {!!$twitter_shares!!} },
                                    { label: "{!!Lang::get('posts::posts.google_shares')!!}", value: {!!$google_shares!!} },
                                    { label: "{!!Lang::get('posts::posts.linkedin_shares')!!}", value: {!!$linkedin_shares!!} },
                                    ];
                                            @elseif($post['provider'] == 1)
<?php
$total = $post['shares'] + $post['comments'] + $post['likes'];
$shares = 0;
$comments = 0;
$likes = 0;

if ($total != 0) {
    $shares = round(($post['shares'] / $total) * 100, 2);
    $comments = round(($post['comments'] / $total) * 100, 2);
    $likes = round(($post['likes'] / $total) * 100, 2);
}
?>

                                    data = [
                                    { label: "{!!Lang::get('posts::posts.facebook_shares')!!}", value: {!!$shares!!} },
                                    { label: "{!!Lang::get('posts::posts.facebook_comments')!!}", value: {!!$comments!!} },
                                    { label: "{!!Lang::get('posts::posts.facebook_likes')!!}", value: {!!$likes!!} },
                                    ];
                                            @elseif($post['provider'] == 2)
<?php
$total = $post['retweets'] + $post['favorites'];
$retweets = 0;
$favorites = 0;

if ($total != 0) {
    $retweets = round(($post['retweets'] / $total) * 100, 2);
    $favorites = round(($post['favorites'] / $total) * 100, 2);
}
?>

                                    data = [
                                    { label: "{!!Lang::get('posts::posts.retweets')!!}", value: {!!$retweets!!} },
                                    { label: "{!!Lang::get('posts::posts.favorites')!!}", value: {!!$favorites!!} },
                                    ];
                                            @elseif($post['provider'] == 3)
<?php
$total = $post['likes'] + $post['dislikes'];
$likes = 0;
$dislikes = 0;

if ($total != 0) {
    $likes = round(($post['likes'] / $total) * 100, 2);
    $dislikes = round(($post['dislikes'] / $total) * 100, 2);
}
?>

                                    data = [
                                    { label: "{!!Lang::get('posts::posts.likes')!!}", value: {!!$likes!!} },
                                    { label: "{!!Lang::get('posts::posts.dislikes')!!}", value: {!!$dislikes!!} },
                                    ];
                                            @elseif($post['provider'] == 4)
<?php
$total = $post['likes'] + $post['comments'];
$likes = 0;
$comments = 0;

if ($total != 0) {
    $likes = round(($post['likes'] / $total) * 100, 2);
    $comments = round(($post['comments'] / $total) * 100, 2);
}
?>

                                    data = [
                                    { label: "{!!Lang::get('posts::posts.likes')!!}", value: {!!$likes!!} },
                                    { label: "{!!Lang::get('posts::posts.comments')!!}", value: {!!$comments!!} },
                                    ];
                                            @endif

<?php
if ($post['provider'] != 0) {
    $engagement = number_format($post['engagement'] * 100, 4);
} else {
    $engagement = $post['engagement'];
}
if ($engagement >= 1) {
    $rate = Lang::get('posts::posts.good');
    $class = "success";
} elseif ($engagement >= .5 && $engagement <= .99) {
    $rate = Lang::get('posts::posts.average');
    $class = "warning";
} else {
    $rate = Lang::get('posts::posts.low');
    $class = "danger";
}
?>

                                    @if ($total)
                                            $(function() {
                                            Morris.Donut({
                                            element: 'hero-donut',
                                                    data: data,
                                                    colors: ['#87d6c6', '#54cdb4', '#1ab394'],
                                                    resize: false,
                                                    labelColor: '#888',
                                                    formatter: function (y) { return y + "%" }
                                            });
                                            });
                                            @endif
                                </script>
                                <div class="pull-left" @if($total) style="width:58%" @else style="width:100%" @endif>
                                     <ul class="list-group clear-list m-t">
                                        @if($post['provider'] == 0)
                                        <li class="list-group-item fist-item" ><span ><img src="{!!assets()!!}/images/1429117426_ShareThis_Social-Network-Communicate-Page-Curl-Effect-Circle-Glossy-Shadow-Shine.png">&nbsp;&nbsp;{!!Lang::get('posts::posts.facebook_shares')!!}:</span> <span class="label label-pa-purple pull-right">{!!$post['facebook_shares']!!}</span> </li>

                                        <li class="list-group-item" ><span ><img src="{!!assets()!!}/images/1429034415_bubble-16.png">&nbsp;&nbsp;{!!Lang::get('posts::posts.facebook_comments')!!}:</span> <span class="label label-pa-purple pull-right">{!!$post['facebook_comments']!!}</span> </li>
                                        <li class="list-group-item" ><span ><img src="{!!assets()!!}/images/1429034310_like-16.png">&nbsp;&nbsp;{!!Lang::get('posts::posts.facebook_likes')!!}:</span> <span class="label label-pa-purple pull-right">{!!$post['facebook_likes']!!}</span> </li>
                                        <li class="list-group-item" ><span ><img src="{!!assets()!!}/images/1429035359_twitter_circle_color-16.png">&nbsp;&nbsp;{!!Lang::get('posts::posts.twitter_shares')!!}:</span> <span class="label label-pa-purple pull-right">{!!$post['twitter_shares']!!}</span> </li>
                                        <li class="list-group-item" ><span ><img src="{!!assets()!!}/images/1429035372_circle-google-plus-16.png">&nbsp;&nbsp;{!!Lang::get('posts::posts.google_shares')!!}:</span> <span class="label label-pa-purple pull-right">{!!$post['google_shares']!!}</span> </li>
                                        <li class="list-group-item" ><span ><img src="{!!assets()!!}/images/1429035388_linkedin_circle_color-16.png">&nbsp;&nbsp;{!!Lang::get('posts::posts.linkedin_shares')!!}:</span> <span class="label label-pa-purple pull-right">{!!$post['linked_in_shares']!!}</span> </li>
                                        @elseif($post['provider'] == 1)
                                        <li class="list-group-item fist-item" ><span ><img src="{!!assets()!!}/images/1429117426_ShareThis_Social-Network-Communicate-Page-Curl-Effect-Circle-Glossy-Shadow-Shine.png">&nbsp;&nbsp;{!!Lang::get('posts::posts.facebook_shares')!!}:</span> <span class="label label-pa-purple pull-right">{!!$post['shares']!!}</span> </li>
                                        <li class="list-group-item" ><span ><img src="{!!assets()!!}/images/1429034415_bubble-16.png">&nbsp;&nbsp;{!!Lang::get('posts::posts.facebook_comments')!!}:</span> <span class="label label-pa-purple pull-right">{!!$post['comments']!!}</span> </li>
                                        <li class="list-group-item" ><span ><img src="{!!assets()!!}/images/1429034310_like-16.png">&nbsp;&nbsp;{!!Lang::get('posts::posts.facebook_likes')!!}:</span> <span class="label label-pa-purple pull-right">{!!$post['likes']!!}</span> </li>
                                        @elseif($post['provider'] == 2)
                                        <li class="list-group-item fist-item" ><span ><img src="{!!assets()!!}/images/1429059314_Twitter_bird.png">&nbsp;&nbsp;{!!Lang::get('posts::posts.retweets')!!}:</span> <span class="label label-pa-purple pull-right">{!!$post['retweets']!!}</span> </li>
                                        <li class="list-group-item" ><span ><img src="{!!assets()!!}/images/1429059386_applications-games.png">&nbsp;&nbsp;{!!Lang::get('posts::posts.favorites')!!}:</span> <span class="label label-pa-purple pull-right">{!!$post['favorites']!!}</span> </li>
                                        @elseif($post['provider'] == 3)
                                        <li class="list-group-item fist-item" ><span ><img src="{!!assets()!!}/images/1429123922_like-01-16.png">&nbsp;&nbsp;{!!Lang::get('posts::posts.likes')!!}:</span> <span class="label label-pa-purple pull-right">{!!$post['likes']!!}</span> </li>
                                        <li class="list-group-item" ><span ><img src="{!!assets()!!}/images/1429123943_dislike-01-16.png">&nbsp;&nbsp;{!!Lang::get('posts::posts.dislikes')!!}:</span> <span class="label label-pa-purple pull-right">{!!$post['dislikes']!!}</span> </li>
                                        <li class="list-group-item" ><span ><img src="{!!assets()!!}/images/1429034415_bubble-16.png">&nbsp;&nbsp;{!!Lang::get('posts::posts.comments')!!}:</span> <span class="label label-pa-purple pull-right">{!!$post['comments']!!}</span> </li>
                                        <li class="list-group-item" ><span ><img src="{!!assets()!!}/images/1429059386_applications-games.png">&nbsp;&nbsp;{!!Lang::get('posts::posts.favorites')!!}:</span> <span class="label label-pa-purple pull-right">{!!$post['favorites']!!}</span> </li>
                                        <li class="list-group-item" ><span ><img src="{!!assets()!!}/images/1429123999_view-01-16.png">&nbsp;&nbsp;{!!Lang::get('posts::posts.views')!!}:</span> <span class="label label-pa-purple pull-right">{!!$post['views']!!}</span> </li>
                                        @elseif($post['provider'] == 4)
                                        <li class="list-group-item fist-item" ><span ><img src="{!!assets()!!}/images/1429034310_like-16.png">&nbsp;&nbsp;{!!Lang::get('posts::posts.likes')!!}:</span> <span class="label label-pa-purple pull-right">{!!$post['likes']!!}</span> </li>
                                        <li class="list-group-item" ><span ><img src="{!!assets()!!}/images/1429034415_bubble-16.png">&nbsp;&nbsp;{!!Lang::get('posts::posts.comments')!!}:</span> <span class="label label-pa-purple pull-right">{!!$post['comments']!!}</span> </li>

                                        @endif
                                        <li class="list-group-item" ><span ><i class="fa fa-line-chart"></i>&nbsp;&nbsp;{!!Lang::get('posts::posts.engagement_rate')!!}:</span> <span class="label label-pa-purple pull-right">{!!$engagement!!} @if($post['provider'] != 0)%@endif</span> </li>

                                    </ul>

                                </div>

                                @if($total)
                                <div class="pull-right" style="width:40%;">
                                    <div id="hero-donut" class="graph" style="margin-top:-90px"></div>
                                </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>

                @if($post['image']['source'])
                <div class="row">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>{!!Lang::get('posts::posts.featured_image')!!}</h5>
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>

                                <a class="close-link">
                                    <i class="fa fa-times"></i>
                                </a>
                            </div>
                        </div>
                        <div class="ibox-content" style="padding: 0">
                            <div class="form-group" style="padding: 0">

                                <div  class="lightBoxGallery" >    
                                    <a href="@if($post['image']['source']) {!!$post['image']['source']!!} @else @endif" title="" data-gallery=""><img width="100%" height="200px" src="@if($post['image']['source']) {!!$post['image']['source']!!} @else @endif"></a>
                                    <!-- The Gallery as lightbox dialog, should be a child element of the document body -->

                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                @endif
            </div>

        </div>
    </div>
</div>

<script src="<?php echo assets() ?>/js/plugins/morris/raphael-2.1.0.min.js"></script>
<script src="<?php echo assets() ?>/js/plugins/morris/morris.js"></script>
<script src="<?php echo assets() ?>/js/plugins/blueimp/jquery.blueimp-gallery.min.js"></script>
@stop
