<?php

Menu::set("sidebar", function($menu) {
    if (User::can('reporters')) {
        $menu->item('users.permissions', trans("admin::common.permissions"), URL::to(ADMIN . '/roles'));
    }
});

include __DIR__ . "/routes.php";
