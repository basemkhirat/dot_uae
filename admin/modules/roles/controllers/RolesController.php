<?php

class RolesController extends BackendController {

    public $data = array();

    public function index() {
        
        if (!User::is("superadmin")) {
            return denied();
        }
       
        if (Request::isMethod("post")) {
            $action = Input::get("action");
            $ids = Input::get("id");
            if (count($ids)) {
                switch ($action) {
                    case "delete":
                        Role::whereIn("role_id", $ids)->delete();
                        RolePermission::whereIn("role_id", $ids)->delete();
                        return Redirect::back()->with("message", trans("roles::roles.role_deleted"));

                        break;
                }
            }
        }

        $ob = new Role();

        if (Input::has("q")) {
            $ob->where('role_name', 'LIKE', '%' . Input::get("q") . '%');
        }

        if (Input::has("per_page")) {
            $this->data["per_page"] = $per_page = Input::get("per_page");
        } else {
            $this->data["per_page"] = $per_page = 20;
        }


        $this->data["roles"] = $ob->paginate($per_page);
        return View::make("roles::show", $this->data);
    }

    public function create() {
        
        if (!User::is("superadmin")) {
            return denied();
        }

        if (Request::isMethod("post")) {

            $name = Input::get("role_name");
            $slug = str_slug($name);

            // check role if exists
            if (Role::where("role_slug", "=", $slug)->count()) {
                return Redirect::back()->withErrors($slug . " is already exists")->withInput(Input::all());
            }

            $rules = array(
                'role_name' => 'required|alpha'
            );

            $validator = Validator::make(Input::all(), $rules);
            if ($validator->fails()) {
                return Redirect::back()->withErrors($validator)->withInput(Input::all());
            }


            Role::insert(array(
                "role_slug" => $slug,
                "role_name" => $name
            ));

            $role_id = DB::getPdo()->lastInsertId();


            // adding permissions
            if ($permissions = Input::get("permissions")) {
                foreach ($permissions as $permission) {
                    RolePermission::insert(array(
                        "role_id" => $role_id,
                        "permission" => $permission
                    ));
                }
            }

            return Redirect::route("roles.edit", array("id" => $role_id))->with("message", trans("roles::roles.role_created"));
        }


        $this->data["role"] = false;
        $this->data["modules"] = Config::get("modules");

        return View::make("roles::edit", $this->data);
    }

    public function edit($role_id) {
        
        if (!User::is("superadmin")) {
            return denied();
        }

        $role = Role::where("role_id", "=", $role_id)->first();

        if (count($role) == 0) {
            App::abort(404);
        }
        
        $name = Input::get("role_name");

        if (Request::isMethod("post")) {

            Role::where('role_id', '=', $role_id)->update(array(

                "role_name" => $name
            ));


            // adding permissions
            RolePermission::where("role_id", $role_id)->delete();
            if ($permissions = Input::get("permissions")) {
                foreach ($permissions as $permission) {
                    RolePermission::insert(array(
                        "role_id" => $role_id,
                        "permission" => $permission
                    ));
                }
            }

            return Redirect::back()->with("message", trans("roles::roles.role_updated"));
        }

        $this->data["role"] = $role;
        $this->data["role_permissions"] = RolePermission::where("role_id", "=", $role_id)->lists("permission")->toArray();
                
        $this->data["modules"] = Config::get("modules");

        return View::make("roles::edit", $this->data);
    }

    public function delete() {
        
        if (!User::is("superadmin")) {
            return denied();
        }
        
        $ids = Input::get("id");

        Role::whereIn("role_id", $ids)->delete();
        //RoleLang::whereIn("role_id", $ids)->delete();
        RolePermission::whereIn("role_id", $ids)->delete();

        return Redirect::back()->with("message", trans("roles::roles.role_deleted"));
    }

}
