@extends("admin::layouts.master")

@section("breadcrumb")
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2><?php echo trans("roles::roles.edit") ?></h2>
        <ol class="breadcrumb">
            <li>
                <a href="<?php echo URL::to(ADMIN . "/roles"); ?>"><?php echo trans("roles::roles.roles") ?></a>
            </li>
            <li class="active">
                <strong><?php echo trans("roles::roles.edit") ?></strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
        <?php if (!User::can("roles.create")) { ?>
            <a href="<?php echo route("roles.create"); ?>" class="btn btn-primary btn-labeled btn-main"> <span class="btn-label icon fa fa-plus"></span> &nbsp; <?php echo trans("roles::roles.add_new") ?></a>
        <?php } ?>
    </div>
</div>
@stop

@section("content")
<script>
    $(document).ready(function () {
        var elems = Array.prototype.slice.call(document.querySelectorAll('.permission-switcher'));
        elems.forEach(function (html) {
            var switchery = new Switchery(html);
        });
    });
</script>


<?php if (Session::has("message")) { ?>
    <div class="alert alert-success alert-dark">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <?php echo Session::get("message"); ?>
    </div>
<?php } ?>

<?php if ($errors->count() > 0) { ?>
    <div class="alert alert-danger alert-dark">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <?php echo implode(' <br /> ', $errors->all()) ?>
    </div>
<?php } ?>

<form action="" method="post" >
    <div class="row">
        <div class="col-md-12">
            <div class="panel">

                <div class="panel-body">

                    <div class="form-group">
                        <input name="role_name" value="<?php echo @Input::old("role_name", $role->role_name); ?>" class="form-control input-lg" value="" placeholder="<?php echo trans("roles::roles.role_name"); ?>" />
                    </div>

                    <?php foreach ($modules as $module){
                        $permissions = Config::get("$module.permissions");
                    ?>
                    
                    <?php if($permissions != NULL){ ?>
                    <div class="panel">

                            <a class="accordion-toggle" data-toggle="collapse" href="#collapse-<?php echo $module; ?>">
                                <?php echo ucfirst(trans(trans("$module::$module.module"))); ?>
                            </a>

                            <div id="collapse-<?php echo $module; ?>" class="panel-collapse in">
                                <div class="panel-body">
                                    <?php foreach ($permissions as $slug) { ?>
                                        <label class="checkbox">
                                            <input <?php if ($role and in_array($module.".".$slug, $role_permissions)) { ?> checked="checked" <?php } ?> type="checkbox" name="permissions[]" value="<?php echo $module.".".$slug; ?>" class="switcher permission-switcher switcher-sm">
                                            <span style="margin: 0 10px 10px;">
                                                <?php echo ucfirst(trans($module."::".$module.".permissions.".$slug)); ?>
                                            </span>
                                        </label>
                                    <?php } ?>
                                </div> <!-- / .panel-body -->
                            </div> <!-- / .collapse -->
                        </div> <!-- / .panel -->
                    <?php } ?>
                    <?php } ?>
                    
                   
                </div> <!-- / .panel-body -->
            </div>
            <div class="panel-footer" style="border-top: 1px solid #ececec; position: relative;">
                <div class="form-group" style="margin-bottom:0">
                    <input type="submit" class="pull-right btn btn-primary" value="<?php echo trans("roles::roles.save"); ?>" />
                </div>
            </div>
        </div>
        <!-- /6. $EASY_PIE_CHARTS -->
    </div>
</form>
<!-- /9. $UNIQUE_VISITORS_STAT_PANEL -->

@stop