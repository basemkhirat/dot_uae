<?php



class RoleLang extends Model {

    
    protected $table = 'roles_langs';
    
    public function scopeLang($query, $lang) {
        $query->where("lang", $lang);
        return $query;
        
    }

}
