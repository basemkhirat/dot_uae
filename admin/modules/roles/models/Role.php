<?php

class Role extends Model {

    protected $table = 'roles';
    protected $primaryKey = 'role_id';
    public $timestamps = false;

    public function scopeLang($query, $lang) {
        $query->join("roles_langs", "roles.role_id", "=", "roles_langs.role_id")
                ->where("roles_langs.lang", $lang);
        return $query;
    }
    
    public function rolePermissions() {
        return $this->hasMany('RolePermission', "role_id", "role_id");
    }
}
