<?php

Route::group(array(
    "prefix" => ADMIN,
    "before" => "auth",
        ), function($route) {
    $route->group(array("prefix" => "roles"), function($route) {
        $route->any('/', array("as" => "roles.show", "uses" => "RolesController@index"));
        $route->any('/create', array("as" => "roles.create", "uses" => "RolesController@create"));
        $route->any('/{id}/edit', array("as" => "roles.edit", "uses" => "RolesController@edit"));
        $route->any('/delete', array("as" => "roles.delete", "uses" => "RolesController@delete"));
    });
});
