<?php

Route::group(array(
    "prefix" => ADMIN,
    "before" => "auth",
        ), function($route) {
    $route->group(array("prefix" => "tags"), function($route) {
        $route->any('/slug', array("as" => "tags.slug", "uses" => "TagController@slug"));
        $route->any('/delete', array("as" => "tags.delete", "uses" => "TagController@delete"));
        $route->any('/create', array("as" => "tags.create", "uses" => "TagController@create"));
        $route->any('/{id}/edit', array("as" => "tags.edit", "uses" => "TagController@edit"));
        $route->any('/search', array("as" => "tags.search", "uses" => "TagController@search"));
        $route->any('/{parent?}', array("as" => "tags.show", "uses" => "TagController@index"));
        $route->get('google/keywords', array("as" => "google.search", "uses" =>"ServicesController@keywords"));
    });
});
