<?php

Menu::set("sidebar", function($menu) {
    if (User::can('tags')) {
        $menu->item('news_options.tags', trans("admin::common.tags"), URL::to(ADMIN . '/tags'))->icon("fa-th-large")->order(3);
    }
});


include __DIR__ . "/routes.php";
