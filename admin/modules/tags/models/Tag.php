<?php

class Tag extends Model {

    protected $table = 'tags';
    protected $fillable = ['tag_name', 'tag_slug', 'mongo_id'];
    protected $primaryKey = 'tag_id';
    public $timestamps = false;

    public static function getTags($key = "", $db) {
        $tags = DB::table('tags')->where('tags.site', '=', $db);
        if ($key != "") {
            $tags->where("tag_name", "LIKE", '%' . $key . '%');
        }
        return $tags->paginate(10);
    }

    public static function getTagPosts($id) {
        return $tags = DB::table('posts_tags')->where("tag_id", "=", $id)->get();
    }

    public static function getTag($id) {
        return $block = DB::table('tags')->where("tag_id", "=", $id)->get();
    }

    public static function saveTag($row) {
        DB::table('tags')->insert($row);
    }

    public static function updateTag($row, $id) {
        DB::table('tags')->where('tag_id', $id)->update($row);
    }

    public static function deleteTag($id) {
        DB::table('tags')->where('tag_id', '=', $id)->delete();
    }

    public static function deleteTagPostsEntry($id) {
        DB::table('posts_tags')->where('tag_id', '=', $id)->delete();
    }

    public function posts() {
        return $this->belongsToMany('Post', 'posts_tags', 'tag_id', 'post_id')->select(DB::raw('count(posts_tags.tag_id) as counts'))->groupBy('posts_tags.tag_id')->orderBy('counts', 'desc');
    }

}
