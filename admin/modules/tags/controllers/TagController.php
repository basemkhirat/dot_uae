<?php

class TagController extends BackendController {

    public $data = array();

    private function tags($parent) {

        $query = Tag::orderBy("tag_id", "DESC");

        if (Input::has("q")) {
            $query->where('tag_name', 'LIKE', '%' . Input::get("q") . '%');
        }

        if (Input::has("per_page")) {
            $this->data["per_page"] = $per_page = Input::get("per_page");
        } else {
            $this->data["per_page"] = $per_page = 20;
        }

        $this->data["tags"] = $query->paginate($per_page);
    }

    public function index($parent = 0) {

        if (Request::isMethod("post")) {
            $action = Input::get("action");
            $ids = Input::get("id");
            if (count($ids)) {
                switch ($action) {
                    case "delete":
                        Tag::whereIn("tag_id", $ids)->delete();

                        return Redirect::route("tags.show")->with("message", "تم حفظ الوسم بنجاح");
                        break;
                }
            }
        }

        $this->data["parent"] = $parent;
        $this->tags($parent);

        $this->data["tag"] = false;
        return View::make("tags::show", $this->data);
    }

    public function slug() {

        if (Request::isMethod("post")) {
            $name = Input::get("name");

            if (trim($name) == "") {
                return "";
            }

            $slug = str_slug($name);
            if (Tag::where("tag_slug", "=", $slug)->count()) {
                return $slug . "-1";
            } else {
                return $slug;
            }
        }
    }

    public function create() {

        if (Request::isMethod("post")) {

            $slug = Input::get("tag_slug");

            if ($slug != "") {
                if (Tag::where("tag_slug", "=", $slug)->count()) {
                    return Redirect::back()->withErrors($slug . " موجود مسبقا")->withInput(Input::all());
                }
            }

            $rules = array(
                'tag_name' => 'required',
                'tag_slug' => 'required'
            );

            $validator = Validator::make(Input::all(), $rules);
            if ($validator->fails()) {
                return Redirect::back()->withErrors($validator)->withInput(Input::all());
            }

            $canonical_url = (substr(Input::get("canonical_url"), 0, 4) != 'http' && Input::get("canonical_url") != '') ? "http://" . Input::get("canonical_url") : Input::get("canonical_url");
            Tag::insert(array(
                "tag_slug" => Input::get("tag_slug"),
                "tag_name" => Input::get("tag_name"),
//                "auto" => Input::get("auto"),
                "meta_title" => Input::get("meta_title"),
                "meta_description" => Input::get("meta_description"),
                "canonical_url" => $canonical_url,
                "robots_index" => Input::get("robots_index"),
                "in_sitemap" => Input::get("in_sitemap")
            ));

            $tag_id = DB::getPdo()->lastInsertId();


            return Redirect::route("tags.edit", array("id" => $tag_id))->with("message", "تم إضافة الوسم بنجاح");
        }
    }

    public function edit($tag_id) {

        $tag = Tag::where("tags.tag_id", "=", $tag_id)->first();

        if (count($tag) == 0) {
            \App::abort(404);
        }

        $this->tags(0);

        if (Request::isMethod("post")) {

            $rules = array(
                'tag_name' => 'required',
                'tag_slug' => 'required'
            );

            $validator = Validator::make(Input::all(), $rules);
            if ($validator->fails()) {
                return Redirect::back()->withErrors($validator)->withInput(Input::all());
            }
            
            $canonical_url = (substr(Input::get("canonical_url"), 0, 4) != 'http' && Input::get("canonical_url") != '') ? "http://" . Input::get("canonical_url") : Input::get("canonical_url");
            Tag::where("tag_id", $tag_id)->update(array(
                "tag_slug" => Input::get("tag_slug"),
                "tag_name" => Input::get("tag_name"),
//                "auto" => Input::get("auto"),
                "meta_title" => Input::get("meta_title"),
                "meta_description" => Input::get("meta_description"),
                "canonical_url" => $canonical_url,
                "robots_index" => Input::get("robots_index"),
                "in_sitemap" => Input::get("in_sitemap")
            ));


            return Redirect::back()->with("message", "تم تعديل الوسم بنجاح");
        }


        $this->data["tag"] = $tag;
        return View::make("tags::show", $this->data);
    }

    public function delete() {
        $ids = Input::get("id");
        if (is_array($ids)) {
            Tag::whereIn("tag_id", $ids)->delete();
        } else {
            $deletedTag = Input::get('deleted');
            $moveToTag = Input::get('move_to') ? Input::get('move_to') : null;
            if ($moveToTag) {
                DB::table('posts_tags')->where('tag_id', '=', $deletedTag)->update(array('tag_id' => $moveToTag));
            } else {
                DB::table('posts_tags')->where('tag_id', '=', $deletedTag)->delete();
            }
            Tag::where("tag_id", $deletedTag)->delete();
        }
        if (!Request::ajax()) {
            return Redirect::route("tags.show")->with("message", "تم حذف الوسم بنجاح");
        }
    }

    function search() {

        $data = Tag::where("tag_name", "LIKE", urldecode(Input::get("term")) . "%")
                ->select("tag_id", "tag_name")
                ->take(6)
                ->get();

        return json_encode($data);
    }

}
