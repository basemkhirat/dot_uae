@extends("admin::layouts.master")

@section("breadcrumb")
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-4">
        <h2><?php echo trans("tags::tags.tags") ?></h2>
        <ol class="breadcrumb">
            <li>
                <a href="<?php echo route("tags.show"); ?>"><?php echo trans("tags::tags.tags") ?> (<?php echo $tags->total() ?>)</a>
            </li>
        </ol>
    </div>

    <div class="col-lg-6">
        <form action="" method="get" class="search_form">
            <div class="input-group">
                <input name="q" value="<?php echo Input::get("q"); ?>" type="text" class=" form-control" placeholder="<?php echo trans("tags::tags.search_tags") ?>.."> <span class="input-group-btn">
                    <button class="btn btn-primary" type="button"> <?php echo trans("tags::tags.search") ?></button> </span>
            </div>
        </form>
    </div>

    <div class="col-lg-2">
        <a href="<?php echo route("tags.show"); ?>" class="btn btn-primary btn-labeled btn-main"> <span class="btn-label icon fa fa-plus"></span> &nbsp; <?php echo trans("tags::tags.add_new") ?></a>
    </div>
</div>
@stop

@section("content")

<?php if (Session::has("message")) { ?>
    <div class="alert alert-success alert-dark">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <?php echo Session::get("message"); ?>
    </div>
<?php } ?>

<?php if ($errors->count() > 0) { ?>
    <div class="alert alert-danger alert-dark">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <?php echo implode(' <br /> ', $errors->all()) ?>
    </div>
<?php } ?>

<div class="row">
    <div class="col-md-6">

        <div class="panel panel-default">
            <div class="panel-heading">
                <span class="panel-title">
                    <i class="fa fa-folder"></i> &nbsp;&nbsp;&nbsp;
                    <?php if (!$tag) { ?>
                        <?php echo trans("tags::tags.add_new") ?>
                    <?php } else { ?>
                        <?php echo trans("tags::tags.edit_tag") ?>
                    <?php } ?>
                </span>
            </div>
            <?php if (!$tag) { ?>
                <form method="post" action="<?php echo route("tags.create"); ?>">
                <?php } else { ?>
                    <form method="post" action="<?php echo route("tags.edit", array("id" => $tag->tag_id)); ?>">
                    <?php } ?>
                    <div class="panel-body" style="padding-bottom:0">

                        <div class="col-md-12">

                            <div class="form-group input-group">
                                <span class="input-group-addon"><i class="fa fa-folder"></i></span>
                                <input  name="tag_name" value="<?php echo @Input::old("tag_name", $tag->tag_name); ?>" class="form-control input-lg" placeholder="<?php echo trans("tags::tags.tag_name") ?>" />
                            </div>

                            <div class="form-group input-group">
                                <span class="input-group-addon"><i class="fa fa-chain"></i></span>
                                <input name="tag_slug" value="<?php echo @Input::old("tag_slug", $tag->tag_slug); ?>" class="form-control input-lg" placeholder="<?php echo trans("tags::tags.tag_slug") ?>" />
                            </div>

                            <!--                            <div class="row form-group">
                                                            <label class="col-sm-2"><?php echo trans("tags::tags.auto") ?></label>
                                                            <div class="col-sm-10">
                                                                <input type="checkbox" id="auto"  value="1" name="auto" class="js-switch" @if(@$tag->auto == 1) checked @endif >
                                                            </div>
                                                        </div>-->
                        </div>
                    </div>
                    <h3 class="col-lg-12" style="margin-top:0;border-top: 1px solid #E7EAEC;padding-top: 10px;">{!!trans('tags::tags.seo_options')!!}</h3>
                    <div class="panel-body">

                        <div class="form-horizontal" style="padding-top:15px">
                            <div class="tab-pane fade active in " id="general-tab">

                                <div class="form-group tooltip-demo">
                                    <label for="meta_title" class="col-sm-2" style="padding-top: 7px;">{!!Lang::get('tags::tags.meta_title')!!}</label>
                                    <div class="col-sm-10">
                                        {!! Form::text("meta_title", string_sanitize(Input::old('meta_title', @$tag->meta_title)), array('class' => 'form-control input-lg', 'id' => 'meta_title', 'placeholder' => Lang::get('tags::tags.meta_title'))) !!}
                                        <div style="font-size: 14px; margin-top: 2px">
                                            {!!trans('tags::tags.meta_subtitle')!!} <span id="metadesc-length"></span>.
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group tooltip-demo">
                                    <label for="meta_description" class="col-sm-2" style="padding-top: 7px;">{!!Lang::get('tags::tags.meta_description')!!}</label>
                                    <div class="col-sm-10">
                                        {!! Form::textarea("meta_description", string_sanitize(Input::old('meta_description', @$tag->meta_description)), ['class' => 'form-control', 'rows' => 2, 'id' => 'meta_description', 'style' => 'resize: vertical;',  'placeholder' => Lang::get('tags::tags.meta_description')]) !!}
                                        <div style="font-size: 14px; margin-top: 2px">
                                            {!!trans('tags::tags.meta_descrip_subtitle')!!} <span id="metadesc-length"></span>.
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="canonical_url" class="col-sm-2" style="padding-top: 7px;">{!!Lang::get('tags::tags.canonical_url')!!}</label>
                                    <div class="col-sm-10">
                                        {!! Form::text("canonical_url", string_sanitize(Input::old('canonical_url', @$tag->canonical_url)), array('class' => 'form-control input-lg', 'id' => 'canonical_url', 'placeholder' => Lang::get('tags::tags.canonical_url'))) !!}
                                        <div style="font-size: 14px; margin-top: 2px">
                                            {!!trans('tags::tags.canonical_url_info')!!}
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="robots_index" class="col-sm-2" style="padding-top: 7px;">{!!Lang::get('tags::tags.meta_robots_index')!!}</label>
                                    <div class="col-sm-10">
                                        <select name="robots_index" id="robots_index" class="form-control chosen-select chosen-rtl" style="width:auto; display: inline-block;">
                                            <option value="0">{!!Lang::get('tags::tags.robots_index_default')!!}</option>
                                            <option value="2" @if(@$tag->robots_index == 2) selected @endif>{!!Lang::get('tags::tags.index')!!}</option>
                                            <option value="1" @if(@$tag->robots_index == 1) selected @endif>{!!Lang::get('tags::tags.noindex')!!}</option>
                                        </select>
                                        <div style="font-size: 14px; margin-top: 2px">
                                            {!!trans('tags::tags.robots_info')!!} <span id="metadesc-length"></span>.
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group" style="margin-top:20px">
                                    <label for="in_sitemap" class="col-sm-2" style="padding-top: 7px;">{!!Lang::get('tags::tags.include_in_sitemap')!!}</label>
                                    <div class="col-sm-10">
                                        <select name="in_sitemap" id="in_sitemap" class="form-control chosen-select chosen-rtl" style="width:auto; display: inline-block;">
                                            <option value="0">{!!Lang::get('tags::tags.auto_detect')!!}</option>
                                            <option value="1" @if(@$tag->in_sitemap == 1) selected @endif>{!!Lang::get('tags::tags.always_include')!!}</option>
                                            <option value="2" @if(@$tag->in_sitemap == 2) selected @endif>{!!Lang::get('tags::tags.never_include')!!}</option>
                                        </select>
                                        <div style="font-size: 14px; margin-top: 2px">
                                            {!!trans('tags::tags.sitemap_info')!!}
                                        </div>
                                    </div>
                                </div>
                            </div> <!-- / .tab-pane -->

                        </div> <!-- / .tab-content -->

                    </div>

                    <div class="panel-footer">
                        <div class="form-group">
                            <input type="submit" class="pull-right btn btn-flat btn-primary" value="<?php echo trans("tags::tags.save_tag") ?>" />
                        </div>
                    </div>

                </form>
        </div>

    </div>

    <div class="col-md-6 pull-right">
        <form action="<?php echo route("tags.show"); ?>" method="post" class="action_form">

            <div class="ibox float-e-margins panel panel-default">
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-sm-6 m-b-xs">

                            <div class="form-group">
                                <select name="action" class="form-control chosen-select chosen-rtl" style="width:auto; display: inline-block;">
                                    <option value="-1" selected="selected"><?php echo trans("tags::tags.bulk_actions") ?></option>
                                    <option value="delete"><?php echo trans("tags::tags.delete") ?></option>
                                </select>
                                <button type="submit" class="btn btn-primary" style="margin-top:5px"><?php echo trans("tags::tags.apply") ?></button>
                            </div>
                        </div>
                        <div class="col-sm-3 m-b-xs">

                        </div>
                        <div class="col-sm-3">

                            <select  name="post_status" id="post_status" class="form-control per_page_filter chosen-select chosen-rtl">
                                <option value="" selected="selected">-- <?php echo trans("tags::tags.per_page") ?> --</option>
                                <?php foreach (array(10, 20, 30, 40) as $num) { ?>
                                    <option value="<?php echo $num; ?>" <?php if ($num == $per_page) { ?> selected="selected" <?php } ?>><?php echo $num; ?></option>
                                <?php } ?>
                            </select>

                        </div>
                    </div>
                    <div class="table-responsive">


                        <table cellpadding="0" cellspacing="0" border="0" class="table table-striped">
                            <thead>
                                <tr>
                                    <th style="width:35px"><input type="checkbox" class="check_all i-checks" name="ids[]" /></th>

                                    <th><?php echo trans("tags::tags.name"); ?></th>
                                    <th><?php echo trans("tags::tags.actions") ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if (count($tags)) { ?>
                                    <?php foreach ($tags as $tag) { ?>
                                        <tr>
                                            <td>
                                                <input class="i-checks" type="checkbox" name="id[]" value="<?php echo $tag->tag_id; ?>" />
                                            </td>

                                            <td>
                                                <a class="text-navy" href="<?php echo URL::to(ADMIN) ?>/tags/<?php echo $tag->tag_id; ?>/edit">
                                                    <?php echo $tag->tag_name; ?>
                                                </a>
                                            </td>
                                            <td class="center">
                                                <a href="<?php echo URL::to(ADMIN) ?>/tags/<?php echo $tag->tag_id; ?>/edit">
                                                    <i class="fa fa-pencil text-navy"></i>
                                                </a>
                                               <!-- <a class="ask" message="<?php echo trans("tags::tags.sure_delete") ?>" href="<?php echo URL::to(ADMIN) ?>/tags/delete?id[]=<?php echo $tag->tag_id; ?>">-->
                                                <a href="#" class="delete_tag" data-toggle="modal" data-target="#modal_delete" data-id="{{$tag->tag_id}}">
                                                    <i class="fa fa-times text-navy"></i>
                                                </a>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                <?php } else { ?>


                                    <tr>
                                        <td colspan="4">
                                            <?php echo trans("tags::tags.no_tags") ?>
                                        </td>

                                    </tr>
                                <?php } ?>

                            </tbody>
                        </table>
                    </div>

                    <div class="row">
                        <div class="col-sm-4">
                            <div class="dataTables_info" id="editable_info" role="status" aria-live="polite">
                                <?php echo trans("tags::tags.page") ?> <?php echo $tags->currentPage() ?> <?php echo trans("tags::tags.of") ?> <?php echo $tags->lastPage() ?>
                            </div>
                        </div>
                        <div class="col-sm-8">
                            <div class="dataTables_paginate paging_simple_numbers" id="editable_paginate">
                                <?php echo str_replace('/?', '?', $tags->appends(Input::all())->render()); ?>
                            </div>
                        </div>
                    </div>

                    </form>
                </div>

            </div>



    </div> <!-- / #content-wrapper -->
    <style>

        .select2-container  {
            font-size: 16px;
            height: 42px;
            line-height: 22px;
        }

    </style>
    <?php /*
      <div id="modal_delete" class="modal fade" role="dialog" style="display: none;">
      <div class="modal-dialog modal-bg">
      <div class="modal-content">
      <div class="modal-header" style="min-height: 49.43px;">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
      </div>
      {{Form::open(array(
      'route' => array('tags.delete'),
      'method' => 'POST'
      ))}}
      <!--<form action="{{route(ADMIN . '.topics.destroy')}}">-->
      <div class="modal-body">
      <div class="row">
      <div class="col-md-6">
      <label>{{Lang::get("tags::tags.move_posts")}}</label>
      </div>
      <div class="col-md-6">
      <select id="all_tags_delete" class="form-control select2" name="move_to">
      <option value="0">{{Lang::get("tags::tags.not_move")}}</option>
      @foreach(Tag::get() as $dbTopic)
      <option value="{{$dbTopic->tag_id}}">{{$dbTopic->tag_name}}</option>
      @endforeach
      </select>
      </div>
      </div>
      </div> <!-- / .modal-content -->
      <div class="modal-footer">
      <button class="btn btn-primary">{{Lang::get("tags::tags.ok")}}</button>
      <a class="btn" data-dismiss="modal">{{Lang::get("tags::tags.cancel")}}</a>
      </div>
      <input id="deleted_id" type="hidden" name="deleted" value=""/>
      </form>
      </div> <!-- / .modal-dialog -->
      </div> <!-- / .modal -->
      </div> <!-- / .modal -->
      <?php */ ?>
    <script>
        $(document).ready(function () {


            $('.i-checks').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green',
            });

            $('.check_all').on('ifChecked', function (event) {
                $("input[type=checkbox]").each(function () {
                    $(this).iCheck('check');
                    $(this).change();
                });
            });

            $('.check_all').on('ifUnchecked', function (event) {
                $("input[type=checkbox]").each(function () {
                    $(this).iCheck('uncheck');
                    $(this).change();
                });
            });


            $("input[name=tag_name]").blur(function () {
                var base = $(this);
                var name = base.val();
                $.post("<?php echo URL::to(ADMIN . "/tags/slug"); ?>", {name: name}, function (cat_slug) {
                    $("input[name=tag_slug]").val(cat_slug);
                });
            });

            $(".per_page_filter").change(function () {
                var base = $(this);
                var per_page = base.val();
                location.href = "<?php echo route("tags.show", array("parent" => 0)) ?>?per_page=" + per_page;
            });



        });



    </script>
    <script>
        $(document).ready(function () {
            
            $('.chosen-select').chosen();
    
            $("#change_photo").filemanager({
                types: "image",
                done: function (result, base) {
                    if (result.length) {
                        var file = result[0];
                        $("#category_image_id").val(file.media_id);
                        $("#category_image").attr("src", file.media_thumbnail);
                    }
                },
                error: function (media_path) {
                    alert(media_path + " <?php echo trans("tags::tags.is_not_an_image") ?>");
                }
            });

            $('.delete_tag').click(function (event) {
                var self = $(this);
                var id = self.attr('data-id');
                $('#all_tags_delete option').prop('disabled', false);
                $('#all_tags_delete option[value=' + id + ']').prop('disabled', true);
                $('#deleted_id').val(id);
            });

            var elem = document.querySelector('.js-switch');
            var switchery = new Switchery(elem);

            function setSwitchery(switchElement, checkedBool) {
                if ((checkedBool && !switchElement.isChecked()) || (!checkedBool && switchElement.isChecked())) {
                    switchElement.setPosition(true);
                    switchElement.handleOnchange(true);
                }
            }
        });

    </script>


    @stop
