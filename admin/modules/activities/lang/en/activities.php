<?php

return [
    "activities" => "Activities",
    "search_users" => "search users",
    "per_page" => "Per page",
    "bulk_actions" => "Bulk actions",
    "delete" => "delete",
    "apply" => "apply",
    "photo" => "Image",
    "name" => "Name",
    "email" => "Email",
    "created" => "Registeration date",
    "actions" => "Actions",
    "sure_delete" => "Are you sure to delete user",
    "page" => "Page",
    "add_new_user" => "Add new user",
    "of" => "of",
    "bulk_actions" => "Bulk actions",
    "search" => "search",
    "start_date" => "Start date",
    "end_date" => "End date",
    "view" => "View",
    "published" => "Published",
    "pending" => "Pending",
    "reviewed" => "Reviewed",
    "news" => "News",
    "title" => "Title",
    "views" => "Views",
    "facebook" => "Facebook",
    "twitter" => "Twitter",
    "googleplus" => "Google plus",
    "status" => "Status",
    "no_posts_for_user" => "No posts for this user",
    "module" => "Activities",
    "permissions" => [
        "manage" => "manage activities"
    ]
];
