<?php

return [
    "activities" => "الإحصائيات",
    "add_new" => "أضف جديد",
    "search_users" => "البحث فى الأعضاء",
    "per_page" => "عرض بكل صفحة",
    "bulk_actions" => "Bulk actions",
    "delete" => "حذف",
    "apply" => "حفظ",
    "photo" => "صورة",
    "name" => "الإسم",
    "email" => "البريد الإلكترونى",
    "created" => "تاريخ التسجيل",
    "actions" => "الحدث",
    "sure_delete" => "هل أنت متأكد من الحذف",
    "page" => "الصفحة",
    "add_new_user" => "أضف عضو جديد",
    "username" => "إسم المستخدم",
    "password" => "كلمة المرور",
    "confirm_password" => "تأكيد كلمة المرور",
    "email" => "البريد الإلكترونى",
    "change" => "تغيير",
    "first_name" => "الإسم الأول",
    "last_name" => "الإسم الأخير",
    "about_me" => "عن العضو",
    "is_not_an_image" => "ليس صورة",
    "role" => "الصلاحية",
    "activation" => "التفعيل",
    "language" => "اللغة",
    "special_permissions" => "صلاحيات خاصة",
    "save_user" => "حفظ العضو",
    "of" => "من",
    "activated" => "مفعل",
    "deactivated" => "غير مفعل",
    "user_created" => "تم إضافة العضو بنجاح",
    "user_updated" => "تم تعديل العضو بنجاح",
    "user_deleted" => "تم حذف العضو بنجاح",
    "bulk_actions" => "اختر أمر",
    "to" => "إلى",
    "search" => "بحث",
    "start_date" => "من",
    "end_date" => "إلى",
    "view" => "عرض",
    "published" => "منشور",
    "pending" => "بإنتظار المراجعه",
    "reviewed" => "تم مراجعته",
    "news" => "الأخبار",
    "title" => "العنوان",
    "views" => "المشاهدات",
    "facebook" => "فيسبوك",
    "twitter" => "تويتر",
    "googleplus" => "جوجل بلس",
    "status" => "الحالة",
    "no_posts_for_user" => "لا توجد أخبار لهذا العضو",
    "module" => "الإحصائيات",
    "permissions" => [
        "manage" => "مشاهدة إحصائيات الأعضاء"
    ]
];
