<?php

class ActivitiesController extends BackendController {

    public $data = array();

    public function index() {

        if (!User::can("activities.manage")) {
            return denied();
        }

        $action = Input::get("action");
        $ids = Input::get("id");
        if (count($ids)) {
            switch ($action) {
                case "delete":
                    User::whereIn("id", $ids)->delete();
                    UserPermission::whereIn("user_id", $ids)->delete();
                    return Redirect::back()->with("message", trans("users::users.user_deleted"));
                    break;
            }
        }


        $start = date("Y-m-d H:i:s", strtotime(urldecode(Input::get("start"))));
        $end = date("Y-m-d H:i:s", strtotime(urldecode(Input::get("end"))));

        $ob = DB::table("users")
                ->addSelect("users.*", "media.media_path", "roles.role_name")
                ->leftJoin("roles", "roles.role_id", "=", "users.role_id")
                ->orderBy("created_at", "DESC");

        if (Input::has("start") and Input::has("end")) {
            $ob->addSelect(DB::raw("(select count(*) from posts where post_published_date >= '" . $start . "' and post_published_date <= '" . $end . "' and posts.post_user_id = users.id and posts.post_status = 1) as published"));
        } else if (Input::has("start")) {
            $ob->addSelect(DB::raw("(select count(*) from posts where post_published_date >= '" . $start . "' and posts.post_user_id = users.id and posts.post_status = 1) as published"));
        } else if (Input::has("end")) {
            $ob->addSelect(DB::raw("(select count(*) from posts where post_published_date <= '" . $end . "' and posts.post_user_id = users.id and posts.post_status = 1) as published"));
        } else {
            $ob->addSelect(DB::raw("(select count(*) from posts where posts.post_user_id = users.id and posts.post_status = 1) as published"));
        }

        if (Input::has("q")) {
            $ob->where(function($where_ob) {
                $q = urldecode(Input::get("q"));
                $where_ob->where('username', 'LIKE', '%' . $q . '%');
                $where_ob->orWhere('first_name', 'LIKE', '%' . $q . '%');
                $where_ob->orWhere('email', 'LIKE', '%' . $q . '%');
            });
        }

        if (Input::has("per_page")) {
            $this->data["per_page"] = $per_page = Input::get("per_page");
        } else {
            $this->data["per_page"] = $per_page = 20;
        }


        $this->data["users"] = $ob->leftJoin("media", "media.media_id", "=", "users.photo")
                ->paginate($per_page);

        //$queries = DB::getQueryLog();
        //print_r(end($queries));
        //return;
        return View::make("activities::show", $this->data);
    }

    public function user($user_id) {

        if (!(User::can("activities.manage"))) {
            return denied();
        }

        $user = User::where("id", "=", $user_id)
                        ->leftJoin("media", "media.media_id", "=", "users.photo")
                        ->leftJoin("roles", "users.role_id", "=", "roles.role_id")->first();
        
        if (count($user) == 0) {
            \App::abort(404);
        }

        $start = date("Y-m-d H:i:s", strtotime(urldecode(Input::get("start"))));
        $end = date("Y-m-d H:i:s", strtotime(urldecode(Input::get("end"))));

        $ob = DB::table("posts");

        if (Input::has("start")) {
            $ob->where("post_created_date", ">=", $start);
        }

        if (Input::has("end")) {
            $ob->where("post_created_date", "<=", $end);
        }
        $ob->where("posts.post_created_date", "!=", "0000-00-00 00:00:00");
        $ob->groupBy(DB::raw("YEAR(post_created_date), MONTH(post_created_date), DAY(post_created_date)"));
        $ob->orderBy("posts.post_created_date", "DESC");

        $this->data["posts"] = $posts = $ob->get();


        $ob = DB::table("posts")
                ->leftJoin("posts_stats", "posts_stats.post_id", "=", "posts.post_id")
                ->join("users", "posts.post_user_id", "=", "users.id")
                ->groupBy("posts.post_id")
                ->where("users.id", "=", $user->id)
                ->orderBy("posts.post_created_date", "DESC")
                ->take(15);

        if (Input::has("start")) {
            $ob->where("post_created_date", ">=", $start);
        }

        if (Input::has("end")) {
            $ob->where("post_created_date", "<=", $end);
        }

        $this->data["rows"] = $ob->get();

        $this->data["user"] = $user;

        return View::make("activities::user", $this->data);
    }

    public function delete() {

        if (!User::can("activities.manage")) {
            return denied();
        }

        $ids = Input::get("id");

        User::whereIn("id", $ids)->delete();
        UserPermission::whereIn("user_id", $ids)->delete();

        return Redirect::back()->with("message", trans("users::users.user_deleted"));
    }

}
