<?php

Menu::set("sidebar", function($menu) {
    if (User::can("activities.manage")) {
        $menu->item('dashboard.activities', trans("admin::common.user_statistics"), URL::to(ADMIN . '/activities'));
    }
});

include __DIR__ . "/routes.php";
