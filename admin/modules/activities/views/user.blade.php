@extends("admin::layouts.master")

@section("breadcrumb")
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-4">
        <h2><?php echo trans("activities::activities.activities") ?></h2>
        <ol class="breadcrumb">
            <li>
                <a href="<?php echo URL::to(ADMIN . "/activities"); ?>"><?php echo trans("activities::activities.activities") ?></a>
            </li>
        </ol>
    </div>

    <div class="col-lg-6">

    </div>
</div>
@stop

@section("content")
<div id="content-wrapper">

    <?php if (Session::has("message")) { ?>
        <div class="alert alert-success alert-dark">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <?php echo Session::get("message"); ?>
        </div>
    <?php } ?>

    <?php if ($errors->count() > 0) { ?>
        <div class="alert alert-danger alert-dark">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <?php echo implode(' <br /> ', $errors->all()) ?>
        </div>
    <?php } ?>

    <form action="" method="get" >
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">

                    <div class="panel-body">

                        <div class="well" style="padding: 7px; margin: 0">
                            <div class="row">
                                <div class="col-md-1 text-center">
                                    <img id="user_photo" style="border: 1px solid #ccc; width: 100%;" src="<?php if ($user and $user->media_path != "") { ?> <?php echo thumbnail($user->media_path); ?> <?php } else { ?> <?php echo assets("images/user.png"); ?><?php } ?>" />
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <div><b><?php echo $user->first_name; ?> <?php echo $user->last_name; ?>  </b></div>
                                        <div><?php echo $user->role_name; ?>  </div>
                                    </div>
                                </div>
                                <div class="col-md-1"> </div>
                                <div class="col-md-7">
                                    <div  style="display:inline-block; margin-top: 6px">



                                        <div class="input-group date datepick">
                                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                            <input name="start" type="text" value="<?php echo Input::get("start"); ?>" class="form-control" placeholder="<?php echo trans("activities::activities.start_date") ?>">
                                        </div>

                                        <div class="input-group date datepick">
                                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                            <input name="end" type="text" value="<?php echo Input::get("end"); ?>" class="form-control" placeholder="<?php echo trans("activities::activities.end_date") ?>">
                                        </div>




                                        <?php /* ?>
                                          <span class="input-group-addon">{{Lang::get('posts::posts.to')}}</span>
                                          <input type="text" class="form-control" name="end" value="<?php echo Input::get("end"); ?>" placeholder="نهاية"/>
                                          <?php */ ?>
                                        <?php /*
                                          <div class="input-daterange input-group" >
                                          <input type="text" class="form-control" name="start" placeholder="بداية" value="<?php echo Input::get("start"); ?>"/>
                                          <span class="input-group-addon">{{Lang::get('posts::posts.to')}}</span>
                                          <input type="text" class="form-control" name="end" value="<?php echo Input::get("end"); ?>" placeholder="نهاية"/>
                                          </div>
                                         * */ ?>


                                    </div>
                                    <button style="display: inline-block;margin-top: -23px;" type="submit" class="btn btn-primary"><?php echo trans("activities::activities.view") ?></button>
                                </div>

                            </div>
                        </div>

                        <div class="graph-container">
                            <div>
                                <canvas id="lineChart" height="114"></canvas>
                            </div>
                        </div>


                    </div> <!-- / .panel-body -->
                </div>

            </div>

            <div style="clear:both"></div>

            <div class="col-md-12">


                <div class="panel panel-default">
                    <div class="panel-heading">
                        <span class="panel-title"><i class="panel-title-icon fa fa-bullhorn"></i> &nbsp; <?php echo trans("activities::activities.news") ?></span>
                        <div class="panel-heading-controls">
                            <div class="panel-heading-text pull-right" style="margin-top: -15px">
                                <a class="text-navy" href="<?php echo URL::to(ADMIN . "/posts?user_id=" . $user->id); ?>"><?php echo trans("dashboard::dashboard.show_all"); ?></a>
                            </div>
                        </div>
                    </div> <!-- / .panel-heading -->
                    <div class="panel-body tab-content-padding">
                        <!-- Panel padding, without vertical padding -->
                        <div class="panel-padding no-padding-vr">




                            <table cellpadding="0" cellspacing="0" border="0" class="table table-striped">
                                <thead>
                                    <tr>
                                        <th style="width:350px"><?php echo trans("activities::activities.title") ?></th>
                                        <th ><?php echo trans("activities::activities.views") ?></th>
                                        <th><?php echo trans("activities::activities.facebook") ?></th>
                                        <th><?php echo trans("activities::activities.twitter") ?></th>
                                        <th><?php echo trans("activities::activities.googleplus") ?></th>
                                        <th><?php echo trans("activities::activities.status") ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php if (count($rows)) { ?>
                                        <?php foreach ($rows as $post) { ?>
                                            <tr>
                                                <td>
                                                    <a href="<?php echo URL::to(ADMIN . "/posts/" . $post->post_id . "/edit") ?>" title="" class="text-navy"><?php echo $post->post_title; ?></a>
                                                    <div class="ticket-info">
                                                        <span><?php echo date("d-m-Y h:i A", strtotime($post->post_updated_date)); ?></span>
                                                    </div>
                                                </td>
                                                <td>
                                                    <?php
                                                    if ($post->post_views != "") {
                                                        echo $post->post_views;
                                                    } else {
                                                        echo "0";
                                                    }
                                                    ?>
                                                </td>

                                                <td>
                                                    <?php
                                                    if ($post->facebook != "") {
                                                        echo $post->facebook;
                                                    } else {
                                                        echo "0";
                                                    }
                                                    ?>
                                                </td>
                                                <td>
                                                    <?php
                                                    if ($post->twitter != "") {
                                                        echo $post->twitter;
                                                    } else {
                                                        echo "0";
                                                    }
                                                    ?>
                                                </td>
                                                <td>
                                                    <?php
                                                    if (@$post->google != "") {
                                                        echo @$post->google;
                                                    } else {
                                                        echo "0";
                                                    }
                                                    ?>
                                                </td>

                                                <td>
                                                    <?php if ($post->post_status == 1) { ?>
                                                        <span class="label label-success ticket-label">منشور</span>
                                                    <?php } elseif ($post->post_submitted == 1 and $post->post_desked == '0') { ?>
                                                        <span class="label label-info ticket-label">منتظر للمراجعة</span>
                                                    <?php } elseif ($post->post_desked == 1) { ?>
                                                        <span class="label label-danger ticket-label">تمت مراجعته</span>
                                                    <?php } else { ?>
                                                        <span class="label label-danger ticket-label">جديد</span>
                                                    <?php } ?>
                                                </td>
                                            </tr>
                                        <?php } ?>
                                    <?php } else { ?>


                                        <tr>
                                            <td colspan="6">
                                                <?php echo trans("activities::activities.no_posts_for_user"); ?>
                                            </td>

                                        </tr>
                                    <?php } ?>

                                </tbody>
                            </table>

                        </div>

                    </div>
                </div> <!-- / .panel-body -->
            </div> <!-- / .panel -->
        </div>

        <!-- /6. $EASY_PIE_CHARTS -->
</div>
</form>

@section("header")
@parent
<link href="<?php echo assets('css/plugins/datetimepicker/bootstrap-datetimepicker.min.css') ?>" rel="stylesheet" type="text/css">
<style>


</style>
@stop

@section("footer")
@parent
<script type="text/javascript" src="<?php echo assets('js/plugins/moment/moment.min.js') ?>"></script>
<script type="text/javascript" src="<?php echo assets('js/plugins/datetimepicker/bootstrap-datetimepicker.min.js') ?>"></script>
<script src = "<?php echo assets() ?>/js/plugins/chartJs/Chart.min.js" ></script>


<?php
$dates_array = array();
foreach ($posts as $post) {
    $dates_array[] = date("Y-m-d", strtotime($post->post_created_date));
    //$reviewed = Post::get_posts_count(3, $user->id, $date);
}

$published_array = array();
foreach ($posts as $post) {
    $date = date("Y-m-d", strtotime($post->post_created_date));
    $published_array[] = Post::get_posts_count(1, $user->id, $date);
}

$pending_array = array();
foreach ($posts as $post) {
    $date = date("Y-m-d", strtotime($post->post_created_date));
    $pending_array[] = Post::get_posts_count(2, $user->id, $date);
}

$reviewed_array = array();
foreach ($posts as $post) {
    $date = date("Y-m-d", strtotime($post->post_created_date));
    $reviewed_array[] = Post::get_posts_count(3, $user->id, $date);
}
?>

<script>
    $(document).ready(function () {

        $('.datepick').datetimepicker({
            format: 'YYYY-MM-DD',
        });

        var lineData = {
            labels: [<?php echo '"' . join('", "', $dates_array) . '"'; ?>],
            datasets: [
                {
                    label: "الأخبار المنشورة",
                    fillColor: "rgba(26,179,148,0.5)",
                    strokeColor: "rgba(26,179,148,0.7)",
                    pointColor: "rgba(26,179,148,1)",
                    pointStrokeColor: "#fff",
                    pointHighlightFill: "#fff",
                    pointHighlightStroke: "rgba(26,179,148,1)",
                    data: [<?php echo join(",", $published_array); ?>]
                },
                {
                    label: "أخبار بانتظار المراجعة",
                    fillColor: "rgba(220,220,220,0.5)",
                    strokeColor: "rgba(220,220,220,1)",
                    pointColor: "rgba(220,220,220,1)",
                    pointStrokeColor: "#fff",
                    pointHighlightFill: "#fff",
                    pointHighlightStroke: "rgba(220,220,220,1)",
                    data: [<?php echo join(",", $pending_array); ?>]
                },
                {
                    label: "أخبار تم مراجعتها",
                    fillColor: "#464F88",
                    strokeColor: "#464F88",
                    pointColor: "#464F88",
                    pointStrokeColor: "#fff",
                    pointHighlightFill: "#fff",
                    pointHighlightStroke: "rgba(26,179,148,1)",
                    data: [<?php echo join(",", $reviewed_array); ?>]
                }


            ]
        };

        var lineOptions = {
            scaleShowGridLines: true,
            scaleGridLineColor: "rgba(0,0,0,.05)",
            scaleGridLineWidth: 1,
            bezierCurve: true,
            bezierCurveTension: 0.4,
            pointDot: true,
            pointDotRadius: 4,
            pointDotStrokeWidth: 1,
            pointHitDetectionRadius: 20,
            datasetStroke: true,
            datasetStrokeWidth: 2,
            datasetFill: true,
            responsive: true,
        };

        var ctx = document.getElementById("lineChart").getContext("2d");
        var myNewChart = new Chart(ctx).Line(lineData, lineOptions);

        $('.chosen-select').chosen();


    });
</script>

<script> /*
 init.push(function () {
 
 Morris.Area({
 element: 'hero-area',
 data: [
<?php
foreach ($posts as $post) {
    $date = date("Y-m-d", strtotime($post->post_created_date));
    $published = Post::get_posts_count(1, $user->id, $date);
    $pending = Post::get_posts_count(2, $user->id, $date);
    $reviewed = Post::get_posts_count(3, $user->id, $date);
    ?>
             {period: '<?php echo date("Y-m-d", strtotime($post->post_created_date)) ?>', published: <?php echo $published; ?>, reviewed: <?php echo $pending; ?>, pending: <?php echo $reviewed; ?>},
<?php } ?>

</script>


@stop

@stop
