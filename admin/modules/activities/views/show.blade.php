@extends("admin::layouts.master")

@section("breadcrumb")
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-4">
        <h2><?php echo trans("activities::activities.activities") ?></h2>
        <ol class="breadcrumb">
            <li>
                <a href="<?php echo URL::to(ADMIN . "/activities"); ?>"><?php echo trans("activities::activities.activities") ?></a>
            </li>
        </ol>
    </div>

    <div class="col-lg-6">
        <form action="" method="get" class="search_form">
            <div class="input-group">
                <input name="q" value="<?php echo Input::get("q"); ?>" type="text" class=" form-control" placeholder="<?php echo trans("users::users.search_users") ?>..."> <span class="input-group-btn">
                    <button class="btn btn-primary" type="button"> <?php echo trans("activities::activities.search") ?></button> </span>
            </div>
        </form>
    </div>
</div>
@stop

@section("content")

<div id="content-wrapper">

    <?php if (Session::has("message")) { ?>
        <div class="alert alert-success">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <?php echo Session::get("message"); ?>
        </div>
    <?php } ?>


    <form action="" method="get" class="action_form">



        <form action="" method="post" class="action_form">

            <div class="ibox float-e-margins panel panel-default">
                <div class="ibox-title">
                    <h5> <?php echo trans("users::users.users") ?> </h5>
                </div>
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-sm-3 m-b-xs">
                            <div class="form-group">
                                <select name="action" class="form-control chosen-select chosen-rtl" style="width:auto; display: inline-block;">
                                    <option value="-1" selected="selected"><?php echo trans("users::users.bulk_actions"); ?></option>
                                    <option value="delete"><?php echo trans("users::users.delete"); ?></option>
                                </select>
                                <button type="submit" class="btn btn-primary" style="margin-top:5px"><?php echo trans("users::users.apply"); ?></button>
                            </div>

                        </div>
                        <div class="col-sm-6 m-b-xs">

                            <div style="display:inline-block; margin-top: 6px">

                                <div class="input-group date datepick">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    <input name="start" type="text" value="<?php echo Input::get("start"); ?>" class="form-control" placeholder="<?php echo trans("activities::activities.start_date") ?>">
                                </div>

                                <div class="input-group date datepick">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    <input name="end" type="text" value="<?php echo Input::get("end"); ?>" class="form-control" placeholder="<?php echo trans("activities::activities.end_date") ?>">
                                </div>

                                <?php /*
                                 * 
                                 *   <span class="input-group-addon">{{Lang::get('activities::activities.to')}}</span>
                                  <input type="text" class="form-control" name="end" value="<?php echo Input::get("end"); ?>" placeholder="نهاية"/>
                                 */ ?>

                            </div>
                            <button style="display: inline-block;margin-top: -23px;" type="submit" class="btn btn-primary"><?php echo trans("activities::activities.view") ?></button>
                        </div>
                        <div class="col-sm-3">

                            <select  name="post_status" id="post_status" class="form-control chosen-select chosen-rtl per_page_filter">
                                <option value="" selected="selected">-- <?php echo trans("users::users.per_page") ?> --</option>
                                <?php foreach (array(10, 20, 30, 40) as $num) { ?>
                                    <option value="<?php echo $num; ?>" <?php if ($num == $per_page) { ?> selected="selected" <?php } ?>><?php echo $num; ?></option>
                                <?php } ?>
                            </select>

                        </div>
                    </div>
                    <div class="table-responsive">

                        <table cellpadding="0" cellspacing="0" border="0" class="table table-striped " id="jq-datatables-example">
                            <thead>
                                <tr>
                                    <th style="width:35px"><input type="checkbox" class="check_all i-checks" name="ids[]" /></th>
                                    <th style="width:50px"><?php echo trans("users::users.photo") ?></th>
                                    <th><?php echo trans("users::users.name"); ?></th>
                                    <th><?php echo trans("activities::activities.published") ?></th>
                                    <th><?php echo trans("activities::activities.pending") ?></th>
                                    <th><?php echo trans("activities::activities.reviewed") ?></th>
                                    <th><?php echo trans("users::users.role"); ?></th>
                                    <th><?php echo trans("users::users.actions") ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($users as $user) { ?>
                                    <tr>
                                        <td>
                                            <input class="i-checks" type="checkbox" name="id[]" value="<?php echo $user->id; ?>" />
                                        </td>
                                        <td>
                                            <?php if ($user->media_path != "") { ?>
                                                <img class="img-rounded" src="<?php echo thumbnail($user->media_path) ?>" />
                                            <?php } else { ?>
                                                <img class="img-rounded" src="<?php echo assets("images/user.png"); ?>" />
                                            <?php } ?>
                                        </td>
                                        <td>
                                            <a class="text-navy" href="<?php echo URL::to(ADMIN) ?>/activities/user/<?php echo $user->id; ?>">
                                                <?php if ($user->first_name != "") { ?>
                                                    <?php echo $user->first_name . " " . $user->last_name; ?>
                                                <?php } else { ?>
                                                    <?php echo $user->username; ?>
                                                <?php } ?>
                                            </a>
                                        </td>
                                        <td><?php echo Post::get_posts_count(1, $user->id); ?></td>
                                        <td><?php echo Post::get_posts_count(2, $user->id); ?></td>
                                        <td><?php echo Post::get_posts_count(3, $user->id); ?></td>

                                        <td><?php echo $user->role_name; ?></td>
                                        <td class="center">
                                            <a href="<?php echo URL::to(ADMIN) ?>/users/<?php echo $user->id; ?>/edit">
                                                <i class="fa fa-pencil text-navy"></i>
                                            </a>

                                            <a class="ask" message="<?php echo trans("users::users.sure_delete") ?>" href="<?php echo URL::to(ADMIN) ?>/users/delete?id[]=<?php echo $user->id; ?>">
                                                <i class="fa fa-times text-navy"></i>
                                            </a>

                                        </td>
                                    </tr>
                                <?php } ?>

                            </tbody>
                        </table>

                        <div class="table-footer clearfix">
                            <div class="DT-label">
                                <div class="dataTables_info" id="jq-datatables-example_info" user="alert" aria-live="polite" aria-relevant="all"><?php echo trans("users::users.page") ?> <?php echo $users->currentPage() ?> <?php echo trans("users::users.of") ?> <?php echo $users->lastPage() ?> </div>
                            </div>
                            <div class="DT-pagination">
                                <div class="dataTables_paginate paging_simple_numbers" id="jq-datatables-example_paginate">
                                    <?php echo str_replace('/?', '?', $users->appends(Input::all())->render()); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    </form>
                    <!-- /11. $JQUERY_DATA_TABLES -->

                    <!-- /9. $UNIQUE_VISITORS_STAT_PANEL -->

                </div>
            </div> <!-- / #content-wrapper -->

            @section("header")
            @parent
            <link href="<?php echo assets('css/plugins/datetimepicker/bootstrap-datetimepicker.min.css') ?>" rel="stylesheet" type="text/css">
            <style>
                .input-group.date.datepick {
                    float: right;
                    margin-right: 3px;
                    width: 140px;
                }

            </style>
            @stop

            @section('footer')

            <script type="text/javascript" src="<?php echo assets('js/plugins/moment/moment.min.js') ?>"></script>
            <script type="text/javascript" src="<?php echo assets('js/plugins/datetimepicker/bootstrap-datetimepicker.min.js') ?>"></script>
            <script>


                $(document).ready(function () {
                    
                    $('.chosen-select').chosen();

                    $('.datepick').datetimepicker({
                        format: 'YYYY-MM-DD',
                    });

                    $('.i-checks').iCheck({
                        checkboxClass: 'icheckbox_square-green',
                        radioClass: 'iradio_square-green',
                    });

                    $('.check_all').on('ifChecked', function (event) {
                        $("input[type=checkbox]").each(function () {
                            $(this).iCheck('check');
                            $(this).change();
                        });
                    });

                    $('.check_all').on('ifUnchecked', function (event) {
                        $("input[type=checkbox]").each(function () {
                            $(this).iCheck('uncheck');
                            $(this).change();
                        });
                    });

                    $(".per_page_filter").change(function () {
                        var base = $(this);
                        var per_page = base.val();
                        location.href = "<?php echo route("activities.show") ?>?per_page=" + per_page;
                    });

                    /*$('#bs-datepicker-range').datepicker({
                     orientation: $('body').hasClass('right-to-left') ? "auto right" : 'auto auto'
                     });*/


                });

            </script>

            @stop
            @stop
