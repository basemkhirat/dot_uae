<?php

Route::group(array(
    "prefix" => ADMIN,
    "before" => "auth",
        ), function($route) {
    $route->group(array("prefix" => "activities"), function($route) {
        $route->any('/', array("as" => "activities.show", "uses" => "ActivitiesController@index"));
        $route->any('/user/{id}', array("as" => "activities.user", "uses" => "ActivitiesController@user"));
    });
});
