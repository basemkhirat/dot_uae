<?php

return [
    "permissions" => [
        "create",
        "edit",
        "delete"
    ]
];
