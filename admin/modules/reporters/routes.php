<?php

Route::group(array(
    "prefix" => ADMIN,
    "before" => "auth",
        ), function($route) {
    $route->group(array("prefix" => "reporters"), function($route) {
        $route->any('/', array("as" => "reporters.show", "uses" => "ReportersController@index"));
        $route->any('/create', array("as" => "reporters.create", "uses" => "ReportersController@create"));
        $route->any('/{id}/edit', array("as" => "reporters.edit", "uses" => "ReportersController@edit"));
        $route->any('/delete', array("as" => "reporters.delete", "uses" => "ReportersController@delete"));
    });
});
