<?php

Menu::set("sidebar", function($menu) {
    if (User::can('reporters')) {
        $menu->item('users.reporters', trans("admin::common.reporters"), route("reporters.show"));        
    }
});

include __DIR__ ."/routes.php";
