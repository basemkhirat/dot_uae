<?php

return [
    "reporters" => "Editors",
    "edit" => "Edit editor",
    "add_new" => "Add new",
    "search_users" => "Search editors ..",
    "search" => "Search",
    "per_page" => "Per page",
    "bulk_actions" => "Bulk actions",
    "delete" => "Delete",
    "apply" => "Apply",
    "photo" => "Photo",
    "name" => "Name",
    "created" => "Created date",
    "articles" => "articles",
    "role" => "Role",
    "actions" => "Actions",
    "sure_delete" => "You are about to delete editor.. continue?",
    "page" => "Page",
    "add_new_user" => "Add new editor",
    "confirm_password" => "Confirm password",
    "change" => "Change",
    "first_name" => "Editor name",
    "is_not_an_image" => "is not an image",
    "role" => "Role",
    "activation" => "Activation",
    "save_user" => "Save editor",
    "of" => "of",
    "activated" => "Activated",
    "deactivated" => "Deactivated",
    "reporter_created" => "Reporter created successfully",
    "reporter_updated" => "Reporter updated successfully",
    "reporter_deleted" => "Reporter deleted successfully",
    
    
    "facebook" => "Facebook",
    "twitter" => "Twitter",
    "googleplus" => "Google plus",
    "linkedin" => "Linked in",
    
    
    "module" => "reporters",
    "permissions" => [
        "create" => "Create reporters",
        "edit" => "Edit reporters",
        "delete" => "Delete reporters",
    ]
];
