<?php

return [
    "reporters" => "المراسلين",
    "add_new" => "أضف جديد",
    "edit" => "تحرير محرر",
    "search_reporters" => "البحث عن محرر",
    "per_page" => "بكل صفحة",
    "search" => "بحث",
    "bulk_actions" => "إختر الحدث",
    "delete" => "حذف",
    "apply" => "حفظ",
    "photo" => "صورة",
    "name" => "الإسم",
    "created" => "تاريخ التسجيل",
    "articles" => "المقالات",
    "role" => "الصلاحية",
    "actions" => "الحدث",
    "sure_delete" => "هل أنت متأكد من الحدث",
    "page" => "الصفحة",
    "add_new_user" => "أضف مراسل جديد",
    "confirm_password" => "تأكيد كلمة المرور",
    "change" => "تغيير",
    "first_name" => "إسم المحرر",
    "last_name" => "الإسم الأخير",
    "about_me" => "عن العضو",
    "is_not_an_image" => "ليست صورة",
    "role" => "الصلاحية",
    "activation" => "التفعيل",
    "language" => "اللغة",
    "special_permissions" => "صلاحيات خاصة",
    "save_user" => "حفظ المراسل",
    "of" => "من",
    "activated" => "مفعل",
    "deactivated" => "غير مفعل",
         "reporter_created" => "تم إضافة مراسل بنجاح",
    "reporter_updated" => "تم تعديل المراسل بنجاح",
    "reporter_deleted" => "تم حذف المراسل بنجاح",
    
    "module" => "المحررين",
    "permissions" => [
        "create" => "إضافة المحررين",
        "edit" => "تحرير المحررين",
        "delete" => "حذف المحررين"
    ]
    

];
