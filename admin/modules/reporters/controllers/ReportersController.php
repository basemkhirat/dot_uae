<?php

class ReportersController extends BackendController {

    public $data = array();

    public function index() {
        
        if (!User::can("reporters.create")) {
            return denied();
        }

        if (Request::isMethod("post")) {
            $action = Input::get("action");
            $ids = Input::get("id");
            if (count($ids)) {
                switch ($action) {
                    case "delete":
                        User::whereIn("id", $ids)->delete();
                        return Redirect::back()->with("message", trans("reporters::reporters.reporter_deleted"));
                        break;
                }
            }
        }

        $ob = User::select(array("users.*", "media.media_path", DB::raw('(select COUNT(*) from posts where posts.post_user_id = users.id) as articles')))
                ->join("roles", "roles.role_id", "=", "users.role_id")
                //->join("roles_langs", "users.role_id", "=", "roles_langs.role_id")
                ->where("roles.role_id", 17)
                ->groupBy("users.id")
                ->orderBy("created_at", "DESC");

        if (Input::has("q")) {
            $ob->where(function($where_ob){
               $where_ob->where('username', 'LIKE', '%' . Input::get("q") . '%')
                       ->orWhere('first_name', 'LIKE', '%' . Input::get("q") . '%')
                       ->orWhere('last_name', 'LIKE', '%' . Input::get("q") . '%');
            });
        }

        if (Input::has("per_page")) {
            $this->data["per_page"] = $per_page = Input::get("per_page");
        } else {
            $this->data["per_page"] = $per_page = 20;
        }


        $this->data["users"] = $ob->leftJoin("media", "media.media_id", "=", "users.photo")
                ->paginate($per_page);
        return View::make("reporters::show", $this->data);
    }

    public function create() {

        if (!User::can("reporters.create")) {
            return denied();
        }

        if (Request::isMethod("post")) {

            $rules = array(
                "first_name" => "required"
            );

            $validator = Validator::make(Input::all(), $rules);
            if ($validator->fails()) {
                return Redirect::back()->withErrors($validator)->withInput(Input::all());
            }

            User::insert(array(
                "username" => str_slug(Input::get("first_name") . " " . Input::get("last_name")),
                "first_name" => Input::get("first_name"),
                "last_name" => Input::get("last_name"),
                "role_id" => 17,
                "photo" => "",
                "about" => "",
                "status" => Input::get("status"),
                "created_at" => date("Y-m-d H:i:S"),
                "facebook" => Input::get("facebook"),
                "twitter" => Input::get("twitter"),
                "google_plus" => Input::get("google_plus"),
                "linked_in" => Input::get("linked_in")
            ));

            $user_id = DB::getPdo()->lastInsertId();
            return Redirect::route("reporters.edit", array("id" => $user_id))->with("message", trans("reporters::reporters.reporter_created"));
        }


        $this->data["user"] = false;
        $this->data["roles"] = Role::get();
        return View::make("reporters::edit", $this->data);
    }

    public function edit($user_id) {
        
        $user = User::where("id", "=", $user_id)
                        ->leftJoin("media", "media.media_id", "=", "users.photo")->first();

        if (count($user) == 0) {
            \App::abort(404);
        }

        if (Request::isMethod("post")) {

            $rules = array(
                "first_name" => "required"
            );

            $validator = Validator::make(Input::all(), $rules);
            if ($validator->fails()) {
                return Redirect::back()->withErrors($validator)->withInput(Input::all());
            }

            User::where("id", $user_id)->update(array(
                "first_name" => Input::get("first_name"),
                "last_name" => Input::get("last_name"),
                "role_id" => 17,
                "photo" => "",
                "about" => "",
                "status" => Input::get("status"),
                "updated_at" => date("Y-m-d H:i:S"),
                "facebook" => Input::get("facebook"),
                "twitter" => Input::get("twitter"),
                "google_plus" => Input::get("google_plus"),
                "linked_in" => Input::get("linked_in")
            ));


            return Redirect::route("reporters.edit", array("id" => $user_id))->with("message", trans("reporters::reporters.reporter_updated"));
        }

        $this->data["user"] = $user;
        $this->data["roles"] = Role::get();

        return View::make("reporters::edit", $this->data);
    }

    public function delete() {
        
        if (!User::can("reporters.delete")) {
            return denied();
        }
        
        $user_id = Input::get("user_id");
        User::where("id", $user_id)->delete();
        UserPermission::where("user_id", $user_id)->delete();

        return Redirect::back()->with("message", trans("reporters::reporters.reporter_deleted"));
    }

}
