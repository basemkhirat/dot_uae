@extends("admin::layouts.master")

@section("breadcrumb")
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-4">
        <h2><?php echo trans("reporters::reporters.reporters") ?></h2>
        <ol class="breadcrumb">
            <li>
                <a href="<?php echo URL::to(ADMIN . "/reporters"); ?>"><?php echo trans("reporters::reporters.reporters") ?> (<?php echo $users->total() ?>)</a>
            </li>
        </ol>
    </div>

    <div class="col-lg-6">
        <form action="" method="get" class="search_form">
            <div class="input-group">
                <input name="q" value="<?php echo Input::get("q"); ?>" type="text" class=" form-control" placeholder="<?php echo trans("reporters::reporters.search_users") ?>..."> <span class="input-group-btn">
                    <button class="btn btn-primary" type="button"> <?php echo trans("reporters::reporters.search") ?></button> </span>
            </div>
        </form>
    </div>

    <div class="col-lg-2">
        <?php if (User::can("reporters.create")) { ?>
            <a href="<?php echo route("reporters.create"); ?>" class="btn btn-primary btn-labeled btn-main"> <span class="btn-label icon fa fa-plus"></span> &nbsp; <?php echo trans("reporters::reporters.add_new") ?></a>
        <?php } ?>
    </div>
</div>
@stop

@section("content")
<div id="content-wrapper">

    <?php if (Session::has("message")) { ?>
        <div class="alert alert-success">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <?php echo Session::get("message"); ?>
        </div>
    <?php } ?>


    <form action="" method="post" class="action_form">

        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5> <?php echo trans("reporters::reporters.reporters") ?> </h5>
            </div>
            <div class="ibox-content">
                <div class="row">
                    <div class="col-sm-5 m-b-xs">
                        <div class="form-group">
                            <select name="action" class="form-control chosen-select chosen-rtl" style="width:auto; display: inline-block;">
                                <option value="-1" selected="selected"><?php echo trans("reporters::reporters.bulk_actions") ?></option>
                                <?php if (User::can("reporters.delete")) { ?>
                                    <option value="delete"><?php echo trans("reporters::reporters.delete") ?></option>
                                <?php } ?>
                            </select>
                            <button type="submit" class="btn btn-primary" style="margin-top:5px"><?php echo trans("reporters::reporters.apply") ?></button>
                        </div>

                    </div>
                    <div class="col-sm-4 m-b-xs">

                    </div>
                    <div class="col-sm-3">

                        <select  name="post_status" id="post_status" class="form-control chosen-select chosen-rtl per_page_filter">
                            <option value="" selected="selected">-- <?php echo trans("reporters::reporters.per_page") ?> --</option>
                            <?php foreach (array(10, 20, 30, 40) as $num) { ?>
                                <option value="<?php echo $num; ?>" <?php if ($num == $per_page) { ?> selected="selected" <?php } ?>><?php echo $num; ?></option>
                            <?php } ?>
                        </select>

                    </div>
                </div>
                <div class="table-responsive">


                    <table cellpadding="0" cellspacing="0" border="0" class="table table-striped " id="jq-datatables-example">
                        <thead>
                            <tr>

                                <?php if (User::can("reporters.delete")) { ?>
                                    <th style="width:35px"><input type="checkbox" class="check_all i-checks" name="ids[]" /></th>
                                <?php } ?>

                                <th><?php echo trans("reporters::reporters.name"); ?></th>

                                <th><?php echo trans("reporters::reporters.actions") ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($users as $user) { ?>
                                <tr>

                                    <?php if (User::can("reporters.delete")) { ?>
                                        <td>
                                            <input class="i-checks" type="checkbox" name="id[]" value="<?php echo $user->id; ?>" />
                                        </td>
                                    <?php } ?>

                                    <td>
                                        <?php if (User::can("reporters.edit")) { ?>
                                        <a class="text-navy" href="<?php echo URL::to(ADMIN) ?>/reporters/<?php echo $user->id; ?>/edit">
                                                <?php echo $user->first_name . " " . $user->last_name; ?>
                                            </a>
                                        <?php } else { ?>
                                            <?php echo $user->first_name . " " . $user->last_name; ?>
                                        <?php } ?>
                                    </td>

                                    <td class="center">
                                        <?php if (User::can("reporters.edit")) { ?>
                                            <a href="<?php echo URL::to(ADMIN) ?>/reporters/<?php echo $user->id; ?>/edit">
                                                <i class="fa fa-pencil text-navy"></i>
                                            </a>
                                        <?php } ?>

                                        <?php if (User::can("reporters.delete")) { ?>
                                            <a class="ask" message="<?php echo trans("reporters::reporters.sure_delete") ?>" href="<?php echo URL::route("users.delete", array("user_id" => $user->id)) ?>">
                                                <i class="fa fa-times text-navy"></i>
                                            </a>
                                        <?php } ?>
                                    </td>
                                </tr>
                            <?php } ?>

                        </tbody>
                    </table>

                    <div class="table-footer clearfix">
                        <div class="DT-label">
                            <div class="dataTables_info" id="jq-datatables-example_info" user="alert" aria-live="polite" aria-relevant="all"><?php echo trans("reporters::reporters.page") ?> <?php echo $users->currentPage() ?> <?php echo trans("reporters::reporters.of") ?> <?php echo $users->lastPage() ?> </div>
                        </div>
                        <div class="DT-pagination">
                            <div class="dataTables_paginate paging_simple_numbers" id="jq-datatables-example_paginate">
                                <?php echo str_replace('/?', '?', $users->appends(Input::all())->render()); ?>
                            </div>
                        </div>
                    </div>
                </div>
                </form>
                <!-- /11. $JQUERY_DATA_TABLES -->

                <!-- /9. $UNIQUE_VISITORS_STAT_PANEL -->

            </div>
        </div> <!-- / #content-wrapper -->

        <script>
            $(document).ready(function () {


                $('.chosen-select').chosen();

                $('.i-checks').iCheck({
                    checkboxClass: 'icheckbox_square-green',
                    radioClass: 'iradio_square-green',
                });

                $('.check_all').on('ifChecked', function (event) {
                    $("input[type=checkbox]").each(function () {
                        $(this).iCheck('check');
                        $(this).change();
                    });
                });

                $('.check_all').on('ifUnchecked', function (event) {
                    $("input[type=checkbox]").each(function () {
                        $(this).iCheck('uncheck');
                        $(this).change();
                    });
                });

                $(".per_page_filter").change(function () {
                    var base = $(this);
                    var per_page = base.val();
                    location.href = "<?php echo route("reporters.show") ?>?per_page=" + per_page;
                });

            });



        </script>

        @stop
