@extends("admin::layouts.master")

@section("breadcrumb")
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2><?php echo trans("reporters::reporters.edit") ?></h2>
        <ol class="breadcrumb">
            <li>
                <a href="<?php echo URL::to(ADMIN . "/reporters"); ?>"><?php echo trans("reporters::reporters.reporters") ?></a>
            </li>
            <li class="active">
                <strong><?php echo trans("reporters::reporters.edit") ?></strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
        <?php if (User::can("reporters.create")) { ?>
            <a href="<?php echo route("reporters.create"); ?>" class="btn btn-primary btn-labeled btn-main"> <span class="btn-label icon fa fa-plus"></span> &nbsp; <?php echo trans("reporters::reporters.add_new") ?></a>
        <?php } ?>
    </div>
</div>
@stop



@section("content")

<div id="content-wrapper">

    <?php if (Session::has("message")) { ?>
        <div class="alert alert-success alert-dark">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <?php echo Session::get("message"); ?>
        </div>
    <?php } ?>

    <?php if ($errors->count() > 0) { ?>
        <div class="alert alert-danger alert-dark">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <?php echo implode(' <br /> ', $errors->all()) ?>
        </div>
    <?php } ?>

    <form action="" method="post" >
        <div class="row">
            <div class="col-md-6">
                <div class="panel panel-default">

                    <div class="panel-body">

                        <div class="row">

                            <div class="col-md-12">
                                <div class="form-group">
                                    <input name="first_name" value="<?php echo @Input::old("first_name", $user->first_name); ?>" class="form-control input-lg" placeholder="<?php echo trans("reporters::reporters.first_name") ?>" />
                                </div>


                            </div>
                        </div>

                    </div> <!-- / .panel-body -->
                </div>
                <div class="panel panel-default">

                    <div class="panel-body">

                        <div class="form-group input-group">
                            <span class="input-group-addon"><i class="fa fa-facebook"></i></span>
                            <input name="facebook" value="<?php echo @Input::old("facebook", $user->facebook); ?>" class="form-control input-lg" placeholder="<?php echo trans("reporters::reporters.facebook") ?>" />
                        </div>

                        <div class="form-group input-group">
                            <span class="input-group-addon"><i class="fa fa-twitter "></i></span>
                            <input name="twitter" value="<?php echo @Input::old("twitter", $user->twitter); ?>" class="form-control input-lg" placeholder="<?php echo trans("reporters::reporters.twitter") ?>" />
                        </div>

                        <div class="form-group input-group">
                            <span class="input-group-addon"><i class="fa fa-google-plus"></i></span>
                            <input name="google_plus" value="<?php echo @Input::old("google_plus", $user->google_plus); ?>" class="form-control input-lg" placeholder="<?php echo trans("reporters::reporters.googleplus") ?>" />
                        </div>

                        <div class="form-group input-group">
                            <span class="input-group-addon"><i class="fa fa-linkedin"></i></span>
                            <input name="linked_in" value="<?php echo @Input::old("linked_in", $user->linked_in); ?>" class="form-control input-lg" placeholder="<?php echo trans("reporters::reporters.linkedin") ?>" />
                        </div>

                    </div> <!-- / .panel-body -->


                </div>


            </div>

            <script>
                $(document).ready(function () {
                    $("#change_photo").filemanager({
                        types: "png|jpg|jpeg|gif|bmp",
                        done: function (files) {
                            if (files.length) {
                                var file = files[0];
                                $("#user_photo_id").val(file.media_id);
                                $("#user_photo").attr("src", file.media_thumbnail);
                            }
                        },
                        error: function (media_path) {
                            alert(media_path + " <?php echo trans("reporters::reporters.is_not_an_image") ?>");
                        }
                    });
                });

            </script>
            <div class="col-md-6">
                <div class="panel panel-default">

                    <div class="panel-body">

                        <div class="row form-group">
                            <label class="col-sm-3 control-label"><?php echo trans("reporters::reporters.activation") ?></label>
                            <div class="col-sm-9">
                                <select class="form-control chosen-select chosen-rtl" name="status">
                                    <option value="1" <?php if ($user and $user->status == 1) { ?> selected="selected" <?php } ?>><?php echo trans("reporters::reporters.activated") ?></option>
                                    <option value="0" <?php if ($user and $user->status == 0) { ?> selected="selected" <?php } ?>><?php echo trans("reporters::reporters.deactivated") ?></option>
                                </select>
                            </div>
                        </div>

                    </div> <!-- / .panel-body -->
                </div>




            </div>
            <div style="clear:both"></div>
            <div class="row">
                <div class="panel-footer" style="border-top: 1px solid #ececec; position: relative;">
                    <div class="form-group" style="margin-bottom:0">
                        <input type="submit" class="pull-right btn btn-flat btn-primary" value="<?php echo trans("reporters::reporters.save_user") ?>" />
                    </div>
                </div>
            </div>
            <!-- /6. $EASY_PIE_CHARTS -->
        </div>
    </form>
    <!-- /9. $UNIQUE_VISITORS_STAT_PANEL -->

</div>
</div> <!-- / #content-wrapper -->


<script>
    $(document).ready(function () {
        $('.chosen-select').chosen();
    });
</script>



@stop
