<?php

Menu::set("sidebar", function($menu) {

    if (User::can('agency')) {
        $menu->item('agencies', trans("admin::common.agencies"), "javascript:void(0)")
                ->order(4)
                ->icon("fa-th-large");

        $agencies = Agency::with('langs')->whereHas('langs', function($q) {
                    $q->whereLang(LANG);
                })->get();

        foreach ($agencies as $agency) {
            $menu->item('agencies.' . $agency->agency_slug, $agency->langs->where('lang', LANG)->first()->agency_name, URL::to(ADMIN . '/agency?agency_id=' . $agency->agency_slug));
            
            $menu->item('agencies.' . $agency->agency_slug . ".all", trans("admin::common.all_news"), URL::to(ADMIN . '/agency?agency_id=' . $agency->agency_slug))
                    ->icon("fa-th-large");

            $agency_cats = AgencyCat::with('langs')->where('agency_id', $agency->agency_id)->whereHas('langs', function($q) {
                        $q->whereLang(LANG);
                    })->get();

            foreach ($agency_cats as $cat) {
                $menu->item('agencies.' . $agency->agency_slug . "." . $cat->cat_slug, $cat->langs->where('lang', LANG)->first()->cat_name, URL::to(ADMIN . '/agency?cat_id=' . $cat->cat_slug))
                        ->icon("fa-th-large");
            }
        }
    }
});

include __DIR__ . "/routes.php";
include __DIR__ . "/helpers.php";

