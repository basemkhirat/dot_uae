@extends("admin::layouts.master")

<?php
$conn = Session::get('conn');
?>

<link href="<?php echo assets() ?>/css/plugins/toastr/toastr.min.css" rel="stylesheet">

<input type="hidden" name="post_temp" value="" id="post_temp">

<a id="move" data-toggle="modal" data-target="#moveModel" style="display:none; "></a>
<!-- Modal -->
<div id="moveModel" class="modal fade" tabindex="-2" role="dialog" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></button>

                <h4 class="modal-title" >{!!Lang::get('agency::agency.move_post')!!}</h4>
            </div>
            <div class="modal-body" style="height:450px; overflow: auto;">

                <div class="form-group" >
                    <?php $cats = Category::where('cat_parent', 0)->whereSite($conn)->get();?>
                    @foreach($cats as $cat)
                    <div class="form-group">
                        <div class="radio i-checks"><label> <input type="radio" value="{!!$cat->id!!}" name="cats">&nbsp;&nbsp; {!!$cat->cat_name!!} </label></div>
                    </div>
                    @endforeach
                </div>
            </div> <!-- / .modal-body -->
            <div class="modal-footer">
                <input type="hidden" id="block_id" value="">
                <button type="button" class="btn btn-default" data-dismiss="modal">{!!Lang::get('agency::agency.close')!!}</button>
                <span id="page-alerts-demo">
                    <button type="button" class="btn btn-primary" data-dismiss="modal" id="save_move" style="display:" >{!!Lang::get('agency::agency.save_changes')!!}</button>
                </span>
            </div>
        </div> <!-- / .modal-content -->
    </div> <!-- / .modal-dialog -->
</div> <!-- /.modal -->
<!-- / Modal -->

@section("breadcrumb")
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2><i class="fa fa-th-large faa-tada animated faa-slow"></i> {!!Lang::get('agency::agency.agency_posts')!!}</h2>
        <ol class="breadcrumb">
            <ol class="breadcrumb" style="background: none;">
                <li><a  href="{!!URL::to('/'.ADMIN.'/dashboard')!!}">{!!Lang::get('admin::common.dashboard')!!}</a></li>
                <li><a  href="{!!URL::to('/'.ADMIN.'/agency')!!}">{!!Lang::get('agency::agency.agency_posts')!!}</a></li>
                <li>{!!Lang::get('posts::posts.preview')!!}</li>
            </ol>
        </ol>
    </div>

</div>
@stop
@section("content")

<div id="content-wrapper">
    <div class="row wrapper animated fadeInRight ">
        <div class="col-lg-10 col-lg-offset-1">
            <div class="ibox-content">
                <div class="pull-right">
                    <div class="tooltip-demo m-t-md">
                        @if(User::can('agency.move') )
                        <a data-toggle="tooltip" data-placement="top" href="#{!!$post->post_id!!}" data-name='{!!$post->post_title!!}' class="move btn btn-success btn-sm" title="{!!Lang::get('agency::agency.move')!!}"><i class="fa fa-paper-plane-o"></i></a>
                        @endif
                    </div>
                </div>
                <div class="text-center article-title">
                    <span class="text-muted"><i class="fa fa-clock-o"></i> @if(DIRECTION == 'rtl') {!!arabic_date($post->post_created_date)!!} @else {!! date('M d, Y', strtotime($post->post_created_date)) !!} @ {!! date('h:i a', strtotime($post->post_created_date)) !!} @endif</span>
                    <h1>
                        {!!$post->post_title!!}
                    </h1>
                </div>
                <div class="full-height-scroll white-bg">
                    <div style="padding: 20px">
                        {!!htmlspecialchars_decode($post->post_content)!!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<script src="<?php echo assets() ?>/js/bootbox.min.js"></script>
<script src="<?php echo assets() ?>/js/plugins/toastr/toastr.min.js"></script>

<script type="text/javascript">

$(document).ready(function () {

    $('.i-checks').iCheck({
        radioClass: 'iradio_square-green',
    });

    $("#save_move").click(function (e) {
        e.preventDefault();

        if ($("[name=cats]:checked").size() == 0) {
            return false;
        }

        $('#overlay').show();
        toastr.options = {
            closeButton: true,
            progressBar: true,
            showMethod: 'slideDown',
            timeOut: 4000
        };

        ajaxData = {
            cats: $('[name=cats]:checked').val(),
            post_id: $("#post_temp").val(),
        };
        console.log(ajaxData);
        $.ajax({
            type: "POST",
            dataType: 'json',
            url: '{!! URL::to("/".ADMIN."/agency/savemove") !!}',
            data: ajaxData,
            beforeSend: function (res) {

            },
            success: function (res) {
                $('#overlay').hide();

                if (res == 'found') {
                    toastr.error("{!!Lang::get('posts::posts.repeated')!!}", "{!!Lang::get('posts::posts.error')!!}");
                } else {
                    toastr.success("{!!Lang::get('posts::posts.moved')!!}", "{!!Lang::get('posts::posts.well_done')!!}");
                }
            },
            complete: function () {
                $('#save_load').remove();
                $(".move").show();
            },
            error: function () {
                $('#overlay').hide();
                toastr.error("{!!Lang::get('posts::posts.not_moved')!!}", "{!!Lang::get('posts::posts.error')!!}");
            }
        });
    });

    $(".move").click(function (e) {
        e.preventDefault();
        var id = $(this).attr("href").substr(1);
        $("#post_temp").val(id);
        $("#move").click();
    });


});

</script>


@stop