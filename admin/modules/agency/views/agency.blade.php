@extends("admin::layouts.master")


<?php
$conn = Session::get('conn');
$params = '';
foreach (Input::except('per_page', 'page') as $key => $value) {
    $params .= '&' . $key . '=' . $value;
}
?>
<link href="<?php echo assets() ?>/css/plugins/toastr/toastr.min.css" rel="stylesheet">

<input type="hidden" name="post_temp" value="" id="post_temp">

<a id="move" data-toggle="modal" data-target="#moveModel" style="display:none; "></a>
<!-- Modal -->
<div id="moveModel" class="modal fade" tabindex="-2" role="dialog" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></button>

                <h4 class="modal-title" >{!!Lang::get('agency::agency.move_post')!!}</h4>
            </div>
            <div class="modal-body" style="height:450px; overflow: auto;">

                <div class="form-group" >
                    <?php $cats = Category::where('cat_parent', 0)->whereSite($conn)->get(); ?>
                    @foreach($cats as $cat)
                    <div class="form-group">
                        <div class="radio i-checks"><label> <input type="radio" value="{!!$cat->id!!}" name="cats">&nbsp;&nbsp; {!!$cat->cat_name!!} </label></div>
                    </div>
                    @endforeach
                </div>
            </div> <!-- / .modal-body -->
            <div class="modal-footer">
                <input type="hidden" id="block_id" value="">
                <button type="button" class="btn btn-default" data-dismiss="modal">{!!Lang::get('agency::agency.close')!!}</button>
                <span id="page-alerts-demo">
                    <button type="button" class="btn btn-primary" data-dismiss="modal" id="save_move" style="display:" >{!!Lang::get('agency::agency.save_changes')!!}</button>
                </span>
            </div>
        </div> <!-- / .modal-content -->
    </div> <!-- / .modal-dialog -->
</div> <!-- /.modal -->
<!-- / Modal -->

@section("breadcrumb")
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-8">
        <h2><i class="fa fa-th-large faa-tada animated faa-slow"></i> {!!Lang::get('agency::agency.agency_posts')!!} ({!!$posts->total()!!})</h2>
        <ol class="breadcrumb" style="background: none;">
            <li><a  href="{!!URL::to('/'.ADMIN.'/dashboard')!!}">{!!Lang::get('admin::common.dashboard')!!}</a></li>
            <li>{!!Lang::get('agency::agency.agency_posts')!!}</li>
        </ol>
    </div>
    <div class="col-sm-4 m-b-xs text-left" >
        {!! Form::open(array('url' => ADMIN.'/agency', 'method' => 'get', 'id' => 'search', 'style' => ' margin-top: 30px;', 'class' => 'choosen-rtl')) !!}

        <div class="input-group">
            <input type="text" placeholder="{!!Lang::get('agency::agency.search')!!}" class="input-sm form-control" value="{!!(Input::get('q')) ? Input::get('q') : ''!!}" name="q" id="q"> <span class="input-group-btn">
                <button type="submit" class="btn btn-sm btn-primary"> {!!Lang::get('agency::agency.search')!!}</button> </span>
        </div>
        {!! Form::close() !!}
    </div>
</div>
@stop
@section("content")

<div id="content-wrapper">

    {!! Form::open(array('url' => ADMIN.'/agency/delete', 'method' => 'POST', 'id' => 'form')) !!}
    <div class="row wrapper animated fadeInRight ">

        <div id="content-wrapper">


            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5> {!!Lang::get('agency::agency.agency_posts')!!} </h5>

                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>

                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-sm-10 m-b-xs">
                            <select name="action" id="action" class="form-control m-b" style="width:auto; display: inline-block;">
                                <option value="" selected="selected">{!!Lang::get('agency::agency.bulk')!!}</option>
                                @if(User::can('agency.delete') )
                                <option value="0">{!!Lang::get('agency::agency.delete')!!}</option>
                                @endif
                            </select>

                            <button class="btn btn-primary">{!!Lang::get('agency::agency.apply')!!}</button>

                            <button type="button" class="btn disabled btn-primary" id="download">{!!Lang::get('agency::agency.download_images')!!}</button>
                        </div>

                        <div class="col-sm-2">

                            <select class="form-control chosen-rtl" id="per_page" name="per_page">
                                <option selected="selected" value="">-- {!!Lang::get('agency::agency.per_page')!!} --</option>
                                <option selected="selected" value="10">10</option>
                                <option value="20">20</option>
                                <option value="30">30</option>
                                <option value="40">40</option>
                            </select>
                        </div>
                    </div>
                    <div class="table-responsive">

                        <table cellspacing="0" cellpadding="0" border="0" class="table table-striped">
                            <thead>
                                <tr>
                                    <th><div class="action-checkbox"><label class="px-single"><input type="checkbox" name="checkall" id="checkall" value="" class="i-checks"><span class="lbl"></span></label></div></th>
                            <th>{!!Lang::get('agency::agency.image')!!}</th>
                            <th style="width:650px">{!!Lang::get('agency::agency.title')!!}</th>
                            <th>{!!Lang::get('agency::agency.created_on')!!}</th>
                            <th>{!!Lang::get('agency::agency.edit')!!}</th>
                            </tr>
                            </thead>
                            <tbody >
                                <?php $count = count($posts);?>
                                @if($count)
                                @foreach($posts as $post)
                                <tr class="odd gradeX">
                                    <td><div class="action-checkbox"><label class="px-single"><input type="checkbox" name="check[]" value="{!!$post->post_id!!}" data-image="{!!$post->media_path!!}" class="i-checks check_in"><span class="lbl"></span></label></div></td>
                                    <td>
                                        <img src="{{{ $post->image ? $post->image->getImageURL('small') : assets("images/default.png") }}}" width="100%" height="100px">
                                    </td>
                                    <td>
                                        <a href="{!!URL::to(ADMIN.'/agency/'.$post->post_id.'/edit')!!}">  {!!$post->post_title!!}</a>
                                    </td>

                                    <td class="center">@if(DIRECTION == 'rtl') {!!arabic_date($post->post_created_date)!!} @else {!! date('M d, Y', strtotime($post->post_created_date)) !!} @ {!! date('h:i a', strtotime($post->post_created_date)) !!} @endif</td>
                                    <td class="tooltip-demo m-t-md">
                                        <a class="btn btn-white btn-sm" data-toggle="tooltip" data-placement="top" href="{!!URL::to(ADMIN.'/agency/'.$post->post_id.'/edit')!!}" title="{!!Lang::get('agency::agency.view')!!}"><i class="fa fa-pencil"></i></a>

                                        @if(User::can('agency.delete'))
                                        <a data-toggle="tooltip" data-placement="top" href="{!!URL::to(ADMIN.'/agency/'.$post->post_id.'/delete')!!}" class="delete_agency btn btn-white btn-sm" message="{!!Lang::get('agency::agency.delete_post_q')!!}" title="{!!Lang::get('agency::agency.delete')!!}"><i class="fa fa-trash-o"></i></a>
                                        @endif
                                        @if(User::can('agency.move') )
                                        <a data-toggle="tooltip" data-placement="top" href="#{!!$post->post_id!!}" data-name='{!!$post->post_title!!}' class="move btn btn-success btn-sm" title="{!!Lang::get('agency::agency.move')!!}"><i class="fa fa-paper-plane-o"></i></a>
                                        @endif
                                    </td>
                                </tr>
                                @endforeach
                                @else
                                <tr class="odd gradeX" style="height:200px">
                                    <td colspan="5" style="vertical-align:middle; text-align:center; font-weight:bold; font-size:22px">{!!Lang::get('agency::agency.no_posts')!!}</td>
                                </tr>
                                @endif
                            </tbody>
                        </table>
                        <div class="panel-footer text-right" style="border-top: 1px solid #ececec; background: none;">
                            <div class="col-sm-2 m-b-xs" style="padding:0">
                                @if($count)
                                {!!Lang::get('agency::agency.showing')!!} {!!$posts->firstItem()!!} {!!Lang::get('agency::agency.to')!!} {!!$posts->lastItem()!!} {!!Lang::get('agency::agency.of')!!} {!!$posts->total()!!} {!!Lang::get('agency::agency.posts')!!}
                                @endif
                            </div>

                            <div class="col-sm-10 m-b-xs" style="padding:0">
                                <div class="pull-right">
                                    @if($count)
                                    {!! $posts->appends(Input::all())->setPath('')->render() !!}
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>

    {!! Form::close() !!}

</div>
</div> <!-- / #content-wrapper -->

<script src="<?php echo assets() ?>/js/bootbox.min.js"></script>
<script src="<?php echo assets() ?>/js/plugins/toastr/toastr.min.js"></script>

<script type = "text/javascript" >
    $(document).ready(function () {
        $('#per_page').change(function () {
            location.href = '{!!URL::to("/".ADMIN."/agency")!!}' + '?per_page=' + $(this).val() + '{!!$params!!}'

        });

        $('#per_page').val("{!!Input::get('per_page')!!}");


        // check all action
        $('#checkall').on('ifChecked', function (event) {
            $("input[name='check[]']").each(function () {
                $(this).iCheck('check');
                $(this).change();
            });
        });
        $('#checkall').on('ifUnchecked', function (event) {
            $("input[name='check[]']").each(function () {
                $(this).iCheck('uncheck');
                $(this).change();
            });
        });
        $('.i-checks').iCheck({
            checkboxClass: 'icheckbox_square-green',
            radioClass: 'iradio_square-green',
        });


        var images = "";
        $('.check_in').on('ifChanged', function (event) {
            new_image = $(this).attr('data-image');
            if (this.checked) {
                all_images = images.split(",");
                found = $.inArray(new_image, all_images) > -1;
                if (!found && new_image != '') {
                    images = images + new_image + ",";
                }
            } else {
                images = images.replace(new_image + ",", "");
            }

            if ($('[name="check[]"]:checked').length && images != '') {
                $("#download").removeClass('disabled');
            } else {
                $("#download").addClass('disabled');
            }
        });

        $("#download").click(function () {
            console.log(images);
            location.href = "{!!URL::to('/'.ADMIN.'/agency/download')!!}?images=" + images;
        });

        // form submit
        $("#form").submit(function (e) {
            if ($("input[name='check[]']:checked").length == 0 || $("#action").val() == '') {
                return false;
            }
        });

        $("#save_move").click(function (e) {
            e.preventDefault();

            if ($("[name=cats]:checked").size() == 0) {
                return false;
            }

            $('#overlay').show();
            toastr.options = {
                closeButton: true,
                progressBar: true,
                showMethod: 'slideDown',
                timeOut: 4000
            };

            ajaxData = {
                cats: $('[name=cats]:checked').val(),
                post_id: $("#post_temp").val(),
            };
            console.log(ajaxData);
            $.ajax({
                type: "POST",
                dataType: 'json',
                url: '{!! URL::to("/".ADMIN."/agency/savemove") !!}',
                data: ajaxData,
                beforeSend: function (res) {

                },
                success: function (res) {
                    $('#overlay').hide();
                    if (res == 'found') {
                        toastr.error("{!!Lang::get('posts::posts.repeated')!!}", "{!!Lang::get('posts::posts.error')!!}");
                    } else {
                        toastr.success("{!!Lang::get('posts::posts.moved')!!}", "{!!Lang::get('posts::posts.well_done')!!}");
                    }
                },
                complete: function () {
                    $('#save_load').remove();
                    $(".move").show();
                },
                error: function () {
                    $('#overlay').hide();
                    toastr.error("{!!Lang::get('posts::posts.not_moved')!!}", "{!!Lang::get('posts::posts.error')!!}");
                }
            });
        });

        $(".move").click(function (e) {
            e.preventDefault();
            var id = $(this).attr("href").substr(1);
            $("#post_temp").val(id);
            $("#move").click();
        });

        // trash page action
        $('.delete_agency').on('click', function (e) {
            e.preventDefault();
            $this = $(this);
            bootbox.dialog({
                message: $this.attr('message'),
                title: $this.attr('title'),
                buttons: {
                    success: {
                        label: "{!!Lang::get('pages::pages.ok')!!}",
                        className: "btn-success",
                        callback: function () {
                            location.href = $this.attr('href');
                        }
                    },
                    danger: {
                        label: "{!!Lang::get('pages::pages.cancel')!!}",
                        className: "btn-primary",
                        callback: function () {
                        }
                    },
                },
                className: "bootbox-sm"
            });
        });
        // search submit
        $("#search").submit(function (e) {
            if ($("#q").val() == '') {
                return false
            }
        });
    });
</script>


@stop
