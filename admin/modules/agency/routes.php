<?php

Route::group(array(
    "prefix" => ADMIN,
    "before" => "auth",
        ), function($route) {
    Route::resource('agency', 'AgenciesController');
    $route->group(array("prefix" => "agency"), function($route) {
        $route->post('savemove', 'AgenciesController@save_move');
        $route->get('download', 'AgenciesController@download');
        $route->post('delete', 'AgenciesController@destroy');
        $route->get('{id}/delete', 'AgenciesController@destroy');
    });
});

Route::get('agency/{slug}/download', "AgenciesController@download_agencies");
Route::get('agency/{slug}/read', "AgenciesController@read_agencies");
Route::get('agency/{slug}/delete', "AgenciesController@delete_agencies");
