<?php

return [
    'home' => 'Home',
    'agency_posts' => 'Agency Posts',
    'search' => 'Search...',
    'all' => 'All',
    'all_posts' => 'All News',
    'bulk' => 'Bulk Actions',
    'delete' => 'Delete',
    'delete_post_q' => 'Are you sure to delete post?',
    'apply' => 'Apply',
    'per_page' => 'Per Page',
    'title' => 'Title',
    'created_on' => 'Post Date',
    'no_posts' => 'No posts found!',
    'showing' => 'Showing',
    'to' => 'to',
    'of' => 'of',
    'posts' => 'posts',
    'move_post' => 'Move Post',
    'move' => 'Move',
    'moved' => 'the post is moved',
    'close' => 'close!',
    'save_changes' => 'Save Changes',
    'well_done' => 'Well done!',
    'image' => 'Image',
    'download_images' => 'Download Images',
    'view' => 'View',
    'edit' => 'Edit',
    
    "module" => "Agency",    
    "permissions" => [
        "delete" => "Delete posts",
        "move" => "Move posts"
    ]
];
