<?php

return [
    'home' => 'الرئيسية',
    'agency_posts' => 'أخبار الوكالات',
    'search' => 'بحث...',
    'all' => 'الكل',
    'all_posts' => 'كل الأخبار',
    'bulk' => 'تنفيذ الأمر',
    'delete' => 'حذف',
    'delete_post_q' => 'هل تريد حذف الخبر بشكل دائم؟',
    'apply' => 'تنفيذ',
    'per_page' => 'لكل صفحة',
    'title' => 'العنوان',
    'created_on' => 'تاريخ الخبر',
    'no_posts' => 'لا يوجد أخبار!',
    'showing' => 'عرض',
    'to' => 'الى',
    'of' => 'من',
    'posts' => 'خبر',
    'move_post' => 'نقل خبر',
    'move' => 'نقل',
    'moved' => 'الخبر تم نقله بنجاح',
    'close' => 'اغلاق',
    'save_changes' => 'حفظ التغييرات',
    'well_done' => 'تم!',
    'image' => 'الصورة',
    'download_images' => 'تحميل الصور',
    'view' => 'عرض',
    'edit' => 'تعديل',
    
    "module" => "الوكالات",    
    "permissions" => [
        "delete" => "حذف الأخبار",
        "move" => "نقل الأخبار"
    ]
];
