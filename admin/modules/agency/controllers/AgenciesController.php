<?php

class AgenciesController extends BackendController {

    public $conn = '';

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function __construct() {
        date_default_timezone_set("Etc/GMT-2");

        $this->conn = Session::get('conn');
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index() {

        $lang = App::getLocale();
        $q = trim(Input::get('q'));
        $cat_slug = Input::get('cat_id');
        $agency_slug = Input::get('agency_id');
        $per_page = (Input::has('per_page')) ? Input::get('per_page') : 20;

        $this->data['posts'] = AgencyPost::filter(['q' => $q, 'cat_slug' => $cat_slug, 'agency_slug' => $agency_slug])
                        ->orderBy('post_created_date', 'DESC')->paginate($per_page);

        return View::make('agency::agency')->with($this->data);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function read_agencies($slug) {
        switch ($slug) {
            case 'mena':
                $this->readMENA();

                break;
            default :
                read_agencies($slug);
                break;
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function delete_agencies($slug) {
        switch ($slug) {
            default :
                delete_agencies($slug);
                break;
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function download_agencies($slug) {
        switch ($slug) {
            default :
                download_agencies($slug);
                break;
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create() {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id) {

        $this->data['post'] = AgencyPost::find($id);
        return View::make('agency::show_post')->with($this->data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id = 0) {
        $checks = Input::get('check');
        if ($id) {
            AgencyPost::destroy($id);
        } else {
            AgencyPost::destroy($checks);
        }
        return Redirect::back();
    }

    // public function readMENA() {
    //     $agency = Agency::whereAgencySlug('mena')->get();
    //     $connctionID = ftp_connect("67.225.219.145");
    //     $login_result = ftp_login($connctionID, 'Administrator', 'QzMxt1294*/');
    //     ftp_pasv($connctionID, true);
    //     // get contents of the current directory
    //     $dirs = ftp_nlist($connctionID, "/AFP/arabic/journal");
    // }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function download() {
        error_reporting(E_ALL);
        ini_set('display_errors', '1');
        if (Input::has('images') && Input::get('images') != '') {
            $images = explode(',', Input::get('images'));
            array_pop($images);

            $images_check = false;

            $zipname = getcwd() . DIRECTORY_SEPARATOR . "blocks" . DIRECTORY_SEPARATOR . "images.zip";
            $uploads = getcwd() . DIRECTORY_SEPARATOR . "uploads";
            $zip = new \ZipArchive;
            $zip->open($zipname, \ZipArchive::CREATE);
            $old_images = array();
            foreach ($images as $key => $value) {
                $url = AMAZON_URL;
                if (strstr($value, "/")) {   // aws file
                    $dir_parts = explode("/", $value);
                    $image_name = end($dir_parts);
                    $url .= $value;
                } else {
                    $image_name = $value;
                    $url .= "uploads/uploads/" . $value;
                }

                $handle = @fopen($url, "r");
                if ($handle) {
                    $images_check = true;
                    file_put_contents($uploads . "/" . $image_name, file_get_contents($url));
                    $zip->addFile(uploads_path($image_name), $image_name);
                    $old_images[] = $uploads . "/" . $image_name;
                }
            }

            $zip->close();

            if (count($old_images)) {
                foreach ($old_images as $key => $value) {
                    unlink($value);
                }
            }

            if ($images_check) {
                header($_SERVER['SERVER_PROTOCOL'] . ' 200 OK');
                header("Content-Type: application/zip");
                header("Content-Transfer-Encoding: Binary");
                header("Content-Length: " . filesize($zipname));
                header("Content-Disposition: attachment; filename=\"" . basename($zipname) . "\"");
                readfile($zipname);
                unlink($zipname);
            } else {
                //unlink($zipname); 
                return Redirect::back();
            }
        }

        exit;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function save_move() {

        $cat_id = Input::get('cats');
        $id = Input::get('post_id');
        $lang = App::getLocale();

        $post = AgencyPost::find($id);

        $data = array();

        $data['post_title'] = $data['post_slug'] = $post->post_title;
        // check if title found before
        $count = Post::where('post_title', '=', $post->post_title)->count();
        if ($count) {
            echo json_encode('found');
            exit;
        }

        $data['post_created_date'] = date("Y-m-d H:i:s", strtotime($post->post_created_date));
        $data['post_type'] = 'post';
        $data['post_status'] = 0;
        $data['post_desked'] = 0;
        $data['post_submitted'] = 0;
        $data['post_closed'] = 0;
        $data['translated'] = 0;
        $data['post_format'] = 1;
        $data['seen'] = 0;
        $data['post_content'] = $post->post_content;
        $data['post_excerpt'] = $post->post_title;
        $data['facebook_title'] = $data['seo_title'] = string_sanitize(mb_substr($post->post_title, 0, 70));
        $data['facebook_description'] = $data['seo_description'] = $data['twitter_description'] = string_sanitize(strip_tags(mb_substr($post->post_content, 0, 160)));
        $data['post_image_id'] = $post->post_image_id;

        // insert post
        $newPost = new Post($data);
        $newPost->save();
        $id = $newPost->post_id;

        $newPost->categories()->attach($cat_id);

        echo json_encode($id);

        exit;
    }

    public function readMENA() {
        $catsArray = array(
            'صحة' => 'healh',
            'سياسة' => 'politics',
            'محافظات' => 'provinces',
            'علوم و تكنولوجيا' => 'science',
            'اقتصاد' => 'economy',
            'حوادث و قضايا' => 'accidents',
            'منوعات' => 'entertaiment',
            'فن و ثقافة' => 'culture',
            'عسكرى' => 'militery',
        );
        $agency = Agency::whereAgencySlug('mena')->first();
        $path = getcwd() . DIRECTORY_SEPARATOR . "agencies/MENA";
        $counter = 0;
        $files = File::files($path);
        $Ps = array();
        foreach ($files as $file) {
            $counter++;
            if ($counter == 20) {
                break;
            }

            $feed = file_get_contents($file); //321
            if (!self::isXML($feed)) {
                File::delete($file);
                continue;
            }
            $xml = new \SimpleXMLElement($feed, LIBXML_NOCDATA);
            $postAgency = new AgencyPost();
            $postAgency->post_created_date = (string) $xml->Date_Time;
            $postAgency->post_title = (string) $xml->Header;
            $postAgency->post_content = (string) $xml->Body;
            $postAgency->post_slug = create_slug($postAgency->post_title);
            $postAgency->agency_slug = 'mena';
            if (isset($catsArray[(string) $xml->Category_Name])) {
                $postAgency->cat_slug = $catsArray[(string) $xml->Category_Name];
            } else {
                $postAgency->cat_slug = create_slug((string) $xml->Category_Name);
            }
            File::delete($file);
            $postAgency->save();
            $Ps[] = $postAgency->post_id;
        }
        var_dump($Ps);
        dd($files);

        // $connctionID = ftp_connect("67.225.219.145");
        // $login_result = ftp_login($connctionID, 'Administrator', 'QzMxt1294*/');
        // ftp_pasv($connctionID, true);
        // // get contents of the current directory
        // $dirs = ftp_nlist($connctionID, $agency->agency_directory);
        // $counter = 0;
        // foreach ($dirs as $dir) {
        //     $counter++;
        //     if ($counter == 10) {
        //         break;
        //     }
        //     $localFile = $path . $dir;
        //     $transfered = ftp_get($connctionID, $localFile, $dir, get_ftp_mode($dir));
        //     if ($transfered) {
        //         $feed = file_get_contents($localFile); //321
        //         if(!self::isXML($feed)){
        //             continue;
        //         }
        //         $xml = new \SimpleXMLElement($feed, LIBXML_NOCDATA);
        //         $postAgency = new AgencyPost();
        //         $postAgency->post_created_date = (string) $xml->Date_Time;
        //         $postAgency->post_title = (string) $xml->Header;
        //         $postAgency->post_content = (string) $xml->Body;
        //         $postAgency->post_slug = create_slug($postAgency->post_title);
        //         if (isset($catsArray[(string) $xml->Category_Name])) {
        //             $postAgency->cat_slug = $catsArray[(string) $xml->Category_Name];
        //         } else {
        //             $postAgency->cat_slug = create_slug((string) $xml->Category_Name);
        //         }
        //         File::delete($localFile);
        //         ftp_delete($connctionID, $dir);
        //         $postAgency->save(); 
        //     }
        // }
    }

    public static function isXML($xml) {
        libxml_use_internal_errors(true);

        $doc = new \DOMDocument('1.0', 'utf-8');
        $doc->loadXML($xml);

        $errors = libxml_get_errors();

        if (empty($errors)) {
            return true;
        } else {
            return false;
        }
    }

}
