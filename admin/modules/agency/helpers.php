<?php

if (!function_exists("read_agencies")) {

    function read_agencies($agency) {

        $path = getcwd() . DIRECTORY_SEPARATOR . "agencies" . DIRECTORY_SEPARATOR;

        if ($agency == 'reuters') {
            $dirPath = $path . 'Reuters/';
        } else if ($agency == 'afp') {
            $dirPath = $path . 'AFP/arabic/journal/';
        } else if ($agency == 'efe') {
            $dirPath = $path . 'EFE/';
        }

        if ($agency == 'efe') {
            $agency_slug = 'efe';
            if ($handle1 = opendir($dirPath)) {
                while (false !== ($entry1 = readdir($handle1))) {

                    if ($entry1 != "." && $entry1 != ".." && $entry1 != "Video Sports" && $entry1 != "Video General") {
                        $cat_slug = create_slug($entry1);
                        $currPath = $dirPath . $entry1;
                        if ($handle = opendir($currPath)) {
                            while (false !== ($entry = readdir($handle))) {
                                $data = [];
                                $data['cat'] = $cat_slug;

                                if ($entry != "." && $entry != "..") {
                                    $path_parts = pathinfo($currPath . "/" . $entry);
                                    if ($path_parts['extension'] == 'XML' || $path_parts['extension'] == 'xml') {
                                        $feed = file_get_contents("$currPath/$entry");
                                        if ($feed) {
                                            $xml = new SimpleXmlElement($feed);
                                            //print_r($xml);
                                            $data['date'] = $xml->NewsEnvelope->DateAndTime;
                                            $data['title'] = $xml->NewsItem->NewsComponent->NewsLines->HeadLine;
                                            //$data['content'] = (array) $xml->NewsItem->NewsComponent->NewsComponent[1]->ContentItem->DataContent->nitf->body;
                                            //print_r($data['content'] = (array) $xml->NewsItem->NewsComponent->ContentItem->DataContent->nitf->body); die;

                                            if (property_exists($xml->NewsItem->NewsComponent, 'NewsComponent')) {
                                                $content = (array) $xml->NewsItem->NewsComponent->NewsComponent[1]->ContentItem->DataContent->nitf->body;
                                                foreach ($xml->NewsItem->NewsComponent->NewsComponent->ContentItem as $meta) {
                                                    $data['images'][] = trim($currPath . "/") . $meta['Href'];
                                                }
                                            } else {
                                                $content = (array) $xml->NewsItem->NewsComponent->ContentItem->DataContent->nitf->body;
                                            }

                                            /* foreach ($content as $meta) {
                                              $data['content'] = (array) $meta->p;
                                              //print_r($meta->p);
                                              } */
                                            foreach ($content as $meta) {
                                                $data['content'] = "<p style='white-space: pre-line'>" . (string) $meta->p . "</p>";
                                                //print_r($meta->p);
                                            }

                                            agency_posts($data, $agency_slug);
                                        }
                                    }
                                }
                            }
                        }
                        // delete file
                        if ($handle = opendir($dirPath . $entry1)) {

                            while (false !== ($entry = readdir($handle))) {

                                if ($entry != "." && $entry != "..") {
                                    if (file_exists($currPath . "/" . $entry)) {
                                        unlink($currPath . "/" . $entry);
                                    }
                                }
                            }
                        }
                        closedir($handle);
                    }
                }
            }
        } else if ($agency == 'afp') {
            $agency_slug = 'afp';
            if ($handle1 = opendir($dirPath)) {
                while (false !== ($entry1 = readdir($handle1))) {

                    if ($entry1 != "." && $entry1 != "..") {
                        $currPath = $dirPath . $entry1;
                        if ($handle = opendir($dirPath . $entry1)) {

                            while (false !== ($entry = readdir($handle))) {

                                if ($entry != "." && $entry != "..") {
                                    if ($entry == 'index.xml') {
                                        $feed = file_get_contents("$currPath/$entry");
                                        if ($feed) {
                                            $xml = new SimpleXmlElement($feed);

                                            foreach ($xml->NewsItem->NewsComponent->NewsComponent as $entry) {
                                                $data = [];
                                                $data['date'] = $xml->NewsEnvelope->DateAndTime;
                                                $data['cat'] = $entry1;

                                                $data['title'] = $entry->NewsLines->HeadLine;

                                                $meta = $entry->NewsItemRef['NewsItem'];
                                                if (file_exists("$currPath/$meta")) {
                                                    $feed = file_get_contents("$currPath/$meta");
                                                    $metaData = new SimpleXmlElement($feed);
                                                    $data['date'] = $metaData->NewsEnvelope->DateAndTime;
                                                    $data['content'] = $metaData->NewsItem->NewsComponent->NewsComponent[0]->ContentItem->DataContent->p;
                                                    $count = 0;
                                                    if (count($metaData->NewsItem->NewsComponent->NewsComponent) > 1) {

                                                        foreach ($metaData->NewsItem->NewsComponent->NewsComponent[1] as $meta) {

                                                            if ($count != 0 && $count != 1 && $count != 2) {
                                                                $data['images'][] = trim($currPath . "/") . $meta->ContentItem['Href'];
                                                            }
                                                            $count++;
                                                        }
                                                    }
                                                }
                                                agency_posts($data, $agency_slug);
                                            }
                                        }
                                    }
                                }
                            }

                            // delete file
                            if ($handle = opendir($dirPath . $entry1)) {

                                while (false !== ($entry = readdir($handle))) {

                                    if ($entry != "." && $entry != "..") {
                                        if (file_exists($currPath . "/" . $entry)) {
                                            unlink($currPath . "/" . $entry);
                                        }
                                    }
                                }
                            }
                            closedir($handle);
                        }
                    }
                }
            }
        } else {
            $agency_slug = 'reuters';
            if ($handle1 = opendir($dirPath)) {
                while (false !== ($entry1 = readdir($handle1))) {
                    if ($entry1 != "." && $entry1 != ".." && $entry1 == "Arabic World Service") {
                        $currPath = $dirPath . $entry1;
                        if ($handle = opendir($currPath)) {
                            while (false !== ($entry = readdir($handle))) {
                                $data = [];
                                //$data['cat'] = $cat_slug;

                                if ($entry != "." && $entry != "..") {
                                    $path_parts = pathinfo($currPath . "/" . $entry);
                                    if ($path_parts['extension'] == 'XML' || $path_parts['extension'] == 'xml') {
                                        $feed = file_get_contents("$currPath/$entry");
                                        if ($feed) {
                                            $xml = new SimpleXmlElement($feed);
                                            $slug = @$xml->itemSet->newsItem->itemMeta->memberOf->name;
                                            if ($slug == 'Entertainment and Lifestyle') {
                                                $data['cat'] = 'entertainment-news';
                                            } else if ($slug == 'Business') {
                                                $data['cat'] = 'business-news';
                                            } else if ($slug == 'Sports') {
                                                $data['cat'] = 'sport-news';
                                            } else if ($slug == 'Science' || $slug == 'Environment') {
                                                $data['cat'] = 'technology-news';
                                            } else {
                                                $data['cat'] = 'world-news';
                                            }
                                            $data['date'] = @$xml->itemSet->newsItem->itemMeta->firstCreated;
                                            $data['title'] = @$xml->itemSet->newsItem->contentMeta->headline;
                                            $content = @$xml->itemSet->newsItem->contentMeta->description;
                                            if (property_exists(@$xml->itemSet->newsItem->contentSet->inlineXML->html->body, 'p')) {
                                                foreach (@$xml->itemSet->newsItem->contentSet->inlineXML->html->body->p as $key => $value) {
                                                    $content .= "<p>" . $value . "</p>";
                                                }
                                            }
                                            $data['content'] = $content;

                                            agency_posts($data, $agency_slug);
                                        }
                                    }
                                }
                            }
                        }

                        // delete file
                        if ($handle = opendir($dirPath . $entry1)) {

                            while (false !== ($entry = readdir($handle))) {

                                if ($entry != "." && $entry != "..") {
                                    if (file_exists($currPath . "/" . $entry)) {
                                        unlink($currPath . "/" . $entry);
                                    }
                                }
                            }
                        }

                        closedir($handle);
                    } else if ($entry1 != "." && $entry1 != ".." && $entry1 == "Pictures") {

                        $currPath = $dirPath . $entry1;

                        if ($handle = opendir($currPath)) {
                            while (false !== ($entry = readdir($handle))) {
                                if ($entry != "." && $entry != "..") {
                                    $data = [];
                                    $data['cat'] = 'photo';

                                    $path_parts = pathinfo($currPath . "/" . $entry);
                                    if ($path_parts['extension'] == 'XML') {

                                        $feed = @file_get_contents("$currPath/$entry");
                                        if ($feed) {
                                            $xml = new SimpleXmlElement($feed);
                                            $data['date'] = $xml->header->sent;
                                            $data['title'] = $xml->itemSet->newsItem->contentMeta->headline;
                                            $data['content'] = $xml->itemSet->newsItem->contentMeta->description;

                                            $data['images'] = [];

                                            $image1 = str_replace(["_0_", "XML"], ["_1_", "JPG"], $entry);
                                            if (file_exists($currPath . "/" . $image1)) {
                                                $data['images'][] = $currPath . "/" . $image1;
                                            } else {
                                                $image1 = str_replace(["_0_", "XML"], ["_1_", "jpg"], $entry);
                                                if (file_exists($currPath . "/" . $image1)) {
                                                    $data['images'][] = $currPath . "/" . $image1;
                                                }
                                            }


                                            $image2 = str_replace(["_0_", "XML"], ["_2_", "JPG"], $entry);
                                            if (file_exists($currPath . "/" . $image2)) {
                                                $data['images'][] = $currPath . "/" . $image2;
                                            } else {
                                                $image2 = str_replace(["_0_", "XML"], ["_2_", "jpg"], $entry);
                                                if (file_exists($currPath . "/" . $image2)) {
                                                    $data['images'][] = $currPath . "/" . $image2;
                                                }
                                            }


                                            $image3 = str_replace(["_0_", "XML"], ["_3_", "JPG"], $entry);
                                            if (file_exists($currPath . "/" . $image3)) {
                                                $data['images'][] = $currPath . "/" . $image3;
                                            } else {
                                                $image3 = str_replace(["_0_", "XML"], ["_3_", "jpg"], $entry);
                                                if (file_exists($currPath . "/" . $image3)) {
                                                    $data['images'][] = $currPath . "/" . $image3;
                                                }
                                            }

                                            $image4 = str_replace(["_0_", "XML"], ["_4_", "JPG"], $entry);
                                            if (file_exists($currPath . "/" . $image4)) {
                                                $data['images'][] = $currPath . "/" . $image4;
                                            } else {
                                                $image4 = str_replace(["_0_", "XML"], ["_4_", "jpg"], $entry);
                                                if (file_exists($currPath . "/" . $image4)) {
                                                    $data['images'][] = $currPath . "/" . $image4;
                                                }
                                            }
                                            agency_posts($data, $agency_slug);
                                        }
                                    }
                                }
                            }

                            // delete file
                            if ($handle = opendir($dirPath . $entry1)) {

                                while (false !== ($entry = readdir($handle))) {

                                    if ($entry != "." && $entry != "..") {
                                        if (file_exists($currPath . "/" . $entry)) {
                                            unlink($currPath . "/" . $entry);
                                        }
                                    }
                                }
                            }

                            closedir($handle);
                        }
                    }
                }
            }
        }
    }

}


if (!function_exists("delete_agencies")) {

    function delete_agencies($agency) {
        $path = getcwd() . DIRECTORY_SEPARATOR . "agencies" . DIRECTORY_SEPARATOR;

        if ($agency == 'reuters') {
            $dirPath = $path . 'Reuters/';
        } else if ($agency == 'afp') {
            $dirPath = $path . 'AFP/arabic/journal/';
        } else if ($agency == 'efe') {
            $dirPath = $path . 'EFE/';
        }

        if ($agency == 'reuters') {
            if ($handle1 = opendir($dirPath)) {
                while (false !== ($entry1 = readdir($handle1))) {

                    if ($entry1 != "." && $entry1 != ".." && $entry1 != "Pictures") {

                        $currPath = $dirPath . $entry1;

                        if ($handle = opendir($currPath)) {

                            while (false !== ($entry = readdir($handle))) {

                                if ($entry != "." && $entry != "..") {
                                    if (file_exists($currPath . "/" . $entry)) {
                                        unlink($currPath . "/" . $entry);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

}

if (!function_exists("agency_posts")) {

    function agency_posts($posts, $agency_slug) {
        $title = "";
        $content = '';
        $date = "";
        $image_path = null;
        $image_id = null;
        $cat = "";
        if (isset($posts['cat'])) {
            $cat = $posts['cat'];
        }

        if (isset($posts['title'])) {
            $title = $posts['title'];
            $slug = create_slug($title);
        }

        if (isset($posts['content'])) {
            if (is_array($posts['content'])) {
                foreach ($posts['content'] as $text) {
                    $content .= "<p>" . $text . "</p>";
                }
            } else {
                $content .= $posts['content'];
            }
        }

        if (isset($posts['date'])) {
            $date = date("Y-m-d H:i:s", strtotime($posts['date']));
        }

        if (isset($posts['images'])) {
            $s3_url = 'https://dotemirates.s3-eu-west-1.amazonaws.com/';

            foreach ($posts['images'] as $ind => $image) {

                if (file_exists($image)) {
                    $imagePath = explode('/', $image);
                    $imagePath = $imagePath[count($imagePath) - 1];
//                    $extension = explode('.', $imagePath);
//                    $extension = $extension[count($extension) - 1];
                    $imagePath = time() * rand() . "." . image_type($image);
                    $uploads = getcwd() . DIRECTORY_SEPARATOR . "uploads" . DIRECTORY_SEPARATOR;
                    $media_hash = sha1_file($image);

                    $imgfound = Media::where('media_hash', $media_hash)->first();

                    if (!$imgfound) {
                        $s3_path = date('Y') . '/' . date('m') . '/' . $imagePath;
                        $media = new Media(['media_path' => $s3_path, 'media_hash' => $media_hash, 'media_type' => 'image', 'media_title' => $title]);
                        $media->save();
                        $image_id = $media->media_id;
                        if ($ind == 0) {
                            $image_path = $imagePath;
                            $image_id = $image_id;
                        }

                        $contentFile = file_get_contents($image);
                        $content .= "<p><img width='100%' src='" . $s3_url . $s3_path . "'></p>";
                        $fp = fopen("$uploads$imagePath", "w");
                        fwrite($fp, $contentFile);
                        fclose($fp);
                        s3_save($s3_path);

                        set_sizes($imagePath);
                        unlink("$uploads$imagePath");
                    } else {
                        $content .= "<p><img width='100%' src='" . $s3_url . $imgfound->media_path . "'></p>";
                        if ($ind == 0) {
                            $image_id = $imgfound->media_id;
                        }
                    }
                }
            }
        }

        if ($title) {
            $found = AgencyPost::where('post_slug', $slug)->first();
            if (!$found) {
                $post = new AgencyPost(['post_created_date' => $date,
                    'post_image_id' => $image_id, 'post_title' => $title,
                    'post_content' => $content, 'post_slug' => $slug, 'cat_slug' => $cat, 'agency_slug' => $agency_slug]);
                $post->save();
            } /* else {
              $post_content = $found->post_content;
              if (mb_strlen($post_content) < mb_strlen($content)) {
              $found->post_content = $content;
              $found->post_image_id = $image_id;
              $found->save();
              }
              } */
        }
    }

}

if (!function_exists("reuters_image")) {

    function reuters_image($dir, $image) {
        $arr = array("World News", "Top News", "Technology News", "Sport News", "Entertainment News", "Business News");
        foreach ($arr as $path) {
            if (file_exists($dir . "/$path/" . $image)) {
                return [$dir . "/$path/" . $image, create_slug($path)];
            }
        }
    }

}

if (!function_exists("download_agencies")) {

    function download_agencies($agency) {
        $path = getcwd() . DIRECTORY_SEPARATOR . "agencies" . DIRECTORY_SEPARATOR;

        if ($agency == 'reuters') {
            $dirPath = '/Reuters';
        } else if ($agency == 'afp') {
            $dirPath = '/AFP/arabic/journal';
        } else if ($agency == 'efe') {
            $dirPath = '/EFE';
        }

        // set up basic connection
        $conn_id = ftp_connect("67.225.219.145");

        // login with username and password
        $login_result = ftp_login($conn_id, 'Administrator', 'QzMxt1294*/');
        ftp_pasv($conn_id, true);

        // get contents of the current directory
        $dirs = ftp_nlist($conn_id, $dirPath);

        for ($j = 0; $j < count($dirs); $j++) {
            if ($dirs[$j] != "/EFE/Video Sports" && $dirs[$j] != "/EFE/Video General") {
                $contents = ftp_nlist($conn_id, $dirs[$j]);

                for ($i = 0; $i < count($contents); $i++) {
                    //download file
                    $file = $path . ltrim($contents[$i], '/');

                    if (file_exists($file)) {
                        unlink($file);
                    }

                    @ftp_get($conn_id, "$file", "$contents[$i]", get_ftp_mode($contents[$i]));
                }

                for ($i = 0; $i < count($contents); $i++) {
                    //delete file
                    ftp_delete($conn_id, $contents[$i]);
                }
            }
        }
    }

}

if (!function_exists("image_type")) {

    function image_type($image = '') {
        $type = exif_imagetype($image);
        switch ($type) {
            case 1:
                $type = 'gif';
                break;
            case 2:
                $type = 'jpg';
                break;
            case 3:
                $type = 'png';
                break;
            default :
                break;
        }
        return $type;
    }

}

if (!function_exists("get_ftp_mode")) {

    function get_ftp_mode($file) {
        $path_parts = pathinfo($file);

        if (!isset($path_parts['extension']))
            return FTP_BINARY;
        switch (strtolower($path_parts['extension'])) {
            case 'am':case 'asp':case 'bat':case 'c':case 'cfm':case 'cgi':case 'conf':
            case 'cpp':case 'css':case 'dhtml':case 'diz':case 'h':case 'hpp':case 'htm':
            case 'html':case 'in':case 'inc':case 'js':case 'm4':case 'mak':case 'nfs':
            case 'nsi':case 'pas':case 'patch':case 'php':case 'php3':case 'php4':case 'php5':
            case 'phtml':case 'pl':case 'po':case 'py':case 'qmail':case 'sh':case 'shtml':
            case 'sql':case 'tcl':case 'tpl':case 'txt':case 'vbs':case 'xml':case 'xrc':
                return FTP_ASCII;
        }
        return FTP_BINARY;
    }

}

if (!function_exists("dateIsBetween")) {

//dateIsBetween("1:00 am", "2:00 am", date('h:i a'));
    function dateIsBetween($from, $to, $date) {
        date_default_timezone_set("Etc/GMT-2");
        $date1 = DateTime::createFromFormat('H:i a', $date);
        $date2 = DateTime::createFromFormat('H:i a', $from);
        $date3 = DateTime::createFromFormat('H:i a', $to);
        if ($date1 > $date2 && $date1 < $date3) {
            return true;
        }
    }

}

if (!function_exists("set_sizes")) {

    function set_sizes($filename) {

        if (File::exists(UPLOADS . "/" . $filename)) {
            $sizes = Config::get("media.sizes");
            $width = \Image::make(UPLOADS . "/" . $filename)->width();
            $height = \Image::make(UPLOADS . "/" . $filename)->height();

            foreach ($sizes as $size => $dimensions) {

                \Image::make(UPLOADS . "/" . $filename)
                        ->fit($dimensions[0], $dimensions[1], function ($constraint) {
                            $constraint->upsize();
                        })
                        ->save(UPLOADS . "/" . $size . "-" . $filename);
                $s3_path = date('Y') . '/' . date('m') . '/' . $size . "-" . $filename;
                s3_save($s3_path);
                unlink(UPLOADS . "/" . $size . "-" . $filename);
            }
        }
    }

}
