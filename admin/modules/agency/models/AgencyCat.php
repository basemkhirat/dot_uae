<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AgencyPost
 *
 * @author Khaled PC
 */
class AgencyCat extends Model {

    protected $table = 'agencies_cats';
    protected $primaryKey = 'cat_id';
    public $timestamps = false;

    public function langs() {
        return $this->hasMany('CatLang', 'cat_id');
    }

    public function posts() {
        return $this->hasMany('AgencyPost', 'cat_slug');
    }

//put your code here
}
