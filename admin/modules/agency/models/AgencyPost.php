<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AgencyPost
 *
 * @author Khaled PC
 */
class AgencyPost extends Model {

    protected $table = 'agency_posts';
    protected $primaryKey = 'post_id';
    public $timestamps = false;
    protected $fillable = [
        'post_created_date',
        'post_image_id',
        'post_title',
        'post_content',
        'post_slug',
        'cat_slug',
        'agency_slug'];
    protected $guarded = ['post_id'];

    public function image() {
        return $this->hasOne('Media', 'media_id', 'post_image_id');
    }

    /**
     * Filter
     */
    public function scopeFilter($query, $args = []) {

        if (isset($args['cat_slug'])) {
            $query->where('cat_slug', '=', $args['cat_slug']);
        }

        if (isset($args['agency_slug'])) {
            $query->where('agency_slug', '=', $args['agency_slug']);
        }

        if (isset($args['q'])) {
            $query->where('post_title', 'LIKE', '%' . $args['q'] . '%');
        }

        return $query;
    }

    //put your code here
}
