<?php

class Agency extends Model {

    protected $table = 'agencies';
    protected $primaryKey = 'agency_id';
    public $timestamps = false;

    public function langs() {
        return $this->hasMany('AgencyLang', 'agency_id');
    }

    public function cats() {
        return $this->hasMany('AgencyCat', 'agency_id');
    }

}
