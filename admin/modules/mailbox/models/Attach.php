<?php

class Attach extends Model {

    protected $table = 'attaches';
    protected $fillable = [
        'name',
        'hash',
        'size',
        'type'
    ];
    public $timestamps = true;

    public function setUpdatedAt($value) {
        // Do nothing.
    }

    public function getUpdatedAtColumn() {
        return null;
    }

    public function mails() {
        return $this->belongsToMany('Mailbox', 'attach_mailbox', 'attach_id', 'mailbox_id');
    }

}
