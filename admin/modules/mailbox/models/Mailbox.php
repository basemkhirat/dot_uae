<?php

class Mailbox extends Model {

    protected $table = 'mailbox';
    protected $fillable = [
        'title',
        'to_id',
        'from_id',
        'draft_to',
        'message',
        'read',
        'status',
        'send_inc'
    ];
    public $timestamps = true;

    public function userTo() {
        return $this->hasOne('User', 'id', 'to_id');
    }

    public function userFrom() {
        return $this->hasOne('User', 'id', 'from_id');
    }

    public function attaches() {
        return $this->belongsToMany('Attach', 'attach_mailbox', 'mailbox_id', 'attach_id');
    }

    public function getTimeAgoAttribute() {
        return time_ago($this->created_at->toDateTimeString());
    }

    public function getTimeAgoShortAttribute() {
        return time_ago($this->created_at->toDateTimeString(), 1);
    }

    public function getSenderNameAttribute() {
        $name = '';
        if ($this->userFrom) {
            $name = $this->userFrom->name;
        }
        return $name;
    }

    public function getSenderImageAttribute() {
        if ($this->userFrom) {
            $image = $this->userFrom->getImageUrl('thumbnail');
        } else {
            $image = assets('images/author.png');
        }
        return $image;
    }

    public function getUrlAttribute() {
        return URL::to(ADMIN . '/mailbox/' . $this->id);
    }

    public function getReadClassAttribute() {
        return ($this->open) ? 'unread' : 'read';
    }

    public static function boot() {

        parent::boot();

        Mailbox::creating(function($mailbox) {
            
        });

        Mailbox::created(function($mailbox) {
            
        });

        Mailbox::deleting(function($mailbox) {
            // delete attaches if not linked in other mails
            foreach ($mailbox->attaches as $key => $attach) {
                $attachN = Attach::with('mails')->find($attach->id);
                if (count($attachN->mails) <= 1) {
                    $attachN->delete();
                }
            }

            $mailbox->attaches()->detach();
        });
    }

    public function scopefilter($query, $args = []) {

        if ($args['status'] && !$args['sender']) {
            $query->where('status', $args['status']);
        }

        if ($args['important']) {
            $query->where('important', 1)->where('status', 1);
        }

        if ($args['q']) {
            $query->where('title', 'LIKE', '%' . $args['q'] . '%')->orWhere('message', 'LIKE', '%' . $args['q'] . '%');
        }

        if ($args['sender'] || $args['status'] == 2) {
            $query->where('from_id', Auth::user()->id);
            if ($args['sender']) {
                $query->where('del_from', '!=', 1)->groupBy('send_inc');
            }
        } else {
            $query->where('del_to', '!=', 1)->where('to_id', Auth::user()->id);
        }

        return $query;
    }

}
