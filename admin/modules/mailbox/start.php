<?php

Menu::set("sidebar", function($menu) {
    $inbox = Mailbox::where('status', 1)->where('open', 0)->where('to_id', Auth::user()->id)->count();
    $all_messages = Mailbox::where('status', 1)->where('to_id', Auth::user()->id)->count();

    $menu->item('site_options.mailbox', trans("admin::common.mailbox") . " " . $inbox . "/" . $all_messages, URL::to(ADMIN . '/mailbox'))
            ->order(5)
            ->icon("fa-envelope");

    $menu->item('site_options.mailbox.inbox', trans("admin::common.inbox"), URL::to(ADMIN . '/mailbox'));
    $menu->item('site_options.mailbox.compose', trans("admin::common.compose_mail"), URL::to(ADMIN . '/mailbox/create'));
});


Menu::set("topnav", function($menu) {
    if (Auth::check()) {
        $data['inboxT'] = Mailbox::where('status', 1)->where('open', 0)->where('to_id', Auth::user()->id)->orderBy('created_at', 'desc')->take(3)->get();
        $data['inboxC'] = Mailbox::where('status', 1)->where('open', 0)->where('to_id', Auth::user()->id)->count();
        $menu->make("mailbox::dropmenu", $data);
    }
});


View::composer('mailbox::*', function($view) {
    $userId = \Auth::user()->id;
    $this->data['inbox'] = \Mailbox::where('status', 1)->where('open', 0)->where('to_id', $userId)->where('del_to', '!=', 1)->count();
    //$this->data['sent'] = \Mailbox::where('from_id', $userId)->count();
    $this->data['important'] = \Mailbox::where('status', 1)->where('important', 1)/* ->where('open', 0) */->where('to_id', $userId)->where('del_to', '!=', 1)->count();
    $this->data['draft'] = \Mailbox::where('status', 2)->where('from_id', $userId)->count();
    $this->data['trash'] = \Mailbox::where('status', 3)->where('open', 0)->where('to_id', $userId)->where('del_to', '!=', 1)->count();
    $view->with($this->data);
});

include __DIR__ . "/routes.php";
include __DIR__ . "/helpers.php";
