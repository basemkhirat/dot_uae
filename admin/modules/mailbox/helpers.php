<?php

if (!function_exists("bytesToSize")) {

    function bytesToSize($bytes, $decimals = 2) {
        $size = array(Lang::get('mailbox::mailbox.byte'), Lang::get('mailbox::mailbox.kilobyte'), Lang::get('mailbox::mailbox.megabyte'), Lang::get('mailbox::mailbox.gigabyte'), Lang::get('mailbox::mailbox.terabyte'));
        $factor = floor((strlen($bytes) - 1) / 3);
        return sprintf("%.{$decimals}f", $bytes / pow(1024, $factor)) . @$size[$factor];
    }

}
