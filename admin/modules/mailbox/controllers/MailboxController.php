<?php

//

class MailboxController extends BackendController {

    public $data = array();
    public $conn = '';

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function __construct() {
        date_default_timezone_set("Etc/GMT-2");

        $this->conn = Session::get('conn');
    }

    public function index() {
        $status = (Input::get('status')) ? Input::get('status') : 1;
        $sender = Input::get('sender');
        $important = Input::get('important');
        $q = trim(Input::get('q'));
        $this->data['messages'] = Mailbox::with('userFrom')->filter(['status' => $status, 'sender' => $sender, 'important' => $important, 'q' => $q])->orderBy('created_at', 'desc')->paginate(20);
        return View::make('mailbox::mailbox')->with($this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create() {
        if (Input::get('id')) {
//            $message = Mailbox::with('attaches')->find(Input::get('id'));
//            $this->data['title'] = $forward->title;
            $this->data['message'] = Mailbox::with('attaches')->find(Input::get('id'));
        } else {
            $this->data['message'] = new Mailbox();
        }
        return View::make('mailbox::compose')->with($this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store() {
        $status = Input::get('status');
        // validate the info, create rules for the inputs
        $rules = array(
            'usernames' => 'required',
            'title' => 'required',
            'message' => 'required',
        );

        // run the validation rules on the inputs from the form
        $validator = Validator::make(Input::all(), $rules);

        $validator->setCustomMessages(Lang::get('admin::validation'));

        // if the validator fails, redirect back to the form
        if ($validator->fails() && $status != 2) {
            return Redirect::back()
                            ->withErrors($validator) // send back all errors to the login form
                            ->withInput(); // send back the input (not the password) so that we can repopulate the form
        } else {
            $data = Input::all();
            $user_id = Auth::user()->id;
            $data['from_id'] = $user_id;
            $attaches = explode(',', Input::get('attaches'));
            if ($status == 1) {
//                $inc = Mailbox::where('from_id', $user_id)/*->where('status', '!=', 2)*/->groupBy('send_inc')->get();
//                $inc = count($inc) + 1;
                $inc = time();
                $usernames = explode(',', Input::get('usernames'));
                $data['draft_to'] = Input::get('usernames');
                $data['send_inc'] = $inc;
                foreach ($usernames as $key => $value) {
                    $user = User::where('username', $value)->first();
                    if ($user) {
                        $data['to_id'] = $user->id;
                        $mail = new Mailbox;
                        $mail->fill($data);
                        $mail->save();
                        $mail->attaches()->sync($attaches);
                    } else {
                        $group = Group::with('users')->where('name', $value)->first();
                        if (count($group->users)) {
                            foreach ($group->users as $key => $user) {
                                if ($user_id != $user->id) {
                                    $data['to_id'] = $user->id;
                                    $mail = new Mailbox;
                                    $mail->fill($data);
                                    $mail->save();
                                    $mail->attaches()->sync($attaches);
                                }
                            }
                        }
                    }
                }
            } else {
                if (Input::get('usernames') || Input::get('title') || Input::get('message')) {
                    $data['draft_to'] = Input::get('usernames');
                    $mail = new Mailbox;
                    $mail->fill($data);
                    $mail->save();
                    $mail->attaches()->sync($attaches);
                }
            }
        }

        return Redirect::to(ADMIN . '/mailbox');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id) {
        $message = Mailbox::with('userFrom', 'attaches')->find($id);
        if (Input::get('sender') != 'me') {
            $user_id = Auth::user()->id;
            if ($message->from_id != $user_id) {
                $message->open = 1;
                $message->save();
            }
        }

        $this->data['message'] = $message;
        return View::make('mailbox::message')->with($this->data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id) {
        $this->data['message'] = Mailbox::with('userFrom', 'attaches')->find($id);
        return View::make('mailbox::compose')->with($this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id) {

        $status = Input::get('status');
        // validate the info, create rules for the inputs
        $rules = array(
            'usernames' => 'required',
            'title' => 'required',
            'message' => 'required',
        );

        // run the validation rules on the inputs from the form
        $validator = Validator::make(Input::all(), $rules);

        $validator->setCustomMessages(Lang::get('admin::validation'));

        // if the validator fails, redirect back to the form
        if ($validator->fails() && $status != 2) {
            return Redirect::back()
                            ->withErrors($validator) // send back all errors to the login form
                            ->withInput(); // send back the input (not the password) so that we can repopulate the form
        } else {
            $data = Input::all();
            $user_id = Auth::user()->id;
            $data['from_id'] = $user_id;
            $attaches = explode(',', Input::get('attaches'));
            if ($status == 1) {
//                $incObj = Mailbox::where('from_id', $user_id)->where('status', '!=', 2)->groupBy('send_inc')->get();
//                $inc = count($incObj) + 1;
                $inc = time();
                $usernames = explode(',', Input::get('usernames'));
                $data['draft_to'] = Input::get('usernames');
                $data['send_inc'] = $inc;
                foreach ($usernames as $key => $value) {
                    $user = User::where('username', $value)->first();
                    if ($user) {
                        $data['to_id'] = $user->id;
                        $mail = new Mailbox;
                        $mail->fill($data);
                        $mail->save();
                        $mail->attaches()->sync($attaches);
                    } else {
                        $group = Group::with('users')->where('name', $value)->first();
                        if (count($group->users)) {
                            foreach ($group->users as $key => $user) {
                                if ($user_id != $user->id) {
                                    $data['to_id'] = $user->id;
                                    $mail = new Mailbox;
                                    $mail->fill($data);
                                    $mail->save();
                                    $mail->attaches()->sync($attaches);
                                }
                            }
                        }
                    }
                }
                Mailbox::destroy($id);
            } else {
                if (Input::get('usernames') || Input::get('title') || Input::get('message')) {
                    $data['draft_to'] = Input::get('usernames');
                    $mail = Mailbox::find($id);
                    $mail->fill($data);
                    $mail->save();
                    $mail->attaches()->sync($attaches);
                }
            }
        }

        return Redirect::to(ADMIN . '/mailbox');
    }

    /**
     * Trash the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function changeStatus($id = 0, $stat = 0) {

        $checks = Input::get('check');
        $stat = ($stat) ? $stat : trim(Input::get('action'));

        if ($id) {
            $checks = array($id);
        }

        if ($stat == 'delete') {
            if (Input::get('status') != 2) {
                $messages = Mailbox::whereIn('id', $checks)->get();
                foreach ($messages as $key => $message) {
                    if (Input::get('sender') == 'me') {
                        $senderMesg = Mailbox::where('send_inc', $message->send_inc)->get();
                        foreach ($senderMesg as $key => $senderM) {
                            if ($senderM->del_from || $senderM->del_to) {
                                $senderM->delete();
                            } else {
                                $senderM->del_from = 1;
                                $senderM->save();
                            }
                        }
                    } else {
                        if ($message->del_from || $message->del_to) {
                            $message->delete();
                        } else {
                            $message->del_to = 1;
                            $message->save();
                        }
                    }
                }
            } else {
                Mailbox::destroy($checks);
            }
        } else {
            if ($stat == 'notopen') {
                $value = 0;
                $stat = 'open';
            } elseif ($stat == 'notimportant') {
                $value = 0;
                $stat = 'important';
            } elseif ($stat == 'trash') {
                $value = 3;
                $stat = 'status';
            } elseif ($stat == 'restore') {
                $value = 1;
                $stat = 'status';
            } else {
                $value = 1;
            }
            $messages = Mailbox::whereIn('id', $checks)->get();
            foreach ($messages as $key => $message) {
                $message->$stat = $value;
                $message->save();
            }
        }

        if ($stat == 'trash') {
            return Redirect::to(ADMIN . '/mailbox?status=3');
        } else {
            return Redirect::back();
        }
    }

    /**
     * Trash the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function delete_attach() {
        $id = Input::get('id');

        $attach = Attach::with('mails')->find($id);
        if (!count($attach->mails)) {
            $attach->delete();
            echo json_encode('deleted');
        } else {
            echo json_encode('removed');
        }

        exit;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id) {
        Mailbox::destroy($id);
        return Redirect::to(ADMIN . '/mailbox?status=1');
    }

    /* Display a listing of the resource.
     *
     * @return Response
     */

    public function important() {
        $id = Input::get('id');
        $important = (Input::get('important')) ? 0 : 1;

        $message = Mailbox::find($id);
        $message->important = $important;
        $message->save();

        echo json_encode($important);
        exit;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    function search() {
        $user_id = Auth::user()->id;
        $q = urldecode(Input::get("term"));
        $data = [];

        $users = User::where("id", "!=", $user_id)
                ->where(function($query) use($q) {
                    $query->where("username", "LIKE", $q . "%")
                    ->orWhere("first_name", "LIKE", $q . "%");
                })
                ->take(10)
                ->get();

        $groups = Group::where("name", "LIKE", $q . "%")
                ->take(10)
                ->get();

        foreach ($users as $key => $user) {
            $data[] = array('username' => $user->username, 'first_name' => $user->first_name, 'last_name' => $user->last_name);
        }
        foreach ($groups as $key => $group) {
            $data[] = array('username' => $group->name, 'first_name' => $group->name, 'last_name' => '');
        }

        return json_encode($data);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function uploads() {
        $output = '';
        $allowed_types = explode(',', Config("media.allowed_file_types"));
        $allowed_size = Config("media.max_file_size");

        foreach (Input::file('files') as $key => $file) {
            $size = $file->getSize();
            $sizeS = bytesToSize($file->getSize());
            $filename = strtolower($file->getClientOriginalName());

            $parts = explode(".", $file->getClientOriginalName());
            $extension = end($parts);
            $filename = time() * rand() . "." . strtolower($extension);
            
            if (!in_array($extension, $allowed_types)) {
                $output .= '<li class="feed-element" style="margin-top:5px;background: #AD3939;color:#fff;direction: ltr;">
                                            <span class="m-l-xs">' . $filename . ' ( ' . Lang::get('mailbox::mailbox.notallowed') . ' )</span>
                                        </li>';
                continue;
            }
            if ($file->getSize() > ($allowed_size * 1024)) {
                $output .= '<li class="feed-element" style="margin-top:5px; background: #AD3939;color:#fff;direction: ltr;">
                                            <span class="m-l-xs">' . $filename . ' ( ' . Lang::get('mailbox::mailbox.maxsize') . ' )</span>
                                        </li>';
                continue;
            }

            $mime_parts = explode("/", $file->getMimeType());
            $mime_type = $mime_parts[0];

            //check hash file is exist
            $f = DB::table("attaches")
                    ->where("hash", sha1_file($file))
                    ->first();

            if (count($f)) {
                $output .= '<li class="feed-element" style="margin-top:5px;direction: ltr;">
                                            <span class="m-l-xs">' . $filename . ' ( ' . $sizeS . ' )</span>
                                            <a class="label label-danger pull-right delete-file" href="#" data-id="' . $f->id . '"><i class="fa fa-times"></i></a>
                                        </li>';
                continue;
            }

            try {

                $operation = $file->move(UPLOADS, $filename);

                // Moving to amazon s3
                s3_save('attach/' . date("Y/m/") . $filename);
            } catch (Exception $exception) {
                
            }


            if ($operation) {

                $attach = new Attach(array(
                    "name" => 'attach/' . date("Y") . "/" . date("m") . "/" . $filename,
                    "type" => $mime_type,
                    "hash" => sha1_file(UPLOADS . "/" . ($filename)),
                    "size" => $size
                ));
                $attach->save();

                $id = DB::getPdo()->lastInsertId();

                // delete file from local server
                if (Config::get("media.s3.status") and Config::get("media.s3.delete_locally")) {
                    $this->delete_hard($filename);
                }

                $filename = ($filename);

                $output .= '<li class="feed-element" style="margin-top:5px;direction: ltr;">
                                            <span class="m-l-xs">' . $filename . ' ( ' . $sizeS . ' )</span>
                                            <a class="label label-danger pull-right delete-file" href="#" data-id="' . $id . '"><i class="fa fa-times"></i></a>
                                        </li>';
                continue;
            } else {
                
            }
        }

        echo $output;
    }

    /**
     * Returns the original file name.
     *
     * It is extracted from the request from which the file has been uploaded.
     * Then it should not be considered as a safe value.
     *
     * @return string|null The original name
     *
     * @api
     */
    public function getClientOriginalName() {
        return $this->originalName;
    }

    function delete_hard($filename) {
        if (File::exists(UPLOADS . "/" . $filename)) {
            unlink(UPLOADS . "/" . $filename);
        }
    }

    //function to format bites bit.ly/19yoIPO
    public function bytesToSize($bytes, $decimals = 2) {
        $size = array(Lang::get('mailbox::mailbox.byte'), Lang::get('mailbox::mailbox.kilobyte'), Lang::get('mailbox::mailbox.megabyte'), Lang::get('mailbox::mailbox.gigabyte'), Lang::get('mailbox::mailbox.terabyte'));
        $factor = floor((strlen($bytes) - 1) / 3);
        return sprintf("%.{$decimals}f", $bytes / pow(1024, $factor)) . @$size[$factor];
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function download() {
        error_reporting(E_ALL);
        ini_set('display_errors', '1');
        if (Input::has('files') && Input::get('files') != '') {

            $tempfiles = explode(',', Input::get('files'));
            $files = [];
            foreach ($tempfiles as $key => $value) {
                $files[] = DB::table('attaches')->where('id', $value)->pluck('name');
            }

            $files_check = false;

            $zipname = getcwd() . DIRECTORY_SEPARATOR . "uploads" . DIRECTORY_SEPARATOR . "attachments.zip";
            $uploads = getcwd() . DIRECTORY_SEPARATOR . "uploads";
            $zip = new \ZipArchive;
            $zip->open($zipname, \ZipArchive::CREATE);
            $old_files = array();
            foreach ($files as $key => $value) {
                $url = AMAZON_URL;
                if (strstr($value, "/")) {   // aws file
                    $dir_parts = explode("/", $value);
                    $file_name = end($dir_parts);
                    $url .= $value;
                } else {
                    $file_name = $value;
                    $url .= "uploads/uploads/" . $value;
                }

                $handle = @fopen($url, "r");
                if ($handle) {
                    $files_check = true;
                    file_put_contents($uploads . "/" . $file_name, file_get_contents($url));
                    $zip->addFile(uploads_path($file_name), $file_name);
                    $old_files[] = $uploads . "/" . $file_name;
                }
            }

            $zip->close();

            if (count($old_files)) {
                foreach ($old_files as $key => $value) {
                    unlink($value);
                }
            }

            if (!is_file($zipname)) {
                header($_SERVER['SERVER_PROTOCOL'] . ' 404 Not Found');
                echo 'File not found';
            } else if (!is_readable($zipname)) {
                header($_SERVER['SERVER_PROTOCOL'] . ' 403 Forbidden');
                echo 'File not readable';
            } else {
                $basename = basename($zipname);
                $length = filesize($zipname);

                // http headers for zip downloads
                header("Pragma: public");
                header("Expires: 0");
                header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
                header("Cache-Control: public");
                header("Content-Description: File Transfer");
                header('Content-Type: application/zip');
                header("Content-Disposition: attachment; filename=\"" . $basename . "\"");
                header("Content-Transfer-Encoding: binary");
                header("Content-Length: " . $length);
                ob_end_flush();
                @readfile($zipname);
                unlink($zipname);
                exit;
            }
        }
    }

}
