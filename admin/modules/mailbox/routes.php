<?php

Route::group(array(
    "prefix" => ADMIN,
    "before" => "auth",
        ), function($route) {
    $route->group(array("prefix" => "mailbox"), function($route) {
        $route->post('important', 'MailboxController@important');
        $route->post('search', 'MailboxController@search');
        $route->post('uploads', 'MailboxController@uploads');
        $route->get('download', 'MailboxController@download');
        $route->get('{id}/{stat}', 'MailboxController@changeStatus');
        $route->post('status', 'MailboxController@changeStatus');
        $route->post('deleteattach', 'MailboxController@delete_attach');
        //$route->get('{id}/delete', 'MailboxController@destroy');
    });
    Route::resource('mailbox', 'MailboxController');
});
