@extends("admin::layouts.master")


@section("content")

<div id="content-wrapper">
    <div class="row">
        <div class="col-lg-3">
            @include("mailbox::partials.mailboxside")
        </div>
        <div class="col-lg-9 animated fadeInRight">
            <div class="mail-box-header">
                <div class="pull-right tooltip-demo">
                    @if(Input::get('sender') != 'me')
                    <a href="{!! URL::to('/'.ADMIN. '/mailbox/create?reply=' . $message->userFrom->username . '&title=' . $message->title) !!}" class="btn btn-white btn-sm" data-toggle="tooltip" data-placement="top" title="{!!Lang::get('mailbox::mailbox.reply')!!}"><i class="fa fa-reply"></i> {!!Lang::get('mailbox::mailbox.reply')!!}</a>
                    @endif
                    <a style="padding: 8px;" href="#" class="btn btn-white btn-sm" data-toggle="tooltip" data-placement="top" title="{!!Lang::get('mailbox::mailbox.print_email')!!}"><i class="fa fa-print"></i> </a>
                    @if($message->status == 3 || Input::get('sender') == 'me')
                    <a style="padding: 8px;" href="{!!URL::to('/'.ADMIN.'/mailbox/'.$message->id.'/delete')!!}@if(Input::get('sender') == 'me')?sender=me @endif" class="btn btn-white btn-sm" data-toggle="tooltip" data-placement="top" title="{!!Lang::get('mailbox::mailbox.delete')!!}"><i class="fa fa-trash"></i> </a>
                    @else
                    <a style="padding: 8px;" href="{!!URL::to('/'.ADMIN.'/mailbox/status/'.$message->id.'/trash')!!}" class="btn btn-white btn-sm" data-toggle="tooltip" data-placement="top" title="{!!Lang::get('mailbox::mailbox.move_to_trash')!!}"><i class="fa fa-trash-o"></i> </a>
                    @endif

                </div>
                <h2>
                    {!!Lang::get('mailbox::mailbox.view_message')!!}
                </h2>
                <div class="well mail-tools tooltip-demo m-t-md" style="cursor: pointer;margin-bottom: 0;">
                    <h3>
                        <span class="font-noraml">{!!Lang::get('mailbox::mailbox.subject')!!}: </span>{!!$message->title!!}
                    </h3>
                    <h5>
                        <span class="pull-right font-noraml"><i class="fa fa-clock-o"></i> {!!date_view($message->created_at)!!}</span>
                        @if(Input::get('sender') == 'me')
                        <span class="font-noraml">{!!Lang::get('mailbox::mailbox.to')!!}: </span>{!!$message->draft_to!!}
                        @else
                        <span class="font-noraml">{!!Lang::get('mailbox::mailbox.from')!!}: </span>{!!$message->sender_name!!}
                        @endif
                    </h5>
                </div>
            </div>
            <div class="mail-box">


                <div class="mail-body">
                    <p>
                        {!!$message->message!!}
                    </p>
                </div>
                @if(count($message->attaches))
                <div class="mail-attachment">
                    <p>
                        <span><i class="fa fa-paperclip"></i> {!!count($message->attaches)!!} {!!Lang::get('mailbox::mailbox.attachments')!!} - </span>
                        <a href="#" id="download">{!!Lang::get('mailbox::mailbox.download_all')!!}</a>
                    </p>

                    <div class="attachment">

                        @foreach($message->attaches as $attach)
                        <div class="file-box" data-id="{!!$attach->id!!}">
                            <div class="file">
                                <a @if($attach->type == 'image') href="{!!AMAZON_URL.$attach->name!!}" title="" data-gallery="" @else href="#" @endif>
                                    <span class="corner"></span>
                                    @if($attach->type == 'image')
                                    <div class="image lightBoxGallery">    
                                        <img src="{!!AMAZON_URL.$attach->name!!}" alt="image" class="img-responsive">
                                    </div>
                                    @else
                                    <div class="icon">
                                        <i class="fa fa-file"></i>
                                    </div>
                                    @endif
                                    <div class="file-name" style="word-wrap: break-word;">
                                        <?php
                                        $path = explode("/", $attach->name);
                                        $name = end($path);
                                        ?>
                                        <div style="direction: ltr; text-align:@if(DIRECTION == 'rtl') right @else left @endif">{!!$name!!}</div>
                                        <small>{!!Lang::get('mailbox::mailbox.added')!!}: {!!date_view($attach->created_at)!!}</small>
                                    </div>
                                </a>
                            </div>
                        </div>
                        @endforeach

                        <div class="clearfix"></div>
                    </div>
                </div>
                @endif
                <div class="mail-body text-right tooltip-demo">
                    @if(Input::get('sender') != 'me')
                    <a class="btn btn-sm btn-white" href="{!! URL::to('/'.ADMIN. '/mailbox/create?reply=' . $message->userFrom->username . '&title=' . $message->title) !!}" data-toggle="tooltip" data-placement="top" title="{!!Lang::get('mailbox::mailbox.reply')!!}"><i class="fa fa-reply"></i> {!!Lang::get('mailbox::mailbox.reply')!!}</a>
                    @endif
                    <a class="btn btn-sm btn-white" href="{!!URL::to('/'.ADMIN.'/mailbox/create?id='.$message->id)!!}"  data-toggle="tooltip" data-placement="top" title="{!!Lang::get('mailbox::mailbox.forward')!!}"><i class="fa fa-arrow-right"></i> {!!Lang::get('mailbox::mailbox.forward')!!}</a>
                    <a title="" data-placement="top" data-toggle="tooltip" type="button" data-original-title="{!!Lang::get('mailbox::mailbox.print')!!}" class="btn btn-sm btn-white" ><i class="fa fa-print"></i> {!!Lang::get('mailbox::mailbox.print')!!}</a>
                    @if($message->status == 3 || Input::get('sender') == 'me')
                    <a style="padding: 8px;" href="{!!URL::to('/'.ADMIN.'/mailbox/'.$message->id.'/delete')!!}@if(Input::get('sender') == 'me')?sender=me @endif" class="btn btn-white btn-sm" data-toggle="tooltip" data-placement="top" title="{!!Lang::get('mailbox::mailbox.delete')!!}"><i class="fa fa-trash"></i> </a>
                    @else
                    <a style="padding: 8px;" href="{!!URL::to('/'.ADMIN.'/mailbox/status/'.$message->id.'/trash')!!}" class="btn btn-white btn-sm" data-toggle="tooltip" data-placement="top" title="{!!Lang::get('mailbox::mailbox.move_to_trash')!!}"><i class="fa fa-trash-o"></i> </a>
                    @endif
                </div>
                <div class="clearfix"></div>


            </div>
        </div>
    </div>
</div>
<link href="<?php echo assets() ?>/css/plugins/blueimp/css/blueimp-gallery.min.css" rel="stylesheet">
<script src="<?php echo assets() ?>/js/plugins/blueimp/jquery.blueimp-gallery.min.js"></script>

<script type = "text/javascript" >
    $(document).ready(function () {
        $("#download").click(function (e) {
            e.preventDefault();
            files = $('.file-box').map(function (index) {
                return $(this).attr('data-id');
            }).get().join();
            console.log(files);
            location.href = "{!!URL::to('/'.ADMIN.'/mailbox/download')!!}?files=" + files;
        });
    });
</script>
@stop
