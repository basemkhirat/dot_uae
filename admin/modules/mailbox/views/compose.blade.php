@extends("admin::layouts.master")

@section("content")

<div id="content-wrapper">
    <div class="row">
        <div class="col-lg-3">
            @include("mailbox::partials.mailboxside")
        </div>
        <div class="col-lg-9 animated fadeInRight">
            <div class="mail-box-header">
                <div class="pull-right tooltip-demo">
                    <a href="#" class="btn btn-white btn-sm draft" data-toggle="tooltip" data-placement="top" title="{!!Lang::get('mailbox::mailbox.move_to_draft')!!}"><i class="fa fa-pencil"></i> {!!Lang::get('mailbox::mailbox.draft')!!}</a>
                    <a href="{!! URL::to('/'.ADMIN. '/mailbox') !!}" class="btn btn-danger btn-sm" data-toggle="tooltip" data-placement="top" title="{!!Lang::get('mailbox::mailbox.discard_email')!!}"><i class="fa fa-times"></i> {!!Lang::get('mailbox::mailbox.discard')!!}</a>
                </div>
                <h2>
                    {!!Lang::get('mailbox::mailbox.compose_mail')!!}
                </h2>
            </div>


            <div class="mail-box">

                @if(isset($message->id))
                {!! Form::open(array('url' => ADMIN.'/mailbox/'.$message->id, 'method' => 'put', 'files' => true, 'class' => 'form-horizontal form-compose', 'id' => 'form', 'novalidate' => 'novalidate')) !!}
                @else
                {!! Form::open(array('url' => ADMIN.'/mailbox', 'files' => true, 'class' => 'form-horizontal form-compose', 'id' => 'form', 'novalidate' => 'novalidate')) !!}
                @endif
                <div class="mail-body">

                    <input type="hidden" name="status" id="status" value="1">
                    <input type="hidden" name="attaches" value="">
                    <div class="form-group"><label class="col-sm-2 control-label">{!!Lang::get('mailbox::mailbox.to')!!}:</label>
                        <?php
                        if (isset($message->id)) {
                            if(Input::get('id'))
                                $usernames = '';
                            else
                                $usernames = $message->draft_to;
                            $title = $message->title;
                            $messageStr = $message->message;
//                        } elseif (isset($fmessage)) {
//                            $usernames = '';
//                            $title = $title;
//                            $messageStr = $fmessage;
                        } else {
                            $usernames = Input::get('reply');
                            $title = Input::get('title');
                            $messageStr = '';
                        }
                        $usernamesArr = explode(',', $usernames);
                        ?>
                        <div class="col-sm-10">
                            <div style="position:relative">
                                <input type="hidden" name="usernames" id="usernames" value="{!!$usernames!!}">
                                <ul id="mynames" style="border: 1px solid #E5E6E7 !important; margin:0 "></ul>
                            </div>
                        </div>
                    </div>
                    <div class="form-group"><label class="col-sm-2 control-label">{!!Lang::get('mailbox::mailbox.subject')!!}:</label>

                        <div class="col-sm-10"><input type="text" class="form-control" value="{!!$title!!}" name="title" id="title" placeholder="{!!Lang::get('mailbox::mailbox.subject')!!}"></div>
                    </div>
                </div>

                <div class="h-200" style="padding: 0 20px;">

                    <div class="summernote">
                        {!! Form::textarea('message',  Input::old('message', $messageStr) , ['placeholder' => Lang::get('mailbox::mailbox.message'), 'class' => 'form-control', 'rows' => 4, 'id' => 'message']) !!}
                    </div>
                    <div class="clearfix"></div>
                </div>
                {!! Form::close() !!}
                <div class="mail-body text-right tooltip-demo">
                    <a href="#" class="btn btn-sm btn-primary" data-toggle="tooltip" data-placement="top" id="send" title="{!!Lang::get('mailbox::mailbox.send')!!}"><i class="fa fa-reply"></i> {!!Lang::get('mailbox::mailbox.send')!!}</a>
                    <a href="#" class="btn btn-white btn-sm draft" data-toggle="tooltip" data-placement="top" title="{!!Lang::get('mailbox::mailbox.move_to_draft')!!}"><i class="fa fa-pencil"></i> {!!Lang::get('mailbox::mailbox.draft')!!}</a>
                    <a href="{!! URL::to('/'.ADMIN. '/mailbox') !!}" class="btn btn-white btn-sm" data-toggle="tooltip" data-placement="top" title="{!!Lang::get('mailbox::mailbox.discard_email')!!}"><i class="fa fa-times"></i> {!!Lang::get('mailbox::mailbox.discard')!!}</a>
                    <form action="{!! URL::to(ADMIN.'/mailbox/uploads') !!}" style="display:inline-block" onSubmit="return false" method="post" enctype="multipart/form-data" id="MyUploadForm">
                        <div class="fileUpload" data-original-title="{!!Lang::get('mailbox::mailbox.attach_files')!!}" data-toggle="tooltip" data-placement="top" >
                            <a class="btn btn-white btn-sm action" href="#" style="padding:8px" id="attachment" title=""><i class="fa fa-paperclip"></i></a>
                            <input id="uploadBtn" type="file" class="upload" name="files" multiple/>
                        </div>
                    </form>
                    <div class="progress progress-striped active m-b-sm" id='progressbox' style="display:none;width: 100%;margin-top: 10px;">
                        <div style="" aria-valuemax="100" aria-valuemin="0" aria-valuenow="0" id='progressbar' role="progressbar" class="progress-bar">
                            <span id="statustxt" class="sr-only">35% Complete (success)</span>
                        </div>
                    </div>
                    <div id="output">
                        <ul class="todo-list small-list">
                            @foreach($message->attaches as $attach)
                                <li class="feed-element" style="margin-top:5px;direction: ltr;">
                                    <?php $path = explode("/", $attach->name); $name = end($path);?>
                                    <span class="m-l-xs">{!!$name!!} ( {!!bytesToSize($attach->size)!!} ) </span>
                                    <a class="label label-danger pull-right delete-file" href="#" data-id="{!!$attach->id!!}"><i class="fa fa-times"></i></a>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
                <div class="clearfix"></div>
                <style>
                    .cke_textarea_inline
                    {
                            padding: 10px;
                            height: 200px;
                            overflow: auto;

                            border: 1px solid #E5E6E7;
                            -webkit-appearance: textfield;
                    }
                    .fileUpload {
                        position: relative;
                        overflow: hidden;
                        top: 12px;
                        //margin: 10px;
                    }
                    .fileUpload input.upload {
                        position: absolute;
                        top: 0;
                        right: 0;
                        margin: 0;
                        padding: 0;
                        font-size: 20px;
                        cursor: pointer;
                        opacity: 0;
                        filter: alpha(opacity=0);
                    }
                </style>
            </div>

            @if(isset($message->id))
            {!! Form::open(array('url' => ADMIN.'/mailbox/'.$message->id, 'method' => 'put', 'files' => true, 'class' => 'form-horizontal form-compose', 'id' => 'myform', 'novalidate' => 'novalidate')) !!}
            @else
            {!! Form::open(array('url' => ADMIN.'/mailbox', 'files' => true, 'class' => 'form-horizontal form-compose', 'id' => 'myform', 'novalidate' => 'novalidate')) !!}
            @endif

            <input type="hidden" name="usernames" id="dusernames" value="">
            <input type="hidden" name="title" id="dtitle" value="">
            <input type="hidden" name="message" id="dmessage" value="">
            <input type="hidden" name="status" value="2">
            <input type="hidden" name="attaches" value="">
            {!! Form::close() !!}
        </div>
    </div>
</div>

@section("footer")
<script type="text/javascript" src="<?php echo assets("ckeditor/ckeditor.js"); ?>"></script>
<link href="<?php echo assets("tagit") ?>/jquery.tagit.css" rel="stylesheet" type="text/css">
<link href="<?php echo assets("tagit") ?>/tagit.ui-zendesk.css" rel="stylesheet" type="text/css">
<script src="<?php echo assets() ?>/js/plugins/validate/jquery.validate.min.js"></script>

<script src="<?php echo assets("tagit") ?>/tag-it.js"></script>

<script>
CKEDITOR.replace("message", {
    language: '<?php echo LANG; ?>'
});
// Turn off automatic editor creation first.
//CKEDITOR.disableAutoInline = true;
//CKEDITOR.inline( 'message' );
var baseURL = '{!! URL::to("/".ADMIN) !!}/';

var back = false;
$(window).on('beforeunload', function () {
    if (back == true)
        return 'Are you sure you want to leave?';
});
//$(window).unload(function () {
//    $.ajax({
//        type: 'POST',
//        async: false,
//        url: baseURL + "mailbox/cancelMail",
//        data: {
//            id: post_id
//        },
//        success: function (a) {
//            console.debug("Ajax call finished");
//        },
//    });
//});
        
$(document).ready(function () {
    $('#title').on('input', function () {
        back = true;
    });
    CKEDITOR.instances['message'].on('change', function (evt) {
        back = true;
    });
    
    var array = [];
    
    @foreach($usernamesArr as $username)
        array.push("{!!$username!!}");
    @endforeach
    
    ajaxcall = false;
    $("#mynames").tagit({
        singleField: true,
        singleFieldNode: $('#usernames'),
        allowSpaces: true,
        minLength: 2,
        placeholderText: "{!!Lang::get('mailbox::mailbox.to')!!}",
        removeConfirmation: true,
        tagSource: function (request, response) {
            $.ajax({
                url: baseURL + 'mailbox/search',
                data: {term: request.term},
                type: "POST",
                dataType: "json",
                success: function (data) {
                    ajaxcall = true;
                    response($.map(data, function (item) {
                        array.push(item.username);
                        return {
                            label: item.first_name + ' ' + item.last_name,
                            value: item.username
                        }
                    }));
                }
            });
        },
        beforeTagAdded: function (event, ui) {
            console.log(array)
            if (array.indexOf(ui.tagLabel) == - 1){
                return false;
            }
            if (ui.tagLabel == "not found"){
                return false;
            }
            
            if(ajaxcall)
                back = true;
        }
    });
    
    //$("#mynames").tagit("createTag", "4");   
    $("#send").click(function (e) {
        attach_files();
        e.preventDefault();
        $("#form").submit();
    });
    
    $("#uploadBtn").change(function (e) {
        e.preventDefault();
        $("#MyUploadForm").submit();
    });
    
    $(".draft").click(function (e) {
        attach_files();
        e.preventDefault();
        $("#dusernames").val($("#usernames").val());
        $("#dtitle").val($("#title").val());
        editor = CKEDITOR.instances['message'];
        $("#dmessage").val(editor.getData());
        back = false;
        $("#myform").submit();
    });
            
    $("#form").validate({
        ignore: [],
        rules: {
            usernames: {
                required: true
            },
            title: {
                required: true
            },
            message: {
                required: function () {
                    CKEDITOR.instances.message.updateElement();
                }
            }
        },
        submitHandler: function () {
            back = false;
            form.submit();
        }
//            messages;{
//                title: {
//                    required: 'Required'
//                }
//            }
    });
    
    var progressbox = $('#progressbox');
    var progressbar = $('#progressbar');
    var statustxt = $('#statustxt');
    var completed = '0%';
    $('#MyUploadForm').submit(function () {
        var data = new FormData();
        var file = document.getElementById('uploadBtn');
        for (var i = 0; i < file.files.length; i++) {
            data.append("files[]", file.files[i]);
	}
        console.log(data);
        $.ajax({
            url: baseURL + 'mailbox/uploads',
            data: data,
            cache: false,
            contentType: false,
            processData: false,
            type: 'POST',
            beforeSend: function (res) {
                //Progress bar
                progressbox.show(); //show progressbar
                progressbar.width(completed); //initial value 0% of progressbar
                statustxt.html(completed); //set status text
                statustxt.css('color', '#000'); //initial color of status text
            },
            xhr: function() {
                var myXhr = $.ajaxSettings.xhr();
                if (myXhr.upload) {
                    myXhr.upload.addEventListener('progress',function(ev) {
                        if (ev.lengthComputable) {
                            //Progress bar
                            var percentUploaded = Math.floor(ev.loaded * 100 / ev.total);
                            progressbar.width(percentUploaded + '%') //update progressbar percent complete
                            statustxt.html(percentUploaded + '%'); //update status text
                            if (percentUploaded > 50)
                            {
                                statustxt.css('color', '#fff'); //change status text to white after 50%
                            }
                            // update UI to reflect percentUploaded
                        } else {
                            console.info('Uploaded '+ev.loaded+' bytes');
                            // update UI to reflect bytes uploaded
                        }
                   }, false);
                }
                return myXhr;
            }
        }).done(function(response) {
            // do stuff
            //progressbox.hide();
            back == true;
            $("#output ul").append(response);
        }).fail(function() {
            // handle error
        });
        //$(this).ajaxSubmit(options);
        // return false to prevent standard browser submit and page navigation 
        return false;
    });
    
    $('.todo-list').on('click', '.delete-file', function (e) {
        $this = $(this);
        e.preventDefault();
        ajaxData = {
            id: $this.attr('data-id'),
        };
        console.log(ajaxData);
        $.ajax({
            type: "POST",
            dataType: 'json',
            url: '{!! URL::to("/".ADMIN."/mailbox/deleteattach") !!}',
            data: ajaxData,
            beforeSend: function (res) {
            },
            success: function (res) {
                $this.parent().remove();
            },
            complete: function () {

            }
        });
    });
        
    function attach_files(){
        arr = $('.feed-element .delete-file').map( function(index) {
            return $(this).attr('data-id');
        }).get().join();
        $('input[name=attaches]').val(arr);
    }
});
jQuery.extend(jQuery.validator.messages, {
    required: "{!!Lang::get('posts::posts.required')!!}",
});
</script>
@stop
@stop
