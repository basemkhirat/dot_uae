<div class="ibox float-e-margins">
    <div class="ibox-content mailbox-content">
        <div class="file-manager">
            <a class="btn btn-block btn-primary compose-mail" href="{!! URL::to(ADMIN.'/mailbox/create') !!}">{!!Lang::get('mailbox::mailbox.compose_mail')!!}</a>
            <div class="space-25"></div>
            <h5>{!!Lang::get('mailbox::mailbox.folders')!!}</h5>
            <ul class="folder-list m-b-md" style="padding: 0">
                <li><a href="{!! URL::to('/'.ADMIN. '/mailbox?status=1') !!}"> <i class="fa fa-inbox "></i> {!!Lang::get('mailbox::mailbox.inbox')!!} @if($inbox) <span class="label label-success pull-right">{!! $inbox !!}</span> @endif </a></li>
                <li><a href="{!! URL::to('/'.ADMIN. '/mailbox?sender=me') !!}"> <i class="fa fa-envelope-o"></i> {!!Lang::get('mailbox::mailbox.send_mail')!!}</a></li>
                <li><a href="{!! URL::to('/'.ADMIN. '/mailbox?important=1') !!}"> <i class="fa fa-certificate"></i> {!!Lang::get('mailbox::mailbox.important')!!} @if($important) <span class="label label-important pull-right">{!! $important !!}</span> @endif</a></li>
                <li><a href="{!! URL::to('/'.ADMIN. '/mailbox?status=2') !!}"> <i class="fa fa-file-text-o"></i> {!!Lang::get('mailbox::mailbox.drafts')!!} @if($draft) <span class="label label-warning pull-right">{!! $draft !!}</span> @endif</a></li>
                <li><a href="{!! URL::to('/'.ADMIN. '/mailbox?status=3') !!}"> <i class="fa fa-trash-o"></i> {!!Lang::get('mailbox::mailbox.trash')!!} @if($trash) <span class="label label-danger pull-right">{!! $trash !!}</span> @endif</a></li>
            </ul>
            <!--                    <h5>Categories</h5>
                                <ul class="category-list" style="padding: 0">
                                    <li><a href="#"> <i class="fa fa-circle text-navy"></i> Work </a></li>
                                    <li><a href="#"> <i class="fa fa-circle text-danger"></i> Documents</a></li>
                                    <li><a href="#"> <i class="fa fa-circle text-primary"></i> Social</a></li>
                                    <li><a href="#"> <i class="fa fa-circle text-info"></i> Advertising</a></li>
                                    <li><a href="#"> <i class="fa fa-circle text-warning"></i> Clients</a></li>
                                </ul>
            
                                <h5 class="tag-title">Labels</h5>
                                <ul class="tag-list" style="padding: 0">
                                    <li><a href=""><i class="fa fa-tag"></i> Family</a></li>
                                    <li><a href=""><i class="fa fa-tag"></i> Work</a></li>
                                    <li><a href=""><i class="fa fa-tag"></i> Home</a></li>
                                    <li><a href=""><i class="fa fa-tag"></i> Children</a></li>
                                    <li><a href=""><i class="fa fa-tag"></i> Holidays</a></li>
                                    <li><a href=""><i class="fa fa-tag"></i> Music</a></li>
                                    <li><a href=""><i class="fa fa-tag"></i> Photography</a></li>
                                    <li><a href=""><i class="fa fa-tag"></i> Film</a></li>
                                </ul>-->
            <div class="clearfix"></div>
        </div>
    </div>
</div>
