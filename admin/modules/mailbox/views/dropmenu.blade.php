<li class="dropdown">
    <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
        <i class="fa fa-envelope"></i>  <span class="label label-warning">{{ $inboxC ? $inboxC : ''}}</span>
    </a>
    <ul class="dropdown-menu dropdown-messages">

        <?php if (count($inboxT)) { ?>
            <?php foreach ($inboxT as $value) { ?>
                <li>
                    <div class="dropdown-messages-box">
                        <a href="{!!$value->url!!}" class="pull-left">
                            <img alt="image" class="img-circle" src="{!!$value->sender_image!!}">
                        </a>
                        <div class="media-body">
                            <small class="pull-right">{!!$value->time_ago_short!!}</small>
                            <strong>{!!$value->sender_name!!}</strong> {!!$value->title!!}. <br>
                            <small class="text-muted">@if(DIRECTION == 'rtl') {!!arabic_date($value->created_at)!!} @else {!! date('M d, Y', strtotime($value->created_at)) !!} @ {!! date('h:i a', strtotime($value->created_at)) !!} @endif</small>
                        </div>
                    </div>
                </li>
                <!--</li>-->
                <li class="divider" style="margin-top:13px"></li>
            <?php } ?>
            <li>
                <div class="text-center link-block">
                    <a href="{!!URL::to(ADMIN . '/mailbox/')!!}">
                        <i class="fa fa-envelope"></i> <strong><?php echo trans('mailbox::mailbox.read_all_message'); ?></strong>
                    </a>
                </div>
            </li>
        <?php } else { ?>
            <?php echo trans('mailbox::mailbox.no_message'); ?>
        <?php } ?>
    </ul>
</li>