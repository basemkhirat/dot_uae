@extends("admin::layouts.master")

@section("content")

<div id="content-wrapper">
    <div class="row">
        <div class="col-lg-3">
            @include("mailbox::partials.mailboxside")
        </div>
        <div class="col-lg-9 animated fadeInRight">
            <div class="mail-box-header">
                <div>
                    {!! Form::open(array('url' => ADMIN.'/mailbox', 'method' => 'get', 'id' => 'search', 'class' => 'pull-right', 'style' => 'width: 50%')) !!}
                    <div class="input-group">
                        <input type="hidden" value="{!!Input::get('status')!!}" name="status">
                        <input type="hidden" value="{!!Input::get('sender')!!}" name="sender">
                        <input type="hidden" value="{!!Input::get('important')!!}" name="important">
                        <input class="form-control input-sm" name="q" id="q" value="{!!(Input::get('q')) ? Input::get('q') : ''!!}" placeholder="{!!Lang::get('mailbox::mailbox.search_email')!!}" type="text">
                        <div class="input-group-btn">
                            <button type="submit" class="btn btn-sm btn-primary">
                                {!!Lang::get('mailbox::mailbox.search')!!}
                            </button>
                        </div>
                    </div>
                    {!! Form::close() !!}
                    <h2>
                        {!!Lang::get('mailbox::mailbox.messages')!!} ({!! $messages->total() !!})
                    </h2>
                </div>
                <div style="clear: both">
                    <div class="mail-tools tooltip-demo m-t-md">
                        <div class="btn-group pull-right">
                            {!!$messages->appends(Input::all())->setPath('')->render()!!}
                        </div>
<!--                        <button class="btn btn-white btn-sm" data-toggle="tooltip" data-placement="left" title="{!!Lang::get('mailbox::mailbox.refresh_inbox')!!}"><i class="fa fa-refresh"></i> {!!Lang::get('mailbox::mailbox.refresh')!!}</button>-->
                        @if(Input::get('status') != 2 && Input::get('sender') != 'me')
                        <button style="padding: 8px;" id="open" data-original-title="{!!Lang::get('mailbox::mailbox.mark_as_read')!!}" class="btn btn-white btn-sm action" data-toggle="tooltip" data-placement="top" title=""><i class="fa fa-eye"></i> </button>
                        <button style="padding: 8px;" id="notopen" data-original-title="{!!Lang::get('mailbox::mailbox.mark_as_unread')!!}" class="btn btn-white btn-sm action" data-toggle="tooltip" data-placement="top" title=""><i class="fa fa-eye-slash"></i> </button>
                        <button style="padding: 8px;" id="important" class="btn btn-white btn-sm action" data-toggle="tooltip" data-placement="top" title="{!!Lang::get('mailbox::mailbox.mark_as_important')!!}"><i class="fa fa-star" style="color: gold;"></i> </button>
                        <button style="padding: 8px;" id="notimportant" class="btn btn-white btn-sm action" data-toggle="tooltip" data-placement="top" title="{!!Lang::get('mailbox::mailbox.mark_as_notimportant')!!}"><i class="fa fa-star"></i> </button>
                        <button style="padding: 8px;" id="@if(Input::get('status') == 3) delete @else trash @endif" data-original-title="@if(Input::get('status') == 3) {!!Lang::get('mailbox::mailbox.delete')!!} @else {!!Lang::get('mailbox::mailbox.move_to_trash')!!} @endif" class="btn btn-white btn-sm action" data-toggle="tooltip" data-placement="top" title=""><i class="fa @if(Input::get('status') == 3) fa-trash @else fa-trash-o @endif"></i> </button>
                        @if(Input::get('status') == 3)<button style="padding: 8px;" id="restore" data-original-title="{!!Lang::get('mailbox::mailbox.restore')!!}" class="btn btn-white btn-sm action" data-toggle="tooltip" data-placement="top" title=""><i class="fa fa-share-square-o"></i> </button>@endif
                        @else
                        <button style="padding: 8px;" id="delete" data-original-title="{!!Lang::get('mailbox::mailbox.delete')!!}" class="btn btn-white btn-sm action" data-toggle="tooltip" data-placement="top" title="">{!!Lang::get('mailbox::mailbox.delete')!!}</button>
                        @endif
                    </div>
                </div>
            </div>

            {!! Form::open(array('url' => ADMIN.'/mailbox/status', 'id' => 'form')) !!}
            <input type="hidden" value="" name="action">
            @if(Input::get('sender') == 'me')
            <input type="hidden" value="me" name="sender">
            @endif
            @if(Input::get('status') == 2)
            <input type="hidden" value="2" name="status">
            @endif
            <div class="mail-box">

                <table class="table table-hover table-mail">
                    <tbody>
                        <?php $count = count($messages); ?>
                        @if($count)
                        @foreach($messages as $message)
                        <?php
                        if (Input::get('status') == 2) {
                            $url = URL::to('/' . ADMIN . '/mailbox/' . $message->id . '/edit');
                        } else {
                            $url = URL::to('/' . ADMIN . '/mailbox/' . $message->id);
                        }
                        ?>
                        <tr class="@if(Input::get('sender') || Input::get('status') == 2) unread @else {!!$message->read_class!!} @endif">
                            <td class="check-mail">
                                <input type="checkbox" name="check[]" value="{!!$message->id!!}" class="i-checks">
                            </td>
                            <td>
                                @if(Input::get('sender') == 'me')
                                <a style="padding: 8px;" href="{!!URL::to('/'.ADMIN.'/mailbox/'.$message->id.'/delete?sender=me')!!}" class="btn btn-white btn-sm" data-toggle="tooltip" data-placement="top" title="{!!Lang::get('mailbox::mailbox.delete')!!}"><i class="fa fa-trash"></i> </a>
                                @elseif(Input::get('status') == 3)
                                <i class="fa fa-trash-o"></i>
                                @elseif(Input::get('status') == 2)
                                <a href="{!! $url !!}"><span style="color: #718D8D;">{!!Lang::get('mailbox::mailbox.draft')!!}</span></a>
                                @else
                                <i class="fa fa-star to-important" data-id="{!!$message->id!!}" data-important="{!!$message->important!!}" style="@if($message->important) color: gold; @endif cursor: pointer"></i>
                                @endif
                            </td>
                            <td class="mail-ontact">
                                @if(Input::get('sender') || Input::get('status') == 2)
                                <a href="{!!$url!!}@if(Input::get('sender') == 'me')?sender=me @endif">{!!Lang::get('mailbox::mailbox.to')!!}: {!!$message->draft_to!!}</a>
                                @else
                                <a href="{!! $url !!}">{!!$message->sender_name!!}</a>
                                @endif
                            </td>
                            <td class="mail-subject"><a href="{!! $url !!}@if(Input::get('sender') == 'me')?sender=me @endif">{!!$message->title!!}</a></td>
                            <td class="">@if(count($message->attaches))<i class="fa fa-paperclip">@endif</i></td>
                            <td class="text-right mail-date">@if(DIRECTION == 'rtl') {!!arabic_date($message->created_at)!!} @else {!! date('M d, Y', strtotime($message->created_at)) !!} @ {!! date('h:i a', strtotime($message->created_at)) !!} @endif</td>
                        </tr>
                        @endforeach
                        @else
                        <tr class="odd gradeX" style="height:200px">
                            <td colspan="5" style="vertical-align:middle; text-align:center; font-weight:bold; font-size:22px">{!!Lang::get('mailbox::mailbox.no_messages')!!}</td>
                        </tr>
                        @endif
                    </tbody>
                </table>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
<!--<script type="text/javascript" src="<?php echo assets("timeago/jquery.timeago.js"); ?>"></script>
<script type="text/javascript" src="<?php echo assets("timeago/locales/jquery.timeago.ar.js"); ?>"></script>-->
<script>
    $(document).ready(function () {
        $('.i-checks').iCheck({
            checkboxClass: 'icheckbox_square-green',
            radioClass: 'iradio_square-green',
        });

        $('.action').on('click', function (e) {
            e.preventDefault();
            $('input[name=action]').val($(this).attr('id'));
            $('#form').submit();
        });

        $("#form").submit(function () {
            if ($("input[name='check[]']:checked").length == 0) {
                return false;
            }
        });

        $(".to-important").click(function () {
            $this = $(this);
            ajaxData = {
                id: $this.attr('data-id'),
                important: $this.attr('data-important'),
            };
            console.log(ajaxData);
            $.ajax({
                type: "POST",
                dataType: 'json',
                url: '{!! URL::to("/".ADMIN."/mailbox/important") !!}',
                data: ajaxData,
                beforeSend: function (res) {
                    if ($this.attr('data-important') == 1) {
                        $this.attr('data-important', res).css('color', '');
                    } else {
                        $this.attr('data-important', res).css('color', 'gold');
                    }
                },
                success: function (res) {
                    $this.attr('data-important', res);
                },
                complete: function () {

                }
            });

        });

        // search submit
        $("#search").submit(function (e) {
            if (!$("#q").val()) {
                return false
            }
        });
//        date = jQuery.timeago(new Date());
//        alert(date);

    });
</script>
@stop
