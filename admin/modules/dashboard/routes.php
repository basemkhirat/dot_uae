<?php

Route::group(array(
    "prefix" => ADMIN,
    "before" => "auth",
        ), function($route) {
    $route->any('/dashboard/{id?}', array("as" => "dashboard.show", "uses" => "DashboardController@index"));
    $route->any('/stats', "DashboardController@stats");
});
