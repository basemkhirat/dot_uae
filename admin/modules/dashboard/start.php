<?php

Menu::set("sidebar", function($menu) {

    
    $menu->item('home_dashboard', trans("dashboard::dashboard.dashboard"), URL::to(ADMIN . '/dashboard'))
            ->order(1)
            ->icon("fa-info-circle");
    
    $menu->item('dashboard', trans("admin::common.statistics"), "javascript:void(0)")
            ->order(17)
            ->icon("fa-info-circle");

    $menu->item('dashboard.general', trans("admin::common.general_statistics"), URL::to(ADMIN . '/dashboard'));

    if (User::can("categories.stats")) {
        $menu->item('dashboard.general', trans("admin::common.categories_statistics"), URL::to(ADMIN . '/stats'));
    }
});





include __DIR__ . "/routes.php";
