<?php

class DashboardController extends BackendController {

    public $data = array();
    public $conn = '';

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function __construct() {
        date_default_timezone_set("Etc/GMT-2");
        $this->conn = Session::get('conn');
    }

    public function index($cat_id = false) {

        $this->data["cat_id"] = $cat_id;

        $this->data["cats"] = $cats = DB::table("categories")
                        ->where("cat_parent", 0)->get();

        $ob = DB::table("posts");

        if ($cat_id) {
            $ob->where("posts_categories.cat_id", $cat_id);
            $ob->join("posts_categories", "posts.post_id", "=", "posts_categories.post_id");
        }

        $this->data["news_count"] = $ob->where("post_status", 1)
                        ->where("post_type", "post")->count();

        $ob = DB::table("posts");
        if ($cat_id) {
            $ob->where("posts_categories.cat_id", $cat_id);
            $ob->join("posts_categories", "posts.post_id", "=", "posts_categories.post_id");
        }
        $this->data["articles_count"] = $ob->where("post_status", 1)
                        ->where("post_type", "article")->count();

        $this->data["users_count"] = DB::table("users")->where("status", 1)->count();
        $this->data["galleries_count"] = DB::table("galleries")->count();
        $this->data["categories_count"] = DB::table("categories")->count();
        $this->data["tags_count"] = DB::table("tags")->count();

        $posts_charts = array();
        for ($i = 0; $i <= 8; $i++) {
            $day = date("Y-m-d", time() - $i * 24 * 60 * 60);

            $ob = DB::table("posts");
            if ($cat_id) {
                $ob->where("posts_categories.cat_id", $cat_id);
                $ob->join("posts_categories", "posts.post_id", "=", "posts_categories.post_id");
            }

            $posts_charts[$day] = $ob->where("post_status", 1)
                    ->where(DB::raw("date(post_created_date)"), $day)
                    ->count();
        }

        $this->data["posts_charts"] = array_reverse($posts_charts);

        $this->data["poll_question"] = $poll_question = DB::table("polls")
                        ->where('is_always_active', '=', 1)
                        ->join("polls_lang", "polls_lang.poll_id", "=", "polls.poll_id")
                        ->where("polls_lang.lang", "=", LANG)
                        ->orderBy("created_at", "DESC")
                        ->limit(1)->first();


        if (count($poll_question)) {
            $this->data["poll_answer"] = DB::table("polls")
                            ->where('poll_parent', '=', $poll_question->poll_id)
                            ->join("polls_lang", "polls_lang.poll_id", "=", "polls.poll_id")
                            ->where("polls_lang.lang", "=", LANG)
                            ->orderBy("total_votes", "DESC")
                            ->limit(1)->first();
        }

        $this->data["users"] = DB::table("users")->where("status", 1)
                        ->leftJoin("media", "media.media_id", "=", "users.photo")
                        ->join("roles", "roles.role_id", "=", "users.role_id")
                        ->orderBy("users.id", "DESC")
                        ->take(4)->get();

        $ob = DB::table("posts");
        if ($cat_id) {
            $ob->where("posts_categories.cat_id", $cat_id);
            $ob->join("posts_categories", "posts.post_id", "=", "posts_categories.post_id");
        }
        $this->data["pending_posts"] = $ob->join("users", "posts.post_user_id", "=", "users.id")
                ->where("posts.post_submitted", 1)
                ->where("posts.post_desked", 0)
                ->orderBy("posts.post_updated_date", "DESC")
                ->groupBy("posts.post_id")
                ->take(15)
                ->get();

        $ob = DB::table("posts");
        if ($cat_id) {
            $ob->where("posts_categories.cat_id", $cat_id);
            $ob->join("posts_categories", "posts.post_id", "=", "posts_categories.post_id");
        }
        $this->data["reviewed_posts"] = $ob->join("users", "posts.post_user_id", "=", "users.id")
                ->where("posts.post_desked", 1)
                ->orderBy("posts.post_updated_date", "DESC")
                ->groupBy("posts.post_id")
                ->take(15)
                ->get();
        /*
          $this->data["comments"] = DB::table("comments")
          ->join("posts", "posts.post_id", "=", "comments.comment_post_id")
          ->join("posts_langs", "posts_langs.post_id", "=", "posts.post_id")
          ->join("users", "comments.comment_user_id", "=","users.id")
          ->where("posts_langs.lang", LANG)
          ->take(15)
          ->get();
         */

        return View::make("dashboard::show", $this->data);
    }

    public function stats() {

        $start = date("Y-m-d H:i:s", strtotime(urldecode(Input::get("start"))));
        $end = date("Y-m-d H:i:s", strtotime(urldecode(Input::get("end"))));

        $cats = Category::with(['postViews', 'postStats'])->where('site', $this->conn)->orderBy('cat_name', 'desc');/*->whereHas('postViews', function($q) {
            $q->where('post_type', 'post');
        });*/

//        $ob1 = DB::table('posts')->leftJoin('posts_categories', 'posts_categories.post_id', '=', 'posts.post_id')
//                ->leftJoin('categories', 'posts_categories.cat_id', '=', 'categories.cat_id')
//                ->where('categories.site', $this->conn)
//                ->where('post_type', 'post')
//                //->where('cat_parent', 0)
//                ->select(DB::raw('sum(post_views) as total'), 'cat_name', 'categories.cat_id')
//                ->groupBy('posts_categories.cat_id')
//                ->orderBy('cat_name', 'desc');
//
//        $ob2 = DB::table('posts_stats')->leftJoin('posts_categories', 'posts_categories.post_id', '=', 'posts_stats.post_id')
//                ->leftJoin('posts', 'posts_stats.post_id', '=', 'posts.post_id')
//                ->leftJoin('categories', 'posts_categories.cat_id', '=', 'categories.cat_id')
//                ->where('categories.site', $this->conn)
//                ->where('post_type', 'post')
//                //->where('cat_parent', 0)
//                ->select(DB::raw('sum(facebook) as facebook'), DB::raw('sum(twitter) as twitter'), DB::raw('sum(youtube) as youtube'), 'cat_name', 'categories.cat_id')
//                ->groupBy('posts_categories.cat_id')
//                ->orderBy('cat_name', 'desc');

        $articlesViews = Post::where('site', $this->conn)->where('post_type', 'article');
        $articlesStats = PostStat::whereHas('posts', function($q) {
                    $q->where('site', $this->conn)->where('post_type', 'article');
                })->select(DB::raw('sum(facebook) as facebook'), DB::raw('sum(twitter) as twitter'), DB::raw('sum(youtube) as youtube'));

//        $ob3 = DB::table('posts')
//                ->where('posts.site', $this->conn)
//                ->where('post_type', 'article');
//
//
//        $ob4 = DB::table('posts_stats')
//                ->leftJoin('posts', 'posts_stats.post_id', '=', 'posts.post_id')
//                ->where('posts.site', $this->conn)
//                ->where('post_type', 'article')
//                ->select(DB::raw('sum(facebook) as facebook'), DB::raw('sum(twitter) as twitter'), DB::raw('sum(youtube) as youtube'));

        if (Input::has("start")) {
            $cats->whereHas('posts', function($q) use ($start) {
                $q->where("post_created_date", ">=", $start);
            });
            $articlesViews->where("post_created_date", ">=", $start);
            $articlesStats->whereHas('posts', function($q) use ($start) {
                $q->where("post_created_date", ">=", $start);
            });
            //$ob4->where("post_created_date", ">=", $start);
        }

        if (Input::has("end")) {
            $cats->whereHas('posts', function($q) use ($end) {
                $q->where("post_created_date", "<=", $end);
            });
            $articlesViews->where("post_created_date", "<=", $end);
            $articlesStats->whereHas('posts', function($q) use ($end) {
                $q->where("post_created_date", "<=", $end);
            });
            //$ob4->where("post_created_date", "<=", $end);
        }

        $obj1 = $cats->get();
        $obj2 = $articlesViews->sum('post_views');
        $obj3 = $articlesStats->first();
//        $article_stats = $ob4->first();

        $stats = array();
        //dd($obj3);
        foreach ($obj1 as $key => $value) {
            $total = (@$value->postViews->first()->total) ? @$value->postViews->first()->total : 0;
            $facebook = (@$value->postStats->first()->facebook) ? @$value->postStats->first()->facebook : 0;
            $twitter = (@$value->postStats->first()->twitter) ? @$value->postStats->first()->twitter : 0;
            $youtube = (@$value->postStats->first()->youtube) ? @$value->postStats->first()->youtube : 0;
            $stats[] = array('cat_name' => $value->cat_name, 'cat_id' => $value->cat_id, 'views' => $total, 'facebook' => $facebook
                , 'twitter' => $twitter, 'youtube' => $youtube);
        }

//        foreach ($cats_stats as $key => $value) {
//            $keyS = $this->search_array($value->cat_name, $stats);
//            if ($keyS || $keyS == '0') {
//                $stats[$keyS]['facebook'] = $value->facebook;
//                $stats[$keyS]['twitter'] = $value->twitter;
//                $stats[$keyS]['youtube'] = $value->youtube;
//            } else {
//                $stats[] = array('cat_name' => $value->cat_name, 'cat_id' => $value->cat_id, 'views' => 0, 'facebook' => $value->facebook, 'twitter' => $value->twitter, 'youtube' => $value->youtube);
//            }
//        }

        $stats[] = array('cat_name' => Lang::get('posts::posts.blog'), 'cat_id' => 'article', 'views' => $obj2, 'facebook' => ($obj3->facebook) ? $obj3->facebook : 0, 'twitter' => ($obj3->twitter) ? $obj3->twitter : 0,
            'youtube' => ($obj3->youtube) ? $obj3->youtube : 0);

        usort($stats, function($a, $b) {
            return $b['views'] - $a['views'];
        });

        $this->data['stats'] = $stats;

        return View::make("dashboard::stats", $this->data);
    }

    private function search_array($search, $arr) {

        foreach ($arr as $key => $value) {
            if ($search == $value['cat_name']) {
                return "$key";
            }
        }
        return false;
    }

}
