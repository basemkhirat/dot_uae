@extends("admin::layouts.master")
@section("breadcrumb")

<style>
    #category_filter_chosen {
        margin-top: 30px;
    }
</style>

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-8">
        <h2><?php echo trans("dashboard::dashboard.dashboard") ?></h2>
        <ol class="breadcrumb">
            <li>
                <a href="<?php echo URL::to(ADMIN . "/dashboard"); ?>"><?php echo trans("dashboard::dashboard.dashboard") ?></a>
            </li>
        </ol>
    </div>

    <div class="col-lg-4">
        <form action="" method="get" class="">
            <select class="form-control chosen-select chosen-rtl" name="cat_id" id="category_filter">
                <option value="0"><?php echo trans("dashboard::dashboard.all_categories") ?></option>
                <?php foreach ($cats as $cat) { ?>
                    <option <?php if ($cat->cat_id == $cat_id) { ?> selected="selected" <?php } ?> value="<?php echo $cat->cat_id; ?>"><?php echo $cat->cat_name; ?></option>
                <?php } ?>  

            </select>
        </form>
    </div>
</div>
@stop

@section("content")

<div class="row  border-bottom white-bg dashboard-header" style="margin:0">

    <div class="col-sm-3">
        <h2><?php echo trans("dashboard::dashboard.dotuae") ?></h2>
        <small><?php echo trans("dashboard::dashboard.welcome") ?></small>
        <ul class="list-group clear-list m-t">
            <li class="list-group-item fist-item">
                <span class="label label-primary pull-right">
                    <?php echo $news_count; ?>
                </span>
                <?php echo trans("dashboard::dashboard.news"); ?>
            </li>
            <li class="list-group-item">
                <span class="label label-primary pull-right">
                    <?php echo $articles_count; ?>
                </span>
                <?php echo trans("dashboard::dashboard.articles"); ?>
            </li>
            <li class="list-group-item">
                <span class="label label-primary pull-right">
                    <?php echo $galleries_count; ?>
                </span>
                <?php echo trans("dashboard::dashboard.galleries"); ?>
            </li>
            <li class="list-group-item">
                <span class="label label-primary pull-right">
                    <?php echo $users_count; ?>
                </span>
                <?php echo trans("dashboard::dashboard.users"); ?>
            </li>
            <li class="list-group-item">
                <span class="label label-primary pull-right">
                    <?php echo $categories_count; ?>
                </span>
                <?php echo trans("dashboard::dashboard.categories"); ?>
            </li>
            <li class="list-group-item">
                <span class="label label-primary pull-right">
                    <?php echo $tags_count; ?>
                </span>
                <?php echo trans("dashboard::dashboard.tags"); ?>
            </li>
        </ul>
    </div>
    <div class="col-sm-9">
        <div style="margin-top:33px">
            <canvas id="lineChart" height="114"></canvas>
        </div>
    </div>
</div>

<div class="row" style="margin-top:10px">

    <div class="col-md-4">

        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>
                    <i class="fa fa-newspaper-o"></i>
                    <?php echo trans("dashboard::dashboard.pending_posts"); ?></h5>
                <strong class="pull-right">
                    <?php if ($cat_id) { ?>
                        <a class="text-navy" href="<?php echo URL::to(ADMIN . "/posts?submitted=1&cat_id=" . $cat_id); ?>">
                        <?php } else { ?>
                            <a class="text-navy" href="<?php echo URL::to(ADMIN . "/posts?submitted=1"); ?>">
                            <?php } ?>
                            <?php echo trans("dashboard::dashboard.show_all"); ?>
                        </a>
                </strong>
            </div>
            <div class="ibox-content">
                <?php if (count($pending_posts)) { ?>
                    <table class="table table-hover no-margins">
                        <thead>
                            <tr>
                                <th><?php echo trans("dashboard::dashboard.title") ?></th>
                                <th><?php echo trans("dashboard::dashboard.date") ?></th>
                            </tr>
                        </thead>
                        <tbody>

                            <?php foreach ($pending_posts as $post) { ?>
                                <tr>
                                    <td><small><a href="<?php echo URL::to(ADMIN . "/posts/" . $post->post_id . "/edit") ?>" title="" class="ticket-title"><?php echo $post->post_title; ?></a></small></td>
                                    <td><i class="fa fa-clock-o"></i> <?php echo $post->post_created_date; ?> </td>
                                </tr>
                            <?php } ?>

                        </tbody>
                    </table>
                <?php } else { ?>
                    <p class="text-center"><?php echo trans("dashboard::dashboard.no_pending_posts") ?></p>
                <?php } ?>
            </div>
        </div>
    </div>

    <div class="col-md-4">

        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>
                    <i class="fa fa-newspaper-o"></i>
                    <?php echo trans("dashboard::dashboard.reviewed_posts"); ?></h5>
                <strong class="pull-right">
                    <?php if ($cat_id) { ?>
                        <a class="text-navy" href="<?php echo URL::to(ADMIN . "/posts?desked=1&cat_id=" . $cat_id); ?>">
                        <?php } else { ?>
                            <a class="text-navy" href="<?php echo URL::to(ADMIN . "/posts?desked=1"); ?>">
                            <?php } ?>
                            <?php echo trans("dashboard::dashboard.show_all"); ?>
                        </a>
                </strong>
            </div>
            <div class="ibox-content">
                <?php if (count($reviewed_posts)) { ?>
                    <table class="table table-hover no-margins">
                        <thead>
                            <tr>
                                <th><?php echo trans("dashboard::dashboard.title") ?></th>
                                <th><?php echo trans("dashboard::dashboard.date") ?></th>
                            </tr>
                        </thead>
                        <tbody>

                            <?php foreach ($reviewed_posts as $post) { ?>
                                <tr>
                                    <td><small><a href="<?php echo URL::to(ADMIN . "/posts/" . $post->post_id . "/edit") ?>" title="" class="ticket-title"><?php echo $post->post_title; ?></a></small></td>
                                    <td><i class="fa fa-clock-o"></i> <?php echo $post->post_created_date; ?> </td>
                                </tr>
                            <?php } ?>


                        </tbody>
                    </table>
                <?php } else { ?>
                    <p class="text-center"><?php echo trans("dashboard::dashboard.no_reviewed_posts") ?></p>
                <?php } ?>
            </div>
        </div>


    </div>

    <div class="col-md-4">

        <div class="ibox float-e-margins">
            <div class="ibox-title">

                <h5>
                    <i class="fa fa-tasks"></i>
                    <?php echo trans("dashboard::dashboard.tasks") ?>
                </h5>

                <strong class="pull-right">
                    <a class="text-navy" href="<?php echo URL::to(ADMIN . "/tasks"); ?>">
                        <?php echo trans("dashboard::dashboard.show_all"); ?>
                    </a>
                </strong>
            </div>
            <div class="ibox-content">

                <?php
                $tasks = Task::join("tasks_users", "tasks.id", "=", "tasks_users.task_id")
                                ->where("done", 0)
                                ->where("status", 1)
                                ->where("tasks_users.user_id", Auth::user()->id)->get();
                ?>

                <?php if (count($tasks)) { ?>
                    <ul class="todo-list m-t small-list">
                        <?php foreach ($tasks as $task) { ?>
                            <li>
                                <a href="#" class="check-link" data-task-id="<?php echo $task->id ?>">
                                    <?php if ($task->done) { ?>
                                        <i class="fa fa-check-square-o text-navy"></i>
                                    <?php } else { ?>
                                        <i class="fa fa-square-o text-navy"></i>
                                    <?php } ?>
                                </a>
                                <span class="m-l-xs <?php if ($task->done) { ?>todo-completed<?php } ?>"><?php echo $task->title; ?></span>
                            </li>
                        <?php } ?>
                        <script>
                            $(document).ready(function () {
                                $(".check-link").click(function () {
                                    var base = $(this);
                                    var task_id = base.attr("data-task-id");

                                    if (base.children('i').hasClass("fa-square-o")) {
                                        var done = 1;
                                    } else {
                                        var done = 0;
                                    }

                                    $.post("<?php echo URL::to(ADMIN . "/tasks"); ?>/" + done + "/done?id=" + task_id);

                                });
                            });
                        </script>

                    </ul>
                <?php } else { ?>
                    <p class="text-center"><?php echo trans("dashboard::dashboard.no_tasks") ?></p>
                <?php } ?>


            </div>
        </div>

    </div>


</div>

<div class="row">

    <div class="col-md-8">

        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>
                    <i class="fa fa-users"></i>
                    <?php echo trans("dashboard::dashboard.recent_users") ?>                </h5>
            </div>
            <div class="ibox-content">
                <table class="table table-striped table-hover">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th><?php echo trans("dashboard::dashboard.photo") ?></th>
                            <th><?php echo trans("dashboard::dashboard.full_name") ?></th>
                            <th><?php echo trans("dashboard::dashboard.role") ?></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody class="valign-middle">
                        <?php
                        $i = 1;
                        foreach ($users as $user) {
                            ?>
                            <tr>
                                <td><?php echo $i; ?></td>
                                <td>
                                    <?php if ($user->media_path != "") { ?>
                                        <img src="<?php echo thumbnail($user->media_path); ?>" alt="" style="width:26px;height:26px;" class="rounded">
                                    <?php } else { ?>
                                        <img src="<?php echo assets("images/user.png"); ?>" alt="" style="width:26px;height:26px;" class="rounded">
                                    <?php } ?>
                                </td>

                                <td>
                                    <a class="text-navy" href="<?php echo route("users.edit", array("id" => $user->id)); ?>">
                                        <?php echo $user->first_name . " " . $user->last_name; ?>
                                    </a>
                                </td>
                                <td><?php echo $user->role_name; ?></td>
                                <td></td>
                            </tr>
                            <?php
                            $i++;
                        }
                        ?>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <?php if (count($poll_question)) { ?>
        <div class="col-md-4">

            <div class="widget style1 navy-bg" style="margin-top:0">
                <div class="row">
                    <div class="col-xs-4 text-center">
                        <i class="fa fa-trophy fa-5x"></i>
                    </div>
                    <div class="col-xs-8 text-right">
                        <strong> 
                            <a style="color:#fff" href="<?php echo URL::to(ADMIN . "/polls/" . $poll_question->poll_id . "/edit"); ?>">
                                <?php echo $poll_question->poll_text; ?> 
                            </a>
                        </strong>
                        <hr/>
                        <div class="text-sm"><a style="color:#fff" href="<?php echo URL::to(ADMIN . "/polls/" . $poll_question->poll_id . "/edit"); ?>"><?php echo $poll_answer->poll_text; ?></a></div>
                        <h2 class="font-bold"><?php
                            if ($poll_question->total_votes != 0) {
                                $percentage = ($poll_answer->total_votes / $poll_question->total_votes) * 100;
                            } else {
                                $percentage = "0";
                            }
                            ?>
                            <?php echo round($percentage); ?> <span class="text-lg text-slim">%</span></h2>
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>

</div>

@section('footer')
<script>
    $(document).ready(function () {
        $("#category_filter").change(function () {
            var base = $(this);
            location.href = "<?php echo URL::to(ADMIN . "/dashboard") ?>/" + base.val();
        });


    });
</script>

<script>
    $(document).ready(function () {
        $('.chosen-select').chosen();

    });

</script>

<script src="<?php echo assets() ?>/js/plugins/chartJs/Chart.min.js"></script>

<script>
    $(document).ready(function () {

        var lineData = {
            labels: [<?php echo '"' . join('", "', array_keys($posts_charts)) . '"'; ?>],
            datasets: [
                {
                    label: "الأخبار",
                    fillColor: "rgba(26,179,148,0.5)",
                    strokeColor: "rgba(26,179,148,0.7)",
                    pointColor: "rgba(26,179,148,1)",
                    pointStrokeColor: "#fff",
                    pointHighlightFill: "#fff",
                    pointHighlightStroke: "rgba(26,179,148,1)",
                    data: [<?php echo join(', ', array_values($posts_charts)); ?>]
                }
            ]
        };

        var lineOptions = {
            scaleShowGridLines: true,
            scaleGridLineColor: "rgba(0,0,0,.05)",
            scaleGridLineWidth: 1,
            bezierCurve: true,
            bezierCurveTension: 0.4,
            pointDot: true,
            pointDotRadius: 4,
            pointDotStrokeWidth: 1,
            pointHitDetectionRadius: 20,
            datasetStroke: true,
            datasetStrokeWidth: 2,
            datasetFill: true,
            responsive: true,
        };

        var ctx = document.getElementById("lineChart").getContext("2d");
        var myNewChart = new Chart(ctx).Line(lineData, lineOptions);

    });
</script>

@stop
@stop