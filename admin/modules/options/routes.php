<?php

Route::group(array(
    "prefix" => ADMIN,
    "before" => "auth",
        ), function($route) {
    $route->group(array("prefix" => "options"), function($route) {
        $route->any('/', array("as" => "options.show", "uses" => "OptionsController@index"));
        $route->any('/seo', array("as" => "options.seo", "uses" => "OptionsController@seo"));
        $route->any('/modules', array("as" => "options.modules", "uses" => "OptionsController@modules"));
        $route->any('/media', array("as" => "options.media", "uses" => "OptionsController@media"));
        $route->any('/social', array("as" => "options.social", "uses" => "OptionsController@social"));
    });
});


Route::any('sitemap', array("as" => "sitemap.update", "uses" => 'SitemapController@update'));
