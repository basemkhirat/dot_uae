<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-3">
        <h2>
            <?php echo trans("options::options.options"); ?>
        </h2>
        <ol class="breadcrumb">
            <li>
                <a href="<?php echo URL::to(ADMIN . "/options"); ?>"><?php echo trans("options::options.options"); ?></a>
            </li>
            <li class="active">
                <strong><?php echo trans("options::options." . $option_page); ?></strong>
            </li>
        </ol>
    </div>

    <div class="col-md-9">
        <ul class="nav nav-tabs option-tabs">
            <li <?php if ($option_page == "main") { ?>class="active"<?php } ?>><a href="<?php echo route("options.show"); ?>"><i class="fa fa-sliders"></i> <?php echo trans("options::options.main") ?></a></li>
            <li <?php if ($option_page == "seo") { ?>class="active"<?php } ?>><a  href="<?php echo route("options.seo"); ?>"><i class="fa fa-line-chart"></i> <?php echo trans("options::options.seo") ?></a></li>
            <li <?php if ($option_page == "media") { ?>class="active"<?php } ?>><a  href="<?php echo route("options.media"); ?>"><i class="fa fa-camera"></i> <?php echo trans("options::options.media") ?></a></li>
            <li <?php if ($option_page == "social") { ?>class="active"<?php } ?>><a  href="<?php echo route("options.social"); ?>"><i class="fa fa-globe"></i>  <?php echo trans("options::options.social") ?></a></li>
        </ul>
    </div>
</div>