@extends("admin::layouts.master")

@section("breadcrumb")
@include("options::partials.nav")
@stop

@section("content")

@include("admin::partials.messages")

<form action="" method="post">
    <div class="row">

        <div class="col-md-12">
            <div class="panel ">
                <!--
                <div class="panel-heading">

                    <div class="panel-options">

                        <ul class="nav nav-tabs">
                            <li class="active"><a data-toggle="tab" href="#options_main"><i class="fa fa-cogs"></i> <?php echo trans("options.options.main") ?></a></li>
                            <li class=""><a data-toggle="tab" href="#options_seo"><i class="fa fa-cogs"></i> <?php echo trans("options.options.seo") ?></a></li>
                        </ul>
                    </div>

                </div>
                -->
                <div class="panel-body">
                    <div class="tab-content">
                        <div id="options_main" class="tab-pane active">
                            <div class="row">
                                <div class="col-md-6">


                                    <div class="form-group">
                                        <label for="site_name"><?php echo trans("options::options.attributes.site_name") ?></label>
                                        <input name="site_name" type="text" value="<?php echo @Input::old("site_name", Config::get("site_name")); ?>" class="form-control" id="site_name" placeholder="<?php echo trans("options::options.attributes.site_name") ?>">
                                    </div>
                                    
                                    <div class="form-group">
                                        <label for="site_slogan"><?php echo trans("options::options.attributes.site_slogan") ?></label>
                                        <input name="site_slogan" type="text" value="<?php echo @Input::old("site_slogan", Config::get("site_slogan")); ?>" class="form-control" id="site_slogan" placeholder="<?php echo trans("options::options.attributes.site_slogan") ?>">
                                    </div>

                                    <div class="form-group">
                                        <label for="site_email"><?php echo trans("options::options.attributes.site_email") ?></label>
                                        <input name="site_email" type="text" value="<?php echo @Input::old("site_email", Config::get("site_email")); ?>" class="form-control" id="site_email" placeholder="<?php echo trans("options::options.attributes.site_email") ?>">
                                    </div>

                                    <div class="form-group">
                                        <label for="site_copyrights"><?php echo trans("options::options.attributes.site_copyrights") ?></label>
                                        <input name="site_copyrights" type="text" value="<?php echo @Input::old("site_copyrights", Config::get("site_copyrights")); ?>" class="form-control" id="site_copyrights" placeholder="<?php echo trans("options::options.attributes.site_copyrights") ?>">
                                    </div>

                                    <div class="form-group">
                                        <label for="timezone"><?php echo trans("options::options.attributes.timezone") ?></label>
                                        <select id="timezone" class="form-control chosen-select chosen-rtl" name="app_timezone">
                                            <?php
                                            for ($i = -12; $i <= 12; $i++) {

                                                if ($i == 0) {
                                                    $zone = "";
                                                } elseif ($i > 0) {
                                                    $zone = "+$i";
                                                } else {
                                                    $zone = $i;
                                                }
                                                ?>
                                                <option value="Etc/GMT<?php echo $zone; ?>" <?php if (Config::get("app.timezone") == "Etc/GMT" . $zone) { ?> selected="selected" <?php } ?>>GMT<?php echo $zone; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label for="date_format"><?php echo trans("options::options.attributes.date_format") ?></label>
                                        <select id="date_format" class="form-control chosen-select chosen-rtl" name="date_format">
                                            <?php foreach (array("Y-m-d H:i A", "Y-m-d", "d/m/Y", "H:i A") as $format) { ?>
                                                <option value="<?php echo $format; ?>" <?php if (Config::get("date_format") == $format) { ?> selected="selected" <?php } ?>><?php echo date($format); ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>

                                </div>
                                <div class="col-md-6">

                                    <div class="form-group">
                                        <label for="app_locale"><?php echo trans("options::options.attributes.locale") ?></label>
                                        <select id="app_locale" class="form-control chosen-select chosen-rtl" name="app_locale">
                                            <?php foreach (Config::get("locales") as $code => $name) { ?>
                                                <option value="<?php echo $code; ?>" <?php if (Config::get("app.locale") == $code) { ?> selected="selected" <?php } ?>><?php echo $name; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>

                                    <fieldset>
                                        <legend><?php echo trans("options::options.attributes.site_status") ?></legend>
      
                                        <div class="form-group switch-row">
                                            <label class="col-sm-10 control-label" for="site_status"><?php echo trans("options::options.attributes.site_status") ?></label>
                                            <div class="col-sm-2">
                                                <input <?php if (Config::get("site_status")) { ?> checked="checked" <?php } ?> type="checkbox" id="site_status" name="site_status" value="1" class="switcher switcher-sm">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="offline_message"><?php echo trans("options::options.attributes.offline_message") ?></label>
                                            <br/>
                                            <textarea class="form-control" id="offline_message" name="offline_message" placeholder="<?php echo trans("options::options.attributes.offline_message") ?>"><?php echo @Input::old("offline_message", Config::get("offline_message")); ?></textarea>
                                        </div>

                                    </fieldset>

                                </div>
                            </div>
                        </div>
                        <div id="options_seo" class="tab-pane">
                            <div class="row">
                                <div class="col-md-6">

                                    <div class="form-group">
                                        <label for="site_title"><?php echo trans("options::options.attributes.site_title") ?></label>
                                        <input name="site_title" type="text" value="<?php echo @Input::old("site_title", Config::get("site_title")); ?>" class="form-control" id="site_title" placeholder="<?php echo trans("options::options.attributes.site_title") ?>">
                                    </div>

                                    <div class="form-group">
                                        <label for="site_description"><?php echo trans("options::options.attributes.site_description") ?></label>
                                        <br/>
                                        <textarea class="form-control" id="site_description" name="site_description" placeholder="<?php echo trans("options::options.attributes.site_description") ?>"><?php echo @Input::old("site_description", Config::get("site_description")); ?></textarea>
                                    </div>

                                    <div class="form-group">
                                        <label for="site_keywords"><?php echo trans("options::options.attributes.site_keywords") ?></label>
                                        <br/>
                                        <input type="hidden" name="site_keywords" id="tags_names" value="<?php echo @Input::old("site_keywords", Config::get("site_keywords")); ?>">
                                        <ul id="mytags"></ul>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>

            </div> <!-- / .panel-body -->
        </div>
    </div>

    <div style="clear:both"></div>
    <div>
        <div class="panel-footer" style="border-top: 1px solid #ececec; position: relative;">
            <div class="form-group" style="margin-bottom:0">
                <input type="submit" class="pull-right btn btn-flat btn-primary" value="<?php echo trans("options::options.save_options") ?>" />
            </div>
        </div>
    </div>

</div>
</form>

@section("header")
<link href="<?php echo assets("tagit") ?>/jquery.tagit.css" rel="stylesheet" type="text/css">
<link href="<?php echo assets("tagit") ?>/tagit.ui-zendesk.css" rel="stylesheet" type="text/css">
@stop
@section("footer")
<script src="<?php echo assets("tagit") ?>/tag-it.js"></script>
<script>
    $(document).ready(function () {
        var elems = Array.prototype.slice.call(document.querySelectorAll('.switcher'));
        elems.forEach(function (html) {
            var switchery = new Switchery(html);
        });
    });
</script>
<script>
    $(document).ready(function () {
        
         $('.chosen-select').chosen();

        $("#mytags").tagit({
            singleField: true,
            singleFieldNode: $('#tags_names'),
            allowSpaces: true,
            minLength: 2,
            placeholderText: "",
            removeConfirmation: true,
            tagSource: function (request, response) {
                $.ajax({
                    url: "<?php echo route("google.search"); ?>",
                    data: {term: request.term},
                    dataType: "json",
                    success: function (data) {
                        console.log(data);
                        response($.map(data, function (item) {
                            return {
                                label: item.name,
                                value: item.name
                            }
                        }));
                    }
                });
            }
        });


    });
</script>
@stop
@stop
