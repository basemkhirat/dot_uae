<?php

class SitemapController extends BackendController {

    public function update() {
        Sitemap::refresh();
        return date(Config("date_format"), time());
    }

}
