<?php

use Illuminate\Database\Eloquent\Model as BModel;

class Model extends BModel {

    /**
     * Error message bag
     * 
     * @var Illuminate\Support\MessageBag
     */
    protected $errors;

    /**
     * Validation rules
     * 
     * @var Array
     */
    protected $creatingRules = [];
    protected $updatingRules = [];
    protected $searchable = [];
    protected $params = array();

    /**
     * Validator instance
     * 
     * @var Illuminate\Validation\Validators
     */
    public $validator;

    public function __construct(array $attributes = array(), Validator $validator = null) {
        $this->params = $attributes;
        parent::__construct($attributes);
        $this->validator = $validator ? : \App::make('validator');
    }

    public function scopeSearch($query, $q) {
        $query->where(function($where_ob) use ($q) {
            $i = 0;
            foreach ($this->searchable as $field) {
                if ($i == 0) {
                    $where_ob->where($field, 'LIKE', '%' . $q . '%');
                } else {
                    $where_ob->orWhere($field, 'LIKE', '%' . $q . '%');
                }
                $i++;
            }
        });
    }

    protected static function boot() {
        parent::boot();

        /* static::creating(function($model) {
          return $model->validate();
          }); */

        //static::saving(function($model) {
        //return $model->validate();
        //});

        /* static::updating(function($model) {
          return $model->validate();
          }); */
    }

    public function save(array $options = array()) {

        if (count($this->sluggable)) {
            if (!$this->exists) {   // on create we will create slug
                foreach ($this->sluggable as $from => $to) {
                    $this->attributes[$from] = Str::slug($this->attributes[$to]);
                }
            } else {
                foreach ($this->sluggable as $from => $to) {
                    unset($this->attributes[$from]);
                }
            }
        }


        parent::save($options);
    }

    public function setAttribute($key, $value) {
        $this->params[$key] = $value;
        parent::setAttribute($key, $value);
    }

    public function fill(array $attributes) {
        $this->params = $attributes;
        parent::fill($attributes);
    }

    /**
     * Validates current attributes against rules
     */
    public function validate() {

        if ($this->exists) { //update
            $rules = array();
            foreach ($this->updatingRules as $input => $rule) {
                foreach ($this->attributes as $field => $attribute) {
                    $rule = str_replace("[" . $field . "]", $this->attributes[$field], $rule);
                    $rules[$input] = $rule;
                }
            }
        } else {
            $rules = $this->creatingRules;
        }

        $v = $this->validator->make($this->params, $rules);
        $v->setCustomMessages((array) trans('admin::validation'));
        $v->setAttributeNames((array) trans($this->module . '::' . $this->module . ".attributes"));

        // getting custom validation

        if (method_exists($this, 'setValidation')) {
            $v = $this->setValidation($v);
        }

        if ($this->exists) {
            if (method_exists($this, 'setUpdateValidation')) {
                $v = $this->setUpdateValidation($v);
            }
        } else {
            if (method_exists($this, 'setCreateValidation')) {
                $v = $this->setCreateValidation($v);
            }
        }

        if ($v->passes()) {
            return true;
        }

        $this->setErrors($v->messages());
        return false;
    }

//    public function save(array $options = []){
//        
//        parent::save($options);
//        
//        
//    }

    /**
     * Set error message bag
     * 
     * @var Illuminate\Support\MessageBag
     */
    protected function setErrors($errors) {
        $this->errors = $errors;
    }

    /**
     * Retrieve error message bag
     */
    public function errors() {
        return $this->errors;
    }

    /**
     * Inverse of wasSaved
     */
    public function hasErrors() {
        return !empty($this->errors);
    }

    public function scopeTree($query, $options = array()) {

        static $template = " ";
        static $index = 0;

        if (!isset($options['table'])) {
            $options['table'] = $this->table;
        }
        if (!isset($options['id'])) {
            $options['id'] = $this->primaryKey;
        }
        if (!isset($options['parent'])) {
            $options['parent'] = $this->parentKey;
        }
        if (!isset($options['start'])) {
            $options['start'] = 0;
        }

        if (!isset($options['row'])) {
            $options['row'] = false;
        }

        if (!isset($options['query'])) {
            $options['query'] = false;
        }

        if (!isset($options['depth'])) {
            $options['depth'] = "0";
        }

        if (!isset($options['count'])) {
            $options['count'] = 0;
        }

        $rows = DB::table($options['table']);

        if ($options['query']) {
            $rows = call_user_func($options['query'], $rows);
        }

        $rows = $rows->where($options['parent'], $options['start'])->get();

        if (count($rows)) {

            $options['count'] ++;
            if ($options['count'] >= 2) {
                $template .= "<ul>";
            }
            foreach ($rows as $row) {
                $index++;

                if ($options['row']) {

                    $list_row = call_user_func($options['row'], $row, $options['count']);

                    if (isset($options['callback'])) {
                        $list_row = call_user_func($options['callback'], $row, $list_row);
                    }

                    $template .= $list_row;
                }

                if ($options['depth'] == $options['count']) {

                    continue;
                } else {

                    $options["start"] = $row->$options['id'];
                    self::tree($options);
                }
            }

            if ($options['count'] >= 2) {
                $template .= "</ul>";
            }
        } else {
            $options['count'] --;
        }


        return $template;
    }

}
