<?php

if (Config::get("app.url") == "http://localhost") {
    Config::set("app.url", Request::root());
}

define("AMAZON", 1);
define("ADMIN", "backend");
define("UPLOADS", "uploads");
define("AMAZON_URL", "https://" . Config::get("media.s3.bucket") . ".s3-" . Config::get("media.s3.region") . ".amazonaws.com/");

include __DIR__ . '/helpers.php';
include __DIR__ . '/filters.php';
include __DIR__ . '/routes.php';
include __DIR__ . '/events.php';
