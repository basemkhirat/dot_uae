<?php

class #module|ucfirst#Controller extends BackendController {
    
    protected $data = [];

    function __construct(){
        parent::__construct();
    }
    
    function index(){
    
        if (Request::isMethod("post")) {
            if (Input::has("action")) {
                switch (Input::get("action")) {
                    case "delete":
                        return $this->delete();
                    {if options.status}
                    case "activate":
                        return $this->status(1);
                    case "deactivate":
                        return $this->status(0);
                    {/if}
                }
            }
        }
        
        $this->data["sort"] = (Input::has("sort")) ? Input::get("sort") : "#key#";
        $this->data["order"] = (Input::has("order")) ? Input::get("order") : "DESC";
        $this->data['per_page'] = (Input::has("per_page")) ? Input::get("per_page") : NULL;
    
        $query = #model|ucfirst#::#loaded_models#orderBy($this->data["sort"], $this->data["order"]);

        {if module.tags}
        if (Input::has("tag_id")) {
            $query->whereHas("tags", function($query) {
                $query->where("tags.tag_id", Input::get("tag_id"));
            });
        }
        {/if}
        {if module.categories}
        if (Input::has("category_id")) {
            $query->whereHas("categories", function($query) {
                $query->where("categories.cat_id", Input::get("category_id"));
            });
        }
        {/if}
        {if module.user}
        if (Input::has("user_id")) {
            $query->whereHas("user", function($query) {
                $query->where("users.id", Input::get("user_id"));
            });
        }
        {/if}
        {if options.status}
        if (Input::has("status")) {
            $query->where("status", Input::get("status"));
        }
        {/if}
        if (Input::has("q")) {
            $query->search(urldecode(Input::get("q")));
        }
        $this->data["#module#"] = $query->paginate($this->data['per_page']);
        return View::make("#module#::show", $this->data);
    
    }
    
    public function create() {

        if (Request::isMethod("post")) {

            $#model# = new #model|ucfirst#();
            [loop attributes as attribute]
            $#model#->#attribute# = Input::get('#attribute#');
            [/loop]
            {if module.image}
            $#model#->image_id = Input::get('image_id');
            {/if}
            {if module.user}
            $#model#->user_id = Auth::user()->id;
            {/if}
            {if options.status}
            $#model#->status = Input::get("status", 0);
            {/if}
            
            if(!$#model#->validate()){
                return Redirect::back()->withErrors($#model#->errors())->withInput(Input::all());
            }
            
            $#model#->save();
            {if module.categories}
            $#model#->syncCategories(Input::get("categories"));
            {/if}
            {if module.tags}
            $#model#->syncTags(Input::get("tags"));
            {/if}
            
            return Redirect::route("#module#.edit", array("#key#" => $#model#->#key#))
                    ->with("message", trans("#module#::#module#.events.created"));
        }
        {if module.categories}
        $this->data["#model#_categories"] = array();
        {/if}
        {if module.tags}
        $this->data["#model#_tags"] = array();
        {/if}
        $this->data["#model#"] = false;
        return View::make("#module#::edit", $this->data);
    }
    
    public function edit($#key#) {

        $#model# = #model|ucfirst#::findOrFail($#key#);

        if (Request::isMethod("post")) {
        
            [loop attributes as attribute]
            $#model#->#attribute# = Input::get('#attribute#');
            [/loop]
            {if module.image}
            $#model#->image_id = Input::get('image_id');
            {/if}
            {if options.status}
            $#model#->status = Input::get("status", 0);
            {/if}
            
            if(!$#model#->validate()){
                return Redirect::back()->withErrors($#model#->errors())->withInput(Input::all());
            }
            
            $#model#->save();
            {if module.categories}
            $#model#->syncCategories(Input::get("categories"));
            {/if}
            {if module.tags}
            $#model#->syncTags(Input::get("tags"));
            {/if}

            return Redirect::route("#module#.edit", array("#key#" => $#key#))->with("message", trans("#module#::#module#.events.updated"));
        }
        
        {if module.categories}
        $this->data["#model#_categories"] = $#model#->categories->lists("cat_id")->toArray();
        {/if}
        {if module.tags}
        $this->data["#model#_tags"] = $#model#->tags->lists("tag_name")->toArray();
        {/if}
        $this->data["#model#"] = $#model#;
        return View::make("#module#::edit", $this->data);
    }
    
    public function delete() {

        $ids = Input::get("#key#");

        if (!is_array($ids)) {
            $ids = array($ids);
        }

        foreach ($ids as $ID) {
            $#model# = #model|ucfirst#::findOrFail($ID);
            {if module.categories}
            $#model#->categories()->detach();
            {/if}
            {if module.tags}
            $#model#->tags()->detach();
            {/if}
            $#model#->delete();
        }

        return Redirect::back()->with("message", trans("#module#::#module#.events.deleted"));
    }
    {if options.status}
    public function status($status) {

        $ids = Input::get("#key#");

        if (!is_array($ids)) {
            $ids = array($ids);
        }

        foreach ($ids as $#key#) {
            $#model# = #model|ucfirst#::findOrFail($#key#);
            $#model#->status = $status;
            $#model#->save();
        }
        
        if($status){
            $message = trans("#module#::#module#.events.activated");
        }else{
            $message = trans("#module#::#module#.events.deactivated");
        }

        return Redirect::back()->with("message", $message);
    }
    {/if}
    
}