<?php

Route::group(array(
    "prefix" => ADMIN,
    "before" => "auth",
        ), function($route) {
        $route->group(array("prefix" => "#module#"), function($route) {
            $route->any('/', array("as" => "#module#.show", "uses" => "#module|ucfirst#Controller@index"));
            $route->any('/create', array("as" => "#module#.create", "uses" => "#module|ucfirst#Controller@create"));
            $route->any('/{#key#}/edit', array("as" => "#module#.edit", "uses" => "#module|ucfirst#Controller@edit"));
            $route->any('/delete', array("as" => "#module#.delete", "uses" => "#module|ucfirst#Controller@delete"));
            {if options.status}$route->any('/{status}/status', array("as" => "#module#.status", "uses" => "#module|ucfirst#Controller@status"));{/if}
            
        });
});
