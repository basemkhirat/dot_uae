{
    "fields": {
        "ID": "increments",
    },
    "grid": [

    ],
    "form": {

    },
    "require": [
        "image",
        "user",
        "tags",
        "categories"
    ],
    "relations": {
    
    },
    "options": {
        "icon" : "fa-bar-chart",   
        "timestamps" : true,
        "status" : true,
        "per_page" : 16
    }
}