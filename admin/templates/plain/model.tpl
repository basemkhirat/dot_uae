<?php

class $Module$ extends Model {

    protected $table = '';
    public $timestamps = false;
    
    protected $fillable = array("*");
    protected $guarded = array();
    
    protected $visible = array("*");
    protected $hidden = array();
    
    protected $rules = [
        
    ];
    
    
    
}