<?php

class $Module$Controller extends BackendController {
    
    function __construct(){
        parent::__construct();
    }
    
    function index(){
        return view("$module$::$module$");
    }
    
}