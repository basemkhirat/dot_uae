<?php

Route::group(array(
    "prefix" => ADMIN,
    "before" => "auth",
        ), function($route) {
        $route->group(array("prefix" => "$module$"), function($route) {
            $route->get('/', '$Module$Controller@index');
        });
});
