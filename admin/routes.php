<?php

Route::any('/wiki', "WikiController@index");
Route::any('/wiki/get', "WikiController@api");

Route::any('/wiki/export', "WikiController@export");
