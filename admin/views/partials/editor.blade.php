<?php
$name = isset($name) ? $name : "post_content";
$value = isset($value) ? $value : "";
$id = isset($id) ? $id : "post-content";
?>

<div style="margin-bottom:5px;">
    <a  id="add_files" class="btn btn-default" href="#"><span class="btn-label icon fa fa-camera"></span>&nbsp;{!!Lang::get('posts::posts.add_media')!!}</a>
</div>  
<textarea name="<?php echo $name; ?>" id="<?php echo $id; ?>"><?php echo $value; ?></textarea>

@section("footer")
@parent
<script>
    CKEDITOR.replace("<?php echo $id; ?>", {
        language: '<?php echo App::getLocale(); ?>',
        height: '200px',
        resize_enabled: true,
        resize_dir: 'vertical',
    });
    $(document).ready(function () {
        1
        $("#add_files").filemanager({
            types: "image|video|audio|pdf",
            done: function (files) {
                if (files.length) {
                    files.forEach(function (file) {
                        var html = "";
                        if (file.media_url.split('.').pop() == 'pdf') {
                            html += "<iframe src='" + file.media_url + "' width='100%' height='300px'  /><br>";
                            html += "<a href='" + file.media_url + "' target='_blank'><img src='" + assetsURL + "images/pdf.png' width='30' height='30'></a>";
                        } else {
                            if (file.media_type == 'image') {
                                html += "<img src='" + file.media_url + "'> ";
                            } else {
                                html += file.media_embed;
                            }
                            var element = CKEDITOR.dom.element.createFromHtml(html);
                            CKEDITOR.instances["<?php echo $id; ?>"].insertElement(element);
                        }

                    });
                }
            },
            error: function (media_path) {
                alert("<?php echo trans("admin::common.not_supported_file") ?>");
            }
        });
    });
</script>
@stop