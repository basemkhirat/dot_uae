<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
    <head>
        <meta charset="utf-8">
        <meta name="robots" content="noindex, nofollow">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title><?php echo Config::get("site_title"); ?></title>
        <link href="<?php echo assets() ?>/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo assets() ?>/font-awesome/css/font-awesome.css" rel="stylesheet">
        <link href="<?php echo assets() ?>/css/font-awesome-animation.css" rel="stylesheet">
        <link href="<?php echo assets() ?>/css/plugins/iCheck/custom.css" rel="stylesheet">
        <link href="<?php echo assets() ?>/css/animate.css" rel="stylesheet">
        <link href="<?php echo assets() ?>/css/style.css" rel="stylesheet">
        <?php if (Config::get("app.locale") == "ar") { ?>
            <link href="<?php echo assets() ?>/uploader/popup.rtl.css" rel="stylesheet" type="text/css">
        <?php } else { ?>
            <link href="<?php echo assets() ?>/uploader/popup.ltr.css" rel="stylesheet" type="text/css">
        <?php } ?>
        <script src="<?php echo assets() ?>/js/jquery-2.1.1.js"></script>
        <link href="<?php echo assets() ?>/css/plugins/switchery/switchery.css" rel="stylesheet">
        <link href="<?php echo assets() ?>/css/plugins/chosen/chosen.css" rel="stylesheet">
        <?php if (Config::get("app.locale") == "ar") { ?>
            <link href="<?php echo assets() ?>/css/rtl.css" rel="stylesheet">
            <link href="<?php echo assets() ?>/css/plugins/bootstrap-rtl/bootstrap-rtl.min.css" rel="stylesheet">
        <?php } ?>
        @yield("header")
        <link href="<?php echo assets() ?>/css/adnan.css" rel="stylesheet">
        <link href="<?php echo assets() ?>/css/master.css" rel="stylesheet">
        <link rel="icon" type="image/png" href="<?php echo assets("images/logo.ico") ?>">
        <script>
            AMAZON = "<?php echo AMAZON; ?>";
            base_url = "<?php echo Config::get("app.url"); ?>";
            baseURL = "<?php echo Config::get("app.url") . ADMIN . '/' ?>";
        </script>
    </head>
    <body class="<?php if (Config::get("app.locale") == "ar") { ?>rtls<?php } ?> <?php if (isset($_COOKIE["mini_nav"]) and $_COOKIE["mini_nav"] == "1") { ?>mini-navbar<?php } ?>">

        <div id="blueimp-gallery" class="blueimp-gallery" style="display: none">
            <div class="slides"></div>
            <h3 class="title"></h3>
            <a class="prev">‹</a>
            <a class="next">›</a>
            <a class="close"><i class="fa fa-times"></i></a>
            <a class="play-pause"></a>
            <ol class="indicator"></ol>
        </div>
        <div id="overlay">
            <div id="loading"><img id="loading" src="<?php echo assets("images/_loading.gif"); ?>"></div>
        </div>