<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav" id="side-menu">
            <li class="nav-header">
                <div class="dropdown profile-element"> <span>
                        <img alt="image" class="img-circle" src="<?php echo get_user_photo(); ?>" />
                    </span>
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <span class="clear"> <span class="block m-t-xs"> <strong class="font-bold"><?php echo ucfirst(Auth::user()->first_name) . " " . ucfirst(Auth::user()->last_name); ?></strong>
                            </span> <span class="text-muted text-xs block"> <?php echo DB::table("roles")->where("role_id", Auth::user()->role_id)->pluck("role_name"); ?> <b class="caret"></b></span> </span> </a>
                    <ul class="dropdown-menu animated fadeInRight m-t-xs">
                        <li><a href="<?php echo URL::to(ADMIN) ?>/posts?user_id=<?php echo Auth::user()->id; ?>"><?php echo trans("auth::auth.my_posts") ?></a></li>
                        <li><a href="<?php echo route("users.edit", array("id" => Auth::user()->id)); ?>"><?php echo trans("auth::auth.edit_profile") ?></a></li>
                        <li class="divider"></li>
                        <li><a href="<?php echo route("auth.logout"); ?>"><?php echo trans("auth::auth.logout") ?></a></li>
                    </ul>
                </div>
                <div class="logo-element">
                    DE
                </div>
            </li>
            <?php echo Menu::get("sidebar")->render(); ?>
        </ul>
    </div>
</nav>