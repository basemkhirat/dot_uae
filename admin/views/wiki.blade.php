<link href="<?php echo assets("css/bootstrap.min.css"); ?>" rel="stylesheet">

<div class="form-group">
    <label for="input-description"></label>
    <div style="margin-bottom:5px;">
        <a class="btn btn-default" data-toggle="modal" data-target="#GSCCModal" href="#">Add Wiki page</a>
        
        <a class="btn btn-default" id="export" data-toggle="modal" data-target="#ExportModal" href="#">Export</a>
    </div>  
    <textarea name="description" id="description"></textarea>
</div>


<div id="ExportModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <form action="" method="post" class="wiki-form">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;  </button>
                    <h4 class="modal-title" id="myModalLabel">Export</h4>
                </div>
                <div class="modal-body">
                    <textarea class="form-control" id="wiki_area" style="height: 400px"></textarea>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </form>
    </div>
</div>

<div id="GSCCModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <form action="" method="post" class="wiki-form">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;  </button>
                    <h4 class="modal-title" id="myModalLabel">Add wikipedia link </h4>
                </div>
                <div class="modal-body">
                    <input id="grabbing-input" type="text" name="link" class="form-control" placeholder="https://wikipedia.org/wiki/Tom_Simpson" />
                    <br />
                    <div class="alert alert-danger error-area" style="display: none" role="alert">
                        <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                        <span class="sr-only">Error:</span>
                        Invalid link
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" id="grabbing-button" class="btn btn-primary" data-loading-text="Getting data...">Get page</button>
                </div>
            </div>
        </form>
    </div>
</div>

<script src="<?php echo assets('js/jquery-2.1.1.js'); ?>"></script>
<script src="<?php echo assets('js/bootstrap.min.js'); ?>"></script>

<script type="text/javascript" src="<?php echo assets('ckeditor/ckeditor.js') ?>"></script>
<script>

    CKEDITOR.replace("description", {
        language: 'en',
        height: '200px',
        resize_enabled: true,
        resize_dir: 'vertical',
    });


    $(document).ready(function () {

    

        $("#export").click(function(){
            var data = CKEDITOR.instances.description.getData();
            $.ajax({
                method: "POST",
                url: "<?php echo URL::to("wiki/export") ?>",
                data: {content: data},
            }).done(function (result) {
                $("#wiki_area").val(result);
            });

        });   
    
        
      


        $(".wiki-form").submit(function () {
            var base = $(this);
            $("#grabbing-button").button('loading');
            $.ajax({
                method: "POST",
                url: "<?php echo URL::to("wiki/get") ?>",
                data: base.serialize(),
                beforeSend: function () {
                    $(".error-area").hide();
                }
            }).done(function (data) {
                //if (data != "") {
                $(".error-area").hide();
                $("#GSCCModal").modal("hide");
                CKEDITOR.instances.description.insertHtml(data);
                $("#grabbing-input").val("");
                $("#grabbing-button").button('reset');
                //}
            }).error(function () {
                $(".error-area").show();
                $("#grabbing-button").button('reset');
            })

            return false;

        });

    });
</script>