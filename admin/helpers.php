<?php

function admin_path($path = "") {
    return base_path("admin/" . $path);
}

function templates_path($path = "") {
    return admin_path("templates/" . $path);
}

function modules_path($path = "") {
    return admin_path("modules/" . $path);
}

function assets($path = "") {

    if (strstr($path, "::")) {
        list($namespace, $path) = explode("::", $path);
        if ($namespace != "admin") {
            $path = "modules/" . $path;
        }
    }

    return asset("admin" . "/" . $path);
}

function admin_url($file = "") {
    return URL::to(ADMIN . "/" . $file);
}

function uploads_path($file = "") {
    return public_path() . "/" . UPLOADS . "/" . $file;
}

function get_user_photo() {
    return Session::get("user_data")["photo"];
}

function countNotEmpty($array) {//counts the non-empty items of an array
    $counter = 0;
    foreach ($array as $item) {
        if (!empty($item)) {
            $counter++;
        }
    }
    return $counter;
}

/* ==========================Adnan========================= */

function url_except($input = '', &$prop) {
    $url = '';
    if (Input::has($input)) {

        $i = 0;
        $prop = $input;
        foreach (Input::except($prop) as $key => $value) {
            if (@$i == 0) {
                $url .= '?' . $key . '=' . $value;
            } else {
                $url .= '&' . $key . '=' . $value;
            }
            $i++;
        }
    }
    return $url;
}

function handle_site() {
    $conn = Input::get('conn');
    if ($conn) {
        Session::put('conn', $conn);
    } else {
        $conn = Session::get('conn');
        if (!$conn) {
            $conn = Config::get("dfLang");
            Session::put('conn', $conn);
        }
    }
}

function switch_lang($lang = 'ar') {
    $url = Request::url();
    $params = '';
    foreach (Input::except('conn') as $key => $value) {
        $params .= '&' . $key . '=' . $value;
    }

    return $url . "?conn=" . $lang . $params;
}

/**
 * get all parent categories
 */
function get_cats($args = []) {
    handle_site();
    $cats = Category::get_cats(['lang' => App::getLocale(), 'parent' => $args['parent'], 'conn' => \Session::get('conn')]);
    return $cats;
}

/**
 * create slug
 */
function create_slug($str, $options = array()) {
    /* $slug = preg_replace('/[^A-Za-z0-9-أإآابتثجحخدذرزسشصضطظعغفقكلمنهولالأل‘ىيئةءؤدً]+/', '-', mb_strtolower($string));
      return $slug; */

    $str = mb_convert_encoding((string) $str, 'UTF-8', mb_list_encodings());

    $defaults = array(
        'delimiter' => '-',
        'limit' => null,
        'lowercase' => true,
        'replacements' => array(),
        'transliterate' => false,
    );

// Merge options
    $options = array_merge($defaults, $options);

    $char_map = array(
// Latin
        'À' => 'A', 'Á' => 'A', 'Â' => 'A', 'Ã' => 'A', 'Ä' => 'A', 'Å' => 'A', 'Æ' => 'AE', 'Ç' => 'C',
        'È' => 'E', 'É' => 'E', 'Ê' => 'E', 'Ë' => 'E', 'Ì' => 'I', 'Í' => 'I', 'Î' => 'I', 'Ï' => 'I',
        'Ð' => 'D', 'Ñ' => 'N', 'Ò' => 'O', 'Ó' => 'O', 'Ô' => 'O', 'Õ' => 'O', 'Ö' => 'O', 'Ő' => 'O',
        'Ø' => 'O', 'Ù' => 'U', 'Ú' => 'U', 'Û' => 'U', 'Ü' => 'U', 'Ű' => 'U', 'Ý' => 'Y', 'Þ' => 'TH',
        'ß' => 'ss',
        'à' => 'a', 'á' => 'a', 'â' => 'a', 'ã' => 'a', 'ä' => 'a', 'å' => 'a', 'æ' => 'ae', 'ç' => 'c',
        'è' => 'e', 'é' => 'e', 'ê' => 'e', 'ë' => 'e', 'ì' => 'i', 'í' => 'i', 'î' => 'i', 'ï' => 'i',
        'ð' => 'd', 'ñ' => 'n', 'ò' => 'o', 'ó' => 'o', 'ô' => 'o', 'õ' => 'o', 'ö' => 'o', 'ő' => 'o',
        'ø' => 'o', 'ù' => 'u', 'ú' => 'u', 'û' => 'u', 'ü' => 'u', 'ű' => 'u', 'ý' => 'y', 'þ' => 'th',
        'ÿ' => 'y',
// Latin symbols
        '©' => '(c)',
// Greek
        'Α' => 'A', 'Β' => 'B', 'Γ' => 'G', 'Δ' => 'D', 'Ε' => 'E', 'Ζ' => 'Z', 'Η' => 'H', 'Θ' => '8',
        'Ι' => 'I', 'Κ' => 'K', 'Λ' => 'L', 'Μ' => 'M', 'Ν' => 'N', 'Ξ' => '3', 'Ο' => 'O', 'Π' => 'P',
        'Ρ' => 'R', 'Σ' => 'S', 'Τ' => 'T', 'Υ' => 'Y', 'Φ' => 'F', 'Χ' => 'X', 'Ψ' => 'PS', 'Ω' => 'W',
        'Ά' => 'A', 'Έ' => 'E', 'Ί' => 'I', 'Ό' => 'O', 'Ύ' => 'Y', 'Ή' => 'H', 'Ώ' => 'W', 'Ϊ' => 'I',
        'Ϋ' => 'Y',
        'α' => 'a', 'β' => 'b', 'γ' => 'g', 'δ' => 'd', 'ε' => 'e', 'ζ' => 'z', 'η' => 'h', 'θ' => '8',
        'ι' => 'i', 'κ' => 'k', 'λ' => 'l', 'μ' => 'm', 'ν' => 'n', 'ξ' => '3', 'ο' => 'o', 'π' => 'p',
        'ρ' => 'r', 'σ' => 's', 'τ' => 't', 'υ' => 'y', 'φ' => 'f', 'χ' => 'x', 'ψ' => 'ps', 'ω' => 'w',
        'ά' => 'a', 'έ' => 'e', 'ί' => 'i', 'ό' => 'o', 'ύ' => 'y', 'ή' => 'h', 'ώ' => 'w', 'ς' => 's',
        'ϊ' => 'i', 'ΰ' => 'y', 'ϋ' => 'y', 'ΐ' => 'i',
// Turkish
        'Ş' => 'S', 'İ' => 'I', 'Ç' => 'C', 'Ü' => 'U', 'Ö' => 'O', 'Ğ' => 'G',
        'ş' => 's', 'ı' => 'i', 'ç' => 'c', 'ü' => 'u', 'ö' => 'o', 'ğ' => 'g',
// Russian
        'А' => 'A', 'Б' => 'B', 'В' => 'V', 'Г' => 'G', 'Д' => 'D', 'Е' => 'E', 'Ё' => 'Yo', 'Ж' => 'Zh',
        'З' => 'Z', 'И' => 'I', 'Й' => 'J', 'К' => 'K', 'Л' => 'L', 'М' => 'M', 'Н' => 'N', 'О' => 'O',
        'П' => 'P', 'Р' => 'R', 'С' => 'S', 'Т' => 'T', 'У' => 'U', 'Ф' => 'F', 'Х' => 'H', 'Ц' => 'C',
        'Ч' => 'Ch', 'Ш' => 'Sh', 'Щ' => 'Sh', 'Ъ' => '', 'Ы' => 'Y', 'Ь' => '', 'Э' => 'E', 'Ю' => 'Yu',
        'Я' => 'Ya',
        'а' => 'a', 'б' => 'b', 'в' => 'v', 'г' => 'g', 'д' => 'd', 'е' => 'e', 'ё' => 'yo', 'ж' => 'zh',
        'з' => 'z', 'и' => 'i', 'й' => 'j', 'к' => 'k', 'л' => 'l', 'м' => 'm', 'н' => 'n', 'о' => 'o',
        'п' => 'p', 'р' => 'r', 'с' => 's', 'т' => 't', 'у' => 'u', 'ф' => 'f', 'х' => 'h', 'ц' => 'c',
        'ч' => 'ch', 'ш' => 'sh', 'щ' => 'sh', 'ъ' => '', 'ы' => 'y', 'ь' => '', 'э' => 'e', 'ю' => 'yu',
        'я' => 'ya',
// Ukrainian
        'Є' => 'Ye', 'І' => 'I', 'Ї' => 'Yi', 'Ґ' => 'G',
        'є' => 'ye', 'і' => 'i', 'ї' => 'yi', 'ґ' => 'g',
// Czech
        'Č' => 'C', 'Ď' => 'D', 'Ě' => 'E', 'Ň' => 'N', 'Ř' => 'R', 'Š' => 'S', 'Ť' => 'T', 'Ů' => 'U',
        'Ž' => 'Z',
        'č' => 'c', 'ď' => 'd', 'ě' => 'e', 'ň' => 'n', 'ř' => 'r', 'š' => 's', 'ť' => 't', 'ů' => 'u',
        'ž' => 'z',
// Polish
        'Ą' => 'A', 'Ć' => 'C', 'Ę' => 'e', 'Ł' => 'L', 'Ń' => 'N', 'Ó' => 'o', 'Ś' => 'S', 'Ź' => 'Z',
        'Ż' => 'Z',
        'ą' => 'a', 'ć' => 'c', 'ę' => 'e', 'ł' => 'l', 'ń' => 'n', 'ó' => 'o', 'ś' => 's', 'ź' => 'z',
        'ż' => 'z',
// Latvian
        'Ā' => 'A', 'Č' => 'C', 'Ē' => 'E', 'Ģ' => 'G', 'Ī' => 'i', 'Ķ' => 'k', 'Ļ' => 'L', 'Ņ' => 'N',
        'Š' => 'S', 'Ū' => 'u', 'Ž' => 'Z',
        'ā' => 'a', 'č' => 'c', 'ē' => 'e', 'ģ' => 'g', 'ī' => 'i', 'ķ' => 'k', 'ļ' => 'l', 'ņ' => 'n',
        'š' => 's', 'ū' => 'u', 'ž' => 'z'
    );

// Make custom replacements
    $str = preg_replace(array_keys($options['replacements']), $options['replacements'], $str);

// Transliterate characters to ASCII
    if ($options['transliterate']) {
        $str = str_replace(array_keys($char_map), $char_map, $str);
    }

// Replace non-alphanumeric characters with our delimiter
    $str = preg_replace('/[^\p{L}\p{Nd}]+/u', $options['delimiter'], $str);

// Remove duplicate delimiters
    $str = preg_replace('/(' . preg_quote($options['delimiter'], '/') . '){2,}/', '$1', $str);

// Truncate slug to max. characters
    $str = mb_substr($str, 0, ($options['limit'] ? $options['limit'] : mb_strlen($str, 'UTF-8')), 'UTF-8');

// Remove delimiter from ends
    $str = trim($str, $options['delimiter']);

    return $options['lowercase'] ? mb_strtolower($str, 'UTF-8') : $str;
}

/**
 * Since Post Date
 *
 * @return Response
 */
function time_ago($date, $granularity = 2) {
    $date = strtotime($date);
//    var_dump($date);
    $difference = 0;
    while (empty($difference)) {
        $difference = time() - $date;
    }
    $periods = array('قرن' => 315360000,
        'سنة' => 31536000,
        'شهر' => 2628000,
        'أسبوع' => 604800,
        'يوم' => 86400,
        'س' => 3600,
        'د' => 60,
        'ث' => 1);

    $retval = '';
    foreach ($periods as $key => $value) {
        if ($difference >= $value) {
            $time = floor($difference / $value);
            $difference %= $value;
            $retval .= ($retval ? ' ' : '') . $time . ' ';
            $retval .= $key;
            $granularity--;
        }
        if ($granularity == '0') {
            break;
        }
    }
    return $retval;
}

function date_view($date) {
    if (DIRECTION == 'rtl') {
        return arabic_date($date);
    } else {
        return date('M d, Y', strtotime($date)) . '@' . date('h:i a', strtotime($date));
    }
}

function arabic_date($target_date = '', $type = '') {

    // PHP Arabic Date

    if ($type != 'mongo') {
        $target_date = strtotime($target_date);
    }

    $months = array(
        "Jan" => "يناير",
        "Feb" => "فبراير",
        "Mar" => "مارس",
        "Apr" => "أبريل",
        "May" => "مايو",
        "Jun" => "يونيو",
        "Jul" => "يوليو",
        "Aug" => "أغسطس",
        "Sep" => "سبتمبر",
        "Oct" => "أكتوبر",
        "Nov" => "نوفمبر",
        "Dec" => "ديسمبر"
    );

    if (!$target_date) {

        $target_date = date('y-m-d'); // The Current Date
    }

    $en_month = date("M", $target_date);

    foreach ($months as $en => $ar) {
        if ($en == $en_month) {
            $ar_month = $ar;
            break;
        }
    }

    $find = array(
        "Sat",
        "Sun",
        "Mon",
        "Tue",
        "Wed",
        "Thu",
        "Fri"
    );

    $replace = array(
        "السبت",
        "الأحد",
        "الإثنين",
        "الثلاثاء",
        "الأربعاء",
        "الخميس",
        "الجمعة"
    );

    $ar_day_format = date('D', $target_date); // The Current Day

    $ar_day = str_replace($find, $replace, $ar_day_format);

    header('Content-Type: text/html; charset=utf-8');

    $standard = array("0", "1", "2", "3", "4", "5", "6", "7", "8", "9");
    $eastern_arabic_symbols = array("٠", "١", "٢", "٣", "٤", "٥", "٦", "٧", "٨", "٩");
    $standard2 = array("pm", "PM", "am", "AM");
    $eastern_arabic_symbols2 = array("م", "م", "ص", "ص");
    $the_date = $ar_day . '، ' . date('j', $target_date) . ' ' . $ar_month . ' ' . date('Y', $target_date) . ' - ' . date('g:i a', $target_date);
    $arabic_date = str_replace($standard, $eastern_arabic_symbols, $the_date);
    $arabic_date = str_replace($standard2, $eastern_arabic_symbols2, $arabic_date);

    // Echo Out the Date
    return $arabic_date;
}

function denied() {
    return "Permission denied";
}

/*
  |--------------------------------------------------------------------------
  | to array
  |--------------------------------------------------------------------------
  |
 */

function to_array($objs, $vars = true) {
    $temp = array();
    $arr = array();
    if ($vars) {
        foreach ($objs as $key => $obj) {
            foreach (get_object_vars($obj) as $key => $value) {
                $temp[$key] = $value;
            }
            $arr[] = $temp;
        }
    } else {
        foreach ($objs as $key => $obj) {
            foreach (get_object_vars($obj) as $key => $value) {
                $arr[] = $value;
            }
        }
    }
    return $arr;
}

function setNullWhenEmpty($var) {
    if (empty($var)) {
        return null;
    } else {
        return $var;
    }
}

function string_sanitize($s) {
    $remove[] = "'";
    $remove[] = '"';
    //$remove[] = "-"; // just as another example

    $result = str_replace($remove, "", $s);
    return $result;
}

function html2wiki($string) {

    $html = Sunra\PhpSimple\HtmlDomParser::str_get_html($string);

    $headings = array("h3", "h2", "h1");

    foreach ($headings as $heading) {

        if ($heading == "h3") {
            $sign = "===";
        }

        if ($heading == "h2") {
            $sign = "==";
        }

        if ($heading == "h1") {
            $sign = "=";
        }

        foreach ($html->find($heading) as $tag) {
            $tag->outertext = $sign . $tag->innertext . $sign . "\r\n";
        }

        foreach ($html->find("em") as $tag) {

            if ($tag->parent()->tag == "strong") {
                $sign = "'''''";
                $tag->outertext = $sign . $tag->innertext . $sign;
            } else {
                $sign = "''";
                $tag->outertext = $sign . $tag->innertext . $sign;
            }
        }

        foreach ($html->find("strong") as $tag) {
            $sign = "'''";
            $tag->outertext = $sign . $tag->innertext . $sign;
        }

        foreach ($html->find("hr") as $tag) {
            $tag->outertext = "---" . "\r\n";
        }

        foreach ($html->find("img") as $tag) {
            $tag->outertext = "[[File:" . $tag->src . " " . $tag->alt . "]]" . "\r\n";
        }

        foreach ($html->find("a") as $tag) {
            $tag->outertext = "[" . $tag->href . " " . $tag->innertext . "]";
        }

        $lists = array("ul", "ol", "dl");

        foreach ($lists as $list) {
            foreach ($html->find($list) as $lists) {
                $parent = $lists->parent();
                if ($parent->tag != 'li' and $parent->tag != "dd") {
                    $list_output = getList($lists, 1, 1);
                    $lists->outertext = $list_output;
                }
            }
        }

        foreach ($html->find("br") as $tag) {
            $tag->outertext = "\r\n";
        }
    }

    
    $content = clean_code(html2wiki_tables($html->outertext));
    return strip_tags($content);
}

function clean_code($text) {
        $content = preg_replace('/(<(script|style)\b[^>]*>).*?(<\/\2>)/is', "$1$3", $text);
        $content = html_entity_decode($content);
        return $content;
}

function getList($list, $depth = 1, $bootstrap = 0) {

    static $text = "";

    if ($bootstrap != 0) {
        $text = "";
        $depth = 0;
    }

    $type = $list->tag;

    if ($type == "ul") {
        $sign = "*";
    } elseif ($type == "ol") {
        $sign = "#";
    } elseif ($type == "dl") {
        $sign = ":";
    } else {
        return "";
    }

    if (count($list->children)) {
        $depth++;
    } else {
        $depth--;
    }

    foreach ($list->children as $li) {

        $list_item = Sunra\PhpSimple\HtmlDomParser::str_get_html($li->outertext);
        foreach ($list_item->find($type) as $listy) {
            $listy->outertext = "";
        }

        if (strip_tags($list_item->outertext) != "") {
            $text .= str_repeat($sign, $depth) . strip_tags($list_item->outertext) . "\r\n";
        }
        foreach ($li->children as $sublist) {
            getList($sublist, $depth, $text);
        }
    }

    return $text;
}

# $str - the HTML markup
# $row_delim - number of dashes used for a row
# $oneline - use one-line markup for cells - ||

function html2wiki_tables($str, $row_delim = 1, $oneline = false) {

    $str = str_replace("\r", '', $str);

    $my_nl = '=N=';
    $html_tags = array(
        # since PHP 5 str_ireplace() can be used for the end tags
        "/\n/",
        '/>\s+</', # spaces between tags
        '/<\/table>/i', # table end
        '/<\/caption>/i', # caption end
        '/<\/tr>/i', # rows end
        '/<\/th>/i', # headers end
        '/<\/td>/i', # cells end
        # e - replacement string gets evaluated before the replacement
        '/<table([^>]*)>/i', # table start
        '/<caption>/i', # caption start
        '/<tr(.*)>/Ui', # row start
        '/<th(.*)>/Ui', # header start
        '/<td(.*)>/Ui', # cell start
        "/\n$my_nl/",
        "/$my_nl/",
        "/\n */", # spaces at beginning of a line
    );

    $wiki_tags = array(
        " \n",
        '><', # remove spaces between tags
        "$my_nl|}", # table end
        '', '', '', '', # caption, rows, headers & cells end
        "'$my_nl{| '.trim(strip_newlines('$1'))", # table start
        "$my_nl|+", # caption
        "'$my_nl|'.str_repeat('-', $row_delim).' '.trim(strip_newlines('$1'))", # rows
        "'$my_nl! '.trim(strip_newlines('$1')).' | '", # headers
        "'$my_nl| '.trim(strip_newlines('$1')).' | '", # cells
        "\n",
        "\n",
        "\n",
    );

    # replace html tags with wiki equivalents
    $str = preg_replace($html_tags, $wiki_tags, $str);

    # remove table row after table start
    $str = preg_replace("/\{\|(.*)\n\|-+ *\n/", "{|$1\n", $str);

    # clear phase
    $s = array('!  |', '|  |', '\\"');
    $r = array('!', '|', '"');
    $str = str_replace($s, $r, $str);

    # use one-line markup for cells
    if ($oneline) {
        $prevcell = false; # the previous row is a table cell
        $prevhead = false; # the previous row is a table header
        $pos = -1;
        while (($pos = strpos($str, "\n", $pos + 1)) !== false) { #echo "\n$str\n";
            switch ($str{$pos + 1}) {
                case '|': # cell start
                    if ($prevcell && $str{$pos + 2} == ' ') {
                        $str = substr_replace($str, ' |', $pos, 1); # s/\n/ |/
                    } else if ($str{$pos + 2} == ' ') {
                        $prevcell = true;
                    } else {
                        $prevcell = false;
                    }
                    $prevhead = false;
                    break;
                case '!': # header cell start
                    if ($prevhead) {
                        $str = substr_replace($str, ' !', $pos, 1); # s/\n/ !/
                    } else {
                        $prevhead = true;
                    }
                    $prevcell = false;
                    break;
                case '{': # possible table start
                    if ($str{$pos + 2} == '|') { # table start
                        $prevcell = $prevhead = false;
                    } else {
                        $str{$pos} = ' ';
                    }
                    break;
                default: $str{$pos} = ' ';
            }
        }
    }
    return $str;
}

function strip_newlines($str) {
    return str_replace("\n", '', $str);
}
