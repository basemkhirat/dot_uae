<?php

class MigrationBuilder {

    private $table = false;
    private static $ignore = array('migrations');
    private static $database = "emirates";
    private static $migrations = false;
    private static $schema = array();
    private static $selects = array('column_name as Field', 'column_type as Type', 'is_nullable as Null', 'column_key as Key', 'column_default as Default', 'extra as Extra', 'data_type as Data_Type');
    private static $instance;
    private static $up = "";
    private static $down = "";

    private static function getTables() {
        return DB::select('SELECT table_name FROM information_schema.tables WHERE Table_Type="' . "BASE TABLE" . '" and table_schema="' . self::$database . '"');
    }

    private static function getTableDescribes($table) {
        return DB::table('information_schema.columns')
                        ->where('table_schema', '=', self::$database)
                        ->where('table_name', '=', $table)
                        ->get(self::$selects);
    }

    private static function getForeignTables() {
        return DB::table('information_schema.KEY_COLUMN_USAGE')
                        ->where('CONSTRAINT_SCHEMA', '=', self::$database)
                        ->where('REFERENCED_TABLE_SCHEMA', '=', self::$database)
                        ->select('TABLE_NAME')->distinct()
                        ->get();
    }

    private static function getForeigns($table) {
        return DB::table('information_schema.KEY_COLUMN_USAGE')
                        ->where('CONSTRAINT_SCHEMA', '=', self::$database)
                        ->where('REFERENCED_TABLE_SCHEMA', '=', self::$database)
                        ->where('TABLE_NAME', '=', $table)
                        ->select('COLUMN_NAME', 'REFERENCED_TABLE_NAME', 'REFERENCED_COLUMN_NAME')
                        ->get();
    }

    private static function compileSchema($class_name) {
        $upSchema = "";
        $downSchema = "";
        $newSchema = "";

        //print_r(self::$schema);

        foreach (self::$schema as $name => $values) {


            if (in_array($name, self::$ignore)) {
                continue;
            }
            $upSchema .= "
        {$values['up']}";
            $downSchema .= "
        {$values['down']}";
        }

        $schema = "<?php

use Illuminate\Database\Migrations\Migration;
 
class " . $class_name . " extends Migration {

    public function up(){
    " . $upSchema . "
    " . self::$up . "
    }

    public function down(){
    " . $downSchema . "
    " . self::$down . "
    }

}";

        return $schema;
    }

    public function up($up) {
        self::$up = $up;
        return self::$instance;
    }

    public function down($down) {
        self::$down = $down;
        return self::$instance;
    }

    public function ignore($tables) {
        self::$ignore = array_merge($tables, self::$ignore);
        return self::$instance;
    }

    public function migrations() {
        self::$migrations = true;
        return self::$instance;
    }

    public function write($module) {
        $schema = self::compileSchema(ucfirst($module) . "_create_" . $this->table . "_table");
        $filename = $module . "_create_" . $this->table . "_table.php";

        //$path = app()->databasePath() . '/migrations/';
        $path = modules_path($module."/migrations");

        file_put_contents($path ."/". $filename, $schema);
    }

    public function get() {
        return self::compileSchema();
    }

    public function convert($tb) {
        self::$schema = array();

        $this->table = $tb;
        self::$instance = new self();
        $database = self::$database;
        $table_headers = array('Field', 'Type', 'Null', 'Key', 'Default', 'Extra');
        //$tables = self::getTables();
        $tables = [];
        $table = new stdClass();
        $table->table_name = $tb;
        $tables[] = $table;
        foreach ($tables as $key => $value) {
            if (in_array($value->table_name, self::$ignore)) {
                continue;
            }

            $down = "Schema::drop('{$value->table_name}');";
            $up = "Schema::create('{$value->table_name}', function($" . "table) {\n";
            $tableDescribes = self
                    ::getTableDescribes($value->table_name);
            foreach ($tableDescribes as $values) {
                $method = "";
                $para = strpos($values->Type, '(');
                $type = $para > -1 ? substr($values->Type, 0, $para) : $values->Type;
                $numbers = "";
                $nullable = $values->Null == "NO" ? "" : "->nullable()";
                $default = empty($values->Default) ? "" : "->default(\"{$values->Default}\")";
                $unsigned = strpos($values->Type, "unsigned") === false ? '' : '->unsigned()';
                $unique = $values->Key == 'UNI' ? "->unique()" : "";
                $choices = '';
                switch ($type) {
                    case 'enum':
                        $method = 'enum';
                        $choices = preg_replace('/enum/', 'array', $values->Type);
                        $choices = ", $choices";
                        break;
                    case 'int' :
                        $method = 'unsignedInteger';
                        break;
                    case 'bigint' :
                        $method = 'bigInteger';
                        break;
                    case 'samllint' :
                        $method = 'smallInteger';
                        break;
                    case 'char' :
                    case 'varchar' :
                        $para = strpos($values->Type, '(');
                        $numbers = ", " . substr($values->Type, $para + 1, -1);
                        $method = 'string';
                        break;
                    case 'float' :
                        $method = 'float';
                        break;
                    case 'decimal' :
                        $para = strpos($values->Type, '(');
                        $numbers = ", " . substr($values->Type, $para + 1, -1);
                        $method = 'decimal';
                        break;
                    case 'tinyint' :
                        if ($values->Type == 'tinyint(1)') {
                            $method = 'boolean';
                        } else {
                            $method = 'tinyInteger';
                        }
                        break;
                    case 'date':
                        $method = 'date';
                        break;
                    case 'timestamp' :
                        $method = 'timestamp';
                        break;
                    case 'datetime' :
                        $method = 'dateTime';
                        break;
                    case 'mediumtext' :
                        $method = 'mediumtext';
                        break;
                    case 'text' :
                        $method = 'text';
                        break;
                }
                if ($values->Key == 'PRI') {
                    $method = 'increments';
                }
                $up .= "\t\t$" . "table->{$method}('{$values->Field}'{$choices}{$numbers}){$nullable}{$default}{$unsigned}{$unique};\n";
            }

            $up .= "\t});";
            self::$schema[$value->table_name] = array(
                'up' => $up,
                'down' => $down
            );
        }

        // add foreign constraints, if any
        $tableForeigns = self::getForeignTables();
        if (sizeof($tableForeigns) !== 0) {
            foreach ($tableForeigns as $key => $value) {
                $up = "Schema::table('{$value->TABLE_NAME}', function($" . "table) {\n";
                $foreign = self::getForeigns($value->TABLE_NAME);
                foreach ($foreign as $k => $v) {
                    $up .= " $" . "table->foreign('{$v->COLUMN_NAME}')->references('{$v->REFERENCED_COLUMN_NAME}')->on('{$v->REFERENCED_TABLE_NAME}');\n";
                }
                $up .= " });\n\n";
                self::$schema[$value->TABLE_NAME . '_foreign'] = array(
                    'up' => $up,
                    'down' => $down
                );
            }
        }

        return self::$instance;
    }

}
