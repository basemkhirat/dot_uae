<?php

class Loader {

    protected static $directories = [];

    public static function add($directories) {

        if (!is_array($directories)) {
            $directories = array($directories);
        }

        $pathes = [];
        foreach ($directories as $directory) {
            foreach (glob($directory . '/*.php') as $file) {
                $pathes[] = $file;
            }
        }

        static::$directories = array_unique(array_merge(static::$directories, $pathes));
    }

    public static function register() {
        foreach (static::$directories as $directory) {
            require_once($directory);
        }
    }

}
