<?php

use Illuminate\Support\ServiceProvider;

class AdminServiceProvider extends ServiceProvider {

    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Modules list.
     *
     * @var array
     */
    protected $modules = [];

    function boot() {

        /*
         * Module creator command
         */

        $this->commands(array('ModuleCommand', 'ModuleMigration'));

        // loading admin views and translations

        $this->loadViewsFrom(dirname(__DIR__) . '/views', 'admin');
        $this->loadTranslationsFrom(dirname(__DIR__) . '/lang', 'admin');

        // publishing admin core assets

        $this->publishes([
            dirname(__DIR__) . '/assets/' => public_path('admin'),
        ]);


        foreach ($this->modules as $module) {
            
            if (file_exists(dirname(__DIR__) . "/plugins/" . $module)) {
                $module_path = dirname(__DIR__) . "/plugins/" . $module;
            }elseif (file_exists(dirname(__DIR__) . "/modules/" . $module)) {
                $module_path = dirname(__DIR__) . "/modules/" . $module;
            }

            // publishing module assets

            if (file_exists($assets = $module_path . '/assets/')) {
                $this->publishes([
                    $assets => $module_path,
                ]);
            }

            // including module bootstrap file

            if (file_exists($bootstrap = $module_path . '/start.php')) {
                include $bootstrap;
            }

            // loading module views and translations

            $this->loadViewsFrom($module_path . '/views', $module);
            $this->loadTranslationsFrom($module_path . '/lang', $module);
        }
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register() {

        // loading admin configuration file
        $this->mergeConfigFrom(
                dirname(__DIR__) . '/config/config.php', null
        );

        Loader::add(array(
            dirname(__DIR__) . "/commands",
            dirname(__DIR__) . "/controllers",
            dirname(__DIR__) . "/models"
        ));

        /*
         * Loading admin providers
         */

        if (file_exists($autoload = dirname(__DIR__) . "/vendor/autoload.php")) {
            require_once $autoload;
        }

        foreach ((array) Config::get("providers") as $provider) {
            $this->app->register($provider);
        }

        $loader = \Illuminate\Foundation\AliasLoader::getInstance();
        foreach ((array) Config::get("aliases") as $alias => $class) {
            $loader->alias($alias, $class);
        }

        // Loading modules configuration files
        $this->modules = Config::get("modules");

        foreach ($this->modules as $module) {

            if (file_exists(dirname(__DIR__) . "/plugins/" . $module)) {
                $module_path = dirname(__DIR__) . "/plugins/" . $module;
            }elseif (file_exists(dirname(__DIR__) . "/modules/" . $module)) {
                $module_path = dirname(__DIR__) . "/modules/" . $module;
            }
   
            Loader::add(array(
                $module_path . "/controllers",
                $module_path . "/models",
                $module_path . "/migrations",
            ));

            if (file_exists($config = $module_path . '/config/config.php')) {
                $this->mergeConfigFrom(
                        $config, $module
                );
            }

            /*
             * Loading modules providers
             */

            if (file_exists($autoload = $module_path . '/vendor/autoload.php')) {
                require_once $autoload;
            }

            foreach ((array) Config::get("$module.providers") as $provider) {
                $this->app->register($provider);
            }

            foreach ((array) Config::get("$module.aliases") as $alias => $class) {
                $loader->alias($alias, $class);
            }
        }

        Loader::register();

        // loading admin bootstrap file
        include dirname(__DIR__) . '/start.php';
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides() {
        return [];
    }

}
