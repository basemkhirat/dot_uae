<?php

$router = $this->app['router'];


$router->before(function($request) {

    // getting all database options 
    foreach (Option::all() as $option) {
        Config::set(ltrim($option->module . "." . $option->name, "."), $option->value);
    }

    // set the time zone;
    date_default_timezone_set(Config::get("app.timezone"));

    // set current locale
    if (in_array(Request::segment(1), array_keys(Config::get('locales')))) {
        Session::put('locale', Request::segment(1));
        return Redirect::to(substr(Request::path(), 3));
    }

    if (Session::has('locale')) {
        App::setLocale(Session::get('locale'));
    } else {
        App::setLocale(Config::get('app.locale'));
    }

    define("LANG", App::getLocale());
    if (LANG == "ar") {
        define("DIRECTION", "rtl");
    } else {
        define("DIRECTION", "ltr");
    }

    // getting site status
    if (Request::segment(1) != ADMIN) {
        if (!Config::get("site_status")) {
            return view("admin::errors.offline");
        }
    }

    // set current user permissions
    $permissions = array();
    if (Auth::check()) {
        $permissions = RolePermission::where("role_id", Auth::user()->role_id)->lists("permission");
        if (!is_array($permissions)) {
            $permissions = $permissions->toArray();
        }
    }

    config::set("user_permissions", $permissions);


    // connection
    if (Input::has('conn')) {
        $conn = Input::get('conn');
        Session::put('conn', $conn);
    } else {
        if (Session::has('conn')) {
            $conn = Session::get('conn');
        } else {
            $conn = Config::get("dfLang");
            Session::put('conn', $conn);
        }
    }

    Config::set("conn", $conn);
});


Route::filter('auth', function() {
    if (Auth::guest()) {
        if (Request::ajax()) {
            return Response::make('Unauthorized', 401);
        } else {
            return Redirect::route('auth.login')->with("url", Request::url());
        }
    }
});

Route::filter('guest', function() {
    if (Auth::check())
        return Redirect::route('users.show');
});


/*
        $this->app['validator']->extend('has', function($attr, $value, $params) {
            if (!count($params)) {
                throw new \InvalidArgumentException('The has validation rule expects at least one parameter, 0 given.');
            }

            foreach ($params as $param) {
                switch ($param) {
                    case 'num':
                        $regex = '/\pN/';
                        break;
                    case 'letter':
                        $regex = '/\pL/';
                        break;
                    case 'lower':
                        $regex = '/\p{Ll}/';
                        break;
                    case 'upper':
                        $regex = '/\p{Lu}/';
                        break;
                    case 'special':
                        $regex = '/[\pP\pS]/';
                        break;
                    default:
                        $regex = $param;
                }

                if (!preg_match($regex, $value)) {
                    return false;
                }
            }

            return true;
        });


        $this->app['validator']->extend('minArr', function($attribute, $value, $parameters) {
            $minSize = (int) $parameters[0];
            $count = countNotEmpty($value);
            return ($count >= $minSize) ? TRUE : FALSE;
        });
*/