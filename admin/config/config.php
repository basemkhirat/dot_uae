<?php

return [

    /*
      |--------------------------------------------------------------------------
      | News aggregator URL
      |--------------------------------------------------------------------------
      |
      |
     */
    "grabber" => "http://52.17.148.86/grabber/index.php",
    /*
      |--------------------------------------------------------------------------
      | Languages
      |--------------------------------------------------------------------------
      |
      |
     */
    'locales' => [
        'ar' => "العربية",
        'en' => 'English'
    ],
    /*
      |--------------------------------------------------------------------------
      | Countries
      |--------------------------------------------------------------------------
      |
      |
     */
    'sites' => [
        'ar' => 'Arabic',
        'en' => 'English',
        'ur' => 'Urdu',
        'hi' => 'Hindi'
    ],
    'dfLang' => 'ar',
    /*
      |--------------------------------------------------------------------------
      | Modules
      |--------------------------------------------------------------------------
      |
      |
     */
    'modules' => [
        "users",
        "dashboard",
        "immediate",
        "posts",
        "options",
        "auth",
        "roles",
        "media",
        "newsletter",
        "shortener",
        "categories",
        "galleries",
        "authors",
        "reporters",
        "reporters",
        "activities",
        "grabber",
        "tags",
        "pages",
        "comments",
        "agency",
        "sets",
        "aggregator",
        "api",
        "mailposts",
        "topics",
        "navigations",
        "polls",
        "notifications",
        "mailbox",
        "sources",
        "chatbox",
        "groups",
        "tasks",
        "cms"
    ],
    /*
      |--------------------------------------------------------------------------
      | Admin Service providers
      |--------------------------------------------------------------------------
      |
      |
     */
    'providers' => [
        Illuminate\Html\HtmlServiceProvider::class,
        Jenssegers\Mongodb\MongodbServiceProvider::class,
        Shift31\LaravelElasticsearch\ElasticsearchServiceProvider::class,
    // Barryvdh\Debugbar\ServiceProvider::class,
    ],
    /*
      |--------------------------------------------------------------------------
      | Admin aliases
      |--------------------------------------------------------------------------
      |
      |
     */
    'aliases' => [
        'Str' => Illuminate\Support\Str::class,
        'Form' => Illuminate\Html\FormFacade::class,
        'HTML' => Illuminate\Html\HtmlFacade::class,
        "ES" => Elasticsearch\Client::class,

    //'Debugbar' => Barryvdh\Debugbar\Facade::class
    ]
];
