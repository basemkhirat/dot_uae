<?php

use Sunra\PhpSimple\HtmlDomParser as HtmlDom;

class WikiController extends BackendController {

    public $data = [];

    function __construct() {
        parent::__construct();
    }

    function index() {
        return view("admin::wiki", $this->data);
    }
    
    function export() {
       
        if (!Input::has("content")) {
            return "";
        }
        
        return html2wiki(Input::get("content"));
    }

    function api() {
        
        if (!Input::has("link")) {
            return "";
        }

        $link = Input::get("link");
        $slug = basename($link);
        $page = preg_replace("/\#.*/i", "", $slug);

        $content = file_get_contents("http://en.wikipedia.org/w/api.php?action=parse&prop=text&page=" . $page . "&format=json");
        $content = json_decode($content);
        $title = ($content->parse->title);
        $content = ($content->parse->text->{"*"});
        $html = HtmlDom::str_get_html($content);

        foreach ($html->find("a") as $a) {
            $a->href = "http://wikipedia.org/wiki/Municipal_incorporation" . $a->href;
        }

        foreach ($html->find("span.mw-editsection") as $span) {
            $span->outertext = "";
        }

        foreach ($html->find("table.infobox") as $span) {
            $span->style = "float:right";
        }

        foreach ($html->find(".thumb") as $element) {
            $element->style = "border: 1px solid #ccc;padding: 5px;margin:5px;" . $element->style;
        }

        foreach ($html->find(".tleft") as $element) {
            $element->style = "float:left;" . $element->style;
        }

        foreach ($html->find(".tright") as $element) {
            $element->style = "float:right;" . $element->style;
        }
        
        echo "<meta charset='utf-8' />";
        echo "<style>.navbox{width: 100%} .nowraplinks ul {   float: left; } .nowraplinks li {  float: left;   margin-left: 16px; }</style>";
        echo '<h1 contenteditable="false">'. $title.'</h1><hr />';
        echo $html->outertext;
    }

}
