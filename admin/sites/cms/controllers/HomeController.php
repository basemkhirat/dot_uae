<?php

namespace Cms;
use Illuminate\Routing\Controller;

class HomeController extends Controller{
    
    protected $data = [];
            
    function index(){
        
        return View("cms::home", $this->data);
    }
    
}