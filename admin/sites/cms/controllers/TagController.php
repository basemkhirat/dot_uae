<?php

namespace Cms;
use Illuminate\Routing\Controller;

class TagController extends Controller{
    
    protected $data = [];
            
    function index(){
        return View("cms::tag", $this->data);
    }
    
}
