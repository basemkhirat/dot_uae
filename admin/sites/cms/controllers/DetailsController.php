<?php

namespace Cms;
use Illuminate\Routing\Controller;

class DetailsController extends Controller{
    
    protected $data = [];
            
    function index(){

        return View("cms::details", $this->data);
    }
    
}
