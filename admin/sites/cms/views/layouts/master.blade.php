<!doctype html>
<html class="">
    <head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# website: http://ogp.me/ns/website#">
        <meta property="og:title" content="<?php echo Config::get("site_title"); ?>" />
        <meta property="og:site_name" content="<?php echo Config::get("site_name"); ?>" />
        <meta property="og:url" content="<?php echo Config::get("base_url"); ?>" />
        <meta property="og:description" content="<?php echo Config::get("site_description") ?>" />
        <meta property="og:image" content="<?php echo Cms\get("site_logo"); ?>" />
        <meta name="description" content="<?php echo Config::get("site_description") ?>" />
        <meta name="twitter:title" content="<?php echo Config::get("site_name"); ?>" />
        <meta name="twitter:description" content="<?php echo Config::get("site_description") ?>" />
        <meta name="twitter:image" content="http://www.dotmsr.com/dot/images/logo.png" />
        <meta name="twitter:url" content="<?php echo Config::get("base_url"); ?>" />
        <title><?php echo Config::get("site_title"); ?></title>
    </head>
    <body>
        @yield("content")
    </body>
</html>