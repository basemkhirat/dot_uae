@extends("cms::layouts.master")

@section("content")

<h2>welcome to home page</h2>

<?php foreach($posts = Post::with('categories', 'tags', 'comments', 'meta')->take(5)->paginate() as $post){ ?>
    
<a href="/details/<?php echo $post->post_slug; ?>">
    <?php echo $post->post_title; ?>
</a>

<hr />
    <?php echo $post->post_content; ?>
<br />
<?php } ?>

<?php echo $posts->render(); ?>

@stop