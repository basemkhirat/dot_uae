<?php

namespace Cms;

function get($key = false) {

    if (!$key) {
        return false;
    }

    switch ($key) {
        case "site_logo":
            return uploads_url(Config::get($key));
            break;
        default:
            if (Config::has($key)) {
                return Config::get($key);
            }
    }
}
