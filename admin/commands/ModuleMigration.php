<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Illuminate\Container\Container;

class ModuleMigration extends Command {

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'module:migrate';

    /**
     * The console command description.
     *
     * @var string
     */

    protected $description = "Migrate specific module";

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(Container $app) {
        parent::__construct();
        $this->app = $app;
    }

  
    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire() {
        
        $module = $this->argument("module");
        
        $migrations = $this->app->make('migration.repository');
        $migrations->createRepository();
        $migrator = $this->app->make('migrator');
        $migrator->run(modules_path($module).'/migrations');
        $this->info("Done");
    }
    
        /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments() {
        return [
            ['module', InputArgument::REQUIRED, 'Module name'],
        ];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions() {
        return [
        /*    ['plain', null, InputOption::VALUE_NONE, 'Module type', null], */
        ];
    }

}
